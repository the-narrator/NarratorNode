package integration.server;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Set;

import game.abilities.DrugDealer;
import game.logic.exceptions.NarratorException;
import game.logic.support.Constants;
import game.logic.templates.BasicRoles;
import json.JSONException;
import json.JSONObject;
import models.idtypes.GameID;
import models.schemas.PlayerSchema;
import nnode.LobbyManager;
import repositories.GameRepo;
import repositories.GameUserRepo;
import repositories.PlayerRepo;
import repositories.UserPermissionRepo;
import services.ModeratorService;
import util.UserFactory;

public class PlayerLobbyTests extends InstanceTests {

    public PlayerLobbyTests(String name) {
        super(name);
    }

    public void testBasicHostRepick() throws JSONException, SQLException {
        UserWrapper host = UserFactory.hostPublicGame();
        UserWrapper c1 = host.addUser();
        UserWrapper c2 = host.addUser();

        host.repick();
        assertFalse(host.isHost());
        assertTrue(c1.isHost() || c2.isHost());
        assertFalse(c1.isHost() && c2.isHost());
    }

    public void testHostRepickSpecific() throws JSONException, SQLException {
        UserWrapper host = UserFactory.hostPublicGame();
        UserWrapper c2 = host.addUser();
        c2.repick();
        host.addUser();

        host.repick(c2);
        assertFalse(host.isHost());
        assertTrue(c2.isHost());

        assertFalse(ModeratorService.lobbyRepickers.containsKey(host.getGameID()));
    }

    public void testSuperUserRepick() throws JSONException, SQLException {
        UserWrapper host = UserFactory.hostGame(UserFactory.PRIVATE, UserFactory.MOD);
        UserWrapper c1 = host.addUser();
        UserWrapper c2 = host.addUser();
        GameID gameID = host.getGameID();

        Set<Long> moderatorIDs = ModeratorService.getModeratorIDs(gameID);

        assertTrue(moderatorIDs.contains(host.id));
        UserPermissionRepo.removeAllPermissions(c1.id);

        c1.failRepick();
        moderatorIDs = ModeratorService.getModeratorIDs(gameID);
        assertTrue(moderatorIDs.contains(host.id));
        assertFalse(ModeratorService.lobbyRepickers.containsKey(gameID));

        host.repick();

        assertFalse(host.isHost());

        host.repick();

        assertTrue(host.isHost());

        host.repick(c2);

        assertFalse(host.isHost());
        assertTrue(c2.isHost());
    }

    public void testHostRepick() throws SQLException, JSONException {
        UserWrapper host = UserFactory.hostPublicGame();

        for(int i = 0; i < 8; i++)
            host.addUser();

        GameID gameID = GameUserRepo.getActiveGameIDByUserID(host.getUserID()).get();
        Set<Long> moderatorIDs = ModeratorService.getModeratorIDs(gameID);
        assertTrue(moderatorIDs.contains(host.id));

        ArrayList<UserWrapper> userWrappers = SwitchWrapper.getUserWrappers(host);
        for(int index = 0, repickCounter = 0; repickCounter < 5; index++){
            if(userWrappers.get(index) != host){
                userWrappers.get(index).repick();
                repickCounter++;
            }
        }

        moderatorIDs = ModeratorService.getModeratorIDs(gameID);
        assertFalse(moderatorIDs.contains(host.id));

        boolean foundHost = false;
        for(UserWrapper client: SwitchWrapper.getUserWrappers(host)){
            if(client.isHost()){
                client.repick(host.name);
                foundHost = true;
                break;
            }
        }
        if(!foundHost)
            fail("Host didn't change");
        assertTrue(host.isHost());
    }

    public void testKickOnlinePlayer() throws SQLException, JSONException {
        UserWrapper host = UserFactory.hostPublicGame();
        UserWrapper n1 = host.addUser();

        host.kick(n1);

        assertEquals(1, GameUserRepo.getIDs(host.getGameID()).size());
    }

    public void testKickOfflinePlayer() throws SQLException, JSONException {
        UserWrapper host = UserFactory.hostPublicGame();
        assertNotNull(host);
        UserWrapper n1 = host.addUser();

        host.kick(n1);

        assertEquals(1, GameUserRepo.getIDs(host.getGameID()).size());
    }

    public void testHostPrivateGame() throws JSONException, SQLException {
        int prevGameSize = GameRepo.getOpenGames().size();
        UserFactory.hostPrivateCustomGame();
        UserWrapper host2 = UserFactory.hostPrivateCustomGame();

        assertPresent(GameUserRepo.getActiveByUserID(host2.getUserID()));

        assertEquals(prevGameSize + 2, GameRepo.getOpenGames().size());
    }

    public void testRemoveRole() throws JSONException, SQLException {
        UserWrapper host = UserFactory.hostGame(PRIVATE, UserFactory.MOD);
        assertNotSame(0, setup.getFactions().size());

        // addSetupHidden updates BasicRoles
        host.addSetupHidden(BasicRoles.Citizen());
        assertEquals(1, BasicRoles.setup.rolesList._setupHiddenList.size());

        host.removeSetupHidden(BasicRoles.Citizen().role);
        assertEquals(0, BasicRoles.setup.rolesList.size(game));
    }

    public void testChangeName() throws JSONException, SQLException {
        UserWrapper host = UserFactory.hostGame(PRIVATE, UserFactory.MOD);
        GameID gameID = host.getGameID();
        host.addUser();
        String newName = "bigMoney";

        assertNotNull(PlayerRepo.getByNameAndGameID(host.getName(), gameID));
        host.changeName(newName);

        assertEquals(newName, PlayerRepo.getActiveByUserID(host.getUserID()).get().name);

        testBadNameChange(host, DrugDealer.BLOCKED);

        host.changeName(newName);

        testBadNameChange(host, "my.man");
    }

    private static void testBadNameChange(UserWrapper user, String name) throws JSONException {
        try{
            user.changeName(name);
            fail();
        }catch(NarratorException e){

        }
    }

    public void testBadInitialName() throws JSONException, SQLException {
        UserWrapper host = UserFactory.hostGame(PRIVATE, UserFactory.MOD);
        UserWrapper wb = host.addUser("my.man");
        assertNotSame(wb.getName(), "my.man");
    }

    public void testFullGameWarning() throws JSONException, SQLException {
        UserWrapper host = UserFactory.hostGame(PRIVATE, UserFactory.MOD);
        host.addSetupHidden(BasicRoles.Citizen());

        UserWrapper fodder = host;
        fodder.prefer(BasicRoles.Citizen());

        for(int i = 0; i < 7; i++)
            host.addPlayer(BasicRoles.Goon());

        host.hasWarning = false;
        assertPlayerSize(host.getGameID(), 8);
        UserWrapper failedJoiner = host.addUser();
        UserWrapper failedJoiner2 = host.addUser();

        assertPlayerSize(host.getGameID(), 8);
        assertTrue(failedJoiner.hasWarning);
        assertTrue(failedJoiner2.hasWarning);
        assertTrue(host.hasWarning);

    }

    public void testMaxCharacterLimit1() throws JSONException, SQLException {
        StringBuilder sb = new StringBuilder();
        for(int i = 0; i < Constants.MAX_DB_PLAYER_LENGTH + 1; i++)
            sb.append("q");

        UserWrapper userWrapper = UserFactory.hostGame(sb.toString(), true, true);

        PlayerSchema player = PlayerRepo.getActiveByUserID(userWrapper.id).get();
        assertTrue(player.name.length() <= Constants.MAX_DB_PLAYER_LENGTH);
    }

    // test same name 40 characters, should go to guest
    public void testMaxCharacterLimit2() throws JSONException, SQLException {
        LobbyManager.reloadInstances(); // clears out the other open lobbies.
        StringBuilder sb = new StringBuilder();
        for(int i = 0; i < Constants.MAX_DB_PLAYER_LENGTH; i++){
            sb.append("q");
        }
        UserWrapper host = UserFactory.hostGame(sb.toString(), PUBLIC, UserFactory.MOD);
        assertPlayerSize(host.getGameID(), 1);
        assertEquals(sb.toString(), host.getName());

        UserWrapper user2 = UserFactory.joinPublicGame(sb.toString());

        assertPlayerSize(host.getGameID(), 2);
        assertEquals(host.getJoinID(), user2.getJoinID());
        assertNotSame(sb.toString(), user2.getName());
    }

    private static void assertPlayerSize(GameID gameID, int expected) throws SQLException {
        int actual = PlayerRepo.getByGameID(gameID).size();
        assertEquals(expected, actual);
    }

    public void testMaxAutoPlayerLimit() throws JSONException, SQLException {
        UserWrapper host = UserFactory.hostGame(PRIVATE, UserFactory.MOD);
        host.addSetupHidden(BasicRoles.Goon(), BasicRoles.Goon(), BasicRoles.Citizen());
        host.addUser();
        host.addUser();

        host.hasWarning = false;

        host.addUser();

        assertTrue(host.hasWarning);
        host.hasWarning = false;
        host.autoStartCheck = false;
        host.startGame();

        assertInProgress();
    }

    public void testMinAutoPlayerLimit() throws JSONException, SQLException {
        UserWrapper host = UserFactory.hostGame(PRIVATE, UserFactory.MOD);
        host.addSetupHidden(BasicRoles.Goon(), BasicRoles.Goon(), BasicRoles.Citizen());

        host.hasWarning = false;
        host.autoStartCheck = false;

        JSONObject response = host.submitStartRequest();
        assertTrue(response.getJSONArray("errors").length() > 0);
        assertFalse(host.getPlayer().game.isStarted());
        assertTrue(host.hasWarning);
        host.hasWarning = false;

        host.addUser();
        host.addUser();
        host.startGame();

        assertInProgress();
    }
}
