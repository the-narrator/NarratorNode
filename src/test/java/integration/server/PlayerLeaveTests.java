package integration.server;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import game.abilities.Ghost;
import game.abilities.Goon;
import game.ai.Brain;
import game.logic.Game;
import game.logic.exceptions.NarratorException;
import game.logic.templates.BasicRoles;
import json.JSONException;
import json.JSONObject;
import models.enums.SetupModifierName;
import models.idtypes.GameID;
import models.schemas.GameOverviewSchema;
import models.schemas.PlayerSchema;
import nnode.LobbyManager;
import nnode.Permission;
import repositories.GameRepo;
import repositories.GameUserRepo;
import repositories.PlayerRepo;
import repositories.UserPermissionRepo;
import services.GameService;
import services.LobbyService;
import services.ModeratorService;
import services.SetupService;
import util.UserFactory;
import util.models.PlayerTestUtil;

public class PlayerLeaveTests extends InstanceTests {

    public PlayerLeaveTests(String name) {
        super(name);
    }

    UserWrapper host;
    GameOverviewSchema gameOverview;
    List<UserWrapper> users;
    long setupID;

    @Override
    public void setUp() throws Exception {
        super.setUp();
        host = UserFactory.hostPublicGame();
        this.setupID = host.getSetupID();
        this.gameOverview = GameRepo.getOverview(host.gameID);
    }

    @Override
    public void runTest() throws Throwable {
        super.runTest();
        BasicRoles.setup = SetupService.getSetup(this.setupID);
    }

    @Override
    public void tearDown() throws Exception {
        SwitchWrapper.stack.clear();
        super.tearDown();
    }

    public void testNotStartedGameNonHostLeaving() throws JSONException, SQLException {
        UserWrapper c1 = host.addUser();

        c1.leaveGame();

        assertGameIDInDatabase();
        assertNotInGame(c1);
        assertInPlayerTable(host);
        assertNotInPlayerTable(c1);
        assertHost(host);
        assertPlayerLeaveEventCalled(c1);
        assertGameEndEventNotCalled();
        assertLobbyCloseEventNotCalled();
    }

    public void testNotStartedGameNonSoloHostLeaving() throws JSONException, SQLException {
        UserWrapper c1 = host.addUser();

        host.leaveGame();

        assertGameIDInDatabase();
        assertNotInGame(host);
        assertInPlayerTable(c1);
        assertNotInPlayerTable(host);
        assertHost(c1);
        assertPlayerLeaveEventCalled(host);
        assertGameEndEventNotCalled();
        assertLobbyCloseEventNotCalled();
    }

    public void testNotStartedGameSoloHostLeaving() throws JSONException, SQLException {
        // NO GIVEN

        host.leaveGame();

        assertGameIDNotInDatabase();
        assertGameIsNotInMemory();
        assertNotInGame(host);
        assertNotInPlayerTable(host);
        assertPlayerLeaveEventNotCalled(host);
        assertGameEndEventNotCalled();
        assertLobbyCloseEventCalled();
    }

    public void testDeleteGameInLobby() throws SQLException, JSONException {

        deleteLobby();

        assertGameIDNotInDatabase();
        assertGameIsNotInMemory();
        assertNotInGame(host);
        assertPlayerLeaveEventNotCalled(host);
        assertGameEndEventNotCalled();
        assertLobbyCloseEventCalled();
    }

    private void deleteLobby() throws SQLException {
        LobbyService.delete(GameRepo.getOverview(gameOverview.id));
    }

    public void testDeleteGameInGame() throws JSONException, SQLException {
        startGame();

        deleteLobby();

        assertGameIDNotInDatabase();
        assertGameIsNotInMemory();
        assertGameCannotBeJoined();
        assertNotInGame(host);
        for(UserWrapper user: users)
            assertNotInGame(user);
        assertPlayerLeaveEventNotCalled(host);
        for(UserWrapper user: users)
            assertPlayerLeaveEventNotCalled(user);
        assertGameEndEventNotCalled();
        assertLobbyCloseEventCalled();
    }

    public void testNoLeavingGame() throws JSONException, SQLException {
        startGame();
        UserWrapper leaver = users.get(0);

        leaveFail(leaver);

        assertGameIDInDatabase();
        assertGameIsInMemory();
        assertGameCannotBeJoined();
        assertInGame(leaver);
        assertInPlayerTable(leaver);
        assertPlayerNotExited(leaver);
        assertPlayerLeaveEventNotCalled(leaver);
        assertGameEndEventNotCalled();
        assertLobbyCloseEventNotCalled();
    }

    public void testGhostNoLeavingGame() throws JSONException, SQLException {
        startGame();
        UserWrapper leaver = getGhost();
        Game game = GameService.getByGameID(gameOverview.id);
        for(UserWrapper user: users){
            if(game.isDay())
                user.vote(leaver);
        }

        leaveFail(leaver);

        assertGameIDInDatabase();
        assertGameIsInMemory();
        assertGameCannotBeJoined();
        assertInGame(leaver);
        assertInPlayerTable(leaver);
        assertPlayerNotExited(leaver);
        assertPlayerLeaveEventNotCalled(leaver);
        assertGameEndEventNotCalled();
        assertLobbyCloseEventNotCalled();
    }

    private static void leaveFail(UserWrapper leaver) throws JSONException {
        try{
            leaver.leaveGame();
            fail();
        }catch(NarratorException e){
            assertEquals("You cannot leave if you are alive.", e.getMessage());
        }
    }

    public void testLeavingIfNotParticipating() throws JSONException, SQLException {
        startGame();
        UserWrapper leaver = getGoon();
        Game game = GameService.getByGameID(gameOverview.id);
        for(UserWrapper user: users){
            if(game.isDay())
                user.vote(leaver);
        }

        leaver.leaveGame();

        assertGameIDInDatabase();
        assertGameIsInMemory();
        assertGameCannotBeJoined();
        assertNotInGame(leaver);
        assertInPlayerTable(leaver);
        assertPlayerExited(leaver);
        assertPlayerLeaveEventNotCalled(leaver);
        assertGameEndEventNotCalled();
        assertLobbyCloseEventNotCalled();
    }

    public void testLeavingIfOnlyBotsRemain() throws JSONException, SQLException {
        BasicRoles.setup = host.getSetup();
        host.addSetupHidden(BasicRoles.SerialKiller());
        host.addSetupHidden(BasicRoles.Citizen(), 2);
        host.addSetupHidden(BasicRoles.Goon(), 4);
        host.updateSetupModifier(SetupModifierName.SELF_VOTE, true);
        UserPermissionRepo.addPermission(host.id, Permission.BOT_ADDING);
        host.addBots(6);
        host.startGame(Game.DAY_START);
        host.vote(host);
        host.endPhase();

        host.leaveGame();

        assertGameIDInDatabase();
        assertPlayerExited(host);
        assertGameIsNotInMemory();
        assertGameCannotBeJoined();
        assertNotInGame(host);
        assertInPlayerTable(host);
        assertLobbyCloseEventCalled();
    }

    public void testDeleteGameOnFinishedGame() throws JSONException, SQLException {
        endGame();

        deleteLobby();

        assertGameEndEventCalled();
        assertPlayerExited(host);
        for(UserWrapper user: users)
            assertPlayerExited(user);
        assertGameIDInDatabase();
        assertGameIsNotInMemory();
        assertGameCannotBeJoined();
        assertNotInGame(host);
        assertInPlayerTable(host);
        for(UserWrapper user: users)
            assertInPlayerTable(user);
        for(UserWrapper user: users)
            assertPlayerExited(user);
        assertLobbyCloseEventCalled();
        assertPlayerLeaveEventNotCalled(host);
        for(UserWrapper user: users)
            assertPlayerLeaveEventNotCalled(user);
    }

    public void testOnEndGame() throws JSONException, SQLException {
        // NO GIVEN

        endGame();

        assertGameEndEventCalled();
        assertLobbyCloseEventNotCalled();
        assertPlayerLeaveEventNotCalled(host);
        for(UserWrapper user: users)
            assertPlayerLeaveEventNotCalled(user);
        assertPlayerNotExited(host);
        for(UserWrapper user: users)
            assertPlayerNotExited(user);
        assertGameIDInDatabase();
        assertGameIsInMemory();
        assertGameCannotBeJoined();
        assertInGame(host);
        for(UserWrapper user: users)
            assertInGame(user);
        assertInPlayerTable(host);
        for(UserWrapper user: users)
            assertInPlayerTable(user);

        host.leaveGame();

        assertLobbyCloseEventNotCalled();
        assertPlayerExited(host);
        for(UserWrapper user: users)
            assertPlayerNotExited(user);
        assertGameIDInDatabase();
        assertGameIsInMemory();
        assertGameCannotBeJoined();
        assertNotInGame(host);
        for(UserWrapper user: users)
            assertInGame(user);
        assertInPlayerTable(host);
        for(UserWrapper user: users)
            assertInPlayerTable(user);

        for(int i = 1; i < users.size(); i++)
            users.get(i).leaveGame();

        assertPlayerExited(host);
        for(int i = 1; i < users.size(); i++)
            assertPlayerExited(users.get(i));
        assertGameIDInDatabase();
        assertGameIsInMemory();
        assertGameCannotBeJoined();
        assertNotInGame(host);
        for(int i = 1; i < users.size(); i++)
            assertNotInGame(users.get(i));
        assertInPlayerTable(host);
        for(UserWrapper user: users)
            assertInPlayerTable(user);

        users.get(0).leaveGame();
        assertPlayerLeaveEventNotCalled(users.get(0));
        assertLobbyCloseEventCalled();
        assertGameIDInDatabase();
        assertGameIsNotInMemory();
        assertGameCannotBeJoined();
        assertNotInGame(users.get(0));
    }

    private void startGame() throws JSONException, SQLException {
        users = new ArrayList<>();
        for(int i = 0; i < 6; i++)
            users.add(host.addUser());
        BasicRoles.setup = SetupService.getSetup(this.gameOverview.setupID);
        host.addSetupHidden(BasicRoles.Ghost());
        host.addSetupHidden(BasicRoles.Vigilante());
        host.addSetupHidden(BasicRoles.SerialKiller());
        host.addSetupHidden(BasicRoles.Goon());
        host.addSetupHidden(BasicRoles.Citizen());
        host.addSetupHidden(BasicRoles.Citizen());
        host.addSetupHidden(BasicRoles.Citizen());
        host.startGame(Game.DAY_START);
    }

    private void endGame() throws JSONException, SQLException {
        startGame();
        Game game = GameService.getByGameID(gameOverview.id);
        Brain.EndGame(game, 0);
    }

    private UserWrapper getGhost() throws SQLException {
        return getRole(Ghost.class.getSimpleName());
    }

    private UserWrapper getGoon() throws SQLException {
        return getRole(Goon.class.getSimpleName());
    }

    private UserWrapper getRole(String roleName) throws SQLException {
        PlayerSchema player = PlayerTestUtil.getSchemaByRoleName(host.getLobby(), roleName);
        long userID = player.userID.get();
        if(host.getUserID() == userID)
            return host;
        for(UserWrapper user: users)
            if(userID == user.id)
                return user;
        throw new NullPointerException();
    }

    private void assertGameIDInDatabase() throws SQLException {
        List<GameID> ids = GameRepo.getAllIDs();
        assertTrue(ids.contains(gameOverview.id));
    }

    private void assertGameIDNotInDatabase() throws SQLException {
        List<GameID> ids = GameRepo.getAllIDs();
        assertFalse(ids.contains(gameOverview.id));
    }

    private void assertGameCannotBeJoined() throws SQLException {
        try{
            GameOverviewSchema game = GameRepo.getOverview(gameOverview.id);
            assertTrue(game.isStarted);
        }catch(NarratorException e){
            String message = "No game with that ID found.";
            assertEquals(message, e.getMessage());
        }

    }

    private void assertGameIsInMemory() {
        assertTrue(LobbyManager.idToLobby.containsKey(gameOverview.lobbyID));
        assertTrue(GameService.gamesMap.containsKey(gameOverview.id));
    }

    private void assertGameIsNotInMemory() {
        assertFalse(LobbyManager.idToLobby.containsKey(gameOverview.lobbyID));
        assertFalse(GameService.gamesMap.containsKey(gameOverview.id));
    }

    private void assertNotInGame(UserWrapper user) throws SQLException {
        assertEmpty(GameUserRepo.getActiveByUserID(user.getUserID()));
    }

    private void assertInGame(UserWrapper user) throws SQLException {
        assertPresent(GameUserRepo.getActiveByUserID(user.getUserID()));
    }

    private void assertHost(UserWrapper user) throws SQLException {
        ModeratorService.assertIsGameModerator(user.getGameID(), user.id, "Is not host");
    }

    private void assertPlayerNotExited(UserWrapper user) throws SQLException {
        assertTrue(GameUserRepo.getActiveByUserID(user.getUserID()).isPresent());
    }

    private void assertPlayerExited(UserWrapper user) throws SQLException {
        assertFalse(GameUserRepo.getActiveByUserID(user.getUserID()).isPresent());
    }

    private void assertPlayerLeaveEventNotCalled(UserWrapper user) throws JSONException {
        for(JSONObject event: SwitchWrapper.serverStack){
            if(!event.getString("event").equals("playerRemove"))
                continue;
            if(event.getLong("userID") != user.id)
                continue;
            fail("leave event fired");
        }
    }

    private void assertPlayerLeaveEventCalled(UserWrapper user) throws JSONException {
        for(JSONObject event: SwitchWrapper.serverStack){
            if(!event.getString("event").equals("playerRemove"))
                continue;
            if(event.getLong("userID") != user.id)
                continue;
            return;
        }
        fail("leave event not fired");
    }

    private void assertGameEndEventNotCalled() throws JSONException {
        for(JSONObject event: SwitchWrapper.serverStack){
            if(!event.getString("event").equals("gameEnd"))
                continue;
            if(!event.getString("joinID").equals(gameOverview.lobbyID))
                continue;
            fail("close event fired");
        }
    }

    private void assertGameEndEventCalled() throws JSONException {
        for(JSONObject event: SwitchWrapper.serverStack){
            if(!event.getString("event").equals("gameEnd"))
                continue;
            if(!event.getString("joinID").equals(gameOverview.lobbyID))
                continue;
            return;
        }
        fail("close event not fired");
    }

    private void assertLobbyCloseEventNotCalled() throws JSONException {
        for(JSONObject event: SwitchWrapper.serverStack){
            if(!event.getString("event").equals("lobbyDelete"))
                continue;
            if(!event.getString("joinID").equals(gameOverview.lobbyID))
                continue;
            fail("close event fired");
        }
    }

    private void assertLobbyCloseEventCalled() throws JSONException {
        for(JSONObject event: SwitchWrapper.serverStack){
            if(!event.getString("event").equals("lobbyDelete"))
                continue;
            if(!event.getString("joinID").equals(gameOverview.lobbyID))
                continue;
            return;
        }
        fail("close event not fired");
    }

    private void assertInPlayerTable(UserWrapper user) throws SQLException {
        // throws no errors
        PlayerRepo.getByGameIDAndUserID(gameOverview.id, user.id);
    }

    private void assertNotInPlayerTable(UserWrapper user) throws SQLException {
        try{
            PlayerRepo.getByGameIDAndUserID(gameOverview.id, user.id);
            fail("player found when not supposed to be in db.");
        }catch(SQLException e){
        }
    }
}
