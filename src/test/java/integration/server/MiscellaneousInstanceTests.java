
package integration.server;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import game.abilities.Agent;
import game.abilities.Architect;
import game.abilities.Blacksmith;
import game.abilities.BreadAbility;
import game.abilities.Bulletproof;
import game.abilities.Disguiser;
import game.abilities.Douse;
import game.abilities.Driver;
import game.abilities.FactionKill;
import game.abilities.FactionSend;
import game.abilities.Framer;
import game.abilities.Godfather;
import game.abilities.GraveDigger;
import game.abilities.Hidden;
import game.abilities.JailExecute;
import game.abilities.JailCreate;
import game.abilities.Mayor;
import game.abilities.Puppet;
import game.abilities.SerialKiller;
import game.abilities.Block;
import game.abilities.Ventriloquist;
import game.abilities.Vote;
import game.ai.Controller;
import game.event.DayChat;
import game.event.JailChat;
import game.event.Message;
import game.event.VoidChat;
import game.logic.Game;
import game.logic.GameFaction;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.exceptions.NarratorException;
import game.logic.support.CommandHandler;
import game.logic.support.action.Action;
import game.logic.support.action.ActionList;
import game.logic.templates.BasicRoles;
import game.setups.Setup;
import json.JSONArray;
import json.JSONException;
import json.JSONObject;
import models.Command;
import models.Faction;
import models.FactionRole;
import models.Role;
import models.enums.AbilityModifierName;
import models.enums.GameModifierName;
import models.enums.RoleModifierName;
import models.enums.SetupModifierName;
import models.idtypes.GameID;
import models.json_output.profile.ProfileAllyJson;
import models.requests.ActionSubmitRequest;
import nnode.Lobby;
import nnode.StateObject;
import nnode.UnreadManager;
import repositories.CommandRepo;
import repositories.GameUserRepo;
import repositories.PlayerRepo;
import repositories.SetupRepo;
import services.ChatService;
import services.FactionRoleService;
import services.GameService;
import services.LobbyService;
import util.Assertions;
import util.CollectionUtil;
import util.UserFactory;
import util.game.GameUtil;
import util.game.LookupUtil;

public class MiscellaneousInstanceTests extends InstanceTests {

    public MiscellaneousInstanceTests(String name) {
        super(name);
    }

    public static final boolean DAY = Game.DAY_START, NIGHT = Game.NIGHT_START;

    public void testJoinedStoryPlayerWritingBasic() throws SQLException, JSONException {
        UserWrapper host = UserFactory.hostPublicGame();
        host.addUser();
        assertEquals(2, getGameUserSize(host));
        host.addUser();
        assertEquals(3, getGameUserSize(host));

        host.addSetupHidden(BasicRoles.Agent());
        host.addSetupHidden(BasicRoles.Citizen());
        host.addSetupHidden(BasicRoles.SerialKiller());

        assertEqual(3, Setup.getMaxPlayerCount(host.getSetup(), GameUtil.getPermissiveModifiers()));

        GameID gameID = host.startGame();

        Assertions.assertGamePlayerCount(gameID);
    }

    public void testGuestJoinedPlayerSize() throws SQLException, JSONException {
        UserWrapper host = UserFactory.hostPublicGame();
        host.addUser();
        assertEquals(2, getGameUserSize(host));
        host.addUser();
        assertEquals(3, getGameUserSize(host));

        host.addSetupHidden(BasicRoles.Agent());
        host.addSetupHidden(BasicRoles.Citizen());
        host.addSetupHidden(BasicRoles.SerialKiller());
        GameID replayID = host.startGame();

        Assertions.assertGamePlayerCount(replayID);
    }

    private static int getGameUserSize(UserWrapper user) throws SQLException {
        return GameUserRepo.getIDs(user.getGameID()).size();
    }

    public void testEscortPlayerSizeLength() throws JSONException, SQLException {
        UserWrapper host = UserFactory.hostPrivateCustomGame();
        GameID gameID = GameUserRepo.getActiveGameIDByUserID(host.getUserID()).get();
        host.addBots(2);
        assertEquals(3, PlayerRepo.getByGameID(gameID).size());

        host.addSetupHidden(BasicRoles.Escort());
        host.addSetupHidden(BasicRoles.Arsonist());
        host.addSetupHidden(BasicRoles.Witch());

        host.prefer(BasicRoles.Escort());

        host.startGame();

        assertEquals(1, host.getActions().size());
        assertTrue(host.getActions().containsKey(Block.COMMAND));
    }

    public void testChatTabs() throws JSONException, SQLException {
        UserWrapper host = UserFactory.hostPrivateCustomGame();
        List<String> botNames = new ArrayList<>(host.addBots(2));
        assertEquals(2, botNames.size());

        host.addSetupHidden(BasicRoles.Escort());
        host.addSetupHidden(BasicRoles.Arsonist());
        host.addSetupHidden(BasicRoles.Witch());

        host.prefer(BasicRoles.Witch());

        host.startGame();
        Game game = host.getGame();
        cancelBrain(host);

        Set<String> keys;
        assertTrue(game.isNight());
        assertTrue(game.isFirstNight());
        assertEquals(0, game.getDayNumber());
        assertEquals(2, host.chatLogs.size());
        assertTrue(host.chatLogs.containsKey(VoidChat.NAME + " " + host.getLobby().game.getDayNumber()));

        host.endNight();

        assertTrue(game.isDay());
        keys = host.chatLogs.keySet();
        assertTrue(keys.contains("Day 1"));
        assertTrue(keys.contains("Night 0"));
        assertFalse(keys.contains("Night 1"));

        host.skipVote();
        host.endPhase();

        // n1
        assertTrue(game.isNight());

        host.endNight();

        // day 2
        keys = host.chatLogs.keySet();
        assertTrue(keys.contains("Day 1"));
        assertTrue(keys.contains("Day 2"));
        assertFalse(keys.contains("Night 0"));
        assertTrue(keys.contains("Night 1"));
        assertFalse(keys.contains("Night 2"));

        host.skipVote();
        host.endPhase();

        // night 2
        keys = host.chatLogs.keySet();
        assertTrue(keys.contains("Day 1"));
        assertTrue(keys.contains("Day 2"));
        assertFalse(keys.contains("Night 0"));
        assertFalse(keys.contains("Night 1"));
        assertTrue(keys.contains("Night 2"));

        host.endNight();

        host.vote(botNames.get(1));
        host.endPhase();

        if(game.isInProgress()){
            host.endPhase();
        }

        assertGameOver();
        host.getPlayer().getChatKeys();
        assertEquals(1, host.keys.size());
    }

    public void testMafChatLogsDayTime() throws JSONException, SQLException {
        UserWrapper host = UserFactory.hostPrivateCustomGame();
        Set<String> bots = host.addBots(7);

        Faction townFaction = host.getSetup().getFactionByColor(Setup.TOWN_C);
        Role drugDealer = BasicRoles.DrugDealer().role;
        FactionRole factionRole = FactionRoleService.createFactionRole(host.id, townFaction.id,
                drugDealer.id).factionRole;

        for(int i = 0; i < 5; i++)
            host.addSetupHidden(BasicRoles.Chauffeur());
        for(int i = 0; i < 3; i++)
            host.addSetupHidden(factionRole);

        host.prefer(BasicRoles.Chauffeur());
        host.startGame();

        assertTrue(host.chatLogs.containsKey(game.getEventManager().voidChat.getName()));

        host.setNightTargetByName(FactionKill.abilityType, bots.iterator().next());
        host.endNight();
    }

    public void testSeparateBasicRoles() throws JSONException, SQLException {
        // host2 needs to be first, because basicRoles.setup references the last game
        // created
        UserWrapper host2 = UserFactory.hostGame(PUBLIC, UserFactory.MOD);
        UserWrapper host1 = UserFactory.hostGame(PUBLIC, UserFactory.MOD);

        host1.upsertFactionRoleAbilityModifier(BasicRoles.Blacksmith(), Blacksmith.abilityType,
                AbilityModifierName.GS_FAULTY_GUNS, true);
        host1.upsertFactionRoleModifier(BasicRoles.Blacksmith(), RoleModifierName.UNDETECTABLE, true);

        Setup setup1 = host1.getSetup();
        Setup setup2 = host2.getSetup();

        assertTrue(setup1.id != setup2.id);

        String name = BasicRoles.Blacksmith().role.getName();

        FactionRole factionRole1 = LookupUtil.findFactionRole(setup1, name, Setup.TOWN_C).get();
        FactionRole factionRole2 = LookupUtil.findFactionRole(setup2, name, Setup.TOWN_C).get();

        assertTrue(factionRole1.roleModifiers.getBoolean(RoleModifierName.UNDETECTABLE, game));
        assertTrue(factionRole1.abilityModifiers.get(Blacksmith.abilityType)
                .getBoolean(AbilityModifierName.GS_FAULTY_GUNS, game));

        try{
            host2.upsertFactionRoleAbilityModifier(factionRole2, Blacksmith.abilityType,
                    AbilityModifierName.GS_FAULTY_GUNS, 1);
            fail();
        }catch(NarratorException e){
            assertEquals("Modifier \"GS_FAULTY_GUNS\" needs a boolean but got integer.", e.getMessage());
        }
        try{
            host2.upsertFactionRoleModifier(factionRole2, RoleModifierName.AUTO_VEST, true);
            fail();
        }catch(NarratorException e){
            assertEquals("Modifier \"AUTO_VEST\" needs a integer but got boolean.", e.getMessage());
        }

        assertFalse(factionRole2.abilityModifiers.containsKey(Blacksmith.abilityType));
        assertEqual(0, factionRole2.roleModifiers.getInt(RoleModifierName.AUTO_VEST, game));
    }

    public void testJailorTargeting() throws JSONException, SQLException {
        UserWrapper host = UserFactory.hostPrivateCustomGame();
        List<String> botNames = new ArrayList<>(host.addBots(2));

        host.addSetupHidden(BasicRoles.Jailor());
        host.addSetupHidden(BasicRoles.Kidnapper());
        host.addSetupHidden(BasicRoles.Kidnapper());

        host.prefer(BasicRoles.Jailor());

        GameID gameID = host.startGame();
        Game game = GameService.getByGameID(gameID);
        cancelBrain(host);

        host.endNight();

        assertFalse(host.abilities.get(JailCreate.JAIL).isEmpty());
        host.doDayAction(botNames.get(0));
        assertFalse(host.submittedActions.isEmpty());

        host.cancelAction(0);
        assertTrue(host.submittedActions.isEmpty());
        assertTrue(host.getGame().actionStack.isEmpty());

        host.doDayAction(botNames.get(0));
        ActionList jailedTargets = host.getPlayer().getActions();
        assertFalse(jailedTargets.isEmpty());
        assertEquals(botNames.get(0), jailedTargets.getFirst().getTarget().getName());
        host.vote(botNames.get(1));
        host.endPhase();

        Player jailedPlayer = game.getPlayerByName(botNames.get(0));
        assertTrue(jailedPlayer.isJailed());
        assertFalse(host.getPlayer().isJailed());
        assertTrue(host.getPlayer().getActions().isEmpty());
        assertTrue(game.isInProgress());
        assertTrue(host.getActions().containsKey(JailCreate.NO_EXECUTE));

        host.endNight();

        assertFalse(game.isInProgress());
        host.requestChat();
    }

    public void testJailorChats() throws JSONException, SQLException {
        UserWrapper host = UserFactory.hostGame(UserFactory.PRIVATE, !UserFactory.MOD);
        Set<String> botNames = host.addBots(2);
        String botName = botNames.iterator().next();

        host.addSetupHidden(BasicRoles.Jailor());
        host.addSetupHidden(BasicRoles.Kidnapper());
        host.addSetupHidden(BasicRoles.Kidnapper());

        host.prefer(BasicRoles.Jailor());

        host.startGame(Game.DAY_START);
        cancelBrain(host);
        host.requestAllGameInfo();

        host.doDayAction(botName);
        host.skipVote();
        host.endPhase();

        host.say("Hello", JailChat.GetKey(botName, 1));
        host.requestChat();
        assertFalse(host.chatLogs.get(JailChat.GetName(botName)).isEmpty());
    }

    public void testJailorChatLook() throws JSONException, SQLException {
        UserWrapper host = UserFactory.hostPublicGame();
        UserWrapper jailor = host;
        UserWrapper fodder = jailor.addUser();
        UserWrapper rando = jailor.addUser();

        host.addSetupHidden(BasicRoles.Jailor());
        host.addSetupHidden(BasicRoles.Citizen());
        host.addSetupHidden(BasicRoles.Kidnapper());

        jailor.prefer(BasicRoles.Jailor());
        fodder.prefer(BasicRoles.Kidnapper());

        host.startGame(Game.DAY_START);
        jailor.requestAllGameInfo();

        jailor.doDayAction(fodder);
        jailor.skipVote();
        fodder.skipVote();

        String key = JailChat.GetKey(fodder.getPlayer(), 1);
        String jailName = JailChat.GetName(fodder.getPlayer());

        assertFalse(fodder.abilities.containsKey(FactionKill.COMMAND));

        jailor.say("Hello", key);

        assertTrue(fodder.chatLogs.containsKey(jailName));
        partialContains(fodder.chatLogs.get(jailName), JailCreate.CAPTOR);
        partialContains(fodder.chatLogs.get(jailName), Message.INITIAL);

        fodder.say("Dont kill me", key);
        // test look from both sides
        assertFalse(jailor.chatLogs.get(jailName).isEmpty());

        jailor.setNightTarget(JailExecute.COMMAND, fodder);
        jailor.endNight();
        fodder.endNight();
        rando.endNight();

        assertFalse(game.isInProgress());
    }

    // i really only care that if i manipulate the setup, add bots, and start, that
    // i'm not creating more setups
    public void testSetupCountRemainsConstant() throws JSONException, SQLException {
        UserWrapper host = UserFactory.hostPrivateCustomGame();
        long setupID = host.createSetup().getJSONObject("response").getLong("id");
        int setupCount = SetupRepo.getCount();

        host.setSetup(setupID);
        host.addSetupHidden(Hidden.MafiaRandom(), 7);
        host.addSetupHidden(Hidden.TownProtective(), 7);

        assertEquals(setupCount, SetupRepo.getCount());

        host.addBots(13);
        assertEquals(setupCount, SetupRepo.getCount());

        GameID gameID = host.startGame();
        assertEquals(setupCount, SetupRepo.getCount());

        LobbyService.getByGameID(gameID).cancelBrain();
        BrainEndGame = false;
        skipTearDown = true;
    }

    public void testGodfatherRuleTexts() throws JSONException, SQLException {
        UserWrapper host = UserFactory.hostPrivateCustomGame();

        Faction faction = host.getSetup().getFactionByColor(Setup.MAFIA_C);
        boolean godfatherHasModifier = false;
        for(FactionRole rt: faction.roleMap.values()){
            if(rt.role.hasAbility(Godfather.abilityType))
                godfatherHasModifier = rt.role.modifiers.getBoolean(RoleModifierName.UNDETECTABLE, game);
        }
        assertTrue(godfatherHasModifier);

        Set<String> botNames = host.addBots(3);

        host.addSetupHidden(BasicRoles.Citizen());
        host.addSetupHidden(BasicRoles.DrugDealer());
        host.addSetupHidden(BasicRoles.Framer());
        host.addSetupHidden(BasicRoles.Godfather());

        host.prefer(BasicRoles.Godfather());

        host.startGame(Game.NIGHT_START);

        GameFaction hostFaction = host.getPlayer().getGameFaction();
        Player botPlayer;
        for(String botName: botNames){
            botPlayer = hostFaction.game.getPlayerByName(botName);
            if(hostFaction != botPlayer.getGameFaction())
                host.setNightTargetByName(FactionKill.abilityType, botName);
        }

        host.endNight();

        assertFalse(game.isInProgress());
    }

    private void clearUnreads(UserWrapper... wpws) {
        for(UserWrapper wpw: wpws)
            for(int i = 0; i < wpw.unread.size();)
                wpw.unread.clear();
    }

    public void testNewMessageRecord1() throws JSONException, SQLException {
        UserWrapper host = UserFactory.hostPrivateCustomGame();
        UserWrapper jailor = host.addUser();
        UserWrapper mayor = host.addUser();

        host.addBots(2);

        host.addSetupHidden(BasicRoles.Mayor());
        host.addSetupHidden(BasicRoles.Jailor());
        host.addSetupHidden(BasicRoles.Sheriff());
        host.addSetupHidden(BasicRoles.Bulletproof());
        host.addSetupHidden(BasicRoles.Assassin());

        mayor.prefer(BasicRoles.Mayor());
        jailor.prefer(BasicRoles.Jailor());

        host.startGame(Game.DAY_START);
        cancelBrain(host);

        assertTrue(game.isDay());
        assertTrue(mayor.getPlayer().is(Mayor.abilityType));

        assertFalse(host.unread.isEmpty());

        String chatKey = game.getEventManager().getDayChat().getName();
        assertUnreadCount(host, chatKey, 1);

        host.unread.clear();

        host.requestUnreads();
        assertUnreadCount(host, chatKey, 1);

        host.setRead(chatKey);
        assertUnreadCount(host, chatKey, 0);

        host.requestUnreads();
        assertTrue(host.isCaughtUp());

        jailor.setRead(chatKey);

        host.vote(jailor);
        assertUnreadCount(host, chatKey, 0);
        assertUnreadCount(jailor, chatKey, 1);

        host.skipVote();
        assertUnreadCount(host, chatKey, 0);
        assertUnreadCount(jailor, chatKey, 2);

        jailor.setRead(chatKey);

        host.say("lol", game.getEventManager().getDayChat().getKey(host.getPlayer()));
        UnreadManager manager = ChatService.getUnreadManager(host.getGameID(), chatKey, null);

        assertUnreadCount(host, chatKey, 0);
        assertUnreadCount(jailor, chatKey, 1);
        assertEquals(1, manager.unreadCount(jailor.getUserID()));

        jailor.say("lol back", game.getEventManager().getDayChat().getKey(jailor.getPlayer()));
        assertEquals(0, manager.unreadCount(jailor.getUserID()));

        jailor.skipVote();

        clearUnreads(host, jailor, mayor);

        mayor.doDayAction();
        assertTrue(mayor.unread.isEmpty());
        assertFalse(jailor.unread.isEmpty());

        clearUnreads(host, jailor, mayor);
        jailor.doDayAction(mayor);
        mayor.skipVote(); // 3rd vote, 5 total players

        assertFalse(host.unread.isEmpty());
        assertEquals(2, jailor.unread.size());
        assertEquals(2, mayor.unread.size()); // for jail but not day chat bc mayor sent the skip vote
    }

    private void assertUnreadCount(UserWrapper w1, String name, int count) {
        if(w1.unread.containsKey(name))
            assertEquals(count, (int) w1.unread.get(name));
        else if(count != 0)
            fail();
    }

    public void testChattingUnreads() throws JSONException, SQLException {
        UserWrapper host = UserFactory.hostPublicGame();
        UserWrapper p2 = host.addPlayer(BasicRoles.Citizen());
        UserWrapper p3 = host.addPlayer(BasicRoles.Goon());
        host.addSetupHidden(BasicRoles.Citizen());
        host.startGame(Game.DAY_START);

        String dayChatKey = DayChat.KEY;
        String dayChatName = "Day 1";

        host.setRead(dayChatName);
        p2.setRead(dayChatName);
        p3.setRead(dayChatName);

        assertUnreadCount(host, dayChatName, 0);
        assertUnreadCount(p2, dayChatName, 0);
        assertUnreadCount(p3, dayChatName, 0);

        host.say("hello", dayChatKey);

        assertUnreadCount(host, dayChatName, 0);
        assertUnreadCount(p2, dayChatName, 1);
        assertUnreadCount(p3, dayChatName, 1);

        host.say("hello2", dayChatKey);

        assertUnreadCount(host, dayChatName, 0);
        assertUnreadCount(p2, dayChatName, 2);
        assertUnreadCount(p3, dayChatName, 2);

        p2.setRead(dayChatName);

        assertUnreadCount(host, dayChatName, 0);
        assertUnreadCount(p2, dayChatName, 0);
        assertUnreadCount(p3, dayChatName, 2);
    }

    public void testNewMessageRecord2() throws JSONException, SQLException {
        UserWrapper host = UserFactory.hostPrivateCustomGame();
        UserWrapper p2 = host.addUser();
        UserWrapper p3 = host.addUser();

        host.addBots(2);

        host.addSetupHidden(BasicRoles.Mayor());
        host.addSetupHidden(BasicRoles.Jailor());
        host.addSetupHidden(BasicRoles.Assassin());
        host.addSetupHidden(BasicRoles.Bulletproof());
        host.addSetupHidden(BasicRoles.Assassin());

        host.prefer(BasicRoles.Assassin());
        p3.prefer(BasicRoles.Mayor());
        p2.prefer(BasicRoles.Jailor());

        host.startGame(Game.NIGHT_START);

        // just has the night chat unread
        assertEquals(1, host.unread.size());

        host.endPhase();

        assertTrue(game.isDay());

        DayChat dc = game.getEventManager().getDayChat();
        String d1 = game.getEventManager().getDayChat().getName();
        clearUnreads(host);
        host.unread.clear();

        p2.say("test 2", dc.getKey(p2.getPlayer()));
        host.setRead(d1);
        host.requestUnreads();

        assertUnreadCount(host, d1, 0);
    }

    public void testInvalidDisplayName() throws JSONException, SQLException {
        UserWrapper nc = new UserWrapper(" ");
        nc.hostGame(true);
        // i can host a game even if the name inputted is garbage.
    }

    public void testAssassinDayAbility() throws JSONException, SQLException {
        UserWrapper host = UserFactory.hostPrivateCustomGame();
        UserWrapper p2 = host.addUser();

        host.addBots(4);

        host.addSetupHidden(BasicRoles.Mayor());
        host.addSetupHidden(BasicRoles.Jailor());
        host.addSetupHidden(BasicRoles.Assassin());
        host.addSetupHidden(BasicRoles.Bulletproof());
        host.addSetupHidden(BasicRoles.Assassin());
        host.addSetupHidden(Hidden.TownProtective());

        host.prefer(BasicRoles.Assassin());
        p2.prefer(BasicRoles.Jailor());

        host.updateGameModifier(GameModifierName.OMNISCIENT_DEAD, true);

        host.startGame(Game.DAY_START);

        assertTrue(host.hasDayAction);
        host.doDayAction(p2);
        assertFalse(host.hasDayAction);
    }

    public void testMayorDayAbility() throws JSONException, SQLException {
        UserWrapper host = UserFactory.hostPrivateCustomGame();
        UserWrapper arsonist = host.addUser();

        host.addBots(3);

        host.addSetupHidden(BasicRoles.Mayor());
        host.addSetupHidden(BasicRoles.Jailor());
        host.addSetupHidden(BasicRoles.Assassin());
        host.addSetupHidden(BasicRoles.Arsonist());
        host.addSetupHidden(BasicRoles.Assassin());

        host.prefer(BasicRoles.Mayor());
        arsonist.prefer(BasicRoles.Arsonist());

        host.startGame(Game.DAY_START);

        assertEquals(0, host.abilities.size()); // votes don't count anymore
        assertEquals(0, arsonist.abilities.size());
    }

    public void testLastWill() throws JSONException, SQLException {
        UserWrapper host = UserFactory.hostPrivateCustomGame();
        host.addSetupHidden(BasicRoles.Goon(), BasicRoles.Citizen(), BasicRoles.Citizen());

        host.updateSetupModifier(SetupModifierName.LAST_WILL, true);

        host.addBots(2);

        host.startGame();

        host.setLastWill("hi");

        assertEquals("hi", host.getPlayer()._lastWill);
    }

    public void testSkipDayEndPhaseOrder() throws JSONException, SQLException {
        UserWrapper host = UserFactory.hostPrivateCustomGame();
        for(int i = 0; i < 3; i++)
            host.addUser();
        host.addSetupHidden(BasicRoles.Witch(), BasicRoles.Citizen(), BasicRoles.Citizen(), BasicRoles.GraveDigger());

        host.startGame(Game.DAY_START);

        endDay();

        host.skipVote();

        List<String> commands = Command.getText(CommandRepo.getByReplayID(host.getLobby().gameID));
        assertTrue(commands.get(commands.size() - 1).contains(CommandHandler.END_PHASE));
    }

    public void testComputerVoting() throws JSONException, SQLException {
        UserWrapper host = UserFactory.hostPrivateCustomGame();

        host.addSetupHidden(BasicRoles.Goon());
        host.addSetupHidden(BasicRoles.Goon());
        for(int i = 0; i < 13; i++)
            host.addSetupHidden(BasicRoles.Citizen());

        Set<String> botNames = host.addBots(14);

        host.startGame(Game.DAY_START);

        host.vote(botNames.iterator().next());

        assertTrue(game.isNight());

        host.endNight();

        if(host.getPlayer().isAlive()){
            host.skipVote();
            assertTrue(game.isNight());
        }
    }

    public void testArchitectPlayerList() throws JSONException, SQLException {
        UserWrapper host = UserFactory.hostGame(PUBLIC, UserFactory.MOD);
        host.addSetupHidden(BasicRoles.Citizen());

        FactionRole mafiaArchitect = LookupUtil
                .findFactionRole(host.getSetup(), BasicRoles.Architect().getName(), Setup.MAFIA_C).get();
        List<UserWrapper> users = new ArrayList<>();
        for(int i = 0; i < 10; i++)
            users.add(host.addPlayer(mafiaArchitect));

        host.prefer(mafiaArchitect);
        host.startGame(DAY);

        List<String> jBuilds = host.abilities.get(Architect.COMMAND);
        assertFalse(jBuilds.isEmpty());

        Player hostPlayer = host.getPlayer();
        Player user1Player = users.get(0).getPlayer();
        Player user2Player = users.get(1).getPlayer();
        host.setNightTarget(Architect.COMMAND, hostPlayer, users.get(0));

        assertFalse(host.submittedActions.isEmpty());

        JSONArray targets = new JSONArray();
        targets.put(host.getName());
        targets.put(users.get(1).getName());

        JSONObject action = new JSONObject();
        action.put(StateObject.command, Architect.COMMAND);
        action.put(StateObject.targets, targets);
        action.put("userID", host.getUserID());
        action.put(StateObject.oldAction, 0);
        action.put("timeLeft", 1);

        host.httpRequest("PUT", "actions", action);

        PlayerList actionTargets = hostPlayer.getActions().getFirst()._targets;
        assertTrue(actionTargets.contains(user2Player));
        assertFalse(actionTargets.contains(user1Player));

        host.endPhase();

        assertFalse(hostPlayer.getRoleCreatedChats(1).isEmpty());
        assertTrue(user1Player.getRoleCreatedChats(1).isEmpty());
        assertFalse(user2Player.getRoleCreatedChats(1).isEmpty());
    }

    public void testDriverDoubleFeedback() throws JSONException, SQLException {
        UserWrapper host = UserFactory.hostGame(PUBLIC, UserFactory.MOD);
        Set<String> botNames = host.addBots(10);
        String botName = botNames.iterator().next();

        host.addSetupHidden(BasicRoles.Witch(), botNames.size());
        host.addSetupHidden(BasicRoles.BusDriver());

        host.prefer(BasicRoles.BusDriver());

        host.startGame(NIGHT);

        host.setNightTargetByName(Driver.abilityType, host.getName(), botName);
        host.endNight();

        // header taken out
        // the selection message
        // the feedback
        partialContains(host.chatLogs.get("Day 1"), Driver.FEEDBACK);
        assertEquals(1, host.chatLogs.get("Night 0").size()); // was 2, but selection message is out now
    }

    public void testGhostAcceptableTargets() throws JSONException, SQLException {
        UserWrapper host = UserFactory.hostGame(PUBLIC, UserFactory.MOD);
        UserWrapper fodder = host.addUser();
        Set<String> botNames = host.addBots(10);
        String botName = botNames.iterator().next();

        host.addSetupHidden(BasicRoles.Ghost());
        host.addSetupHidden(BasicRoles.SerialKiller());

        host.addSetupHidden(BasicRoles.Citizen(), botNames.size());

        fodder.prefer(BasicRoles.SerialKiller());

        host.prefer(BasicRoles.Ghost());

        host.startGame(NIGHT);

        assertIsNight();

        fodder.setNightTarget(SerialKiller.COMMAND, host);
        fodder.endNight();
        host.endNight();

        fodder.skipVote();

        assertTrue(game.isNight());
        assertTrue(host.abilities.containsKey(Ventriloquist.abilityType.command));

        fodder.endNight();

        assertTrue(game.isNight());
        host.setNightTargetByName(Ventriloquist.abilityType, botName);
        host.endNight();

        ActionSubmitRequest request = new ActionSubmitRequest(Puppet.abilityType, Vote.COMMAND,
                CollectionUtil.toArrayList(botName, fodder.getName()), host.getUserID());
        host.setTarget(request);
        host.getLobby().cancelBrain();
    }

    public void testSnitchDying() throws JSONException, SQLException {
        UserWrapper host = UserFactory.hostGame(PRIVATE, UserFactory.MOD);

        Set<String> botNames = host.addBots(15);
        host.addSetupHidden(BasicRoles.SerialKiller());
        host.addSetupHidden(BasicRoles.Snitch(), 15);

        host.prefer(BasicRoles.SerialKiller());

        host.startGame(Game.NIGHT_START);

        host.setNightTargetByName(SerialKiller.abilityType, botNames.iterator().next());
        host.endNight();
    }

    public void testDisguiserLeaving() throws JSONException, SQLException {
        UserWrapper disg = UserFactory.hostGame(PRIVATE, UserFactory.MOD);
        disg.addSetupHidden(BasicRoles.Disguiser());

        disg.updateGameModifier(GameModifierName.CHAT_ROLES, true);

        FactionRole yakAgent = LookupUtil.findFactionRole(BasicRoles.setup, Agent.class.getSimpleName(), Setup.YAKUZA_C)
                .get();
        FactionRole yakFramer = LookupUtil
                .findFactionRole(BasicRoles.setup, Framer.class.getSimpleName(), Setup.YAKUZA_C).get();
        UserWrapper doc = disg.addPlayer(BasicRoles.Doctor());
        UserWrapper yak1 = disg.addPlayer(yakAgent);
        UserWrapper yak2 = disg.addPlayer(yakFramer);

        disg.startGame(Game.NIGHT_START);

        disg.setNightTarget(Disguiser.COMMAND, yak1);
        disg.endNight();
        doc.endNight();
        yak1.endNight();
        yak2.endNight();

        disg.skipVote();
        doc.skipVote();

        disg.setNightTarget(FactionSend.COMMAND, disg, yak2);
        disg.setNightTarget(FactionKill.COMMAND, yak2);
        doc.endNight();
        disg.endNight();
        yak2.endNight();

        isDead(yak2.getPlayer());
    }

    public void testDeadArsonActionText() throws JSONException, SQLException {
        UserWrapper host = UserFactory.hostGame(PRIVATE, UserFactory.MOD);
        host.addSetupHidden(BasicRoles.Arsonist(), 4);
        host.addSetupHidden(BasicRoles.GraveDigger());
        host.addSetupHidden(BasicRoles.Citizen());
        UserWrapper arson = host.addPlayer(BasicRoles.Arsonist());

        host.addBots(5);
        host.prefer(BasicRoles.GraveDigger());

        host.startGame(Game.DAY_START);

        Game narrator = host.getPlayer().game;

        host.vote(arson);

        assertTrue(narrator.isNight());

        host.setNightTarget(GraveDigger.COMMAND, Douse.COMMAND, arson, host);
        String commandText = host.submittedActionsJSON.getJSONArray(StateObject.actionList).getJSONObject(0)
                .getString("text");
        assertTrue(commandText.contains(arson.getName()));
    }

    public void testLynchNightDeathLog() throws JSONException {
        Controller p1 = addPlayer(BasicRoles.Citizen());
        Controller p2 = addPlayer(BasicRoles.Citizen());
        Controller p3 = addPlayer(BasicRoles.Goon());

        dayStart();
        p1.doDayAction(new Action(p1.getPlayer(), Vote.abilityType, p2.getPlayer()));
        p3.doDayAction(new Action(p3.getPlayer(), Vote.abilityType, p2.getPlayer()));

        Message e = game.getEventManager().getDayChat().getEvents().getLast();

        JSONArray chatLog = new JSONArray();
        Lobby.AppendMessage(chatLog, e, p1.getPlayer(), p1.getPlayer().getID(), Optional.empty());

        JSONObject jo = chatLog.getJSONObject(0);
        JSONArray chat = jo.getJSONArray("chat");

        String night1 = game.getEventManager().getNightLog(VoidChat.KEY).getName();
        for(int i = 0; i < chat.length(); i++){
            if(chat.getString(i).equals(night1))
                return;
        }
        fail();
    }

    // this is a copy
    public void testJailorFeedbackTest() throws JSONException, SQLException {
        UserWrapper baker = UserFactory.hostGame(PRIVATE, UserFactory.MOD);
        baker.addSetupHidden(BasicRoles.Baker());
        Setup setup = baker.getSetup();

        assertEquals(1, setup.rolesList._setupHiddenList.size());
        Optional<FactionRole> factionRole = LookupUtil.findFactionRole(setup, BasicRoles.Kidnapper().getName(),
                Setup.MAFIA_C);
        assertPresent(factionRole);

        UserWrapper j1 = baker.addPlayer(BasicRoles.Kidnapper());
        UserWrapper j2 = baker.addPlayer(BasicRoles.Kidnapper());
        baker.addPlayer(BasicRoles.Kidnapper());

        setup = baker.getSetup();
        assertEquals(4, PlayerRepo.getByGameID(baker.getGameID()).size());
        assertEquals(4, setup.rolesList._setupHiddenList.size());

        baker.prefer(BasicRoles.Baker());
        baker.updateGameModifier(GameModifierName.CHAT_ROLES, true);

        baker.startGame(Game.DAY_START);

        j1.doDayAction(j2);
        j1.skipVote();
        baker.endPhase();

        baker.setNightTarget(BreadAbility.COMMAND, j2);
        baker.endPhase();
        baker.endPhase();
        baker.endPhase();

        j1.doDayAction(j2);
        j1.skipVote();
        baker.endPhase();

        j1.setNightTarget(JailExecute.COMMAND, j2);
        baker.endPhase();
        assertEquals(3, game.getLiveSize());
    }

    private void jail(UserWrapper p1, UserWrapper p2) throws SQLException {
        p1.doDayAction(p2);
    }

    public void testMasonSeesAllies() throws JSONException, SQLException {
        UserWrapper masonLeader = UserFactory.hostGame(PRIVATE, UserFactory.MOD);
        masonLeader.addSetupHidden(BasicRoles.MasonLeader());

        UserWrapper framer = masonLeader.addPlayer(BasicRoles.Framer());
        UserWrapper clubber = masonLeader.addPlayer(BasicRoles.Clubber());
        UserWrapper disg = masonLeader.addPlayer(BasicRoles.Disguiser());
        masonLeader.upsertFactionRoleModifier(BasicRoles.Disguiser(), RoleModifierName.UNDETECTABLE, true);

        masonLeader.updateGameModifier(GameModifierName.CHAT_ROLES, true);

        masonLeader.startGame(Game.NIGHT_START);

//        assertNotNull(host.user.player.role.getID());
        isRole(masonLeader, BasicRoles.MasonLeader());
        isRole(framer, BasicRoles.Framer());
        isRole(clubber, BasicRoles.Clubber());
        isRole(disg, BasicRoles.Disguiser());

        disg.setNightTarget(Disguiser.COMMAND, masonLeader);

        masonLeader.endNight();
        disg.endNight();
        clubber.endNight();
        framer.endNight();

        disg.skipVote();
        clubber.skipVote();

        assertEquals(1, ProfileAllyJson.getSet(clubber.getPlayer()).size());

        long townFactionID = masonLeader.getGame().setup.getFactionByColor(Setup.TOWN_C).id;
        long mafiaFactionID = masonLeader.getGame().setup.getFactionByColor(Setup.MAFIA_C).id;

        ProfileAllyJson ally = ProfileAllyJson.getSet(framer.getPlayer()).iterator().next();
        assertTrue(ally.modifiers.undetectable.get());
        assertEquals(masonLeader.getPlayer().getID(), ally.name);
        assertEquals(BasicRoles.Disguiser().getName(), ally.roleName);
        assertEquals(mafiaFactionID, ally.factionID);
        assertEquals(disg.getPlayer().gameRole.factionRole.id, ally.factionRoleID);

        ally = ProfileAllyJson.getSet(clubber.getPlayer()).iterator().next();
        assertEquals(masonLeader.getPlayer().gameRole.factionRole.id, ally.factionRoleID);
        assertFalse(ally.modifiers.undetectable.isPresent());
        assertEquals(masonLeader.getPlayer().getID(), ally.name);
        assertEquals(BasicRoles.MasonLeader().getName(), ally.roleName);
        assertEquals(townFactionID, ally.factionID);
    }

    public void testRemoveFromNightChats() throws JSONException, SQLException {
        UserWrapper host = UserFactory.hostGame(PRIVATE, UserFactory.MOD);
        host.addSetupHidden(BasicRoles.Citizen());

        UserWrapper fodder = host;
        fodder.prefer(BasicRoles.Citizen());

        UserWrapper jailor = host.addPlayer(BasicRoles.Jailor());
        UserWrapper gf = host.addPlayer(BasicRoles.Godfather());
        UserWrapper agent = host.addPlayer(BasicRoles.Agent());
        host.addPlayer(BasicRoles.Agent());

        host.updateGameModifier(GameModifierName.CHAT_ROLES, true);

        host.startGame(Game.DAY_START);

        jail(jailor, gf);
        host.endPhase();

        agent.say(gibberish(), agent.getPlayer().getColor());
        agent.setNightTarget(FactionKill.COMMAND, fodder);
        host.endPhase();

        jail(jailor, gf);
        host.endPhase();

        host.endPhase();
        host.endPhase();

        host.endPhase();

        jail(jailor, gf);
        host.endPhase();
    }

    public void testNeutralKillersImmune() throws JSONException, SQLException {
        UserWrapper host = UserFactory.hostPublicGame();
        for(Role role: host.getSetup().roles){
            if(role.hasAbility(SerialKiller.abilityType))
                if(role.hasAbility(Bulletproof.abilityType))
                    return;
        }

        fail("Serial Killer was not invulnerable");
    }
}
