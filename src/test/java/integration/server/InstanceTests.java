package integration.server;

import java.sql.SQLException;

import game.logic.Player;
import integration.logic.SuperTest;
import models.FactionRole;
import nnode.LobbyManager;
import repositories.BaseRepo;
import repositories.Connection;
import repositories.SetupRepo;
import repositories.UserRepo;
import util.UserFactory;

public class InstanceTests extends SuperTest {

    protected static final boolean PRIVATE = UserFactory.PRIVATE, PUBLIC = UserFactory.PUBLIC;

    public InstanceTests(String name) {
        super(name);
    }

    @Override
    public void setUp() throws Exception {
        BaseRepo.connection = new Connection();
        wipeTables();
        SwitchWrapper.init();
        super.setUp();
    }

    public static void wipeTables() throws SQLException {
        SetupRepo.deleteAll();
        UserRepo.deleteAll();
        LobbyManager.reloadInstances();
    }

    @Override
    public void tearDown() throws Exception {
        SwitchWrapper.cleanUp();
        Thread.sleep(1050);
        BaseRepo.connection.close();
        super.tearDown();
    }

    protected void reloadInstances() throws SQLException {
        reloadInstances(true);
    }

    protected void reloadInstances(boolean killCurrent) throws SQLException {
        if(killCurrent)
            SwitchWrapper.killDBConnections();
        SwitchWrapper.phoneBook.clear();
        LobbyManager.reloadInstances();
    }

    public void isRole(UserWrapper w, FactionRole factionRole) throws SQLException {
        Player p = w.getPlayer();
        assertEquals(factionRole.id, p.gameRole.factionRole.id);
    }

    public void cancelBrain(UserWrapper userWrapper) throws SQLException {
        userWrapper.getLobby().cancelBrain();
    }

    public void testMock() {
        // need this so build doesn't hiccup. It expects there to be tests in this file.
    }
}
