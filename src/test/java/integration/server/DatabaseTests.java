package integration.server;

import java.sql.SQLException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.Set;

import game.abilities.Citizen;
import game.abilities.CultLeader;
import game.abilities.FactionKill;
import game.abilities.Gunsmith;
import game.abilities.Hidden;
import game.abilities.Infiltrator;
import game.abilities.Sleepwalker;
import game.abilities.Vigilante;
import game.abilities.support.Gun;
import game.logic.Game;
import game.logic.Player;
import game.logic.exceptions.NarratorException;
import game.logic.support.Constants;
import game.logic.templates.BasicRoles;
import game.setups.Setup;
import integration.logic.SuperTest;
import json.JSONException;
import models.Ability;
import models.Faction;
import models.FactionRole;
import models.Role;
import models.enums.AbilityModifierName;
import models.enums.AbilityType;
import models.enums.GameModifierName;
import models.enums.VoteSystemTypes;
import models.idtypes.GameID;
import models.json_output.profile.ProfileAllyJson;
import models.modifiers.AbilityModifier;
import models.schemas.PlayerSchema;
import nnode.LobbyManager;
import nnode.StateObject;
import repositories.GameRepo;
import repositories.GameUserRepo;
import services.FactionAbilityModifierService;
import services.FactionAbilityService;
import services.FactionRoleService;
import services.ModeratorService;
import services.PlayerService;
import services.RoleAbilityModifierService;
import services.RoleService;
import util.UserFactory;
import util.models.PlayerTestUtil;

public class DatabaseTests extends InstanceTests {

    public DatabaseTests(String name) {
        super(name);
    }

    public void testReloadableInstancesMoreThanTwice() throws JSONException, SQLException {
        LobbyManager.reloadInstances();
        UserWrapper host = UserFactory.hostPublicGame();
        int prevLength = LobbyManager.idToLobby.size();

        UserWrapper p1 = host.addUser();
        UserWrapper p2 = host.addUser();

        host.addSetupHidden(BasicRoles.Goon());
        host.addSetupHidden(BasicRoles.Goon());
        host.addSetupHidden(BasicRoles.SerialKiller());

        p1.prefer(BasicRoles.Goon());
        p2.prefer(BasicRoles.Goon());

        host.startGame(Game.NIGHT_START);

        reloadInstances();

        assertEquals(prevLength + 1, LobbyManager.idToLobby.size());

        reloadInstances();

        assertEquals(prevLength + 1, LobbyManager.idToLobby.size());
        SuperTest.BrainEndGame = false;
    }

    public void testLeavePostGameEnding() throws JSONException, SQLException {
        UserWrapper host = UserFactory.hostPublicGame();
        ArrayList<UserWrapper> others = new ArrayList<>();
        int otherCount = 2;
        for(int i = 0; i < otherCount; i++)
            others.add(host.addUser());
        host.addSetupHidden(BasicRoles.Citizen());
        host.addSetupHidden(BasicRoles.Goon());
        host.addSetupHidden(BasicRoles.Cultist());

        host.updateGameModifier(GameModifierName.VOTE_SYSTEM, VoteSystemTypes.PLURALITY.value);
        host.startGame(Game.DAY_START);

        host.vote(others.get(0));
        others.get(1).vote(others.get(0));
        host.endPhase();

        assertTrue(host.getLobby().game.isFinished());
        host.leaveGame();
        assertFalse(GameUserRepo.getActiveByUserID(host.getUserID()).isPresent());
    }

    public void testLeaveGame() throws JSONException, SQLException {
        UserWrapper host = UserFactory.hostPublicGame();
        ArrayList<UserWrapper> others = new ArrayList<>();
        int otherCount = 2;
        for(int i = 0; i < otherCount; i++)
            others.add(host.addUser());
        host.addSetupHidden(BasicRoles.Citizen(), BasicRoles.Goon(), BasicRoles.Cultist());

        host.startGame(Game.DAY_START);
        assertTrue(game.isStarted());

        try{
            host.leaveGame();
            fail();
        }catch(NarratorException e){
            assertEquals("You cannot leave if you are alive.", e.getMessage());
        }

        for(UserWrapper o: others){
            if(!game.isDay())
                break;
            o.vote(host);
        }

        boolean isExited = !GameUserRepo.getActiveByUserID(host.getUserID()).isPresent();

        assertFalse(isExited);

        host.leaveGame();

        isExited = !GameUserRepo.getActiveByUserID(host.getUserID()).isPresent();
        assertTrue(isExited);
    }

    public void testBotGameLeaving() throws JSONException, SQLException {
        LobbyManager.idToLobby.clear();
        UserWrapper host = UserFactory.hostPrivateCustomGame();
        host.addBots(13);
        UserWrapper user = host.addUser();

        host.addSetupHidden(BasicRoles.Goon(), 1);
        host.addSetupHidden(BasicRoles.Vigilante(), 1);

        host.prefer(BasicRoles.Goon());
        user.prefer(BasicRoles.Vigilante());

        // i want to make sure that prefers persist
        host.addSetupHidden(BasicRoles.Citizen(), 13);

        host.startGame(Game.NIGHT_START);
        GameID gameID = host.getGameID();

        isRole(user, BasicRoles.Vigilante());
        assertNotSame(host.getPlayer().getGameFaction().getColor(), user.getPlayer().getGameFaction().getColor());

        user.setNightTarget(Gun.COMMAND, host);
        host.setNightTarget(FactionKill.COMMAND, user);
        host.endPhase();

        isDead(host, user);
        assertGameOver();

        host.leaveGame();
        user.leaveGame();

        assertTrue(GameRepo.getAllIDs().contains(gameID));

        reloadInstances();

        // game is gone, not loading it.
        assertEquals(0, LobbyManager.idToLobby.size());
    }

    public void testModeratorPersistsAfterGameStart() throws JSONException, SQLException {
        UserWrapper host = UserFactory.hostPublicGame();
        host.addPlayer(BasicRoles.Goon());
        host.addPlayer(BasicRoles.SerialKiller());

        host.addSetupHidden(Hidden.NeutralKillingRandom());

        host.startGame();

        ModeratorService.assertIsGameModerator(host.getGameID(), host.getUserID(), "Host changed but should not have!");
    }

    public void testNewModifiers() throws JSONException, SQLException {
        UserWrapper host = UserFactory.hostPrivateCustomGame();
        UserWrapper p1 = host.addPlayer(BasicRoles.Goon());
        UserWrapper p2 = host.addPlayer(BasicRoles.SerialKiller());

        host.addSetupHidden(Hidden.NeutralKillingRandom());

        host.startGame(Game.NIGHT_START);

        assertTrue(p2.getPlayer().isInvulnerable());

        p1.setNightTarget(FactionKill.COMMAND, host);
        p1.endNight();
        p2.endNight();
        host.endNight();

        isAlive(host.getPlayer());

        host.skipVote();
        p2.skipVote();

        p1.setNightTarget(FactionKill.COMMAND, p2);
        p1.endNight();
        p2.endNight();
        host.endNight();

        isAlive(p2.getPlayer());

        reloadInstances();

        Game game = host.getGame();
        assertTrue(game.isStarted());
        Player host2Player = PlayerService.getByUserID(host.getUserID()).get();
        assertTrue(host2Player.isAlive());

        game = host.getGame();
        assertTrue(game.isStarted());
        Player p22Player = PlayerService.getByUserID(p2.getUserID()).get();
        assertTrue(p22Player.isAlive());
    }

    public void testNewTeamModifiers() throws JSONException, SQLException {
        UserWrapper host = UserFactory.hostPrivateCustomGame();
        UserWrapper gunsmithGoon = host.addPlayer(BasicRoles.Goon());
        UserWrapper serialKiller = host.addPlayer(BasicRoles.SerialKiller());

        host.addSetupHidden(BasicRoles.Citizen());
        Faction mafiaFaction = host.getSetup().getFactionByColor(BasicRoles.Goon().getColor());
        long factionAbilityID = FactionAbilityService.createFactionAbility(host.id, mafiaFaction.id,
                Gunsmith.abilityType).ability.id;
        FactionAbilityModifierService.upsertModifier(host.id, factionAbilityID, new AbilityModifier(
                AbilityModifierName.GS_FAULTY_GUNS, true, 0, Setup.MAX_PLAYER_COUNT, Instant.now()));

        host.startGame(Game.NIGHT_START);

        assertTrue(serialKiller.getPlayer().isInvulnerable());

        gunsmithGoon.setNightTarget(Gunsmith.COMMAND, Gunsmith.FAULTY, serialKiller);
        gunsmithGoon.endNight();
        serialKiller.endNight();
        host.endNight();

        assertEquals(1, serialKiller.getPlayer().getAbility(Vigilante.class).getRealCharges());

        reloadInstances();
    }

    public void testInternalNameLoading() throws JSONException, SQLException {
        UserWrapper host = UserFactory.hostPublicGame();
        ArrayList<UserWrapper> clients = new ArrayList<>();
        clients.add(host);
        for(int i = 0; i < 3; i++)
            clients.add(host.addUser());

        host.addSetupHidden(BasicRoles.Sleepwalker());
        host.addSetupHidden(BasicRoles.Citizen(), 2);
        host.addSetupHidden(BasicRoles.Agent());

        host.startGame();

        PlayerSchema player = PlayerTestUtil.getSchemaByRoleName(host.getLobby(), Sleepwalker.class.getSimpleName());
        long sleepwalkerID = player.userID.get();
        assertNotNull(sleepwalkerID);

        boolean assertionFound = false;
        clients.add(host);
        for(UserWrapper client: clients){
            if(client.id == sleepwalkerID){
                assertEquals(Citizen.class.getSimpleName(), client.role.getString(StateObject.roleName));
                assertTrue(client.getPlayer().is(Sleepwalker.abilityType));
                assertionFound = true;
            }
        }
        assertTrue(assertionFound);

        reloadInstances();

        Player sleepwalkerPlayer = PlayerService.getByUserID(sleepwalkerID).get();
        assertTrue(sleepwalkerPlayer.is(Sleepwalker.abilityType));
        FactionRole factionRole = sleepwalkerPlayer.gameRole.factionRole;
        assertEquals(Citizen.class.getSimpleName(), factionRole.receivedRole.getName());
        skipTearDown = true;
    }

    private FactionRole createInfiltrator(UserWrapper user) throws SQLException {
        LinkedHashSet<AbilityType> abilityTypes = new LinkedHashSet<>();
        abilityTypes.add(Infiltrator.abilityType);
        long hostID = user.getUserID();
        Role role = RoleService.createRole(hostID, user.getSetupID(), "Infiltrator", abilityTypes).role;
        return FactionRoleService.createFactionRole(hostID, BasicRoles.setup.getFactionByColor(Setup.MAFIA_C).id,
                role.id).factionRole;
    }

    public void testCultVis() throws JSONException, SQLException {
        UserWrapper host = UserFactory.hostPublicGame();
        ArrayList<UserWrapper> clients = new ArrayList<>();
        clients.add(host);
        for(int i = 0; i < 2; i++)
            clients.add(host.addUser());

        FactionRole infiltrator = createInfiltrator(host);
        removeCultLeaderCooldown(host);

        host.addSetupHidden(infiltrator);
        host.addSetupHidden(BasicRoles.CultLeader());
        host.addSetupHidden(BasicRoles.Citizen());
        host.startGame();

        UserWrapper infil = null, rebel = null, villager = null;
        for(UserWrapper nc: clients){
            if(nc.getPlayer() == null)
                continue;
            if(Citizen.hasNoAbilities(nc.getPlayer()))
                villager = nc;
            if(nc.getPlayer().is(Infiltrator.abilityType))
                infil = nc;
            if(nc.getPlayer().is(CultLeader.abilityType))
                rebel = nc;
        }

        rebel.setNightTarget(CultLeader.COMMAND, infil);
        game.forceEndNight(Constants.NO_TIME_LEFT);

        assertIsDay();

        rebel.skipVote();
        endDay();

        assertIsNight();

        rebel.setNightTarget(CultLeader.COMMAND, villager);
        game.forceEndNight(Constants.NO_TIME_LEFT);

        assertIsDay();

        Set<ProfileAllyJson> allies = ProfileAllyJson.getSet(rebel.getPlayer());
        for(ProfileAllyJson ally: allies){
            if(BasicRoles.Cultist().id == ally.factionRoleID)
                return;
        }
        fail("Didn't find the right ally role name");
    }

    private void removeCultLeaderCooldown(UserWrapper host) throws SQLException {
        Role role = BasicRoles.CultLeader().role;
        long roleID = role.id;
        Ability ability = role.getAbility(CultLeader.abilityType);
        AbilityModifier modifier = new AbilityModifier(AbilityModifierName.COOLDOWN, 0, 0, Setup.MAX_PLAYER_COUNT,
                Instant.now());
        RoleAbilityModifierService.upsertModifier(host.id, roleID, ability.id, modifier);
    }
}
