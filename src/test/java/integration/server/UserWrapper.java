package integration.server;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import game.abilities.Hidden;
import game.abilities.Vote;
import game.abilities.util.AbilityUtil;
import game.ai.Controller;
import game.ai.ControllerList;
import game.event.ChatMessage;
import game.logic.Game;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.exceptions.PhaseException;
import game.logic.support.CommandHandler;
import game.logic.support.Constants;
import game.logic.support.action.Action;
import game.logic.templates.BasicRoles;
import game.logic.templates.TextController;
import game.setups.Setup;
import integration.logic.SuperTest;
import json.JSONArray;
import json.JSONException;
import json.JSONObject;
import models.FactionRole;
import models.Role;
import models.SetupHidden;
import models.controllerResponse.BotAddResponseJson;
import models.enums.AbilityModifierName;
import models.enums.AbilityType;
import models.enums.GameModifierName;
import models.enums.PreferType;
import models.enums.RoleModifierName;
import models.enums.SetupModifierName;
import models.idtypes.GameID;
import models.json_output.GameJson;
import models.requests.ActionSubmitRequest;
import models.schemas.PlayerSchema;
import nnode.Lobby;
import nnode.StateObject;
import repositories.GameRepo;
import repositories.GameUserRepo;
import repositories.PlayerRepo;
import repositories.UserRepo;
import services.ChatService;
import services.GameService;
import services.LobbyService;
import services.ModeratorService;
import services.SetupService;
import services.UserService;
import util.CollectionUtil;
import util.PlayerUtil;
import util.TestUtil;
import util.UserFactory;
import util.game.HiddenUtil;
import util.game.LookupUtil;
import util.game.VoteUtil;
import util.models.RoleTestUtil;
import util.models.SetupHiddenTestUtil;

public class UserWrapper extends TextController {

    String lobbyID;
    GameID gameID;
    private Player player;

    String name;

    ArrayList<Action> submittedActions;
    JSONObject submittedActionsJSON, role, setup;
    HashMap<String, List<String>> abilities;
    HashMap<String, ArrayList<String>> chatLogs;
    HashMap<String, String> keys;
    boolean hasDayAction;
    public boolean hasWarning;
    public long id;

    public HashMap<String, Integer> unread;

    public UserWrapper(String name) throws SQLException {
        super();
        super.texter = new NodeTextController(this);
        this.name = name;

        this.id = createUserID();
        SwitchWrapper.phoneBook.put(id, this);

        abilities = new HashMap<>();
        chatLogs = new HashMap<>();
        keys = new HashMap<>();
        unread = new HashMap<>();
        submittedActions = new ArrayList<>();
        hasWarning = false;
    }

    private static long createUserID() throws SQLException {
        return UserRepo.create("userName");
    }

    // TOOD rename to add user
    public UserWrapper addUser() throws JSONException, SQLException {
        return UserFactory.joinPrivateGame(this.lobbyID);
    }

    // TOOD rename to add user
    public UserWrapper addUser(String name) throws JSONException, SQLException {
        return UserFactory.joinPrivateGame(name, this.lobbyID);
    }

    public UserWrapper addPlayer(FactionRole input) throws JSONException, SQLException {
        Setup setup = input.faction.setup;
        Optional<Hidden> hiddenOpt = HiddenUtil.getHiddenSingle(setup, input);
        Hidden hidden;
        if(hiddenOpt.isPresent())
            hidden = hiddenOpt.get();
        else{
            createHidden(input);
            hidden = HiddenUtil.getHiddenSingle(BasicRoles.setup, input).get();
        }
        this.addSetupHidden(hidden);
        return UserFactory.joinPrivateGame(this.gameID).prefer(input);
    }

    private static class NodeTextController implements TextInput {

        UserWrapper nc;

        public NodeTextController(UserWrapper nc) {
            this.nc = nc;
        }

        @Override
        public void text(String p, String message, double timeLeft) {
            JSONObject jo = new JSONObject();
            try{
                jo.put(StateObject.message, message);
                jo.put("timeLeft", timeLeft);
                nc.push(jo);
            }catch(JSONException e){
                e.printStackTrace();
            }
        }

    }

    public Game getGame() throws SQLException {
        GameID gameID = GameUserRepo.getActiveGameIDByUserID(getUserID()).get();
        return GameService.getByGameID(gameID);
    }

    public long getSetupID() throws SQLException {
        return GameRepo.getOverview(this.gameID).setupID;
    }

    public Setup getSetup() throws SQLException {
        return SetupService.getSetup(this.getSetupID());
    }

    public Lobby getLobby() throws SQLException {
        return LobbyService.getStartedByUserID(getUserID()).get();
    }

    @Override
    public String getName() {
        if(GameService.gamesMap.containsKey(this.gameID))
            return this.getPlayer().getName();

        try{
            PlayerSchema player = PlayerRepo.getByGameIDAndUserID(this.gameID, this.id);
            return player.name;
        }catch(SQLException e){

        }
        return this.name;
    }

    @Override
    public String toString() {
        return this.getName();
    }

    public long getUserID() {
        return id;
    }

    public JSONObject httpRequest(String method, String url, JSONObject jo) throws JSONException {
        JSONObject request = new JSONObject();
        request.put("method", method);
        request.put("url", url);
        request.put("body", jo);
        request.put("httpRequest", true);
        request.put("requestID", Long.toString(this.id));
        return SwitchWrapper.send(request);
    }

    void push(JSONObject jo) throws JSONException {
        jo.put(StateObject.userID, id);
        SwitchWrapper.send(jo);
    }

    public void leaveGame() throws JSONException {
        JSONObject body = new JSONObject();
        body.put("kickedUserID", this.id);
        body.put("kickerID", this.id);
        JSONObject response = httpRequest("DELETE", "players", body);
        TestUtil.assertNoErrors(response);
    }

    public JSONObject createSetup() throws JSONException {
        JSONObject setupRequest = new JSONObject();
        setupRequest.put("name", "mySetup" + this.id);
        JSONObject body;
        body = new JSONObject();
        body.put("userID", this.id);
        body.put("setup", setupRequest);
        return httpRequest("POST", "setups", body);
    }

    public void hostGame(boolean publicHosting) throws JSONException, SQLException {
        JSONObject setupResponse = TestUtil.assertNoErrors(createSetup());
        long setupID = setupResponse.getJSONObject("response").getLong("id");

        JSONObject rules = new JSONObject();

        JSONObject body = new JSONObject();
        body.put("rules", rules);
        body.put("userID", this.id);
        body.put("setupID", setupID);
        body.put("isPublic", publicHosting);
        body.put("hostName", name);

        JSONObject response = httpRequest("POST", "games", body);
        TestUtil.assertNoErrors(response);
        this.lobbyID = GameJson.getLobbyID(response.getJSONObject("response"));

        BasicRoles.setup = SetupService.getSetup(setupID);

        requestGameState();
        GameID gameID = getGameIDFromResponse(response).get();
        this.gameID = gameID;
    }

    public void hostGame(boolean publicHosting, String key) throws JSONException {
        JSONObject rules = new JSONObject();

        JSONObject body = new JSONObject();
        body.put("rules", rules);
        body.put("userID", this.id);
        body.put("setupKey", key);
        body.put("isPublic", publicHosting);
        body.put("hostName", name);

        JSONObject response = httpRequest("POST", "games", body);
        TestUtil.assertNoErrors(response);

        requestGameState();
    }

    public void joinGame(GameID gameID) throws JSONException, SQLException {
        JSONObject body = new JSONObject();
        body.put("gameID", gameID);
        body.put("playerName", this.name);
        body.put("userID", this.id);
        JSONObject response = httpRequest("POST", "players", body);
        TestUtil.assertNoErrors(response);

        this.gameID = gameID;
        this.lobbyID = GameJson.getLobbyID(response.getJSONObject("response"));
        requestGameState();
    }

    public void joinGame(String lobbyID) throws JSONException, SQLException {
        JSONObject body = new JSONObject();
        body.put("joinID", lobbyID);
        body.put("playerName", this.name);
        body.put("userID", this.id);
        JSONObject response = httpRequest("POST", "players", body);

        requestGameState();
        Optional<GameID> gameID = getGameIDFromResponse(response);
        if(!gameID.isPresent())
            return;
        this.gameID = gameID.get();
        this.lobbyID = GameJson.getLobbyID(response.getJSONObject("response"));
    }

    public void joinGame() throws JSONException, SQLException {
        JSONObject body = new JSONObject();
        body.put("playerName", this.name);
        body.put("userID", this.id);
        JSONObject response = httpRequest("POST", "players", body);
        TestUtil.assertNoErrors(response);

        requestGameState();
        this.lobbyID = GameJson.getLobbyID(response.getJSONObject("response"));
        this.gameID = getGameIDFromResponse(response).get();
    }

    private static Optional<GameID> getGameIDFromResponse(JSONObject response) throws JSONException {
        JSONObject gameResponse = response.getJSONObject("response");
        if(gameResponse.has("id"))
            return Optional.of(new GameID(gameResponse.getLong("id")));
        return Optional.empty();
    }

    public JSONObject createHidden(FactionRole factionRole) throws JSONException, SQLException {
        JSONObject response = createHidden(factionRole.getName());
        TestUtil.assertNoErrors(response);
        long hiddenID = response.getJSONObject("response").getLong("id");
        return addHiddenSpawn(hiddenID, factionRole.id);
    }

    public JSONObject addHiddenSpawn(long hiddenID, long... factionRoleIDs) throws JSONException, SQLException {
        JSONObject args = new JSONObject();
        args.put("userID", getUserID());

        JSONArray spawns = new JSONArray();
        JSONObject spawn;
        for(long factionRoleID: factionRoleIDs){
            spawn = new JSONObject();
            spawn.put("hiddenID", hiddenID);
            spawn.put("factionRoleID", factionRoleID);
            spawn.put("minPlayerCount", 0);
            spawn.put("maxPlayerCount", Setup.MAX_PLAYER_COUNT);
            spawns.put(spawn);
        }

        args.put("spawns", spawns);
        JSONObject response = httpRequest("POST", "hiddenSpawns", args);
        TestUtil.assertNoErrors(response);
        BasicRoles.setup = getSetup();
        return response;
    }

    public JSONObject createHidden(String name) throws JSONException, SQLException {
        JSONObject args = new JSONObject();
        args.put("userID", getUserID());
        args.put("setupID", this.getSetupID());
        args.put("name", name);
        return httpRequest("POST", "hiddens", args);

    }

    public void addSetupHidden(FactionRole factionRole, int count) throws JSONException, SQLException {
        Setup setup = getSetup();
        Optional<Hidden> hiddenOpt = LookupUtil.findHidden(setup, factionRole.role.getName());
        if(!hiddenOpt.isPresent()){
            createHidden(factionRole);
            setup = getSetup();
        }
        Hidden hidden = LookupUtil.findHidden(setup, factionRole.role.getName()).get();
        for(int i = 0; i < count; i++)
            addSetupHidden(hidden);
    }

    public void addSetupHidden(FactionRole... factionRoles) throws JSONException, SQLException {
        for(FactionRole factionRole: factionRoles)
            addSetupHidden(factionRole);
    }

    public JSONObject addSetupHidden(FactionRole factionRole) throws JSONException, SQLException {
        Setup setup = factionRole.faction.setup;
        Hidden hidden = LookupUtil.findHidden(setup, factionRole.role.getName(), factionRole.faction.color);
        if(hidden == null){
            JSONObject response = createHidden(factionRole);
            TestUtil.assertNoErrors(response);
            setup = BasicRoles.setup = SetupService.getSetup(setup.id);
            return addSetupHidden(setup.getFactionRole(factionRole.id));
        }
        return addSetupHidden(hidden.id);
    }

    public void addSetupHidden(Hidden hidden, int count) throws JSONException, SQLException {
        for(int j = 0; j < count; j++)
            TestUtil.assertNoErrors(addSetupHidden(hidden));
    }

    public JSONObject addSetupHidden(Hidden hidden) throws JSONException, SQLException {
        return addSetupHidden(hidden.id);
    }

    public void setExposed(long setupHiddenID) throws JSONException {
        JSONObject args = new JSONObject();
        args.put("userID", getUserID());
        args.put("setupHiddenID", setupHiddenID);
        args.put("isExposed", true);
        httpRequest("PUT", "setupHiddens", args);
    }

    public JSONObject addSetupHidden(long hiddenID) throws JSONException, SQLException {
        JSONObject args = new JSONObject();
        args.put("userID", getUserID());
        args.put("hiddenID", hiddenID);
        args.put("isExposed", false);
        args.put("mustSpawn", false);
        args.put("minPlayerCount", 0);
        args.put("maxPlayerCount", Setup.MAX_PLAYER_COUNT);
        JSONObject response = httpRequest("POST", "setupHiddens", args);
        BasicRoles.setup = getSetup();
        return response;
    }

    public void removeSetupHidden(Role role) throws JSONException, SQLException {
        SetupHidden setupHidden = SetupHiddenTestUtil.findSetupHidden(BasicRoles.setup, role.getName());
        removeSetupHidden(setupHidden);
        BasicRoles.setup = getSetup();
    }

    public void removeSetupHidden(SetupHidden setupHidden) throws JSONException {
        JSONObject args = new JSONObject();
        args.put("userID", getUserID());
        args.put("setupHiddenID", setupHidden.id);
        httpRequest("DELETE", "setupHiddens", args);
    }

    public void upsertFactionRoleAbilityModifier(FactionRole factionRole, AbilityType abilityType,
            AbilityModifierName modifierName, Object value) throws JSONException, SQLException {
        JSONObject args = new JSONObject();
        args.put("userID", getUserID());
        args.put("factionRoleID", factionRole.id);
        args.put("abilityID", factionRole.role.getAbility(abilityType).id);
        args.put("value", value);
        args.put("minPlayerCount", 0);
        args.put("maxPlayerCount", Setup.MAX_PLAYER_COUNT);
        args.put("name", modifierName.toString());
        JSONObject response = httpRequest("POST", "factionRoleAbilityModifiers", args);
        TestUtil.assertNoErrors(response);
    }

    public void upsertFactionRoleModifier(FactionRole factionRole, RoleModifierName name, Object val)
            throws JSONException, SQLException {
        JSONObject args = new JSONObject();
        args.put("userID", getUserID());
        args.put("factionRoleID", factionRole.id);
        args.put("value", val);
        args.put("minPlayerCount", 0);
        args.put("maxPlayerCount", Setup.MAX_PLAYER_COUNT);
        args.put("name", name.toString());
        JSONObject response = httpRequest("POST", "factionRoleModifiers", args);
        TestUtil.assertNoErrors(response);
    }

    public String getJoinID() throws SQLException {
        return this.lobbyID;
    }

    public GameID getGameID() throws SQLException {
        return this.gameID;
    }

    public GameID startGame() throws JSONException, SQLException {
        return startGame(Game.NIGHT_START);
    }

    public boolean autoStartCheck = true;

    public GameID startGame(boolean dayStart) throws JSONException, SQLException {
        updateSetupModifier(SetupModifierName.DAY_START, dayStart);
        Setup setup = this.getSetup();
        Game unstartedGame = GameService.getGame(this.getGameID(), setup);
        int playerCount = unstartedGame.players.size();
        if(autoStartCheck)
            SuperTest.assertEquals(setup.rolesList.size(unstartedGame), playerCount);

        JSONObject response = submitStartRequest();
        TestUtil.assertNoErrors(response);
        Game game = SuperTest.game = getGame();

        game.setSeed(0);
        if(game.isStarted()){
            return this.gameID;
        }else if(autoStartCheck)
            throw new PhaseException("Game wasn't started.");
        return null;
    }

    public JSONObject submitStartRequest() throws JSONException {
        JSONObject args = new JSONObject();
        args.put("gameID", this.gameID.getValue());
        args.put("start", true);
        return httpRequest("POST", "games", args);
    }

    @Override
    public Player getPlayer() {
        if(this.player != null)
            return this.player;
        try{
            PlayerSchema schema = PlayerRepo.getByGameIDAndUserID(this.gameID, getUserID());
            this.player = getLobby().game.players.getDatabaseMap().get(schema.id);
            return this.player;
        }catch(SQLException e){
            throw new Error(e);
        }
    }

    public JSONObject failRepick() throws JSONException, SQLException {
        return TestUtil.assertErrors(repick(""));
    }

    public JSONObject repick() throws JSONException, SQLException {
        return TestUtil.assertNoErrors(repick(""));
    }

    public boolean isHost() throws SQLException {
        Set<Long> moderatorIDs = ModeratorService.getModeratorIDs(getGameID());
        return moderatorIDs.contains(getUserID());
    }

    public void repick(UserWrapper user) throws JSONException, SQLException {
        repick(user.getName());
    }

    public JSONObject repick(String name) throws JSONException, SQLException {
        JSONArray activeUserIDs = new JSONArray();
        for(long gameUserID: UserService.getUserIDs(this.getGameID()))
            activeUserIDs.put(gameUserID);
        JSONObject args = new JSONObject();
        args.put("activeUserIDs", activeUserIDs);
        args.put("repickTarget", name);
        args.put("userID", getUserID());
        return httpRequest("POST", "moderators", args);
    }

    public void kick(UserWrapper nc) throws JSONException {
        JSONObject args = new JSONObject();
        args.put("kickedUserID", nc.getUserID());
        args.put("kickerID", this.id);
        httpRequest("DELETE", "players", args);
    }

    public Set<String> addBots(int botCount) throws JSONException, SQLException {
        Set<PlayerSchema> players = PlayerRepo.getByGameID(this.gameID);
        Set<String> playerNames = PlayerUtil.getNames(players);
        JSONObject args = new JSONObject();
        args.put("botCount", botCount);
        args.put("userID", this.id);

        JSONObject response = httpRequest("POST", "players", args);
        TestUtil.assertNoErrors(response);

        Set<String> playerNames2 = BotAddResponseJson.getPlayerNames(response.getJSONObject("response"));
        return CollectionUtil.diff(playerNames2, playerNames);
    }

    public PlayerList getBots() throws SQLException {
        PlayerList bots = new PlayerList();

        for(Player p: getGame().players){
            if(p.isComputer()){
                bots.add(p);
            }
        }

        return bots;
    }

    public UserWrapper prefer(FactionRole factionRole) throws JSONException, SQLException {
        long setupID = factionRole.faction.setup.id;
        preferRequest(PreferType.ROLE, factionRole.getName(), setupID);
        preferRequest(PreferType.FACTION, factionRole.getColor(), setupID);
        return this;
    }

    public void preferRequest(PreferType preferType, String name, long setupID) throws JSONException {
        JSONObject jo = new JSONObject();
        jo.put("preferName", name);
        jo.put("setupID", setupID);
        jo.put("userID", this.getUserID());
        JSONObject response = httpRequest("POST", "prefers", jo);
        TestUtil.assertNoErrors(response);
    }

    public HashMap<String, List<String>> getActions() {
        return abilities;
    }

    public void receiveMessage(JSONObject jo) throws JSONException, SQLException {
        if(jo.has(StateObject.guiUpdate)){
            if(jo.has(StateObject.playerLists))
                savePlayerLists(jo.getJSONObject(StateObject.playerLists));

            if(jo.has(StateObject.actions))
                saveActions(jo.getJSONObject(StateObject.actions));

            if(jo.has(StateObject.getDayActions)){
                JSONArray dayActions = jo.getJSONArray(StateObject.getDayActions);
                hasDayAction = false;
                for(int i = 0; i < dayActions.length(); i++){
                    if(!dayActions.getString(i).equalsIgnoreCase(Vote.COMMAND)){
                        hasDayAction = true;
                        break;
                    }
                }
            }
            if(jo.has("setup"))
                this.setup = jo.getJSONObject("setup");
            if(jo.has(StateObject.roleInfo))
                role = jo.getJSONObject(StateObject.roleInfo);
        }else if(jo.has(StateObject.warning)){
            this.hasWarning = true;
        }else if(jo.has(StateObject.unreadUpdate)){
            String update = jo.getString(StateObject.unreadUpdate);
            unread.put(update, jo.getInt(StateObject.count));
        }else if(jo.has(StateObject.speechContent)){

        }else if(jo.has("requestID")){
            if(jo.getJSONArray("errors").length() != 0)
                this.hasWarning = true;
        }else{// should handle other handleobject possibilities
            saveChats(jo);
        }

    }

    void updateSetupModifier(SetupModifierName name, boolean b) throws JSONException {
        JSONObject args = new JSONObject();
        args.put("userID", getUserID());
        args.put("name", name.toString());
        args.put("value", b);
        args.put("minPlayerCount", 0);
        args.put("maxPlayerCount", Setup.MAX_PLAYER_COUNT);

        httpRequest("POST", "setupModifiers", args);
    }

    void updateGameModifier(GameModifierName ruleID, boolean b) throws JSONException {
        JSONObject args = new JSONObject();
        args.put("userID", getUserID());
        args.put("name", ruleID.toString());
        args.put("value", b);

        httpRequest("PUT", "gameModifiers", args);
    }

    void updateGameModifier(GameModifierName name, int value) throws JSONException {
        JSONObject args = new JSONObject();
        args.put("userID", getUserID());
        args.put("name", name.toString());
        args.put("value", value);

        httpRequest("PUT", "gameModifiers", args);
    }

    void updateSetupModifier(SetupModifierName name, int value) throws JSONException {
        JSONObject args = new JSONObject();
        args.put("userID", getUserID());
        args.put("name", name.toString());
        args.put("value", value);

        httpRequest("PUT", "setupModifiers", args);
    }

    JSONObject setSetup(long setupID) throws JSONException, SQLException {
        JSONObject args = new JSONObject();
        args.put("userID", getUserID());
        args.put("setupID", setupID);
        args.put("gameID", getGameID());
        JSONObject response = httpRequest("PUT", "setups", args);
        BasicRoles.setup = SetupService.getSetup(setupID);
        return response;
    }

    public JSONObject createTeam(String teamName, String teamColor) throws JSONException, SQLException {
        JSONObject args = new JSONObject();
        args.put("userID", getUserID());
        args.put("setupID", BasicRoles.setup.id);
        args.put("name", teamName);
        args.put("color", teamColor);
        args.put("description", "");
        JSONObject response = httpRequest("POST", "factions", args);
        BasicRoles.setup = SetupService.getSetup(BasicRoles.setup.id);
        return response;
    }

    public JSONObject createFactionRole(String roleName, String teamColor) throws JSONException, SQLException {
        Setup setup = BasicRoles.setup;
        JSONObject args = new JSONObject();
        args.put("userID", getUserID());
        args.put("factionID", setup.getFactionByColor(teamColor).id);
        args.put("roleID", RoleTestUtil.getRole(setup, roleName).id);
        return httpRequest("POST", "factionRoles", args);
    }

    public void deleteFactionRole(FactionRole factionRole) throws JSONException {
        JSONObject args = new JSONObject();
        args.put("userID", getUserID());
        args.put("factionRoleID", factionRole.id);
        httpRequest("DELETE", "factionRoles", args);
    }

    private void saveActions(JSONObject jo) throws JSONException, SQLException {
        submittedActions.clear();
        submittedActionsJSON = jo;

        JSONArray jActions = jo.getJSONArray(StateObject.actionList), jPlayerNames;
        JSONObject jAction;
        Action a;
        Player p, me = getPlayer();
        String command;
        PlayerList pl;
        List<String> args;
        for(int i = 0; i < jActions.length(); i++){
            jAction = jActions.getJSONObject(i);

            command = jAction.getString(StateObject.command);
            jPlayerNames = jAction.getJSONArray(StateObject.playerNames);
            pl = new PlayerList();
            for(int playerCounter = 0; playerCounter < jPlayerNames.length(); playerCounter++){
                p = me.game.getPlayerByName(jPlayerNames.getString(playerCounter));
                pl.add(p);
            }
            args = new LinkedList<>();
            if(jAction.has(StateObject.option))
                args.add(jAction.getString(StateObject.option));
            if(jAction.has(StateObject.option2))
                args.add(jAction.getString(StateObject.option2));
            if(jAction.has(StateObject.option3))
                args.add(jAction.getString(StateObject.option3));
            a = new Action(me, AbilityUtil.getAbilityByCommand(command), args, pl);
            submittedActions.add(a);
        }
    }

    private void savePlayerLists(JSONObject jo) throws JSONException, SQLException {
        JSONArray types = jo.getJSONArray(StateObject.type);
        abilities.clear();
        List<String> pl;
        String type;
        JSONArray jPlayers;
        JSONObject jObject;
        String name;
        for(int i = 0; i < types.length(); i++){
            pl = new LinkedList<>();
            type = types.getString(i);
            if(!jo.getJSONObject(type).has("players"))
                continue;
            jPlayers = jo.getJSONObject(type).getJSONArray("players");

            for(int j = 0; j < jPlayers.length(); j++){
                jObject = jPlayers.getJSONObject(j);
                name = jObject.getString(StateObject.playerName);
                if(name.equalsIgnoreCase(Constants.SKIP_DAY))
                    continue;
                pl.add(name);
            }

            abilities.put(type, pl);
        }
    }

    private int getDayNumber(String input) {
        Matcher matcher = Pattern.compile("\\d+").matcher(input);
        matcher.find();
        return Integer.valueOf(matcher.group());
    }

    private void saveChats(JSONObject jo) throws JSONException, SQLException {
        if(jo.has(StateObject.chatReset)){
            chatLogs.clear();
            keys.clear();
            JSONArray chats = jo.getJSONArray(StateObject.chatReset);
            JSONObject chatJSON;
            String chatName;
            for(int i = 0; i < chats.length(); i++){
                chatJSON = chats.getJSONObject(i);
                chatName = chatJSON.getString(StateObject.chatName);
                if(chatJSON.has(StateObject.isDay)){
                    for(int j = getDayNumber(chatName) - 1; j >= 1; j--){
                        this.chatLogs.put("Day " + j, new ArrayList<>());
                    }
                }
                this.chatLogs.put(chatName, new ArrayList<>());
                if(chatJSON.has(StateObject.chatKey)){
                    this.keys.put(chatName, chatJSON.getString(StateObject.chatKey));
                }
            }
        }
        JSONObject message;
        JSONArray chatNames;
        String chatLabel;
        if(jo.get(StateObject.message) instanceof JSONArray){
            JSONArray messages = jo.getJSONArray(StateObject.message);
            for(int i = 0; i < messages.length(); i++){
                message = messages.getJSONObject(i);
                chatNames = message.getJSONArray("chat");
                for(int k = 0; k < chatNames.length(); k++){
                    chatLabel = chatNames.getString(k);
                    pushToSingleChat(chatLabel, message);
                }
            }
        }else{
            jo.put("text", jo.get("message"));
            jo.remove("message");
            for(String key: this.chatLogs.keySet()){
                pushToSingleChat(key, jo);
            }
        }
    }

    private void pushToSingleChat(String chatLabel, JSONObject jo) throws JSONException, SQLException {
        ArrayList<String> messages = this.chatLogs.get(chatLabel);
        if(messages == null){
            Player player = getPlayer();
            System.err.println(player.getChats());
            System.err.println(this.chatLogs);
            player.getEvents();
            throw new NullPointerException(chatLabel);
        }
        String text = jo.getString("text");
        if(jo.has(StateObject.messageType)
                && jo.getString(StateObject.messageType).equals(ChatMessage.class.getSimpleName()))
            text = jo.getString(StateObject.sender) + text;
        messages.add(text);
    }

    public void setNightTargetByName(AbilityType abilityType, String... names) {
        List<String> targets = CollectionUtil.toArrayList(names);
        ActionSubmitRequest request = new ActionSubmitRequest(abilityType, targets, this.getUserID());
        setTarget(request);
    }

    public void setNightTarget(String command, String opt1, Controller... targets) throws SQLException {
        PlayerList playerTargets = ControllerList.ToPlayerList(getGame(), targets);
        Action action = new Action(getPlayer(), AbilityUtil.getAbilityByCommand(command), opt1, null, playerTargets);
        setNightTarget(action);
    }

    public void setNightTarget(String command, Controller... targets) throws SQLException {
        PlayerList playerTargets = ControllerList.ToPlayerList(getGame(), targets);
        Action action = new Action(getPlayer(), AbilityUtil.getAbilityByCommand(command), null, (String) null,
                playerTargets);
        setNightTarget(action);
    }

    @Override
    public void setNightTarget(Action action) {
        setTarget(new ActionSubmitRequest(action, this.getUserID()));
    }

    public void setTarget(ActionSubmitRequest request) {
        try{
            JSONObject response = httpRequest("PUT", "actions", request.toJson());
            TestUtil.assertNoErrors(response);
        }catch(JSONException e){
            throw new Error(e);
        }
    }

    public void endPhase() throws JSONException {
        JSONObject jo = new JSONObject();
        jo.put(StateObject.message, CommandHandler.END_PHASE);
        push(jo);
    }

    public void vote(String votedName) {
        ActionSubmitRequest request = new ActionSubmitRequest(Vote.abilityType, CollectionUtil.toArrayList(votedName),
                this.getUserID());
        setTarget(request);
    }

    public void vote(Controller targetController) throws SQLException {
        Player target = targetController.getPlayer();
        Action action = new Action(this.getPlayer(), Vote.abilityType, target);
        this.doDayAction(action);
    }

    public void unvote() {
        this.cancelAction(0, Constants.ALL_TIME_LEFT);
    }

    public void skipVote() throws SQLException {
        this.doDayAction(VoteUtil.skipAction(getPlayer(), Constants.ALL_TIME_LEFT));
    }

    public void doDayAction() throws SQLException {
        Player player = this.getPlayer();
        AbilityType abilityType = player.getRoleAbilities().getDayAbilities().get(0).getAbilityType();
        Action action = new Action(player, abilityType);
        super.doDayAction(action);
    }

    public void doDayAction(String name) {
        Player player = this.getPlayer();
        AbilityType abilityType = player.getRoleAbilities().getDayAbilities().get(0).getAbilityType();
        ActionSubmitRequest request = new ActionSubmitRequest(abilityType, CollectionUtil.toArrayList(name),
                this.getUserID());
        setTarget(request);
    }

    public void doDayAction(Controller targetController) throws SQLException {
        this.doDayAction(targetController.getName());
    }

    public void doDayAction(Controller... wps) throws SQLException {
        Player player = this.getPlayer();
        AbilityType abilityType = player.getRoleAbilities().getDayAbilities().get(0).getAbilityType();
        PlayerList targets = ControllerList.ToPlayerList(player.game, wps);
        Action action = new Action(player, abilityType, new LinkedList<>(), targets);
        super.doDayAction(action);
    }

    public void cancelAction(int actionIndex) {
        cancelAction(actionIndex, Constants.ALL_TIME_LEFT);
    }

    public void endNight() throws JSONException {
        sendActionMessage(Constants.END_NIGHT);
    }

    private void sendActionMessage(String message) throws JSONException {
        JSONObject args = new JSONObject();
        args.put("userID", this.getUserID());
        args.put("timeLeft", Constants.ALL_TIME_LEFT);
        args.put("message", message);
        JSONObject response = httpRequest("PUT", "actions", args);
        TestUtil.assertNoErrors(response);
    }

    @Override
    public void cancelAction(int actionIndex, double timeLeft) {
        try{
            JSONObject args = new JSONObject();
            args.put("command", this.submittedActions.get(actionIndex).abilityType.command);
            args.put("actionIndex", actionIndex);
            args.put("userID", getUserID());
            args.put("timeLeft", timeLeft);
            httpRequest("DELETE", "actions", args);
        }catch(JSONException e){
            throw new Error(e);
        }
    }

    public void requestChat() throws JSONException {
        JSONObject jo = new JSONObject();
        jo.put(StateObject.message, StateObject.requestChat);
        push(jo);
    }

    public void requestGameState() throws JSONException {
        JSONObject jo = new JSONObject();
        jo.put(StateObject.message, StateObject.requestGameState);
        push(jo);
    }

    public void requestUnreads() throws JSONException {
        JSONObject jo = new JSONObject();
        jo.put(StateObject.message, StateObject.requestUnreads);
        push(jo);
    }

    public boolean isCaughtUp() {
        for(Integer i: unread.values()){
            if(i != 0)
                return false;
        }
        return true;
    }

    public void setRead(String name) throws JSONException {
        unread.put(name, 0);

        JSONObject jo = new JSONObject();
        jo.put(StateObject.message, StateObject.setReadChat);
        jo.put(StateObject.setReadChat, name);

        push(jo);
    }

    public void changeName(String name) throws JSONException {
        JSONObject args = new JSONObject();
        args.put("userID", this.getUserID());
        args.put("playerName", name);
        JSONObject response = httpRequest("PUT", "players", args);
        TestUtil.assertNoErrors(response);
    }

    @Override
    public void setLastWill(String s) {
        JSONObject args = new JSONObject();
        try{
            args.put("message", "lw " + s);
            args.put("timeLeft", 1);
            args.put("userID", this.getUserID());
            JSONObject response = httpRequest("PUT", "actions", args);
            TestUtil.assertNoErrors(response);
        }catch(JSONException e){
            throw new Error();
        }
    }

    @Override
    public void log(String string) {
        // TODO Auto-generated method stub
    }

    @Override
    public Controller setName(String name) {
        this.name = name;
        return this;
    }

    @Override
    public boolean isTargeting(AbilityType ability, Controller... targets) {
        // TODO use webplayer storage

        return getPlayer().isTargeting(ability, targets);

    }

    public void requestAllGameInfo() throws JSONException, SQLException {
        Lobby lobby = getLobby();
        lobby.sendGameState(getUserID());
        lobby.resetChat(getUserID());
        ChatService.pushUnreads(lobby.gameID, getUserID());
    }

}
