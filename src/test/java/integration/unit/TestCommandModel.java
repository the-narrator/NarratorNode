package integration.unit;

import java.util.Optional;

import game.logic.support.Constants;
import junit.framework.TestCase;
import models.Command;
import models.idtypes.PlayerDBID;

public class TestCommandModel extends TestCase {
    public void testEquals() {
        Command command1 = new Command(Optional.of(new PlayerDBID(1)), "command", Constants.ALL_TIME_LEFT);
        Command command2 = new Command(Optional.of(new PlayerDBID(1)), "command", Constants.ALL_TIME_LEFT);

        assertTrue(command1.equals(command2));
    }

    public void testNotEquals() {
        Command command1 = new Command(Optional.of(new PlayerDBID(1)), "command", Constants.ALL_TIME_LEFT);
        Command command2 = new Command(Optional.of(new PlayerDBID(2)), "command", Constants.ALL_TIME_LEFT);

        assertFalse(command1.equals(command2));
    }
}
