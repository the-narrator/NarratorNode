package integration.unit;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import game.abilities.Hidden;
import game.abilities.Thief;
import game.logic.templates.BasicRoles;
import game.logic.templates.NativeControllerGenerator;
import game.setups.Setup;
import integration.logic.SuperTest;
import junit.framework.TestCase;
import models.FactionRole;
import models.GeneratedRole;
import models.SetupHidden;
import models.schemas.SetupHiddenSchema;
import services.GameSpawnsService;
import util.Util;

public class TestRoleAssignmentService extends TestCase {
    public void testDecrementingPassOutTracker() {
        String[] tick = new String[] { "11100", "11010", "11001", "10110", "10101", "10011", "01110", "01101", "01011",
                "00111", };

        assertTick(tick[0], tick[1]);
        assertTick(tick[1], tick[2]);
        assertTick(tick[2], tick[3]);
        assertTick(tick[3], tick[4]);
        assertTick(tick[4], tick[5]);
        assertTick(tick[5], tick[6]);
        assertTick(tick[6], tick[7]);
        assertTick(tick[7], tick[8]);
        assertTick(tick[8], tick[9]);
        assertTick(tick[9], null);

        assertTick("10", "01");
        assertTick("1110111", "1101111");
    }

    private static void assertTick(String inputString, String resultString) {
        boolean[] input = stringToArray(inputString);
        boolean[] expectedResult = stringToArray(resultString);
        boolean[] result = GameSpawnsService.decrementPassOutTracker(input);
        if(result == null){
            assertNull(expectedResult);
            return;
        }

        for(int i = 0; i < expectedResult.length; i++)
            assertEquals(expectedResult[i], result[i]);

    }

    private static boolean[] stringToArray(String input) {
        if(input == null)
            return null;
        boolean[] arr = new boolean[input.length()];
        for(int i = 0; i < input.length(); i++){
            arr[i] = input.charAt(i) == '1';
        }
        return arr;
    }

    public void testIsValid() {
        SuperTest.generator = new NativeControllerGenerator();
        SuperTest.newNarrator();
        Thief.template(SuperTest.setup.getFactionByColor(Setup.BENIGN_C));

        assertIsValid(3, BasicRoles.Mason(), BasicRoles.Thief(), BasicRoles.Goon(), BasicRoles.Mason(),
                BasicRoles.Enforcer());
        assertIsNotValid(3, BasicRoles.Mason(), BasicRoles.Thief(), BasicRoles.Goon(), BasicRoles.Mason(),
                BasicRoles.Citizen());
        assertIsValid(3, BasicRoles.Citizen(), BasicRoles.Thief(), BasicRoles.Citizen(), BasicRoles.Goon(),
                BasicRoles.Goon());
        assertIsNotValid(3, BasicRoles.Citizen(), BasicRoles.Thief(), BasicRoles.Citizen(), BasicRoles.Goon(),
                BasicRoles.Citizen());
    }

    private static void assertIsValid(int playerCount, FactionRole... factionRoleArray) {
        assertValidity(true, playerCount, factionRoleArray);
    }

    private static void assertIsNotValid(int playerCount, FactionRole... factionRoleArray) {
        assertValidity(false, playerCount, factionRoleArray);
    }

    private static void assertValidity(boolean isValid, int playerCount, FactionRole... factionRoleArray) {
        ArrayList<GeneratedRole> generatedRoles = new ArrayList<>();
        List<GeneratedRole> thiefRoles = new LinkedList<>();
        Hidden hidden = Hidden.TownGovernment();
        SetupHiddenSchema schema = new SetupHiddenSchema(Util.getID(), hidden.id, false, false, 0,
                Setup.MAX_PLAYER_COUNT);
        SetupHidden setupHidden = new SetupHidden(hidden, schema);
        GeneratedRole generatedRole;
        for(int i = 0; i < playerCount; i++){
            generatedRole = new GeneratedRole(setupHidden);
            generatedRole.factionRoles.add(factionRoleArray[i]);
            generatedRoles.add(generatedRole);
        }
        for(int i = playerCount; i < factionRoleArray.length; i++){
            generatedRole = new GeneratedRole(setupHidden);
            generatedRole.factionRoles.add(factionRoleArray[i]);
            thiefRoles.add(generatedRole);
        }
        assertEquals(isValid, GameSpawnsService.isValid(playerCount, generatedRoles, thiefRoles) == null);
    }
}
