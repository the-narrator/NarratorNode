package integration.controller;

import java.sql.SQLException;

import game.logic.Player;
import game.logic.support.Constants;
import game.logic.templates.BasicRoles;
import integration.ControllerTestCase;
import integration.server.UserWrapper;
import json.JSONArray;
import json.JSONException;
import json.JSONObject;
import services.PlayerService;
import util.FakeConstants;
import util.TestRequest;

public class TestActionController extends ControllerTestCase {

    public void testSubmitBadFrameActionNoOpt() throws JSONException, SQLException {
        UserWrapper host = new UserWrapper(FakeConstants.PLAYER_NAME);
        host.hostGame(false);
        host.addSetupHidden(BasicRoles.Framer());
        host.addSetupHidden(BasicRoles.Citizen());
        host.addSetupHidden(BasicRoles.Framer());
        host.addBots(2);
        host.prefer(BasicRoles.Framer());
        host.startGame(false); // night start
        JSONArray targets = new JSONArray();
        Player player = PlayerService.getByUserID(host.getUserID()).get();
        targets.put(player.getName());
        JSONObject request = new JSONObject();
        request.put("userID", host.id);
        request.put("command", "frame");
        request.put("targets", targets);
        request.put("timeLeft", Constants.ALL_TIME_LEFT);

        JSONObject response = TestRequest.put("actions", request);
        JSONArray errors = response.getJSONArray("errors");

        assertNotSame(errors.get(0), "");
    }

}
