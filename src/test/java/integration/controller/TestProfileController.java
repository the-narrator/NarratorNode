package integration.controller;

import game.abilities.CultLeader;
import game.abilities.Detective;
import game.abilities.Infiltrator;
import game.abilities.Sheriff;
import game.abilities.Vote;
import game.ai.Controller;
import game.logic.Player;
import game.logic.templates.BasicRoles;
import game.setups.Setup;
import integration.logic.SuperTest;
import json.JSONException;
import models.Faction;
import models.Role;
import models.enums.GameModifierName;
import models.enums.SetupModifierName;
import models.enums.VoteSystemTypes;
import models.json_output.profile.ProfileAbilityJson;
import models.json_output.profile.ProfileAllyJson;
import models.json_output.profile.ProfileRoleCardJson;
import services.FactionRoleService;
import services.GameService;
import services.RoleService;
import services.SetupModifierService;
import util.models.FactionRoleTestUtil;

public class TestProfileController extends SuperTest {
    public TestProfileController(String name) {
        super(name);
    }

    public void testSheriffNotInDayJson() throws JSONException {
        Controller sheriff = addPlayer(BasicRoles.Sheriff());
        addPlayer(BasicRoles.Goon(), 2);

        dayStart();

        assertLackOfCommand(sheriff, Sheriff.COMMAND);
    }

    public void testSheriffInNightJson() throws JSONException {
        Controller sheriff = addPlayer(BasicRoles.Sheriff());
        addPlayer(BasicRoles.Goon(), 2);

        nightStart();

        assertCommand(sheriff, Sheriff.COMMAND);
    }

    public void testSheriffn0Peek() throws JSONException {
        Controller sheriff = addPlayer(BasicRoles.Sheriff());
        Controller citizen = addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Goon());
        SetupModifierService.upsertModifier(setup, SetupModifierName.SHERIFF_PREPEEK, true);

        nightStart();

        ProfileRoleCardJson roleCard = new ProfileRoleCardJson(sheriff.getPlayer());
        for(String detail: roleCard.details){
            if(detail.contains(citizen.getName()))
                return;
        }
        fail("Profile was missing peek.");
    }

    public void testNoVotingDuringDiscussion() throws JSONException {
        Controller citizen = addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Goon());
        GameService.upsertModifier(game, GameModifierName.VOTE_SYSTEM, VoteSystemTypes.DIMINISHING_POOL.value);
        GameService.upsertModifier(game, GameModifierName.DISCUSSION_LENGTH, 1);

        dayStart();

        assertLackOfCommand(citizen, Vote.COMMAND);
    }

    public void testConvertingInfiltratorAllies() throws JSONException {
        Faction mafia = setup.getFactionByColor(Setup.MAFIA_C);
        Role role = RoleService.createRole(setup, "Infiltrator", Infiltrator.abilityType);
        Controller infil = addPlayer(FactionRoleService.createFactionRole(mafia, role));
        Player cultLeader = addPlayer(BasicRoles.CultLeader()).getPlayer();
        addPlayer(BasicRoles.Citizen());

        setTarget(cultLeader, infil, CultLeader.abilityType);
        endNight();

        ProfileAllyJson ally = ProfileAllyJson.getSet(cultLeader).iterator().next();
        assertEquals(BasicRoles.Cultist().getName(), ally.roleName);
        assertEquals(BasicRoles.CultLeader().faction.id, ally.factionID);
    }

    public void testCultAllies() throws JSONException {
        Player cultLeader = addPlayer(BasicRoles.CultLeader()).getPlayer();
        Player citizen = addPlayer(BasicRoles.Citizen()).getPlayer();
        addPlayer(BasicRoles.Citizen());

        FactionRoleTestUtil.addCultRole(BasicRoles.CultLeader(), BasicRoles.Detective());
        SetupModifierService.upsertModifier(setup, SetupModifierName.CULT_PROMOTION, true);
        SetupModifierService.upsertModifier(setup, SetupModifierName.CULT_KEEPS_ROLES, false);

        setTarget(cultLeader, CultLeader.abilityType, Detective.class.getSimpleName(), citizen);
        endNight();

        assertEquals(1, ProfileAllyJson.getSet(cultLeader).size());
        assertEquals(1, ProfileAllyJson.getSet(citizen).size());
    }

    private static void assertLackOfCommand(Controller controller, String command) throws JSONException {
        command = command.toLowerCase();
        Player player = controller.getPlayer();
        ProfileRoleCardJson roleCard = new ProfileRoleCardJson(player);
        for(ProfileAbilityJson ability: roleCard.abilities)
            if(ability.command != null)
                assertNotSame(ability.command.toLowerCase(), command);
    }

    private static void assertCommand(Controller controller, String command) throws JSONException {
        command = command.toLowerCase();
        Player player = controller.getPlayer();
        ProfileRoleCardJson roleCard = new ProfileRoleCardJson(player);
        for(ProfileAbilityJson ability: roleCard.abilities)
            if(ability.command.equalsIgnoreCase(command))
                return;
        fail(command + " not found");
    }
}
