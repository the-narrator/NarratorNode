package integration.controller;

import java.sql.SQLException;
import java.time.Instant;

import game.abilities.Blacksmith;
import game.setups.Setup;
import integration.ControllerTestCase;
import integration.server.UserWrapper;
import json.JSONArray;
import json.JSONException;
import json.JSONObject;
import models.Ability;
import models.Role;
import models.enums.AbilityModifierName;
import models.modifiers.AbilityModifier;
import services.RoleAbilityModifierService;
import util.FakeConstants;
import util.TestRequest;
import util.TestUtil;
import util.models.RoleTestUtil;

public class TestRoleController extends ControllerTestCase {
    public void testGetAbilityModifierTrue() throws JSONException, SQLException {
        UserWrapper host = new UserWrapper(FakeConstants.PLAYER_NAME);
        host.hostGame(false);
        long roleID = upsertModifier(host, true);

        JSONObject response = getRequest(host.id, roleID);

        JSONObject ability = response.getJSONArray("abilities").getJSONObject(0);
        JSONArray modifiers = ability.getJSONArray("modifiers");

        JSONObject modifier;
        for(int i = 0; i < modifiers.length(); i++){
            modifier = modifiers.getJSONObject(i);
            if(AbilityModifierName.AS_FAKE_VESTS.toString().equals(modifier.getString("name"))){
                assertTrue(modifier.getBoolean("value"));
                return;
            }
        }
        fail("modifier not found");
    }

    public void testGetAbilityModifierFalse() throws JSONException, SQLException {
        UserWrapper host = new UserWrapper(FakeConstants.PLAYER_NAME);
        host.hostGame(false);
        long roleID = upsertModifier(host, false);

        JSONObject response = getRequest(host.id, roleID);

        JSONObject ability = response.getJSONArray("abilities").getJSONObject(0);
        JSONArray modifiers = ability.getJSONArray("modifiers");

        JSONObject modifier;
        for(int i = 0; i < modifiers.length(); i++){
            modifier = modifiers.getJSONObject(i);
            if(AbilityModifierName.AS_FAKE_VESTS.toString().equals(modifier.getString("name"))){
                assertFalse(modifier.getBoolean("value"));
                assertTrue(modifier.has("label"));
                return;
            }
        }
        fail("modifier not found");
    }

    private static JSONObject getRequest(long hostID, long roleID) throws JSONException {
        JSONObject request = new JSONObject();
        request.put("userID", hostID);
        request.put("roleID", roleID);
        JSONObject response = TestRequest.get("roles", request);
        TestUtil.assertNoErrors(response);
        return response.getJSONObject("response");
    }

    private static long upsertModifier(UserWrapper host, Object value) throws SQLException {
        Setup setup = host.getSetup();
        Role blacksmith = RoleTestUtil.getRoleWithAbility(setup, Blacksmith.abilityType);
        Ability ability = blacksmith.getAbility(Blacksmith.abilityType);
        RoleAbilityModifierService.upsertModifier(host.id, blacksmith.id, ability.id, new AbilityModifier(
                AbilityModifierName.AS_FAKE_VESTS, value, 0, Setup.MAX_PLAYER_COUNT, Instant.now()));
        return blacksmith.id;
    }
}
