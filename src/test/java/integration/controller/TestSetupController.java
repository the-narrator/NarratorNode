package integration.controller;

import java.sql.SQLException;
import java.util.Collection;
import java.util.concurrent.ExecutionException;

import integration.ControllerTestCase;
import integration.server.InstanceTests;
import integration.server.UserWrapper;
import json.JSONException;
import json.JSONObject;
import models.schemas.FactionSchema;
import repositories.FactionRepo;

public class TestSetupController extends ControllerTestCase {

    @Override
    public void setUp() throws Exception {
        super.setUp();
        InstanceTests.wipeTables();
    }

    public void testCreateSetup() throws JSONException, SQLException, InterruptedException, ExecutionException {
        UserWrapper user = new UserWrapper("userName");
        JSONObject setupResponse = user.createSetup();
        long setupID = setupResponse.getJSONObject("response").getLong("id");

        Collection<FactionSchema> factions = FactionRepo.getBySetupID(setupID).get();
        assertNotSame(0, factions.size());
    }
}
