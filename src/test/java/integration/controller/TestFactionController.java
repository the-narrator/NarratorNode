package integration.controller;

import java.sql.SQLException;

import game.setups.Setup;
import integration.ControllerTestCase;
import integration.server.UserWrapper;
import json.JSONArray;
import json.JSONException;
import json.JSONObject;
import models.Faction;
import models.schemas.GameOverviewSchema;
import repositories.FactionRepo;
import repositories.GameRepo;
import services.SetupService;
import util.FakeConstants;
import util.TestRequest;

public class TestFactionController extends ControllerTestCase {

    public void testDeleteFaction() throws JSONException, SQLException {
        UserWrapper host = new UserWrapper(FakeConstants.PLAYER_NAME);
        host.hostGame(false);
        Faction faction = getFactionToDelete(host);
        JSONObject request = new JSONObject();
        request.put("factionID", faction.id);
        request.put("userID", host.id);

        JSONObject response = TestRequest.delete("factions", request);

        JSONArray errors = response.getJSONArray("errors");
        assertEquals(0, errors.length());
        assertNull(FactionRepo.getIDBySetupIDAndColor(faction.setup.id, faction.color));
    }

    private static Faction getFactionToDelete(UserWrapper user) throws SQLException {
        GameOverviewSchema game = GameRepo.getOverview(user.getGameID());
        Setup setup = SetupService.getSetup(game.setupID);
        return setup.factions.values().iterator().next();
    }

}
