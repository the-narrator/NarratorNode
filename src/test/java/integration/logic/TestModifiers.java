package integration.logic;

import java.util.Optional;

import game.abilities.Agent;
import game.abilities.Armorsmith;
import game.abilities.Baker;
import game.abilities.Blackmailer;
import game.abilities.Burn;
import game.abilities.Doctor;
import game.abilities.FactionKill;
import game.abilities.Infiltrator;
import game.abilities.Sheriff;
import game.abilities.TeamTakedown;
import game.ai.Controller;
import game.logic.exceptions.NarratorException;
import game.logic.support.Constants;
import game.logic.templates.BasicRoles;
import game.setups.Setup;
import models.Faction;
import models.FactionRole;
import models.Role;
import models.enums.AbilityModifierName;
import models.enums.AbilityType;
import models.enums.FactionModifierName;
import models.enums.GameModifierName;
import models.enums.RoleModifierName;
import models.enums.SetupModifierName;
import models.modifiers.Modifiers;
import services.FactionAbilityModifierService;
import services.FactionModifierService;
import services.FactionRoleService;
import services.GameService;
import services.RoleAbilityModifierService;
import services.RoleModifierService;
import services.RoleService;
import services.SetupModifierService;
import util.game.LookupUtil;
import util.game.ModifierUtil;
import util.models.ModifierTestUtil;
import util.models.RoleTestUtil;

public class TestModifiers extends SuperTest {

    public TestModifiers(String name) {
        super(name);
    }

    public void test0AutoVest() {
        modifyAbilityCooldown(BasicRoles.Sheriff(), Sheriff.abilityType, 0);
        Modifiers<AbilityModifierName> modifiers = BasicRoles.Sheriff().role.getAbility(Sheriff.abilityType).modifiers;
        assertEquals(0, ModifierTestUtil.getValue(modifiers, AbilityModifierName.COOLDOWN));

        // ignores negative value for cooldown
        try{
            modifyAbilityCooldown(BasicRoles.Sheriff(), Sheriff.abilityType, Constants.UNLIMITED);
            fail();
        }catch(NarratorException e){
        }

        modifyAbilityCooldown(BasicRoles.Sheriff(), Sheriff.abilityType, 3);
        assertEquals(3, ModifierTestUtil.getValue(modifiers, AbilityModifierName.COOLDOWN));

        try{
            modifyAbilityCooldown(BasicRoles.Sheriff(), Sheriff.abilityType, -5);
            fail();
        }catch(NarratorException e){
        }
    }

    public void testRunningOutOfBread() {
        modifyAbilityCharges(BasicRoles.Baker(), Baker.abilityType, 1);

        Controller baker = addPlayer(BasicRoles.Baker());
        Controller cit = addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Goon());

        editRule(SetupModifierName.CHARGE_VARIABILITY, 0);

        nightStart();

        assertPassableBreadCount(1, baker);
        // assertEquals(1, baker.getBread().size());

        nextNight();

        assertPassableBreadCount(1, baker);
        // assertEquals(1, baker.getBread().size());

        setTarget(baker, cit);
        nextNight();

        assertPassableBreadCount(0, baker);
        // assertEquals(0, baker.getBread().size());
    }

    public void test0ChargesFail() {
        try{
            modifyAbilityCharges(BasicRoles.Armorsmith(), Armorsmith.abilityType, 0);
            fail();
        }catch(NarratorException e){
        }
    }

    public void testFactionKillPlusRoleAbility() {
        Controller bm = addPlayer(BasicRoles.Blackmailer());
        Controller mayor = addPlayer(BasicRoles.Mayor());
        Controller gs = addPlayer(BasicRoles.Gunsmith());
        addPlayer(BasicRoles.Citizen());

        modifyTeamAbility(Setup.MAFIA_C, AbilityModifierName.ZERO_WEIGHTED, FactionKill.abilityType, true);

        mafKill(bm, gs);
        // assertEquals(0, bm.getActionWeight());
        assertTrue(bm.getPlayer().getActions().canAddAnotherAction(Blackmailer.abilityType));
        assertFalse(bm.getPlayer().getActions().canAddAnotherAction(FactionKill.abilityType));
        setTarget(bm, mayor);
        assertActionSize(2, bm);
        assertFalse(bm.getPlayer().getActions().canAddAnotherAction(Blackmailer.abilityType));

        cancelAction(bm, 0);
        assertActionSize(1, bm);
        assertTrue(bm.getPlayer().getActions().canAddAnotherAction(Blackmailer.abilityType));
        assertFalse(bm.getPlayer().getActions().canAddAnotherAction(FactionKill.abilityType));

        setTarget(bm, mayor);
        cancelAction(bm, 1);
        assertTrue(bm.getPlayer().getActions().freeActions.isEmpty());
        assertFalse(bm.getPlayer().getActions().canAddAnotherAction(Blackmailer.abilityType));
        assertTrue(bm.getPlayer().getActions().canAddAnotherAction(FactionKill.abilityType));

        mafKill(bm, gs);
        assertActionSize(2, bm);

        cancelAction(bm, 0);
        cancelAction(bm, 0);

        setTarget(bm, mayor);
        // assertEquals(1, bm.getActionWeight());
        assertFalse(bm.getPlayer().getActions().canAddAnotherAction(Blackmailer.abilityType));
        assertTrue(bm.getPlayer().getActions().canAddAnotherAction(FactionKill.abilityType));
        mafKill(bm, gs);
        assertActionSize(2, bm);
        assertFalse(bm.getPlayer().getActions().canAddAnotherAction(Blackmailer.abilityType));
        assertFalse(bm.getPlayer().getActions().canAddAnotherAction(FactionKill.abilityType));

        mafKill(bm, gs);
        setTarget(bm, mayor);

        endNight();

        isDead(gs);
        assertStatus(mayor, Blackmailer.abilityType);
    }

    public void testTripleKill() {
        Controller fodder = addPlayer(BasicRoles.Citizen());
        Controller baker1 = addPlayer(BasicRoles.Baker());
        Controller baker2 = addPlayer(BasicRoles.Baker());
        Controller agent = addPlayer(BasicRoles.Agent());

        modifyTeamAbility(Setup.MAFIA_C, AbilityModifierName.ZERO_WEIGHTED, FactionKill.abilityType, true);

        setTarget(baker1, agent);
        setTarget(baker2, agent);
        nextNight();

        mafKill(agent, baker1);
        assertTrue(agent.getPlayer().getActions().canAddAnotherAction(FactionKill.abilityType));
        assertTrue(agent.getPlayer().getActions().canAddAnotherAction(Agent.abilityType));
        mafKill(agent, baker2);
        assertTrue(agent.getPlayer().getActions().canAddAnotherAction(FactionKill.abilityType));
        assertTrue(agent.getPlayer().getActions().canAddAnotherAction(Agent.abilityType));
        mafKill(agent, fodder);
        assertFalse(agent.getPlayer().getActions().canAddAnotherAction(FactionKill.abilityType));
        assertTrue(agent.getPlayer().getActions().canAddAnotherAction(Agent.abilityType));
        setTarget(agent, agent);
        assertFalse(agent.getPlayer().getActions().canAddAnotherAction(FactionKill.abilityType));
        assertFalse(agent.getPlayer().getActions().canAddAnotherAction(Agent.abilityType));
        assertActionSize(4, agent);
    }

    public void testBadSelfTargets() {
        Role infiltratorRole = RoleService.createRole(setup, "Infiltrator", Infiltrator.abilityType);
        Role teamTakedownRole = RoleService.createRole(setup, "TeamTakedowner", TeamTakedown.abilityType);
        Faction mafia = setup.getFactionByColor(Setup.MAFIA_C);
        FactionRole infiltrator = FactionRoleService.createFactionRole(mafia, infiltratorRole);
        FactionRole teamTakedowner = FactionRoleService.createFactionRole(mafia, teamTakedownRole);
        tryBadSelfTarget(BasicRoles.Amnesiac(), BasicRoles.Assassin(), BasicRoles.Bomb(), BasicRoles.Bulletproof(),
                BasicRoles.Commuter(), BasicRoles.Commuter(), BasicRoles.Coward(), BasicRoles.Coroner(),
                BasicRoles.Cultist(), BasicRoles.Doctor(), BasicRoles.Executioner(), BasicRoles.Ghost(),
                BasicRoles.GraveDigger(), infiltrator, BasicRoles.Jailor(), BasicRoles.Marshall(), BasicRoles.Mason(),
                BasicRoles.Mayor(), BasicRoles.Miller(), BasicRoles.Survivor(), teamTakedowner, BasicRoles.Veteran(),
                BasicRoles.Vigilante());

        tryBadSelfTarget(Burn.abilityType, BasicRoles.Arsonist());
    }

    private void tryBadSelfTarget(FactionRole... members) {
        AbilityType toEdit;
        for(FactionRole factionRole: members){
            toEdit = RoleTestUtil.getFirstAbilityType(factionRole.role);
            tryBadSelfTarget(toEdit, factionRole);
        }
    }

    private void tryBadSelfTarget(AbilityType toEdit, FactionRole factionRole) {
        try{
            if(LookupUtil.findFactionRole(setup, factionRole.role.getName(), factionRole.getColor()) == null)
                addPlayer(factionRole);

            modifyAbilitySelfTarget(factionRole, toEdit, true);
            fail(toEdit + " was able to be edited, but shouldn't be.");
        }catch(NarratorException e){
        }
        try{
            modifyAbilitySelfTarget(factionRole, toEdit, false);
            fail();
        }catch(NarratorException e){
        }
    }

    public void testBadModifierTypes() {
        try{
            RoleModifierService.upsertModifier(BasicRoles.Doctor().role, RoleModifierName.AUTO_VEST, true);
            fail();
        }catch(NarratorException e){
        }

        try{
            RoleModifierService.upsertModifier(BasicRoles.Doctor().role, RoleModifierName.UNDETECTABLE, 1);
            fail();
        }catch(NarratorException e){
        }

        try{
            RoleAbilityModifierService.upsert(BasicRoles.Doctor().role, Doctor.abilityType, AbilityModifierName.CHARGES,
                    true);
            fail();
        }catch(NarratorException e){
        }

        try{
            RoleAbilityModifierService.upsert(BasicRoles.Doctor().role, Doctor.abilityType,
                    AbilityModifierName.SELF_TARGET, 3);
            fail();
        }catch(NarratorException e){
        }

        try{
            SetupModifierService.upsertModifier(setup, SetupModifierName.DAY_START, 1);
            fail();
        }catch(NarratorException e){
        }

        try{
            GameService.upsertModifier(game, GameModifierName.DAY_LENGTH_MIN, true);
            fail();
        }catch(NarratorException e){
        }

        try{
            GameService.upsertModifier(game, GameModifierName.DAY_LENGTH_START, true);
            fail();
        }catch(NarratorException e){
        }

        try{
            GameService.upsertModifier(game, GameModifierName.DAY_LENGTH_DECREASE, true);
            fail();
        }catch(NarratorException e){
        }

        try{
            FactionModifierService.upsertModifier(setup.getFactionByColor(Setup.TOWN_C),
                    FactionModifierName.KNOWS_ALLIES, 3);
            fail();
        }catch(NarratorException e){
        }

        try{
            FactionModifierService.upsertModifier(setup.getFactionByColor(Setup.TOWN_C),
                    FactionModifierName.WIN_PRIORITY, true);
            fail();
        }catch(NarratorException e){
        }

        Faction mafia = setup.getFactionByColor(Setup.MAFIA_C);
        try{
            FactionAbilityModifierService.upsertModifier(mafia, FactionKill.abilityType, AbilityModifierName.COOLDOWN,
                    true);
            fail();
        }catch(NarratorException e){
        }

        try{
            FactionAbilityModifierService.upsertModifier(mafia, FactionKill.abilityType,
                    AbilityModifierName.ZERO_WEIGHTED, 3);
            fail();
        }catch(NarratorException e){
        }
    }

    public void testHiddenFlip() {
        Controller flipper = addPlayer(BasicRoles.Citizen());
        Controller sk = addPlayer(BasicRoles.SerialKiller());
        addPlayer(BasicRoles.Sheriff(), 2);

        modifyRole(BasicRoles.Citizen(), RoleModifierName.HIDDEN_FLIP, true);

        setTarget(sk, flipper);

        endNight();

        assertEquals(Optional.empty(), flipper.getPlayer().getGraveyardColor());
        assertEquals(Optional.empty(), flipper.getPlayer().getGraveyardRoleName());
    }

    public void testHiddenRoleFlipOnElimination() {
        Controller flipper = addPlayer(BasicRoles.Citizen());
        Controller sk = addPlayer(BasicRoles.SerialKiller());
        Controller sheriff1 = addPlayer(BasicRoles.Sheriff());
        Controller sheriff2 = addPlayer(BasicRoles.Sheriff());

        RoleModifierService.upsertModifier(BasicRoles.Citizen().role, RoleModifierName.HIDDEN_FLIP, true);

        dayStart();

        assertTrue(flipper.getPlayer().gameRole.modifiers.getBoolean(RoleModifierName.HIDDEN_FLIP));

        voteOut(flipper, sk, sheriff1, sheriff2);

        assertEquals(Optional.empty(), flipper.getPlayer().getGraveyardColor());
        assertEquals(Optional.empty(), flipper.getPlayer().getGraveyardRoleName());
    }

    public void testModifiersAreIntOrBool() {
        for(SetupModifierName setupModifier: SetupModifierName.values())
            assertTrue(ModifierUtil.IsIntModifier(setupModifier) || ModifierUtil.IsBoolModifier(setupModifier)
                    || ModifierUtil.IsStringModifier(setupModifier));
        for(AbilityModifierName abilityModifier: AbilityModifierName.values())
            assertTrue(ModifierUtil.IsIntModifier(abilityModifier) || ModifierUtil.IsBoolModifier(abilityModifier)
                    || ModifierUtil.IsStringModifier(abilityModifier));
        for(FactionModifierName factionModifier: FactionModifierName.values())
            assertTrue(ModifierUtil.IsIntModifier(factionModifier) || ModifierUtil.IsBoolModifier(factionModifier));
        for(RoleModifierName roleModifier: RoleModifierName.values())
            assertTrue(ModifierUtil.IsIntModifier(roleModifier) || ModifierUtil.IsBoolModifier(roleModifier));
    }
}
