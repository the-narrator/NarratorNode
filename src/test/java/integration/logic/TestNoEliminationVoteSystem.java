package integration.logic;

import game.ai.Controller;
import game.event.Message;
import game.logic.support.Constants;
import game.logic.templates.BasicRoles;
import models.enums.GameModifierName;
import models.enums.SetupModifierName;
import models.enums.VoteSystemTypes;
import services.GameService;
import services.SetupModifierService;
import util.TestVoteMessageUtil;

public class TestNoEliminationVoteSystem extends SuperTest {

    Controller goon1;
    Controller goon2;
    Controller citizen1;
    Controller citizen2;
    Controller citizen3;

    public TestNoEliminationVoteSystem(String name) {
        super(name);
    }

    @Override
    public void roleInit() {
        GameService.upsertModifier(game, GameModifierName.VOTE_SYSTEM, VoteSystemTypes.NO_ELIMINATION.value);
        goon1 = addPlayer(BasicRoles.Goon());
        goon2 = addPlayer(BasicRoles.Goon());
        citizen1 = addPlayer(BasicRoles.Citizen());
        citizen2 = addPlayer(BasicRoles.Citizen());
        citizen3 = addPlayer(BasicRoles.Citizen());

        SuperTest.BrainEndGame = false;
    }

    public void testEndDayNoVotes() {
        dayStart();
        endDay();

        assertIsNight();
    }

    public void testEveryoneVotingSkiDay() {
        skipVote(goon1, goon2, citizen1, citizen2, citizen3);
        assertIsDay();

        endDay();

        assertIsNight();
    }

    public void testEveryoneVotingOnePerson() {
        SetupModifierService.upsertModifier(setup, SetupModifierName.SELF_VOTE, true);
        voteOut(goon1, goon1, goon2, citizen1, citizen2, citizen3);
        assertIsDay();

        endDay();

        assertIsNight();
        isAlive(goon1);
    }

    public void testTie() {
        vote(goon2, goon1);
        vote(goon1, goon2);

        TestVoteMessageUtil.assertLastVoteText(goon1.getName() + " voted for " + goon2.getName() + ".", Message.PUBLIC);
        assertIsDay();

        endDay();

        assertIsNight();
        isAlive(goon1, goon2);
    }

    public void testChangeFromModkilledPlayer() {
        vote(goon1, goon2);
        goon2.getPlayer().modkill(Constants.ALL_TIME_LEFT);

        isDead(goon2);

        // this used to fail
        vote(goon1, citizen1);
    }
}
