package integration.logic;

import game.ai.Controller;
import game.event.EventLog;
import game.event.EventManager;
import game.event.VoidChat;
import game.logic.support.Constants;
import game.logic.templates.BasicRoles;
import models.enums.GamePhase;

public class TestEvent extends SuperTest {

    public TestEvent(String name) {
        super(name);
    }

    public void testChatNamesAndSizes() {
        Controller esc = addPlayer(BasicRoles.Escort());
        addPlayer(BasicRoles.Witch());
        addPlayer(BasicRoles.Arsonist());

        nightStart();

        EventManager em = game.getEventManager();
        // Header nZero = (Header) em.accessNightChats(VoidChat.KEY, 0,
        // false).events.get(0);

        assertTrue(em.getNightLog(VoidChat.KEY).isActive());

        endNight();
        // assertEquals(0, nZero.getDay());
        // assertEquals("Night 0", nZero.getName());
        // assertTrue(em.getDayChat(1).isActive());

        // day 1
        assertChatLogSize(2, esc);

        skipDay();

        // night 1
        assertChatLogSize(2, esc);

        endNight();

        // day 2. should have nighttime, day1, day 2
        assertChatLogSize(2, esc);
    }

    public void testOnGameOverDay() {
        Controller baker = addPlayer(BasicRoles.Baker());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller chauf = addPlayer(BasicRoles.Chauffeur());

        nightStart();

        endNight();

        assertChatLogSize(2, baker);

        voteOut(chauf, cit, baker);

        // day, night, dead, mafia
        assertChatLogSize(4, baker);

        assertEquals("Night 0", game.getEventManager().getNightLog(Constants.VOID_CHAT).getName());
        for(EventLog el: baker.getPlayer().getChats()){
            if(el instanceof VoidChat){
                assertEquals("Night 0", el.getName());
            }
        }

        assertTrue(game.getEventManager().getDayChat().isActive());
    }

    public void testOnGameOverNight() {
        Controller baker = addPlayer(BasicRoles.Baker());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller chauf = addPlayer(BasicRoles.Chauffeur());

        voteOut(cit, chauf, baker);

        mafKill(chauf, baker);
        endNight();

        assertEquals(GamePhase.FINISHED, game.phase);
        assertEquals(2, game.getDayNumber());
        // day, night, dead, mafia
        assertChatLogSize(4, baker);

        assertEquals("Night 1", game.getEventManager().getNightLog(Constants.VOID_CHAT).getName());
        for(EventLog el: baker.getPlayer().getChats()){
            if(el instanceof VoidChat)
                assertEquals("Night 1", el.getName());
        }

        assertTrue(game.getEventManager().getDayChat().isActive());
    }

    public void testWinMessageOnDayGameEnd() {
        Controller p1 = addPlayer(BasicRoles.Citizen());
        Controller p2 = addPlayer(BasicRoles.Citizen());
        Controller p3 = addPlayer(BasicRoles.Goon());

        dayStart();
        skipDay();
        endNight();

        vote(p1, p3);
        vote(p2, p3);
        assertNotNull(game.getWinMessage());
        assertTrue(game.getHappenings().contains("won"));
    }

    public void testWinMessageOnNightGameEnd() {
        Controller p1 = addPlayer(BasicRoles.Citizen());
        Controller p3 = addPlayer(BasicRoles.Goon());
        addPlayer(BasicRoles.Citizen());

        nightStart();
        mafKill(p3, p1);
        endNight();

        assertNotNull(game.getWinMessage());
        assertTrue(game.getHappenings().contains("won"));
    }
}
