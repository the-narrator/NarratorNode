package integration.logic;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import game.abilities.Armorsmith;
import game.abilities.Bulletproof;
import game.abilities.Burn;
import game.abilities.Doctor;
import game.abilities.Driver;
import game.abilities.FactionSend;
import game.abilities.GameAbility;
import game.abilities.Gunsmith;
import game.abilities.Witch;
import game.ai.Controller;
import game.logic.Player;
import game.logic.exceptions.NarratorException;
import game.logic.templates.BasicRoles;
import game.setups.Setup;
import models.Faction;
import models.FactionRole;
import models.Role;
import models.enums.AbilityModifierName;
import models.enums.RoleModifierName;
import services.FactionRoleService;
import services.RoleModifierService;
import services.RoleService;
import util.TestChatUtil;
import util.models.FactionRoleTestUtil;

public class TestModifierText extends SuperTest {

    public TestModifierText(String name) {
        super(name);
    }

    public void testModifierText() {
        Controller arson = addPlayer(BasicRoles.Arsonist());
        addPlayer(BasicRoles.BusDriver());
        addPlayer(BasicRoles.Commuter());
        addPlayer(BasicRoles.Vigilante());
        addPlayer(BasicRoles.Ghost());

        dayStart();

        List<String> arsonRoleSpecs = ((Player) arson).getRoleCardDetails();
        partialExcludes(arsonRoleSpecs, "target yourself with the burn ability");
        partialExcludes(arsonRoleSpecs, Bulletproof.NIGHT_ACTION_DESCRIPTION);
    }

    public void testWitchSelfTargeting() {
        Faction mafia = setup.getFactionByColor(Setup.MAFIA_C);
        Role role = RoleService.createRole(setup, "WitchDoctor", Witch.abilityType, Doctor.abilityType);
        FactionRole witchDoctor = FactionRoleService.createFactionRole(mafia, role);

        Controller mash = addPlayer(witchDoctor);
        Controller witch = addPlayer(BasicRoles.Witch());
        addPlayer(BasicRoles.Citizen(), 2);

        modifyAbilitySelfTarget(BasicRoles.Witch(), false);
        modifyAbilitySelfTarget(witchDoctor, Witch.abilityType, false);

        nightStart();

        GameAbility roleCard = witch.getPlayer().getFirstAbility();
        assertEquals(false, roleCard.modifiers.getBoolean(AbilityModifierName.SELF_TARGET, game));

        List<String> specs = roleCard.getProfileRoleCardDetails(witch.getPlayer());
        assertContains(specs, "You may not cause self-targets.");

        ArrayList<String> rTexts = getPublicDescription(BasicRoles.Witch());
        assertContains(rTexts, "May not cause self-targets.");

        specs = mash.getPlayer().getRoleCardDetails();
        assertContains(specs, "You may not use your Witch ability to cause self-targets.");

        rTexts = getPublicDescription(mash.getPlayer().initialAssignedRole.assignedRole);
        assertContains(rTexts, "May not cause self-targets with the Witch ability.");

        try{
            witch(witch, mash, mash);
            fail();
        }catch(NarratorException e){
        }
    }

    public void testBusDriverModifierText() {
        Controller bd = addPlayer(BasicRoles.BusDriver());
        addPlayer(BasicRoles.Goon(), 2);
        // selftarget is defaulting to true
//        modifyAbilitySelfTarget(BasicRoles.BusDriver(), true);

        nightStart();

        Driver roleCard = bd.getPlayer().getAbility(Driver.class);
        assertEquals(null, roleCard.getProfileSelfTargetText(bd.getPlayer()));

        ArrayList<String> rTexts = getPublicDescription(BasicRoles.BusDriver());
        assertFalse(rTexts.contains("May not target themselves."));
    }

    public void testRoleModTextWithOutOfRangePlCount() {
        FactionRole factionRole = BasicRoles.Agent();
        int minRange = 4;
        for(int i = 0; i < minRange - 1; i++)
            this.addPlayer();
        RoleModifierService.upsertModifier(factionRole.role, RoleModifierName.AUTO_VEST, minRange,
                Setup.MAX_PLAYER_COUNT, Setup.MAX_PLAYER_COUNT);

        List<String> publicDescription = factionRole.getPublicDescription(Optional.of(game));
        for(String blurb: publicDescription){
            assertFalse(blurb.contains("auto vest"));
        }
    }

    public void testSelfTargetGSWarning() {
        Controller witch = addPlayer(BasicRoles.Witch());
        Controller gs = addPlayer(BasicRoles.Gunsmith());
        Controller bd = addPlayer(BasicRoles.BusDriver());

        modifyAbilitySelfTarget(BasicRoles.Gunsmith(), true);
        modifyAbilitySelfTarget(BasicRoles.BusDriver(), false);

        nightStart();

        Player bdPlayer = bd.getPlayer();
        Driver roleCard = bdPlayer.getAbility(Driver.class);
        assertFalse(roleCard.modifiers.getBoolean(AbilityModifierName.SELF_TARGET, game));
        assertEquals("You may not target yourself.", roleCard.getProfileSelfTargetText(bdPlayer));

        ArrayList<String> rTexts = getPublicDescription(BasicRoles.BusDriver());
        assertContains(rTexts, "May not target themselves.");

        witch(witch, gs, gs);
        nextNight();

        assertTotalGunCount(1, gs);
        try{
            drive(bd, gs, bd);
            fail();
        }catch(NarratorException e){
        }
        assertFalse(bdPlayer.getAcceptableTargets(Driver.abilityType).contains(bdPlayer));

        witch(witch, gs, gs);
        setTarget(gs, gs, Gunsmith.abilityType);

        TestMafiaTeam.gotWarning(gs);
        TestChatUtil.resetWarnings();

        nextNight();

        assertTotalGunCount(2, gs);

        setTarget(gs, gs, Gunsmith.abilityType);
        TestMafiaTeam.gotWarning(gs);
        endNight();

        assertTotalGunCount(3, gs);
    }

    public void testSelfTargetingBasic() {
        Faction town = setup.getFactionByColor(Setup.TOWN_C);
        Role role = RoleService.createRole(setup, "Gashmitch", Gunsmith.abilityType, Armorsmith.abilityType);
        FactionRole mash = FactionRoleService.createFactionRole(town, role);

        Controller gs = addPlayer(BasicRoles.Gunsmith());
        Controller gsbs = addPlayer(mash);
        addPlayer(BasicRoles.Arsonist());

        modifyAbilitySelfTarget(BasicRoles.Gunsmith(), true);
        modifyAbilitySelfTarget(mash, Armorsmith.abilityType, true);

        try{
            modifyAbilitySelfTarget(BasicRoles.Arsonist(), Burn.abilityType, true);
            fail();
        }catch(NarratorException e){
        }

        try{
            modifyTeamAbility(Setup.MAFIA_C, AbilityModifierName.SELF_TARGET, FactionSend.abilityType, false);
            fail();
        }catch(NarratorException e){
        }

        nightStart();

        List<String> gsRoleSpecs = gs.getPlayer().getRoleCardDetails();
        assertContains(gsRoleSpecs, "You may target yourself.");

        ArrayList<String> gsRuleTexts = getPublicDescription(BasicRoles.Gunsmith());
        assertContains(gsRuleTexts, "May target themselves.");

        List<String> gsBsRoleSpecs = gsbs.getPlayer().getRoleCardDetails();
        assertContains(gsBsRoleSpecs, "You may target yourself with the armorsmith ability.");

        ArrayList<String> gsBsRuleTexts = getPublicDescription(gsbs.getPlayer().initialAssignedRole.assignedRole);
        assertContains(gsBsRuleTexts, "May target themselves with the armorsmith ability.");

        List<String> arsRoleSpecs = gsbs.getPlayer().getRoleCardDetails();
        partialExcludes(arsRoleSpecs, "You may not target yourself with the burn ability.");

        ArrayList<String> arsRuleTexts = getPublicDescription(mash);
        partialExcludes(arsRuleTexts, "May target themselves with the douse ability.");
        partialExcludes(arsRuleTexts, "May target themselves with the burn ability.");
        partialExcludes(arsRuleTexts, "May target themselves with the undouse ability.");

        setTarget(gs, gs);
        endNight();

        assertRealGunCount(1, gs);
    }

    private static ArrayList<String> getPublicDescription(FactionRole factionRole) {
        return FactionRoleTestUtil.getPublicDescription(factionRole);
    }

}
