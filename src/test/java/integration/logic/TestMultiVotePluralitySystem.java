package integration.logic;

import game.abilities.Puppet;
import game.abilities.Ventriloquist;
import game.abilities.Vote;
import game.ai.Controller;
import game.event.EventList;
import game.event.Message;
import game.event.VoteAnnouncement;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.exceptions.NarratorException;
import game.logic.support.CommandHandler;
import game.logic.support.Constants;
import game.logic.support.action.Action;
import game.logic.templates.BasicRoles;
import models.enums.GameModifierName;
import models.enums.GamePhase;
import models.enums.SetupModifierName;
import models.enums.VoteSystemTypes;
import services.ActionService;
import services.GameService;
import services.SetupModifierService;
import util.TestVoteMessageUtil;

public class TestMultiVotePluralitySystem extends SuperTest {

    Controller goon1;
    Controller goon2;
    Controller citizen1;
    Controller citizen2;
    Controller citizen3;

    @Override
    public void roleInit() {
        GameService.upsertModifier(game, GameModifierName.VOTE_SYSTEM, VoteSystemTypes.MULTI_VOTE_PLURALITY.value);
        goon1 = addPlayer(BasicRoles.Goon());
        goon2 = addPlayer(BasicRoles.Goon());
        citizen1 = addPlayer(BasicRoles.Citizen());
        citizen2 = addPlayer(BasicRoles.Citizen());
        citizen3 = addPlayer(BasicRoles.Citizen());
    }

    public TestMultiVotePluralitySystem(String name) {
        super(name);
    }

    public void testSubmitMoreThanOneVote() {
        vote(goon1, goon2, citizen1, citizen2);
        vote(citizen2, goon2, goon1);

        assertDayRemaining(1);

        endDay();

        isDead(goon2);
    }

    public void testNoSubmittingNonSkipVotesIfBlackmailed() {
        Controller blackmailer = addPlayer(BasicRoles.Blackmailer());
        addPlayer(BasicRoles.Citizen(), 2); // stave off parity

        setTarget(blackmailer, citizen1);
        endNight();

        skipVote(goon2);

        try{
            vote(citizen1, game.skipper, citizen2);
            fail();
        }catch(NarratorException e){
        }
    }

    public void testBadVotes() {
        try{
            vote(goon1, goon2, goon2);
            fail();
        }catch(NarratorException e){
        }

        try{
            vote(goon1);
            fail();
        }catch(NarratorException e){
        }
    }

    public void testVoteCount() {
        vote(goon1, goon2, citizen1, citizen2, citizen3);
        vote(goon2, goon1, citizen1, citizen2, citizen3);
        vote(citizen1, goon1, goon2, citizen2, citizen3);
        vote(citizen2, goon1, goon2, citizen1, citizen3);
        vote(citizen3, goon1, goon2, citizen1, citizen2);

        assertVoteCount(4, goon1);
        assertVoteCount(4, goon2);
        assertVoteCount(4, citizen1);
        assertVoteCount(4, citizen2);
        assertVoteCount(4, citizen3);

        endPhase();

        assertIsDay();

        unvote(goon2);

        isDead(goon2);
        assertIsNight();
    }

    public void testNoSelfVote() {
        SetupModifierService.upsertModifier(setup, SetupModifierName.SELF_VOTE, false);

        try{
            vote(goon1, goon1, goon2, citizen3);
            fail();
        }catch(NarratorException e){

        }
    }

    public void testSelfTarget() {
        SetupModifierService.upsertModifier(setup, SetupModifierName.SELF_VOTE, true);
        vote(goon1, goon1, goon2, citizen3);

        assertVoteCount(1, goon1);
        assertEquals(1, goon1.getPlayer().getActions().size());
    }

    public void testMayor() {
        Controller mayor = addPlayer(BasicRoles.Mayor());
        SetupModifierService.upsertModifier(setup, SetupModifierName.MAYOR_VOTE_POWER, game.players.size());

        vote(mayor, goon1, goon2);
        reveal(mayor);

        assertVoteCount(game.players.size() + 1, goon1);
        assertIsDay();
    }

    public void testMayorNoEndingDayEarly() {
        Controller mayor = addPlayer(BasicRoles.Mayor());
        SetupModifierService.upsertModifier(setup, SetupModifierName.MAYOR_VOTE_POWER, game.players.size());

        reveal(mayor);
        vote(mayor, goon1);

        assertIsDay();
    }

    public void testMarshall() {
        Controller marshall = addPlayer(BasicRoles.Marshall());

        order(marshall);
        vote(marshall, goon1, goon2);
        vote(goon1, goon2);
        endDay();

        assertIsDay();
        assertVoteCount(1, goon1);
    }

    public void testTieVoteStalling() {
        vote(goon1, goon2, citizen1);
        vote(citizen3, goon2, citizen1);
        endDay();

        assertIsDay();

        unvote(goon1);

        assertIsDay();

        vote(goon1, goon2);

        assertIsNight();
        isDead(goon2);
    }

    public void testNoSkipping() {
        SetupModifierService.upsertModifier(setup, SetupModifierName.SKIP_VOTE, false);

        try{
            vote(goon1, game.skipper);
            fail();
        }catch(NarratorException e){
        }
    }

    public void testSkipDay() {
        vote(goon1, game.skipper);
        vote(goon2, goon1, game.skipper);
        vote(citizen1, goon1, game.skipper, goon2);
        endDay();

        assertIsNight();
        assertEmpty(game.getDeadPlayers());
    }

    public void testUnvote() {
        vote(goon1, citizen1, citizen2, citizen3);
        unvote(goon1);

        assertVoteCount(0, citizen1);
        assertVoteCount(0, citizen2);
        assertVoteCount(0, citizen3);
    }

    public void testVentriloquist() {
        Controller ventriloquist = addPlayer(BasicRoles.Ventriloquist());

        setTarget(ventriloquist, citizen1, Ventriloquist.abilityType);
        endNight();

        ventVote(ventriloquist, citizen1, citizen2, goon2, game.skipper);

        assertVoteCount(1, citizen2);
        assertVoteCount(1, goon2);
        assertVoteCount(1, game.skipper);
    }

    public void testVoteText() {
        vote(goon1, citizen1);

        assertLastVoteText(goon1.getName() + " voted for " + citizen1.getName() + ".");
    }

    public void testUnvoteTextSingle() {
        vote(goon1, citizen1);
        unvote(goon1);

        assertLastVoteText(goon1.getName() + " unvoted " + citizen1.getName() + ".");
    }

    public void testUnvoteTextDouble() {
        vote(goon1, citizen1, citizen2);
        unvote(goon1);

        assertLastVoteText(goon1.getName() + " unvoted " + citizen1.getName() + " and " + citizen2.getName() + ".");
    }

    public void testUnvoteTextTriple() {
        vote(goon1, citizen1, citizen2, citizen3);
        unvote(goon1);

        assertLastVoteText(goon1.getName() + " unvoted " + citizen1.getName() + ", " + citizen2.getName() + ", and "
                + citizen3.getName() + ".");
    }

    public void testSingleSkipVote() {
        vote(goon1, game.skipper);

        assertLastVoteText(goon1.getName() + " voted for eliminating no one.");
        assertLastVoteText(goon1.getName() + " voted for eliminating no one.", goon2.getPlayer().getID());
        assertLastVoteText("You voted for eliminating no one.", goon1.getPlayer().getID());
    }

    public void testVoteAndSkipVote() {
        vote(goon1, game.skipper, goon2);

        assertLastVoteText(goon1.getName() + " voted for eliminating no one and " + goon2.getName() + ".");
    }

    public void testChangeSingleVote() {
        vote(goon1, citizen1);
        vote(goon1, citizen2);

        assertLastVoteText(
                goon1.getName() + " changed their vote from " + citizen1.getName() + " to " + citizen2.getName() + ".");
        assertLastVoteText("You changed your vote from " + citizen1.getName() + " to " + citizen2.getName() + ".",
                goon1.getPlayer().getID());
        assertLastVoteText(goon1.getName() + " changed their vote from you to " + citizen2.getName() + ".",
                citizen1.getPlayer().getID());
        assertLastVoteText(goon1.getName() + " changed their vote from " + citizen1.getName() + " to you.",
                citizen2.getPlayer().getID());
    }

    public void testChangeMultiVote() {
        vote(goon1, citizen1, goon2);
        vote(goon1, citizen2, citizen3);

        assertLastVoteText(goon1.getName() + " changed their vote from " + citizen1.getName() + " and "
                + goon2.getName() + " to " + citizen2.getName() + " and " + citizen3.getName() + ".");

    }

    public void testUnvoteSome() {
        vote(goon1, citizen1, citizen2, citizen3);
        vote(goon1, citizen1, citizen2);

        assertLastVoteText(goon1.getName() + " unvoted " + citizen3.getName() + ".");
        assertLastVoteText("You unvoted " + citizen3.getName() + ".", goon1.getPlayer().getID());
        assertLastVoteText(goon1.getName() + " unvoted " + citizen3.getName() + ".", citizen2.getPlayer().getID());
        assertLastVoteText(goon1.getName() + " unvoted you.", citizen3.getPlayer().getID());
    }

    public void testSameVoteNoFail() {
        vote(goon1, citizen1, citizen2);
        vote(goon1, citizen1, citizen2);
    }

    public void testReplaceVoteWithVote() {
        vote(citizen1, citizen2);

        Action action = Vote.getAction(citizen1, game, goon1);
        ActionService.replaceAction(action, 0);

        assertVoteCount(0, citizen2);
        assertVoteCount(1, goon1);
    }

    public void testReplaceVoteWithPuppetVote() {
        Controller ventriloquist = addPlayer(BasicRoles.Ventriloquist());

        setTarget(ventriloquist, goon1, Ventriloquist.abilityType);
        endNight();
        vote(ventriloquist, citizen2);

        Action action = Puppet.getAction(ventriloquist, Vote.getAction(goon1, game, goon2));
        ActionService.replaceAction(action, 0);

        assertVoteCount(0, citizen2);
        assertVoteCount(0, goon1);
        assertVoteCount(1, goon2);
    }

    public void testReplacePuppetVoteWithPuppetVote() {
        Controller ventriloquist = addPlayer(BasicRoles.Ventriloquist());

        setTarget(ventriloquist, goon1, Ventriloquist.abilityType);
        endNight();
        ventVote(ventriloquist, goon1, citizen2);

        Action action = Puppet.getAction(ventriloquist, Vote.getAction(goon1, game, ventriloquist));
        ActionService.replaceAction(action, 0);

        assertVoteCount(0, citizen2);
        assertVoteCount(0, goon1);
        assertVoteCount(1, ventriloquist);
    }

    public void testReplacePuppetVoteWithVote() {
        Controller ventriloquist = addPlayer(BasicRoles.Ventriloquist());

        setTarget(ventriloquist, goon1, Ventriloquist.abilityType);
        endNight();
        ventVote(ventriloquist, goon1, citizen2);

        Action action = Vote.getAction(ventriloquist, game, goon1);
        ActionService.replaceAction(action, 0);

        assertVoteCount(0, citizen2);
        assertVoteCount(1, goon1);
    }

    public void testLastVoteDuringTrial() {
        turnOnTrial();

        vote(0.99, goon2, citizen1);

        assertDayRemaining(0.99);

        vote(0.1, goon1, citizen1);

        assertLastVoteText(goon1.getName() + " voted for " + citizen1.getPlayer().getName() + ".");
        assertVoteAnnouncementCount(2);
    }

    public void testNoVotingTextDuringTrial() {
        turnOnTrial();

        vote(0.1, goon1, citizen1);

        assertLastVoteText(goon1.getName() + " voted for " + citizen1.getPlayer().getName() + ".");
        assertIsOnTrial(citizen1);

        unvote(goon1);
        vote(goon1, citizen2);

        assertIsOnTrial(citizen1);
        assertLastVoteText(goon1.getName() + " voted for " + citizen1.getPlayer().getName() + ".");

        endPhase(0.1);

        assertIsOnTrial(citizen2);
        assertLastVoteText(goon1.getName() + " changed their vote from " + citizen1.getPlayer().getName() + " to "
                + citizen2.getPlayer().getName() + ".");
    }

    public void testNoUnvoteTextDuringTrial() {
        turnOnTrial();
        String voteText = goon1.getName() + " voted for " + citizen1.getPlayer().getName() + ".";

        vote(0.1, goon1, citizen1);

        assertLastVoteText(voteText);
        assertIsOnTrial(citizen1);

        unvote(goon1);

        assertIsOnTrial(citizen1);
        assertLastVoteText(voteText);
        assertVoteTextCount(voteText, 1);

        endPhase(0.1);

        assertOnVotePhase();
        assertDayRemaining(0.1);
        assertLastVoteText(goon1.getName() + " unvoted " + citizen1.getPlayer().getName() + ".");
    }

    public void testAssertActionsMidTrial() {
        turnOnTrial();

        vote(0.99, citizen1, goon1);
        vote(0.1, citizen2, goon1);

        assertIsOnTrial(goon1);

        vote(0.99, citizen1, goon1, game.skipper);
        vote(0.99, citizen1, goon1);
        Action action = citizen1.getPlayer().getActions().getFirst();

        assertEquals(1, action.getTargets().size());
        assertTrue(action.getTargets().contains(goon1.getPlayer()));
    }

    public void testTrialIfAllVoting() {
        turnOnTrial();

        vote(0.99, citizen1, goon1);
        vote(0.99, goon2, goon1);
        vote(0.99, citizen3, goon1);
        vote(0.99, citizen2, goon1);

        assertIsOnTrial(goon1);

        endPhase(0.99);

        isDead(goon1);
    }

    public void testTrialNotEnoughTimeLeft() {
        turnOnTrial();

        vote(0.50, citizen1, goon1);
        vote(0.50, citizen2, goon1);

        assertOnVotePhase();
        assertDayRemaining(0.5);
    }

    public void testTimeLeftEliminationAfterTrial() {
        turnOnTrial();

        vote(0.51, citizen1, goon1);
        vote(0.51, citizen2, goon1);

        assertOnVotePhase();

        vote(0.51, citizen3, goon1);

        assertIsOnTrial(goon1);
        assertDayRemaining(1);

        endPhase(0.51);

        isDead(goon1);
    }

    public void testSkipIfAllVoting() {
        turnOnTrial();
        Player skipper = game.skipper;

        vote(citizen1, skipper);
        vote(goon2, skipper);
        vote(citizen3, skipper);
        vote(citizen2, skipper);
        vote(0.99, goon1, skipper);

        assertIsNight();
        assertDayRemaining(1);
    }

    public void testUnvoteTriggeringTrial() {
        turnOnTrial();
        vote(0.50, citizen1, goon1);
        vote(0.50, citizen2, goon2);

        assertOnVotePhase();

        unvote(0.1, citizen1);

        assertIsOnTrial(goon2);
        assertEquals(0, citizen1.getPlayer().getActions().size());
    }

    public void testResetVotesAfterSkipDay() {
        turnOnTrial();
        vote(0.01, citizen1, game.skipper);

        assertIsNight();

        endNight();

        assertVoteCount(0, game.skipper);
    }

    public void testTimeLeftResetsAfterSecondDay() {
        turnOnTrial();
        vote(0.02, citizen1, goon1);

        endPhase(0.01);

        assertIsNight();

        endNight();

        assertDayRemaining(1);
    }

    public void testUnvoteChangesTimeLeft() {
        vote(0.99, citizen1, goon1);
        unvote(0.8, citizen1);

        assertDayRemaining(0.8);
    }

    public void testCheckVoteCommand() {
        turnOnTrial();

        vote(0.50, citizen1, goon1);

        assertDayRemaining(0.5);
        assertOnVotePhase();

        checkVote(0.1);

        assertIsOnTrial(goon1);

        unvote(citizen1);
        endPhase();

        assertDayRemaining(0.1);
    }

    public void testVotingDuringTrial() {
        turnOnTrial();

        vote(0.49, citizen1, goon1);

        assertDayRemaining(0.49);

        vote(0.49, citizen2, goon1);

        assertIsOnTrial(goon1);

        unvote(citizen1);

        assertVoteCount(2, goon1);
        assertIsOnTrial(goon1);

        unvote(citizen2);
        endPhase(0.48);

        assertOnVotePhase();
        assertDayRemaining(0.49);
        assertVoteCount(0, goon1);
    }

    public void testGetOffTrialWithLessVotes() {
        turnOnTrial();

        vote(0.51, citizen1, goon1);
        vote(0.51, citizen2, goon1);
        vote(0.51, citizen3, goon1);

        assertIsOnTrial(goon1);

        unvote(citizen1);
        endPhase(0.51);

        assertOnVotePhase();
    }

    public void testVotingWhileOnTrial() {
        turnOnTrial();

        vote(0.1, citizen1, goon1);

        assertIsOnTrial(goon1);

        vote(0.1, goon1, citizen2);

        endPhase(0.1);

        assertOnVotePhase();
        assertDayRemaining(0.1);
    }

    public void testAutoUnvoteModkilledPlayers() {
        vote(goon1, citizen1);

        citizen1.getPlayer().modkill(1);

        assertEmpty(game.voteSystem.getVoteTargets(goon1.getPlayer()));
    }

    private static void assertDayRemaining(double timeLeft) {
        assertEquals(timeLeft, game.timeLeft);
    }

    private static void checkVote(double timeLeft) {
        CommandHandler ch = new CommandHandler(game);
        ch.command(null, Constants.CHECK_VOTE, "", null, false, timeLeft);
    }

    private static void vote(double timeLeft, Controller voterC, Controller... votedC) {
        if(!game.isStarted())
            dayStart();
        Player voter = voterC.getPlayer();
        Action action = Vote.getAction(voter.getPlayer(), timeLeft, game, votedC);
        voter.doDayAction(action);
    }

    private static void unvote(double timeLeft, Controller unvoterC) {
        unvoterC.cancelAction(0, timeLeft);
    }

    private static void endPhase(double timeLeft) {
        game.endPhase(timeLeft);
    }

    private static void assertIsOnTrial(Controller controller) {
        PlayerList trialed = game.voteSystem.getPlayersOnTrial();
        assertEquals(game.phase, GamePhase.TRIAL_PHASE);
        assertEquals(1, trialed.size());
        assertEquals(controller.getPlayer(), trialed.getFirst());
    }

    private static void assertOnVotePhase() {
        Player trialed = game.voteSystem.getPlayerOnTrial();
        assertEquals(GamePhase.VOTE_PHASE, game.phase);
        assertEquals(null, trialed);
    }

    private static void turnOnTrial() {
        GameService.upsertModifier(game, GameModifierName.TRIAL_LENGTH, 1);
    }

    public void assertLastVoteText(String text) {
        assertLastVoteText(text, Message.PUBLIC);
    }

    public void assertVoteTextCount(String text, int count) {
        String happenings = game.getHappenings();
        while (happenings.contains(text)){
            count--;
            happenings = happenings.replaceFirst(text, "");
        }
        assertEquals(0, count);
    }

    public static void assertLastVoteText(String text, String access) {
        TestVoteMessageUtil.assertLastVoteText(text, access);
    }

    public void assertVoteAnnouncementCount(int expectedCount) {
        EventList events = game.getEventManager().getEventLog(Constants.DAY_CHAT).getEvents();
        int count = 0;
        for(Message message: events){
            if(message instanceof VoteAnnouncement)
                count++;
        }
        assertEquals(expectedCount, count);
    }
}
