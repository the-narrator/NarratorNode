package integration.logic.abilities;

import game.abilities.CultLeader;
import game.abilities.Infiltrator;
import game.abilities.MasonLeader;
import game.abilities.Sheriff;
import game.abilities.support.CultInvitation;
import game.ai.Controller;
import game.event.ChatMessage;
import game.logic.exceptions.NarratorException;
import game.logic.exceptions.PlayerTargetingException;
import game.logic.support.Constants;
import game.logic.support.action.Action;
import game.logic.templates.BasicRoles;
import game.setups.Setup;
import integration.logic.SuperTest;
import models.Faction;
import models.Role;
import models.enums.AbilityModifierName;
import models.enums.SetupModifierName;
import services.FactionRoleService;
import services.RoleAbilityModifierService;
import services.RoleService;
import util.models.FactionRoleTestUtil;
import util.models.RoleTestUtil;

public class TestInfiltrator extends SuperTest {

    public TestInfiltrator(String s) {
        super(s);
    }

    ChatMessage cm;
    Controller infil, maf, cLeader, cultist;

    @Override
    public void roleInit() {
        Faction mafia = setup.getFactionByColor(Setup.MAFIA_C);
        Role role = RoleService.createRole(setup, Infiltrator.ROLE_NAME, Infiltrator.abilityType);
        infil = addPlayer(FactionRoleService.createFactionRole(mafia, role));
        maf = addPlayer(BasicRoles.Goon());
        cultist = addPlayer(BasicRoles.Cultist());
        cLeader = addPlayer(BasicRoles.CultLeader());
    }

    private void nightTalking() {
        nightStart();
        say(cLeader, "whatup", cultist.getColor());
    }

    private void infilStatTest() {
        endNight();

        assertEquals(maf.getColor(), infil.getColor());
        assertTrue(infil.getPlayer().getFactions().contains(cLeader.getPlayer().getGameFaction()));
        assertTrue(infil.is(Infiltrator.abilityType));
    }

    public void testBasic() {
        nightTalking();

        setTarget(cLeader, infil);

        infilStatTest();
    }

    public void testPromotion() {
        editRule(SetupModifierName.CULT_PROMOTION, true);
        editRule(SetupModifierName.CONVERT_REFUSABLE, false);
        editRule(SetupModifierName.CULT_KEEPS_ROLES, false);
        FactionRoleTestUtil.addCultRole(BasicRoles.CultLeader(), BasicRoles.Sheriff());

        nightTalking();

        setTarget(CultLeader.getUpgradeAction(cLeader, infil, Sheriff.class.getSimpleName(), game));

        infilStatTest();
    }

    public void testRefusal() {
        editRule(SetupModifierName.CULT_PROMOTION, false);
        editRule(SetupModifierName.CONVERT_REFUSABLE, true);
        editRule(SetupModifierName.CULT_KEEPS_ROLES, true);

        addPlayer(BasicRoles.Citizen());

        nightTalking();

        setTarget(cLeader, infil);

        infilStatTest();

        assertFalse(infil.getPlayer().hasDayAction(CultInvitation.abilityType));

        try{
            infil.doDayAction(
                    new Action(infil.getPlayer(), CultInvitation.abilityType, Constants.ACCEPT, (String) null));
            fail();
        }catch(PlayerTargetingException e){
        }
    }

    public void testClubber() {
        Controller clubber = addPlayer(BasicRoles.Clubber());
        nightTalking();

        setTarget(cLeader, infil);
        infilStatTest();

        skipDay();
        setTarget(clubber, infil);
        endNight();

        isAlive(infil);
    }

    public void testCultMason() {
        Role role = BasicRoles.CultLeader().role;
        Faction faction = setup.getFactionByColor(Setup.YAKUZA_C);

        Controller mLeader = addPlayer(BasicRoles.MasonLeader());
        Controller cLeader2 = addPlayer(FactionRoleService.createFactionRole(faction, role));

        nightTalking();
        setTarget(cLeader, infil);
        setTarget(cLeader2, infil);
        setTarget(mLeader, infil);

        endNight();

        assertEquals(4, infil.getPlayer().getFactions().size());

        skipDay();

        assertContains(infil.getChatKeys(), infil.getColor(), cLeader.getColor(), cLeader2.getColor(),
                mLeader.getColor());
    }

    public void testCultThenMasonThenCult() {
        Role role = BasicRoles.CultLeader().role;
        Faction faction = setup.getFactionByColor(Setup.YAKUZA_C);

        Controller mLeader = addPlayer(BasicRoles.MasonLeader());
        Controller cLeader2 = addPlayer(FactionRoleService.createFactionRole(faction, role));
        Controller disguiser = new TestDisguiser("").neutDisguiser();

        editRule(SetupModifierName.MASON_PROMOTION, true);

        setTarget(cLeader, infil);
        nextNight();

        isAlive(mLeader);

        setTarget(mLeader, infil);
        nextNight();

        assertFalse(infil.is(MasonLeader.abilityType));
        isAlive(mLeader);

        setTarget(cLeader2, infil);
        endNight();

        assertEquals(4, infil.getPlayer().getFactions().size());

        skipDay();

        assertContains(infil.getChatKeys(), infil.getColor(), cLeader.getColor(), mLeader.getColor());

        setTarget(disguiser, infil);
        nextNight();

        assertContains(disguiser.getChatKeys(), infil.getColor(), cLeader.getColor(), mLeader.getColor());

        isDead(infil);
    }

    public void testAttainKill() {
        Role infiltratorRole = RoleTestUtil.getRole(setup, "Infiltrator");
        Role cultLeaderRole = BasicRoles.CultLeader().role;
        Faction yakuza = setup.getFactionByColor(Setup.YAKUZA_C);
        Faction threat = setup.getFactionByColor(Setup.THREAT_C);

        Controller cLeader2 = addPlayer(FactionRoleService.createFactionRole(yakuza, cultLeaderRole));
        Controller infil2 = addPlayer(FactionRoleService.createFactionRole(threat, infiltratorRole));

        setTarget(cLeader2, infil2);
        nextNight();

        mafKill(infil2, infil);
        endNight();

        isDead(infil);
    }

    public void testNoBackToBackModifier() {
        try{
            Role role = RoleTestUtil.getRole(setup, Infiltrator.ROLE_NAME);
            RoleAbilityModifierService.upsert(role, Infiltrator.abilityType, AbilityModifierName.BACK_TO_BACK, false);
            fail();
        }catch(NarratorException e){
        }
    }

    public void testNoZeroWeightModifier() {
        try{
            Role role = RoleTestUtil.getRole(setup, Infiltrator.ROLE_NAME);
            RoleAbilityModifierService.upsert(role, Infiltrator.abilityType, AbilityModifierName.ZERO_WEIGHTED, false);
            fail();
        }catch(NarratorException e){
        }
    }
}
