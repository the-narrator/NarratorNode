package integration.logic.abilities;

import game.abilities.Blackmailer;
import game.abilities.Burn;
import game.abilities.Douse;
import game.abilities.DrugDealer;
import game.abilities.Framer;
import game.abilities.GameAbility;
import game.abilities.GraveDigger;
import game.abilities.Block;
import game.abilities.Undouse;
import game.abilities.Veteran;
import game.abilities.Vigilante;
import game.abilities.Visit;
import game.abilities.support.Gun;
import game.ai.Controller;
import game.ai.ControllerList;
import game.logic.exceptions.PlayerTargetingException;
import game.logic.support.action.Action;
import game.logic.templates.BasicRoles;
import game.setups.Setup;
import integration.logic.SuperTest;
import models.Faction;
import models.FactionRole;
import models.Role;
import models.enums.AbilityModifierName;
import models.enums.SetupModifierName;
import services.FactionRoleService;
import util.CollectionUtil;
import util.Util;
import util.models.ActionTestUtil;

public class TestGraveDigger extends SuperTest {

    public TestGraveDigger(String s) {
        super(s);
    }

    Controller gd;

    @Override
    public void roleInit() {
        gd = addPlayer(BasicRoles.GraveDigger());
    }

    private void basicDig(Controller... target) {
        String command;
        if(target[0].getPlayer().getRoleAbilities().getNightAbilities(target[0].getPlayer()).isEmpty())
            command = null;
        else{
            GameAbility a = target[0].getPlayer().getRoleAbilities().getNightAbilities(target[0].getPlayer()).getFirst();
            if(GraveDigger.isBannedAction(a.getAbilityType()))
                command = null;
            else
                command = a.getCommand();
        }
        setTarget(new Action(gd.getPlayer(), GraveDigger.abilityType, Util.toStringList(command),
                ControllerList.ToPlayerList(game, target)));
    }

    public void testGraveDiggerDeadSK() {
        Controller sk = addPlayer(BasicRoles.SerialKiller());
        Controller doc = addPlayer(BasicRoles.Doctor());
        Controller cit = addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Citizen());

        voteOut(sk, cit, doc, gd);
        isDead(sk);

        try{
            setTarget(sk, cit);
            fail();
        }catch(PlayerTargetingException e){
        }

        basicDig(sk, cit);

        endNight();

        try{
            setTarget(sk, cit);
        }catch(PlayerTargetingException e){
        }

        isDead(cit);
    }

    public void testGraveDiggerDocSave() {
        Controller sk = addPlayer(BasicRoles.SerialKiller());
        Controller doc = addPlayer(BasicRoles.Doctor());
        addPlayer(BasicRoles.Citizen());

        nightStart();
        try{
            setTarget(gd, doc);
            fail();
        }catch(PlayerTargetingException e){
        }
        setTarget(sk, doc);

        nextNight();

        basicDig(doc, gd);
        setTarget(sk, gd);

        endNight();

        isAlive(sk);
    }

    public void testGunsmithGivingAlliesGuns() {
        Role role = BasicRoles.Gunsmith().role;
        Faction mafia = setup.getFactionByColor(Setup.MAFIA_C);

        Controller gs = addPlayer(FactionRoleService.createFactionRole(mafia, role));
        Controller maf = addPlayer(BasicRoles.Goon());
        Controller cit = addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Citizen());

        modifyRole(BasicRoles.Gunsmith().role, AbilityModifierName.GS_FAULTY_GUNS, false);

        dayStart();
        voteOut(gs, maf, gd, cit);

        basicDig(gs, maf);

        endNight();

        assertTotalGunCount(1, maf);
    }

    public void testGunsmithGivingAlliesGunsNoTeamKnowing() {
        Role role = BasicRoles.Gunsmith().role;
        Faction mafia = setup.getFactionByColor(Setup.OUTCAST_C);

        Controller gs = addPlayer(FactionRoleService.createFactionRole(mafia, role));
        Controller maf = addPlayer(BasicRoles.Agent());
        Controller cit = addPlayer(BasicRoles.Citizen());

        modifyRole(BasicRoles.Gunsmith().role, AbilityModifierName.GS_FAULTY_GUNS, false);

        voteOut(gs, maf, gd, cit);

        isDead(gs);

        basicDig(gs, maf);

        endNight();

        assertTotalGunCount(1, maf);
    }

    public void testGraveDiggerVigilanteLimitedShots() {
        modifyRole(BasicRoles.Vigilante(), AbilityModifierName.CHARGES, 1);

        Controller vigi = addPlayer(BasicRoles.Vigilante());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller doc = addPlayer(BasicRoles.Doctor());
        Controller lookout = addPlayer(BasicRoles.Lookout());

        editRule(SetupModifierName.CHARGE_VARIABILITY, 0);

        dayStart();
        voteOut(vigi, cit, gd, doc);

        setTarget(gd, GraveDigger.abilityType, Gun.COMMAND, vigi, cit);

        endNight();

        isDead(cit);

        assertTotalGunCount(0, vigi);

        skipDay();

        setTarget(gd, GraveDigger.abilityType, Gun.COMMAND, vigi, doc);
        setTarget(lookout, doc);

        endNight();
        TestLookout.seen(lookout, vigi);
        isAlive(doc);
    }

    public void testGraveDiggerCitizen() {
        Controller digger = gd;
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller lookout = addPlayer(BasicRoles.Lookout());
        Controller maf = addPlayer(BasicRoles.Goon());

        dayStart();

        voteOut(cit, digger, lookout, maf);

        basicDig(cit, maf);
        setTarget(lookout, maf);

        endNight();

        TestLookout.seen(lookout, cit);
    }

    public void testGraveDiggingArson() {
        Controller arson = addPlayer(BasicRoles.Arsonist());
        Controller arson2 = addPlayer(BasicRoles.Arsonist());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller doc = addPlayer(BasicRoles.Doctor());
        Controller bd = addPlayer(BasicRoles.BusDriver());

        dayStart();

        voteOut(arson, gd, cit, doc, bd);

        setTarget(gd, GraveDigger.abilityType, Douse.COMMAND, arson, cit);
        nextNight();

        assertStatus(cit, Douse.abilityType);

        setTarget(gd, GraveDigger.abilityType, Undouse.COMMAND, arson, cit);
        setTarget(arson2, doc);

        nextNight();

        setTarget(gd, GraveDigger.abilityType, Burn.COMMAND, arson);

        endNight();

        isDead(doc);
        isAlive(cit);
    }

    public void testDiggingUpVeteranKillingAll() {
        Controller amnes = addPlayer(BasicRoles.Amnesiac());
        Controller veteran = addPlayer(BasicRoles.Veteran());
        Controller coroner = addPlayer(BasicRoles.Coroner());

        dayStart();
        voteOut(veteran, coroner, gd, amnes);

        setTarget(amnes, veteran);
        setTarget(coroner, veteran);
        basicDig(veteran, amnes);

        endNight();

        isDead(amnes, coroner, gd);
        assertGameOver();
    }

    public void testDiggingVeteranVisitation() {
        modifyRole(BasicRoles.Veteran(), AbilityModifierName.CHARGES, 1);

        Controller veteran = addPlayer(BasicRoles.Veteran());
        Controller lookout = addPlayer(BasicRoles.Lookout());
        Controller cit = addPlayer(BasicRoles.Citizen());

        editRule(SetupModifierName.CHARGE_VARIABILITY, 0);

        alert(veteran);
        endNight();

        assertPerceivedChargeRemaining(0, veteran, Veteran.abilityType);

        voteOut(veteran, lookout, gd, cit);

        basicDig(veteran, cit);
        setTarget(lookout, cit);
        endNight();

        isAlive(gd);
        TestLookout.seen(lookout, veteran);

    }

    public void testDiggerBusDriver() {
        Controller sk = addPlayer(BasicRoles.SerialKiller());
        Controller bd = addPlayer(BasicRoles.BusDriver());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller cit2 = addPlayer(BasicRoles.Citizen());

        voteOut(bd, cit, gd, sk);

        basicDig(bd, cit, cit2);
        setTarget(sk, cit);

        endNight();

        isAlive(cit);
        isDead(cit2);
    }

    public void testDiggingGraveDigger() {
        Controller digger1 = addPlayer(BasicRoles.GraveDigger());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller lookout = addPlayer(BasicRoles.Lookout());
        Controller bd = addPlayer(BasicRoles.BusDriver());

        voteOut(digger1, gd, cit, bd);

        basicDig(digger1, cit);
        setTarget(lookout, cit);

        endNight();

        TestLookout.seen(lookout, digger1);
    }

    public void testNoVisitCorpses() {
        Controller amn = addPlayer(BasicRoles.Amnesiac());
        Controller witch = addPlayer(BasicRoles.Witch());
        Controller agent_killed = addPlayer(BasicRoles.Agent());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller mayor = addPlayer(BasicRoles.Mayor());
        Controller lookout = addPlayer(BasicRoles.Lookout());

        voteOut(amn, agent_killed, cit, mayor, witch);

        basicDig(amn, gd);
        setTarget(lookout, gd);
        endNight();
        TestLookout.seen(lookout, amn);

        voteOut(agent_killed, cit, mayor, gd, witch);

        basicDig(agent_killed, gd);
        setTarget(lookout, gd);
        endNight();
        TestLookout.seen(lookout, agent_killed);

        voteOut(cit, mayor, gd, witch);

        basicDig(cit, gd);
        setTarget(lookout, gd);
        endNight();
        TestLookout.seen(lookout, cit);

        voteOut(mayor, gd, witch, lookout);

        basicDig(mayor, gd);
        setTarget(lookout, gd);
        endNight();
        TestLookout.seen(lookout, mayor);

    }

    public void testBreadedDigger() {
        Controller digger = gd;
        Controller vigi = addPlayer(BasicRoles.Vigilante());
        Controller doc = addPlayer(BasicRoles.Doctor());
        Controller doc2 = addPlayer(BasicRoles.Doctor());
        Controller detect = addPlayer(BasicRoles.Detective());
        Controller lookout = addPlayer(BasicRoles.Lookout());
        Controller baker2 = addPlayer(BasicRoles.Baker());
        Controller baker = addPlayer(BasicRoles.Baker());

        modifyRole(BasicRoles.Vigilante(), AbilityModifierName.CHARGES, 2);
        editRule(SetupModifierName.CHARGE_VARIABILITY, 0);

        nightStart();

        assertTotalGunCount(2, vigi);

        setTarget(baker, digger);
        setTarget(baker2, digger);
        endNight();

        voteOut(vigi, lookout, baker, doc, detect, baker2);

        isDead(vigi);

        corpseShoot(digger, vigi, doc);
        corpseShoot(digger, vigi, doc2);
        corpseShoot(digger, vigi, lookout);
        setTarget(lookout, lookout);

        assertTotalGunCount(2, vigi);

        endNight();

        isDead(doc, doc2);
        isAlive(lookout);
        TestLookout.seen(lookout, vigi);
    }

    public void testSpyCorpse() {
        Controller spy = addPlayer(BasicRoles.Spy());
        Controller digger = gd;
        Controller lookout = addPlayer(BasicRoles.Lookout());
        Controller witch = addPlayer(BasicRoles.Witch());

        voteOut(spy, digger, lookout, witch);

        basicDig(spy, witch);
        setTarget(lookout, witch);
        endNight();

        TestLookout.seen(lookout, spy);
    }

    public void testSpyCorpseCH() {
        Controller digger = gd;
        Controller spy = addPlayer(BasicRoles.Spy());
        Controller lookout = addPlayer(BasicRoles.Lookout());
        Controller witch = addPlayer(BasicRoles.Witch());

        voteOut(spy, digger, lookout, witch);

        diggerTarget(digger, Visit.COMMAND, spy, witch);
        setTarget(lookout, witch);
        endNight();

        TestLookout.seen(lookout, spy);
    }

    public void testConflictingSubmissions() {
        modifyRole(BasicRoles.Vigilante(), AbilityModifierName.CHARGES, 1);

        Controller digger1 = gd;
        Controller digger2 = addPlayer(BasicRoles.GraveDigger());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller lookout = addPlayer(BasicRoles.Lookout());
        Controller vigi = addPlayer(BasicRoles.Vigilante());

        editRule(SetupModifierName.CHARGE_VARIABILITY, 0);

        voteOut(vigi, digger1, digger2, cit);

        setTarget(lookout, digger2);
        corpseShoot(digger1, vigi, digger2);
        corpseShoot(digger2, vigi, digger1);
        endNight(digger2, digger1);
        endNight();

        isDead(digger1);
        isAlive(digger2);
        TestLookout.seen(lookout, vigi);
    }

    private void corpseShoot(Controller digger, Controller shooter, Controller target) {
        setTarget(ActionTestUtil.getGraveDiggerAction(digger, Vigilante.getAction(shooter, target, game)));
    }

    public void testConflictingSubmissions2() {
        modifyRole(BasicRoles.Vigilante(), AbilityModifierName.CHARGES, 1);

        Controller digger1 = gd;
        Controller digger2 = addPlayer(BasicRoles.GraveDigger());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller lookout = addPlayer(BasicRoles.Lookout());
        Controller vigi = addPlayer(BasicRoles.Vigilante());

        editRule(SetupModifierName.CHARGE_VARIABILITY, 0);

        voteOut(vigi, digger1, digger2, cit);

        setTarget(lookout, digger1);
        corpseShoot(digger1, vigi, digger2);
        corpseShoot(digger2, vigi, digger1);
        endNight(digger1, digger2);
        endNight();

        isDead(digger2);
        isAlive(digger1);
        TestLookout.seen(lookout, vigi);
    }

    public void testCorpseFramer() {
        Controller framer = addPlayer(BasicRoles.Framer());
        Controller digger = gd;
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller sheriff = addPlayer(BasicRoles.Sheriff());

        voteOut(framer, digger, cit, sheriff);

        setTarget(sheriff, cit);
        setTarget(ActionTestUtil.getGraveDiggerAction(digger, Framer.getAction(framer, cit, framer.getColor(), game)));
        endNight();

        TestSheriff.seen(sheriff, framer.getColor());
    }

    // commands dont get logged by the dead people
    // this shouldn't happen because i'm literally adding the action itself.

    public void testCorpseFramerCH() {
        Controller framer = addPlayer(BasicRoles.Framer());
        Controller digger = gd;
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller sheriff = addPlayer(BasicRoles.Sheriff());

        voteOut(framer, digger, cit, sheriff);
        setTarget(sheriff, cit);

        diggerTarget(digger, Framer.COMMAND, framer.getColor(), framer, cit);
        endNight();

        TestSheriff.seen(sheriff, framer.getColor());
    }

    public void testCorpseDrugCH() {
        Controller digger = gd;
        Controller dealer = addPlayer(BasicRoles.DrugDealer());
        Controller stripper = addPlayer(BasicRoles.Escort());
        Controller sheriff = addPlayer(BasicRoles.Sheriff());

        voteOut(dealer, digger, stripper, sheriff);
        setTarget(sheriff, stripper);

        diggerTarget(digger, DrugDealer.COMMAND, DrugDealer.BLOCKED, dealer, sheriff);
        endNight();

        partialContains(sheriff, Block.FEEDBACK);
    }

    public void testNotUsingTeamAbilityAndBannedActions() {
        Controller goon = addPlayer(BasicRoles.Goon());
        addPlayer(BasicRoles.Goon());
        Controller vet = addPlayer(BasicRoles.Veteran());
        Controller cit2 = addPlayer(BasicRoles.Citizen());

        addTeamAbility(Setup.MAFIA_C, Block.abilityType);

        voteOut(goon, vet, cit2, gd);

        for(FactionRole factionRole: setup.getFactionRolesWithAbility(Blackmailer.abilityType, Block.abilityType))
            FactionRoleService.deleteFactionRole(factionRole);

        try{
            // there are no blackmailers in this game, so shouldn't be able to use it.
            diggerTarget(gd, Blackmailer.COMMAND, goon, vet);
            fail();
        }catch(PlayerTargetingException e){
        }

        try{
            diggerTarget(gd, GraveDigger.COMMAND, goon);
            fail();
        }catch(PlayerTargetingException e){
        }

        try{
            diggerTarget(gd, Block.COMMAND, goon, vet);
            fail();
        }catch(PlayerTargetingException e){
        }
    }

    public void testCantTargetCorpseAgain() {
        Controller stripper = addPlayer(BasicRoles.Escort());
        Controller vigi = addPlayer(BasicRoles.Vigilante());
        Controller cit = addPlayer(BasicRoles.Citizen());

        editRule(SetupModifierName.GD_REANIMATE, false);

        voteOut(stripper, vigi, cit, gd);

        basicDig(stripper, vigi);
        shoot(vigi, gd);
        nextNight();

        isAlive(gd);

        try{
            basicDig(stripper, vigi);
            fail();
        }catch(PlayerTargetingException e){
        }
    }

    public void diggerTarget(Controller gDigger, String option, Controller... players) {
        Action gdAction = new Action(gDigger.getPlayer(), GraveDigger.abilityType, CollectionUtil.toArrayList(option),
                ControllerList.list(players).toPlayerList(game));
        setTarget(gdAction);
    }

    public void diggerTarget(Controller gDigger, String option, String option2, Controller... players) {
        Action gdAction = new Action(gDigger.getPlayer(), GraveDigger.abilityType, option, option2,
                ControllerList.list(players).toPlayerList(game));
        setTarget(gdAction);
    }
}
