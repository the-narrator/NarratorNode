package integration.logic.abilities;

import game.abilities.Detective;
import game.abilities.Sleepwalker;
import game.ai.Controller;
import game.logic.Game;
import game.logic.exceptions.NarratorException;
import game.logic.support.RoleAssigner;
import game.logic.templates.BasicRoles;
import integration.logic.SuperTest;
import junit.framework.AssertionFailedError;
import models.FactionRole;
import models.enums.AbilityModifierName;
import models.enums.SetupModifierName;
import services.FactionRoleModifierService;
import services.GameService;
import services.RoleAbilityModifierService;
import services.SetupModifierService;
import util.TestUtil;

public class TestSleepwalker extends SuperTest {

    public TestSleepwalker(String s) {
        super(s);
    }

    public void testSleepwalkerBasic() {
        Controller sw = addPlayer();
        Controller detect = addPlayer();
        Controller witch = addPlayer();
        Controller cit = addPlayer();

        RoleAssigner ra = TestUtil.assignRoles(game, new Controller[] { sw, detect, witch, cit }, new FactionRole[] {
                BasicRoles.Sleepwalker(), BasicRoles.Detective(), BasicRoles.Witch(), BasicRoles.Citizen() });

        SetupModifierService.upsertModifier(setup, SetupModifierName.DAY_START, Game.NIGHT_START);
        GameService.internalStart(game, ra);

        setTarget(detect, sw);
        endNight();

        assertTrue(sw.getPlayer().getActions().isEmpty());

        printHappenings = false;
        try{
            TestDetective.seen(detect, sw);
            return;
        }catch(AssertionFailedError e){
        }
        try{
            TestDetective.seen(detect, cit);
            return;
        }catch(AssertionFailedError e){
        }
        try{
            TestDetective.seen(detect, witch);
            return;
        }catch(AssertionFailedError e){
        }
        printHappenings = true;
        TestDetective.seen(detect, detect);

    }

    public void testElectroVest() {
        Controller sw = addPlayer();
        Controller aSmith = addPlayer();
        Controller cit = addPlayer();
        Controller cult = addPlayer();
        Controller em = addPlayer();
        Controller em2 = addPlayer();
        Controller escort = addPlayer();

        RoleAssigner roleAssigner = TestUtil.assignRoles(game,
                new Controller[] { sw, aSmith, cit, cult, em, em2, escort },
                new FactionRole[] { BasicRoles.Sleepwalker(), BasicRoles.Armorsmith(), BasicRoles.Citizen(),
                        BasicRoles.Cultist(), BasicRoles.ElectroManiac(), BasicRoles.ElectroManiac(),
                        BasicRoles.Escort() });

        RoleAbilityModifierService.upsert(BasicRoles.Sleepwalker().role, Sleepwalker.abilityType,
                AbilityModifierName.SELF_TARGET, true);
        FactionRoleModifierService.addModifier(BasicRoles.Sleepwalker(), AbilityModifierName.SELF_TARGET,
                Sleepwalker.abilityType, false);
        SetupModifierService.upsertModifier(setup, SetupModifierName.DAY_START, Game.NIGHT_START);
        GameService.internalStart(game, roleAssigner);

        assertTrue(sw.is(Sleepwalker.abilityType));

        setTarget(aSmith, sw);
        nextNight();

        electrify(em, aSmith, cit, cult);
        setTarget(em, sw);
        setTarget(em2, escort);
        setTarget(escort, sw);

        nextNight();

        isCharged(sw, aSmith, cit, cult, escort);

        vest(sw);
        endNight();

        assertIsDay();
        assertEquals(1, game.getDeadSize());
    }

    public void testFramerRandomElement() {
        Controller detect = addPlayer();
        Controller fodder = addPlayer();
        Controller sw = addPlayer();
        Controller framer = addPlayer();
        Controller maf = addPlayer();

        RoleAssigner ra = TestUtil.assignRoles(game, new Controller[] { detect, fodder, sw, framer, maf },
                new FactionRole[] { BasicRoles.Detective(), BasicRoles.Citizen(), BasicRoles.Sleepwalker(),
                        BasicRoles.Framer(), BasicRoles.Goon() });

        editRule(SetupModifierName.FOLLOW_GETS_ALL, false);
        SetupModifierService.upsertModifier(setup, SetupModifierName.DAY_START, Game.NIGHT_START);
        GameService.internalStart(game, ra);

        mafKill(maf, fodder);
        frame(framer, sw, maf.getColor());
        setTarget(detect, sw);

        endNight();

        TestDetective.seen(detect, fodder);
    }

    public void testSwGunsmith() {
        Controller gs = addPlayer();
        Controller detect = addPlayer();
        Controller sw = addPlayer();
        Controller citizen = addPlayer();
        Controller framer = addPlayer();

        RoleAssigner ra = TestUtil.assignRoles(game, new Controller[] { gs, detect, sw, citizen, framer },
                new FactionRole[] { BasicRoles.Gunsmith(), BasicRoles.Detective(), BasicRoles.Sleepwalker(),
                        BasicRoles.Citizen(), BasicRoles.Framer() });

        editRule(SetupModifierName.FOLLOW_GETS_ALL, true);

        SetupModifierService.upsertModifier(setup, SetupModifierName.DAY_START, Game.NIGHT_START);
        GameService.internalStart(game, ra);

        setTarget(gs, sw);

        nextNight();

        shoot(sw, gs);
        setTarget(detect, sw);

        endNight();

        TestDetective.seen(detect, gs);
    }

    public void testSwWitch() {
        Controller witch = addPlayer(BasicRoles.Witch());
        Controller detect = addPlayer(BasicRoles.Detective());
        Controller sw = addPlayer(BasicRoles.Sleepwalker());
        Controller cit1 = addPlayer(BasicRoles.Citizen());
        Controller cit2 = addPlayer(BasicRoles.Citizen());
        Controller cit3 = addPlayer(BasicRoles.Citizen());

        TestUtil.assignRoles(game, new Controller[] { witch, detect, sw, cit1, cit2, cit3 },
                new FactionRole[] { BasicRoles.Witch(), BasicRoles.Detective(), BasicRoles.Sleepwalker(),
                        BasicRoles.Citizen(), BasicRoles.Citizen(), BasicRoles.Citizen() });

        setTarget(detect, sw);
        witch(witch, sw, detect);

        endNight();

        TestDetective.seen(detect, detect);
    }

    public void testSleepWalkerVesting() {
        Controller as = addPlayer();
        Controller sw = addPlayer();
        Controller agent = addPlayer();
        Controller cit = addPlayer();

        RoleAssigner ra = TestUtil.assignRoles(game, new Controller[] { as, sw, agent, cit }, new FactionRole[] {
                BasicRoles.Armorsmith(), BasicRoles.Sleepwalker(), BasicRoles.Agent(), BasicRoles.Citizen() });

        SetupModifierService.upsertModifier(setup, SetupModifierName.DAY_START, Game.NIGHT_START);
        GameService.internalStart(game, ra);

        setTarget(as, sw);
        nextNight();

        setTarget(agent, sw);
        vest(sw);
        nextNight();

        partialExcludes(agent, Detective.NO_VISIT);
    }

    public void testNoBackToBackModifier() {
        try{
            FactionRoleModifierService.addModifier(BasicRoles.Sleepwalker(), AbilityModifierName.BACK_TO_BACK,
                    Sleepwalker.abilityType, false);
            fail();
        }catch(NarratorException e){
        }
    }

    public void testNoZeroWeightModifier() {
        try{
            FactionRoleModifierService.addModifier(BasicRoles.Sleepwalker(), AbilityModifierName.ZERO_WEIGHTED,
                    Sleepwalker.abilityType, false);
            fail();
        }catch(NarratorException e){
        }
    }
}
