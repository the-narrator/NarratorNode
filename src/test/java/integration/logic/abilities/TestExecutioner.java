package integration.logic.abilities;

import java.util.Set;

import game.abilities.Bulletproof;
import game.abilities.Executioner;
import game.abilities.Jester;
import game.abilities.SerialKiller;
import game.ai.Controller;
import game.ai.ControllerList;
import game.logic.exceptions.NarratorException;
import game.logic.support.Random;
import game.logic.templates.BasicRoles;
import game.setups.Setup;
import integration.logic.SuperTest;
import models.Faction;
import models.FactionRole;
import models.Role;
import models.enums.AbilityModifierName;
import models.enums.SetupModifierName;
import models.modifiers.Modifiers;
import services.FactionRoleModifierService;
import services.FactionRoleService;
import services.RoleAbilityModifierService;
import services.RoleService;
import services.SetupModifierService;
import util.TestUtil;
import util.game.LookupUtil;
import util.models.AbilityTestUtil;
import util.models.ModifierTestUtil;

public class TestExecutioner extends SuperTest {

    public TestExecutioner(String name) {
        super(name);
    }

    public void testWin() {
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller maf = addPlayer(BasicRoles.Goon());
        Controller exec = addPlayer(BasicRoles.Executioner());

        dayStart();

        SetExecTarget(exec, cit);

        voteOut(cit, exec, maf);

        isWinner(maf, exec);
    }

    public static void SetExecTarget(Controller exec, Controller target) {
        Executioner.SetTarget(exec.getPlayer(), target.getPlayer());

        skipTearDown = true;
    }

    /*
     * public void testUnconvertableAfterWin(){ Controller exec =
     * addPlayer(BasicRoles.Executioner()); Controller cit =
     * addPlayer(BasicRoles.Citizen()); Controller maf =
     * addPlayer(BasicRoles.Mafioso()); Controller cultLeader =
     * addPlayer(BasicRoles.CultLeader());
     *
     * n.getRandom().queuePlayer(cit);
     *
     * dayStart(); assertEquals(((Executioner) exec.getRole()).getTarget(exec),
     * cit);
     *
     * lynch(cit, exec, maf, cultLeader); setTarget(cultLeader, exec);
     *
     * endNight(); assertTrue(exec.getRole().isWinner(exec, n));
     * assertEquals(Constants.A_CULT, exec.getColor()); }
     */

    public void testLoss() {
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller maf = addPlayer(BasicRoles.Goon());
        Controller exec = addPlayer(BasicRoles.Executioner());

        dayStart();

        SetExecTarget(exec.getPlayer(), maf.getPlayer());

        voteOut(cit, exec, maf);

        isWinner(maf);
        isLoser(exec);
    }

    public void testExecDeathLossP3() {
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller maf = addPlayer(BasicRoles.Goon());
        Controller exec = addPlayer(BasicRoles.Executioner());

        nightStart();

        SetExecTarget(exec, cit);

        setTarget(maf, exec, KILL);

        endNight();

        isLoser(exec);
    }

    public void testExecDeathLossP4() {
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller maf = addPlayer(BasicRoles.Goon());
        Controller exec = addPlayer(BasicRoles.Executioner());
        Controller cit2 = addPlayer(BasicRoles.Citizen());

        nightStart();

        SetExecTarget(exec, maf);
        mafKill(maf, exec);
        endNight();

        voteOut(maf, cit, cit2);

        assertGameOver();
        isLoser(exec);
    }

    public void testTargetDeathLoss() {
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller maf = addPlayer(BasicRoles.Goon());
        Controller exec = addPlayer(BasicRoles.Executioner());

        nightStart();
        SetExecTarget(exec, cit);

        setTarget(maf, cit, KILL);

        endNight();

        isLoser(exec);
    }

    public void testPermanentInvul() {
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller exec = addPlayer(BasicRoles.Executioner());
        Controller sk = addPlayer(BasicRoles.SerialKiller());
        TestUtil.addAbility(BasicRoles.Executioner().role, Bulletproof.abilityType);

        nightStart();
        SetExecTarget(exec, cit);

        setTarget(sk, exec);
        endNight();

        assertInProgress();
    }

    public void testInvulFailure() {
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller exec = addPlayer(BasicRoles.Executioner());
        Controller sk = addPlayer(BasicRoles.SerialKiller());

        editRule(SetupModifierName.EXECUTIONER_WIN_IMMUNE, true);

        nightStart();
        SetExecTarget(exec, cit);

        setTarget(sk, exec);
        endNight();

        assertGameOver();
    }

    public void testInvulSuccess() {
        Controller cit2 = addPlayer(BasicRoles.Citizen());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller exec = addPlayer(BasicRoles.Executioner());
        Controller sk = addPlayer(BasicRoles.SerialKiller());

        editRule(SetupModifierName.EXECUTIONER_WIN_IMMUNE, true);

        dayStart();
        SetExecTarget(exec, cit);

        voteOut(cit, cit2, exec, sk);

        setTarget(sk, exec);

        endNight();

        assertGameOver();
    }

    public void testExecutionerDemotionWithoutJester() {
        Set<Role> roles = LookupUtil.findRolesWithAbility(setup, Jester.abilityType);
        for(Role role: roles)
            RoleService.delete(role);

        addPlayer(BasicRoles.Executioner());
        addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Goon());

        assertBadGameSettings();
    }

    public void testNoExecutionerDemotionWithoutJester() {
        Set<Role> roles = LookupUtil.findRolesWithAbility(setup, Jester.abilityType);
        for(Role role: roles)
            RoleService.delete(role);

        SetupModifierService.upsertModifier(setup, SetupModifierName.EXECUTIONER_TO_JESTER, false);

        addPlayer(BasicRoles.Executioner());
        addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Goon());

        dayStart();
    }

    public void testExecToJester() {
        Controller cit2 = addPlayer(BasicRoles.Citizen());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller exec = addPlayer(BasicRoles.Executioner());
        Controller sk = addPlayer(BasicRoles.SerialKiller());
        Faction benignFaction = BasicRoles.Jester().faction;

        editRule(SetupModifierName.EXECUTIONER_TO_JESTER, true);

        nightStart();
        SetExecTarget(exec, cit);

        setTarget(sk, cit);
        endNight();

        Set<FactionRole> jesterRoles = benignFaction.getPossibleFactionRolesWithAbility(game, Jester.abilityType);
        assertFalse(jesterRoles.isEmpty());
        assertTrue(exec.is(Jester.abilityType));
        assertFalse(exec.is(Executioner.abilityType));

        voteOut(exec, cit2, sk);

        endNight();

        isWinner(exec);
    }

    public void testTownOnlyTarget() {
        Controller cit1 = addPlayer(BasicRoles.Citizen());
        Controller cit2 = addPlayer(BasicRoles.Citizen());
        Controller cit3 = addPlayer(BasicRoles.Citizen());
        Controller exec = addPlayer(BasicRoles.Executioner());
        addPlayer(BasicRoles.Arsonist());
        addPlayer(BasicRoles.Godfather());
        addPlayer(BasicRoles.Agent());

        editRule(SetupModifierName.EXEC_TOWN, true);

        long seed = new Random().nextLong();
        // seed = Long.parseLong("-8495726696867851616");

        System.out.println("Exec town test - " + seed);

        game.setSeed(seed);

        dayStart();
        assertEquals(3, game.getFaction(Setup.TOWN_C)._startingSize);
        assertEquals(1, game.getFaction(Setup.BENIGN_C)._startingSize);
        assertEquals(2, game.getFaction(Setup.MAFIA_C)._startingSize);

        int original = game.getEventManager().dayChat.getEvents().events.get(0).getDay();

        Controller target = getTarget(exec);
        assertTrue(target == cit1 || target == cit2 || target == cit3);

        skipDay();
        nextNight();

        assertEqual(original, game.getEventManager().dayChat.getEvents().events.get(0).getDay());
    }

    private Controller getTarget(Controller exec) {
        return exec.getPlayer().getAbility(Executioner.class).getTarget();
    }

    // test
    // need to start off with at least as many charges as there are town

    public void testCharges() {
        Role mashedRole = RoleService.createRole(setup, "Serial Executioner", SerialKiller.abilityType,
                Executioner.abilityType);
        FactionRole mashed = FactionRoleService.createFactionRole(setup.getFactionByColor(Setup.BENIGN_C), mashedRole);
        RoleAbilityModifierService.upsert(mashedRole, Executioner.abilityType, AbilityModifierName.CHARGES, 2);
        Modifiers<AbilityModifierName> modifiers = mashedRole.getAbility(Executioner.abilityType).modifiers;
        assertEquals(2, ModifierTestUtil.getValue(modifiers, AbilityModifierName.CHARGES));

        Controller exec = addPlayer(mashed);
        Controller doc1 = addPlayer(BasicRoles.Doctor());
        Controller doc2 = addPlayer(BasicRoles.Doctor());
        Controller doc3 = addPlayer(BasicRoles.Doctor());
        addPlayer(BasicRoles.Agent());
        addPlayer(BasicRoles.Agent());
        addPlayer(BasicRoles.Agent());

        editRule(SetupModifierName.EXECUTIONER_TO_JESTER, false);
        editRule(SetupModifierName.CHARGE_VARIABILITY, 7);

        Executioner executioner = AbilityTestUtil.getGameAbility(Executioner.class, game);
        assertFalse(executioner.hasCooldownAbility());
        assertNotNull(executioner.isChargeModifiable());

        nightStart();

        assertEquals(1, exec.getPlayer().getAbility(Executioner.class).getRealCharges());

        Controller target1 = getTarget(exec);

        setTarget(exec, target1, SerialKiller.abilityType);
        endNight();

        Controller target2 = getTarget(exec);

        isAlive(exec);
        assertNotSame(target2, target1);

        skipDay();

        Controller liveDoc = ControllerList.list(doc1, doc2, doc3).getLivePlayers(game).getFirst();
        setTarget(liveDoc, exec);
        setTarget(exec, target2, SerialKiller.abilityType);
        endNight();

        isDead(exec);
        assertNotSame(0, exec.getPlayer().getDeathType().attacks.size());
    }

    public void testNoChoices() {
        Controller vigilante = addPlayer(BasicRoles.Vigilante());
        Controller maf = addPlayer(BasicRoles.Goon());
        addPlayer(BasicRoles.Executioner());

        editRule(SetupModifierName.EXEC_TOWN, true);
        modifyAbilityCharges(BasicRoles.Executioner(), 100);
        editRule(SetupModifierName.CHARGE_VARIABILITY, 0);

        shoot(vigilante, maf);
        mafKill(maf, vigilante);
        endNight();
    }

    public void testKeepOtherAbilities() {
        Role mashedRole = RoleService.createRole(setup, "Serial Executioner", SerialKiller.abilityType,
                Executioner.abilityType);
        FactionRole mashed = FactionRoleService.createFactionRole(setup.getFactionByColor(Setup.BENIGN_C), mashedRole);
        RoleAbilityModifierService.upsert(mashedRole, Executioner.abilityType, AbilityModifierName.CHARGES, 2);
        Modifiers<AbilityModifierName> modifiers = mashedRole.getAbility(Executioner.abilityType).modifiers;
        assertEquals(2, ModifierTestUtil.getValue(modifiers, AbilityModifierName.CHARGES));

        Controller exec = addPlayer(mashed);
        addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Agent());
        addPlayer(BasicRoles.Agent());

        editRule(SetupModifierName.EXECUTIONER_TO_JESTER, true);

        nightStart();

        Controller target1 = getTarget(exec);

        setTarget(exec, target1, SerialKiller.abilityType);
        endNight();

        assertEquals(3, exec.getPlayer().getRoleAbilities().size());
        assertTrue(exec.is(SerialKiller.abilityType));
    }

    public void testNoZeroWeightModifier() {
        try{
            FactionRoleModifierService.addModifier(BasicRoles.Executioner(), AbilityModifierName.ZERO_WEIGHTED,
                    Executioner.abilityType, false);
            fail();
        }catch(NarratorException e){
        }
    }

    public void testNoBackToBackModifier() {
        try{
            FactionRoleModifierService.addModifier(BasicRoles.Executioner(), AbilityModifierName.BACK_TO_BACK,
                    Executioner.abilityType, false);
            fail();
        }catch(NarratorException e){
        }
    }
}
