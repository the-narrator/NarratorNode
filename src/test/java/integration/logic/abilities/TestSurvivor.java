package integration.logic.abilities;

import java.util.Optional;

import game.abilities.DrugDealer;
import game.abilities.Survivor;
import game.ai.Controller;
import game.logic.exceptions.NarratorException;
import game.logic.exceptions.PlayerTargetingException;
import game.logic.templates.BasicRoles;
import integration.logic.SuperTest;
import models.enums.AbilityModifierName;
import models.enums.SetupModifierName;
import services.FactionRoleModifierService;

public class TestSurvivor extends SuperTest {
    public TestSurvivor(String name) {
        super(name);
    }

    private static void setVests(int i) {
        modifyRole(BasicRoles.Survivor(), AbilityModifierName.CHARGES, i);
        editRule(SetupModifierName.CHARGE_VARIABILITY, 0);
    }

    public void testBasicSurvivor() {
        setVests(4);

        Controller c = addPlayer(BasicRoles.Citizen());
        Controller v = addPlayer(BasicRoles.Vigilante());
        Controller survivor = addPlayer(BasicRoles.Survivor());
        Controller goon = addPlayer(BasicRoles.Goon());

        nightStart();

        assertNull(survivor.getPlayer().getAcceptableTargets(Survivor.abilityType));

        // basic role card checks
        assertTrue(survivor.getPlayer().getRoleName().equals(Survivor.class.getSimpleName()));
        assertTrue(survivor.getPlayer().getRoleCardDetails()
                .contains(Survivor.GetNightActionDescription(Optional.of(game), game.setup)));

        // since citizens are the basis for survivors, need to check that citizens can't
        // use vests without actually having vests
        try{
            vest(c);
            fail();
        }catch(PlayerTargetingException e){
        }
        assertTrue(survivor.getCommands().get(0).equals(Survivor.abilityType));
        vest(survivor);
        setTarget(v, survivor, GUN);
        mafKill(goon, survivor);

        endNight();

        isAlive(survivor);
        assertFalse(survivor.getPlayer().isVesting());
        assertInProgress();

        voteOut(c, v, survivor, goon);

        mafKill(goon, v);
        setTarget(v, goon, GUN);
        endNight();

        assertGameOver();
        isWinner(survivor);
        isLoser(goon, c);
    }

    public void testMMWitchVisit() {
        setVests(1);

        addPlayer(BasicRoles.Citizen());
        Controller s = addPlayer(BasicRoles.Survivor());
        Controller w = addPlayer(BasicRoles.Witch());
        Controller mm = addPlayer(BasicRoles.MassMurderer());

        vest(s);
        setTarget(mm, mm);
        witch(w, s, mm);

        endNight();

        isAlive(s);

        skipDay();
        setTarget(mm, mm);
        witch(w, s, mm);

        endNight();

        isDead(s);
    }

    // also tests usage
    public void testCultLeaderConversions() {
        setVests(1);
        Controller s = addPlayer(BasicRoles.Survivor());
        Controller cl = addPlayer(BasicRoles.CultLeader());
        Controller sk = addPlayer(BasicRoles.SerialKiller());

        nightStart();

        assertTotalVestCount(1, s);

        setTarget(cl, s);
        endNight();
        skipDay();
        setTarget(sk, s);
        vest(s);
        endNight();

        assertInProgress();

        skipDay();
        setTarget(sk, s);
        try{
            vest(s);
            fail();
        }catch(PlayerTargetingException e){
        }
        endNight();

        assertGameOver();
    }

    public void testBlockVest() {

        addPlayer(BasicRoles.Citizen());
        Controller a = addPlayer(BasicRoles.Agent());
        Controller c = addPlayer(BasicRoles.Consort());
        Controller s = addPlayer(BasicRoles.Survivor());

        nightStart();
        vest(s);
        mafKill(a, s);
        setTarget(c, s);

        endNight();

        isDead(s);
    }

    public void testArsonAndVest() {

        addPlayer(BasicRoles.Citizen());
        Controller s = addPlayer(BasicRoles.Survivor());
        Controller a1 = addPlayer(BasicRoles.Arsonist());
        Controller a2 = addPlayer(BasicRoles.Arsonist());

        nightStart();

        setTarget(a1, s);
        setTarget(a2);
        vest(s);

        endNight();

        isDead(s);
    }

    public void testDrugBlockFail() {
        setVests(1);

        Controller sk = addPlayer(BasicRoles.SerialKiller());
        Controller surv = addPlayer(BasicRoles.Survivor());
        Controller dd = addPlayer(BasicRoles.DrugDealer());
        Controller dd2 = addPlayer(BasicRoles.DrugDealer());
        Controller cons = addPlayer(BasicRoles.Consort());

        setTarget(cons, surv);
        vest(surv);
        drug(dd, surv, DrugDealer.BLOCKED);

        endNight();

        assertTotalVestCount(1, surv);
        assertFakeVestCount(0, surv);
        assertRealVestCount(1, surv);

        skipDay();

        vest(surv);
        setTarget(sk, surv);
        drug(dd, surv, DrugDealer.BLOCKED);
        drug(dd2, surv, DrugDealer.BLOCKED);

        endNight();

        isAlive(surv);
        assertTotalVestCount(1, surv);
        assertFakeVestCount(1, surv);
        assertRealVestCount(0, surv);

        skipDay();

        assertTrue(surv.getPlayer().isAcceptableTarget(getVestAction(surv.getPlayer())));

        vest(surv);
        setTarget(sk, surv);

        endNight();

        isDead(surv);
    }

    public void testNoImmunityOnGameOver() {
        Controller survivor = addPlayer(BasicRoles.Survivor());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller maf = addPlayer(BasicRoles.Goon());

        mafKill(maf, cit);
        vest(survivor);
        endNight();

        isInvuln(survivor, false);
    }

    public void testSurvivorImmunityJesterKill() {
        Controller survivor = addPlayer(BasicRoles.Survivor());
        Controller jester = addPlayer(BasicRoles.Jester());
        addPlayer(BasicRoles.Goon());
        addPlayer(BasicRoles.Mason());
        addPlayer(BasicRoles.Mason());

        vote(survivor, jester);
        endDay();

        vest(survivor);
        endNight();

        isInvuln(survivor, false);

        skipDay();
        for(int i = 0; i < 5; i++){
            nextNight();
        }

        assertAttackSize(1, survivor);
    }

    public void testNoBackToBackModifier() {
        try{
            FactionRoleModifierService.addModifier(BasicRoles.Survivor(), AbilityModifierName.BACK_TO_BACK,
                    Survivor.abilityType, false);
            fail();
        }catch(NarratorException e){
        }
    }

    public void testNoZeroWeightModifier() {
        try{
            FactionRoleModifierService.addModifier(BasicRoles.Survivor(), AbilityModifierName.ZERO_WEIGHTED,
                    Survivor.abilityType, false);
            fail();
        }catch(NarratorException e){
        }
    }
}
