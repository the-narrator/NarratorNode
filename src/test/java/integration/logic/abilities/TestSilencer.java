package integration.logic.abilities;

import java.util.ArrayList;

import game.ai.Controller;
import game.event.DayChat;
import game.event.Message;
import game.logic.exceptions.NarratorException;
import game.logic.templates.BasicRoles;
import integration.logic.SuperTest;
import models.enums.AbilityModifierName;

public class TestSilencer extends SuperTest {

    public TestSilencer(String name) {
        super(name);
    }

    Controller silencer;

    @Override
    public void roleInit() {
        silencer = addPlayer(BasicRoles.Silencer());
    }

    public void testBasic() {
        Controller cit = addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Citizen());

        setTarget(silencer, cit);
        endNight();

        try{
            say(cit, "hi", DayChat.KEY);
            fail();
        }catch(NarratorException e){
        }
    }

    public void testAnnounced() {
        Controller serialKiller = addPlayer(BasicRoles.SerialKiller());
        Controller cit = addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Citizen());
        modifyRole(BasicRoles.Silencer().role, AbilityModifierName.SILENCE_ANNOUNCED, true);

        setTarget(silencer, cit);
        ArrayList<Message> announcements = addAnnouncementListener();
        endNight();

        assertSilenceIsAnnounced(cit, announcements);

        skipDay();
        setTarget(serialKiller, cit);
        endNight();

        assertSilenceIsAnnounced(cit, announcements);
    }

    public void testIsNotAnnounced() {
        Controller cit = addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Citizen());
        modifyRole(BasicRoles.Silencer().role, AbilityModifierName.SILENCE_ANNOUNCED, false);

        setTarget(silencer, cit);
        ArrayList<Message> announcements = addAnnouncementListener();
        endNight();

        assertSilenceIsNotAnnounced(announcements);
    }

    public void testNoAnnouncementIfDead() {
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller serialKiller = addPlayer(BasicRoles.SerialKiller());
        addPlayer(BasicRoles.Citizen());
        modifyRole(BasicRoles.Silencer().role, AbilityModifierName.SILENCE_ANNOUNCED, true);

        setTarget(silencer, cit);
        setTarget(serialKiller, cit);
        ArrayList<Message> announcements = addAnnouncementListener();
        endNight();

        assertSilenceIsNotAnnounced(announcements);

        nextDay();
        nextDay();

        assertSilenceIsNotAnnounced(announcements);
    }

    private void assertSilenceIsNotAnnounced(ArrayList<Message> announcements) {
        for(Message message: announcements){
            if(message.access(Message.PUBLIC).contains(" is silenced."))
                fail("Announcement was not supposed to happen.");
        }
    }

    private void assertSilenceIsAnnounced(Controller cit, ArrayList<Message> announcements) {
        for(Message message: announcements){
            String stringMessage = message.access(Message.PUBLIC);
            if(stringMessage.contains(" is silenced.") && stringMessage.contains(cit.getName()))
                return;
        }
        fail("Announcement was supposed to happen.");
    }
}
