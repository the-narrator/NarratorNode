package integration.logic.abilities;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import game.abilities.Armorsmith;
import game.abilities.Blackmailer;
import game.abilities.Coroner;
import game.abilities.CultLeader;
import game.abilities.Cultist;
import game.abilities.Detective;
import game.abilities.Doctor;
import game.abilities.FactionKill;
import game.abilities.GameAbility;
import game.abilities.Hidden;
import game.abilities.Investigator;
import game.abilities.Lookout;
import game.abilities.Sheriff;
import game.abilities.Ventriloquist;
import game.abilities.support.CultInvitation;
import game.ai.Controller;
import game.logic.Player;
import game.logic.exceptions.IllegalGameSettingsException;
import game.logic.exceptions.NarratorException;
import game.logic.exceptions.PlayerTargetingException;
import game.logic.support.Constants;
import game.logic.support.Option;
import game.logic.templates.BasicRoles;
import game.setups.Setup;
import integration.logic.SuperTest;
import models.Faction;
import models.FactionRole;
import models.Role;
import models.enums.AbilityModifierName;
import models.enums.AbilityType;
import models.enums.FactionModifierName;
import models.enums.RoleModifierName;
import models.enums.SetupModifierName;
import services.FactionModifierService;
import services.FactionRoleModifierService;
import services.FactionRoleService;
import services.RoleService;
import services.SetupModifierService;
import util.game.LookupUtil;
import util.models.FactionRoleTestUtil;

public class TestCult extends SuperTest {

    public TestCult(String name) {
        super(name);
    }

    public void testOpposingTeam() {
        addPlayer(BasicRoles.CultLeader());
        addPlayer(BasicRoles.Witch());
        addPlayer(BasicRoles.Cultist());
        addPlayer(BasicRoles.Cultist());

        editRule(SetupModifierName.CULT_PROMOTION, false);

        assertBadGameSettings();

        addPlayer(BasicRoles.Citizen());
        nightStart();
    }

    public void testNoConvertingIfDead() {
        Controller cultLeader = addPlayer(BasicRoles.CultLeader());
        Controller citizen = addPlayer(BasicRoles.Citizen());
        Controller serialKiller = addPlayer(BasicRoles.SerialKiller());
        addPlayer(BasicRoles.Citizen());

        setTarget(serialKiller, citizen);
        setTarget(cultLeader, citizen);
        endNight();

        assertEquals(BasicRoles.Citizen().getColor(), citizen.getColor());
    }

    public void testNoCultToConvertInto() {
        RoleService.delete(BasicRoles.Cultist().role);
        SetupModifierService.upsertModifier(setup, SetupModifierName.CULT_KEEPS_ROLES, false);
        addPlayer(BasicRoles.CultLeader());
        addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Citizen());

        assertBadGameSettings();
    }

    public void testNoChargeKick() {
        modifyRole(BasicRoles.Citizen(), RoleModifierName.UNCONVERTABLE, true);
        modifyAbilityCharges(BasicRoles.CultLeader(), CultLeader.abilityType, 1);
        editRule(SetupModifierName.CHARGE_VARIABILITY, 0);

        Controller cl = addPlayer(BasicRoles.CultLeader());
        Controller cit = addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.SerialKiller());

        setTarget(cl, cit);
        assertRealChargeRemaining(1, cl);
        endNight();

        assertStatus(cit, CultLeader.abilityType, false);
        assertRealChargeRemaining(1, cl);
    }

    public void testBasicAction() {
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller cl = addPlayer(BasicRoles.CultLeader());
        addPlayer(BasicRoles.Cultist());

        setTarget(cl, cit);

        endNight();

        assertGameOver();
    }

    public void testKeepRoleAndChat() {
        Controller sher = addPlayer(BasicRoles.Sheriff());
        Controller cl = addPlayer(BasicRoles.CultLeader());
        addPlayer(BasicRoles.Citizen());

        editRule(SetupModifierName.CULT_KEEPS_ROLES, true);
        editRule(SetupModifierName.CULT_PROMOTION, false);

        setTarget(cl, sher);

        endNight();

        assertTrue(sher.getPlayer().hasAbility(Sheriff.abilityType));

        // andChat portion
        skipDay();

        partialContains(sher.getChatKeys(), cl.getColor());
    }

    public void testChangeRole() {
        Controller cl = addPlayer(BasicRoles.CultLeader());
        Controller cult = addPlayer(BasicRoles.Cultist());
        Controller doc = addPlayer(BasicRoles.Doctor());

        editRule(SetupModifierName.CULT_KEEPS_ROLES, false);
        editRule(SetupModifierName.CULT_PROMOTION, false);

        setTarget(cl, doc);

        endNight();

        assertFalse(doc.getPlayer().hasAbility(Doctor.abilityType));
        isAlive(cult);
    }

    private void cooldown(int cooldown) {
        modifyAbilityCooldown(BasicRoles.CultLeader(), cooldown);
    }

    public void testCultConversionCooldown() {
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller doc = addPlayer(BasicRoles.Doctor());
        Controller cL = addPlayer(BasicRoles.CultLeader());

        int cooldown = 3;
        cooldown(3);
        editRule(SetupModifierName.CULT_PROMOTION, false);

        setTarget(cL, doc);

        nextNight();

        for(int i = 0; i < cooldown; i++){
            try{
                setTarget(cL, cit);
                fail();
            }catch(PlayerTargetingException e){
            }
            nextNight();
        }

        setTarget(cL, cit);
    }

    public void testPowerRoleConversionCooldown() {
        Controller cit2 = addPlayer(BasicRoles.Citizen());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller doc = addPlayer(BasicRoles.Doctor());
        Controller cL = addPlayer(BasicRoles.CultLeader());

        int cd = 5;
        editRule(SetupModifierName.CULT_POWER_ROLE_CD, cd);
        cooldown(0);
        editRule(SetupModifierName.CULT_PROMOTION, false);

        setTarget(cL, cit2);
        nextNight();

        setTarget(cL, doc);
        nextNight();

        for(int i = 0; i < cd; i++){
            try{
                setTarget(cL, cit);
                fail();
            }catch(PlayerTargetingException e){
            }
            nextNight();
        }

        setTarget(cL, cit);
    }

    public void testMultipleConversions() {
        Controller cit1 = addPlayer(BasicRoles.Citizen());
        Controller cit2 = addPlayer(BasicRoles.Citizen());
        Controller cit3 = addPlayer(BasicRoles.Citizen());
        Controller cit4 = addPlayer(BasicRoles.Citizen());
        Controller cL = addPlayer(BasicRoles.CultLeader());

        cooldown(0);
        editRule(SetupModifierName.CULT_PROMOTION, false);

        setTarget(cL, cit1);
        endNight();

        skipDay();

        setTarget(cL, cit2);
        endNight();

        voteOut(cit4, cit1, cit2, cit3);
    }

    public void testMayorCult() {
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller mayor = addPlayer(BasicRoles.Mayor());
        Controller cL = addPlayer(BasicRoles.CultLeader());
        FactionRoleTestUtil.addCultRole(BasicRoles.CultLeader(), BasicRoles.Mayor());

        editRule(SetupModifierName.CULT_KEEPS_ROLES, true);
        editRule(SetupModifierName.CULT_PROMOTION, false);

        setTarget(cL, mayor);
        endNight();

        reveal(mayor);
        voteOut(cit, mayor);

        assertGameOver();
    }

    public void testMafTeamUnrecruitable() {
        Controller cL = addPlayer(BasicRoles.CultLeader());
        addPlayer(BasicRoles.Citizen());
        Controller maf = addPlayer(BasicRoles.Goon());

        Faction faction = setup.getFactionByColor(BasicRoles.Goon().getColor());
        FactionModifierService.upsertModifier(faction, FactionModifierName.IS_RECRUITABLE, false);

        editRule(SetupModifierName.CULT_PROMOTION, false);

        setTarget(cL, maf);

        endNight();

        assertEquals(BasicRoles.Goon().getColor(), maf.getColor());
    }

    public void testCultRuleText() {
        editRule(SetupModifierName.CONVERT_REFUSABLE, true);
        editRule(SetupModifierName.CHARGE_VARIABILITY, 0);
        FactionRole cultLeader = BasicRoles.CultLeader();
        List<String> publicDescription = FactionRoleTestUtil.getPublicDescription(cultLeader);

        assertContains(publicDescription, SetupModifierName.CONVERT_REFUSABLE.getActiveFormat());
    }

    public void testKeepSendingPower() {
        Controller bm = addPlayer(BasicRoles.Blackmailer());
        Controller maf = addPlayer(BasicRoles.Goon());
        Controller cult = addPlayer(BasicRoles.CultLeader());
        FactionRoleTestUtil.addCultRole(BasicRoles.CultLeader(), BasicRoles.Goon());

        editRule(SetupModifierName.CULT_PROMOTION, false);

        nightStart();

        setTarget(cult, maf);
        endNight();

        assertEquals(cult.getColor(), maf.getColor());
        skipDay();

        assertTrue(maf.getCommands().contains(FactionKill.abilityType));

        assertTrue(maf.getPlayer().isAcceptableTarget(maf.getPlayer().action(bm.getPlayer(), FactionKill.abilityType)));

        mafKill(maf, bm);
        endNight();

        isDead(bm);
    }

    public void testAllies() {
        Player loyalist = addPlayer(BasicRoles.Chauffeur()).getPlayer();
        Player recruit = addPlayer(BasicRoles.Blackmailer()).getPlayer();
        Player cult = addPlayer(BasicRoles.CultLeader()).getPlayer();

        editRule(SetupModifierName.CULT_KEEPS_ROLES, false);
        editRule(SetupModifierName.CULT_PROMOTION, false);

        setTarget(cult, recruit);
        endNight();

        assertTrue(loyalist.alliesWith(recruit));
        assertFalse(loyalist.alliesWith(cult));

        assertTrue(recruit.alliesWith(cult));
        assertFalse(recruit.alliesWith(loyalist));

        assertTrue(cult.alliesWith(recruit));
        assertFalse(cult.alliesWith(loyalist));
    }

    public void testPossibleKeepRole() {
        addPlayer(BasicRoles.Goon());
        addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.CultLeader());
        Faction cultFaction = BasicRoles.CultLeader().faction;
        editRule(SetupModifierName.CULT_KEEPS_ROLES, true);

        Set<FactionRole> factionRoles = cultFaction.getPossibleFactionRolesWithAbility(game, Cultist.abilityType);

        assertTrue(factionRoles.isEmpty());
    }

    public void testPossibleNoKeepRole() {
        addPlayer(BasicRoles.Goon());
        addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.CultLeader());
        editRule(SetupModifierName.CULT_KEEPS_ROLES, false);
        Faction cultFaction = BasicRoles.CultLeader().faction;

        Set<FactionRole> factionRoles = cultFaction.getPossibleFactionRolesWithAbility(game, Cultist.abilityType);

        assertFalse(factionRoles.isEmpty());
    }

    public void testRefuseCultConversion() {
        Controller chf = addPlayer(BasicRoles.Chauffeur());
        Controller bm = addPlayer(BasicRoles.Blackmailer());
        Controller cult = addPlayer(BasicRoles.CultLeader());
        FactionRoleTestUtil.addCultRole(BasicRoles.CultLeader(), BasicRoles.Blackmailer());

        editRule(SetupModifierName.CONVERT_REFUSABLE, true);
        editRule(SetupModifierName.CULT_PROMOTION, false);

        setTarget(cult, bm);
        endNight();

        assertEquals(chf.getColor(), bm.getColor());

        assertFalse(bm.getPlayer().hasDayAction(Blackmailer.abilityType));
        assertTrue(bm.getPlayer().hasDayAction(CultInvitation.abilityType));

        skipDay();

        setTarget(cult, bm);
        endNight();

        assertEquals(chf.getColor(), bm.getColor());

        doDayAction(bm, CultInvitation.abilityType, Constants.ACCEPT);
        assertEquals(chf.getColor(), bm.getColor());

        skipDay();
        assertEquals(cult.getColor(), bm.getColor());
    }

    public void testVentRefuse() {
        addPlayer(BasicRoles.Vigilante(), 2);
        Controller vent = addPlayer(BasicRoles.Ventriloquist());
        Controller cl = addPlayer(BasicRoles.CultLeader());
        FactionRoleTestUtil.addCultRole(BasicRoles.CultLeader(), BasicRoles.Ventriloquist());

        editRule(SetupModifierName.CULT_PROMOTION, false);
        editRule(SetupModifierName.CONVERT_REFUSABLE, true);

        setTarget(cl, vent);
        setTarget(vent, cl, Ventriloquist.abilityType);
        endNight();

        doDayAction(vent, CultInvitation.abilityType, Constants.ACCEPT);
        skipDay();

        assertEquals(vent.getColor(), cl.getColor());
    }

    public void testrefuseCultCommandHandler() {
        Controller chf = addPlayer(BasicRoles.Chauffeur());
        Controller bm = addPlayer(BasicRoles.Blackmailer());
        Controller cult = addPlayer(BasicRoles.CultLeader());
        FactionRoleTestUtil.addCultRole(BasicRoles.CultLeader(), BasicRoles.Blackmailer());

        editRule(SetupModifierName.CULT_PROMOTION, false);
        editRule(SetupModifierName.CONVERT_REFUSABLE, true);

        setTarget(cult, bm);

        endNight();

        assertEquals(chf.getColor(), bm.getColor());

        command(bm, Constants.DECLINE);

        assertFalse(bm.getPlayer().hasDayAction(Blackmailer.abilityType));
        assertTrue(bm.getPlayer().hasDayAction(CultInvitation.abilityType));

        skipDay();

        setTarget(cult, bm);
        endNight();

        assertEquals(chf.getColor(), bm.getColor());

        command(bm, Constants.ACCEPT);
        assertEquals(chf.getColor(), bm.getColor());

        skipDay();
        assertEquals(cult.getColor(), bm.getColor());
    }

    public void testCultNotKnowingTeammates() {
        addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Framer());
        addPlayer(BasicRoles.Framer());
        addPlayer(Hidden.AnyRandom());

        editRule(SetupModifierName.CULT_PROMOTION, false);

        setTeamRule(BasicRoles.CultLeader().getColor(), FactionModifierName.KNOWS_ALLIES, false);

        try{
            dayStart();
            fail();
        }catch(IllegalGameSettingsException e){
        }
    }

    public void testCultLeaderSameTarget() {
        Role role = BasicRoles.CultLeader().role;
        Faction mafia = setup.getFactionByColor(Setup.MAFIA_C);
        Controller cl1 = addPlayer(BasicRoles.CultLeader());
        Controller cl2 = addPlayer(FactionRoleService.createFactionRole(mafia, role));
        Controller cit = addPlayer(BasicRoles.Citizen());

        editRule(SetupModifierName.CULT_PROMOTION, false);

        setTarget(cl1, cit);
        setTarget(cl2, cit);

        endNight();

        assertEquals(Setup.TOWN_C, cit.getColor());
    }

    public void testDoubleCultLeader() {
        addPlayer(BasicRoles.CultLeader());
        addPlayer(BasicRoles.CultLeader());
        addPlayer(BasicRoles.Citizen());

        editRule(SetupModifierName.CULT_PROMOTION, false);

        assertBadGameSettings();
    }

    // when converting a unique faction role, and the unique faction role already
    // exists on cult team
    // if the new faction role would be unique, fail conversion
    // also do cult invitation roles too

    public void testAlreadyHavingUniqueNighttime() {
        FactionRole cultDoctorFactionRole = FactionRoleTestUtil.addCultRole(BasicRoles.CultLeader(),
                BasicRoles.Doctor());
        FactionRoleModifierService.upsertModifier(cultDoctorFactionRole, RoleModifierName.UNIQUE, true);
        SetupModifierService.upsertModifier(setup, SetupModifierName.CULT_KEEPS_ROLES, true);
        SetupModifierService.upsertModifier(setup, SetupModifierName.CONVERT_REFUSABLE, false);

        addPlayer(cultDoctorFactionRole);
        Controller regDoctor = addPlayer(BasicRoles.Doctor());
        Controller cultLeader = addPlayer(BasicRoles.CultLeader());

        setTarget(cultLeader, regDoctor);
        endNight();

        assertNotSame(regDoctor.getColor(), cultLeader.getColor());
    }

    public void testAlreadyHavingUniqueDaytime() {
        FactionRole cultDoctorFactionRole = FactionRoleTestUtil.addCultRole(BasicRoles.CultLeader(),
                BasicRoles.Doctor());
        FactionRoleModifierService.upsertModifier(cultDoctorFactionRole, RoleModifierName.UNIQUE, true);
        SetupModifierService.upsertModifier(setup, SetupModifierName.CULT_KEEPS_ROLES, true);
        SetupModifierService.upsertModifier(setup, SetupModifierName.CONVERT_REFUSABLE, true);

        addPlayer(cultDoctorFactionRole);
        Controller regDoctor = addPlayer(BasicRoles.Doctor());
        Controller cultLeader = addPlayer(BasicRoles.CultLeader());

        setTarget(cultLeader, regDoctor);
        endNight();

        doDayAction(regDoctor, CultInvitation.abilityType, Constants.ACCEPT);
        skipDay();

        assertNotSame(regDoctor.getColor(), cultLeader.getColor());
    }

    public void testSheriffDetection() {
        Controller sheriff = addPlayer(BasicRoles.Sheriff());
        Controller maf = addPlayer(BasicRoles.Goon());
        Controller cl = addPlayer(BasicRoles.CultLeader());

        editRule(SetupModifierName.CULT_PROMOTION, false);

        setTarget(cl, sheriff);
        setTarget(sheriff, maf);
        endNight();

        partialContains(sheriff, maf.getPlayer().getGameFaction().getName());

        skipDay();

        setTarget(sheriff, maf);
        endNight();

        partialContains(sheriff, maf.getPlayer().getGameFaction().getName(), 2);
    }

    public void testOptionWhenNoPowerup() {
        addPlayer(BasicRoles.Citizen());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller cult = addPlayer(BasicRoles.CultLeader());

        editRule(SetupModifierName.CULT_PROMOTION, false);

        try{
            promote(cult, cit, Sheriff.class);
            fail();
        }catch(PlayerTargetingException e){
        }
    }

    private void addAllPromoteRoles() {
        Faction faction = BasicRoles.CultLeader().faction;
        Role role;
        for(AbilityType abilityType: CultLeader.PROMOTES){
            role = LookupUtil.findRolesWithAbility(setup, abilityType).iterator().next();
            FactionRoleService.createFactionRole(faction, role);
        }
    }

    public void testCultPromotion() {
        Controller sheriff = addPlayer(BasicRoles.Sheriff());
        Controller coroner = addPlayer(BasicRoles.Coroner());
        Controller doctor = addPlayer(BasicRoles.Doctor());
        Controller doctor2 = addPlayer(BasicRoles.Doctor());
        Controller lookout = addPlayer(BasicRoles.Lookout());
        Controller detect = addPlayer(BasicRoles.Detective());
        Controller detect2 = addPlayer(BasicRoles.Detective());
        Controller cult = addPlayer(BasicRoles.CultLeader());
        Controller cit1 = addPlayer(BasicRoles.Citizen());
        Controller cit2 = addPlayer(BasicRoles.Citizen());
        Controller cit3 = addPlayer(BasicRoles.Citizen());
        Controller amnes = addPlayer(BasicRoles.Amnesiac());
        Controller jester = addPlayer(BasicRoles.Jester());
        Controller jester2 = addPlayer(BasicRoles.Jester());
        addPlayer(BasicRoles.Goon());

        editRule(SetupModifierName.CULT_PROMOTION, true);
        editRule(SetupModifierName.CONVERT_REFUSABLE, false);
        editRule(SetupModifierName.CULT_KEEPS_ROLES, true);
        cooldown(0);

        addAllPromoteRoles();

        try{
            nightStart();
            fail();
        }catch(IllegalGameSettingsException e){
        }

        editRule(SetupModifierName.CULT_KEEPS_ROLES, false);

        voteOut(detect2, sheriff, coroner, doctor, doctor2, lookout, detect, cult, cit1, jester, amnes, cit2, cit3);

        Player cultPlayer = cult.getPlayer();
        ArrayList<Option> options = cultPlayer.getAbility(CultLeader.class).getOptions(cultPlayer);
        assertEquals(6, options.size());

        promote(cult, cit1, Investigator.class);
        nextNight();

        assertTrue(cit1.is(Investigator.abilityType));
        assertEquals(5, cultPlayer.getAbility(CultLeader.class).getOptions(cultPlayer).size());

        setTarget(cult, cit2);
        nextNight();

        assertTrue(cit2.is(Cultist.abilityType));

        try{
            promote(cult, doctor, Armorsmith.class);
            fail();
        }catch(PlayerTargetingException e){
        }
        try{
            promote(cult, doctor, Investigator.class);
            fail();
        }catch(PlayerTargetingException e){
        }
        promote(cult, doctor, Sheriff.class);
        nextNight();

        assertTrue(doctor.is(Cultist.abilityType));

        setTarget(cult, sheriff);
        nextNight();

        assertTrue(sheriff.is(Sheriff.abilityType));
        assertTrue(cultPlayer.getOptions(CultLeader.abilityType).contains(new Option(Lookout.class.getSimpleName())));

        setTarget(cult, lookout);
        nextNight();

        assertTrue(lookout.is(Lookout.abilityType));

        promote(cult, coroner, Coroner.class);
        nextNight();

        assertTrue(coroner.is(Cultist.abilityType));

        promote(cult, detect, Coroner.class);
        nextNight();

        assertTrue(coroner.is(Cultist.abilityType));

        setTarget(cult, doctor2);
        nextNight();

        assertTrue(doctor2.is(Cultist.abilityType));

        setTarget(amnes, detect2);
        promote(cult, cit3, Detective.class);
        nextNight();

        assertTrue(amnes.is(Detective.abilityType));
        assertTrue(cit3.is(Detective.abilityType));

        promote(cult, jester, Coroner.class);
        nextNight();

        assertTrue(jester.is(Cultist.abilityType));

        setTarget(cult, jester2);
        nextNight();

        assertTrue(jester2.is(Cultist.abilityType));

        // test for name change, and put it here. the option should say 'savage' if the
        // cultist name is a savage
        // recruiting 'non powerless roles' like 'epsilons' specifically jester
    }

    public void testLosePassiveCharge() {
        Controller cultLeader = addPlayer(BasicRoles.CultLeader());
        Controller vigi = addPlayer(BasicRoles.Vigilante());
        Controller surv = addPlayer(BasicRoles.Survivor());
        Controller baker = addPlayer(BasicRoles.Baker());
        addPlayer(BasicRoles.Goon(), 2);

        editRule(SetupModifierName.CULT_KEEPS_ROLES, false);
        editRule(SetupModifierName.SELF_BREAD_USAGE, true);
        cooldown(0);

        setTarget(cultLeader, baker);
        endNight();

        assertPassableBreadCount(0, baker);
        skipDay();

        setTarget(cultLeader, surv);
        endNight();

        assertTotalVestCount(0, surv);
        skipDay();

        setTarget(cultLeader, vigi);
        endNight();

        assertTotalGunCount(0, vigi);
    }

    public void testGetItems() {
        Controller cl = addPlayer(BasicRoles.CultLeader());
        Controller as = addPlayer(BasicRoles.Armorsmith());
        Controller cit = addPlayer(BasicRoles.Citizen());

        cooldown(0);
        editRule(SetupModifierName.CULT_POWER_ROLE_CD, 0);
        editRule(SetupModifierName.CONVERT_REFUSABLE, true);
        editRule(SetupModifierName.CULT_PROMOTION, false);
        editRule(SetupModifierName.CULT_KEEPS_ROLES, true);

        setTarget(cl, cit);
        setTarget(as, cit);

        endNight();

        assertRealVestCount(1, cit);
        assertEquals(cit.getColor(), cl.getColor());
    }

    public void testNoZeroWeightModifier() {
        try{
            FactionRoleModifierService.addModifier(BasicRoles.Cultist(), AbilityModifierName.ZERO_WEIGHTED,
                    Cultist.abilityType, false);
            fail();
        }catch(NarratorException e){
        }
    }

    public void testNoBackToBackModifier() {
        try{
            FactionRoleModifierService.addModifier(BasicRoles.Cultist(), AbilityModifierName.BACK_TO_BACK,
                    Cultist.abilityType, false);
            fail();
        }catch(NarratorException e){
        }
    }

    private void promote(Controller cl, Controller target, Class<? extends GameAbility> c) {
        if(c == null)
            setTarget(cl, target);
        else
            setTarget(CultLeader.getUpgradeAction(cl, target, c.getSimpleName(), game));
    }
}
