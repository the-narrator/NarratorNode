package integration.logic.abilities;

import java.util.ArrayList;
import java.util.List;

import game.abilities.Detective;
import game.abilities.DrugDealer;
import game.abilities.FactionKill;
import game.abilities.FactionSend;
import game.abilities.GraveDigger;
import game.abilities.JailCreate;
import game.abilities.JailExecute;
import game.abilities.Lookout;
import game.abilities.Sleepwalker;
import game.abilities.util.AbilityUtil;
import game.ai.Brain;
import game.ai.Controller;
import game.ai.ControllerList;
import game.event.ChatMessage;
import game.event.FactionChat;
import game.event.Feedback;
import game.event.Message;
import game.logic.Game;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.exceptions.ChatException;
import game.logic.exceptions.PhaseException;
import game.logic.exceptions.PlayerTargetingException;
import game.logic.support.Constants;
import game.logic.support.Random;
import game.logic.support.RoleAssigner;
import game.logic.support.action.Action;
import game.logic.templates.BasicRoles;
import integration.logic.SuperTest;
import models.Command;
import models.FactionRole;
import models.enums.AbilityModifierName;
import models.enums.AbilityType;
import models.enums.GameModifierName;
import models.enums.SetupModifierName;
import services.GameService;
import services.SetupModifierService;
import util.TestChatUtil;
import util.TestUtil;
import util.Util;

public class TestJailor extends SuperTest {

    public TestJailor(String s) {
        super(s);
    }

    public void testComm() {
        Controller jailor = addPlayer(BasicRoles.Jailor());
        Controller fodder = addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Goon());

        dayStart();// so presize can work correctly

        int preSize = TestChatUtil.getMessages((Player) jailor).size();
        jail(jailor, fodder);
        int postSize = TestChatUtil.getMessages((Player) jailor).size();
        assertEqual(preSize + 1, postSize);

        preSize = postSize;
        cancelAction(jailor, 0);
        postSize = TestChatUtil.getMessages((Player) jailor).size();
        assertEqual(preSize + 1, postSize);
    }

    public void testJailorBasicAndFeedback() {
        Controller jailor = addPlayer(BasicRoles.Jailor());
        Controller fodder = addPlayer(BasicRoles.Citizen());
        Controller mafioso = addPlayer(BasicRoles.Goon());

        dayStart();

        jail(jailor, fodder);
        assertActionSize(1, jailor);

        cancelAction(jailor, 0);

        assertDayActionSize(0);
        assertEquals(0, jailor.getPlayer().getActions().size());
        assertEquals(2, jailor.getPlayer().getAcceptableTargets(JailCreate.abilityType).size());

        skipDay();

        assertFalse(fodder.getPlayer().isJailed());
        // this might fail when alaises are increased
        assertTrue(game.getCommands().get(0).text.toLowerCase().contains(JailCreate.JAIL.toLowerCase()));

        // no one being jailed right now, so invalid action
        try{
            setTarget(jailor, fodder);
            fail();
        }catch(PlayerTargetingException e){
        }

        assertFalse(jailor.getCommands().contains(JailCreate.abilityType));

        endNight();

        jail(jailor, mafioso);
        try{
            // self
            jail(jailor, jailor);
            fail();
        }catch(PlayerTargetingException e){
        }

        jail(jailor, fodder);
        skipDay();

        // same, already jailing someone
        try{
            execute(jailor, mafioso);
            fail();
        }catch(PlayerTargetingException e){
        }

        // preSize = extractComm(fodder).size();
        execute(jailor, fodder);
        // postSize = extractComm(fodder).size();
        // assertEquals(preSize + 1, postSize);

        // preSize = postSize;
        cancelAction(jailor, 0);
        // postSize = extractComm(fodder).size();
        // assertEquals(preSize + 1, postSize);

        assertTrue(jailor.getCommands().contains(JailExecute.abilityType));

        execute(jailor, fodder);

        endNight();

        isDead(fodder);
        assertGameOver();
    }

    public void testParsingAbility() {
        Controller kidnapper = addPlayer(BasicRoles.Kidnapper());
        Controller as = addPlayer(BasicRoles.Armorsmith());
        Controller gs = addPlayer(BasicRoles.Gunsmith());

        setTarget(as, kidnapper);
        setTarget(gs, kidnapper);
        endNight();

        jail(kidnapper, as);
        skipDay();

        assertEquals(JailExecute.abilityType, AbilityUtil.getAbilityByCommand(JailExecute.COMMAND));

        endNight();
        command(kidnapper, JailCreate.JAIL, as.getName());
    }

    public void testDayActionParsing() {
        addPlayer(BasicRoles.Citizen());
        Controller host = addPlayer(BasicRoles.Jailor());
        Controller fodder = addPlayer(BasicRoles.Kidnapper());

        try{
            setTarget(host, JailCreate.abilityType, null, fodder);
            fail();
        }catch(PhaseException | PlayerTargetingException e){
        }
    }

    public void testStopActionSubmit() {
        Controller jailor = addPlayer(BasicRoles.Jailor());
        Controller survivor = addPlayer(BasicRoles.Survivor());
        addPlayer(BasicRoles.Goon());

        jail(jailor, survivor);

        assertFalse(jailor.getPlayer().getAbility(AbilityType.JailCreate).hasCooldownAbility());
        assertActionSize(1, jailor);

        skipDay();

        try{
            vest(survivor);
            fail();
        }catch(PlayerTargetingException e){
        }

        nextNight();

        // able to submit actions again
        vest(survivor);
    }

    public void testNonVisiting() {
        Controller jailor = addPlayer(BasicRoles.Jailor());
        Controller lookout = addPlayer(BasicRoles.Lookout());
        Controller fodder = addPlayer(BasicRoles.Witch());

        jail(jailor, fodder);
        skipDay();

        setTarget(lookout, fodder);
        endNight();

        partialContains(lookout, Lookout.NO_VISIT);
    }

    public void testWitchManipulatingNoJailTargets() {
        Controller jailor = addPlayer(BasicRoles.Jailor());
        Controller lookout = addPlayer(BasicRoles.Lookout());
        Controller witch = addPlayer(BasicRoles.Witch());

        witch(witch, jailor, lookout);
        setTarget(lookout, lookout);
        endNight();

        TestLookout.seen(lookout, jailor);
    }

    public void testCommandHandling() {
        Controller jailor = addPlayer(BasicRoles.Jailor());
        Controller survivor = addPlayer(BasicRoles.Survivor());
        addPlayer(BasicRoles.Goon());

        dayStart();

        command(jailor, JailCreate.JAIL, survivor.getName());

        skipDay();

        isJailed(survivor);

        endNight();

        command(jailor, JailCreate.JAIL, survivor.getName());
        command(jailor, Constants.CANCEL, Integer.toString(1)); // command handler expects it 1 index based

        skipDay();

        notJailed(survivor);
    }

    public void testRoleConversion() {
        Controller masonLeader = addPlayer(BasicRoles.MasonLeader());
        Controller cultLeader = addPlayer(BasicRoles.CultLeader());
        Controller jailor = addPlayer(BasicRoles.Jailor());
        Controller cit = addPlayer(BasicRoles.Citizen());

        dayStart();
        String oldColor = cit.getColor();

        jail(jailor, cit);
        skipDay();

        setTarget(masonLeader, cit);
        endNight();

        jail(jailor, cit);
        skipDay();
        setTarget(cultLeader, cit);
        endNight();

        assertEquals(oldColor, cit.getColor());
    }

    public void testDayDiedJailor() {
        Controller jailor = addPlayer(BasicRoles.Jailor());
        Controller jailor2 = addPlayer(BasicRoles.Jailor());
        Controller survivor = addPlayer(BasicRoles.Survivor());
        Controller citizen = addPlayer(BasicRoles.Citizen());
        Controller maf = addPlayer(BasicRoles.Goon());

        jail(jailor, maf);
        jail(jailor2, jailor);

        voteOut(jailor, survivor, citizen, maf);

        assertStatus(maf, JailCreate.abilityType, false);
        assertStatus(jailor, JailCreate.abilityType, false);
    }

    public void testPostLynchDay() {
        Controller jailor = addPlayer(BasicRoles.Jailor());
        Controller survivor = addPlayer(BasicRoles.Survivor());
        Controller fodder = addPlayer(BasicRoles.Citizen());
        Controller agent = addPlayer(BasicRoles.Agent());

        jail(jailor, survivor);
        assertActionSize(1, jailor);
        voteOut(fodder, agent, survivor, jailor);

        partialContains(survivor, JailCreate.DRAGGED_MESSAGE);

        try{
            execute(jailor, survivor);
            fail();
        }catch(PlayerTargetingException e){
        }

        assertStatus(survivor, JailCreate.abilityType, true);
        assertActionSize(0, jailor);

        endNight();
        assertActionSize(0, jailor);
    }

    public void testSleepwalkerCantWalk() {
        Controller sw = addPlayer("A");
        Controller jailor = addPlayer("B");
        Controller agent = addPlayer("C");
        Controller citizen = addPlayer("D");

        RoleAssigner ra = TestUtil.assignRoles(game, new Controller[] { sw, jailor, agent, citizen },
                new FactionRole[] { BasicRoles.Sleepwalker(), BasicRoles.Jailor(), BasicRoles.Agent(),
                        BasicRoles.Citizen() });

        SetupModifierService.upsertModifier(setup, SetupModifierName.DAY_START, Game.DAY_START);
        GameService.internalStart(game, ra);

        assertTrue(sw.is(Sleepwalker.abilityType));

        jail(jailor, sw);
        skipDay();

        partialContains(sw, JailCreate.DRAGGED_MESSAGE);

        setTarget(agent, sw);
        endNight();

        partialContains(agent, Detective.NO_VISIT);
    }

    public void testBreadedJail() {
        Controller jailor = addPlayer(BasicRoles.Jailor());
        Controller baker = addPlayer(BasicRoles.Baker());
        Controller mafioso = addPlayer(BasicRoles.Goon());
        Controller witch = addPlayer(BasicRoles.Witch());
        addPlayer(BasicRoles.Witch());

        setCharges(2);

        setTarget(baker, jailor);
        endNight();

        jail(jailor, baker);

        assertTrue(jailor.getPlayer().getActions().isTargeting(baker.getPlayer(), JailCreate.abilityType));
        jail(jailor, baker);

        jail(jailor, mafioso);
        skipDay();

        assertPassableBreadCount(0, jailor);

        execute(jailor, mafioso, baker);
        endNight();

        isDead(baker, mafioso);

        jail(jailor, witch);

        skipDay();

        assertPerceivedChargeRemaining(0, jailor, JailExecute.abilityType);

        try{
            setTarget(jailor, witch);
            fail();
        }catch(PlayerTargetingException e){
        }
    }

    public void testJailLoop1() {
        Controller j1 = addPlayer(BasicRoles.Jailor());
        Controller j2 = addPlayer(BasicRoles.Jailor());
        Controller c = addPlayer(BasicRoles.Chauffeur());
        Controller j3 = addPlayer(BasicRoles.Jailor());
        Controller baker = addPlayer(BasicRoles.Baker());

        setTarget(baker, j2);

        endNight();

        jail(j1, c);
        jail(j2, j1); // fail
        jail(j3, j2);
        jail(j2, j3); // fail

        skipDay();

        isJailed(c, j2);
        notJailed(j1, j3);
    }

    public void testJailLoop2() {
        Controller j1 = addPlayer(BasicRoles.Jailor());
        Controller j2 = addPlayer(BasicRoles.Jailor());
        Controller j3 = addPlayer(BasicRoles.Jailor());
        Controller c = addPlayer(BasicRoles.Chauffeur());
        Controller baker = addPlayer(BasicRoles.Baker());

        setTarget(baker, j2);

        endNight();

        jail(j1, c);
        jail(j2, j1);
        jail(j2, j3);
        jail(j3, j2);

        skipDay();

        isJailed(c, j3);
        notJailed(j1, j2);
    }

    public void testBreadedJailCH() {
        Controller jailor = addPlayer(BasicRoles.Jailor());
        Controller baker = addPlayer(BasicRoles.Baker());
        Controller mafioso = addPlayer(BasicRoles.Goon());

        setTarget(baker, jailor);
        endNight();

        chJail(jailor, baker);
        chJail(jailor, mafioso);
        assertActionSize(2, jailor);
        command(jailor, Constants.CANCEL, Integer.toString(2));

        skipDay();

        isJailed(baker);
        notJailed(mafioso);
    }

    private void isJailed(Controller... jailed) {
        for(Controller p: jailed)
            assertStatus(p, JailCreate.abilityType);
    }

    private void notJailed(Controller... jailed) {
        for(Controller p: jailed)
            assertStatus(p, JailCreate.abilityType, false);
    }

    private void chJail(Controller jailor, Controller jailedTarget) {
        command(jailor, JailCreate.JAIL, jailedTarget.getName());
    }

    private void setCharges(int i) {
        modifyRole(BasicRoles.Jailor(), AbilityModifierName.CHARGES, i);
        modifyRole(BasicRoles.Kidnapper(), AbilityModifierName.CHARGES, i);
        editRule(SetupModifierName.CHARGE_VARIABILITY, 0);
    }

    public void testCharges() {
        setCharges(1);

        Controller jailor = addPlayer(BasicRoles.Jailor());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller maf = addPlayer(BasicRoles.Goon());
        addPlayer(BasicRoles.Goon());

        jail(jailor, cit);
        jail(jailor, maf);
        assertActionSize(1, jailor);
        assertEquals(1, game.actionStack.size());
        skipDay();

        execute(jailor, maf);
        endNight();

        isDead(maf);
        assertTrue(jailor.getPlayer().getAcceptableTargets(JailCreate.abilityType).contains(cit.getPlayer()));
        assertFalse(jailor.getCommands().contains(JailExecute.abilityType));

        jail(jailor, cit);
        skipDay();

        try{
            execute(jailor, cit);
            fail();
        }catch(PlayerTargetingException e){
        }
    }

    public void testJailingSamePeople() {
        setCharges(2);

        Controller j1 = addPlayer(BasicRoles.Jailor());
        Controller j2 = addPlayer(BasicRoles.Jailor());
        Controller maf = addPlayer(BasicRoles.Goon());

        jail(j1, maf);
        jail(j2, maf);

        skipDay();

        assertPerceivedChargeRemaining(2, j1, JailExecute.abilityType);
        execute(j1, maf);
        try{
            setTarget(j2, maf);
            fail();
        }catch(PlayerTargetingException e){
        }
    }

    public void testCorpseJailor() {
        Controller gDigger = addPlayer(BasicRoles.GraveDigger());
        Controller jailor = addPlayer(BasicRoles.Jailor());
        Controller lookout = addPlayer(BasicRoles.Lookout());
        Controller cit = addPlayer(BasicRoles.Citizen());

        voteOut(jailor, lookout, cit, gDigger);

        setTarget(gDigger, GraveDigger.abilityType, null, jailor, cit);
        setTarget(lookout, cit);

        endNight();

        TestLookout.seen(lookout, jailor);
    }

    public void testSeparateExecutionAttempts() {
        Controller jailor = addPlayer(BasicRoles.Jailor());
        Controller baker = addPlayer(BasicRoles.Baker());
        Controller baker2 = addPlayer(BasicRoles.Baker());
        addPlayer(BasicRoles.Witch());

        setTarget(baker, jailor);
        nextNight();
        setTarget(baker, jailor);
        endNight();

        // jailor has 2 bread

        jail(jailor, baker);
        assertUseableBreadCount(2, jailor);
        assertTrue(jailor.getPlayer().getActions().canAddAnotherAction(JailCreate.abilityType));
        jail(jailor, baker2);
        skipDay();

        // nighttime, day, 2 jails
        assertChatLogSize(4, jailor);
        assertPassableBreadCount(1, jailor);
        notJailed(jailor);
        isJailed(baker, baker2);

        execute(jailor, baker2);
        execute(jailor, baker);
        endNight();

        isAlive(baker2);
        isDead(baker);
    }

    public void testWitchForceExecution() {
        setCharges(Constants.UNLIMITED);

        Controller witch = addPlayer(BasicRoles.Witch());
        Controller jailor = addPlayer(BasicRoles.Jailor());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller baker = addPlayer(BasicRoles.Baker());
        Controller poisoner = addPlayer(BasicRoles.Poisoner());
        Controller lookout = addPlayer(BasicRoles.Lookout());

        editRule(SetupModifierName.BREAD_PASSING, false);

        setTarget(baker, witch);
        endNight();

        jail(jailor, cit);
        skipDay();

        // force single execution
        setTarget(baker, jailor); // jailor has 1
        witch(witch, jailor, jailor);
        endNight();

        isDead(cit);
        assertPerceivedChargeRemaining(Constants.UNLIMITED, jailor, JailCreate.abilityType);
        assertCooldownRemaining(0, jailor, JailCreate.abilityType);

        jail(jailor, poisoner);
        skipDay();

        // visit/use bread
        setTarget(baker, jailor); // jailor will have 2
        setTarget(lookout, baker);
        execute(jailor, poisoner);
        witch(witch, jailor, baker); // jailor will visit baker, since executing all targets already
        endNight();

        isDead(poisoner);
        TestLookout.seen(lookout, jailor);
        assertUseableBreadCount(1, jailor);

        jail(jailor, baker);
        jail(jailor, lookout);
        assertActionSize(2, jailor);

        skipDay();

        JailCreate jCard = (JailCreate) jailor.getPlayer().getAbility(JailCreate.abilityType);
        assertTrue(jCard.getJailedTargets().contains(baker.getPlayer()));
        assertTrue(jCard.getJailedTargets().contains(lookout.getPlayer()));

        // force another execution
        // witch has a bread from the start of the game
        witch(witch, jailor, baker);
        witch(witch, jailor, jailor);
        endNight();

        isDead(baker, lookout);
    }

    public void testSwitchingAndInvulnerable() {
        Controller jailor = addPlayer(BasicRoles.Jailor());
        Controller bd = addPlayer(BasicRoles.BusDriver());
        Controller captive = addPlayer(BasicRoles.ElectroManiac());
        addPlayer(BasicRoles.Arsonist());

        jail(jailor, captive);
        skipDay();

        drive(bd, jailor, captive);
        execute(jailor, captive);
        endNight();

        isAlive(jailor);
        isDead(captive);
    }

    public void testExecutionBlock() {
        setCharges(1);

        Controller jailor = addPlayer(BasicRoles.Jailor());
        Controller consort = addPlayer(BasicRoles.Consort());
        Controller fodder = addPlayer(BasicRoles.DrugDealer());

        jail(jailor, fodder);
        skipDay();

        execute(jailor, fodder);
        setTarget(consort, jailor);
        endNight();

        isAlive(fodder);
        assertRealChargeRemaining(1, jailor, JailExecute.abilityType);
    }

    public void testFakeBlock() {
        setCharges(1);

        Controller jailor = addPlayer(BasicRoles.Jailor());
        Controller dealer = addPlayer(BasicRoles.DrugDealer());
        Controller consort = addPlayer(BasicRoles.Consort());
        addPlayer(BasicRoles.Executioner());

        jail(jailor, consort);
        assertIsDay();
        skipDay();

        execute(jailor, consort);
        drug(dealer, jailor, DrugDealer.BLOCKED);
        endNight();

        isDead(consort);
        assertPerceivedChargeRemaining(0, jailor, JailExecute.abilityType);
    }

    public void testDoctorHeal() {
        Controller doc = addPlayer(BasicRoles.Doctor());
        Controller jailor = addPlayer(BasicRoles.Jailor());
        Controller fodder = addPlayer(BasicRoles.Cultist());
        Controller bg = addPlayer(BasicRoles.Bodyguard());

        jail(jailor, fodder);
        skipDay();

        execute(jailor, fodder);
        setTarget(doc, fodder);
        setTarget(bg, fodder);
        endNight();

        isDead(fodder);
        isAlive(bg, jailor);
        assertGameOver();
    }

    // former captives will always see jailor as 'jailor' and not the dead full role
    // name
    public void testJailChatKeys() {
        Controller jailor = addPlayer(BasicRoles.Jailor());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller agent = addPlayer(BasicRoles.Agent());
        Controller gf = addPlayer(BasicRoles.Godfather());

        jail(jailor, cit);
        skipDay();

        String chatKey = JailCreate.KeyCreator(cit, game.getDayNumber());

        ArrayList<String> chatKeys = gf.getChatKeys();
        assertTrue(chatKeys.contains(Constants.VOID_CHAT));
        assertTrue(chatKeys.contains(gf.getColor()));

        chatKeys = cit.getChatKeys();
        assertTrue(chatKeys.contains(Constants.VOID_CHAT));
        assertTrue(chatKeys.contains(chatKey));

        chatKeys = jailor.getChatKeys();
        assertTrue(chatKeys.contains(Constants.VOID_CHAT));
        assertTrue(chatKeys.contains(chatKey));

        say(jailor, gibberish(), chatKey);
        ChatMessage cm = (ChatMessage) game.getEventManager().getNightLog(chatKey).getEvents().getLast();
        assertNotNull(cm.message);
        assertNotNull(cm.getSender());
        partialContains(jailor, gibberish);
        partialContains(cit, gibberish);
        partialExcludes(agent, gibberish);

        String jailorLook = cm.access(jailor.getPlayer());
        assertTrue(jailorLook.startsWith("You"));

        String captiveLook = cm.access(cit.getPlayer());
        assertTrue(captiveLook.startsWith(JailCreate.CAPTOR));

        String privateLook = cm.access(Message.PRIVATE);
        assertTrue(privateLook.startsWith(jailor.getName()));

        say(cit, gibberish(), chatKey);
        partialContains(jailor, gibberish);
        partialContains(cit, gibberish);
        partialExcludes(agent, gibberish);

        cm = (ChatMessage) game.getEventManager().getNightLog(chatKey).getEvents().getLast();
        jailorLook = cm.access(jailor, game);
        assertTrue(jailorLook.startsWith(cit.getName()));

        captiveLook = cm.access(cit, game);
        assertTrue(captiveLook.startsWith("You"));

        say(jailor, "okee", chatKey);
        cm = (ChatMessage) game.getEventManager().getNightLog(chatKey).getEvents().getLast();

        mafKill(agent, jailor);
        endNight();

        partialContains(Command.getText(game.getCommands()), Constants.END_NIGHT, 4);

        captiveLook = cm.access(cit, game);
        assertTrue(captiveLook.startsWith(JailCreate.CAPTOR));

        try{
            say(cit, "okee", chatKey);
            fail();
        }catch(ChatException e){
        }

        skipDay();

        try{
            say(cit, "okee", chatKey);
            fail();
        }catch(ChatException e){
        }
    }

    public void testAttackOnCaptive() {
        Controller jailor = addPlayer(BasicRoles.Jailor());
        Controller captive = addPlayer(BasicRoles.Chauffeur());
        Controller sk = addPlayer(BasicRoles.SerialKiller());

        jail(jailor, captive);
        skipDay();

        setTarget(sk, captive);
        endNight();

        isAlive(captive);
    }

    public void testJailorSwitchingToTeamKill() {
        Controller kidnapper = addPlayer(BasicRoles.Kidnapper());
        Controller captive = addPlayer(BasicRoles.Goon());
        addPlayer(BasicRoles.CultLeader());

        jail(kidnapper, captive);
        skipDay();

        execute(kidnapper, captive);
        mafKill(kidnapper, captive);
        assertFalse(kidnapper.getPlayer().getActions().getActions(FactionSend.abilityType).isEmpty());
        execute(kidnapper, captive);
    }

    public void testAIMass() {
        for(int i = 0; i < 10; i++){
            addPlayer(BasicRoles.Baker());
        }
        for(int i = 0; i < 5; i++){
            addPlayer(BasicRoles.Kidnapper());
        }

        removeTeamAbility(BasicRoles.Coward().getColor(), FactionKill.abilityType);

        long seed = new Random().nextLong();
        // long seed = Long.parseLong("5043824732104431967");
        System.out.println("Jailor Loop: " + seed);

        game.setSeed(seed);
        editRule(SetupModifierName.BREAD_PASSING, false);

        nightStart();

        Brain.EndGame(game, seed);
    }

    // jailing teammates don't allow
    public void testEnclosingChat() {
        addPlayer(BasicRoles.Citizen());
        Controller cit2 = addPlayer(BasicRoles.Citizen());
        Controller k1 = addPlayer(BasicRoles.Kidnapper());
        Controller k2 = addPlayer(BasicRoles.Kidnapper());
        addPlayer(BasicRoles.Goon());

        jail(k1, cit2);
        skipDay();

        execute(k1, cit2);
        endNight();

        jail(k2, k1);
        skipDay();
    }

    public void testJailedTargetsMemoryCleared() {
        Controller kidnapper = addPlayer(BasicRoles.Kidnapper());
        Controller b1 = addPlayer(BasicRoles.Baker());
        Controller b2 = addPlayer(BasicRoles.Baker());
        Controller b3 = addPlayer(BasicRoles.Baker());
        Controller f1 = addPlayer(BasicRoles.Framer());

        setTarget(b1, kidnapper);
        setTarget(b2, kidnapper);
        setTarget(b3, kidnapper);

        endNight();

        jail(kidnapper, b2);
        jail(kidnapper, f1);
        jail(kidnapper, b3);
        jail(kidnapper, b1);

        nextDay();
        nextDay();
    }

    public void testBurningCaptiveInJail() {
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller baker = addPlayer(BasicRoles.Baker());
        Controller doctor = addPlayer(BasicRoles.Doctor());
        Controller jailor = addPlayer(BasicRoles.Jailor());
        Controller arsonist = addPlayer(BasicRoles.Arsonist());
        Controller arsonist2 = addPlayer(BasicRoles.Arsonist());

        setTarget(arsonist, cit);
        setTarget(baker, jailor);
        setTarget(arsonist2, baker);
        endNight();

        jail(jailor, cit);
        jail(jailor, baker);
        skipDay();

        setTarget(doctor, baker);
        burn(arsonist);
        endNight();

        isAlive(baker);
        isDead(cit);
    }

    public void testFactionKill() {
        Controller as = addPlayer(BasicRoles.Armorsmith());
        Controller kidnapper = addPlayer(BasicRoles.Kidnapper());
        Controller fodder = addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.ElectroManiac());

        jail(kidnapper, as);
        skipDay();

        execute(kidnapper, as);
        mafKill(kidnapper, fodder);

        endNight();

        isAlive(as);
        isDead(fodder);
    }

    public void testTryingToOverExecute() {
        setCharges(1);

        Controller jailor = addPlayer(BasicRoles.Jailor());
        Controller baker = addPlayer(BasicRoles.Baker());
        Controller witch = addPlayer(BasicRoles.Witch());

        setTarget(baker, jailor);
        endNight();

        jail(jailor, baker);
        jail(jailor, witch);
        skipDay();

        try{
            setTarget(jailor, baker, witch);
            fail();
        }catch(PlayerTargetingException e){
        }
    }

    public void testRemoveFromNightChats() {
        Controller fodder = addPlayer(BasicRoles.Citizen());
        Controller jailor = addPlayer(BasicRoles.Jailor());
        Controller gf = addPlayer(BasicRoles.Godfather());
        Controller agent = addPlayer(BasicRoles.Agent());
        addPlayer(BasicRoles.Agent());

        jail(jailor, gf);
        skipDay();

        assertEquals(2, game.getEventManager().getNightLog(gf.getColor()).getMembers().size());

        say(agent, gibberish(), agent.getColor());
        partialExcludes(gf, gibberish);
        ChatMessage factionChat = (ChatMessage) game.getEventManager().getNightLog(gf.getColor()).getEvents().getLast();

        mafKill(agent, fodder);
        partialExcludes(gf, "will kill");

        FactionChat fc = (FactionChat) game.getEventManager().getNightLog(gf.getColor());
        for(Message m: fc.getEvents()){
            assertFalse(m.hasAccess(gf, game));
        }

        endNight();
        partialExcludes(gf, gibberish);

        jail(jailor, gf);
        skipDay();

        partialExcludes(gf, gibberish);

        nextNight();

        assertTrue(factionChat.hasAccess(gf.getPlayer()));
        for(Message m: fc.getEvents())
            assertTrue(m.hasAccess(gf.getPlayer()));

        endNight();

        jail(jailor, gf);
        skipDay();

        assertTrue(factionChat.hasAccess(gf.getPlayer()));
    }

    public void testJailedTargetsClearingUponDeath() {
        Controller baker = addPlayer(BasicRoles.Baker());
        Controller baker2 = addPlayer(BasicRoles.Baker());
        Controller killer = addPlayer(BasicRoles.Godfather());
        Controller jailor = addPlayer(BasicRoles.Jailor());
        Controller fodder = addPlayer(BasicRoles.Citizen());

        setTarget(baker2, jailor);
        setTarget(baker, jailor);
        endNight();

        jail(jailor, fodder);///
        jail(jailor, baker);
        jail(jailor, baker2);
        assertActionSize(3, jailor);
        skipDay();

        mafKill(killer, jailor);
        endNight();

        skipDay();
    }

    public void testWitchJailorActionsSorted() {
        GameService.upsertModifier(game, GameModifierName.AUTO_PARITY, true);
        Controller witch = addPlayer(BasicRoles.Witch());
        Controller kidnapper = addPlayer(BasicRoles.Kidnapper());
        Controller jailed = addPlayer(BasicRoles.Doctor());
        Controller goon = addPlayer(BasicRoles.Goon());

        jail(kidnapper, jailed);
        skipVote(kidnapper);
        send(kidnapper, goon);
        witch(witch, kidnapper, kidnapper);
        endNight();
    }

    public void testJailorFeedbackTest() {
        Controller baker = addPlayer(BasicRoles.Baker());
        Controller k1 = addPlayer(BasicRoles.Kidnapper());
        Controller k2 = addPlayer(BasicRoles.Kidnapper());
        addPlayer(BasicRoles.Kidnapper());

        dayStart();

        jail(k1, k2);
        skipDay();

        setTarget(baker, k2);
        nextNight();
        endNight();

        jail(k1, k2);
        skipDay();

        assertIsNight();

        try{
            cleanKill(k1, k2);
            fail();
        }catch(PlayerTargetingException e){
        }

        execute(k1, k2);
        endNight();

        isDead(k2);
        assertFalse(k2.getPlayer().getDeathType().getList().contains(Constants.HIDDEN_KILL_FLAG));

        ArrayList<String> events;
        for(Message m: k2.getPlayer().getEvents()){
            events = m.getEnclosingChats(game, k2.getPlayer());
            if(events == null)
                continue;
            assertFalse(events.contains(Feedback.CHAT_NAME));
        }
    }

    public void testCleanOption() {
        Controller jailor = addPlayer(BasicRoles.Jailor());
        Controller baker = addPlayer(BasicRoles.Baker());
        Controller fodder = addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Agent(), 2);

        setCleans(1);

        setTarget(baker, jailor);
        endNight();

        jail(jailor, fodder);
        jail(jailor, baker);
        skipDay();

        cleanKill(jailor, baker);
        endNight();

        isDead(baker);

        assertTrue(baker.getPlayer().getDeathType().getList().contains(Constants.HIDDEN_KILL_FLAG));

        jail(jailor, fodder);
        skipDay();

        try{
            cleanKill(jailor, fodder);
            fail();
        }catch(PlayerTargetingException e){
        }
    }

    private void execute(Controller jailor, Controller... targets) {
        setTarget(jailor, new ControllerList(targets), JailExecute.abilityType);
    }

    // over cleaning what you can actually do
    // test text

    private void cleanKill(Controller j, Controller... t) {
        PlayerList targets = ControllerList.ToPlayerList(game, t);
        List<String> args = Util.toStringList(Integer.toString(t.length));
        Action action = new Action(j.getPlayer(), JailExecute.abilityType, args, targets);
        setTarget(action);
    }

    private void setCleans(int i) {
        TestUtil.modifyAbility(JailExecute.abilityType, AbilityModifierName.JAILOR_CLEAN_ROLES, i);
    }
}
