package integration.logic.abilities;

import game.abilities.Vigilante;
import game.ai.Controller;
import game.logic.exceptions.NarratorException;
import game.logic.support.Constants;
import game.logic.templates.BasicRoles;
import integration.logic.SuperTest;
import models.enums.AbilityModifierName;
import models.enums.SetupModifierName;
import services.FactionRoleModifierService;
import services.SetupModifierService;

public class TestVigilante extends SuperTest {
    public TestVigilante(String s) {
        super(s);
    }

    public void testNoZeroWeightModifier() {
        try{
            FactionRoleModifierService.addModifier(BasicRoles.Vigilante(), AbilityModifierName.ZERO_WEIGHTED,
                    Vigilante.abilityType, false);
            fail();
        }catch(NarratorException e){
        }
    }

    public void testNoBackToBackModifier() {
        try{
            FactionRoleModifierService.addModifier(BasicRoles.Vigilante(), AbilityModifierName.BACK_TO_BACK,
                    Vigilante.abilityType, false);
            fail();
        }catch(NarratorException e){
        }
    }

    public void testCultedVigilanteWithGunsmithGuns() {
        Controller gunsmith = addPlayer(BasicRoles.Gunsmith());
        Controller vigilante = addPlayer(BasicRoles.Vigilante());
        Controller cultLeader = addPlayer(BasicRoles.CultLeader());

        FactionRoleModifierService.addModifier(BasicRoles.Vigilante(), AbilityModifierName.CHARGES,
                Vigilante.abilityType, Constants.UNLIMITED);
        SetupModifierService.upsertModifier(setup, SetupModifierName.CULT_KEEPS_ROLES, false);

        setTarget(gunsmith, vigilante);

        nextNight();

        setTarget(gunsmith, vigilante);
        setTarget(cultLeader, vigilante);
        endNight();

        assertRealGunCount(2, vigilante);
    }
}
