package integration.logic.abilities;

import game.abilities.Armorsmith;
import game.abilities.DrugDealer;
import game.abilities.Survivor;
import game.ai.Controller;
import game.logic.exceptions.PlayerTargetingException;
import game.logic.support.action.Action;
import game.logic.templates.BasicRoles;
import integration.logic.SuperTest;
import models.Role;
import models.enums.AbilityModifierName;
import models.enums.SetupModifierName;

public class TestArmorsmith extends SuperTest {

    public TestArmorsmith(String name) {
        super(name);
    }

    public void testBasicFunction() {
        Controller as = addPlayer(BasicRoles.Armorsmith());
        Controller m1 = addPlayer(BasicRoles.Goon());
        Controller cit = addPlayer(BasicRoles.Citizen());

        realVests();

        try{
            fakeVest(as, m1);
            fail();
        }catch(PlayerTargetingException e){
        }
        setTarget(as, cit);

        endNight();

        assertRealChargeRemaining(1, cit, Survivor.abilityType);

        skipDay();

        assertTrue(cit.getCommands().contains(Survivor.abilityType));
        vest(cit);
        mafKill(m1, cit);

        endNight();

        isAlive(m1);
    }

    private void fakeVest(Controller as, Controller target) {
        setTarget(as, Armorsmith.abilityType, Armorsmith.FAKE, (String) null, target);
    }

    private void realVests() {
        Role role = BasicRoles.Armorsmith().role;
        modifyRole(role, AbilityModifierName.AS_FAKE_VESTS, false);
    }

    private void fakeVests() {
        Role role = BasicRoles.Armorsmith().role;
        modifyRole(role, AbilityModifierName.AS_FAKE_VESTS, true);
    }

    public void testForceWitchUse() {
        Controller as = addPlayer(BasicRoles.Armorsmith());
        Controller bd = addPlayer(BasicRoles.BusDriver());
        Controller witch = addPlayer(BasicRoles.Witch());

        witch(witch, as, bd);
        nextNight();

        witch(witch, bd, bd);
        endNight();

        assertFalse(bd.getCommands().contains(Survivor.abilityType));
    }

    public void testForceWitchUseWitch() {
        Controller as = addPlayer(BasicRoles.Armorsmith());
        Controller witch = addPlayer(BasicRoles.Witch());
        Controller witch2 = addPlayer(BasicRoles.Witch());

        nightStart();
        witch(witch, as, witch2);

        nextNight();
        assertTrue(witch2.getCommands().contains(Survivor.abilityType));

        witch(witch, witch2, witch2);
        endNight(witch);

        endNight();
        skipDay();
        assertFalse(witch2.getCommands().contains(Survivor.abilityType));
    }

    public void testFakeArmorUse() {
        Controller as = addPlayer(BasicRoles.Armorsmith());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller dd = addPlayer(BasicRoles.DrugDealer());
        Controller sk = addPlayer(BasicRoles.SerialKiller());

        nightStart();

        Action armor_action = getVestAction(cit);

        assertFalse(cit.getPlayer().isAcceptableTarget(armor_action));

        drug(dd, cit, DrugDealer.VEST);
        setTarget(as, cit);

        endNight();

        assertTotalVestCount(2, cit);
        assertFakeVestCount(1, cit);
        assertRealVestCount(1, cit);

        assertTrue(getVest(cit, 0).isReal());
        assertTrue(getVest(cit, 1).isFake());

        skipDay();

        assertTrue(cit.getPlayer().isAcceptableTarget(armor_action));

        setTarget(sk, cit);
        vest(cit);

        endNight();
        isAlive(cit);
        assertTotalVestCount(1, cit);
        assertRealVestCount(0, cit);
        assertFakeVestCount(1, cit);

        skipDay();
        assertTrue(cit.getPlayer().isAcceptableTarget(armor_action));

        setTarget(sk, cit);
        vest(cit);

        endNight();

        isDead(cit);
    }

    public void testFakeArmor() {
        fakeVests();

        Controller as = addPlayer(BasicRoles.Armorsmith());
        Controller fodder = addPlayer(BasicRoles.Citizen());
        Controller sk = addPlayer(BasicRoles.SerialKiller());
        addPlayer(BasicRoles.Amnesiac());

        setTarget(as, fodder, Armorsmith.abilityType);
        assertEquals(Armorsmith.REAL, as.getPlayer().getActions().getFirst().getArg1());

        fakeVest(as, fodder);
        endNight();

        assertTotalVestCount(1, fodder);
        assertFakeVestCount(1, fodder);
        assertTrue(getVest(fodder, 0).isFake());

        skipDay();

        vest(fodder);
        assertNotNull(fodder.getPlayer().getActions().mainAction);
        setTarget(sk, fodder);
        endNight();

        isDead(fodder);
    }

    public void testCharges() {
        modifyAbilityCharges(BasicRoles.Armorsmith(), Armorsmith.abilityType, 1);

        Controller as = addPlayer(BasicRoles.Armorsmith());
        Controller cit = addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Arsonist());

        editRule(SetupModifierName.CHARGE_VARIABILITY, 0);

        setTarget(as, cit);
        nextNight();

        assertFalse(as.getCommands().contains(Armorsmith.abilityType));
    }

    public void testCooldown() {
        modifyAbilityCooldown(BasicRoles.Armorsmith(), Armorsmith.abilityType, 1);

        Controller as = addPlayer(BasicRoles.Armorsmith());
        Controller cit = addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Arsonist());

        editRule(SetupModifierName.CHARGE_VARIABILITY, 0);

        setTarget(as, cit);
        nextNight();

        assertFalse(as.getCommands().contains(Armorsmith.abilityType));

        nextNight();

        assertTrue(as.getCommands().contains(Armorsmith.abilityType));
    }
}
