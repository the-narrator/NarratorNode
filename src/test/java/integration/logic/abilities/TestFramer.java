package integration.logic.abilities;

import java.util.ArrayList;

import game.abilities.Framer;
import game.abilities.Sheriff;
import game.ai.Controller;
import game.event.Announcement;
import game.event.Message;
import game.logic.templates.BasicRoles;
import game.setups.Setup;
import integration.logic.SuperTest;

public class TestFramer extends SuperTest {

    public TestFramer(String name) {
        super(name);
    }

    public void testFramer() {
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller sheriff = addPlayer(BasicRoles.Sheriff());
        Controller framer = addPlayer(BasicRoles.Framer());

        nightStart();

        setTarget(sheriff, cit);
        frame(framer, cit, framer.getColor());

        endNight();

        ArrayList<Object> list = Sheriff.generateFeedback(game.getFaction(framer.getColor()), game);

        Message om = new Announcement(game).add(list);

        partialContains(sheriff, om.access(sheriff.getPlayer()));
    }

    public void testBakerFramer() {
        Controller baker = addPlayer(BasicRoles.Baker());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller sheriff = addPlayer(BasicRoles.Sheriff());
        Controller sheriff2 = addPlayer(BasicRoles.Sheriff());
        Controller framer = addPlayer(BasicRoles.Framer());
        addPlayer(BasicRoles.SerialKiller());
        addPlayer(BasicRoles.MassMurderer());

        setTarget(baker, framer);

        nextNight();

        frame(framer, cit, BasicRoles.SerialKiller().getColor());
        frame(framer, baker, BasicRoles.MassMurderer().getColor());
        setTarget(sheriff, cit);
        setTarget(sheriff2, baker);

        endNight();

        ArrayList<Object> list = Sheriff.generateFeedback(game.getFaction(BasicRoles.MassMurderer().getColor()), game);

        Message om = new Announcement(game).add(list);

        partialContains(sheriff2, om.access(sheriff2.getPlayer()));

        list = Sheriff.generateFeedback(game.getFaction(BasicRoles.SerialKiller().getColor()), game);

        om = new Announcement(game).add(list);

        partialContains(sheriff, om.access(sheriff.getPlayer()));

    }

    // multiple framers going around
    // vet, agent, detect, lookout
    public void testVetVisit() {
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller detect = addPlayer(BasicRoles.Detective());
        Controller doc = addPlayer(BasicRoles.Doctor());
        Controller lookout = addPlayer(BasicRoles.Lookout());
        Controller vet = addPlayer(BasicRoles.Veteran());
        Controller framer = addPlayer(BasicRoles.Framer());
        Controller killer = addPlayer(BasicRoles.Goon());

        nightStart();
        frame(framer, cit, framer.getColor());
        mafKill(killer, vet);
        setTarget(detect, cit);
        setTarget(lookout, vet);
        setTarget(doc, lookout);
        setTarget(vet);

        endNight();

        TestLookout.seen(lookout, killer, cit);
        TestDetective.seen(detect, vet);
        isDead(killer);
        isAlive(cit);
    }

    public void testBasicVisit() {

        Controller fodder = addPlayer(BasicRoles.Citizen());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller lookout = addPlayer(BasicRoles.Lookout());
        Controller framer = addPlayer(BasicRoles.Framer());
        Controller killer = addPlayer(BasicRoles.Goon());

        nightStart();
        frame(framer, cit, framer.getColor());
        mafKill(killer, fodder);
        setTarget(lookout, fodder);

        endNight();

        TestLookout.seen(lookout, killer, cit);
    }

    public void testDetectiveVisit() {

        Controller fodder = addPlayer(BasicRoles.Citizen());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller detect = addPlayer(BasicRoles.Detective());
        Controller framer = addPlayer(BasicRoles.Framer());
        Controller killer = addPlayer(BasicRoles.Goon());

        nightStart();
        frame(framer, cit, framer.getColor());
        mafKill(killer, fodder);
        setTarget(detect, cit);

        endNight();

        TestDetective.seen(detect, fodder);
    }

    public void testGfFrameVisit() {
        Controller fodder = addPlayer(BasicRoles.Citizen());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller lookout = addPlayer(BasicRoles.Lookout());
        Controller framer = addPlayer(BasicRoles.Framer());
        Controller killer = addPlayer(BasicRoles.Godfather());

        frame(framer, cit, BasicRoles.Framer().getColor());
        mafKill(killer, fodder);
        setTarget(lookout, fodder);

        endNight();

        TestLookout.seen(lookout, cit);
    }

    public void testMultiFramerVisit() {
        Controller gFodder = addPlayer(BasicRoles.Gunsmith());
        Controller dFodder = addPlayer(BasicRoles.Doctor());
        Controller cit1 = addPlayer(BasicRoles.Citizen());
        Controller cit2 = addPlayer(BasicRoles.Citizen());
        Controller lookout1 = addPlayer(BasicRoles.Lookout());
        Controller lookout2 = addPlayer(BasicRoles.Lookout());
        Controller framer1 = addPlayer(BasicRoles.Framer());
        Controller godfather1 = addPlayer(BasicRoles.Godfather());
        Controller framer2 = addPlayer(BasicRoles.getMember(Setup.YAKUZA_C, Framer.abilityType));
        Controller godfather2 = addPlayer(BasicRoles.Godfather(Setup.YAKUZA_C));

        nightStart();
        assertTrue(!godfather2.getPlayer().isDetectable());

        setTarget(lookout1, gFodder);
        setTarget(lookout2, dFodder);
        frame(framer1, cit1, framer2.getColor());
        frame(framer2, cit2, framer2.getColor());
        mafKill(godfather1, gFodder);
        mafKill(godfather2, dFodder);

        endNight();
        TestLookout.seen(lookout2, cit2);
        TestLookout.seen(lookout1, cit1);
    }
}
