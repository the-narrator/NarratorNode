package integration.logic.abilities;

import game.abilities.Assassin;
import game.abilities.Citizen;
import game.abilities.GameAbility;
import game.abilities.Snitch;
import game.ai.Controller;
import game.logic.templates.BasicRoles;
import game.setups.Setup;
import integration.logic.SuperTest;
import models.Faction;
import models.Role;
import services.FactionRoleService;
import services.RoleService;

public class TestInvestigator extends SuperTest {

    public TestInvestigator(String s) {
        super(s);
    }

    public void testBasicAbility() {
        addPlayer(BasicRoles.Citizen(), 2);
        Controller invest = addPlayer(BasicRoles.Investigator());
        Controller ass = addPlayer(BasicRoles.Assassin());

        setTarget(invest, ass);

        endNight();

        seen(invest, Assassin.class);
    }

    public static void seen(Controller p, Class<? extends GameAbility> c) {
        partialContains(p, c.getSimpleName() + ".");
    }

    public static void notSeen(Controller p, Class<? extends GameAbility> c) {
        partialExcludes(p, c.getSimpleName() + ".");
    }

    public void testUndetectableSelection() {
        Controller invest = addPlayer(BasicRoles.Investigator());
        Controller gf = addPlayer(BasicRoles.Godfather());
        addPlayer(BasicRoles.Citizen());

        setTarget(invest, gf);
        endNight();

        partialContains(invest, Citizen.class.getSimpleName());
    }

    public void testUndetectableSelectionNewName() {
        Role role = RoleService.createRole(setup, "Villager");
        Faction faction = setup.getFactionByColor(Setup.TOWN_C);
        Controller invest = addPlayer(BasicRoles.Investigator());
        Controller gf = addPlayer(BasicRoles.Godfather());
        addPlayer(FactionRoleService.createFactionRole(faction, role));

        nightStart();

        assertFalse(gf.getPlayer().isDetectable());
        assertNotNull(Snitch.getCitizenTeam(game));

        setTarget(invest, gf);
        endNight();

        partialContains(invest, "Villager");
    }

    public void testFramedTarget() {
        Controller snitch = addPlayer(BasicRoles.Snitch());
        Controller invest = addPlayer(BasicRoles.Investigator());
        Controller gf = addPlayer(BasicRoles.Godfather());
        Controller framer = addPlayer(BasicRoles.Framer());

        setTarget(invest, gf);
        frame(framer, gf, snitch.getColor());
        endNight();

        partialContains(invest, Snitch.class.getSimpleName() + ".");
    }
}
