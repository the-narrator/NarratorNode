package integration.logic.abilities;

import game.abilities.Coroner;
import game.abilities.Douse;
import game.abilities.GraveDigger;
import game.abilities.SerialKiller;
import game.ai.Controller;
import game.event.Feedback;
import game.logic.exceptions.PlayerTargetingException;
import game.logic.templates.BasicRoles;
import integration.logic.SuperTest;
import models.enums.SetupModifierName;

public class TestCoroner extends SuperTest {

    public TestCoroner(String name) {
        super(name);
    }

    public void testBasicAbility() {
        Controller coroner = addPlayer(BasicRoles.Coroner());
        Controller vig = addPlayer(BasicRoles.Vigilante());
        Controller witch2 = addPlayer(BasicRoles.Witch());
        Controller witch = addPlayer(BasicRoles.Witch());
        Controller sk = addPlayer(BasicRoles.SerialKiller());

        editRule(SetupModifierName.CORONER_LEARNS_ROLES, false);

        voteOut(sk, witch, witch2, coroner);

        try{
            setTarget(coroner, vig);
            fail();
        }catch(PlayerTargetingException e){
        }

        setTarget(coroner, sk);
        setTarget(coroner, sk);

        assertActionSize(1, coroner);
        assertEquals(1, coroner.getPlayer().getAction(Coroner.abilityType)._targets.size());

        witch(witch, vig, witch);
        endNight();

        isDead(witch);
        partialContains(coroner, SerialKiller.ROLE_NAME + ".");

        skipDay();

        setTarget(coroner, witch);
        endNight();

        partialContains(coroner, witch.getPlayer().getRoleName() + ".");
    }

    public void testExhuming() {
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller coroner = addPlayer(BasicRoles.Coroner());
        Controller gd = addPlayer(BasicRoles.GraveDigger());
        Controller arso = addPlayer(BasicRoles.Arsonist());

        editRule(SetupModifierName.CORONER_EXHUMES, true);

        voteOut(arso, gd, coroner, cit);

        assertTrue(GraveDigger.isBannedAction(Coroner.abilityType));
        assertEquals(4, gd.getPlayer().getAbility(GraveDigger.class).getOptions(gd.getPlayer()).size());

        setTarget(gd, GraveDigger.abilityType, Douse.COMMAND, arso, coroner);
        setTarget(coroner, arso);

        endNight();

        assertStatus(coroner, Douse.abilityType, false);
    }

    public void testCoroRoleHistory() {
        Controller coroner = addPlayer(BasicRoles.Coroner());
        Controller witch = addPlayer(BasicRoles.Witch());
        Controller jan = addPlayer(BasicRoles.Janitor());
        Controller jan2 = addPlayer(BasicRoles.Janitor());
        Controller cit = addPlayer(BasicRoles.Citizen());

        editRule(SetupModifierName.CORONER_LEARNS_ROLES, true);

        setTarget(jan, cit);
        setTarget(jan2, cit);
        nextNight();

        assertEquals(1, cit.getPlayer().getRoleVisits().size());

        witch(witch, cit, jan);
        endNight();

        assertEquals(2, cit.getPlayer().getRoleVisits().size());

        voteOut(cit, coroner, witch, jan);
        setTarget(coroner, cit);
        endNight();

        String text;
        for(Feedback f: getFeedback(coroner.getPlayer(), game.getDayNumber() - 1)){
            text = f.access(coroner, game);
            if(text.contains(BasicRoles.Witch().getName()) && text.contains(BasicRoles.Janitor().getName()))
                return;
        }
        fail("Feedback failed");
    }
}