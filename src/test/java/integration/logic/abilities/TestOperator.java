package integration.logic.abilities;

import game.abilities.Driver;
import game.abilities.ElectroManiac;
import game.abilities.Operator;
import game.abilities.Sheriff;
import game.ai.Controller;
import game.ai.ControllerList;
import game.logic.support.Constants;
import game.logic.templates.BasicRoles;
import integration.logic.SuperTest;
import models.enums.AbilityModifierName;

public class TestOperator extends SuperTest {

    public TestOperator(String name) {
        super(name);
    }

    Controller operator;

    @Override
    public void roleInit() {
        operator = addPlayer(BasicRoles.Operator());
    }

    public void testBasic() {
        Controller sheriff = addPlayer(BasicRoles.Sheriff());
        Controller f1 = addPlayer(BasicRoles.Citizen());
        Controller f2 = addPlayer(BasicRoles.Citizen());
        Controller sk = addPlayer(BasicRoles.SerialKiller());

        tele(operator, sheriff, sk);
        setTarget(sheriff, f1);
        setTarget(sk, f2);
        endNight();

        isDead(f1);

        partialContains(sheriff, Operator.FEEDBACK);
        partialContains(sk, Operator.FEEDBACK);

        isDead(f1);
        isAlive(f2);
    }

    public void testMMInteraction() {
        Controller f = addPlayer(BasicRoles.Citizen());
        Controller mm = addPlayer(BasicRoles.MassMurderer());
        Controller s2 = addPlayer(BasicRoles.Sheriff());
        Controller s1 = addPlayer(BasicRoles.Sheriff());

        setTarget(s1, f);
        setTarget(s2, mm);
        setTarget(mm, f);
        tele(operator, s1, s2);

        endNight();

        isDead(f, s2);
        isAlive(s1, operator);
    }

    public void testGunSwitch() {
        Controller gs = addPlayer(BasicRoles.Gunsmith());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller bp = addPlayer(BasicRoles.Bulletproof());
        Controller sk = addPlayer(BasicRoles.SerialKiller());

        modifyRole(BasicRoles.Gunsmith().role, AbilityModifierName.GS_FAULTY_GUNS, false);

        setTarget(gs, cit);
        nextNight();

        tele(operator, cit, sk);
        shoot(cit, bp);
        setTarget(sk, gs);
        endNight();

        isDead(gs);
        assertEquals(Constants.GUN_KILL_FLAG, gs.getPlayer().getDeathType().getList().get(0));
    }

    public void testDoubleSingleBD() {
        Controller baker1 = addPlayer(BasicRoles.Baker());
        Controller baker2 = addPlayer(BasicRoles.Baker());
        Controller bd = addPlayer(BasicRoles.BusDriver());
        Controller sk = addPlayer(BasicRoles.SerialKiller());

        drive(bd, baker1, baker2);
        setTarget(sk, baker2);
        tele(operator, sk, bd);
        endNight();

        assertEquals(1, game.getDeadSize());
        partialExcludes(baker1, Driver.FEEDBACK);
        partialExcludes(baker2, Driver.FEEDBACK);

    }

    public void testDoubleWithTwoSingles() {
        Controller baker = addPlayer(BasicRoles.Baker());
        Controller sher = addPlayer(BasicRoles.Sheriff());
        Controller gs = addPlayer(BasicRoles.Gunsmith());
        Controller bd = addPlayer(BasicRoles.BusDriver());
        Controller sk = addPlayer(BasicRoles.SerialKiller());

        modifyRole(BasicRoles.Gunsmith().role, AbilityModifierName.GS_FAULTY_GUNS, false);

        setTarget(gs, sher);
        setTarget(baker, sher);
        nextNight();

        drive(bd, bd, sk);
        shoot(sher, baker);
        setTarget(sher, gs, Sheriff.abilityType);
        tele(operator, sher, bd);
        endNight(operator);
        endNight();

        isDead(bd);
        isAlive(baker);

        TestSheriff.seen(sher, sk.getColor());
    }

    public void testDontMessWithSend() {
        Controller busDriver = addPlayer(BasicRoles.BusDriver());
        Controller goon1 = addPlayer(BasicRoles.Goon());
        Controller goon2 = addPlayer(BasicRoles.Goon());

        drive(busDriver, goon1, goon2);
        mafKill(goon1, goon2);
        tele(operator, goon1, busDriver);
        endNight();

        // m1 has m2 and m1
        // m1 is going to kill himself

        partialExcludes(goon1, Driver.FEEDBACK);
        partialExcludes(goon2, Driver.FEEDBACK);
        isDead(goon1);
        isAlive(goon2);
    }

    public void testSpyInteraction() {
        Controller em = addPlayer(BasicRoles.ElectroManiac());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller spy = addPlayer(BasicRoles.Spy());
        Controller goon = addPlayer(BasicRoles.Goon());

        setTarget(em, ControllerList.list(cit, goon), ElectroManiac.abilityType);
        spy(spy, goon.getColor());
        tele(operator, em, spy);
        endNight();

        // should throw an error in old implementation
    }

    public void testAllowSelfTarget() {
        Controller spy = addPlayer(BasicRoles.Spy());
        addPlayer(BasicRoles.Goon());

        tele(operator, spy, operator);
    }
}
