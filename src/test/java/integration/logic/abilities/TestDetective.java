package integration.logic.abilities;

import java.util.ArrayList;

import game.abilities.BreadAbility;
import game.abilities.Detective;
import game.abilities.SerialKiller;
import game.ai.Controller;
import game.ai.ControllerList;
import game.event.Feedback;
import game.logic.Player;
import game.logic.templates.BasicRoles;
import integration.logic.SuperTest;
import models.enums.SetupModifierName;

public class TestDetective extends SuperTest {

    public TestDetective(String name) {
        super(name);
    }

    public void testBasicFunction() {
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller cit2 = addPlayer(BasicRoles.Citizen());
        Controller detec = addPlayer(BasicRoles.Detective());
        Controller detec2 = addPlayer(BasicRoles.Detective());
        Controller detec3 = addPlayer(BasicRoles.Detective());
        Controller maf = addPlayer(BasicRoles.Goon());
        Controller sk = addPlayer(BasicRoles.SerialKiller());

        nightStart();

        assertTrue(sk.is(SerialKiller.abilityType));

        setTarget(detec, maf);
        endNight(detec);
        assertTrue(detec.getPlayer().endedNight());
        setTarget(detec2, cit);
        mafKill(maf, sk);

        assertEquals(1, game.getEndedNightPeople().size());
        assertEquals(detec, game.getEndedNightPeople().getFirst());
        endNight();

        isAlive(sk);
        seen(detec, sk);
        partialContains(detec2, Detective.NO_VISIT);

        skipDay();

        setTarget(detec3, detec2);
        setTarget(detec2, sk);
        setTarget(sk, cit2);

        endNight();

        seen(detec3, sk);

    }

    public void testVisitingUndetectablePerson() {
        Controller detect = addPlayer(BasicRoles.Detective());
        Controller sherif = addPlayer(BasicRoles.Sheriff());
        Controller gf = addPlayer(BasicRoles.Godfather());

        setTarget(sherif, gf);
        setTarget(detect, sherif);
        endNight();

        seen(detect, gf);
    }

    public void testAmnesiacVisitingDead() {
        Controller detective = addPlayer(BasicRoles.Detective());
        Controller vigilante = addPlayer(BasicRoles.Vigilante());
        Controller amnesiac = addPlayer(BasicRoles.Amnesiac());
        Controller goon = addPlayer(BasicRoles.Goon());
        addPlayer(BasicRoles.Goon());

        nightStart();

        shoot(vigilante, goon);
        endNight();
        skipDay();

        setTarget(detective, amnesiac);
        setTarget(amnesiac, goon);
        endNight();

        seen(detective, goon);
    }

    public static void seen(Controller detec, Controller... players) {
        seen(detec, ControllerList.list(players), new ControllerList());
    }

    public static void seen(Controller detect_c, ControllerList playerList, ControllerList alreadySeen) {
        Player detect = detect_c.getPlayer();
        ArrayList<Object> parts = Detective.FeedbackGenerator(detect, playerList.toPlayerList(game),
                alreadySeen.toPlayerList(game));
        Feedback f = new Feedback(game.skipper);
        f.add(parts);
        f.setVisibility(detect);
        String feedback = f.access(detect);
        f.removeVisiblity(detect);
        partialContains(detect, feedback);
    }

    public void testDetective() {
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller detec = addPlayer(BasicRoles.Detective());
        Controller detec2 = addPlayer(BasicRoles.Detective());
        Controller maf = addPlayer(BasicRoles.Goon());
        Controller maf2 = addPlayer(BasicRoles.Goon());

        nightStart();

        // tie
        // setTarget(maf, SEND, null, null, maf);
        // mafKill(maf2, cit);
        setTarget(detec, maf);
        setTarget(detec, maf);
        setTarget(detec2, maf2);
        setTarget(detec2, maf2);
        endNight();

        isAlive(cit, detec, detec2, maf, maf2);
        partialContains(detec, Detective.NO_VISIT);
        partialContains(detec2, Detective.NO_VISIT);

        voteOut(maf2, maf, detec, detec2);

        setTarget(detec, maf);

        endNight();

        partialContains(detec, Detective.NO_VISIT, 2);

        skipDay();

        setTarget(detec, maf);
        endNight();

        partialContains(detec, Detective.NO_VISIT, 3);

    }

    public void testBreadedDetective() {
        Controller detect = addPlayer(BasicRoles.Detective());
        Controller baker = addPlayer(BasicRoles.Baker());
        Controller chauf = addPlayer(BasicRoles.Chauffeur());
        Controller fodder1 = addPlayer(BasicRoles.Agent());
        Controller fodder2 = addPlayer(BasicRoles.Amnesiac());

        editRule(SetupModifierName.FOLLOW_GETS_ALL, false);

        nightStart();

        assertTrue(baker.getCommands().contains(BreadAbility.abilityType));
        assertFalse(baker.getPlayer().getAcceptableTargets(BreadAbility.abilityType).isEmpty());

        setTarget(baker, detect);
        nextNight();

        setTarget(baker, detect);
        nextNight();

        assertPassableBreadCount(2, detect);

        follow(detect, chauf);
        follow(detect, chauf);
        follow(detect, chauf);
        assertActionSize(3, detect);
        drive(chauf, fodder1, fodder2);

        endNight();

        TestDetective.seen(detect, fodder1);
        TestDetective.seen(detect, fodder2);

        // bottom of feedback
        partialContains(detect, Detective.NO_ONE_ELSE);
    }

    private void follow(Controller detective, Controller target) {
        setTarget(detective, target, Detective.abilityType);
    }

    public void testPierceImmunity() {
        addPlayer(BasicRoles.Architect());
        Controller detect = addPlayer(BasicRoles.Detective());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller gf = addPlayer(BasicRoles.Godfather());

        editRule(SetupModifierName.FOLLOW_PIERCES_IMMUNITY, true);

        mafKill(gf, cit);
        assertActionSize(1, gf);
        setTarget(detect, gf);
        endNight();

        seen(detect, cit);
    }
}
