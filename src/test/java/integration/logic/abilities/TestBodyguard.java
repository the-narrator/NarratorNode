package integration.logic.abilities;

import game.abilities.Bodyguard;
import game.abilities.CultLeader;
import game.abilities.Doctor;
import game.abilities.Douse;
import game.abilities.Gunsmith;
import game.abilities.MassMurderer;
import game.abilities.Poisoner;
import game.abilities.SerialKiller;
import game.ai.Controller;
import game.logic.exceptions.PlayerTargetingException;
import game.logic.support.Constants;
import game.logic.templates.BasicRoles;
import integration.logic.SuperTest;
import models.FactionRole;
import models.enums.AbilityType;
import models.enums.SetupModifierName;

public class TestBodyguard extends SuperTest {

    public TestBodyguard(String name) {
        super(name);
    }

    public void testBasicFunction() {
        Controller bg = addPlayer(BasicRoles.Bodyguard());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller killer = addPlayer(BasicRoles.Goon());

        mafKill(killer, cit);
        setTarget(bg, cit);

        endNight();
        isAlive(cit);
        isDead(bg, killer);
        isWinner(bg);
        assertAttackSize(1, bg);
        assertEquals(Constants.BODYGUARD_KILL_FLAG, bg.getPlayer().getDeathType().get(0));
    }

    public void testImmuneKilling() {
        killTest(BasicRoles.SerialKiller(), SerialKiller.abilityType);
    }

    private void killTest(FactionRole m, AbilityType abilityType) {
        Controller bg = addPlayer(BasicRoles.Bodyguard());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller killer = addPlayer(m);

        setTarget(killer, cit, abilityType);
        setTarget(bg, cit);

        endNight();
        isAlive(cit);
        isDead(bg, killer);
        isWinner(bg);
    }

    public void testSelfWitch() {
        Controller bg = addPlayer(BasicRoles.Bodyguard());
        Controller vig = addPlayer(BasicRoles.Vigilante());
        Controller witch = addPlayer(BasicRoles.Witch());

        try{
            setTarget(bg, bg);
            fail();
        }catch(PlayerTargetingException e){
        }

        shoot(vig, witch);
        cancelAction(vig, 0);
        assertActionSize(0, vig);
        shoot(vig, witch);
        witch(witch, bg, witch);

        endNight();

        isWinner(witch);
    }

    public void testDoctorBG() {
        Controller bg = addPlayer(BasicRoles.Bodyguard());
        Controller doc = addPlayer(BasicRoles.Doctor());
        Controller sk = addPlayer(BasicRoles.SerialKiller());
        addPlayer(BasicRoles.Witch());

        nightStart();

        setTarget(doc, bg);
        setTarget(bg, doc);
        setTarget(sk, doc);

        endNight();

        isAlive(bg);
        isAlive(doc);
        isDead(sk);
    }

    /*
     * it is more favorable for you to submit night actions sooner
     */
    public void testSecondKiller() {

        Controller bg = addPlayer(BasicRoles.Bodyguard());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller cit2 = addPlayer(BasicRoles.Citizen());
        Controller vig = addPlayer(BasicRoles.Vigilante());
        Controller witch = addPlayer(BasicRoles.Witch());
        Controller sk = addPlayer(BasicRoles.SerialKiller());

        nightStart();

        shoot(vig, cit);
        endNight(vig);
        setTarget(bg, cit);
        setTarget(sk, cit);

        endNight();

        voteOut(witch, cit2, vig);
    }

    public void testBGTargetSave() {

        Controller bg = addPlayer(BasicRoles.Bodyguard());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller doc = addPlayer(BasicRoles.Doctor());
        Controller maf = addPlayer(BasicRoles.Goon());

        nightStart();

        mafKill(maf, cit);
        setTarget(bg, cit);
        setTarget(doc, maf);

        endNight();

        isAlive(maf);
    }

    public void testDoubleKillers() {
        // even though the sk is invulnerable, the bodyguard will still trigger
        Controller bg1 = addPlayer(BasicRoles.Bodyguard());
        Controller bg2 = addPlayer(BasicRoles.Bodyguard());
        Controller vig = addPlayer(BasicRoles.Vigilante());
        Controller sk = addPlayer(BasicRoles.SerialKiller());
        addPlayer(BasicRoles.Citizen());

        nightStart();

        shoot(vig, sk);
        setTarget(sk, vig);
        setTarget(bg1, sk);
        setTarget(bg2, vig);

        endNight();

        isDead(bg1, bg2, vig, sk);
        assertAttackSize(1, sk);
        assertAttackSize(1, vig);
    }

    public void testBGChaining() {
        Controller bg = addPlayer(BasicRoles.Bodyguard());
        Controller bg2 = addPlayer(BasicRoles.Bodyguard());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller goon = addPlayer(BasicRoles.Goon());
        addPlayer(BasicRoles.Citizen());

        nightStart();

        mafKill(goon, cit);
        setTarget(bg, cit);
        endNight(bg);
        setTarget(bg2, bg);

        endNight();

        isDead(goon, bg2);
        isAlive(cit, bg);
    }

    /*
     * kill goes through even in death
     */
    public void testBGDeathAction() {
        Controller bg = addPlayer(BasicRoles.Bodyguard());
        Controller vig = addPlayer(BasicRoles.Vigilante());
        Controller maf = addPlayer(BasicRoles.Goon());
        Controller sk2 = addPlayer(BasicRoles.SerialKiller());
        Controller sk1 = addPlayer(BasicRoles.SerialKiller());

        nightStart();

        mafKill(maf, vig);
        endNight(maf);

        setTarget(sk1, bg);
        setTarget(sk2, bg);
        setTarget(bg, vig);

        endNight();
        isDead(bg, maf);
        isAlive(vig, sk1, sk2);
    }

    public void testMMProtection() {
        Controller bg = addPlayer(BasicRoles.Bodyguard());
        Controller mm = addPlayer(BasicRoles.MassMurderer());
        addPlayer(BasicRoles.MassMurderer());

        nightStart();

        setTarget(bg, mm);
        setTarget(mm, mm);

        endNight();

        isDead(bg);
        isAlive(mm);
    }

    public void testBodyguardProtectingCorrectAttacks() {
        Controller bg = addPlayer(BasicRoles.Bodyguard());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller baker = addPlayer(BasicRoles.Baker());
        Controller gunsmith = addPlayer(BasicRoles.Gunsmith());
        Controller mm = addPlayer(BasicRoles.MassMurderer());
        addPlayer(BasicRoles.Witch());

        setTarget(baker, mm);
        setTarget(gunsmith, mm);

        nextNight();

        setTarget(mm, cit, MassMurderer.abilityType);
        shoot(mm, baker);
        setTarget(gunsmith, cit, Gunsmith.abilityType);
        setTarget(bg, gunsmith);

        assertActionSize(2, mm);

        endNight();

        isDead(baker, mm, bg);
        isAlive(cit, gunsmith);

        partialContains(cit, Bodyguard.TARGET_FEEDBACK);
        partialContains(gunsmith, Bodyguard.TARGET_FEEDBACK);
    }

    public void testBGUsingBread() {
        Controller bg = addPlayer(BasicRoles.Bodyguard());
        Controller baker = addPlayer(BasicRoles.Baker());
        Controller cit = addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Witch());

        setTarget(baker, bg);
        assertPassableBreadCount(0, bg);

        nextNight();

        assertUseableBreadCount(1, bg);
        setTarget(bg, baker);
        setTarget(bg, cit);
        assertActionSize(2, bg);
        cancelAction(bg, 0);
        assertActionSize(1, bg);
        setTarget(bg, baker);

        endNight();

        assertUseableBreadCount(0, bg);
        assertPassableBreadCount(0, bg);
    }

    public void testBGProtectingOneShot() {
        Controller bg = addPlayer(BasicRoles.Bodyguard());
        Controller baker = addPlayer(BasicRoles.Baker());
        Controller vigi = addPlayer(BasicRoles.Vigilante());
        Controller witch = addPlayer(BasicRoles.Witch());
        addPlayer(BasicRoles.Framer());

        setTarget(baker, vigi);

        nextNight();

        shoot(vigi, witch);
        shoot(vigi, baker);
        setTarget(bg, witch);

        endNight();

        isDead(baker, bg, vigi);
        isAlive(witch);
    }

    public void testImmuneFeedback() {
        Controller bg = addPlayer(BasicRoles.Bodyguard());
        Controller doc = addPlayer(BasicRoles.Doctor());
        Controller gf = addPlayer(BasicRoles.Godfather());
        Controller sk = addPlayer(BasicRoles.SerialKiller());
        Controller sk2 = addPlayer(BasicRoles.SerialKiller());
        Controller as = addPlayer(BasicRoles.Armorsmith());
        Controller cit = addPlayer(BasicRoles.Citizen());

        setTarget(as, cit);
        nextNight();

        setTarget(doc, bg);
        setTarget(bg, gf);
        setTarget(sk, gf);
        endNight();

        partialContains(gf, Bodyguard.TARGET_FEEDBACK);
        partialContains(bg, Doctor.SUCCESFULL_HEAL);

        skipDay();
        setTarget(sk2, cit);
        setTarget(bg, cit);
        vest(cit);

        endNight();
        partialExcludes(cit, Bodyguard.TARGET_FEEDBACK);
    }

    public void testJailedFeedback() {
        Controller jailor = addPlayer(BasicRoles.Jailor());
        Controller fodder = addPlayer(BasicRoles.Framer());
        Controller bg = addPlayer(BasicRoles.Bodyguard());
        Controller sk = addPlayer(BasicRoles.SerialKiller());
        addPlayer(BasicRoles.Witch());

        jail(jailor, fodder);
        skipDay();

        setTarget(bg, fodder);
        setTarget(sk, fodder);
        endNight();

        partialExcludes(fodder, Bodyguard.TARGET_FEEDBACK);
        partialExcludes(fodder, Doctor.TARGET_FEEDBACK);
    }

    public void testKilledDousedTargetProtecting() {
        Controller bg = addPlayer(BasicRoles.Bodyguard());
        Controller sk = addPlayer(BasicRoles.SerialKiller());
        Controller ars = addPlayer(BasicRoles.Arsonist());
        Controller cit = addPlayer(BasicRoles.Citizen());

        editRule(SetupModifierName.GUARD_REDIRECTS_DOUSE, true);

        setTarget(bg, cit);
        setTarget(sk, bg);
        setTarget(ars, cit, Douse.abilityType);
        endNight();

        isDead(bg);
        assertStatus(bg, Douse.abilityType);
        assertStatus(cit, Douse.abilityType, false);
    }

    public void testPoisonConversion() {
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller bg = addPlayer(BasicRoles.Bodyguard());
        Controller cultLeader = addPlayer(BasicRoles.CultLeader());
        Controller pois = addPlayer(BasicRoles.Poisoner());
        Controller pois2 = addPlayer(BasicRoles.Poisoner());

        editRule(SetupModifierName.GUARD_REDIRECTS_POISON, true);
        editRule(SetupModifierName.GUARD_REDIRECTS_CONVERT, true);

        setTarget(bg, cit);
        setTarget(cultLeader, cit);
        setTarget(pois, cit);
        setTarget(pois2, cit);
        endNight();

        assertStatus(bg, Poisoner.abilityType);
        assertStatus(cit, Poisoner.abilityType, false);

        assertStatus(bg, CultLeader.abilityType);
        assertStatus(cit, CultLeader.abilityType, false);
    }

    public void testPoisonConversionButDead() {
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller bg = addPlayer(BasicRoles.Bodyguard());
        Controller cl = addPlayer(BasicRoles.CultLeader());
        Controller pois = addPlayer(BasicRoles.Poisoner());
        Controller pois2 = addPlayer(BasicRoles.Poisoner());
        Controller sk = addPlayer(BasicRoles.SerialKiller());

        editRule(SetupModifierName.GUARD_REDIRECTS_POISON, true);
        editRule(SetupModifierName.GUARD_REDIRECTS_CONVERT, true);

        setTarget(sk, bg);
        setTarget(bg, cit);
        setTarget(cl, cit);
        setTarget(pois, cit);
        setTarget(pois2, cit);
        endNight();

        assertStatus(bg, Poisoner.abilityType, false);
        assertStatus(cit, Poisoner.abilityType);

        assertStatus(bg, CultLeader.abilityType, false);
        assertStatus(cit, CultLeader.abilityType);

        assertEquals("Cultist", cit.getRoleName());
    }

    // if they're dead at the time, they won't take the cult/poison effect
}
