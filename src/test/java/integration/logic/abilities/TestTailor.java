package integration.logic.abilities;

import java.util.ArrayList;
import java.util.List;

import game.abilities.DrugDealer;
import game.abilities.GraveDigger;
import game.abilities.Tailor;
import game.ai.Controller;
import game.ai.ControllerList;
import game.event.DeathAnnouncement;
import game.event.Message;
import game.logic.PlayerList;
import game.logic.exceptions.PlayerTargetingException;
import game.logic.exceptions.UnknownRoleException;
import game.logic.support.Constants;
import game.logic.support.action.Action;
import game.logic.templates.BasicRoles;
import game.logic.templates.HTMLDecoder;
import game.setups.Setup;
import integration.logic.SuperTest;
import models.Faction;
import models.enums.AbilityModifierName;
import models.enums.SetupModifierName;
import util.Util;
import util.models.FactionRoleTestUtil;

public class TestTailor extends SuperTest {

    public TestTailor(String name) {
        super(name);
    }

    /*
     * not testing these features because of the nature of the implementation
     *
     * invest sheriff coroner framing
     */

    Controller tailor;

    @Override
    public void roleInit() {
        tailor = addPlayer(BasicRoles.Tailor());
    }

    private void suit(String color, String roleName, Controller target) {
        setTarget(tailor, Tailor.abilityType, color, roleName, target);
    }

    public void testBasic() {
        Controller cit = addPlayer(BasicRoles.Sheriff());
        Controller sk = addPlayer(BasicRoles.SerialKiller());
        Controller witch = addPlayer(BasicRoles.Witch());

        editRule(SetupModifierName.TAILOR_FEEDBACK, true);

        setTarget(sk, cit);
        tailor(tailor, cit, witch);

        endNight();
        assertSuited(cit, witch);
        partialContains(cit, Tailor.FEEDBACK);
    }

    public void testBadOptions() {
        Controller cit = addPlayer(BasicRoles.Sheriff());
        addPlayer(BasicRoles.SerialKiller());

        nightStart();

        try{
            suit(Setup.BENIGN_C, "Roofus", cit);
            fail();
        }catch(UnknownRoleException | PlayerTargetingException e){
        }
        try{
            suit(Setup.MAFIA_C, "Roofus", cit);
            fail();
        }catch(UnknownRoleException | PlayerTargetingException e){
        }
    }

    public void testCharges() {
        setCharges(1);

        Controller tailor = addPlayer(BasicRoles.Tailor());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller coroner = addPlayer(BasicRoles.Coroner());

        nightStart();

        tailor(tailor, cit, coroner);

        nextNight();

        try{
            tailor(tailor, cit, coroner);
            fail();
        }catch(PlayerTargetingException e){
        }
    }

    public void testDDCharges() {
        setCharges(1);

        Controller tailor = addPlayer(BasicRoles.Tailor());
        Controller dd = addPlayer(BasicRoles.DrugDealer());
        Controller escort = addPlayer(BasicRoles.Escort());

        drug(dd, tailor, DrugDealer.BLOCKED);
        tailor(tailor, dd, escort);

        endNight();

        assertPerceivedChargeRemaining(1, tailor);
    }

    private void setCharges(int i) {
        modifyRole(BasicRoles.Tailor(), AbilityModifierName.CHARGES, i);
        editRule(SetupModifierName.CHARGE_VARIABILITY, 0);
    }

    public void testElectroCharges() {
        setCharges(1);

        Controller tailor = addPlayer(BasicRoles.Tailor());
        Controller electro = addPlayer(BasicRoles.ElectroManiac());
        Controller doc = addPlayer(BasicRoles.Doctor());
        addPlayer(BasicRoles.Doctor());

        electrify(electro, tailor);

        setTarget(doc, tailor);
        tailor(tailor, electro, doc);

        endNight();

        isAlive(tailor);
        assertPerceivedChargeRemaining(0, tailor);

        voteOut(electro, tailor, doc, this.tailor);

        assertSuited(electro, doc);
    }

    public void testSnitch() {
        Controller tailor = addPlayer(BasicRoles.Tailor());
        Controller snitch = addPlayer(BasicRoles.Snitch());
        Controller doc = addPlayer(BasicRoles.Doctor());
        addPlayer(BasicRoles.Snitch());

        modifyAbilitySelfTarget(BasicRoles.Tailor(), true);
        editRule(SetupModifierName.SNITCH_PIERCES_SUIT, false);

        nightStart();

        assertTrue(tailor.getPlayer().getAbility(Tailor.class).modifiers.getBoolean(AbilityModifierName.SELF_TARGET,
                game));

        setTarget(doc, tailor);
        tailor(tailor, tailor, doc);
        setTarget(snitch, tailor);

        nextNight();
        hasSuits(tailor);
        mafKill(tailor, snitch);

        ArrayList<Message> announcements = addAnnouncementListener();

        endNight();

        TestSnitch.SnitchAnnouncement(announcements, doc.getRoleName(), tailor.getRoleName());
    }

    public void testSnitchPiercing() {
        Controller tailor = addPlayer(BasicRoles.Tailor());
        Controller snitch = addPlayer(BasicRoles.Snitch());
        Controller doc = addPlayer(BasicRoles.Doctor());
        addPlayer(BasicRoles.Snitch());

        modifyAbilitySelfTarget(BasicRoles.Tailor(), true);
        editRule(SetupModifierName.SNITCH_PIERCES_SUIT, true);

        setTarget(doc, tailor);
        tailor(tailor, tailor, doc);
        setTarget(snitch, tailor);

        nextNight();
        hasSuits(tailor);
        mafKill(tailor, snitch);

        ArrayList<Message> announcements = addAnnouncementListener();

        endNight();

        TestSnitch.SnitchAnnouncement(announcements, tailor.getRoleName(), doc.getRoleName());
    }

    public void testDayLynchSuitDisappear() {
        Controller sk = addPlayer(BasicRoles.SerialKiller());
        Controller doc = addPlayer(BasicRoles.Doctor());
        Controller cit = addPlayer(BasicRoles.Doctor());
        Controller fodder = addPlayer(BasicRoles.Agent());

        nightStart();

        tailor(tailor, fodder, sk);

        endNight();

        voteOut(tailor, doc, sk, cit);

        setTarget(sk, fodder);

        endNight();

        assertSuited(fodder, fodder);
    }

    public void testNightKillSuitPersist() {
        Controller tailor = addPlayer(BasicRoles.Tailor());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller electro = addPlayer(BasicRoles.ElectroManiac());
        addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Agent());

        startNight();

        electrify(electro, tailor, cit);

        tailor(tailor, cit, tailor);

        endNight();

        assertSuited(cit, tailor);
    }

    public void testNightKillSuitDisappear() {
        Controller tailor = addPlayer(BasicRoles.Tailor());
        Controller cit = addPlayer(BasicRoles.BusDriver());
        Controller agent = addPlayer(BasicRoles.Agent());
        Controller sk = addPlayer(BasicRoles.SerialKiller());

        setTarget(sk, cit);
        tailor(tailor, cit, tailor);
        mafKill(agent, tailor);

        endNight();

        assertSuited(cit, cit);
    }

    public void testWitch() {
        Controller witch = addPlayer(BasicRoles.Witch());
        Controller detect = addPlayer(BasicRoles.Detective());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller sk = addPlayer(BasicRoles.SerialKiller());

        tailor(tailor, sk, tailor);
        setTarget(detect, tailor);
        witch(witch, tailor, cit);
        setTarget(sk, cit);

        endNight();

        assertSuited(cit, tailor);
        TestDetective.seen(detect, cit);
    }

    public void testJanOverride() {
        Controller janitor = addPlayer(BasicRoles.Janitor());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller tailor = addPlayer(BasicRoles.Tailor());
        addPlayer(BasicRoles.Citizen());

        nightStart();

        tailor(tailor, cit, janitor);

        nextNight();
        mafKill(tailor, cit);
        setTarget(janitor, cit);

        endNight();
        DeathAnnouncement da;
        String access, access2;
        for(Message m: game.getEventManager().getEvents(Message.PUBLIC)){
            if(m instanceof DeathAnnouncement){
                da = (DeathAnnouncement) m;
                if(da.dead.contains(cit.getPlayer())){
                    access = m.access(Message.PUBLIC, new HTMLDecoder());
                    assertTrue(access.contains("???") || access.contains("could not"));
                    if(!access.contains("could not"))
                        assertTrue(access.contains(Constants.INITIAL));

                    access2 = m.access(Message.PRIVATE, new HTMLDecoder());

                    if(!access.contains("could not"))
                        assertTrue(access2.contains(cit.getColor()));
                    assertTrue(access2.contains(cit.getRoleName()));
                    return;
                }
            }
        }
        fail();
    }

    public void testTailorCorpse() {
        setCharges(1);

        Controller digger = addPlayer(BasicRoles.GraveDigger());
        Controller tailor = addPlayer(BasicRoles.Tailor());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller sk = addPlayer(BasicRoles.SerialKiller());
        addPlayer(BasicRoles.Veteran());

        setTarget(sk, tailor);
        nextNight();

        tailorDig(digger, tailor, cit, sk);

        nextNight();
        assertRealChargeRemaining(0, tailor, Tailor.abilityType);
        hasSuits(cit);

        tailorDig(digger, tailor, cit, cit);
        setTarget(sk, cit);

        endNight();

        assertSuited(cit, sk);
    }

    private static void tailorDig(Controller digger, Controller dead, Controller target,
            Controller factionRoleController) {
        List<String> args = Util.toStringList(Tailor.COMMAND, factionRoleController.getColor(),
                factionRoleController.getRoleName());
        PlayerList targets = ControllerList.ToPlayerList(game, dead, target);
        Action action = new Action(digger.getPlayer(), GraveDigger.abilityType, args, targets);
        setTarget(action);
    }

    public void testTailorCommand() {
        Controller tailor = addPlayer(BasicRoles.Tailor());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller sk = addPlayer(BasicRoles.SerialKiller());
        addPlayer(BasicRoles.Armorsmith());

        nightStart();

        command(tailor, Tailor.COMMAND, cit.getName(), sk.getPlayer().getGameFaction().getName(), sk.getRoleName());
        setTarget(sk, cit);

        endNight();

        assertSuited(cit, sk);
    }

    public void testNoCultSuit() {
        Controller cl = addPlayer(BasicRoles.Cultist());
        Controller tailor = addPlayer(BasicRoles.Tailor());
        Controller citizen = addPlayer(BasicRoles.Citizen());

        nightStart();
        try{
            setTarget(tailor, Tailor.abilityType, cl.getColor(), tailor.getRoleName(), citizen);
            fail();
        }catch(UnknownRoleException | PlayerTargetingException e){
        }

    }

    public void testCultedSuit() {
        Controller cl = addPlayer(BasicRoles.CultLeader());
        Controller tailor = addPlayer(BasicRoles.Tailor());
        Controller citizen = addPlayer(BasicRoles.Citizen());
        FactionRoleTestUtil.addCultRole(BasicRoles.CultLeader(), BasicRoles.Tailor());

        nightStart();

        setTarget(tailor, Tailor.abilityType, cl.getColor(), tailor.getRoleName(), citizen);

        endNight();

        assertEquals(cl.getColor(), citizen.getPlayer().suits.get(0).factionRole.getColor());
    }

    public void testTailorCorpseCH() {
        Controller gd = addPlayer(BasicRoles.GraveDigger());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller sk = addPlayer(BasicRoles.SerialKiller());
        addPlayer(BasicRoles.Citizen());

        voteOut(tailor, gd, cit, sk);

        command(gd, GraveDigger.COMMAND, tailor.getName(), cit.getName(), sk.getName(), Tailor.COMMAND,
                sk.getPlayer().getGameFaction().getName(), sk.getRoleName());
        setTarget(sk, cit);
        endNight();

        assertSuited(cit, sk);
    }

    public void testPromotionTailorRoles() {
        Controller cultLeader = addPlayer(BasicRoles.CultLeader());
        addPlayer(BasicRoles.BusDriver());
        addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Goon());
        FactionRoleTestUtil.addCultRole(BasicRoles.CultLeader(), BasicRoles.Sheriff());

        editRule(SetupModifierName.CULT_KEEPS_ROLES, false);
        editRule(SetupModifierName.CULT_PROMOTION, true);

        nightStart();

        String sheriffRoleName = BasicRoles.Sheriff().getName();
        tailor(cultLeader, sheriffRoleName, BasicRoles.CultLeader().faction);
        endNight();

        assertEquals(sheriffRoleName, cultLeader.getPlayer().suits.get(0).factionRole.getName());
    }

    private void tailor(Controller target, String roleName, Faction faction) {
        setTarget(tailor, Tailor.abilityType, faction.getColor(), roleName, target);
    }
}
