package integration.logic.abilities;

import game.ai.Controller;
import game.logic.GameFaction;
import game.logic.exceptions.PlayerTargetingException;
import game.logic.templates.BasicRoles;
import integration.logic.SuperTest;
import models.enums.SetupModifierName;

public class TestMassMurderer extends SuperTest{

	public TestMassMurderer(String s) {
		super(s);
	}

	public void testNoKill(){
		

		Controller cit = addPlayer(BasicRoles.Citizen());
		Controller cit2 = addPlayer(BasicRoles.Citizen());
		Controller detect = addPlayer(BasicRoles.Detective());
		Controller witch = addPlayer(BasicRoles.Witch());
		Controller mm = addPlayer(BasicRoles.MassMurderer());
		
		nightStart();
		
		setTarget(mm, detect);
		setTarget(detect, mm);
		
		endNight();
		
		TestDetective.seen(detect, detect);
		skipDay();
		
		setTarget(mm, cit);
		endNight();
		
		isDead(cit);
		
		skipDay();
		
		setTarget(mm, witch);
		witch(witch , cit2, witch);
		setTarget(detect, witch);
		
		endNight();
		
		isAlive(witch);
		isWinner(witch, mm);
	}
	
	public void testLose(){
		Controller cit = addPlayer(BasicRoles.Citizen());
		Controller vig = addPlayer(BasicRoles.Vigilante());
		Controller mm  = addPlayer(BasicRoles.MassMurderer());
		Controller mm2 = addPlayer(BasicRoles.MassMurderer());
		
		dayStart();
		
		voteOut(mm2, mm, vig, cit);
		
		setTarget(vig, cit, GUN);
		
		assertInProgress();
		
		endNight();

		GameFaction town = game.getFaction(cit.getColor());
		assertTrue(game.getFaction(mm.getColor()).winsOver(town));
		assertGameOver();
		isLoser(vig, cit, mm2);
		isWinner(mm);
		
	}
	
	public void testImmuneMM(){
		Controller cit = addPlayer(BasicRoles.Citizen());
		Controller vig = addPlayer(BasicRoles.Vigilante());
		Controller mm  = addPlayer(BasicRoles.MassMurderer());
		

		nightStart();
		
		setTarget(mm, mm);
		setTarget(vig, mm, GUN);
		
		endNight();
		
		isLoser(cit);
	}
	
	public void testBDTargeting(){
		
		Controller bd1 = addPlayer(BasicRoles.BusDriver());
		Controller bd2 = addPlayer(BasicRoles.BusDriver());
		Controller doc1 = addPlayer(BasicRoles.Doctor());
		Controller doc2 = addPlayer(BasicRoles.Doctor());
		Controller doc3 = addPlayer(BasicRoles.Doctor());
		Controller mm = addPlayer(BasicRoles.MassMurderer());
		Controller mm2 = addPlayer(BasicRoles.MassMurderer());
		

		nightStart();
		
		setTarget(doc1, bd1);
		setTarget(doc2, bd2);
		setTarget(doc3, bd2);
		setTarget(mm, mm2);
		setTarget(mm2, mm);
		drive(bd1, mm, mm2);
		drive(bd2, mm2, mm);
		
		endNight();
		
		isAlive(bd2);
		isDead(bd1);
	}
	
	public void testCooldownAndBlocking(){
		Controller cit     = addPlayer(BasicRoles.Citizen());
		Controller cit1    = addPlayer(BasicRoles.Citizen());
		Controller lookout = addPlayer(BasicRoles.Lookout());
		Controller sheriff = addPlayer(BasicRoles.Sheriff());
		Controller consort = addPlayer(BasicRoles.Consort());
		Controller witch   = addPlayer(BasicRoles.Witch());
		Controller mm      = addPlayer(BasicRoles.MassMurderer());

		editRule(SetupModifierName.MM_SPREE_DELAY, 1);
		
		/*
		 * block test
		 */
		setTarget(consort, mm);
		setTarget(mm, mm);
		endNight();
		
		isAlive(consort);
		voteOut(consort, mm, witch, lookout, cit);
		
		setTarget(sheriff, cit1);
		setTarget(mm, cit1);
		endNight();
		
		isDead(cit1, sheriff);
		
		skipDay();
		
		
		try{
			setTarget(mm, cit);
			fail();
		} catch(PlayerTargetingException e){}
		
		setTarget(lookout, cit);
		witch(witch, mm, cit);
		endNight();
		
		TestLookout.seen(lookout, mm);
		skipDay();
		
		/*
		 * witch dies to who she's visiting target
		 */
		witch(witch, lookout, mm);
		setTarget(mm, lookout);
		
		//witch dies
		
		endNight();
		
		voteOut(mm, lookout, cit);
		
		isLoser(mm);
		isWinner(lookout);
	}
	
	
}
