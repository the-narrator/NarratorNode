package integration.logic.abilities;

import game.abilities.GameAbility;
import game.abilities.Scout;
import game.ai.Controller;
import game.logic.templates.BasicRoles;
import game.setups.Setup;
import integration.logic.SuperTest;
import models.Faction;
import models.Role;
import services.FactionRoleService;
import services.RoleService;

public class TestScout extends SuperTest {

    public TestScout(String s) {
        super(s);
    }

    Controller scout;

    @Override
    public void roleInit() {
        Faction faction = setup.getFactionByColor(Setup.CULT_C);
        Role role = RoleService.createRole(setup, "Scout", Scout.abilityType);
        scout = this.addPlayer(FactionRoleService.createFactionRole(faction, role));
    }

    public void testBasicAbility() {
        Controller sheriff = addPlayer(BasicRoles.Sheriff());
        Controller citizen = addPlayer(BasicRoles.Citizen());

        notRecruitable(sheriff);
        isRecruitable(citizen);
    }

    private void notRecruitable(Controller p) {
        if(game.isDay())
            skipDay();
        setTarget(scout, p);
        endNight();

        partialContains(scout, Scout.NOT_RECRUITABLE);
    }

    private void isRecruitable(Controller p) {
        if(game.isDay())
            skipDay();
        setTarget(scout, p);
        endNight();

        partialContains(scout, Scout.RECRUITABLE);
    }

    public static void seen(Controller p, Class<? extends GameAbility> c) {
        partialContains(p, c.getSimpleName() + ".");
    }

    public static void notSeen(Controller p, Class<? extends GameAbility> c) {
        partialExcludes(p, c.getSimpleName() + ".");
    }
}
