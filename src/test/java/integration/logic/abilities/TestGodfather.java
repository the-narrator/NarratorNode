package integration.logic.abilities;

import game.abilities.Godfather;
import game.logic.exceptions.NarratorException;
import game.logic.templates.BasicRoles;
import game.setups.Setup;
import integration.logic.SuperTest;
import models.enums.AbilityModifierName;
import services.FactionRoleModifierService;

public class TestGodfather extends SuperTest {

    public TestGodfather(String name) {
        super(name);
    }

    public void testDifferentTeams() {

        addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Godfather());
        addPlayer(BasicRoles.Godfather(Setup.YAKUZA_C));

        nightStart();
    }

    public void testNoBackToBackModifier() {
        try{
            FactionRoleModifierService.addModifier(BasicRoles.Godfather(), AbilityModifierName.BACK_TO_BACK,
                    Godfather.abilityType, false);
            fail();
        }catch(NarratorException e){
        }
    }

    public void testNoZeroWeightModifier() {
        try{
            FactionRoleModifierService.addModifier(BasicRoles.Godfather(), AbilityModifierName.ZERO_WEIGHTED,
                    Godfather.abilityType, false);
            fail();
        }catch(NarratorException e){
        }
    }
}
