package integration.logic.abilities;

import game.abilities.Doctor;
import game.abilities.GameAbility;
import game.abilities.Janitor;
import game.ai.Controller;
import game.logic.support.Constants;
import game.logic.templates.BasicRoles;
import integration.logic.SuperTest;
import models.enums.AbilityModifierName;
import models.enums.SetupModifierName;
import util.TestUtil;

public class TestJanitor extends SuperTest {

    public TestJanitor(String name) {
        super(name);
    }

    public void testJanitor() {
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller cit2 = addPlayer(BasicRoles.Citizen());
        Controller jan = addPlayer(BasicRoles.Janitor());
        Controller jan2 = addPlayer(BasicRoles.Janitor());
        Controller maf = addPlayer(BasicRoles.Goon());

        mafKill(maf, cit2);
        setTarget(jan2, cit2);
        setTarget(jan, cit);
        endNight();

        assertStatus(cit, Janitor.abilityType);
        assertStatus(cit2, Janitor.abilityType);
        assertEquals(Constants.HIDDEN_KILL_FLAG, cit2.getPlayer().getDeathType().getList().get(0));

        skipDay();
        endNight();

        assertStatus(cit, Janitor.abilityType, false);
        assertStatus(cit2, Janitor.abilityType);
    }

    public void testJanGetRole() {
        addPlayer(BasicRoles.Citizen());
        Controller doc = addPlayer(BasicRoles.Doctor());
        Controller sk = addPlayer(BasicRoles.SerialKiller());
        Controller jan = addPlayer(BasicRoles.Janitor());

        editRule(SetupModifierName.JANITOR_GETS_ROLES, true);

        setTarget(jan, doc);
        setTarget(sk, doc);
        endNight();

        seen(jan, Doctor.class);
    }

    public void testJanNotSeen() {
        Controller citizen = addPlayer(BasicRoles.Citizen());
        Controller doctor = addPlayer(BasicRoles.Doctor());
        Controller janitor = addPlayer(BasicRoles.Janitor());
        Controller janitor2 = addPlayer(BasicRoles.Janitor());
        Controller serialKiller = addPlayer(BasicRoles.SerialKiller());

        editRule(SetupModifierName.JANITOR_GETS_ROLES, true);

        setTarget(janitor, doctor);
        setTarget(janitor2, doctor);
        endNight();

        notSeen(janitor, Doctor.class);

        voteOut(doctor, janitor, citizen, serialKiller);

        seen(janitor, Doctor.class);
        seen(janitor2, Doctor.class);
    }

    public void testOnSuccessCDay() {
        TestUtil.modifyAbility(BasicRoles.Janitor().role, Janitor.abilityType, AbilityModifierName.COOLDOWN, 1);

        Controller janitor = addPlayer(BasicRoles.Janitor());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller cit2 = addPlayer(BasicRoles.Citizen());

        editRule(SetupModifierName.CHARGE_VARIABILITY, 0);

        setTarget(janitor, cit);
        endNight();

        assertCooldownRemaining(0, janitor);

        skipDay();

        assertCooldownRemaining(0, janitor);

        setTarget(janitor, cit);
        endNight();

        assertCooldownRemaining(0, janitor);

        voteOut(cit, janitor, cit2);

        assertCooldownRemaining(1, janitor);
    }

    private static void seen(Controller jan, Class<? extends GameAbility> c) {
        TestInvestigator.seen(jan, c);
    }

    private static void notSeen(Controller jan, Class<? extends GameAbility> c) {
        TestInvestigator.notSeen(jan, c);
    }
}
