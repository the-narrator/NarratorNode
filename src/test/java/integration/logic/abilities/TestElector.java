package integration.logic.abilities;

import game.abilities.Bulletproof;
import game.abilities.DrugDealer;
import game.abilities.Elector;
import game.ai.Controller;
import game.logic.exceptions.NarratorException;
import game.logic.templates.BasicRoles;
import integration.logic.SuperTest;
import models.enums.GameModifierName;
import models.enums.SetupModifierName;
import services.GameService;
import util.TestUtil;

public class TestElector extends SuperTest {
    public TestElector(String s) {
        super(s);
    }

    Controller electorA;

    @Override
    public void roleInit() {
        electorA = addPlayer(BasicRoles.Elector());
    }

    public void testBadTarget() {
        Controller deadCitizen = addPlayer(BasicRoles.Citizen());
        Controller assassin = addPlayer(BasicRoles.Assassin());
        addPlayer(BasicRoles.Citizen());

        doDayAction(assassin, deadCitizen);
        skipDay();

        try{
            setTarget(electorA, deadCitizen, assassin);
            fail();
        }catch(NarratorException e){
        }

        try{
            setTarget(electorA, assassin, deadCitizen);
            fail();
        }catch(NarratorException e){
        }

        try{
            setTarget(electorA, assassin);
            fail();
        }catch(NarratorException e){
        }

        try{
            setTarget(electorA, assassin, assassin);
            fail();
        }catch(NarratorException e){
        }

        try{
            setTarget(electorA, assassin);
            fail();
        }catch(NarratorException e){
        }
    }

    public void testBasic() {
        Controller cit1 = addPlayer(BasicRoles.Citizen());
        Controller cit2 = addPlayer(BasicRoles.Citizen());

        setTarget(electorA, cit1, cit2);
        endNight();

        partialContains(cit1, Elector.LOST_FEEDBACK);
        partialContains(cit2, Elector.GAINED_FEEDBACK);

        vote(electorA, cit2);
        vote(cit1, cit2);
        vote(cit2, electorA);

        assertGameOver();
    }

    public void testRegainVotePowerDay() {
        Controller citizen1 = addPlayer(BasicRoles.Citizen());
        Controller assassin = addPlayer(BasicRoles.Assassin());
        Controller citizen2 = addPlayer(BasicRoles.Citizen());
        Controller citizen3 = addPlayer(BasicRoles.Citizen());

        setTarget(electorA, citizen1, citizen2);
        endNight();

        voteOut(citizen3, electorA, citizen1, assassin);

        assertIsDay();

        doDayAction(assassin, citizen2);

        assertIsNight();
    }

    public void testRegainVotePowerNight() {
        Controller citizen1 = addPlayer(BasicRoles.Citizen());
        Controller serialKiller = addPlayer(BasicRoles.SerialKiller());
        Controller citizen2 = addPlayer(BasicRoles.Citizen());

        setTarget(electorA, citizen1, citizen2);
        endNight();

        assertVotePower(2, citizen2);
        assertVotePower(0, citizen1);

        skipDay();

        setTarget(serialKiller, citizen2);
        endNight();

        assertVotePower(1, citizen1);
    }

    public void testDoubleSteal() {
        Controller electorB = addPlayer(BasicRoles.Elector());
        Controller citizenC = addPlayer(BasicRoles.Citizen());

        setTarget(electorA, citizenC, electorA);
        setTarget(electorB, citizenC, electorB);
        endNight(electorA);
        endNight();

        assertVotePower(2, electorA);
        assertVotePower(0, citizenC);
    }

    public void testResolveDebts() {
        Controller electorB = addPlayer(BasicRoles.Elector());
        Controller citizenC = addPlayer(BasicRoles.Citizen());
        Controller electorD = addPlayer(BasicRoles.Elector());
        Controller mayorE = addPlayer(BasicRoles.Mayor());

        doDayAction(mayorE);
        skipDay();

        setTarget(electorA, citizenC, electorA);
        endNight(electorA);
        setTarget(electorB, citizenC, electorB);
        endNight(electorB);
        setTarget(electorD, mayorE, citizenC);

        endNight();

        assertVotePower(2, electorA);
        assertVotePower(0, citizenC);
        assertVotePower(2, electorB);
    }

    public void testResolveDebtsNoCarryOver() {
        Controller electorB = addPlayer(BasicRoles.Elector());
        Controller citizenC = addPlayer(BasicRoles.Citizen());

        setTarget(electorA, citizenC, electorA);
        endNight(electorA);
        setTarget(electorB, citizenC, electorB);
        endNight(electorB);

        nextNight();

        setTarget(electorA, electorA, citizenC);
        endNight();

        assertVotePower(1, citizenC);
    }

    public void testNoDoubleFeedback() {
        Controller electorB = addPlayer(BasicRoles.Elector());
        Controller citizenC = addPlayer(BasicRoles.Citizen());
        Controller citizenD = addPlayer(BasicRoles.Citizen());
        Controller assassinE = addPlayer(BasicRoles.Assassin());

        setTarget(electorA, citizenC, citizenD);
        setTarget(electorB, citizenD, electorB);
        endNight();

        assertVotePower(1, citizenD);
        assertVotePower(2, electorB);
        assertVotePower(0, citizenC);
        partialContains(electorB, Elector.GAINED_FEEDBACK);
        partialContains(citizenC, Elector.LOST_FEEDBACK);
        partialExcludes(citizenD, Elector.GAINED_FEEDBACK);
        partialExcludes(citizenD, Elector.LOST_FEEDBACK);

        doDayAction(assassinE, electorB);
        assertVotePower(1, citizenD);
        assertVotePower(1, citizenC);
    }

    public void testNoDrugDealerWipeForFeedback() {
        Controller drugDealer = addPlayer(BasicRoles.DrugDealer());
        Controller drugDealer2 = addPlayer(BasicRoles.DrugDealer());
        Controller citizen1 = addPlayer(BasicRoles.Citizen());
        Controller citizen2 = addPlayer(BasicRoles.Citizen());

        setTarget(electorA, citizen1, citizen2);
        drug(drugDealer, citizen1, DrugDealer.WIPE);
        drug(drugDealer2, citizen2, DrugDealer.WIPE);
        endNight();

        partialContains(citizen1, Elector.LOST_FEEDBACK);
        partialContains(citizen2, Elector.GAINED_FEEDBACK);
    }

    public void testDisguiserDelayedIntoZeroVoter() {
        Controller disguiser = addPlayer(BasicRoles.Disguiser());
        Controller citizen = addPlayer(BasicRoles.Citizen());
        Controller citizen2 = addPlayer(BasicRoles.Citizen());

        setTarget(electorA, citizen, citizen2);
        endNight();

        skipDay();

        setTarget(disguiser, citizen);
        endNight();

        assertVotePower(0, disguiser);
    }

    public void testDisguiserDelayedIntoTwoVoter() {
        Controller disguiser = addPlayer(BasicRoles.Disguiser());
        Controller citizen = addPlayer(BasicRoles.Citizen());
        Controller citizen2 = addPlayer(BasicRoles.Citizen());

        setTarget(electorA, citizen, citizen2);
        endNight();

        skipDay();

        setTarget(disguiser, citizen2);
        endNight();

        assertVotePower(2, disguiser);
    }

    public void testDisguiserGettingBackTargetsStolenVotes() {
        Controller disguiser = addPlayer(BasicRoles.Disguiser());
        Controller citizen = addPlayer(BasicRoles.Citizen());
        Controller citizen2 = addPlayer(BasicRoles.Citizen());
        Controller assassin = addPlayer(BasicRoles.Assassin());
        addPlayer(BasicRoles.Citizen());

        setTarget(electorA, citizen, citizen2);
        endNight();

        skipDay();

        setTarget(disguiser, citizen);
        endNight();

        assertVotePower(0, disguiser);
        assertTrue(citizen2.getPlayer().positiveVotePower.contains(disguiser));

        doDayAction(assassin, citizen2);

        assertVotePower(1, disguiser);
    }

    public void testMayorDisguiser() {
        Controller disguiser = addPlayer(BasicRoles.Disguiser());
        Controller mayor = addPlayer(BasicRoles.Mayor());
        addPlayer(BasicRoles.Citizen());
        editRule(SetupModifierName.MAYOR_VOTE_POWER, 3);

        doDayAction(mayor);
        skipDay();

        setTarget(electorA, mayor, disguiser);
        endNight();

        assertVotePower(3, mayor);
        assertVotePower(2, disguiser);

        skipDay();

        setTarget(electorA, mayor, disguiser);
        endNight();

        assertVotePower(2, mayor);
        assertVotePower(3, disguiser);

        skipDay();

        setTarget(disguiser, mayor);
        endNight();

        assertVotePower(4, disguiser);
    }

    public void testMayorVoteReturning() {
        Controller mayor = addPlayer(BasicRoles.Mayor());
        Controller assassin = addPlayer(BasicRoles.Assassin());
        Controller electroManiac = addPlayer(BasicRoles.ElectroManiac());
        Controller elector2 = addPlayer(BasicRoles.Elector());
        TestUtil.removeAbility(BasicRoles.ElectroManiac().role, Bulletproof.abilityType);
        editRule(SetupModifierName.MAYOR_VOTE_POWER, 3);

        doDayAction(mayor);
        skipDay();

        setTarget(electroManiac, assassin);
        setTarget(electorA, mayor, assassin);
        setTarget(elector2, mayor, electroManiac);
        endNight();

        assertVotePower(2, mayor);

        doDayAction(assassin, electroManiac);

        assertVotePower(4, mayor);
    }

    public void testParity() {
        GameService.upsertModifier(game, GameModifierName.AUTO_PARITY, true);
        Controller goon = addPlayer(BasicRoles.Goon());
        Controller assassin = addPlayer(BasicRoles.Assassin());
        addPlayer(BasicRoles.Citizen(), 3);

        setTarget(electorA, electorA, goon);
        endNight();

        doDayAction(assassin, electorA);
        assertIsNight();
    }

    // you can only give 'your' votes

}
