package integration.logic.abilities;

import game.abilities.ArmsDetector;
import game.ai.Controller;
import game.logic.templates.BasicRoles;
import game.setups.Setup;
import integration.logic.SuperTest;
import models.Faction;
import models.Role;
import models.enums.SetupModifierName;
import services.FactionRoleService;
import services.RoleService;

public class TestArmsDetector extends SuperTest {

    public TestArmsDetector(String s) {
        super(s);
    }

    Controller tsa;

    @Override
    public void roleInit() {
        Role role = RoleService.createRole(setup, "Arms Detector", ArmsDetector.abilityType);
        Faction faction = setup.getFactionByColor(Setup.TOWN_C);
        tsa = addPlayer(FactionRoleService.createFactionRole(faction, role));
    }

    public void testSheriff() {
        Controller sheriff = addPlayer(BasicRoles.Sheriff());
        addPlayer(BasicRoles.Goon());

        assertHasGuns(sheriff);
    }

    public void testImmune() {
        addPlayer(BasicRoles.Sheriff());
        addPlayer(BasicRoles.Goon());
        Controller gf = addPlayer(BasicRoles.Godfather());

        assertNoGuns(gf);
    }

    public void testTeamWithFactionKill() {
        addPlayer(BasicRoles.Sheriff());
        Controller goon = addPlayer(BasicRoles.Goon());

        assertHasGuns(goon);
    }

    public void testFramedFaction() {
        Controller citizen = addPlayer(BasicRoles.Citizen());
        Controller framer = addPlayer(BasicRoles.Framer());

        frame(framer, citizen, Setup.MAFIA_C);
        assertHasGuns(citizen);
    }

    public void testVigiHasGuns() {
        Controller vigi = addPlayer(BasicRoles.Vigilante());
        Controller stoog = addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Agent());

        editRule(SetupModifierName.CHARGE_VARIABILITY, 0);
        modifyAbilityCharges(BasicRoles.Vigilante(), 1);

        assertHasGuns(vigi);

        skipDay();
        shoot(vigi, stoog);
        assertNoGuns(vigi);
    }

    public void assertHasGuns(Controller p) {
        armsCheck(p, ArmsDetector.HAS_GUNS);
    }

    public void assertNoGuns(Controller p) {
        armsCheck(p, ArmsDetector.NO_GUNS);
    }

    public void armsCheck(Controller p, String feedback) {
        setTarget(tsa, p);
        endNight();

        partialContains(tsa, feedback);
    }
}
