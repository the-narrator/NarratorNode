package integration.logic.abilities;

import java.util.ArrayList;

import game.abilities.Blacksmith;
import game.ai.Controller;
import game.ai.ControllerList;
import game.logic.exceptions.PlayerTargetingException;
import game.logic.support.Constants;
import game.logic.support.action.Action;
import game.logic.templates.BasicRoles;
import game.setups.Setup;
import integration.logic.SuperTest;
import models.FactionRole;
import models.Role;
import models.enums.AbilityModifierName;
import services.FactionRoleModifierService;
import services.RoleAbilityModifierService;
import util.RepeatabilityTest;
import util.TestUtil;
import util.game.LookupUtil;
import util.models.AbilityTestUtil;
import util.models.ActionTestUtil;

public class TestBlacksmith extends SuperTest {

    public TestBlacksmith(String name) {
        super(name);
    }

    private static final String REAL = Blacksmith.REAL;
    private static final String FAKE = Blacksmith.FAKE;
    private static final String NOGUN = Blacksmith.NOGUN;
    private static final String FAULTY = Blacksmith.FAULTY;
    private static final String NULL = null;

    private static Controller bs, c1, c2, gd, sk;

    private void setupBasic(BlacksmithBlock block, String opt2, String opt3, boolean gun, boolean vest) {
        c1 = addPlayer(BasicRoles.Doctor());
        c2 = addPlayer(BasicRoles.Doctor());
        bs = addPlayer(EvilBlacksmith());
        sk = addPlayer(BasicRoles.SerialKiller());
        gd = addPlayer(BasicRoles.GraveDigger());

        fakeVests();
        fakeGuns();

        ControllerList players = new ControllerList();
        if(gun)
            players.add(c1);
        if(vest)
            players.add(c2);

        setTarget(bs, Blacksmith.abilityType, opt2, opt3, players);
        nextNight();

        block.run();
        // cleanup that usually happens when tests 'finish'
        RepeatabilityTest.runRepeatabilityTest();
        charges = null;

        newNarrator();
        c1 = addPlayer(BasicRoles.Citizen());
        c2 = addPlayer(BasicRoles.Citizen());
        bs = addPlayer(EvilBlacksmith());
        gd = addPlayer(BasicRoles.GraveDigger());
        sk = addPlayer(BasicRoles.SerialKiller());

        fakeGuns();
        fakeVests();

        dayStart();

        voteOut(bs, c1, c2, gd);

        players = new ControllerList();
        if(gun)
            players.add(c1);
        if(vest)
            players.add(c2);

        setTarget(ActionTestUtil.getGraveDiggerAction(gd, Blacksmith.getAction(bs, players, opt2, opt3, game)));
        nextNight();
        block.run();
    }

    public void testSelection() {
        Controller p3 = addPlayer(BasicRoles.Blacksmith());
        Controller p1 = addPlayer(BasicRoles.Agent());
        addPlayer(BasicRoles.Agent());

        nightStart();

        Blacksmith bs = AbilityTestUtil.getGameAbility(Blacksmith.class, game);
        ArrayList<Object> s = bs.getActionDescription(
                new Action(p3.getPlayer(), Blacksmith.abilityType, Blacksmith.NOGUN, Blacksmith.REAL, p1.getPlayer()));
        for(Object o: s){
            if(o instanceof String){
                if(o.toString().toLowerCase().contains(Constants.VEST_COMMAND.toLowerCase()))
                    return;
            }
        }
        fail();
    }

    public void testFaultyCommands() {
        fakeGuns();
        fakeVests();

        Controller bs = addPlayer(BasicRoles.Blacksmith());
        Controller p1 = addPlayer(BasicRoles.Agent());
        addPlayer(BasicRoles.Agent());

        nightStart();

        command(bs, Blacksmith.COMMAND, p1.getName(), Blacksmith.REAL, Blacksmith.FAKE);
        assertEquals(Blacksmith.NOARMOR, bs.getPlayer().getActions().getFirst().getArg2());
    }

    interface BlacksmithBlock {
        void run();
    }

    public void testRealGun() {
        setupBasic(new BlacksmithBlock() {
            @Override
            public void run() {
                shoot(c1, c2);
                endNight();

                isDead(c2);
            }
        }, REAL, NOGUN, true, false);
    }

    public void testFakeGun() {
        setupBasic(new BlacksmithBlock() {
            @Override
            public void run() {
                shoot(c1, c2);
                endNight();

                isDead(c1);
            }
        }, FAULTY, NOGUN, true, false);
    }

    public void testRealVest() {
        setupBasic(new BlacksmithBlock() {
            @Override
            public void run() {
                setTarget(sk, c2);
                assertRealVestCount(1, c2);
                vest(c2);
                endNight();

                isAlive(c2);
            }
        }, NOGUN, REAL, false, true);
    }

    public void testFakeVest() {
        setupBasic(new BlacksmithBlock() {
            @Override
            public void run() {
                setTarget(sk, c2);
                assertFakeVestCount(1, c2);
                vest(c2);
                endNight();

                isDead(c2);
            }
        }, NOGUN, FAKE, false, true);
    }

    public void testRealGunRealVest() {
        setupBasic(new BlacksmithBlock() {
            @Override
            public void run() {
                shoot(c1, gd);
                setTarget(sk, c2);
                vest(c2);
                endNight();

                isAlive(c2);
                isDead(gd);
            }
        }, REAL, REAL, true, true);
    }

    public void testRealGunFakeVest() {
        setupBasic(new BlacksmithBlock() {
            @Override
            public void run() {
                shoot(c1, gd);
                setTarget(sk, c2);
                vest(c2);
                endNight();

                isDead(gd, c2);
            }
        }, REAL, FAKE, true, true);
    }

    public void testFakeGunFakeVest() {
        setupBasic(new BlacksmithBlock() {
            @Override
            public void run() {
                shoot(c1, gd);
                setTarget(sk, c2);
                vest(c2);
                endNight();

                isDead(c1, c2);
            }
        }, FAULTY, FAKE, true, true);
    }

    public void testFakeGunRealVest() {
        setupBasic(new BlacksmithBlock() {
            @Override
            public void run() {
                shoot(c1, gd);
                setTarget(sk, c2);
                vest(c2);
                endNight();

                isDead(c1);
                isAlive(c2);
            }
        }, FAULTY, REAL, true, true);
    }

    public void testFactionRoleOverrideModifier() {
        Controller bs_good = addPlayer(BasicRoles.Blacksmith());
        Controller bs_bad = addPlayer(EvilBlacksmith());
        Controller citizen = addPlayer(BasicRoles.Citizen());
        Controller citizen2 = addPlayer(BasicRoles.Citizen());

        Role role = BasicRoles.Blacksmith().role;
        RoleAbilityModifierService.upsert(role, Blacksmith.abilityType, AbilityModifierName.GS_FAULTY_GUNS, true);
        FactionRoleModifierService.addModifier(BasicRoles.Blacksmith(), AbilityModifierName.GS_FAULTY_GUNS,
                Blacksmith.abilityType, false);

        nightStart();

        try{
            setTarget(bs_good, Blacksmith.abilityType, FAKE, FAKE, citizen);
            fail();
        }catch(PlayerTargetingException e){
        }
        setTarget(bs_bad, Blacksmith.abilityType, FAULTY, FAKE, citizen, citizen2);

        nextNight();

        shoot(citizen, citizen2);
        setTarget(bs_bad, Blacksmith.abilityType, REAL, FAKE, bs_good, citizen);
        nextNight();

        isDead(citizen);

        vest(citizen2);
        shoot(bs_good, citizen2);

        endNight();

        isDead(citizen2);
    }

    public void testGoodBadBlacksmith() {
        Role blacksmithRole = BasicRoles.Blacksmith().role;
        FactionRole BADBlacksmithFactionRole = LookupUtil.findFactionRole(setup, "Blacksmith", Setup.OUTCAST_C).get();

        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller cit2 = addPlayer(BasicRoles.Citizen());
        Controller bs_good = addPlayer(BasicRoles.Blacksmith());
        Controller bs_bad = addPlayer(EvilBlacksmith());

        modifyRole(blacksmithRole, AbilityModifierName.GS_FAULTY_GUNS, false);
        modifyRole(blacksmithRole, AbilityModifierName.AS_FAKE_VESTS, false);

        FactionRoleModifierService.addModifier(BADBlacksmithFactionRole, AbilityModifierName.GS_FAULTY_GUNS,
                Blacksmith.abilityType, true);
        FactionRoleModifierService.addModifier(BADBlacksmithFactionRole, AbilityModifierName.AS_FAKE_VESTS,
                Blacksmith.abilityType, true);

        nightStart();

        assertEquals(Setup.TOWN_C, bs_good.getColor());

        Blacksmith bs_card = bs_bad.getPlayer().getAbility(Blacksmith.class);
        assertTrue(bs_card.modifiers.getBoolean(AbilityModifierName.AS_FAKE_VESTS, game));
        assertTrue(bs_card.modifiers.getBoolean(AbilityModifierName.GS_FAULTY_GUNS, game));

        bs_card = bs_good.getPlayer().getAbility(Blacksmith.class);
        assertFalse(bs_card.modifiers.getBoolean(AbilityModifierName.AS_FAKE_VESTS, game));
        assertFalse(bs_card.modifiers.getBoolean(AbilityModifierName.GS_FAULTY_GUNS, game));

        try{
            setTarget(bs_good, Blacksmith.abilityType, FAKE, FAKE, cit);
            fail();
        }catch(PlayerTargetingException e){
        }

        setTarget(bs_good, Blacksmith.abilityType, REAL, REAL, cit, cit2);
        setTarget(bs_bad, Blacksmith.abilityType, FAULTY, FAKE, cit, cit2);

        assertNotNull(bs_bad.getPlayer().getActions().getFirst().getArg1());
        assertNotNull(bs_bad.getPlayer().getActions().getFirst().getArg2());

        nextNight();
        shoot(cit, bs_good);
        nextNight();

        shoot(cit, bs_bad);
        endNight();
        isDead(cit);

        assertRealVestCount(1, cit2);
    }

    public void testBadTargetingRealEverything() {
        Controller bs = addPlayer(BasicRoles.Blacksmith());
        Controller p1 = addPlayer(BasicRoles.Citizen());
        Controller p2 = addPlayer(BasicRoles.Citizen());
        Controller sk = addPlayer(BasicRoles.SerialKiller());

        realGuns();
        realVests();

        nightStart();

        badTarget(bs, NULL, NULL, p1);
        badTarget(bs, null, FAKE, p1);
        badTarget(bs, NOGUN, null, p1, p2);
        badTarget(bs);
        badTarget(bs, p1);
        badTarget(bs, bs);
        badTarget(bs, p1, p1);
        badTarget(bs, p1, p2, sk);
        badTarget(bs, FAULTY, null, p1);
    }

    public void badTarget(Controller bs, String gunOpt, String vestOpt, Controller... targets) {
        try{
            setTarget(bs, Blacksmith.abilityType, gunOpt, vestOpt, targets);
            fail();
        }catch(PlayerTargetingException e){
        }
    }

    public void badTarget(Controller bs, Controller... targets) {
        badTarget(bs, null, null, targets);
    }

    public static void fakeGuns() {
        TestUtil.modifyAbility(Blacksmith.abilityType, AbilityModifierName.GS_FAULTY_GUNS, true);
    }

    public static void fakeVests() {
        TestUtil.modifyAbility(Blacksmith.abilityType, AbilityModifierName.AS_FAKE_VESTS, true);
    }

    public static FactionRole EvilBlacksmith() {
        return BasicRoles.getMember(Setup.OUTCAST_C, Blacksmith.abilityType);
    }

    public static void realGuns() {
        TestUtil.modifyAbility(Blacksmith.abilityType, AbilityModifierName.AS_FAKE_VESTS, false);
    }

    public static void realVests() {
        TestUtil.modifyAbility(Blacksmith.abilityType, AbilityModifierName.AS_FAKE_VESTS, false);
    }
}
