package integration.logic.abilities;

import game.abilities.Bomb;
import game.ai.Controller;
import game.logic.exceptions.NarratorException;
import game.logic.templates.BasicRoles;
import integration.logic.SuperTest;
import models.enums.AbilityModifierName;
import services.FactionRoleModifierService;

public class TestBomb extends SuperTest {

    public TestBomb(String name) {
        super(name);
    }

    public void testBasic() {
        addPlayer(BasicRoles.Citizen());
        Controller bomb = addPlayer(BasicRoles.Bomb());
        Controller maf = addPlayer(BasicRoles.Goon());

        mafKill(maf, bomb);

        endNight();

        assertGameOver();
        isDead(bomb, maf);
    }

    public void testNoBackToBackModifier() {
        try{
            FactionRoleModifierService.addModifier(BasicRoles.Bomb(), AbilityModifierName.BACK_TO_BACK,
                    Bomb.abilityType, false);
            fail();
        }catch(NarratorException e){
        }
    }

    public void testNoZeroWeightModifier() {
        try{
            FactionRoleModifierService.addModifier(BasicRoles.Bomb(), AbilityModifierName.ZERO_WEIGHTED,
                    Bomb.abilityType, false);
            fail();
        }catch(NarratorException e){
        }
    }
}
