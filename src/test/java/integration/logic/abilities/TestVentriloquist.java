package integration.logic.abilities;

import game.abilities.Assassin;
import game.abilities.Burn;
import game.abilities.GraveDigger;
import game.abilities.JailCreate;
import game.abilities.Mayor;
import game.abilities.Puppet;
import game.abilities.Ventriloquist;
import game.abilities.Vigilante;
import game.ai.Controller;
import game.event.DayChat;
import game.event.DeadChat;
import game.logic.Player;
import game.logic.exceptions.ChatException;
import game.logic.exceptions.IllegalActionException;
import game.logic.exceptions.NarratorException;
import game.logic.exceptions.PlayerTargetingException;
import game.logic.exceptions.VotingException;
import game.logic.support.action.Action;
import game.logic.templates.BasicRoles;
import integration.logic.SuperTest;
import models.enums.AbilityModifierName;
import models.enums.SetupModifierName;
import services.FactionRoleModifierService;

public class TestVentriloquist extends SuperTest {

    public TestVentriloquist(String name) {
        super(name);
    }

    public void testBasicVent() {
        Controller vent = addPlayer(BasicRoles.Ventriloquist());
        Controller puppet = addPlayer(BasicRoles.Citizen());
        Controller cit = addPlayer(BasicRoles.Citizen());

        dayStart();
        assertFalse(vent.getCommands().contains(Puppet.abilityType));
        skipDay();

        createPuppet(vent, puppet);
        endNight();

        hasPuppets(vent);
        assertTrue(vent.getCommands().contains(Puppet.abilityType));

        partialContains(puppet, Ventriloquist.FEEDBACK);

        assertNull(game.getEventManager().getDayChat().getKey(puppet.getPlayer()));

        try{
            say(puppet, "I cannot say this", DayChat.KEY);
            fail();
        }catch(ChatException e){
        }

        try{
            vote(puppet, vent);
            fail();
        }catch(NarratorException e){
        }

        try{
            skipVote(puppet);
            fail();
        }catch(NarratorException e){
        }

        Player ventPlayer = vent.getPlayer();
        Player citPlayer = cit.getPlayer();
        try{
            vent.doDayAction(new Action(ventPlayer, Puppet.abilityType, citPlayer));
            fail();
        }catch(VotingException | PlayerTargetingException | IllegalActionException e){
        }

        isPuppeted(cit, false);
        try{
            ventVote(cit, cit, vent);
            fail();
        }catch(VotingException | IllegalActionException | PlayerTargetingException e){
        }

        try{
            ventVote(puppet, cit, cit);
            fail();
        }catch(VotingException | IllegalActionException | PlayerTargetingException e){
        }

        try{
            vent.doDayAction(new Action(ventPlayer, Puppet.abilityType, Puppet.COMMAND, (String) null));
            fail();
        }catch(NarratorException e){
        }

        ventVote(vent, puppet, cit);

        vote(vent, cit);

        endNight();
        assertGameOver();
    }

    public void testSkipUnvoting() {
        Controller vent = addPlayer(BasicRoles.Ventriloquist());
        Controller puppet = addPlayer(BasicRoles.Citizen());
        Controller cit = addPlayer(BasicRoles.Citizen());

        dayStart();
        assertNotContains(vent.getCommands(), Puppet.abilityType);
        skipDay();

        createPuppet(vent, puppet);
        endNight();

        ventVote(vent, puppet, cit);
        try{
            skipVote(puppet);
            fail();
        }catch(NarratorException e){
        }

        ventSkipVote(vent, puppet);

        assertVoteTarget(game.skipper, puppet);

        try{
            unvote(puppet);
            fail();
        }catch(NarratorException e){
        }
        ventUnvote(vent, puppet);

        assertVoteTarget(null, puppet);
    }

    public void testDayActionStopping() {
        Controller mayor = addPlayer(BasicRoles.Mayor());
        Controller gs = addPlayer(BasicRoles.Gunsmith());
        Controller jailor = addPlayer(BasicRoles.Jailor());
        Controller vent = addPlayer(BasicRoles.Ventriloquist());
        Controller ars = addPlayer(BasicRoles.Arsonist());
        Controller assassin = addPlayer(BasicRoles.Assassin());

        editRule(SetupModifierName.GS_DAY_GUNS, true);

        setTarget(gs, mayor);
        setTarget(ars, mayor);
        createPuppet(vent, mayor);

        endNight();

        assertNotContains(mayor.getCommands(), Mayor.abilityType);
        assertNotContains(mayor.getCommands(), Vigilante.abilityType);
        assertContains(ars.getCommands(), Burn.abilityType);

        try{
            shoot(mayor, vent);
            fail();
        }catch(IllegalActionException | PlayerTargetingException e){
        }

        try{
            reveal(mayor);
            fail();
        }catch(IllegalActionException | PlayerTargetingException e){
        }

        skipDay();

        createPuppet(vent, assassin);
        endNight();

        reveal(mayor);
        assertVotePower(game.getInt(SetupModifierName.MAYOR_VOTE_POWER) + 1, mayor);
        // gotta add puppet vote as an ability, not keep reinsantiating it.
        assertContains(vent.getCommands(), Puppet.abilityType);

        try{
            doDayAction(assassin, vent);
            fail();
        }catch(IllegalActionException | PlayerTargetingException e){
        }
        isAlive(vent);

        skipDay();

        createPuppet(vent, jailor);
        endNight();

        doDayAction(assassin, mayor);
        assertNotContains(assassin.getCommands(), Assassin.abilityType);
        assertNotContains(jailor.getCommands(), JailCreate.abilityType);

        try{
            jail(jailor, vent);
            fail();
        }catch(NarratorException e){
        }
        skipDay();

        setTarget(ars, assassin);
        createPuppet(vent, ars);

        endNight();

        assertNoAcceptableTargets(ars, Burn.abilityType);
        assertContains(jailor.getCommands(), JailCreate.abilityType);
    }

    public void testDeadChatAccess() {
        Controller fodder2 = addPlayer(BasicRoles.Citizen());
        Controller fodder3 = addPlayer(BasicRoles.Citizen());
        Controller assassin = addPlayer(BasicRoles.Assassin());
        Controller vent = addPlayer(BasicRoles.Ventriloquist());
        Controller fodder = addPlayer(BasicRoles.Witch());
        Controller sk = addPlayer(BasicRoles.SerialKiller());
        Controller poisoner = addPlayer(BasicRoles.Poisoner());

        createPuppet(vent, fodder);
        setTarget(sk, fodder);

        endNight();

        say(fodder, "Can anyone hear me?", DeadChat.KEY);
        skipDay();

        createPuppet(vent, fodder2);
        endNight();

        try{
            say(fodder2, "Can I really not say anything?", DayChat.KEY);
            fail();
        }catch(ChatException e){
        }

        doDayAction(assassin, fodder2);
        say(fodder2, "Can anyone hear ME?", DeadChat.KEY);

        skipDay();

        setTarget(poisoner, fodder3);
        createPuppet(vent, fodder3);
        endNight();

        skipDay();
        isDead(fodder3);
        say(fodder3, "Can anyone hear ME?", DeadChat.KEY);
    }

    public void testVentSameTarget() {
        Controller vent1 = addPlayer(BasicRoles.Ventriloquist());
        Controller vent2 = addPlayer(BasicRoles.Ventriloquist());
        Controller vent3 = addPlayer(BasicRoles.Ventriloquist());
        Controller fodder1 = addPlayer(BasicRoles.Citizen());
        Controller fodder3 = addPlayer(BasicRoles.Framer());
        Controller ass1 = addPlayer(BasicRoles.Assassin());
        Controller ass2 = addPlayer(BasicRoles.Assassin());

        createPuppet(vent3, fodder3);
        createPuppet(vent2, fodder1);
        createPuppet(vent1, fodder1);
        endNight(vent1);
        endNight();

        isPuppeted(fodder1);
        hasPuppets(vent2, false);
        hasPuppets(vent1);
        assertEquals(vent1, fodder1.getPlayer().getPuppeteer());

        try{
            ventVote(vent1, vent1, fodder3);
            fail();
        }catch(NarratorException e){
        }

        doDayAction(ass1, vent3);

        vote(fodder3, ass1);
        isPuppeted(vent3, false);

        doDayAction(ass2, vent1);
        isPuppeted(fodder1);
        hasPuppets(vent2);

        ventVote(vent2, fodder1, vent2);
    }

    public void testVentDyingAfterVentAction() {
        Controller electro = addPlayer(BasicRoles.ElectroManiac());
        Controller vent = addPlayer(BasicRoles.Ventriloquist());
        Controller fodder = addPlayer(BasicRoles.Framer());
        Controller mover = addPlayer(BasicRoles.Detective());
        addPlayer(BasicRoles.Amnesiac());

        electrify(electro, mover, vent);

        createPuppet(vent, fodder);
        setTarget(mover, vent);

        endNight();

        isPuppeted(fodder, false);
    }

    private void createPuppet(Controller vent, Controller puppet) {
        setTarget(vent, puppet, Ventriloquist.abilityType);
    }

    public void testBreadedVent() {
        Controller baker = addPlayer(BasicRoles.Baker());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller vent = addPlayer(BasicRoles.Ventriloquist());
        Controller witch = addPlayer(BasicRoles.Witch());

        setTarget(baker, vent);
        nextNight();

        createPuppet(vent, baker);
        createPuppet(vent, cit);
        endNight();

        ventVote(vent, baker, vent);
        ventVote(vent, cit, vent);
        vote(witch, vent);

        isDead(vent);
    }

    public void testBMedVentrilo() {
        Controller bmer = addPlayer(BasicRoles.Blackmailer());
        Controller vent = addPlayer(BasicRoles.Ventriloquist());
        Controller puppet = addPlayer(BasicRoles.Citizen());

        setTarget(bmer, vent);
        createPuppet(vent, puppet);
        endNight();

        try{
            vote(vent, bmer);
            fail();
        }catch(VotingException e){
        }

        ventVote(vent, puppet, bmer);
        assertTrue(vent.getPlayer().getPuppets().contains(puppet.getPlayer()));
        try{
            say(vent, "I can't talk from my mouth", DayChat.KEY);
            fail();
        }catch(ChatException e){
        }
        assertEquals(1, vent.getChatKeys().size());
        assertTrue(vent.getChatKeys().contains(puppet.getName()));
        say(vent, gibberish(), puppet.getName());
        partialContains(gibberish);
    }

    public void testDisguisedVented() {
        Controller citizenA = addPlayer(BasicRoles.Citizen());
        Controller citizenB = addPlayer(BasicRoles.Citizen());
        Controller disguiserC = addPlayer(BasicRoles.Disguiser());
        Controller disguiserD = addPlayer(BasicRoles.Disguiser());
        Controller agentE = addPlayer(BasicRoles.Agent());
        Controller ventF = addPlayer(BasicRoles.Ventriloquist());
        Controller poisonerG = addPlayer(BasicRoles.Poisoner());

        createPuppet(ventF, citizenA);
        setTarget(disguiserC, citizenA);
        endNight();

        isPuppeted(disguiserC);
        assertVotePower(1, disguiserC);

        ventVote(ventF, disguiserC, agentE);
        vote(ventF, agentE);
        vote(disguiserD, agentE);
        vote(citizenB, agentE);

        isDead(agentE);

        createPuppet(ventF, citizenB);
        setTarget(disguiserD, citizenB);
        endNight();

        ventVote(ventF, disguiserD, poisonerG);
        vote(ventF, poisonerG);
        vote(disguiserC, poisonerG);
    }

    public void testDeadPuppet() {
        Controller assassin = addPlayer(BasicRoles.Assassin());
        Controller vent = addPlayer(BasicRoles.Ventriloquist());
        Controller fodder = addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Architect());

        createPuppet(vent, fodder);
        endNight();

        doDayAction(assassin, fodder);
        hasPuppets(vent, false);
    }

    public void testVentCooldown() {
        Controller vent = addPlayer(BasicRoles.Ventriloquist());
        Controller pupp = addPlayer(BasicRoles.Poisoner());
        Controller pup2 = addPlayer(BasicRoles.Agent());
        Controller witch = addPlayer(BasicRoles.Witch());

        modifyRoleAbility(BasicRoles.Ventriloquist().role, AbilityModifierName.BACK_TO_BACK, Ventriloquist.abilityType,
                false);

        createPuppet(vent, pupp);
        nextNight();

        assertFalse(pupp.getPlayer().in(vent.getPlayer().getAcceptableTargets(Ventriloquist.abilityType)));
        try{
            createPuppet(vent, pupp);
            fail();
        }catch(PlayerTargetingException e){
        }

        createPuppet(vent, pup2);
        nextNight();

        assertFalse(pup2.getPlayer().in(vent.getPlayer().getAcceptableTargets(Ventriloquist.abilityType)));
        try{
            createPuppet(vent, pup2);
            fail();
        }catch(PlayerTargetingException e){
        }

        createPuppet(vent, pupp);
        witch(witch, vent, pup2);
        endNight();

        assertNull(pup2.getPlayer().getPuppeteer());
        hasPuppets(vent, false);
    }

    public void testNoBackToBackFromWitch() {
        Controller ventriloquist = addPlayer(BasicRoles.Ventriloquist());
        Controller puppet = addPlayer(BasicRoles.Citizen());
        Controller witch = addPlayer(BasicRoles.Witch());

        modifyRoleAbility(BasicRoles.Ventriloquist().role, AbilityModifierName.BACK_TO_BACK, Ventriloquist.abilityType,
                false);

        setTarget(witch, ventriloquist, puppet);
        nextNight();

        try{
            setTarget(ventriloquist, puppet);
            fail();
        }catch(NarratorException e){
        }

        setTarget(witch, ventriloquist, puppet);
        endNight();

        hasPuppets(ventriloquist, false);
    }

    public void testVentControllingVent() {
        Controller vent1 = addPlayer(BasicRoles.Ventriloquist());
        Controller vent2 = addPlayer(BasicRoles.Ventriloquist());
        Controller cit = addPlayer(BasicRoles.Citizen());

        createPuppet(vent2, vent1);
        createPuppet(vent1, cit);

        endNight();

        say(vent1, "You puppetted me!", cit.getName());
        ventVote(vent2, vent1, cit);

        say(vent2, "haha", vent1.getName());
        ventVote(vent2, vent1, vent2);
    }

    public void testVentCorpse() {
        Controller digger = addPlayer(BasicRoles.GraveDigger());
        Controller vent = addPlayer(BasicRoles.Ventriloquist());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller lookout = addPlayer(BasicRoles.Lookout());

        voteOut(vent, digger, cit, lookout);

        setTarget(lookout, cit);
        setTarget(digger, GraveDigger.abilityType, null, (String) null, vent, cit);
        endNight();

        TestLookout.seen(lookout, vent);
        isPuppeted(cit, false);
        partialExcludes(cit, Ventriloquist.FEEDBACK);
    }

    public void testVentCharges() {
        Controller vent = addPlayer(BasicRoles.Ventriloquist());
        Controller citizen = addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Citizen());

        FactionRoleModifierService.addModifier(BasicRoles.Ventriloquist(), AbilityModifierName.CHARGES,
                Ventriloquist.abilityType, 1);

        createPuppet(vent, citizen);
        nextNight();

        try{
            setTarget(vent, citizen);
            fail();
        }catch(NarratorException e){

        }
    }
}
