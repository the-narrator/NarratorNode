package integration.logic.abilities;

import java.util.List;

import game.abilities.Burn;
import game.abilities.Citizen;
import game.abilities.Douse;
import game.abilities.Undouse;
import game.ai.Controller;
import game.ai.ControllerList;
import game.event.EventList;
import game.event.Message;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.exceptions.NarratorException;
import game.logic.support.Constants;
import game.logic.templates.BasicRoles;
import game.logic.templates.HTMLDecoder;
import integration.logic.SuperTest;
import models.enums.AbilityModifierName;
import models.enums.AbilityType;
import models.enums.GameModifierName;
import models.enums.SetupModifierName;
import models.enums.VoteSystemTypes;
import services.FactionAbilityService;
import services.FactionRoleModifierService;
import services.GameService;

public class TestArsonist extends SuperTest {

    private static final AbilityType DOUSE = Douse.abilityType;
    private static final AbilityType UNDOUSE = Undouse.abilityType;

    public TestArsonist(String name) {
        super(name);
    }

    public void testBasicAbility() {
        Controller c1 = addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Citizen(), 2);
        Controller a1 = addPlayer(BasicRoles.Arsonist());
        Controller a2 = addPlayer(BasicRoles.Arsonist());

        setTarget(a1, c1, DOUSE);
        setTarget(a2);

        endNight();

        isDead(c1);
        assertEquals(0, c1.getPlayer().getDeathDay());
    }

    public void testModifierTextNoArchitect() {
        Controller arson = addPlayer(BasicRoles.Arsonist());
        addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Goon());

        dayStart();

        List<String> arsonRoleSpecs = ((Player) arson).getRoleCardDetails();
        partialExcludes(arsonRoleSpecs, Burn.ARCHITECT_ROOM_SPREAD_TEXT);
        partialExcludes(arsonRoleSpecs, Burn.ARCHITECT_ROOM_NO_SPREAD_TEXT);
    }

    public void testFeedback() {
        Controller c1 = addPlayer(BasicRoles.Citizen());
        Controller a1 = addPlayer(BasicRoles.Arsonist());
        addPlayer(BasicRoles.Citizen());

        setName(c1, "Ciitizen");

        setTarget(a1, c1, DOUSE);
        partialExcludes(a1, Citizen.class.getSimpleName());
    }

    public void testBurnUndouse() {
        Controller c1 = addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Citizen(), 2);
        Controller a1 = addPlayer(BasicRoles.Arsonist());

        nightStart();
        burn(a1);

        nextNight();

        setTarget(a1, c1, DOUSE);
    }

    public void testBusArson() {
        Controller bd = addPlayer(BasicRoles.BusDriver());
        Controller c1 = addPlayer(BasicRoles.Citizen());
        Controller a1 = addPlayer(BasicRoles.Arsonist());

        setTarget(a1, c1, DOUSE);
        drive(bd, bd, c1);

        endNight();

        assertStatus(c1, Douse.abilityType, false);
        assertStatus(bd, Douse.abilityType, true);
    }

    public void testArsonDayBurnEnd() {
        Controller c1 = addPlayer(BasicRoles.Citizen());
        Controller c2 = addPlayer(BasicRoles.Citizen());
        Controller c3 = addPlayer(BasicRoles.Citizen());
        Controller a1 = addPlayer(BasicRoles.Arsonist());
        Controller a2 = addPlayer(BasicRoles.Arsonist());

        nightStart();

        setTarget(a1, c1, DOUSE);
        setTarget(a2, c2, DOUSE);

        endNight();

        vote(c1, a1);
        vote(c2, a1);
        vote(a1, c3);
        vote(a2, c3);

        assertInProgress();
        burn(a2);

        assertGameOver();
    }

    public void testArsonDayBurnedNoEnd() {
        Controller c1 = addPlayer(BasicRoles.Citizen());
        Controller c2 = addPlayer(BasicRoles.Citizen());
        Controller c3 = addPlayer(BasicRoles.Citizen());
        Controller a1 = addPlayer(BasicRoles.Arsonist());
        Controller a2 = addPlayer(BasicRoles.Arsonist());

        setAllies(BasicRoles.Arsonist(), BasicRoles.Arsonist());

        setTarget(a1, c1, DOUSE);
        setTarget(a2, c2, DOUSE);

        endNight();

        vote(c1, a1);
        vote(c2, a1);
        vote(a2, c3);

        assertInProgress();
        burn(a2);

        assertTrue(c1.getPlayer().getDeathType().getList().contains(Constants.ARSON_KILL_FLAG));
        assertFalse(c1.getPlayer().getDeathType().isLynch());

        EventList el = game.getEventManager().getEvents(Message.PUBLIC);

        assertTrue(el.access(Message.PUBLIC, new HTMLDecoder()).contains("explosion"));

        isDead(c1, c2);

        vote(a1, c3);

        isDead(c3);
        isDead(c2);
        isDead(c1);

        assertGameOver();

        isWinner(a2);
    }

    public void testUndouseWhileOtherBurning() {
        Controller c1 = addPlayer(BasicRoles.Citizen());
        Controller c2 = addPlayer(BasicRoles.Citizen());
        Controller c3 = addPlayer(BasicRoles.BusDriver());
        Controller a1 = addPlayer(BasicRoles.Arsonist());
        Controller a2 = addPlayer(BasicRoles.Arsonist());

        setTarget(a1, a1);
        nextNight();

        burn(a2);

        setTarget(a1, a2, UNDOUSE);

        partialContains(a1, "You will douse yourself");

        endNight();

        isAlive(a1, a2, c1, c2, c3);
    }

    public void testUndousingText() {
        addPlayer(BasicRoles.Citizen(), 2);
        Controller c3 = addPlayer(BasicRoles.Citizen());
        Controller ars = addPlayer(BasicRoles.Arsonist());

        nightStart();

        setTarget(ars, c3);
        setTarget(ars, c3, Undouse.abilityType);

        partialContains(ars, "dousing");
    }

    public void testCantBurnImmediatelyAfter() {
        addPlayer(BasicRoles.Citizen(), 3);
        Controller a1 = addPlayer(BasicRoles.Arsonist());

        nightStart();

        burn(a1);

        endNight();

        assertFalse(a1.getPlayer().hasDayAction(Burn.abilityType));

        skipDay();
        endNight();

        burn(a1);

        skipDay();

        assertFalse(a1.getPlayer().getAbility(Burn.abilityType).isAcceptableTarget(a1.getPlayer()));
        assertTrue(a1.getPlayer().getAcceptableTargets(Burn.abilityType) != null);
    }

    public void testCantDayBurnAgain() {
        addPlayer(BasicRoles.Citizen(), 3);
        Controller a1 = addPlayer(BasicRoles.Arsonist());

        editRule(SetupModifierName.ARSON_DAY_IGNITES, 1);

        dayStart();

        burn(a1);

        skipDay();
        endNight();

        assertNotContains(a1.getCommands(), Burn.abilityType);
    }

    public void testSelfKill() {
        addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.BusDriver());
        Controller a1 = addPlayer(BasicRoles.Arsonist());
        Controller a2 = addPlayer(BasicRoles.Arsonist());

        nightStart();

        setTarget(a1);
        setTarget(a2, a2, DOUSE);

        endNight();

        isAlive(a1);
        skipDay();
        setTarget(a1, a1, DOUSE);
        setTarget(a2, a2, DOUSE);

        endNight();
        skipDay();

        setTarget(a1);
        setTarget(a2);
        endNight();

        isDead(a1, a2);
    }

    public void testDousedNotBurning() {
        addPlayer(BasicRoles.BusDriver(), 2);
        Controller a1 = addPlayer(BasicRoles.Arsonist());
        Controller a2 = addPlayer(BasicRoles.Arsonist());

        setTarget(a1, a1, DOUSE);
        setTarget(a2, a2, DOUSE);

        nextNight();

        assertNull(a1.getPlayer().getAcceptableTargets(Burn.abilityType));
        setTarget(a1);
        endNight();

        isDead(a1);
    }

    public void testGameEnd() {
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller arsonist = addPlayer(BasicRoles.Arsonist());
        addPlayer(BasicRoles.Amnesiac(), 6);

        setTarget(arsonist, cit);
        endNight();

        burn(arsonist);

        isDead(cit);
        assertGameOver();
    }

    public void testNoBurning() {
        addPlayer(BasicRoles.Citizen());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller ars = addPlayer(BasicRoles.Arsonist());

        setTarget(ars, cit);
        nextNight();

        burn(ars);
        cancelAction(ars, 0);
        assertActionSize(0, ars);

        endNight();

        isAlive(cit);
    }

    public void testNoBackToBackModifier() {
        try{
            FactionRoleModifierService.addModifier(BasicRoles.Arsonist(), AbilityModifierName.BACK_TO_BACK,
                    Burn.abilityType, false);
            fail();
        }catch(NarratorException e){
        }
    }

    public void testNoZeroWeightedModifier() {
        try{
            FactionRoleModifierService.addModifier(BasicRoles.Arsonist(), AbilityModifierName.ZERO_WEIGHTED,
                    Burn.abilityType, false);
            fail();
        }catch(NarratorException e){
        }
    }

    public void testBurnDeathRemovesDeadMultivotes() {
        GameService.upsertModifier(game, GameModifierName.VOTE_SYSTEM, VoteSystemTypes.MULTI_VOTE_PLURALITY.value);

        Controller arsonist = addPlayer(BasicRoles.Arsonist());
        Controller fodder1 = addPlayer(BasicRoles.Citizen());
        Controller fodder2 = addPlayer(BasicRoles.Citizen());
        ControllerList citizens = addPlayer(BasicRoles.Citizen(), 3);

        setTarget(arsonist, fodder1);
        endNight();
        endPhase();

        assertIsNight();

        setTarget(arsonist, fodder2);
        endNight();

        vote(arsonist, fodder1, fodder2, citizens.getFirst());

        burn(arsonist);

        isDead(fodder1, fodder2);
        PlayerList arsonVoteTargets = game.voteSystem.getVoteTargets(arsonist.getPlayer());
        assertEquals(1, arsonVoteTargets.size());
        assertTrue(arsonVoteTargets.contains(citizens.getFirst().getPlayer()));
    }

    public void testFactionalBurn() {
        Controller goon = addPlayer(BasicRoles.Goon());
        Controller citizen = addPlayer(BasicRoles.Citizen());
        Controller arson = addPlayer(BasicRoles.Arsonist());
        addPlayer(BasicRoles.Citizen());

        FactionAbilityService.createFactionAbility(BasicRoles.Goon().faction, Burn.abilityType);

        nightStart();

        setTarget(arson, citizen, Douse.abilityType);

        nextNight();

        // this used to null pointer
        goon.getPlayer().getAcceptableTargets(Burn.abilityType);

        burn(goon);
        endNight();

        isDead(citizen);
    }
}
