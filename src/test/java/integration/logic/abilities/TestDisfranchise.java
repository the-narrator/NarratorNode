package integration.logic.abilities;

import java.util.ArrayList;

import game.ai.Controller;
import game.event.Message;
import game.logic.exceptions.NarratorException;
import game.logic.templates.BasicRoles;
import integration.logic.SuperTest;
import models.enums.AbilityModifierName;

public class TestDisfranchise extends SuperTest {

    public TestDisfranchise(String name) {
        super(name);
    }

    Controller axe;

    @Override
    public void roleInit() {
        axe = addPlayer(BasicRoles.Disfranchiser());
    }

    public void testBasic() {
        Controller cit = addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Citizen());

        setTarget(axe, cit);
        endNight();

        try{
            vote(cit, axe);
            fail();
        }catch(NarratorException e){
        }
    }

    public void testAnnounced() {
        Controller serialKiller = addPlayer(BasicRoles.SerialKiller());
        Controller cit = addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Citizen());
        modifyRole(BasicRoles.Disfranchiser().role, AbilityModifierName.DISFRANCHISED_ANNOUNCED, true);

        setTarget(axe, cit);
        ArrayList<Message> announcements = addAnnouncementListener();
        endNight();

        assertDisfranchiseIsAnnounced(cit, announcements);

        skipDay();
        setTarget(serialKiller, cit);
        endNight();

        assertDisfranchiseIsAnnounced(cit, announcements);
    }

    public void testIsNotAnnounced() {
        Controller cit = addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Citizen());
        modifyRole(BasicRoles.Disfranchiser().role, AbilityModifierName.DISFRANCHISED_ANNOUNCED, false);

        setTarget(axe, cit);
        ArrayList<Message> announcements = addAnnouncementListener();
        endNight();

        assertDisfranchiseIsNotAnnounced(announcements);
    }

    public void testNoAnnouncementIfDead() {
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller serialKiller = addPlayer(BasicRoles.SerialKiller());
        addPlayer(BasicRoles.Citizen());
        modifyRole(BasicRoles.Disfranchiser().role, AbilityModifierName.DISFRANCHISED_ANNOUNCED, true);

        setTarget(axe, cit);
        setTarget(serialKiller, cit);
        ArrayList<Message> announcements = addAnnouncementListener();
        endNight();

        assertDisfranchiseIsNotAnnounced(announcements);

        nextDay();
        nextDay();

        assertDisfranchiseIsNotAnnounced(announcements);
    }

    private void assertDisfranchiseIsNotAnnounced(ArrayList<Message> announcements) {
        for(Message message: announcements){
            if(message.access(Message.PUBLIC).contains(" may not vote today."))
                fail("Announcement was not supposed to happen.");
        }
    }

    private void assertDisfranchiseIsAnnounced(Controller cit, ArrayList<Message> announcements) {
        for(Message message: announcements){
            String stringMessage = message.access(Message.PUBLIC);
            if(stringMessage.contains(" may not vote today.") && stringMessage.contains(cit.getName()))
                return;
        }
        fail("Announcement was supposed to happen.");
    }
}
