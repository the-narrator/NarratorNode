package integration.logic.abilities;

import java.util.ArrayList;
import java.util.Optional;

import game.abilities.Interceptor;
import game.ai.Controller;
import game.event.Feedback;
import game.logic.Player;
import game.logic.templates.BasicRoles;
import integration.logic.SuperTest;
import models.FactionRole;

public class TestInterceptor extends SuperTest {

    public TestInterceptor(String s) {
        super(s);
    }

    Controller interceptor;

    @Override
    public void roleInit() {
        interceptor = addPlayer(BasicRoles.Interceptor());
    }

    public void testNoKill() {
        addPlayer(BasicRoles.Citizen());
        Controller cit = addPlayer(BasicRoles.Citizen());

        setTarget(interceptor, cit);
        endNight();

        isAlive(cit);
    }

    public void testBasicFunction() {
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller doc = addPlayer(BasicRoles.Doctor());
        Controller witch = addPlayer(BasicRoles.Witch());

        setTarget(interceptor, cit);
        setTarget(doc, cit);
        witch(witch, cit, cit);
        endNight();

        seen(doc, interceptor);
        seen(cit, interceptor);

        isDead(witch);
        isAlive(doc, cit);
    }

    public void testDoctorSave() {
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller doc = addPlayer(BasicRoles.Doctor());
        Controller witch = addPlayer(BasicRoles.Witch());

        setTarget(interceptor, cit);
        setTarget(doc, witch);
        witch(witch, cit, cit);
        endNight();

        seen(witch, interceptor);
        seen(doc, interceptor);
        seen(cit, interceptor);
    }

    public void testBakerKill() {
        Controller baker = addPlayer(BasicRoles.Baker());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller lookout = addPlayer(BasicRoles.Lookout());

        setTarget(baker, interceptor);
        nextNight();

        intercept(cit);
        intercept(cit);
        setTarget(baker, cit);
        setTarget(lookout, cit);
        endNight();

        isDead(lookout, baker);
        isAlive(cit, interceptor);
    }

    public void testTwoInterceptors() {
        Controller intc2 = addPlayer(BasicRoles.Interceptor());
        Controller cit = addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Goon(), 2);

        setTarget(intc2, cit);
        setTarget(interceptor, cit);
        endNight();

        isDead(interceptor, intc2);

        partialContains(intc2, Interceptor.DEATH_FEEDBACK);
        partialContains(interceptor, Interceptor.DEATH_FEEDBACK);
    }

    public void testBlock() {
        Controller strip = addPlayer(BasicRoles.Escort());
        Controller sher = addPlayer(BasicRoles.Lookout());

        setTarget(strip, interceptor);
        setTarget(interceptor, interceptor);
        setTarget(sher, interceptor);
        endNight();

        isAlive(strip, interceptor, sher);
        partialExcludes(strip, Interceptor.DEATH_FEEDBACK);
        partialExcludes(sher, Interceptor.DEATH_FEEDBACK);
        partialExcludes(interceptor, Interceptor.DEATH_FEEDBACK);
    }

    public void testBG() {
        Controller bg = addPlayer(BasicRoles.Bodyguard());
        Controller lookout = addPlayer(BasicRoles.Lookout());
        addPlayer(BasicRoles.Agent());

        setTarget(interceptor, interceptor);
        setTarget(lookout, interceptor);
        setTarget(bg, lookout);
        endNight();

        isDead(interceptor, bg);
    }

    public void testEMCase() {
        Controller em = addPlayer(BasicRoles.ElectroManiac());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller lookout = addPlayer(BasicRoles.Lookout());

        electrify(em, cit, lookout);

        setTarget(lookout, cit);
        setTarget(interceptor, cit);
        endNight();

        isDead(cit);
    }

    public void testLateActors() {
        Controller lookout = addPlayer(BasicRoles.Lookout());
        Controller detect = addPlayer(BasicRoles.Detective());
        addPlayer(BasicRoles.Architect());

        setTarget(interceptor, interceptor);
        setTarget(detect, interceptor);
        setTarget(lookout, interceptor);
        endNight(lookout);
        endNight();

        isDead(lookout);
        seen(detect, interceptor);
    }

    public void testPublicDescription() {
        addPlayer(BasicRoles.Citizen(), 2);

        startDay();

        FactionRole factionRole = interceptor.getPlayer().gameRole.factionRole;
        ArrayList<String> description = factionRole.getPublicDescription(Optional.of(game));

        assertTrue(
                description.contains("Identity is revealed to all visitors of " + factionRole.getName() + " target"));
    }

    public static void seen(Controller visitor_c, Controller intc) {
        Player visitor = visitor_c.getPlayer();
        ArrayList<Object> parts = Interceptor.FeedbackGenerator(intc.getPlayer());
        Feedback f = new Feedback(game.skipper);
        f.add(parts);
        f.setVisibility(visitor);
        String feedback = f.access(visitor);
        f.removeVisiblity(visitor);
        partialContains(visitor, feedback);
    }

    private void intercept(Controller target) {
        setTarget(interceptor, target, Interceptor.abilityType);
    }
}
