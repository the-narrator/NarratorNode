package integration.logic;

import game.abilities.Hidden;
import game.logic.templates.BasicRoles;
import game.setups.Setup;
import models.FactionRole;
import models.enums.RoleModifierName;
import services.FactionRoleModifierService;
import services.HiddenService;
import services.HiddenSpawnService;

public class TestHiddenSpawns extends SuperTest {

    public TestHiddenSpawns(String name) {
        super(name);
    }

    public void testHiddenCannotFullySpawn() {
        FactionRole[] factionRoles = new FactionRole[] { BasicRoles.Citizen(), BasicRoles.Goon(), BasicRoles.Doctor(),
                BasicRoles.Detective() };
        Hidden hidden = HiddenService.createHidden(setup, "BadHidden");
        addSpawnableRole(hidden, factionRoles[0], 0);
        addSpawnableRole(hidden, factionRoles[1], 0);
        addSpawnableRole(hidden, factionRoles[2], 0);
        addSpawnableRole(hidden, factionRoles[3], 5);

        for(FactionRole factionRole: factionRoles){
            addPlayer();
            FactionRoleModifierService.upsertModifier(factionRole, RoleModifierName.UNIQUE, true);
            addRole(hidden, 1);
        }

        assertBadGameSettings();
        BrainEndGame = false;
    }

    private void addSpawnableRole(Hidden hidden, FactionRole factionRole, int minPlayerCount) {
        HiddenSpawnService.addSpawnableRoles(hidden, factionRole, minPlayerCount, Setup.MAX_PLAYER_COUNT);
    }

}
