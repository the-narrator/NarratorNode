package integration.logic;

import game.abilities.Douse;
import game.abilities.JailCreate;
import game.abilities.Ventriloquist;
import game.abilities.Vote;
import game.ai.Controller;
import game.logic.exceptions.NarratorException;
import game.logic.exceptions.PlayerTargetingException;
import game.logic.exceptions.VotingException;
import game.logic.support.Constants;
import game.logic.templates.BasicRoles;
import models.Command;
import models.enums.GameModifierName;
import models.enums.SetupModifierName;
import services.GameService;
import services.SetupModifierService;

public class TestPluralityVoteSystem extends SuperTest {

    public TestPluralityVoteSystem(String name) {
        super(name);
    }

    public void testVoteCountPersistThroughGameOver() {
        Controller p3 = addPlayer(BasicRoles.Citizen());
        Controller p2 = addPlayer(BasicRoles.Citizen());
        Controller p1 = addPlayer(BasicRoles.Agent());

        dayStart();
        voteOut(p1, p2, p3);
        assertVoteTarget(p1, p2);
        assertVoteTarget(p1, p3);
    }

    public void testAcceptableTargetsSkipper() {
        Controller goon = addPlayer(BasicRoles.Goon());
        addPlayer(BasicRoles.Citizen(), 2);
        SetupModifierService.upsertModifier(setup, SetupModifierName.SKIP_VOTE, true);

        dayStart();

        goon.getPlayer().getAcceptableTargets(Vote.abilityType).contains(game.skipper);
    }

    public void testUnvotingWithJailor() {
        Controller jailor = addPlayer(BasicRoles.Jailor());
        Controller citizen = addPlayer(BasicRoles.Citizen());
        Controller goon = addPlayer(BasicRoles.Goon());

        jail(jailor, citizen);
        vote(jailor, goon);
        skipVote(goon);
        unvote(jailor);
        skipVote(citizen);

        assertIsNight();
        assertStatus(citizen, JailCreate.abilityType);
    }

    public void testVoting() {
        Controller citizen1 = addPlayer(BasicRoles.Citizen());
        Controller citizen2 = addPlayer(BasicRoles.Citizen());
        Controller citizen3 = addPlayer(BasicRoles.Citizen());
        Controller mafia = addPlayer(BasicRoles.Goon());

        editRule(SetupModifierName.SELF_VOTE, false);

        dayStart();

        skipVote(citizen1);
        assertVoteTarget(game.skipper, citizen1);
        unvote(citizen1);
        assertVoteTarget(null, citizen1);

        skipVote(citizen1, citizen2);
        assertVoteTarget(game.skipper, citizen1);
        assertVoteTarget(game.skipper, citizen2);
        skipVote(citizen3);

        setTarget(mafia, citizen3, KILL);

        endNight();

        // dead vote player
        try{
            vote(citizen3, mafia);
            fail();
        }catch(NarratorException e){
        }

        // Controller vote dead
        try{
            vote(mafia, citizen3);
            fail();
        }catch(VotingException | PlayerTargetingException e){
        }

        // dead vote itself
        try{
            vote(citizen3, citizen3);
            fail();
        }catch(VotingException | PlayerTargetingException e){
        }

        // can't vote self
        try{
            vote(citizen2, citizen2);
            fail();
        }catch(VotingException | PlayerTargetingException e){
        }

        try{
            unvote(citizen3);
            fail();
        }catch(NarratorException e){
        }

        // dead vote null
        try{
            vote(citizen3, null);
            fail();
        }catch(NullPointerException | PlayerTargetingException e){
        }

        try{
            vote(mafia, null);
            fail();
        }catch(NullPointerException | PlayerTargetingException e){
        }
    }

    public void testUnvotingCorrectCommand() {
        Controller p1 = addPlayer(BasicRoles.Agent());
        addPlayer(BasicRoles.Armorsmith());
        addPlayer(BasicRoles.Bodyguard());

        dayStart();

        skipVote(p1);
        unvote(p1);

        for(Command command: game.getCommands()){
            if(command.text.contains(Constants.CANCEL))
                return;
        }
        fail("No unvote command");
    }

    public void testSelfVote() {
        Controller p1 = addPlayer(BasicRoles.Citizen());
        Controller p2 = addPlayer(BasicRoles.Agent());
        Controller p3 = addPlayer(BasicRoles.Agent());

        editRule(SetupModifierName.SELF_VOTE, true);

        vote(p1, p1);
        vote(p2, p2);
        vote(p3, p3);
    }

    public void testNoSkipVote() {
        Controller p1 = addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Agent(), 2);

        editRule(SetupModifierName.SKIP_VOTE, false);

        dayStart();

        try{
            skipVote(p1);
            fail();
        }catch(NarratorException e){
        }

    }

    public void testNotSkippedIfOnlyVentsVoting() {
        Controller vent = addPlayer(BasicRoles.Ventriloquist());
        Controller vent2 = addPlayer(BasicRoles.Ventriloquist());
        Controller citizen = addPlayer(BasicRoles.Citizen());
        Controller goon = addPlayer(BasicRoles.Goon());

        editRule(SetupModifierName.SKIP_VOTE, true);

        setTarget(vent, citizen, Ventriloquist.abilityType);
        setTarget(vent2, goon, Ventriloquist.abilityType);
        endNight();

        ventVote(vent, citizen, goon);
        ventVote(vent2, goon, citizen);

        endDay();
        assertIsDay();
    }

    public void testNoSkipVoteForceEndDay() {
        addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Agent(), 2);

        editRule(SetupModifierName.SKIP_VOTE, false);

        dayStart();

        endDay();
        assertIsDay();
    }

    public void testSkipperVoting() {
        Controller p1 = addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Witch());
        addPlayer(BasicRoles.Witch());

        dayStart();

        try{
            skipVote(game.skipper);
            fail();
        }catch(Error e){
        }

        try{
            vote(game.skipper, p1);
            fail();
        }catch(Error e){
        }

        try{
            game.skipper.cancelAction(0, Constants.ALL_TIME_LEFT);
            fail();
        }catch(NarratorException e){
        }
    }

    public void testNoVotingDuringDiscussion() {
        GameService.upsertModifier(game, GameModifierName.DISCUSSION_LENGTH, 1);

        Controller p1 = addPlayer(BasicRoles.Citizen());
        Controller p2 = addPlayer(BasicRoles.Witch());
        addPlayer(BasicRoles.Witch());

        dayStart();

        try{
            vote(p1, p2);
            fail();
        }catch(NarratorException e){
        }

        endPhase();
    }

    public void testTieVoteExtraVote() {
        Controller p1 = addPlayer(BasicRoles.Citizen());
        Controller p2 = addPlayer(BasicRoles.Witch());
        Controller p3 = addPlayer(BasicRoles.Witch());
        Controller p4 = addPlayer(BasicRoles.Citizen());

        vote(p1, p2);
        vote(p2, p1);
        vote(p3, p1);
        vote(p4, p2);

        endDay();

        assertIsDay();
        vote(p4, p1);

        assertIsNight();
    }

    public void testContinuedTieVote() {
        Controller p1 = addPlayer(BasicRoles.Citizen());
        Controller p2 = addPlayer(BasicRoles.Witch());
        Controller p3 = addPlayer(BasicRoles.Citizen());
        Controller p4 = addPlayer(BasicRoles.Witch());

        vote(p1, p2);
        vote(p2, p1);

        endDay();
        assertIsDay();

        vote(p3, p4);
        assertIsDay();

        vote(p3, p1);
        assertIsNight();
    }

    public void testTieVoteUnvote() {
        Controller p1 = addPlayer(BasicRoles.Citizen());
        Controller p2 = addPlayer(BasicRoles.Witch());
        Controller p3 = addPlayer(BasicRoles.Witch());
        Controller p4 = addPlayer(BasicRoles.Citizen());

        vote(p1, p2);
        vote(p2, p1);
        vote(p3, p1);
        vote(p4, p2);

        endDay();

        assertIsDay();
        unvote(p4);

        assertIsNight();
    }

    public void testTieVoteAssassination() {
        Controller p1 = addPlayer(BasicRoles.Citizen());
        Controller p2 = addPlayer(BasicRoles.Witch());
        Controller p3 = addPlayer(BasicRoles.Citizen());
        Controller p4 = addPlayer(BasicRoles.Assassin());
        addPlayer(BasicRoles.Citizen(), 2);

        vote(p1, p2);
        vote(p2, p1);
        vote(p3, p1);
        vote(p4, p2);

        endDay();

        assertIsDay();
        doDayAction(p4, p1);

        assertIsNight();
    }

    public void testTieVoteMayor() {
        Controller p1 = addPlayer(BasicRoles.Mayor());
        Controller p2 = addPlayer(BasicRoles.Witch());
        Controller p3 = addPlayer(BasicRoles.Citizen());
        Controller p4 = addPlayer(BasicRoles.Assassin());

        vote(p1, p2);
        vote(p2, p1);
        vote(p3, p1);
        vote(p4, p2);

        endDay();

        assertIsDay();
        doDayAction(p1);

        assertIsNight();
        isDead(p2);
    }

    public void testTieVoteBurn() {
        Controller p1 = addPlayer(BasicRoles.Citizen());
        Controller p2 = addPlayer(BasicRoles.Witch());
        Controller p3 = addPlayer(BasicRoles.Citizen());
        Controller p4 = addPlayer(BasicRoles.Arsonist());
        addPlayer(BasicRoles.Citizen());

        setTarget(p4, p3, Douse.abilityType);
        endNight();

        vote(p1, p2);
        vote(p2, p1);
        vote(p3, p1);
        vote(p4, p2);

        endDay();

        assertIsDay();
        doDayAction(p4);

        assertIsNight();
    }

    public void testTieVoteElectrocution() {
        Controller p1 = addPlayer(BasicRoles.Citizen());
        Controller p2 = addPlayer(BasicRoles.Witch());
        Controller p3 = addPlayer(BasicRoles.Gunsmith());
        Controller p4 = addPlayer(BasicRoles.ElectroManiac());
        addPlayer(BasicRoles.Goon());

        editRule(SetupModifierName.GS_DAY_GUNS, true);

        setTarget(p4, p1);
        setTarget(p3, p1);
        endNight();

        vote(p1, p2);
        vote(p2, p1);
        vote(p3, p1);
        vote(p4, p2);

        endDay();

        assertIsDay();
        shoot(p1, p4);

        assertIsNight();
        isDead(p2, p1);
        isAlive(p4);
    }

    public void testModKill() {
        Controller soloEvil = addPlayer(BasicRoles.Witch());
        addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Citizen(), 2);

        dayStart();

        soloEvil.getPlayer().modkill(1);
    }
}
