package integration.logic;

import game.logic.Player;
import game.logic.exceptions.NamingException;
import game.logic.exceptions.NarratorException;
import game.logic.support.Constants;
import game.logic.templates.BasicRoles;
import game.setups.Setup;
import models.Faction;
import models.requests.FactionUpdateRequest;
import services.FactionService;
import util.TestUtil;
import util.models.FactionTestUtil;

public class TeamTests extends SuperTest {

    public TeamTests(String name) {
        super(name);
    }

    public void testInvalidTeamColor() {
        tryBadTeamColor(null);
        tryBadTeamColor(Setup.MAFIA_C);
        tryBadTeamColor("#234");
        tryBadTeamColor(Constants.A_RANDOM);
        tryBadTeamColor(Constants.A_SKIP);
        tryGoodTeamColor("#FFFFFE");
        tryGoodTeamColor("#Fd1ddd");
    }

    private static final boolean GOOD = true, BAD = false;

    private void tryBadTeamColor(String color) {
        tryTeamColor(color, BAD);
    }

    private void tryGoodTeamColor(String color) {
        tryTeamColor(color, GOOD);
    }

    private void tryTeamColor(String color, boolean isGoodColor) {
        try{
            FactionTestUtil.createFaction(setup, color, TestUtil.getRandomString(8));
            if(!isGoodColor)
                fail();
            newNarrator();
        }catch(NarratorException e){
            if(isGoodColor)
                fail();
        }
    }

    public void testInvalidTeamName() {
        String name = "Crypt";
        String tName = "TheTeam";
        game.addPlayer(name);
        Faction t = FactionTestUtil.createFaction(setup, "#ff0011", TestUtil.getRandomString(8));

        FactionUpdateRequest request = new FactionUpdateRequest(tName, t.description);
        FactionService.update(t, request);

        Faction t2 = FactionTestUtil.createFaction(setup, "#fe0021", TestUtil.getRandomString(8));
        setNameFail(t2, tName);
        setNameFail(t2, "Randoms");
        setNameFail(t2, "Random");
        setNameFail(t2, "Neutrals");
        setNameFail(t2, "Neutral");
        setNameFail(t2, BasicRoles.Doctor().getName());
    }

    private void setNameFail(Faction faction, String newTeamName) {
        FactionUpdateRequest request = new FactionUpdateRequest(newTeamName, faction.description);
        try{
            FactionService.update(faction, request);
            fail();
        }catch(NamingException e){
        }
    }

    public void testPrefers() {
        Player p = addPlayer("joe");
        Faction faction = setup.getFactionByColor(Setup.TOWN_C);
        p.teamPrefer(faction);
        assertContains(p.tPrefers, faction);
    }

    // not allowing teams to add/remove abilities if game's started
    // check everyone's nightstart, night end, since it depends on who actually did
    // it.

}
