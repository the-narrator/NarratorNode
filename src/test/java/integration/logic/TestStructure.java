package integration.logic;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import game.abilities.Armorsmith;
import game.abilities.Citizen;
import game.abilities.Doctor;
import game.abilities.DrugDealer;
import game.abilities.FactionKill;
import game.abilities.Goon;
import game.abilities.Gunsmith;
import game.abilities.Hidden;
import game.abilities.Jester;
import game.abilities.Punch;
import game.abilities.SerialKiller;
import game.abilities.Sheriff;
import game.abilities.TeamTakedown;
import game.ai.Computer;
import game.ai.Controller;
import game.event.ArchitectChat;
import game.event.Message;
import game.logic.Game;
import game.logic.GameFaction;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.exceptions.IllegalActionException;
import game.logic.exceptions.IllegalGameSettingsException;
import game.logic.exceptions.NamingException;
import game.logic.exceptions.NarratorException;
import game.logic.exceptions.PhaseException;
import game.logic.exceptions.PlayerTargetingException;
import game.logic.exceptions.VotingException;
import game.logic.support.CommandHandler;
import game.logic.support.Constants;
import game.logic.support.StoryPackage;
import game.logic.support.action.Action;
import game.logic.templates.BasicRoles;
import game.logic.templates.NativeStoryPackage;
import game.setups.Setup;
import junit.framework.TestCase;
import models.Command;
import models.Faction;
import models.Role;
import models.SetupHidden;
import models.enums.AbilityModifierName;
import models.enums.FactionModifierName;
import models.enums.GameModifierName;
import models.enums.RoleModifierName;
import models.enums.SetupModifierName;
import models.idtypes.PlayerDBID;
import services.FactionRoleModifierService;
import services.FactionRoleService;
import services.GameService;
import services.RoleService;
import services.SetupHiddenService;
import util.RepeatabilityTest;
import util.models.SetupHiddenTestUtil;

public class TestStructure extends SuperTest {

    public TestStructure(String name) {
        super(name);
    }

    public void testPhaseEndCommand() {
        Controller p1 = addPlayer(BasicRoles.Citizen());
        Controller p3 = addPlayer(BasicRoles.Detective());
        addPlayer(BasicRoles.Chauffeur());
        addPlayer(BasicRoles.SerialKiller());

        dayStart();

        vote(p1, p3);

        command(game.skipper, CommandHandler.END_PHASE);

        assertIsNight();
        isDead(p3);

        command(game.skipper, CommandHandler.END_PHASE);

        assertIsDay();
    }

    // initial setup
    public void testMinimumRoleSize() {
        for(int i = 0; i < 3; i++)
            addPlayer(Computer.toLetter(i + 1));

        assertEquals(3, game.players.size());

        addRole(BasicRoles.Citizen());

        // tests
        assertBadGameSettings();

        addRole(BasicRoles.Citizen());
        addRole(BasicRoles.Goon());

        assertEquals(3, game.setup.rolesList.size(game));
        nightStart();
    }

    public void testTargetDead() {
        // people shouldn't be able to target dead people
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller cit2 = addPlayer(BasicRoles.Citizen());
        Controller cit3 = addPlayer(BasicRoles.Citizen());
        Controller sk = addPlayer(BasicRoles.SerialKiller());

        vote(cit3, cit);
        vote(cit2, cit);
        vote(cit, sk);
        vote(sk, cit);

        try{
            setTarget(sk, cit);
            fail();
        }catch(PlayerTargetingException e){
        }
    }

    public void testSameCharges() {
        Controller veteran = addPlayer(BasicRoles.Veteran());
        Controller spy = addPlayer(BasicRoles.Spy());
        Controller tailor = addPlayer(BasicRoles.Tailor());
        Controller jailor = addPlayer(BasicRoles.Jailor());

        game.setSeed(1);
        nightStart();

        newNarrator();

        Controller veteran2 = addPlayer(BasicRoles.Veteran());
        Controller spy2 = addPlayer(BasicRoles.Spy());
        Controller tailor2 = addPlayer(BasicRoles.Tailor());
        Controller jailor2 = addPlayer(BasicRoles.Jailor());

        game.setSeed(1);
        nightStart();

        chargeEquality(veteran, veteran2);
        chargeEquality(spy, spy2);
        chargeEquality(tailor, tailor2);
        chargeEquality(jailor, jailor2);

        SuperTest.BrainEndGame = false;
    }

    // guaranteeing its only the single role
    private void chargeEquality(Controller p1, Controller p2) {
        assertEquals(p1.getPlayer().getRoleAbilities().get(0).getPerceivedCharges(),
                p2.getPlayer().getRoleAbilities().get(0).getPerceivedCharges());
    }

    public void testLotOfFeedback() {

        Controller bd = addPlayer(BasicRoles.BusDriver());
        Controller agent = addPlayer(BasicRoles.Lookout());
        Controller cons = addPlayer(BasicRoles.Consort());
        Controller cont = addPlayer(BasicRoles.Consort());
        Controller witch = addPlayer(BasicRoles.Witch());

        nightStart();

        setTarget(agent, bd);
        setTarget(cont, agent);
        setTarget(cons, agent);
        witch(witch, agent, agent);

        endNight();
    }

    public void testDoubleFeedbackOld() {

        addPlayer(BasicRoles.Citizen());
        Controller cit2 = addPlayer(BasicRoles.Citizen());
        Controller doc = addPlayer(BasicRoles.Doctor());
        Controller vig = addPlayer(BasicRoles.Vigilante());

        Controller sk = addPlayer(BasicRoles.SerialKiller());

        nightStart();

        setTarget(sk, cit2);
        setTarget(vig, cit2, GUN);
        setTarget(doc, cit2);

        endNight();

        isDead(cit2);

    }

    // makes sure that all setups have an opponent
    public void testValidOpponentCheck() {
        addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Citizen());

        assertBadGameSettings();
    }

    public void testValidOpponentCheck2() {
        addPlayer(Hidden.AnyRandom());
        addPlayer(Hidden.MafiaRandom());
        addPlayer(Hidden.TownRandom());
        addPlayer(Hidden.NeutralRandom());

        nightStart();
    }

    // tests to see if there are enough players
    public void testPlayerSizeCheck() {
        addPlayer("Test1");
        addPlayer("Test2");

        addRole(BasicRoles.Sheriff());
        addRole(BasicRoles.Sheriff());
        addRole(BasicRoles.Goon());

        try{
            dayStart();
            fail();
        }catch(IllegalGameSettingsException e){
        }

    }

    // regular game
    public void testGameStructure() {
        Controller citizen = addPlayer(BasicRoles.Citizen());
        Controller doctor = addPlayer(BasicRoles.Doctor());
        Controller sheriff = addPlayer(BasicRoles.Sheriff());
        Controller mafia1 = addPlayer(BasicRoles.Goon());
        Controller mafia2 = addPlayer(BasicRoles.Agent());
        Controller serialKiller = addPlayer(BasicRoles.SerialKiller());

        editRule(SetupModifierName.HEAL_SUCCESS_FEEDBACK, true);
        editRule(SetupModifierName.HEAL_FEEDBACK, true);

        nightStart();

        assertEquals(0, game.getDayNumber());

        assertInProgress();

        setTarget(serialKiller, mafia1);
        setTarget(doctor, mafia1);

        setTarget(sheriff, citizen);

        setTarget(mafia2, citizen, KILL);
        setTarget(mafia1, mafia2, SEND);
        setTarget(mafia2, mafia2, SEND);

        endNight();

        isAlive(mafia1);
        isAlive(mafia2);
        isAlive(sheriff);
        isAlive(serialKiller);
        isAlive(doctor);

        isDead(citizen);

        partialContains(citizen, Goon.ANONYMOUS_DEATH_FEEDBACK);
        partialContains(doctor, Doctor.SUCCESFULL_HEAL);
        partialContains(sheriff, Sheriff.NOT_SUSPICIOUS);
        partialContains(mafia1, Doctor.TARGET_FEEDBACK);
        // assertEquals(0, serialKiller.getNightFeedback().size());
        // assertEquals(0, mafia2.getNightFeedback().size());

        assertInProgress();
        assertEquals(1, game.getDayNumber());

        assertEquals(1, game.getDeadSize());
        assertEquals(citizen, (game.getDeadList(0)).get(0));

        assertIsDay();

        // dead person voting
        try{
            vote(citizen, sheriff);
            fail();
        }catch(NarratorException e){
        }

        // person voting dead person
        try{
            vote(sheriff, citizen);
            fail();
        }catch(VotingException | PlayerTargetingException e){
        }

        vote(sheriff, mafia2);

        assertVoteTarget(mafia2, sheriff);

        unvote(sheriff);
        vote(sheriff, mafia2);
        skipVote(sheriff);
        vote(serialKiller, mafia2);

        skipDay();

        /*
         * sheriff doctor serial killer mafia1 mafia2
         */

        setTarget(sheriff, serialKiller);
        setTarget(doctor, sheriff);
        setTarget(mafia1, serialKiller, KILL);
        setTarget(mafia1, mafia1, SEND);
        setTarget(mafia2, mafia1, SEND);
        setTarget(serialKiller, sheriff);

        endNight();

        isAlive(serialKiller);
        assertEquals(1, game.getDeadSize());

        voteOut(mafia2, sheriff, mafia1, doctor);

        try{
            vote(serialKiller, mafia2);
            fail();
        }catch(NarratorException e){
        }

        isDead(mafia2);

        /*
         * sheriff doctor serial killer mafia1
         */

        setTarget(doctor, mafia1);
        setTarget(serialKiller, doctor);
        setTarget(mafia1, sheriff, KILL);

        endNight();

        isWinner(serialKiller);
    }

    public void testDeathDay() {
        Controller A = addPlayer(BasicRoles.Doctor());
        Controller C = addPlayer(BasicRoles.Sheriff());
        Controller E = addPlayer(BasicRoles.Escort());
        Controller G = addPlayer(BasicRoles.Citizen());
        Controller H = addPlayer(BasicRoles.Sheriff());
        Controller I = addPlayer(BasicRoles.Sheriff());
        Controller K = addPlayer(BasicRoles.Sheriff());
        Controller voss = addPlayer(BasicRoles.Mayor());
        Controller P = addPlayer(BasicRoles.Goon());
        Controller Q = addPlayer(BasicRoles.Blackmailer());
        Controller R = addPlayer(BasicRoles.Jester());
        Controller T = addPlayer(BasicRoles.Jester());
        Controller S = addPlayer(BasicRoles.SerialKiller());
        addPlayer(BasicRoles.Lookout());
        addPlayer(BasicRoles.Sheriff());
        addPlayer(BasicRoles.Doctor());
        addPlayer(BasicRoles.Escort());
        addPlayer(BasicRoles.Veteran());
        addPlayer(BasicRoles.Chauffeur());
        addPlayer(BasicRoles.Goon());

        dayStart();

        PlayerList players = game.getAllPlayers();

        voteOut(I, R, A, C, E, G, H, K, P, Q, T, voss);

        players.sortByDeath();
        assertTrue(players.get(19) == I);

        isDead(I);
        assertEqual(I.getPlayer().getDeathDay(), 1);
        assertTrue(I.getPlayer().getDeathType().isLynch());

        setTarget(S, A);

        endNight();

        isDead(A);
        assertEqual(1, A.getPlayer().getDeathDay());

        players.sortByDeath();
        assertTrue(players.get(18) == I);
        assertTrue(players.get(19) == A);

    }

    public void testModKill() {
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller cit2 = addPlayer(BasicRoles.Citizen());
        Controller cit3 = addPlayer(BasicRoles.Citizen());
        Controller cit4 = addPlayer(BasicRoles.Citizen());
        Controller cit5 = addPlayer(BasicRoles.Citizen());

        Controller maf = addPlayer(BasicRoles.Goon());

        dayStart();

        vote(cit, cit2);

        cit.getPlayer().modkill(1);

        skipDay();

        endNight(cit2, cit3, maf, cit5);

        command(cit4, CommandHandler.MODKILL);

        assertIsDay();

        vote(cit2, maf);
        vote(cit5, maf);
        vote(cit3, cit2);
        vote(maf, cit2);

        cit3.getPlayer().modkill(1);

        assertGameOver(); // in favor of town
    }

    public void testHiddenFlipModKill() {
        Controller cit = addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Goon());
        FactionRoleModifierService.upsertModifier(BasicRoles.Citizen(), RoleModifierName.HIDDEN_FLIP, true);

        dayStart();
        Player player = cit.getPlayer();
        player.modkill(1);

        assertTrue(player.getDeathType().isHidden());
    }

    public void testChangeRole() {
        Controller doc1 = addPlayer(BasicRoles.Doctor());
        Controller doc2 = addPlayer(BasicRoles.Doctor());
        Controller sk = addPlayer(BasicRoles.SerialKiller());
        addPlayer(BasicRoles.Goon());
        addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Sheriff());

        nightStart();

        game.changeRole(doc1.getPlayer(), BasicRoles.Citizen(), Constants.ALL_TIME_LEFT);
        Controller cit = doc1;
        try{
            setTarget(cit, doc2);
            fail();
        }catch(NarratorException e){
        }

        game.changeRole(cit.getPlayer(), BasicRoles.Sheriff(), Constants.ALL_TIME_LEFT);
        Controller sheriff = cit;
        assertTrue(sheriff.is(Sheriff.abilityType));

        setTarget(sheriff, sk);
        assertTrue(sk.getPlayer().in(sheriff.getPlayer().getTargets(Sheriff.abilityType)));
        assertTrue(sk.is(SerialKiller.abilityType));

        endNight();
        assertTrue(doc1.getPlayer().initialAssignedRole.assignedRole.role.hasAbility(Doctor.abilityType));
        assertFalse(Citizen.hasNoAbilities(doc1.getPlayer()));

        assertFalse(Citizen.hasNoAbilities(cit.getPlayer()));
        assertFalse(cit.getPlayer().initialAssignedRole.assignedRole.role.hasAbility(Sheriff.abilityType));

    }

    /*
     * tests killing during the day and night
     */

    public void testEndGameText() {
        Controller cit2 = addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Citizen());
        Controller sk = addPlayer(BasicRoles.SerialKiller());

        setTarget(sk, cit2);

        endNight();

        assertFalse(game.getHappenings().contains("Night 1"));
        assertEquals(1, game.getDayNumber());
        assertTrue(game.getHappenings().contains("Day 1"));
    }

    public void testSingleNightAction() {
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller cit2 = addPlayer(BasicRoles.Citizen());
        Controller cit3 = addPlayer(BasicRoles.Citizen());
        Controller sk = addPlayer(BasicRoles.SerialKiller());

        setTarget(sk, cit);
        endNight();

        isDead(cit);
        skipVote(cit2, cit3);

        try{
            setTarget(cit, game.skipper);
            fail();
        }catch(PlayerTargetingException e){
        }

        assertTrue(game.getDeadList(game.getDayNumber() - 1).contains(cit.getPlayer()));
    }

    public void testCitizen() {
        Controller citizen1 = addPlayer(BasicRoles.Citizen());
        Controller citizen2 = addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Goon());

        nightStart();

        try{
            setTarget(citizen1, citizen2);
            fail();
        }catch(NarratorException e){
        }
        try{
            setTarget(citizen1, game.skipper);
        }catch(NarratorException e){
        }
    }

    public void testHappenings() {
        Controller bd = addPlayer(BasicRoles.BusDriver());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller detect = addPlayer(BasicRoles.Detective());
        Controller doc = addPlayer(BasicRoles.Doctor());
        Controller doc2 = addPlayer(BasicRoles.Doctor());
        Controller lookout = addPlayer(BasicRoles.Lookout());
        Controller mayor = addPlayer(BasicRoles.Mayor());
        Controller sher = addPlayer(BasicRoles.Sheriff());
        Controller vig = addPlayer(BasicRoles.Vigilante());
        Controller vig2 = addPlayer(BasicRoles.Vigilante());

        Controller agent = addPlayer(BasicRoles.Agent());
        Controller bm = addPlayer(BasicRoles.Blackmailer());
        Controller chf = addPlayer(BasicRoles.Chauffeur());
        Controller framer = addPlayer(BasicRoles.Framer());
        Controller jan = addPlayer(BasicRoles.Janitor()); // 20
        Controller maf = addPlayer(BasicRoles.Goon());

        Controller exec = addPlayer(BasicRoles.Executioner());
        Controller jest = addPlayer(BasicRoles.Jester());
        Controller witch = addPlayer(BasicRoles.Witch());
        Controller sk = addPlayer(BasicRoles.SerialKiller());

        modifyRole(BasicRoles.Vigilante(), AbilityModifierName.CHARGES, Constants.UNLIMITED);

        dayStart();

        reveal(mayor);
        skipDay();

        frame(framer, mayor, framer.getColor());
        drive(bd, exec, jest);
        shoot(vig, witch);
        witch(witch, vig, vig2);
        shoot(vig2, vig);
        drive(chf, bd, vig);
        setTarget(sher, mayor);

        endNight();

        assertIsDay();

        voteOut(mayor, lookout, detect, doc, doc2, vig, sk, exec, jest, maf, witch);

        assertIsNight();

        // packageTest();
        setTarget(detect, lookout);
        setTarget(lookout, doc);
        setTarget(agent, cit);
        mafKill(maf, jest);
        setTarget(bm, cit);
        setTarget(sher, maf);
        setTarget(doc, jest);
        setTarget(jan, exec);
        setTarget(sk, doc);
        setTarget(doc2, doc);
        shoot(vig, framer);

        endNight();

        // packageTest();

        vote(sk, exec);
        voteOut(cit, sher, doc, doc2, exec, bm, jan, detect, chf, witch, vig);

        // cit lynched

        setTarget(lookout, detect);
        setTarget(detect, lookout);
        setTarget(sk, doc);
        mafKill(maf, jan);
        // jan and doc die

        endNight();

        skipDay();
        setTarget(sk, detect);
        mafKill(bm, lookout);
        send(agent, bm);
        // lookout and detective die here

        endNight();

        voteOut(sk, jest, exec, maf, agent, doc2, witch, chf);

        // sk lynched

        send(maf, bm);
        mafKill(bm, bm);
        // bm dead

        endNight();

        skipDay();
        endNight();

        voteOut(sher, jest, exec, doc2, agent, witch, chf);
        // sher lynched

        mafKill(agent, agent);

        mafKill(agent, agent);

        endNight();

        voteOut(chf, exec, doc2, vig, witch);

        shoot(vig, witch);
        // vig.setTarget(witch);
        mafKill(maf, vig);

        endNight();
        assertEquals(1, maf.getPlayer().getGameFaction().size());
        isDead(vig);
        isDead(witch);

        voteOut(jest, exec, doc2, maf);

        endNight();

        assertFalse(game.isInProgress());

        // .packageTest();

        // System.out.println(n.getPrivateEvents(false));
    }

    public void testPackaging() {
        modifyRole(BasicRoles.Vigilante(), AbilityModifierName.CHARGES, Constants.UNLIMITED);

        Controller bd = addPlayer(BasicRoles.BusDriver());
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller detect = addPlayer(BasicRoles.Detective());
        Controller doc = addPlayer(BasicRoles.Doctor());
        Controller doc2 = addPlayer(BasicRoles.Doctor());
        Controller lookout = addPlayer(BasicRoles.Lookout());
        Controller mayor = addPlayer(BasicRoles.Mayor());// 6
        Controller sher = addPlayer(BasicRoles.Sheriff());
        Controller vig = addPlayer(BasicRoles.Vigilante());// 8

        Controller agent = addPlayer(BasicRoles.Agent());
        Controller bm = addPlayer(BasicRoles.Blackmailer());
        Controller chf = addPlayer(BasicRoles.Chauffeur());
        Controller framer = addPlayer(BasicRoles.Framer());
        Controller jan = addPlayer(BasicRoles.Janitor());// 20
        Controller maf = addPlayer(BasicRoles.Goon());

        Controller exec = addPlayer(BasicRoles.Executioner());// 15
        Controller jest = addPlayer(BasicRoles.Jester()); // 16
        Controller witch = addPlayer(BasicRoles.Witch()); // 17
        Controller sk = addPlayer(BasicRoles.SerialKiller());

        game.setSeed(random.nextLong());

        dayStart();

        // packageTest();

        reveal(mayor);
        skipDay();

        // packageTest();

        frame(framer, mayor, framer.getColor());
        drive(bd, exec, jest);
        shoot(vig, witch);
        witch(witch, vig, vig);
        drive(chf, bd, vig);
        setTarget(sher, mayor);

        endNight();
        voteOut(mayor, lookout, detect, doc, doc2, vig, sk, exec, jest, maf, witch);

        setTarget(detect, lookout);
        setTarget(lookout, doc);
        setTarget(agent, cit);
        mafKill(maf, jest);
        setTarget(bm, cit);
        setTarget(sher, maf);
        setTarget(doc, jest);
        setTarget(jan, exec);
        setTarget(sk, doc);
        setTarget(doc2, doc);
        shoot(vig, framer);

        endNight();

        vote(sk, exec);
        voteOut(cit, sher, doc, doc2, exec, bm, jan, detect, chf, witch, vig);

        // cit lynched

        setTarget(lookout, detect);
        setTarget(detect, lookout);
        setTarget(sk, doc);
        mafKill(maf, jan);
        // jan and doc die

        endNight();

        skipDay();
        setTarget(sk, detect);
        setTarget(bm, lookout, KILL);
        setTarget(agent, bm, SEND);
        // lookout and detective die here

        endNight();

        voteOut(sk, jest, exec, maf, agent, doc2, witch, chf);

        // sk lynched

        send(maf, bm);
        mafKill(bm, bm);
        // bm dead

        endNight();

        skipDay();
        endNight();

        voteOut(sher, jest, exec, doc2, agent, witch, chf);
        // sher lynched

        // makes sure that when the mafia has no target, no null pointer is thrown
        mafKill(agent, agent);

        endNight();

        voteOut(chf, exec, doc2, vig, witch);

        shoot(vig, witch);
        mafKill(maf, vig);

        endNight();
        assertEquals(1, game.getFaction(maf.getColor()).getMembers().size());
        isDead(vig);
        isDead(witch);

        voteOut(jest, exec, doc2, maf);

        endNight();

        assertGameOver();
    }

    public void testEndNightBarring() {
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller sk = addPlayer(BasicRoles.SerialKiller());
        Controller witch = addPlayer(BasicRoles.Witch());

        nightStart();

        assertTrue(game.isFirstNight());

        endNight(witch, sk);
        try{
            setTarget(sk, cit);
            fail();
        }catch(PlayerTargetingException e){
        }

        try{
            witch(witch, cit, cit);
            fail();
        }catch(PlayerTargetingException e){
        }

        nextNight();
        assertFalse(game.isFirstNight());
    }

    public void testCommandRecording() {
        Controller p1 = addPlayer(BasicRoles.Citizen());
        Controller p2 = addPlayer(BasicRoles.Witch());
        Controller p3 = addPlayer(BasicRoles.Jester());

        dayStart();

        vote(p1, p2);
        say(p3, "why thoe votes yo?", Constants.DAY_CHAT);

        List<String> commands = Command.getText(game.getCommands());
        assertEquals(2, commands.size());
        assertTrue(commands.get(1).toLowerCase().contains("vote"));
        assertTrue(commands.get(1).contains("say"));
    }

    public void testCommandsParsing() {
        SuperTest.checkEnclosingChats = false;
        long seed = random.nextLong();
//        seed = Long.parseLong("4453197250242653519");

        System.out.println("Text: " + seed);
        setupNarratorForTexting(seed);

        dayStart();
        simulationTest(seed);

        ArrayList<Command> commands = game.getCommands();
        Game n_old = game;
        setupNarratorForTexting(seed);
        StoryPackage sp = NativeStoryPackage.deserialize(n_old);
        game = sp.narrator;

        GameService.internalStart(game, StoryPackage.GetRoleAssigner(sp));

        assertTrue(game.isStarted());

        Controller newP;
        for(Player oldP: n_old.players){
            newP = game.getPlayerByID(oldP.getID());
            assertEquals(oldP.getInitialColor(), newP.getPlayer().getInitialColor());
            RepeatabilityTest.assertSameInitialAbilities(oldP, newP.getPlayer());
        }

        for(Player player: game.players)
            player.databaseID = new PlayerDBID(Math.abs(player.getID().hashCode()));

        Map<PlayerDBID, Player> playerDBMap = game.players.getDatabaseMap();
        Controller owner;
        CommandHandler th = new CommandHandler(game);
        Player submitter;
        try{
            for(Command command: commands){
                if(command.text.length() == 0)
                    continue;
                if(command.playerID.isPresent()){
                    owner = playerDBMap.get(command.playerID.get());
                    if(owner != null)
                        submitter = owner.getPlayer();
                    else
                        submitter = null;
                }else{
                    submitter = null;
                }
                th.command(submitter, command.text, "");
            }
        }catch(IllegalActionException | VotingException | PlayerTargetingException | PhaseException
                | NullPointerException e){
            e.printStackTrace();
//            writeToFile("log1.txt", n_old);
//            writeToFile("log2.txt", narrator);
            TestCase.fail(Long.toString(seed));
            return;
        }

        assertTrue(game.getDayNumber() == n_old.getDayNumber());
        assertEquals(game.getWinMessage().access(Message.PRIVATE), n_old.getWinMessage().access(Message.PRIVATE));
        Controller q;
        for(Controller p: game.players){
            q = n_old.getPlayerByID(p.getPlayer().getID());
            assertEquals(p.getPlayer().isAlive(), q.getPlayer().isAlive());
            assertEquals(p.getPlayer().isWinner(), q.getPlayer().isWinner());
        }

        SuperTest.skipTearDown = true;
    }

    private void setupNarratorForTexting(long seed) {
        newNarrator();
        game.setSeed(seed);

        addPlayer(BasicRoles.Framer(), 8);
        addPlayer(Hidden.MafiaRandom(), 4);
        addPlayer(Hidden.YakuzaRandom(), 4);
        addPlayer(Hidden.NeutralRandom(), 2);
    }

    public void testHeaders() {
        Controller p1 = addPlayer(BasicRoles.Agent());
        addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Citizen());

        nightStart();

        nextNight();
        String happenings, oldHappenings;
        assertTrue(game.getHappenings().contains("Night 0"));
        assertTrue(game.getHappenings().contains("Night 1"));

        oldHappenings = game.getHappenings();
        endNight(p1);

        happenings = game.getHappenings();
        String happeningsRemainder = happenings.replace(oldHappenings, "");
        assertFalse(happeningsRemainder.contains("Night 1"));

        nextNight();
        assertTrue(game.getHappenings().contains("Night 0"));
        assertTrue(game.getHappenings().contains("Night 1"));
        assertTrue(game.getHappenings().contains("Night 2"));
    }

    public void testActionStackClearedOnGameOver() {
        Controller cit = addPlayer(BasicRoles.Citizen());
        Controller maf = addPlayer(BasicRoles.DrugDealer());
        addPlayer(BasicRoles.Witch());

        mafKill(maf, cit);
        endNight();

        // if i ever were to change this, i need to account for things like amnesiac
        // turning into something on game ending.
        // or else parsing this in the 'state object' will throw errors
        assertActionSize(0, maf);
    }

    public void testLetterer() {
        test("A", 1);
        test("Z", 26);
        test("AA", 27);
        test("AZ", 27 + 25);
    }

    private void test(String letter, int i) {
        assertEquals(letter, Computer.toLetter(i));
    }

    public void testDuplicateName() {
        Controller bob = addPlayer("Bob");
        addPlayer("Joe");

        try{
            addPlayer("Bob");
            fail();
        }catch(NamingException e){
        }

        try{
            bob.setName("joe");
            fail();
        }catch(NamingException e){
        }
    }

    public void testReplaceAction() {
        Controller gunsmith = addPlayer(BasicRoles.Gunsmith());
        Controller mafioso = addPlayer(BasicRoles.Goon());
        Controller cit = addPlayer(BasicRoles.Citizen());

        nightStart();
        setTarget(gunsmith, cit);
        Action a = gunsmith.getPlayer().getAction(Gunsmith.abilityType);
        Action newAction = new Action(gunsmith.getPlayer(), Gunsmith.abilityType, mafioso.getPlayer());
        gunsmith.getPlayer().getActions().replace(a, newAction);

        endNight();
        assertTotalGunCount(1, mafioso);
        Command newCommand = new Command(gunsmith.getPlayer(), Gunsmith.COMMAND + " " + mafioso.getName(),
                Constants.ALL_TIME_LEFT);
        List<Command> commands = game.getCommands();
        assertTrue(commands.get(1).equals(newCommand));
        assertTrue(commands.contains(newCommand));
    }

    public void testSheriffCleanup2() {
        addPlayer(BasicRoles.Sheriff());
        addPlayer(BasicRoles.Godfather());
        addPlayer(BasicRoles.Citizen());

        dayStart();

        for(GameFaction gameFaction: game.getFactions()){
            if(gameFaction.getColor().equals(BasicRoles.Sheriff().getColor()))
                assertFalse(gameFaction.faction.sheriffCheckables.isEmpty());
            else
                assertTrue(gameFaction.faction.sheriffCheckables.isEmpty());
        }
    }

    // testName
    // badname
    public void testReservedNames() {
        String nameSplit = ":\t";

        tryName(Armorsmith.FAKE);
        tryName(Gunsmith.REAL);
        tryName(DrugDealer.BLOCKED);
        tryName(Punch.COMMAND);
        tryName(Setup.TOWN_C);
        tryName(nameSplit);
        tryName("Doug" + nameSplit);
        tryName("Doug" + nameSplit + "Bow");
        tryName("");
        tryName(null);
        tryName("boy.ohboy");
        tryName("boy---ohboy");
        tryName("boy$ohboy");
        tryName("doug" + ArchitectChat.KEY_SEPERATOR + "george");
        tryName("1");
        Set<Faction> teams = setup.getFactions();
        for(Faction t: teams){
            tryName(t.getName());
            tryName(t.getColor());
        }
    }

    public void testNightless() {
        GameService.upsertModifier(game, GameModifierName.NIGHT_LENGTH, 0);
        assertEqual(0, game.getInt(GameModifierName.NIGHT_LENGTH));

        Role role = RoleService.createRole(setup, "TakedownJester", Jester.abilityType, TeamTakedown.abilityType);
        Faction faction = setup.getFactionByColor(Setup.BENIGN_C);

        addPlayer(BasicRoles.Goon());
        addPlayer(BasicRoles.Citizen());
        addPlayer(BasicRoles.Mayor());
        addPlayer(BasicRoles.Assassin());
        addPlayer(BasicRoles.Bomb());
        addPlayer(BasicRoles.Jester());
        addPlayer(BasicRoles.Executioner());
        addPlayer(BasicRoles.Bulletproof());
        addPlayer(BasicRoles.Mason());
        addPlayer(BasicRoles.Mason());
        addPlayer(FactionRoleService.createFactionRole(faction, role));

        editRule(SetupModifierName.JESTER_CAN_ANNOY, false);
        editRule(SetupModifierName.JESTER_KILLS, 1);
        editRule(SetupModifierName.DAY_START, false);

        assertBadGameSettings();

        editRule(SetupModifierName.DAY_START, true);

        assertBadGameSettings();

        setTeamRule(Setup.MAFIA_C, FactionModifierName.HAS_NIGHT_CHAT, false);

        assertBadGameSettings();

        removeTeamAbility(Setup.MAFIA_C, FactionKill.abilityType);
        assertFalse(setup.getFactionByColor(Setup.MAFIA_C).hasSharedAbilities());

        editRule(SetupModifierName.JESTER_CAN_ANNOY, true);

        assertBadGameSettings();

        editRule(SetupModifierName.JESTER_CAN_ANNOY, false);

        addPlayer(BasicRoles.Sheriff());
        editRule(SetupModifierName.SHERIFF_PREPEEK, false);

        assertBadGameSettings();

        editRule(SetupModifierName.SHERIFF_PREPEEK, true);

        Controller p = addPlayer(BasicRoles.Spy());

        assertBadGameSettings();

        SetupHidden setupHidden = SetupHiddenTestUtil.findSetupHidden(setup, "Spy");
        SetupHiddenService.deleteSetupHidden(game, setupHidden);
        addRole(BasicRoles.Citizen());

        dayStart();

        try{
            skipVote(p);
            fail();
        }catch(NarratorException e){
        }

        for(Controller x: game.players){
            if(x == p)
                continue;
            vote(x, p);
            if(p.getPlayer().isDead())
                break;
        }

        assertIsDay();
    }

    public void testEndGameNightless() {
        Controller p1 = addPlayer(BasicRoles.Citizen());
        Controller p2 = addPlayer(BasicRoles.Citizen());
        Controller p3 = addPlayer(BasicRoles.Goon());

        removeTeamAbility(Setup.MAFIA_C, FactionKill.abilityType);
        assertFalse(setup.getFactionByColor(Setup.MAFIA_C).hasSharedAbilities());
        GameService.upsertModifier(game, GameModifierName.NIGHT_LENGTH, 0);

        dayStart();

        vote(p1, p3);
        vote(p2, p3);

        assertGameOver();
    }

    public void testNameTargeting() {
        Controller sk = addPlayer(BasicRoles.SerialKiller());
        addPlayer(BasicRoles.Witch());
        addPlayer(BasicRoles.Vigilante());
        addPlayer(BasicRoles.Vigilante());

        nightStart();

        command(sk, SerialKiller.COMMAND + " 3");
        endNight();

        assertEquals(1, game.getDeadSize());
    }

    public void testCapitalization() {
        Controller a = addPlayer(BasicRoles.Citizen()).setName("a");
        Controller b = addPlayer(BasicRoles.Citizen()).setName("B");
        Controller c = addPlayer(BasicRoles.Goon()).setName("c");
        Controller d = addPlayer(BasicRoles.Godfather()).setName("D");

        nightStart();

        assertTrue(a == game.getPlayerByName("1"));
        assertTrue(b == game.getPlayerByName("2"));
        assertTrue(c == game.getPlayerByName("3"));
        assertTrue(d == game.getPlayerByName("4"));
    }

    private void tryName(String name) {
        try{
            game.addPlayer(name);
            fail();
        }catch(NamingException e){
        }
    }
}
