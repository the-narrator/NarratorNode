package util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import game.event.Feedback;
import game.event.Message;
import game.logic.Player;
import game.logic.exceptions.NarratorException;
import json.JSONArray;
import json.JSONException;
import json.JSONObject;

public class TestChatUtil {

    private static HashMap<Player, List<Message>> playerMessageMap = new HashMap<>();
    private static HashMap<Player, List<Message>> playerWarningMap = new HashMap<>();

    public static void addPlayer(Player player) {
        playerMessageMap.put(player, new ArrayList<>());
        playerWarningMap.put(player, new ArrayList<>());
    }

    public static List<Message> filterByText(List<Message> messages, Player chatMessageViewer, String feedback) {
        List<Message> newMessages = new LinkedList<>();
        for(Message s: messages){
            if(s.access(chatMessageViewer).contains(feedback))
                newMessages.add(s);
        }
        return newMessages;
    }

    public static List<Feedback> getFeedbackMessages(Player player, int dayNumber) {
        List<Feedback> feedbackMessages = new LinkedList<>();
        Feedback feedback;
        for(Message message: getMessages(player)){
            if(!(message instanceof Feedback))
                continue;
            feedback = (Feedback) message;
            if(feedback.getDay() - 1 == dayNumber)
                feedbackMessages.add(feedback);
        }
        return feedbackMessages;
    }

    public static Collection<Player> getPlayers() {
        return playerMessageMap.keySet();
    }

    public static List<Message> getMessages(Player player) {
        return playerMessageMap.get(player);
    }

    public static List<Message> getWarnings(Player player) {
        return playerWarningMap.get(player);
    }

    public static void onMessageReceive(Player player, Message message) {
        playerMessageMap.get(player).add(message);
    }

    public static void onWarningReceive(Player player, Message warning) {
        playerWarningMap.get(player).add(warning);
    }

    public static void reset() {
        playerMessageMap.clear();
        playerWarningMap.clear();
    }

    public static void resetWarnings() {
        for(Player player: playerWarningMap.keySet())
            resetWarnings(player);
    }

    public static void resetWarnings(Player player) {
        playerWarningMap.get(player).clear();
    }

    public static void assertMessageTextInChatMessages(JSONObject serverResponse) throws JSONException {
        JSONObject response = serverResponse.getJSONObject("response");
        JSONArray messages = response.getJSONArray("message");

        JSONObject jChatMessage;
        String messageType, messageText;
        for(int i = 0; i < messages.length(); ++i){
            jChatMessage = messages.getJSONObject(i);
            messageType = jChatMessage.getString("messageType");
            messageText = jChatMessage.getString("text");
            if(messageText.equals(FakeConstants.MESSAGE_TEXT) && messageType.equals("ChatMessage"))
                return;
        }
        throw new NarratorException("Message not found");
    }
}
