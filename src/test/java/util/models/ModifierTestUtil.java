package util.models;

import game.logic.support.rules.ModifierName;
import models.modifiers.Modifier;
import models.modifiers.Modifiers;

public class ModifierTestUtil {
    public static <T extends ModifierName> int getValue(Modifiers<T> modifiers, T modifierName) {
        Integer seenValue = null;
        for(Modifier<T> modifier: modifiers){
            if(modifier.name != modifierName)
                continue;
            if(seenValue != null)
                throw new Error("Duplicate modifier");
            seenValue = modifier.value.getIntValue();
        }

        if(seenValue == null)
            throw new Error("No value found.");
        return seenValue;
    }
}
