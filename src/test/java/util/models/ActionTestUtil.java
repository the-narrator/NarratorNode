package util.models;

import java.util.List;

import game.abilities.GameAbility;
import game.abilities.GraveDigger;
import game.abilities.util.AbilityUtil;
import game.ai.Controller;
import game.logic.Game;
import game.logic.PlayerList;
import game.logic.support.action.Action;
import models.modifiers.Modifiers;
import util.Util;

public class ActionTestUtil {

    public static Action getGraveDiggerAction(Controller graveDigger, Action action) {
        Game game = action.owner.game;
        GameAbility ability = AbilityUtil.CREATOR(action.abilityType, game, new Modifiers<>());
        List<String> args = Util.toStringList(ability.getCommand());
        args.addAll(action.args);

        PlayerList targets = action.getTargets();
        targets.add(0, action.owner);

        return new Action(graveDigger.getPlayer(), GraveDigger.abilityType, args, targets);
    }

}
