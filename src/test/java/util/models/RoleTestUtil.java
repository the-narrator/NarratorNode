package util.models;

import game.logic.exceptions.NarratorException;
import game.setups.Setup;
import models.Ability;
import models.Role;
import models.enums.AbilityModifierName;
import models.enums.AbilityType;
import models.enums.RoleModifierName;
import models.modifiers.AbilityModifier;
import models.modifiers.Modifier;
import models.modifiers.RoleModifier;
import util.game.LookupUtil;

public class RoleTestUtil {

    public static void copyModifiers(Role toRole, Role fromRole) {
        for(Modifier<RoleModifierName> modifier: fromRole.modifiers)
            toRole.modifiers.add(new RoleModifier(modifier));

        Ability toAbility;
        for(Ability fromAbility: fromRole.abilityMap.values()){
            toAbility = toRole.getAbility(fromAbility.type);
            for(Modifier<AbilityModifierName> modifier: fromAbility.modifiers)
                toAbility.modifiers.add(new AbilityModifier(modifier.name, modifier.value, modifier.minPlayerCount,
                        modifier.maxPlayerCount, modifier.upsertedAt));
        }
    }

    public static AbilityType getFirstAbilityType(Role role) {
        return role.abilityMap.values().iterator().next().type;
    }

    public static Role getRole(Setup setup, String roleName) {
        return LookupUtil.findRole(setup, roleName).get();
    }

    public static Role getRoleWithAbility(Setup setup, AbilityType abilityType) {
        for(Role role: setup.roles)
            if(role.hasAbility(abilityType))
                return role;
        throw new NarratorException("not found. shouldn't happen.");
    }
}
