package util.models;

import java.sql.SQLException;
import java.util.Optional;

import game.abilities.Architect;
import game.abilities.Disguiser;
import game.abilities.Infiltrator;
import game.abilities.MasonLeader;
import game.abilities.SerialKiller;
import game.event.ArchitectChat;
import game.event.DeadChat;
import game.event.Message;
import game.logic.Player;
import game.logic.support.Constants;
import game.logic.templates.BasicRoles;
import game.setups.Setup;
import integration.server.UserWrapper;
import json.JSONException;
import json.JSONObject;
import models.FactionRole;
import models.Role;
import models.schemas.PlayerSchema;
import nnode.Lobby;
import repositories.PlayerRepo;
import services.ActionService;
import services.ChatService;
import services.FactionRoleService;
import services.LobbyService;
import services.PlayerService;
import services.RoleService;
import services.UserService;
import util.CollectionUtil;
import util.FakeConstants;
import util.UserFactory;

public class GameTestUtil {

    public static void buildRoom(Lobby game) throws JSONException, SQLException {
        PlayerSchema architect = PlayerTestUtil.getSchemaByRoleName(game, "Architect");
        PlayerSchema citizen = PlayerTestUtil.getSchemaByRoleName(game, "Citizen");
        PlayerSchema masonLeader = PlayerTestUtil.getSchemaByRoleName(game, "Mason Leader");
        String command = Message.CommandCreator(Architect.COMMAND, masonLeader.name, citizen.name);
        ActionService.submitAction(architect.userID.get(), command, Constants.ALL_TIME_LEFT);
        game.game.forceEndDay(Constants.NO_TIME_LEFT);
    }

    public static Lobby createStartedGame() throws JSONException, SQLException {
        UserWrapper host = new UserWrapper(FakeConstants.PLAYER_NAME);
        host.hostGame(false);
        Setup setup = host.getSetup();
        long factionID = setup.getFactionByColor(Setup.MAFIA_C).id;
        Role infiltratorRole = RoleService.createRole(host.id, setup.id, "Infiltrator",
                CollectionUtil.toSet(Infiltrator.abilityType)).role;
        FactionRole infiltrator = FactionRoleService.createFactionRole(host.getUserID(), factionID,
                infiltratorRole.id).factionRole;

        BasicRoles.setup = host.getSetup();

        host.addSetupHidden(BasicRoles.Architect());
        host.addSetupHidden(BasicRoles.Citizen());
        host.addSetupHidden(BasicRoles.Doctor());
        host.addSetupHidden(BasicRoles.Goon());
        host.addSetupHidden(infiltrator);
        host.addSetupHidden(BasicRoles.Mason());
        host.addSetupHidden(BasicRoles.MasonLeader());
        host.addSetupHidden(BasicRoles.getMember(Setup.YAKUZA_C, Disguiser.abilityType));
        host.addSetupHidden(BasicRoles.SerialKiller());

        int currentPlayerSize = PlayerRepo.getByGameID(host.getGameID()).size();
        int rolesListSize = 9; // lobby.game.setup.rolesList.size(lobby.game)
        int playerCountToAdd = rolesListSize - currentPlayerSize;
        for(int i = 0; i < playerCountToAdd; i++)
            UserFactory.joinPrivateGame(host.getGameID());

        host.startGame(true);

        return LobbyService.getByGameID(host.getGameID());
    }

    public static void disguiseByRoleName(Lobby lobby, String roleName) throws JSONException, SQLException {
        PlayerSchema disguiser = PlayerTestUtil.getSchemaByRoleName(lobby, "Disguiser");
        PlayerSchema goon = PlayerTestUtil.getSchemaByRoleName(lobby, roleName);
        String command = Message.CommandCreator(Disguiser.COMMAND, goon.name);
        ActionService.submitAction(disguiser.userID.get(), command, Constants.ALL_TIME_LEFT);
        lobby.game.forceEndNight(Constants.ALL_TIME_LEFT);
    }

    public static void recruitByRoleName(Lobby lobby, String roleName) throws JSONException, SQLException {
        Player recruited = PlayerTestUtil.getByRoleName(lobby, roleName);
        PlayerSchema masonLeader = PlayerTestUtil.getSchemaByRoleName(lobby, MasonLeader.ROLE_NAME);
        String command = Message.CommandCreator(MasonLeader.COMMAND, recruited.getName());
        ActionService.submitAction(masonLeader.userID.get(), command, Constants.ALL_TIME_LEFT);
        lobby.game.forceEndNight(Constants.ALL_TIME_LEFT);
    }

    public static void skipToDay(Lobby lobby) {
        lobby.game.forceEndNight(Constants.ALL_TIME_LEFT);
    }

    public static void skipToNight(Lobby lobby) {
        lobby.game.forceEndDay(0);
    }

    public static void speakDeadByRoleName(Lobby lobby, String roleName) throws JSONException, SQLException {
        PlayerSchema user = PlayerTestUtil.getSchemaByRoleName(lobby, roleName);
        sendGameChatMessage(user.userID.get(), DeadChat.KEY, FakeConstants.MESSAGE_TEXT);
    }

    public static void speakFactionByRoleName(Lobby lobby, String roleName) throws JSONException, SQLException {
        PlayerSchema user = PlayerTestUtil.getSchemaByRoleName(lobby, roleName);
        Player player = PlayerService.getByUserID(user.userID.get()).get();
        sendGameChatMessage(user.userID.get(), player.getColor(), FakeConstants.MESSAGE_TEXT);
    }

    public static void speakRoom(Lobby lobby) throws JSONException, SQLException {
        PlayerSchema citizenUser = PlayerTestUtil.getSchemaByRoleName(lobby, "Citizen");
        Player citizenPlayer = PlayerService.getByUserID(citizenUser.userID.get()).get();
        PlayerSchema masonLeaderUser = PlayerTestUtil.getSchemaByRoleName(lobby, "Mason Leader");
        Player masonLeaderPlayer = PlayerService.getByUserID(masonLeaderUser.userID.get()).get();
        String chatKey = ArchitectChat.GetKey(citizenPlayer, masonLeaderPlayer, lobby.game.getDayNumber());
        sendGameChatMessage(citizenUser.userID.get(), chatKey, FakeConstants.MESSAGE_TEXT);
    }

    private static void sendGameChatMessage(long userID, String chatKey, String messageText)
            throws JSONException, SQLException {
        ChatService.submit(userID, chatKey, FakeConstants.MESSAGE_TEXT, Optional.empty(), new JSONObject());

    }

    public static void stabByRoleNameDoctor(Lobby lobby, String roleName) throws JSONException, SQLException {
        PlayerSchema doctor = PlayerTestUtil.getSchemaByRoleName(lobby, roleName);
        PlayerSchema serialKiller = PlayerTestUtil.getSchemaByRoleName(lobby, "Serial Killer");
        String command = Message.CommandCreator(SerialKiller.COMMAND, doctor.name);
        ActionService.submitAction(serialKiller.userID.get(), command, 1);
        lobby.game.forceEndNight(Constants.ALL_TIME_LEFT);
    }

    public static void voteOutByRoleName(Lobby lobby, String roleName) throws JSONException, SQLException {
        PlayerSchema citizen = PlayerTestUtil.getSchemaByRoleName(lobby, roleName);
        int count = 0;
        for(long userID: UserService.getUserIDs(lobby.gameID)){
            if(citizen.userID.get() != userID && count < (lobby.game.players.size() / 2 + 1)){
                count++;
                ActionService.submitAction(userID, "vote " + citizen.name, Constants.ALL_TIME_LEFT);
            }
        }
    }
}
