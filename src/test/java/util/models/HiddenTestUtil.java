package util.models;

import game.abilities.Hidden;
import game.setups.Setup;
import models.FactionRole;
import services.HiddenService;
import util.Util;

public class HiddenTestUtil {
    public static void addFactionRole(Hidden hidden, FactionRole factionRole) {
        HiddenService.addFactionRole(Util.getID(), hidden, factionRole, 0, Setup.MAX_PLAYER_COUNT);
    }
}
