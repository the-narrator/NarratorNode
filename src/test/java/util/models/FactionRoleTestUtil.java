package util.models;

import java.util.ArrayList;
import java.util.Optional;

import models.FactionRole;
import services.FactionRoleService;

public class FactionRoleTestUtil {

    public static FactionRole addCultRole(FactionRole cultFaction, FactionRole factionRole) {
        return FactionRoleService.createFactionRole(cultFaction.faction, factionRole.role);
    }

    public static ArrayList<String> getPublicDescription(FactionRole factionRole) {
        return factionRole.getPublicDescription(Optional.empty());
    }
}
