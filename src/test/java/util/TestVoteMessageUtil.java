package util;

import game.event.EventList;
import game.event.Message;
import game.event.VoteAnnouncement;
import game.logic.support.Constants;
import integration.logic.SuperTest;

public class TestVoteMessageUtil {
    public static void assertLastVoteText(String text, String access) {

        EventList events = SuperTest.game.getEventManager().getEventLog(Constants.DAY_CHAT).getEvents();
        VoteAnnouncement voteAnnouncement = null;
        for(Message message: events){
            if(message instanceof VoteAnnouncement)
                voteAnnouncement = (VoteAnnouncement) message;
        }
        if(voteAnnouncement == null)
            SuperTest.fail("No vote message found");
        SuperTest.assertEquals(text, voteAnnouncement.access(access));
    }
}
