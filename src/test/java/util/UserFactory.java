package util;

import java.sql.SQLException;

import game.ai.Computer;
import integration.server.UserWrapper;
import json.JSONException;
import models.idtypes.GameID;
import nnode.Permission;
import repositories.UserPermissionRepo;

public class UserFactory {

    public static final boolean MOD = true;
    public static final boolean PRIVATE = false;
    public static final boolean PUBLIC = true;
    public static int nameCounter = 1;

    public static UserWrapper hostPrivateCustomGame() throws JSONException, SQLException {
        return hostGame(UserFactory.PRIVATE, !UserFactory.MOD);
    }

    public static UserWrapper hostPublicGame() throws JSONException, SQLException {
        return hostGame(UserFactory.PUBLIC, !UserFactory.MOD);
    }

    public static UserWrapper HostGame(boolean public_hosting) throws JSONException, SQLException {
        return hostGame(public_hosting, !UserFactory.MOD);
    }

    public static UserWrapper hostGame(boolean public_hosting, boolean mod) throws JSONException, SQLException {
        return UserFactory.hostGame(Computer.toLetter(nameCounter++), public_hosting, mod);
    }

    public static UserWrapper hostGame(String name, boolean public_hosting, boolean mod)
            throws JSONException, SQLException {
        UserWrapper userWrapper = new UserWrapper(name);

        if(!mod){
            UserPermissionRepo.removeAllPermissions(userWrapper.id);
        }else{
            UserPermissionRepo.addPermission(userWrapper.id, Permission.GAME_EDITING);
            UserPermissionRepo.addPermission(userWrapper.id, Permission.BOT_ADDING);
            UserPermissionRepo.addPermission(userWrapper.id, Permission.KICK);
        }

        userWrapper.hostGame(public_hosting);
        return userWrapper;
    }

    public static UserWrapper joinPublicGame() throws JSONException, SQLException {
        return UserFactory.joinPublicGame(Computer.toLetter(UserFactory.nameCounter++));
    }

    public static UserWrapper joinPublicGame(String name) throws JSONException, SQLException {
        UserWrapper user = new UserWrapper(name);
        user.joinGame();
        return user;
    }

    public static UserWrapper joinPrivateGame(String lobbyID) throws JSONException, SQLException {
        UserWrapper nc = new UserWrapper(Computer.toLetter(nameCounter++));
        nc.joinGame(lobbyID);
        return nc;
    }

    public static UserWrapper joinPrivateGame(GameID gameID) throws JSONException, SQLException {
        UserWrapper nc = new UserWrapper(Computer.toLetter(nameCounter++));
        nc.joinGame(gameID);
        return nc;
    }

    public static UserWrapper joinPrivateGame(String name, String gameID) throws JSONException, SQLException {
        UserWrapper nc = new UserWrapper(name);
        nc.joinGame(gameID);
        return nc;
    }
}
