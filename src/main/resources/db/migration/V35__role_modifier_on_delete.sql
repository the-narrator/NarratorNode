DELETE from role_int_modifiers
WHERE role_id NOT IN (
    SELECT id FROM roles
);

DELETE from role_bool_modifiers
WHERE role_id NOT IN (
  SELECT id FROM roles
);

ALTER TABLE role_bool_modifiers
    ADD CONSTRAINT role_bool_modifiers_role_ref
        FOREIGN KEY (role_id) REFERENCES roles(id) ON DELETE CASCADE;

ALTER TABLE role_int_modifiers
    ADD CONSTRAINT role_int_modifiers_role_ref
        FOREIGN KEY (role_id) REFERENCES roles(id) ON DELETE CASCADE;
