CREATE TABLE saved_games (
	game_id SERIAL PRIMARY KEY,
	date TIMESTAMP NOT NULL,
	game_seed BIGINT NOT NULL,
	hostName varchar(40) NOT NULL,
	hostToken varchar(128) NOT NULL,
	isFinished BOOLEAN DEFAULT true,
	isPrivate BOOLEAN DEFAULT true NOT NULL,
	instance_id char(4) DEFAULT null
);

CREATE TABLE commands_saved_games (
	command_id SERIAL PRIMARY KEY,
	game_id BIGINT NOT NULL,
	command VARCHAR(550) NOT NULL,
	counter BIGINT NOT NULL
);

CREATE TABLE player_saved_games (
	player_id SERIAL PRIMARY KEY,
	game_id BIGINT NOT NULL,
	color CHAR(7) NOT NULL,
	role VARCHAR(40) NOT NULL,
	baseName VARCHAR(40) NOT NULL,
	name VARCHAR(40) NOT NULL,
	token VARCHAR(128),
	isExited BOOLEAN DEFAULT true,

	winner BOOLEAN,
	deathday SMALLINT
);

CREATE TABLE player_deaths(
	game_id BIGINT NOT NULL,
	token varchar(128),
	baseName varchar(40) NOT NULL,
	death_type VARCHAR(30)
);

CREATE TABLE faction_saved_games (
	team_id serial PRIMARY KEY,
	game_id BIGINT NOT NULL,
	color CHAR(7) NOT NULL,
	name varchar(30) NOT NULL,
	descrip VARCHAR(500)
);

CREATE TABLE enemies_saved_games(
	enemies_id serial PRIMARY KEY,
	game_id BIGINT NOT NULL,
	teamColor1 CHAR(7) NOT NULL,
	teamColor2 CHAR(7) NOT NULL
);

CREATE TABLE sheriff_detectables_saved_games(
	team_id SERIAL PRIMARY KEY,
	game_id BIGINT NOT NULL,
	teamColor CHAR(7) NOT NULL,
	detectedColor CHAR(7) NOT NULL
);

CREATE TABLE roles_list_saved_games(
	rt_internal_id serial PRIMARY KEY,
	game_id BIGINT NOT NULL,
	roleName VARCHAR(40) NOT NULL,
	internalName VARCHAR(40),
	color CHAR(7) NOT NULL,
	rt_id SMALLINT,
	spawn VARCHAR(47),
	exposed BOOLEAN NOT NULL DEFAULT false,

	playerAssigned varchar(40) NOT NULL
);


CREATE TABLE roles_list_composition_saved_games(
	rr_id SERIAL PRIMARY KEY,
	rt_id SMALLINT NOT NULL,
	roleName VARCHAR(40) NOT NULL,
	internalName VARCHAR(40),
	color CHAR(7) NOT NULL,
	game_id BIGINT NOT NULL
);

CREATE TABLE role_modifiers_saved_games(
	game_id BIGINT NOT NULL,
	randomMemberComposition BOOLEAN NOT NULL,  /* for if the role is part of the main roles list, or not*/
	rt_id BIGINT NOT NULL,
	modifierName VARCHAR(40),
	modifierIValue MEDIUMINT,
	modifierBValue BOOLEAN
);

CREATE TABLE ability_makeup_saved_games(
	game_id BIGINT NOT NULL,
	randomMemberComposition BOOLEAN NOT NULL,
	rt_id BIGINT NOT NULL,
	abilityName VARCHAR(20)
);

CREATE TABLE team_abilities_saved_games(
	game_id BIGINT NOT NULL,
	color CHAR(7) NOT NULL,
	abilityName VARCHAR(20)
);

CREATE TABLE team_modifiers_saved_games(
	game_id BIGINT NOT NULL,
	color CHAR(7) NOT NULL,
	modifierName VARCHAR(40) NOT NULL,
	modifierIValue MEDIUMINT,
	modifierBValue BOOLEAN
);

CREATE TABLE stories(
	story_id serial PRIMARY KEY,
	story VARCHAR(600),
	team VARCHAR(36),
	death VARCHAR(50),
	role VARCHAR(100),
	genre VARCHAR(30)
);

CREATE TABLE role_type_stories(
	role_id CHAR(1),
	role VARCHAR(30)
);

CREATE TABLE death_type_stories(
	death_id CHAR(1),
	death VARCHAR(30)
);

CREATE TABLE permissions(
	user CHAR(28) NOT NULL,
	permission SMALLINT NOT NULL
);

/* ROLE */

CREATE TABLE role_stats(
  baseName VARCHAR(40),
  total BIGINT DEFAULT 0,
  wins BIGINT DEFAULT 0,
  lynched BIGINT DEFAULT 0
);

CREATE TABLE users(
  token VARCHAR(128) PRIMARY KEY NOT NULL,
  android_id VARCHAR(152),
  points BIGINT DEFAULT 0,
  winrate DOUBLE(5, 2) DEFAULT 0.0,
  accountType VARCHAR(10) NOT NULL
);

/* DISCORD */

CREATE TABLE discord_role_to_guild(
	role_id VARCHAR(64) NOT NULL,
	guild_id VARCHAR(64) NOT NULL
);

CREATE TABLE discord_edited_channels(
	channel_id VARCHAR(64) NOT NULL
);

CREATE TABLE discord_subscribers(
	guild_id VARCHAR(64) NOT NULL,
	user_id VARCHAR(64) NOT NULL
);

CREATE TABLE discord_night_channels(
	channel_id VARCHAR(64) NOT NULL,
	game_id BIGINT NOT NULL
);

CREATE TABLE discord_history(
	channel_id VARCHAR(64) NOT NULL,
	user_id VARCHAR(64) NOT NULL,
	text VARCHAR(500) NOT NULL,
	date TIMESTAMP NOT NULL
);

CREATE TABLE discord_last_tagged(
	user_id VARCHAR(64) NOT NULL UNIQUE,
	timestamp DATETIME NOT NULL
);

CREATE TABLE discord_autotag_block(
	blocked VARCHAR(64) NOT NULL,
	blocker VARCHAR(64) NOT NULL
);


/* SEED_TABLES */
CREATE TABLE seed_team_win_counts(
	seed BIGINT NOT NULL,
	playerCount SMALLINT NOT NULL,
	setup VARCHAR(30) NOT NULL,
	color CHAR(7) NOT NULL,
	success BIGINT NOT NULL
);

ALTER TABLE seed_team_win_counts ADD CONSTRAINT uq_combo UNIQUE(seed, playerCount, setup, color);

CREATE TABLE seed_counts(
	seed BIGINT NOT NULL,
	playerCount SMALLINT NOT NULL,
	setup VARCHAR(30) NOT NULL,
	attempts BIGINT NOT NULL
);

ALTER TABLE seed_counts ADD CONSTRAINT uq_combo UNIQUE(seed, playerCount, setup);

CREATE TABLE seed_variability(
	seed BIGINT NOT NULL,
	playerCount SMALLINT NOT NULL,
	setup VARCHAR(30) NOT NULL,
	variability DOUBLE(12, 10) NOT NULL
);

ALTER TABLE seed_variability ADD CONSTRAINT uq_combo UNIQUE(seed, playerCount, setup);

CREATE TABLE bad_seeds(
	game_seed BIGINT NOT NULL,
	brain_seed BIGINT NOT NULL,
	playerCount SMALLINT NOT NULL,
	setup VARCHAR(30) NOT NULL
);




CREATE TABLE player_input(
	input_id serial PRIMARY KEY,
	game_id BIGINT NOT NULL,
	text VARCHAR(5000)
);
