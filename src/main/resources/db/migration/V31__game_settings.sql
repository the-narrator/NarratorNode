CREATE TABLE game_int_modifiers(
   game_id BIGINT UNSIGNED NOT NULL,
   name VARCHAR(40) NOT NULL,
   value MEDIUMINT NOT NULL
);
ALTER TABLE game_int_modifiers
    ADD CONSTRAINT game_int_modifiers_game_id_name
    UNIQUE(game_id, name);
ALTER TABLE game_int_modifiers
    ADD CONSTRAINT game_int_modifiers_on_game_delete
    FOREIGN KEY (game_id)
    REFERENCES replays(id)
    ON DELETE CASCADE;

CREATE TABLE game_bool_modifiers(
   game_id BIGINT UNSIGNED NOT NULL,
   name VARCHAR(40) NOT NULL,
   value BOOLEAN NOT NULL
);
ALTER TABLE game_bool_modifiers
    ADD CONSTRAINT game_bool_modifiers_game_id_name
    UNIQUE(game_id, name);
ALTER TABLE game_bool_modifiers
    ADD CONSTRAINT game_bool_modifiers_on_game_delete
    FOREIGN KEY (game_id)
    REFERENCES replays(id)
    ON DELETE CASCADE;

INSERT INTO game_bool_modifiers
    SELECT replays.id AS game_id, setup_bool_modifiers.name as name, setup_bool_modifiers.value as value
    FROM replays
    LEFT JOIN setup_bool_modifiers ON setup_bool_modifiers.setup_id = replays.setup_id
    WHERE LOWER(setup_bool_modifiers.name) IN ('host_voting', 'omniscient_dead', 'chat_roles');

INSERT INTO game_int_modifiers
    SELECT replays.id AS game_id, setup_int_modifiers.name as name, setup_int_modifiers.value as value
    FROM replays
    LEFT JOIN setup_int_modifiers ON setup_int_modifiers.setup_id = replays.setup_id
    WHERE UPPER(setup_int_modifiers.name) IN ('DAY_LENGTH', 'NIGHT_LENGTH', 'TRIAL_LENGTH', 'DISCUSSION_LENGTH', 'ROLE_PICKING_LENGTH', 'VOTE_SYSTEM');

DELETE FROM setup_int_modifiers
WHERE UPPER(name) IN ('DAY_LENGTH', 'NIGHT_LENGTH', 'TRIAL_LENGTH', 'DISCUSSION_LENGTH', 'ROLE_PICKING_LENGTH', 'VOTE_SYSTEM');

DELETE FROM setup_bool_modifiers
WHERE LOWER(name) IN ('host_voting', 'omniscient_dead', 'chat_roles');
