CREATE TABLE player_preferences(
   setup_id BIGINT UNSIGNED NOT NULL,
   user_id BIGINT UNSIGNED NOT NULL,
   entity_id BIGINT UNSIGNED NOT NULL,
   prefer_type VARCHAR(7),
   UNIQUE KEY `player_preferences_unique` (`setup_id`,`user_id`, `entity_id`, `prefer_type`),
   CONSTRAINT `player_preferences_on_setup_delete` FOREIGN KEY (`setup_id`) REFERENCES `setups` (`id`) ON DELETE CASCADE
);
