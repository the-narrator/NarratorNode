ALTER TABLE commands
	ADD CONSTRAINT commands_counter_replay_id
	UNIQUE (counter, replay_id);

ALTER TABLE faction_abilities
	ADD CONSTRAINT faction_abilities_faction_id_name
	UNIQUE (faction_id, name);

ALTER TABLE faction_ability_bool_modifiers
	ADD CONSTRAINT faction_ability_bool_modifiers_faction_ability_id_name
	UNIQUE (faction_ability_id, name);

ALTER TABLE faction_ability_int_modifiers
	ADD CONSTRAINT faction_ability_int_modifiers_faction_ability_id_name
	UNIQUE (faction_ability_id, name);

ALTER TABLE faction_enemies ADD COLUMN setup_id BIGINT UNSIGNED;
UPDATE faction_enemies fe
	INNER JOIN factions f ON f.id = fe.enemy_a
	SET fe.setup_id = f.setup_id;
ALTER TABLE faction_enemies CHANGE COLUMN setup_id setup_id BIGINT UNSIGNED;

ALTER TABLE faction_enemies
	DROP FOREIGN KEY faction_enemies_on_faction1_delete;
ALTER TABLE faction_enemies
	DROP FOREIGN KEY faction_enemies_on_faction2_delete;
ALTER TABLE faction_enemies
	DROP INDEX faction_enemies_on_faction2_delete;
ALTER TABLE faction_enemies
	ADD CONSTRAINT faction_enemies_on_faction1_delete
	FOREIGN KEY (enemy_A)
	REFERENCES factions(id)
	ON DELETE CASCADE;
DELETE FROM faction_enemies
    WHERE enemy_b NOT IN (
        SELECT id FROM factions
    );
ALTER TABLE faction_enemies
	ADD CONSTRAINT faction_enemies_on_faction2_delete
	FOREIGN KEY (enemy_b)
	REFERENCES factions(id)
	ON DELETE CASCADE;

ALTER TABLE faction_bool_modifiers
	ADD CONSTRAINT faction_bool_modifiers_faction_id_name
	UNIQUE (faction_id, name);

ALTER TABLE faction_int_modifiers
	ADD CONSTRAINT faction_int_modifiers_faction_id_name
	UNIQUE (faction_id, name);

ALTER TABLE factions
	ADD CONSTRAINT factions_setup_id_color
	UNIQUE (setup_id, color);
ALTER TABLE factions
	ADD CONSTRAINT factions_setup_id_name
	UNIQUE (setup_id, name);

ALTER TABLE role_abilities
	ADD CONSTRAINT role_abilities_role_id_name
	UNIQUE (role_id, name);

ALTER TABLE role_ability_bool_modifiers
	ADD CONSTRAINT role_ability_bool_modifiers_role_ability_id_name
	UNIQUE (role_ability_id, name);

ALTER TABLE role_ability_int_modifiers
	ADD CONSTRAINT role_ability_int_modifiers_role_ability_id_name
	UNIQUE (role_ability_id, name);

CREATE TABLE role_int_modifiers2 LIKE role_int_modifiers;
ALTER TABLE role_int_modifiers2
	ADD CONSTRAINT role_int_modifiers_role_id_name
	UNIQUE (role_id, name);
INSERT IGNORE INTO role_int_modifiers2
    SELECT * FROM role_int_modifiers;
DROP TABLE role_int_modifiers;
ALTER TABLE role_int_modifiers2 RENAME TO role_int_modifiers;

CREATE TABLE role_bool_modifiers2 LIKE role_bool_modifiers;
ALTER TABLE role_bool_modifiers2
	ADD CONSTRAINT role_bool_modifiers_role_id_name
	UNIQUE (role_id, name);
INSERT IGNORE INTO role_bool_modifiers2
    SELECT * FROM role_bool_modifiers;
DROP TABLE role_bool_modifiers;
ALTER TABLE role_bool_modifiers2 RENAME TO role_bool_modifiers;

ALTER TABLE setup_bool_modifiers
	ADD CONSTRAINT setup_bool_modifiers_setup_id_name
	UNIQUE (setup_id, name);

ALTER TABLE setup_int_modifiers
	ADD CONSTRAINT setup_int_modifiers_setup_id_name
	UNIQUE (setup_id, name);

ALTER TABLE sheriff_checkables
	ADD CONSTRAINT sheriff_checkables_sheriff_faction_id_detectable_faction_id
	UNIQUE (sheriff_faction_id, detectable_faction_id);
