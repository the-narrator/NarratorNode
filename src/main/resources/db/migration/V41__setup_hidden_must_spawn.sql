ALTER TABLE setup_hiddens ADD COLUMN must_spawn BOOLEAN AFTER is_exposed;
UPDATE setup_hiddens SET must_spawn = false;
ALTER TABLE setup_hiddens CHANGE COLUMN must_spawn must_spawn BOOLEAN NOT NULL;
