ALTER TABLE setup_iterations ADD COLUMN `author_id` BIGINT(20) UNSIGNED NOT NULL;

ALTER TABLE setup_iterations ADD CONSTRAINT `author_id`
        FOREIGN KEY (author_id)
        REFERENCES users (id)
        ON DELETE CASCADE;
