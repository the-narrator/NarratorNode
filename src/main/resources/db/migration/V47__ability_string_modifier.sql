CREATE TABLE `role_ability_string_modifiers` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `role_ability_id` bigint(20) unsigned NOT NULL,
    `name` varchar(40) NOT NULL,
    `value` varchar(25) NOT NULL,
    `min_player_count` tinyint(3) unsigned NOT NULL,
    `max_player_count` tinyint(3) unsigned NOT NULL,
    `upserted_at` datetime NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `role_ability_string_modifiers_unique` (`role_ability_id`,`name`,`min_player_count`,`max_player_count`),
    KEY `role_ability_string_modifiers_on_role_ability_delete` (`role_ability_id`),
    CONSTRAINT `role_ability_string_modifiers_on_role_ability_delete` FOREIGN KEY (`role_ability_id`) REFERENCES `role_abilities` (`id`) ON DELETE CASCADE
);

CREATE TABLE `faction_ability_string_modifiers` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `faction_ability_id` bigint(20) unsigned NOT NULL,
    `name` varchar(20) NOT NULL,
    `value` varchar(25) NOT NULL,
    `min_player_count` tinyint(3) unsigned NOT NULL,
    `max_player_count` tinyint(3) unsigned NOT NULL,
    `upserted_at` datetime NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `faction_ability_string_modifiers_unique` (`faction_ability_id`,`name`,`min_player_count`,`max_player_count`),
    KEY `faction_ability_string_modifiers_on_faction_ability_delete` (`faction_ability_id`),
    CONSTRAINT `faction_ability_string_modifiers_on_faction_ability_delete` FOREIGN KEY (`faction_ability_id`) REFERENCES `faction_abilities` (`id`) ON DELETE CASCADE
);

CREATE TABLE `faction_string_modifiers` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `faction_id` bigint(20) unsigned NOT NULL,
    `name` varchar(20) DEFAULT NULL,
    `value` varchar(25) NOT NULL,
    `min_player_count` tinyint(3) unsigned NOT NULL,
    `max_player_count` tinyint(3) unsigned NOT NULL,
    `upserted_at` datetime NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `faction_string_modifiers_unique` (`faction_id`,`name`,`min_player_count`,`max_player_count`),
    CONSTRAINT `faction_string_modifiers_on_faction_delete` FOREIGN KEY (`faction_id`) REFERENCES `factions` (`id`) ON DELETE CASCADE
);

CREATE TABLE `faction_role_role_string_modifiers` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `faction_role_id` bigint(20) unsigned NOT NULL,
    `name` varchar(40) NOT NULL,
    `value` varchar(25) NOT NULL,
    `min_player_count` tinyint(3) unsigned NOT NULL,
    `max_player_count` tinyint(3) unsigned NOT NULL,
    `upserted_at` datetime NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `faction_role_role_string_modifiers_unique` (`faction_role_id`,`name`,`min_player_count`,`max_player_count`),
    CONSTRAINT `faction_role_role_string_modifiers_on_faction_role_delete` FOREIGN KEY (`faction_role_id`) REFERENCES `faction_roles` (`id`) ON DELETE CASCADE
);

CREATE TABLE `role_string_modifiers` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `role_id` bigint(20) unsigned NOT NULL,
    `name` varchar(40) NOT NULL,
    `value` varchar(25) NOT NULL,
    `min_player_count` tinyint(3) unsigned NOT NULL,
    `max_player_count` tinyint(3) unsigned NOT NULL,
    `upserted_at` datetime NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `role_string_modifiers_unique` (`role_id`,`name`,`min_player_count`,`max_player_count`),
    CONSTRAINT `role_string_modifiers_on_role_delete` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
);

CREATE TABLE `setup_string_modifiers` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `setup_id` bigint(20) unsigned NOT NULL,
    `name` varchar(40) NOT NULL,
    `value` varchar(25) NOT NULL,
    `min_player_count` tinyint(3) unsigned NOT NULL,
    `max_player_count` tinyint(3) unsigned NOT NULL,
    `upserted_at` datetime NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `setup_string_modifiers_unique` (`setup_id`,`name`,`min_player_count`,`max_player_count`),
CONSTRAINT `setup_string_modifiers_on_setup_delete` FOREIGN KEY (`setup_id`) REFERENCES `setups` (`id`) ON DELETE CASCADE
);
