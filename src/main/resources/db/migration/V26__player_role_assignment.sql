CREATE TABLE player_roles (
    game_id BIGINT UNSIGNED NOT NULL,
    setup_hidden_id BIGINT UNSIGNED NOT NULL,
    player_id BIGINT UNSIGNED NOT NULL
);

ALTER TABLE player_roles ADD CONSTRAINT setup_hidden_id_game_id UNIQUE(setup_hidden_id, game_id);
ALTER TABLE player_roles ADD CONSTRAINT player_id UNIQUE(player_id);

CREATE TABLE game_spawns (
    game_id BIGINT UNSIGNED NOT NULL,
    setup_hidden_id BIGINT UNSIGNED NOT NULL,
    faction_role_id BIGINT UNSIGNED NOT NULL
);

ALTER TABLE game_spawns ADD CONSTRAINT game_id_setup_hidden_id UNIQUE(game_id, setup_hidden_id);

ALTER TABLE player_roles ADD CONSTRAINT player_roles_on_player_delete FOREIGN KEY (player_id) REFERENCES players(id) ON DELETE CASCADE;
ALTER TABLE game_spawns ADD CONSTRAINT game_spawns_on_game_delete FOREIGN KEY (game_id) REFERENCES replays(id) ON DELETE CASCADE;

ALTER TABLE players DROP COLUMN hidden_id;
ALTER TABLE players DROP COLUMN faction_role_id;
