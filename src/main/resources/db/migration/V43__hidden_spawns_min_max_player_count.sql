RENAME TABLE hidden_faction_roles TO hidden_spawns;
ALTER TABLE hidden_spawns ADD `id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY FIRST;

ALTER TABLE hidden_spawns ADD COLUMN min_player_count TINYINT UNSIGNED;
UPDATE hidden_spawns SET min_player_count = 0;
ALTER TABLE hidden_spawns CHANGE COLUMN min_player_count min_player_count TINYINT UNSIGNED NOT NULL;

ALTER TABLE hidden_spawns ADD COLUMN max_player_count TINYINT UNSIGNED;
UPDATE hidden_spawns SET max_player_count = 255;
ALTER TABLE hidden_spawns CHANGE COLUMN max_player_count max_player_count TINYINT UNSIGNED NOT NULL;
