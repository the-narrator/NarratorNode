UPDATE faction_enemies fe
    INNER JOIN factions f ON fe.enemy_a = f.id
SET fe.setup_id = f.setup_id;

CREATE TABLE faction_enemies2 LIKE faction_enemies;
ALTER TABLE faction_enemies2 ADD CONSTRAINT enemy_a_enemy_b UNIQUE(enemy_a, enemy_b);
INSERT IGNORE INTO faction_enemies2
SELECT * FROM faction_enemies;
DROP TABLE faction_enemies;
ALTER TABLE faction_enemies2 RENAME TO faction_enemies;

ALTER TABLE faction_enemies CHANGE COLUMN setup_id setup_id BIGINT NOT NULL;
