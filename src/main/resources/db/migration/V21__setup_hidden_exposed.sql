ALTER TABLE setup_hiddens ADD COLUMN is_exposed BOOLEAN DEFAULT FALSE;
UPDATE setup_hiddens sh
LEFT JOIN hiddens h ON h.id = sh.hidden_id
SET sh.is_exposed = h.is_exposed;
ALTER TABLE hiddens DROP COLUMN is_exposed;
