CREATE TABLE `faction_role_ability_string_modifiers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `faction_role_id` bigint(20) unsigned NOT NULL,
  `role_ability_id` bigint(20) unsigned NOT NULL,
  `name` varchar(40) NOT NULL,
  `value` varchar(25) NOT NULL,
  `min_player_count` tinyint(3) unsigned NOT NULL,
  `max_player_count` tinyint(3) unsigned NOT NULL,
  `upserted_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `faction_role_ability_string_modifiers_unique` (`faction_role_id`,`role_ability_id`,`name`,`min_player_count`,`max_player_count`),
  KEY `faction_role_ability_string_modifiers_on_role_ability_delete` (`role_ability_id`),
  CONSTRAINT `faction_role_ability_string_modifiers_on_faction_role_delete` FOREIGN KEY (`faction_role_id`) REFERENCES `faction_roles` (`id`) ON DELETE CASCADE,
  CONSTRAINT `faction_role_ability_string_modifiers_on_role_ability_delete` FOREIGN KEY (`role_ability_id`) REFERENCES `role_abilities` (`id`) ON DELETE CASCADE
);
