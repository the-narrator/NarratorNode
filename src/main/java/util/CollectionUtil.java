package util;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

public class CollectionUtil {
    public static <T> LinkedHashSet<T> toSet(T item) {
        LinkedHashSet<T> set = new LinkedHashSet<>();
        set.add(item);
        return set;
    }

    @SafeVarargs
    public static <T> LinkedHashSet<T> toSet(T... items) {
        LinkedHashSet<T> set = new LinkedHashSet<>();
        for(T item: items)
            set.add(item);
        return set;
    }

    @SafeVarargs
    public static <T> ArrayList<T> toArrayList(T... items) {
        ArrayList<T> list = new ArrayList<>();
        for(T item: items)
            list.add(item);
        return list;
    }

    public static <T> Set<T> diff(Set<T> set1, Set<T> set2) {
        Set<T> set3 = new HashSet<>();
        for(T ele: set1)
            if(!set2.contains(ele))
                set3.add(ele);
        return set3;
    }
}
