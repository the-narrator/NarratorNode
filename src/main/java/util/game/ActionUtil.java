package util.game;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.LinkedList;

import game.abilities.Architect;
import game.abilities.Vote;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.support.action.Action;
import models.enums.AbilityType;

public class ActionUtil {

    public static PlayerList getActionTargets(ArrayList<Action> actions) {
        PlayerList actionTargets = new PlayerList();
        for(Action a: actions){
            actionTargets.add(a.getTargets());
        }
        return actionTargets;
    }

    public static final Comparator<Action> DayVoteSorter = new Comparator<Action>() {
        @Override
        public int compare(Action p1, Action p2) {
            if(p1.abilityType == p2.abilityType)
                return 0;
            if(p1.abilityType == Vote.abilityType)
                return -1;
            if(p2.abilityType == Vote.abilityType)
                return 1;
            return 0;
        }
    };
    public static final Comparator<Action> EndDayResolveSorter = new Comparator<Action>() {
        @Override
        public int compare(Action a1, Action a2) {
            if(a1.owner.is(Architect.abilityType) && !a2.owner.is(Architect.abilityType))
                return 1;
            if(a2.owner.is(Architect.abilityType) && !a1.owner.is(Architect.abilityType))
                return -1;
            return 0;
        }
    };

    public static Action getSimpleAction(Player submitter, AbilityType abilityType, Player... targets) {
        return new Action(submitter, abilityType, new LinkedList<>(), new PlayerList(targets));
    }
}
