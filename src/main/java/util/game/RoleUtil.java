package util.game;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import models.Role;

public class RoleUtil {
    public static Set<String> getNamesSet(Set<Role> roles) {
        Set<String> names = new HashSet<>();
        for(Role role: roles)
            names.add(role.name);
        return names;
    }

    public static ArrayList<String> getNamesList(ArrayList<Role> roles) {
        ArrayList<String> names = new ArrayList<>();
        for(Role role: roles)
            names.add(role.getName());
    
        return names;
    }
}
