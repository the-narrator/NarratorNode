package util.game;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import game.abilities.Hidden;
import game.logic.Game;
import game.logic.exceptions.UnknownTeamException;
import game.setups.Setup;
import models.Ability;
import models.Faction;
import models.FactionRole;
import models.Role;
import models.SetupHidden;
import models.enums.AbilityType;
import models.enums.FactionModifierName;
import models.enums.SetupModifierName;
import models.modifiers.Modifier;

public class LookupUtil {

    public static Hidden findHidden(Setup setup, long hiddenID) {
        for(Hidden hidden: setup.hiddens){
            if(hidden.id == hiddenID)
                return hidden;
        }
        return null;
    }

    public static Optional<Role> findRole(Setup setup, String name) {
        for(Role role: setup.roles)
            if(role.getName().equals(name))
                return Optional.of(role);
        return Optional.empty();
    }

    public static Optional<Role> findRole(Setup setup, long roleID) {
        for(Role role: setup.roles)
            if(role.id == roleID)
                return Optional.of(role);
        return Optional.empty();
    }

    public static Optional<Faction> findFactionByName(Setup setup, String factionName) {
        for(Faction faction: setup.getFactions())
            if(faction.name.equals(factionName))
                return Optional.of(faction);
        return Optional.empty();
    }

    public static Optional<Faction> findFaction(Setup setup, long factionID) {
        for(Faction faction: setup.factions.values()){
            if(faction.id == factionID)
                return Optional.of(faction);
        }
        return Optional.empty();
    }

    public static Optional<Faction> findFactionByColor(Setup setup, String color) {
        for(Faction faction: setup.factions.values()){
            if(faction.color.equals(color))
                return Optional.of(faction);
        }
        return Optional.empty();
    }

    public static Optional<Hidden> findHidden(Setup setup, String name) {
        for(Hidden hidden: setup.hiddens){
            if(hidden.getName().equals(name))
                return Optional.of(hidden);
        }
        return Optional.empty();
    }

    public static Hidden findHidden(Setup setup, String name, String color) {
        for(Hidden hidden: setup.hiddens){
            if(!hidden.getColors().contains(color))
                continue;
            if(!hidden.getName().equals(name))
                continue;
            return hidden;
        }
        return null;
    }

    public static Set<Role> findRolesWithAbility(Setup setup, AbilityType abilityType) {
        Set<Role> rolesWithAbility = new HashSet<>();
        for(Role role: setup.roles)
            if(role.hasAbility(abilityType))
                rolesWithAbility.add(role);
        return rolesWithAbility;
    }

    public static Faction findFactionByAbilityID(Setup setup, long factionAbilityID) {
        for(Faction faction: setup.factions.values()){
            for(Ability ability: faction.abilities){
                if(ability.id == factionAbilityID)
                    return faction;
            }
        }
        return null;
    }

    public static Ability findFactionAbilityByID(Setup setup, long factionAbilityID) {
        for(Faction faction: setup.factions.values()){
            for(Ability ability: faction.abilities){
                if(ability.id == factionAbilityID)
                    return ability;
            }
        }
        return null;
    }

    public static Optional<FactionRole> findFactionRole(Setup setup, String roleName, String color) {
        Optional<Faction> factionOpt = findFactionByColor(setup, color);
        if(!factionOpt.isPresent())
            throw new UnknownTeamException(color + " is not a known faction color.");
        Faction faction = factionOpt.get();
        for(FactionRole factionRole: faction.roleMap.values()){
            if(factionRole.role.getName().equals(roleName))
                return Optional.of(factionRole);
        }
        return Optional.empty();
    }

    public static SetupHidden findSetupHidden(Game game, long setupHiddenID) {
        return game.setup.rolesList.get(setupHiddenID);
    }

    public static FactionRole findFactionRole(Setup setup, long factionRoleID) {
        for(Faction faction: setup.factions.values()){
            for(FactionRole factionRole: faction.roleMap.values()){
                if(factionRole.id == factionRoleID)
                    return factionRole;
            }
        }
        return null;
    }

    public static Modifier<SetupModifierName> findSetupModifier(Game narrator, SetupModifierName name) {
        for(Modifier<SetupModifierName> modifier: narrator.setup.modifiers)
            if(modifier.name == name)
                return modifier;
        return null;
    }

    public static Modifier<FactionModifierName> findFactionModifier(Faction faction, FactionModifierName name) {
        for(Modifier<FactionModifierName> modifier: faction.modifiers){
            if(modifier.name == name)
                return modifier;
        }
        return null;
    }
}
