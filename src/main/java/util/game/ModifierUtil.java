package util.game;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import game.logic.exceptions.NarratorException;
import game.logic.support.Constants;
import game.logic.support.rules.ModifierName;
import game.setups.Setup;
import models.Ability;
import models.ModifierValue;
import models.enums.AbilityModifierName;
import models.enums.AbilityType;
import models.enums.FlavorAlias;
import models.enums.SetupModifierName;
import models.modifiers.Modifier;
import models.modifiers.Modifiers;
import util.Util;

public class ModifierUtil {

    public static <T extends ModifierName> Modifiers<T> mergeModifiers(Optional<Integer> playerCount,
            Modifiers<T> baseModifiers, Modifiers<T> overrideModifiers) {
        if(playerCount.isPresent())
            return mergeModifiers(playerCount.get(), baseModifiers, overrideModifiers);
        return mergeModifiers(baseModifiers, overrideModifiers);
    }

    public static <T extends ModifierName> Modifiers<T> mergeModifiers(int playerCount, Modifiers<T> baseModifiers,
            Modifiers<T> overrideModifiers) {
        Modifiers<T> modifiers = new Modifiers<>();
        for(Modifier<T> modifier: baseModifiers.getInRange(playerCount))
            modifiers.add(modifier);

        // could come from factionRoleModifiers which would be null
        if(overrideModifiers != null)
            for(Modifier<T> modifier: overrideModifiers.getInRange(playerCount)){
                modifiers.removeAll(modifier.name);
                modifiers.add(modifier);
            }
        return modifiers;
    }

    public static <T extends ModifierName> Modifiers<T> mergeModifiers(Modifiers<T> baseModifiers,
            Modifiers<T> overrideModifiers) {
        Modifiers<T> modifiers = new Modifiers<>();

        for(Modifier<T> modifier: baseModifiers)
            modifiers.add(modifier);

        // could come from factionRoleModifiers which would be null
        if(overrideModifiers != null)
            for(Modifier<T> modifier: overrideModifiers)
                modifiers.add(modifier);

        return modifiers;
    }

    public static Map<AbilityType, Modifiers<AbilityModifierName>> mergeAbilityModifiers(int playerCount,
            Map<AbilityType, Ability> abilityMap,
            Map<AbilityType, Modifiers<AbilityModifierName>> factionRoleAbilityModifiers2) {
        Map<AbilityType, Modifiers<AbilityModifierName>> modifierMap = new HashMap<>();

        Modifiers<AbilityModifierName> roleAbilityModifiers;
        Modifiers<AbilityModifierName> mergedModifiers;
        Modifiers<AbilityModifierName> factionRoleAbilityModifiers;
        for(AbilityType abilityType: abilityMap.keySet()){
            roleAbilityModifiers = abilityMap.get(abilityType).modifiers;
            if(factionRoleAbilityModifiers2.containsKey(abilityType)){
                factionRoleAbilityModifiers = factionRoleAbilityModifiers2.get(abilityType);
            }else{
                factionRoleAbilityModifiers = new Modifiers<>();
            }
            mergedModifiers = ModifierUtil.mergeModifiers(playerCount, roleAbilityModifiers,
                    factionRoleAbilityModifiers);

            modifierMap.put(abilityType, mergedModifiers);
        }
        return modifierMap;
    }

    public static Map<AbilityType, Modifiers<AbilityModifierName>> mergeAbilityModifiers(
            Map<AbilityType, Ability> abilityMap,
            Map<AbilityType, Modifiers<AbilityModifierName>> factionRoleAbilityModifiersMap) {
        Map<AbilityType, Modifiers<AbilityModifierName>> modifierMap = new HashMap<>();

        Modifiers<AbilityModifierName> roleAbilityModifiers;
        Modifiers<AbilityModifierName> mergedModifiers;
        Modifiers<AbilityModifierName> factionRoleAbilityModifiers;
        for(AbilityType abilityType: abilityMap.keySet()){
            roleAbilityModifiers = abilityMap.get(abilityType).modifiers;
            if(factionRoleAbilityModifiersMap.containsKey(abilityType)){
                factionRoleAbilityModifiers = factionRoleAbilityModifiersMap.get(abilityType);
            }else{
                factionRoleAbilityModifiers = new Modifiers<>();
            }
            mergedModifiers = ModifierUtil.mergeModifiers(roleAbilityModifiers, factionRoleAbilityModifiers);

            modifierMap.put(abilityType, mergedModifiers);
        }
        return modifierMap;
    }

    public static boolean IsBoolModifier(ModifierName modifierName) {
        return Arrays.asList(ModifierName.BOOL_MODIFIERS).contains(modifierName);
    }

    public static boolean IsStringModifier(ModifierName modifierName) {
        return Arrays.asList(ModifierName.STRING_MODIFIERS).contains(modifierName);
    }

    public static boolean IsIntModifier(ModifierName modifierName) {
        return Arrays.asList(ModifierName.INT_MODIFIERS).contains(modifierName);
    }

    public static void checkModifierValueType(Modifier<?> modifier) {
        ModifierName name = modifier.name;
        Object internalValue = modifier.value.internalValue;

        String expectedType = null;
        if(IsIntModifier(name) && !(internalValue instanceof Integer))
            expectedType = "integer";
        else if(IsBoolModifier(name) && !(internalValue instanceof Boolean))
            expectedType = "boolean";
        else if(IsStringModifier(name) && !(internalValue instanceof String))
            expectedType = "string";

        if(expectedType != null)
            throw new NarratorException("Modifier \"" + name + "\" needs a " + expectedType + " but got "
                    + internalValue.getClass().getSimpleName().toLowerCase() + ".");
    }

    public static void minOutOfBoundsException(ModifierName name, int currentValue, int minValue) {
        if(currentValue < minValue)
            throw new NarratorException("Value for " + name.toString() + "cannot be below " + minValue + ".");
    }

    public static void maxOutOfBoundsException(ModifierName name, int currentValue, int maxValue) {
        if(currentValue > maxValue)
            throw new NarratorException("Value for " + name.toString() + " cannot exceed " + maxValue + ".");
    }

    public static boolean isDefault(Modifier<? extends ModifierName> modifier) {
        return modifier.minPlayerCount == 0 && modifier.maxPlayerCount == Setup.MAX_PLAYER_COUNT;
    }

    public static <T extends ModifierName> String replaceFormats(Optional<Integer> playerCount,
            Modifiers<SetupModifierName> modifiers, String format) {
        String keyFormat;
        String value;
        for(FlavorAlias alias: FlavorAlias.values()){
            keyFormat = "#" + alias.toString() + "#";
            value = modifiers.getString(alias.modifierName, playerCount);
            if(format.indexOf(keyFormat) == 0) // capitalize if first word
                value = Util.TitleCase(value);
            format = format.replaceAll(keyFormat, value);
        }
        return format;
    }

    public static <T extends ModifierName> String getBooleanDescription(Optional<Integer> playerCount,
            Modifiers<SetupModifierName> modifiers, T modifierName, Optional<ModifierValue> modifier) {
        String format;
        if(modifier.isPresent()){
            if(modifier.get().getBoolValue())
                format = modifierName.getActiveFormat();
            else
                format = modifierName.getInactiveFormat();
        }else
            format = modifierName.getUnsureFormat();
        return replaceFormats(playerCount, modifiers, format);
    }

    public static <T extends ModifierName> String getIntDescription(Optional<Integer> playerCount,
            Modifiers<SetupModifierName> setupModifiers, T modifierName, Optional<ModifierValue> modifier) {
        String format;
        if(modifier.isPresent()){
            int value = modifier.get().getIntValue();
            if(value == Constants.UNLIMITED)
                format = modifierName.getUnlimitedFormat();
            else
                format = modifierName.getLimitedFormat().replace("%s", Integer.toString(value));
        }else
            format = modifierName.getUnsureFormat();
        return replaceFormats(playerCount, setupModifiers, format);
    }

    public static <T extends ModifierName> String getTextValue(T modifierName, Optional<Integer> playerCount,
            Modifiers<SetupModifierName> setupModifiers, Modifiers<T> modifiers) {
        Optional<ModifierValue> modifier = modifiers.getIfSingle(modifierName, playerCount);
        if(IsBoolModifier(modifierName))
            return getBooleanDescription(playerCount, setupModifiers, modifierName, modifier);
        return getIntDescription(playerCount, setupModifiers, modifierName, modifier);
    }
}
