package util.game;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import game.abilities.Hidden;
import game.logic.Game;
import game.setups.Setup;
import models.FactionRole;
import models.HiddenSpawn;
import models.enums.GameModifierName;
import services.HiddenService;

public class HiddenUtil {
    public static Hidden createHiddenSingle(Setup setup, FactionRole role) {
        Optional<Hidden> singleHidden = getHiddenSingle(setup, role);
        if(singleHidden.isPresent())
            return singleHidden.get();

        return HiddenService.createHidden(setup, role.role.getName(), role);
    }

    public static Optional<Hidden> getHiddenSingle(Setup setup, FactionRole factionRole) {
        for(Hidden rm: setup.hiddens){
            if(rm.size() != 1)
                continue;
            if(rm.getAllFactionRoles().iterator().next().id == factionRole.id)
                return Optional.of(rm);
        }
        return Optional.empty();
    }

    public static boolean spawnable(Game game, HiddenSpawn hiddenSpawn) {
        int playerCount = game.players.size();
        if(playerCount < hiddenSpawn.minPlayerCount || playerCount > hiddenSpawn.maxPlayerCount)
            return false;
        if(!hiddenSpawn.factionRole.role.isChatRole())
            return true;
        return game.getBool(GameModifierName.CHAT_ROLES);
    }

    public static Set<String> getNames(Set<Hidden> hiddens) {
        Set<String> hiddenNames = new HashSet<>();
        for(Hidden hidden: hiddens)
            hiddenNames.add(hidden.name);
        return hiddenNames;
    }

    public static ArrayList<String> getArrayNames(ArrayList<Hidden> hiddens) {
        ArrayList<String> hiddenNames = new ArrayList<>();
        for(Hidden hidden: hiddens)
            hiddenNames.add(hidden.name);
        return hiddenNames;
    }
}
