package util.game;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import game.logic.Game;
import game.logic.Player;
import game.logic.exceptions.NarratorException;
import game.logic.support.RolePackage;
import models.FactionRole;
import models.SetupHidden;

public class SetupHiddenUtil {
    public static Map<SetupHidden, RolePackage> rolePackageToMap(List<RolePackage> roles) {
        Map<SetupHidden, RolePackage> map = new HashMap<>();
        for(RolePackage rp: roles)
            map.put(rp.assignedSetupHidden, rp);
        return map;
    }

    public static FactionRole getSpawnedFactionRole(Game game, SetupHidden setupHidden) {
        for(Player player: game.players)
            if(player.initialAssignedRole.assignedSetupHidden == setupHidden)
                return player.initialAssignedRole.assignedRole;

        for(RolePackage rp: game.unpickedRoles)
            if(setupHidden == rp.assignedSetupHidden)
                return rp.assignedRole;

        throw new NarratorException("Setup Hidden not found.");
    }

    public static boolean spawnable(int playerCount, SetupHidden setupHidden) {
        return playerCount >= setupHidden.minPlayerCount && playerCount <= setupHidden.maxPlayerCount;
    }
}
