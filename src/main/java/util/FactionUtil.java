package util;

import java.util.Comparator;
import java.util.HashSet;
import java.util.Set;

import models.Faction;

public class FactionUtil {

    public static Set<String> getColors(Set<Faction> factions) {
        Set<String> colors = new HashSet<>();
        for(Faction faction: factions)
            colors.add(faction.color);
        return colors;
    }

    public static Set<String> getNames(Set<Faction> factions) {
        Set<String> colors = new HashSet<>();
        for(Faction faction: factions)
            colors.add(faction.name);
        return colors;
    }

    public static final Comparator<Faction> nameComparator = new Comparator<Faction>() {

        @Override
        public int compare(Faction faction1, Faction faction2) {
            return faction1.name.compareTo(faction2.name);
        }

    };
}
