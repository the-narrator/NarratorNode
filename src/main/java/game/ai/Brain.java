package game.ai;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import game.abilities.FactionKill;
import game.abilities.Ghost;
import game.abilities.Puppet;
import game.abilities.Vote;
import game.logic.DeathType;
import game.logic.Game;
import game.logic.GameFaction;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.exceptions.PhaseException;
import game.logic.support.Constants;
import game.logic.support.Random;
import game.logic.support.action.Action;
import models.FactionRole;
import models.SetupHidden;
import models.enums.GameModifierName;
import models.enums.GamePhase;
import models.enums.VoteSystemTypes;
import models.logic.vote_systems.MultiVotePluralitySystem;
import services.GameService;
import util.game.VoteUtil;

public class Brain {

    public HashMap<Player, Computer> computers = new HashMap<>();

    private ArrayList<Claim> claims = new ArrayList<>();
    HashMap<GameFaction, ArrayList<PlayerList>> suspList = new HashMap<>();
    private double ballsyMaf = 0.99;

    public Random random;
    public PlayerList slaves;

    public Game narrator;

    public Brain(Game n, Random r) {
        this(n, n.getAllPlayers(), r);
        setNarrator(n);
    }

    public Brain(Game narrator, PlayerList slaves, Random random) {
        this.narrator = narrator;
        mafKiller = new HashMap<>();
        mafKillTarget = new HashMap<>();
        this.random = random;

        this.slaves = slaves;
        for(Player p: slaves)
            computers.put(p, new Computer(p, this));
    }

    public Brain(HashMap<Player, Controller> controls, Random rand) {
        this.narrator = slaves.get(0).game;
        mafKiller = new HashMap<>();
        mafKillTarget = new HashMap<>();
        this.random = rand;

        this.slaves = new PlayerList();
        for(Player p: controls.keySet()){
            slaves.add(p);
            computers.put(p, new Computer(p, this, controls.get(p)));
        }
    }

    public Brain setNarrator(Game n) {
        BrainListener bl = new BrainListener(this);
        if(n.isStarted())
            bl.onFirstPhaseStart();
        n.addListener(bl);
        return this;
    }

    public int size() {
        return computers.size();
    }

    public void dayAction() {
        if(slaves.isEmpty() || !slaves.get(0).game.isDay())
            return;
        talkings();
        randomLynch();
        if(narrator.voteSystem.getClass().equals(MultiVotePluralitySystem.class) && this.narrator.isInProgress())
            narrator.endPhase(0);
    }

    private void randomLynch() {
        Player choice = random.getPlayer(slaves.getLivePlayers());
        Brain.Vote(this, choice, "No one's died in a while, so I'll random vote...", Constants.NO_TIME_LEFT);
        if(narrator.getInt(GameModifierName.VOTE_SYSTEM) == VoteSystemTypes.DIMINISHING_POOL.value)
            narrator.endPhase(0);
    }

    private void talkings() {
        for(Player s: slaves.getLivePlayers()){
            computers.get(s).talkings();
        }
    }

    // returns people that voted
//    private ControllerList voting(HashMap<Computer, PlayerList[]> accusedMap) throws ClaimInterrupt{
//    	ControllerList voters = new ControllerList();
//    	if(preconditionVotingChecks(voters))
//    		return voters;
//
//    	Narrator n = slaves.get(0).narrator;
//    	PlayerList livePlayers = n.getLivePlayers();
//
//    	int minVoteToLynch = getMinLynchVote();
//
//    	boolean dub = false;
//    	for(Player p: livePlayers){
//    		int voteCount = p.getVoteCount();
//    		if(voteCount == 0){
//    			continue;
//    		}
//
//    		if(voteCount < minVoteToLynch){//if i find a new low non0
//    			minVoteToLynch = voteCount;
//    			dub = false;
//    		}else if(voteCount == minVoteToLynch){
//    			dub = true;
//    		}
//    	}
//    	PlayerList choices;
//    	PlayerList needToChange;
//
//    	choices = n.getLivePlayers();
//		needToChange = choices.intersect(slaves);
//
//		int voteCount;
//		PlayerList votersOfP;
//    	if(minVoteToLynch != getMinLynchVote() && !claimsIncreased()){
//    		choices = new PlayerList();
//    		needToChange = new PlayerList();
//
//	    	for(Player p: slaves){
//	    		if(p.isDead())
//	    			continue;
//	    		voteCount = p.getVoteCount();
//	    		votersOfP = p.getVoters();
//	    		if(voteCount > minVoteToLynch){
//	    			choices.add(p);
//	    		}else if (voteCount == minVoteToLynch){
//	    			if(dub && minVoteToLynch > 1){
//		    			choices.add(p);
//	    			}
//	    			for(Player toChange: votersOfP)
//	    				if(slaves.contains(toChange))
//	    					needToChange.add(toChange);
//	    		}else{
//	    			for(Player toChange: votersOfP)
//	    				if(slaves.contains(toChange))
//	    					needToChange.add(toChange);
//	    		}
//	    	}
//    	}
//
//    	Computer c = null;
//    	PlayerList playerChoices, shifty, safe;
//    	PlayerList[] processedPeople;
//    	for(Player p: n._players){
//    		c = computers.get(p);
//    		if(c == null)
//    			continue;
//    		processedPeople = accusedMap.get(c);
//    		if(processedPeople == null)
//    			continue;
//    		playerChoices = choices.copy();
//    		shifty = processedPeople[0];
//    		safe = processedPeople[1];
//    		playerChoices.remove(safe);
//    		playerChoices.softIntersect(shifty);
//    		if(playerChoices.isEmpty())
//    			playerChoices = n.getLivePlayers();
//    		if(c.vote(playerChoices) != null){
//    			voters.add(p);
//    			if(c.slave.getVoteTarget() == null)
//    				break;
//    		}
//    		if(!n.isDay())
//    			break;
//    	}
//
//    	return voters;
//    }

    public void nightAction() {
        reset();
        for(Player c: slaves.sortByID()){
            if(c.isEliminated())
                continue;
            Computer comp = computers.get(c);
            if(comp != null && c.game.isNight())
                comp.doNightAction();
        }
        reset();
    }

    private HashMap<String, Player> mafKiller;
    private HashMap<String, Player> mafKillTarget;

    public boolean talkingEnabled = true;

    public void reset() {
        mafKiller.clear();
        mafKillTarget.clear();
        suspList.clear();
    }

    public Player getMafSender(Player slave) {
        Player killer = null;
        for(GameFaction x: slave.getFactions()){
            killer = mafKiller.get(x.getColor());
            if(x.hasMember(slave) && killer != null){
                return killer;
            }
        }

        Optional<GameFaction> optTeam = slave.getFactions().getFactionWithAbility(FactionKill.abilityType);
        if(!optTeam.isPresent())
            return null;

        GameFaction theTeam = optTeam.get();
        for(Player poss: theTeam.getMembers().getLivePlayers()){
            if(poss.getRoleAbilities().getNightAbilities(poss).getNonpassive().filterHasCharge().isEmpty()){
                killer = poss;
                mafKiller.put(theTeam.getColor(), killer);
                return killer;
            }
        }
        killer = random.getPlayer(theTeam.getMembers().getLivePlayers());
        mafKiller.put(theTeam.getColor(), killer);
        return killer;
    }

    public Player getMafKillTarget(Player slave) {
        Player killTarget = mafKillTarget.get(slave.getColor());
        if(killTarget != null)
            return killTarget;

        PlayerList aliveTeammates = slave.getGameFaction().getMembers().getLivePlayers();
        PlayerList alive = slave.getAcceptableTargets(FactionKill.abilityType);

        PlayerList choices = alive.compliment(aliveTeammates);

        PlayerList outedChoices = getHighProfilePeople(slave).getLivePlayers();
        if(outedChoices.isEmpty() || choices.intersect(outedChoices).isEmpty()
                || choices.compliment(outedChoices).isEmpty())
            killTarget = random.getPlayer(choices);
        if(ballsyMaf >= random.nextDouble() && killTarget == null){
            killTarget = random.getPlayer(choices.intersect(outedChoices));
        }else if(killTarget == null){
            killTarget = random.getPlayer(choices.compliment(outedChoices));
        }
        if(!slave.getGameFaction().hasAbility(FactionKill.abilityType))
            killTarget = null;
        else if(slave.getGameFaction().getAbility(FactionKill.abilityType).isOnCooldown())
            killTarget = null;

        // killTarget = random.getPlayer(alive.compliment(aliveTeammates));
        mafKillTarget.put(slave.getColor(), killTarget);
        return killTarget;
    }

    public PlayerList getHighProfilePeople(Player slave) {
        PlayerList profiles = new PlayerList();
        if(mayor != null)
            profiles.add(mayor);
        PlayerList teammates = slave.getGameFaction().getMembers();
        for(Claim c: claims){
            if(!c.getAccused().intersect(teammates).isEmpty()){
                if(!teammates.contains(c.getClaimer()))
                    profiles.add(c.getClaimer());
            }

        }
        return profiles;

    }

    public static void EndGame(Game n, long seed) {
        EndGame(n, seed, null, -1);
    }

    public static boolean EndGame(Game n, long seed, Integer endGameDay, int maximumRunningTime) {
        Random r = new Random();
        r.setSeed(seed);
        r.reset();
        Brain b = new Brain(n, r);
        b.talkingEnabled = false;
        return b.endGame(endGameDay, maximumRunningTime);
    }

    public void endGame() {
        endGame(null, -1);
    }

    public boolean endGame(Integer endDay, int timeToEnd) {
        if(slaves.isEmpty())
            return false;
        Game n = slaves.get(0).game;
        if(!n.isStarted())
            GameService.internalStart(n);// predictable role assignment
        long startTime = System.currentTimeMillis();
        while (n.isInProgress() && (endDay == null || n.getDayNumber() < endDay)
                && (timeToEnd < 0 || timeToEnd >= System.currentTimeMillis() - startTime)){
            if(n.phase == GamePhase.ROLE_PICKING_PHASE)
                n.endPhase(0);
            if(n.isDay()){
                if(n.phase == GamePhase.DISCUSSION_PHASE)
                    n.endPhase(0);
                if(n.phase == GamePhase.TRIAL_PHASE)
                    n.endPhase(0);
                dayAction();
            }else
                nightAction();
        }
        return n.isInProgress();
    }

    public void endDay() {
        if(slaves.isEmpty())
            return;
        Game n = slaves.get(0).game;
        while (n.isDay())
            dayAction();
    }

    public Claim claim(Claim c) {
        if(!c.getClaimer().isSilenced()){
            claims.add(c);
            return c;
        }
        return null;
    }

    public ArrayList<PlayerList> getSuspiciousPeople(Player bot) {

        GameFaction botFaction = bot.getGameFaction();
        ArrayList<PlayerList> ret = suspList.get(botFaction);
        if(ret != null)
            return ret;
        ret = new ArrayList<>();

        Game narrator = bot.game;

        HashMap<Player, Integer> scores = new HashMap<Player, Integer>();
        for(Player p: narrator.getLivePlayers()){
            scores.put(p, 0);
        }

        for(Player dead: narrator.getDeadPlayers()){
            DeathType dt = dead.getDeathType();
            if(!dt.isLynch() || dt.isHidden())
                continue;
            PlayerList lynch = dt.getLynchers().getLivePlayers();
            GameFaction deadTeam = dead.getGameFaction();

            if(botFaction.isEnemy(deadTeam)){// if person voted for my enemy, i think more highly of them
                for(Player lyncher: lynch){
                    increment(scores, lyncher);
                }
                for(Player notLyncher: narrator.getLivePlayers().remove(lynch)){
                    decrement(scores, notLyncher);
                }
            }else{
                for(Player lyncher: lynch){
                    decrement(scores, lyncher);
                }
                for(Player notLyncher: narrator.getLivePlayers().remove(lynch)){
                    increment(scores, notLyncher);
                }
            }
        }
        // this gives me a list of all the different types of ratings that exist
        ArrayList<Integer> ratings = new ArrayList<>();
        for(Integer rating: scores.values()){
            if(!ratings.contains(rating))
                ratings.add(rating);
        }

        if(ratings.size() == 1){
            ret.add(narrator.getLivePlayers());
        }else{

            // put them
            Collections.sort(ratings);
            Collections.reverse(ratings);

            // fill the return
            for(@SuppressWarnings("unused")
            Integer rating: ratings){
                ret.add(new PlayerList());
            }

            for(Player p: narrator.getLivePlayers()){
                Integer rating = scores.get(p);
                int index = ratings.indexOf(rating);
                ret.get(index).add(p);
            }
            suspList.put(botFaction, ret);
        }
        return ret;
    }

    private void increment(HashMap<Player, Integer> map, Player p) {
        map.put(p, map.get(p) + 1);
    }

    private void decrement(HashMap<Player, Integer> map, Player p) {
        map.put(p, map.get(p) - 1);
    }

    public boolean gridlock() {
        int greatDeathDay = 0;
        Game n = slaves.get(0).game;
        for(Player p: n.getDeadPlayers()){
            greatDeathDay = Math.max(greatDeathDay, p.getDeathDay());
        }
        if(n.getDayNumber() - greatDeathDay > 10){
            return true;
        }
        return false;
    }

    Player mayor = null; // TODO DELETE there shouldn't be a singular mayor

    private Set<String> safeColors;

    public Set<String> getSafeColors(Game n) {
        if(safeColors == null)
            resetSafeColors(n);
        if(safeColors.size() == 0)
            resetSafeColors(n);
        return safeColors;
    }

    public Player getRevealedMayor() {
        return mayor;
    }

    public RoleClaim getRoleClaim(Player slave) {
        RoleClaim rc;
        for(Claim c: claims){
            if(c instanceof RoleClaim){
                rc = (RoleClaim) c;
                if(rc.getClaimer() == slave)
                    return rc;
            }

        }
        return null;
    }

    void resetSafeColors(Game n) {
        safeColors = new HashSet<String>();

        HashMap<String, Integer> memberSizes = new HashMap<>();
        for(GameFaction t: n.getFactions()){
            memberSizes.put(t.getColor(), 0);
        }

        for(RoleGroup rt: publicRoles){
            for(String color: rt.getColors())
                memberSizes.put(color, memberSizes.get(color) + 1);
        }

        Set<GameFaction> teams = n.getFactions();

        int rating;
        for(GameFaction t1: teams){
            if(memberSizes.get(t1.getColor()) == 0)
                continue;
            rating = 0;
            for(GameFaction t2: teams){
                if(t1.isEnemy(t2))
                    rating -= memberSizes.get(t2.getColor());
                else
                    rating += memberSizes.get(t2.getColor());
            }

            if(rating >= 0)
                safeColors.add(t1.getColor());
        }
    }

    public RoleGroupList publicRoles;

    void setPublicRoles(Game game) {
        publicRoles = new RoleGroupList();
        for(SetupHidden setupHidden: game.setup.rolesList.iterable(game))
            publicRoles.add(new RoleGroup().add(setupHidden.hidden));
    }

    public void crossOff(PlayerList pList) {
        if(pList != null)
            for(Player player: pList)
                publicRoles.crossOff(player);
        for(Computer computer: computers.values()){
            if(computer.player.isAlive())
                computer.setRolesList();
        }
    }

    int lastDeath = -1;

    public int count(FactionRole a) {
        int count = 0;
        for(RoleGroup roleGroup: publicRoles){
            if(roleGroup.contains(a))
                count++;
        }
        return count;
    }

    public void prettyPrintRoles() {

    }

//	public boolean isAtHammer(Player slave, Player choice) {
//		int minLynchVote = getMinLynchVote();
//		int votes = choice.getVoteCount();
//		if(choice.getVoters().contains(slave)){
//			return false;
//		}
//		return votes + slave.getVotePower() >= minLynchVote;
//	}
    public ArrayList<Claim> getClaims() {
        ArrayList<Claim> claims = new ArrayList<>();
        for(Claim c: this.claims){
            claims.add(c);
        }
        return claims;
    }

    public static void Vote(Brain brain, Player target, String message, double timeLeft) {
        Computer c;
        int day = target.game.getDayNumber();
        for(Player p: brain.slaves.getLivePlayers()){
            if(target == p)
                continue;
            if(p.game.isNight())
                break;
            if(day != target.game.getDayNumber())
                break;
            if(target.isDead())
                break;
            c = brain.computers.get(p);
            if(c == null)
                continue;
            PlayerList voteTargets = c.player.game.voteSystem.getVoteTargets(c.player);
            if(c.player.isDisenfranchised()){
                c.controller.doDayAction(VoteUtil.skipAction(c.player, timeLeft));
                continue;
            }
            if(c.player.isPuppeted() && brain.computers.get(c.player.getPuppeteer()) != null){
                Computer ventro = brain.computers.get(c.player.getPuppeteer());
                if(message != null && brain.talkingEnabled)
                    ventro.say(message, c.player.getName());
                if(!voteTargets.contains(target)){
                    Action action = Puppet.getAction(ventro.player, Vote.getAction(c.player, p.game, target));
                    ventro.controller.doDayAction(action);
                }
            }else{
                if(message != null && brain.talkingEnabled && !c.player.isSilenced())
                    c.say(message, Constants.DAY_CHAT);
                if(!voteTargets.contains(target)){
                    Action action = Vote.getAction(c.player, timeLeft, p.game, target);
                    c.controller.doDayAction(action);
                }
            }
        }
    }

    public static void SkipDay(Brain b, Game narrator, double timeLeft) {
        if(narrator.isDay()){
            for(Player slave: b.slaves){
                if(!slave.game.isDay())
                    return;// voters;
                if(slave.isDead() && slave.is(Ghost.abilityType) && slave.hasPuppets()){
                    b.computers.get(slave).skipVoteAssist(timeLeft);
                    continue;
                }
                if(slave.isDead())
                    continue;
                b.computers.get(slave).skipVoteAssist(timeLeft);

            }
        }
        if(narrator.isDay()){
            throw new PhaseException();
        }
    }

}
