package game.ai;

import game.abilities.Thief;
import game.event.DeathAnnouncement;
import game.event.EventList;
import game.event.Feedback;
import game.event.Message;
import game.logic.Game;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.templates.PartialListener;

public class BrainListener extends PartialListener {

    private Brain brain;
    private Game game;

    public BrainListener(Brain brain) {
        this.brain = brain;
        this.game = brain.narrator;
    }

    // not an Override function
    public void onFirstPhaseStart() {
        brain.setPublicRoles(game);
        for(Computer c: brain.computers.values()){
            if(c.player.isAlive())
                c.setRolesList();
        }
    }

    @Override
    public void onAssassination(Player assassin, Player target, DeathAnnouncement e) {
        if(target == null)
            return;
        brain.crossOff(e.dead);
        brain.resetSafeColors(game);
        brain.lastDeath = game.getDayNumber();
    }

    @Override
    public void onDayBurn(Player arson, PlayerList burned, DeathAnnouncement e) {
        brain.crossOff(burned);
        brain.resetSafeColors(game);
        if(burned != null && !burned.isEmpty())
            brain.lastDeath = game.getDayNumber();
    }

    @Override
    public void onDayPhaseStart(PlayerList newDead) {
        if(game.isFirstPhase() && !game.setup.rolesList.contains(Thief.abilityType, game.players.size()))
            onFirstPhaseStart();

        brain.crossOff(newDead);
        brain.resetSafeColors(game);
        if(newDead != null && !newDead.isEmpty())
            brain.lastDeath = game.getDayNumber();
    }

    @Override
    public void onElectroExplosion(PlayerList deadPeople, DeathAnnouncement explosion) {
        brain.crossOff(deadPeople);
        brain.resetSafeColors(game);
        brain.lastDeath = game.getDayNumber();
    }

    @Override
    public void onGameStart() {
        for(Player player: game.getAllPlayers()){
            if(!brain.computers.containsKey(player)){
                brain.slaves.add(player);
                brain.computers.put(player, new Computer(player, brain));
            }
        }
    }

    @Override
    public void onMessageReceive(Player player, Message message) {
        if(brain.computers.containsKey(player) && message instanceof Feedback)
            brain.computers.get(player).feedbackMessages.add((Feedback) message);
    }

    @Override
    public void onModKill(PlayerList bad) {
        brain.crossOff(bad);
        brain.resetSafeColors(game);
    }

    @Override
    public void onNightPhaseStart(PlayerList lynched, PlayerList poisoned, EventList e) {
        if(game.isFirstPhase() && !game.setup.rolesList.contains(Thief.abilityType, this.game.players.size()))
            onFirstPhaseStart();

        brain.crossOff(lynched);
        brain.resetSafeColors(game);
        if(lynched != null && !lynched.isEmpty() && !lynched.contains(game.skipper))
            brain.lastDeath = game.getDayNumber();
    }

    @Override
    public void onRolepickingPhaseStart() {
        onFirstPhaseStart();
    }
}
