package game.ai;

import game.abilities.Sheriff;
import game.logic.FactionList;
import game.logic.GameFaction;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.support.Constants;
import models.Faction;

public class SheriffClaim extends Claim {

    private PlayerList accused;
    private FactionList teams;

    public SheriffClaim(Player prosecutor, PlayerList targets, FactionList t, Brain b) {
        super(prosecutor, b);
        this.accused = targets;
        teams = new FactionList();
        if(t != null)
            teams.add(t);
        else{
            GameFaction sheriffTeam = prosecutor.getGameFaction();
            for(GameFaction friend: prosecutor.game.getFactions()){
                if(!sheriffTeam.isEnemy(friend))// if sheriff team is not an enemy of this team
                    teams.add(friend.faction);
            }
        }
    }

    @Override
    public PlayerList getAccused() {
        return accused.copy();
    }

    @Override
    public boolean believable(Computer computer) {
        if(accused.getLivePlayers().isEmpty())// no one left in the claim to accuse
            return false;

        RoleGroupList livingRoles = computer.rolesList;
        return livingRoles.hasTeam(teams);
    }

    @Override
    public boolean outlandish(Computer c) {
        if(!accused.getLivePlayers().isEmpty())// if there are some people still alive
            return false;
        for(Faction t: teams)
            for(Player p: accused)
                if(p.getGameFaction().getColor().equals(t.getColor()))
                    return false;
        return true;
    }

    @Override
    public boolean valid(RoleGroupList rolesList, Computer c) {
        if(c.player.equals(getClaimer()))
            return true;

        if(!rolesList.contains(Sheriff.abilityType))
            return false;
        return true;
    }

    @Override
    public void talkAboutBadClaim(Computer c) {
        if(c.player.isPuppeted() || c.player.isSilenced())
            return;
        StringBuilder sb = new StringBuilder();
        sb.append("I don't believe " + getClaimer().getName() + "'s claim that " + accused.getStringName()
                + " is part of the ");
        if(teams.size() == 1)
            sb.append(teams.get(0).name);
        else{
            Faction lastTeam = teams.get(teams.size() - 1);
            for(Faction t: teams){
                if(t == lastTeam){
                    sb.deleteCharAt(sb.length() - 1);
                    sb.append(" or ");
                }
                sb.append(t.name);
                sb.append(", ");
            }
            sb.deleteCharAt(sb.length() - 1);
            sb.deleteCharAt(sb.length() - 1);
            sb.append('.');
        }
        c.say(sb.toString(), Constants.DAY_CHAT);

    }

    @Override
    public PlayerList getPlayersToActOn(Computer computer) {
        if(outlandish(computer))
            return new PlayerList(getClaimer());
        int allies = 0;
        for(Faction faction: teams){
            if(!faction.enemies.contains(computer.player.getGameFaction().faction))
                allies++;
        }
        if(allies >= teams.size())
            return new PlayerList();
        return getAccused();
    }

    @Override
    public PlayerList getSafePlayers(Computer computer) {
        // if(!believable(computer))
        return new PlayerList();

    }
}
