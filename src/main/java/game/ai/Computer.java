package game.ai;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import game.abilities.Amnesiac;
import game.abilities.Block;
import game.abilities.BreadAbility;
import game.abilities.Burn;
import game.abilities.Detective;
import game.abilities.Douse;
import game.abilities.Driver;
import game.abilities.DrugDealer;
import game.abilities.Executioner;
import game.abilities.FactionKill;
import game.abilities.FactionSend;
import game.abilities.Framer;
import game.abilities.GameAbility;
import game.abilities.JailCreate;
import game.abilities.Janitor;
import game.abilities.Jester;
import game.abilities.Lookout;
import game.abilities.Mayor;
import game.abilities.ProxyKill;
import game.abilities.Puppet;
import game.abilities.SerialKiller;
import game.abilities.Sheriff;
import game.abilities.Spy;
import game.abilities.Survivor;
import game.abilities.Tailor;
import game.abilities.Undouse;
import game.abilities.Veteran;
import game.abilities.Vigilante;
import game.abilities.Vote;
import game.ai.roles.AIStripper;
import game.ai.roles.AIWitch;
import game.event.DayChat;
import game.event.Feedback;
import game.logic.FactionList;
import game.logic.Game;
import game.logic.GameFaction;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.exceptions.PlayerTargetingException;
import game.logic.support.Constants;
import game.logic.support.Option;
import game.logic.support.action.Action;
import game.logic.support.action.ActionList;
import json.JSONObject;
import models.Faction;
import models.FactionRole;
import models.enums.AbilityType;
import models.enums.SetupModifierName;
import util.Util;
import util.game.RandomUtil;
import util.game.VoteUtil;

public class Computer {

    public static final String NAME = "Slave";

    public Player player;
    public Brain brain;
    public Controller controller;
    protected Game narrator;

    public List<Claim> badClaims;
    public List<Feedback> feedbackMessages;

    public Computer(Player slave, Brain brain, Controller controller) {
        this.player = slave;
        this.brain = brain;
        this.controller = controller;
        this.narrator = slave.game;
        memory = new Memory();
        badClaims = new ArrayList<>();
        feedbackMessages = new LinkedList<>();
    }

    public Computer(Player slave, Brain brain) {
        this(slave, brain, slave);
    }

    public static final String STRIPPER = Block.class.getSimpleName();
    public static final String WITCH = Block.class.getSimpleName();

    public boolean doRoleNightAction(AbilityType abilityType) {
        switch (abilityType) {
        case Block:
            AIStripper.doRoleNightAction(this);
            return true;
        case Witch:
            AIWitch.doRoleNightAction(this);
            return true;
        default:
            return false;
        }
    }

    public boolean useVest() {
        return false;
    }

    public boolean useGun() {
        return false;
    }

    public boolean mafKilling() {
        if(brain.getMafSender(player) != player)
            return false;
        PlayerList pl = player.game.getAllPlayers();
        pl.remove(player);
        if(pl.isEmpty())
            return false;
        for(Player player: player.getGameFaction().getMembers()){
            if(pl.size() == 1)
                break;
            pl.remove(player);
        }
        mafKillAbility(FactionKill.abilityType, pl);
        return true;
    }

    public PlayerList getFactionmates() {
        GameFaction faction = player.getGameFaction();
        if(!faction.knowsTeam())
            return new PlayerList();
        return faction.getMembers();

    }

    public void doNightAction() {
        submitNightTargets();
        controller.endNight(Constants.NO_TIME_LEFT);
    }

    public void say(String message, String key) {
        this.controller.say(message, key, Optional.empty(), new JSONObject());
    }

    private void submitNightTargets() {
        if(player.endedNight())
            controller.cancelEndNight(Constants.NO_TIME_LEFT);

        if(player.isJailed())
            return;

        if(useGun())
            return;

        if(useVest())
            return;

        if(mafKilling())
            return;

        for(AbilityType command: player.getCommands())
            if(doRoleNightAction(command))
                return;

        // if(slave.getTeam().knowsTeam() && !slave.isBlackmailed())
        // controller.say(slave, "hi", slave.getTeam().getName());
        PlayerList selections = player.game.getLivePlayers();

        ArrayList<AbilityType> abilityTypes = player.getCommands();

        // Collections.reverse(commands);

        for(AbilityType abilityType: abilityTypes){
            if(player.getAbility(abilityType) == null)
                player.getAbility(abilityType);

            if(!player.getAbility(abilityType).isNightAbility(player))
                continue;

            if(player.getAbility(abilityType).getPerceivedCharges() == 0)
                continue;

            if(mafSendAbility(abilityType))
                continue;

            if(!player.getActions().canAddAnotherAction(player.getAbility(abilityType).getAbilityType()))
                break;

            if(arsonAbility(abilityType, selections))
                continue;

            if(janitorAbility(abilityType))
                continue;
            if(proxyKillingAbility(abilityType))
                continue;
            if(driverAbility(abilityType))
                continue;
            if(tailorAbility(abilityType, narrator))
                continue;

            if(skAbility(abilityType))
                continue;

            // }
            if(Survivor.abilityType == abilityType){
                controller.setNightTarget(new Action(player, Survivor.abilityType));
                continue;
            }

            if(vigiAbility(abilityType))
                continue;

            if(mafKillAbility(abilityType, selections))
                continue;

            Player choice;

            if(player.is(Veteran.abilityType) && Veteran.abilityType == abilityType){
                if(player.getAbility(Veteran.abilityType).getPerceivedCharges() != 0)
                    controller.setNightTarget(new Action(player, Veteran.abilityType));
                return;
            }else if(player.is(Spy.abilityType) && Spy.abilityType == abilityType){
                choice = player;
            }else if(player.is(Amnesiac.abilityType) && Amnesiac.abilityType == abilityType)
                choice = getTargetChoice(abilityType, selections.getDeadPlayers());
            else
                choice = getTargetChoice(abilityType, selections);

            if(choice == null)
                continue;

            if(framerMainAbility(abilityType, choice))
                continue;

            if(drugDealerMainAbility(abilityType, choice))
                continue;

            if(spyMainAbility(abilityType))
                continue;

            try{
                controller.setNightTarget(new Action(player, abilityType, choice));
            }catch(PlayerTargetingException e){
                e.printStackTrace();
            }
        }
        arsonAction = false;
    }

    private boolean arsonAction = false;

    private boolean arsonAbility(AbilityType abilityType, PlayerList choices) {
        if(!player.is(Burn.abilityType) || player.is(Undouse.abilityType) || player.is(Douse.abilityType))
            return false;

        boolean arsonAbility = Burn.abilityType == abilityType || Douse.abilityType == abilityType
                || Undouse.abilityType == abilityType;

        // if arson already performed an arson action and this is an arson ability
        if(arsonAction)
            return arsonAbility;

        // if arson didn't perform their action yet and this isn't an arson ability
        if(!arsonAbility)
            return false;

        arsonAction = true;
        Player player = controller.getPlayer();

        int choice = brain.random.nextInt(10);
        if(choice == 0 && !narrator.isFirstNight())
            controller.setNightTarget(new Action(player, Undouse.abilityType, player));
        else if(choice <= 3 && player.isAcceptableTarget(new Action(player, Burn.abilityType, player))
                && !narrator.isFirstNight()){
            controller.setNightTarget(new Action(player, Burn.abilityType));
            // lastBurned = narrator.getDayNumber();
        }else{
            PlayerList prevDouses = new PlayerList();
            for(int i = 0; i < player.game.getDayNumber(); i++){
                ActionList prevs = player.getPrevNightTarget(i);
                if(prevs == null)
                    continue;
                PlayerList prevD = prevs.getTargets(Douse.abilityType);
                if(prevD != null)
                    prevDouses.add(prevD);
            }
            choices = choices.copy().remove(player);
            choices.remove(prevDouses);
            if(choices.isEmpty()){
                controller.setNightTarget(new Action(player, Burn.abilityType));
                // lastBurned = narrator.getDayNumber();
            }else
                controller.setNightTarget(new Action(player, Douse.abilityType, brain.random.getPlayer(choices)));
        }
        return true;
    }
    // private int lastBurned = 0;

    private boolean framerMainAbility(AbilityType abilityType, Player choice) {
        if(!player.is(Framer.abilityType) || Framer.abilityType != abilityType)
            return false;

        Set<GameFaction> factions = narrator.getFactions();
        String teamName = RandomUtil.getElem(brain.random, factions).getName();

        controller.setNightTarget(new Action(player, Framer.abilityType, teamName, choice));
        return true;
    }

    private boolean drugDealerMainAbility(AbilityType abilityType, Player choice) {
        if(player.is(DrugDealer.abilityType) && DrugDealer.abilityType == abilityType){
            controller.setNightTarget(new Action(player, DrugDealer.abilityType, DrugDealer.WIPE, choice));
            return true;
        }
        return false;
    }

    private boolean spyMainAbility(AbilityType abilityType) {
        if(player.is(Spy.abilityType) && Spy.abilityType == abilityType){
            for(Option t: player.getOptions().get(abilityType)){
                controller.setNightTarget(
                        new Action(player, Spy.abilityType, Util.toStringList(t.getValue()), Player.list()));
                return true;
            }
            return true;
        }
        return false;
    }

    private boolean vigiAbility(AbilityType abilityType) {
        if(Vigilante.abilityType == abilityType){
            PlayerList choices = player.game.getLivePlayers().remove(player);
            choices.remove(brain.getRevealedMayor());
            if(player.game.isNight() && player.getGameFaction().knowsTeam())
                choices.remove(player.getGameFaction().getMembers());

            PlayerList temp;
            ArrayList<PlayerList> suspicious = brain.getSuspiciousPeople(player);// returned to you by the least
                                                                                 // suspicious people to most
            for(PlayerList susp: suspicious){
                if(choices.size() <= 1)
                    break;
                temp = choices.copy();
                temp.remove(susp);
                if(!temp.isEmpty())
                    choices = temp;
            }
            if(!choices.isEmpty())
                controller.setNightTarget(new Action(player, Vigilante.abilityType, brain.random.getPlayer(choices)));
            return true;
        }
        return false;
    }

    private boolean skAbility(AbilityType abilityType) {
        if(player.is(SerialKiller.abilityType) && SerialKiller.abilityType == abilityType){
            PlayerList choices = player.game.getLivePlayers().remove(player);
            if(player.getGameFaction().knowsTeam())
                choices.remove(player.getGameFaction().getMembers());
            controller.setNightTarget(new Action(player, SerialKiller.abilityType, brain.random.getPlayer(choices)));
            return true;
        }
        return false;
    }

    private boolean driverAbility(AbilityType abilityType) {
        if(player.is(Driver.abilityType) && Driver.abilityType == abilityType){
            PlayerList choices = player.getAcceptableTargets(Driver.abilityType);
            Player p1 = brain.random.getPlayer(choices);
            Player p2 = brain.random.getPlayer(choices.remove(p1));
            if(p1 != null && p2 != null)
                controller.setNightTarget(new Action(player, Driver.abilityType, p1, p2));
            return true;
        }
        return false;
    }

    private boolean proxyKillingAbility(AbilityType abilityType) {
        if(player.is(ProxyKill.abilityType) && ProxyKill.abilityType == abilityType){
            PlayerList choices = player.game.getLivePlayers();
            Player p1 = player;
            Player p2 = brain.random.getPlayer(choices.remove(player.getGameFaction().getMembers()));
            if(p1 != null && p2 != null)
                controller.setNightTarget(new Action(player, ProxyKill.abilityType, p1, p2));
            return true;
        }
        return false;
    }

    private boolean tailorAbility(AbilityType abilityType, Game narrator) {
        if(player.is(Tailor.abilityType) && Tailor.abilityType == abilityType){
            PlayerList choices = player.game.getLivePlayers();
            if(!player.getAbility(Tailor.class).canSelfTarget())
                choices.remove(player);
            Player p1 = brain.random.getPlayer(choices);
            if(p1 != null)
                controller.setNightTarget(new Action(player, Tailor.abilityType, player.getInitialColor(),
                        player.gameRole.factionRole.getName(), p1));
            return true;
        }
        return false;
    }

    private boolean janitorAbility(AbilityType abilityType) {
        if(Janitor.abilityType == abilityType && player.hasAbility(abilityType)){
            if(brain.getMafSender(player) != player){
                Player killTarget = brain.getMafKillTarget(player);
                if(killTarget == null)
                    return true;
                if(killTarget.isDead()){
                    brain.reset();
                    killTarget = brain.getMafKillTarget(player);
                    if(killTarget == null || killTarget.isDead())
                        return true;
                }
                controller.setNightTarget(new Action(player, Janitor.abilityType, killTarget));
            }
            return true;
        }
        return false;
    }

    private boolean mafSendAbility(AbilityType abilityType) {
        if(FactionSend.abilityType == abilityType){
            Player sender = brain.getMafSender(player);
            if(sender == null || sender.isDead())
                brain.reset();
            sender = brain.getMafSender(player);
            if(sender == player || !player.isSilenced() && sender != null){ // if not blackmailed, they can choose
                                                                            // whoever they want. if they are, they'll
                                                                            // shut up if its not themselves
                controller.setNightTarget(new Action(player, FactionSend.abilityType, sender));

            }
            return true;
        }

        return false;
    }

    private boolean mafKillAbility(AbilityType abilityType, PlayerList selections) {
        if(FactionKill.abilityType == abilityType && player.hasAbility(abilityType)){
            selections = selections.copy();
            selections.remove(player.getGameFaction().getMembers());
            if(brain.getMafSender(player) != player)
                return false;

            Player killTarget = brain.getMafKillTarget(player);
            if(killTarget == null)
                return true;

            if(killTarget.isDead()){
                brain.reset();
                killTarget = brain.getMafKillTarget(player);
            }

            // stops situation where disguiser is part of the team, and mafia don't know who
            // to kill.
            int dayNumber = player.game.getDayNumber();
            ActionList prevTargets = player.getPrevNightTarget(dayNumber - 1);
            ActionList prevprevTargets = player.getPrevNightTarget(dayNumber - 2);
            if(prevTargets != null && prevprevTargets != null){
                if(prevTargets.isTargeting(killTarget, FactionKill.abilityType)
                        && prevprevTargets.isTargeting(killTarget, FactionKill.abilityType))
                    killTarget = player.game.getLivePlayers().remove(player).getRandom(brain.random);
            }

            controller.setNightTarget(new Action(player, FactionKill.abilityType, killTarget));
            if(!player.isSilenced() && brain.talkingEnabled)
                say("I will kill " + killTarget.getName(), player.getGameFaction().getColor());

        }
        return true;

    }

    private Player getTargetChoice(AbilityType abilityType, PlayerList possible) {
        GameAbility a = player.getAbility(abilityType);
        if(a == null || a.getPerceivedCharges() == 0)
            return null;
        Player selection = brain.random.getPlayer(possible);
        if(selection == null)
            return null;

        List<String> arg;
        ArrayList<Option> options = player.getOptions(abilityType);
        if(options.isEmpty())
            arg = new LinkedList<>();
        else
            arg = Util.toStringList(options.get(0).getValue());

        AbilityType abilityNumber = player.getAbility(abilityType).getAbilityType();
        if(player.isAcceptableTarget(new Action(player, abilityNumber, arg, Player.list(selection)))){
            selection = player.game.getPlayerByName(selection.getName());
            return selection;
        }
        return getTargetChoice(abilityType, possible.remove(selection));
    }

    private boolean noPrevNight() {
        Game n = player.game;
        if(n.getDayNumber() == 1 && n.getBool(SetupModifierName.DAY_START))
            return true;
        return false;
    }

    void talkings() {
        if(player.is(Mayor.abilityType) && player.getAbility(Mayor.abilityType).canUseDuringDay(player)){
            controller.doDayAction(new Action(player, Mayor.abilityType));
            brain.mayor = player;
        }
        if(player.is(JailCreate.abilityType) && !player.isPuppeted()
                && player.getAbility(JailCreate.class).charges.hasCharges()){
            int jailedPeople = 0;
            int useableBread = BreadAbility.getUseableBread(player);
            for(Player target: player.getAcceptableTargets(JailCreate.abilityType).shuffle(brain.random, null)){
                if(jailedPeople >= useableBread + 1){
                    break;
                }
                controller.doDayAction(new Action(player, JailCreate.abilityType, target));
                jailedPeople++;
            }
        }
        if(player.isSilenced() || player.isPuppeted())
            return;
        if(noPrevNight())
            return;
        if(player.is(Detective.abilityType))
            detectiveTalk();
        else if(player.is(Lookout.abilityType))
            lookoutTalk();
        else if(player.is(Sheriff.abilityType)){
            sheriffTalk();
        }else if(player.is(Executioner.abilityType)){
            exeuctionerTalk();
        }else if(player.is(Jester.abilityType)){
            jesterTalk();
        }

    }

    private void fakeSheriffClaim(Player target) {
        Set<Faction> checked;
        Iterator<Faction> iterator;
        int random;
        for(Faction t: narrator.setup.getPossibleFactionsWithRolesWithAbility(narrator, Sheriff.abilityType)){
            checked = t.sheriffCheckables;
            if(checked.isEmpty())
                continue;
            random = brain.random.nextInt(checked.size());
            iterator = checked.iterator();
            while (random != 0){
                iterator.next();
                random--;
            }
            sheriffClaim(new PlayerList(target), iterator.next());
            return;
        }
    }

    private void exeuctionerTalk() {
        Executioner ex = player.getAbility(Executioner.class);
        Player target = ex.getTarget();
        if(target.isDead())
            return;
        fakeSheriffClaim(target);
    }

    private void jesterTalk() {
        Player randAccused = brain.random.getPlayer(player.game.getLivePlayers());
        fakeSheriffClaim(randAccused);
    }

    private void sheriffClaim(PlayerList targets, Faction team) {
        sheriffClaim(targets, new FactionList(team));
    }

    private void sheriffClaim(PlayerList targets, FactionList team) {
        if(player.isSilenced() || player.isPuppeted())
            return;

        String say = "I'm " + player.getRoleName() + ". ";
        say = say + targets.getStringName() + " is ";
        if(team == null)
            say += "not suspicious.";
        else
            say += "part of the " + team.getName() + ".";
        if(brain.talkingEnabled)
            say(say, DayChat.KEY);

        SheriffClaim sc = new SheriffClaim(player, targets, team, brain);
        brain.claim(sc);
    }

    private List<Feedback> getFeedback() {
        return this.feedbackMessages;
    }

    private void detectiveTalk() {
        int lastNight = player.game.getDayNumber() - 1;
        Game game = player.game;
        for(Feedback e: getFeedback()){
            if(e.access(player).startsWith(Detective.FEEDBACK)){
                Player target = e.getPlayers().get(0);
                if(target.isAlive())
                    continue;
                if(target.getDeathType().isLynch())
                    continue;
                ArrayList<String[]> attacks = target.getDeathType().getList();
                if(isEvilAttack(attacks)){
                    PlayerList followed = player.getPrevNightTarget(lastNight).getTargets(Detective.abilityType);
                    String say = "I'm Detective. " + followed.getStringName() + " may have killed " + target.getName();
                    if(brain.talkingEnabled)
                        say(say, DayChat.KEY);
                    ArrayList<Faction> teams = new ArrayList<>();
                    for(String[] attack: attacks){
                        if(attack == Constants.FACTION_KILL_FLAG){
                            for(Faction t: game.setup.getPossibleFactions(game)){
                                if(t.hasAbility(FactionKill.abilityType))
                                    teams.add(t);
                            }
                        }else if(attack == Constants.SK_KILL_FLAG){
                            for(Faction skTeam: getSKTeams()){
                                teams.add(skTeam);
                            }
                        }
                    }
                    brain.claim(new DetectiveClaim(player, followed, teams, brain));
                }
            }
        }
    }

    private boolean isEvilAttack(ArrayList<String[]> attacks) {
        for(GameFaction t: player.game.getFactions()){
            if(t.hasAbility(FactionKill.abilityType) && attacks.contains(Constants.FACTION_KILL_FLAG)){
                return true;
            }
        }
        if(attacks.contains(Constants.SK_KILL_FLAG))
            return true;
        return false;
    }

    private Set<Faction> getSKTeams() {
        return narrator.setup.getPossibleFactionsWithRolesWithAbility(narrator, SerialKiller.abilityType);
    }

    private void lookoutTalk() {
        int lastNight = player.game.getDayNumber() - 1;
        Game game = player.game;
        for(Feedback e: getFeedback()){
            if(e.access(player).startsWith(Lookout.FEEDBACK)){
                PlayerList watchedTargets = player.getPrevNightTarget(lastNight).getTargets(Lookout.abilityType);

                if(!watchedTargets.hasDead())
                    continue;
                // if (watched.getDeathType().isLynch())
                // continue;
                ArrayList<String[]> attacks = new ArrayList<>();
                for(Player watched: watchedTargets)
                    if(watched.isDead())
                        attacks.addAll(watched.getDeathType().getList());
                if(isEvilAttack(attacks)){

                    PlayerList possibleAttackers = e.getPlayers();

                    for(Player dead: watchedTargets){
                        if(dead.isAlive())
                            continue;
                        String say = "I'm Lookout. " + possibleAttackers.getStringName() + " may have killed "
                                + dead.getName();
                        if(brain.talkingEnabled)
                            say(say, DayChat.KEY);

                        ArrayList<Faction> teams = new ArrayList<>();
                        for(String[] attack: attacks){
                            if(attack == Constants.FACTION_KILL_FLAG){
                                for(Faction t: game.setup.getPossibleFactions(game)){
                                    if(t.hasAbility(FactionKill.abilityType))
                                        teams.add(t);
                                }
                            }else if(attack == Constants.SK_KILL_FLAG){
                                for(Faction skTeam: getSKTeams()){
                                    teams.add(skTeam);
                                }
                            }
                        }
                        if(!teams.isEmpty())
                            brain.claim(new LookoutClaim(player, possibleAttackers, teams, brain));
                    }
                }
            }
        }
    }

    private void sheriffTalk() {
        int lastNight = player.game.getDayNumber() - 1;

        PlayerList targets = player.getPrevNightTarget(lastNight).getTargets(Sheriff.abilityType).getLivePlayers();
        if(targets.isEmpty())
            return;

        String color;
        for(Feedback e: getFeedback()){
            String s = e.access(player).replace("\n", "");
            if(s.startsWith("Your target")){
                boolean friendly = s.startsWith(Sheriff.NOT_SUSPICIOUS);
                FactionList tl;
                if(friendly)
                    tl = null;
                else if(e.getHTStrings().isEmpty()){
                    tl = new FactionList();
                    for(Faction team: player.getGameFaction().faction.enemies)
                        tl.add(team);
                }else{
                    color = e.getHTStrings().get(0).getColor();
                    tl = new FactionList(player.game.setup.getFactionByColor(color));
                }
                sheriffClaim(targets, tl);
            }

        }
    }

//	public Controller vote(PlayerList choices) throws ClaimInterrupt{
//    	if (slave.isDead() && !slave.is(Ghost.class))
//    		return null;
//
//        if(slave.isDead() && slave.is(Ghost.class) && !slave.hasPuppets())
//        	return null;
//
//		Narrator n = slave.narrator;
//    	if (n.isNight() || !n.isInProgress())
//            return null;
//
//        //arson killed himself
//        if (slave.isDead() && !slave.is(Ghost.class))
//            return null;
//
//        if(slave.isDead() && slave.hasPuppets()){
//        	choices.remove(slave.getPuppets().getFirst());
//        }
//
//		if(slave.isPuppeted())
//			return null;
//		if(slave.isBlackmailed()){
//			if(slave.getVoteTarget() == slave.getSkipper())
//				return null;
//			return skipVoteAssist();
//		}
//		choices = choices.copy();
//		choices.remove(slave);
//
//		if(choices.isEmpty()){
//			if(slave.getVoteTarget() == slave.getSkipper())
//				return null;
//			if(brain.talkingEnabled)
//				controller.say("I don't have anyone to vote for, so I'm skipping the vote", Constants.DAY_CHAT);
//			skipVoteAssist();
//			Player puppet;
//			for(int i = 0; i < slave.getPuppets().size() && slave.narrator.isDay(); i++){
//				puppet = slave.getPuppets().get(i);
//				if(puppet.getVoteTarget() != slave.getSkipper())
//					controller.ventSkipVote(slave.getPuppets().get(i));
//			}
//			return null;
//		}
//
//		//if(choices.size() < 3)
//			//controller.say(slave, "These are my choices" + choices , Constants.PUBLIC);
//
//
//		if(!brain.gridlock()){
//			if (slave.getTeam().knowsTeam()){
//				choices.softRemove(slave.getTeam().getMembers());
//			}
//
//			ArrayList<PlayerList> suspicious = brain.getSuspiciousPeople(slave);//returned to you by the least suspicious people to most
//			if(suspicious.get(0).size() == n.getLiveSize() && slave.getVoters().size() >= 2){
//				choices.softIntersect(slave.getVoters());
//			}else{
//				for(PlayerList susp: suspicious){
//					if(choices.size() <= 1)
//						break;
//					choices.softRemove(susp);
//				}
//			}
//		}
//		HashMap<Player, Integer> voteCounts = new HashMap<>();
//		int voteCount;
//		for(Player p: choices){
//			voteCount = p.getVoteCount();
//			if(slave.getVoteTarget() == p)
//				voteCount *= voteCount;
//			if(voteCount > 0)
//				voteCounts.put(p, voteCount);
//		}
//		Player choice = null;
//		final boolean startOwnTrain = brain.random.nextInt(voteCounts.size()) == 0;
//		if(startOwnTrain){
//			choice = brain.random.getPlayer(choices);
//		}else{
//			int totalVoteCount = 0;
//			for(Integer i: voteCounts.values())
//				totalVoteCount += i;
//
//			int threshold = brain.random.nextInt(totalVoteCount) + (choices.size());
//
//			for(Player p: voteCounts.keySet()){
//				threshold -= voteCounts.get(p);
//				if(threshold <= 0){
//					choice = p;
//					break;
//				}
//			}
//		}
//		if(choice == null)
//			choice = brain.random.getPlayer(choices);
//
//
//		if(slave.getVoteTarget() == choice)
//			return null;
//		Computer choiceComputer = brain.computers.get(choice);
//		if(!brain.isAtHammer(slave, choice) || choiceComputer == null){
//			return voteHelper(choice);
//		}
//
//		//make them role claim now
//		if(brain.getRoleClaim(choice) != null){
//			return voteHelper(choice);
//		}
//
//		RoleClaim c = (RoleClaim) choiceComputer.roleClaim();//computer claimed
//		if(c == null)//can't make humans role claim
//			return voteHelper(choice);
//
//		throw new ClaimInterrupt();
//	}

//	private Controller voteHelper(Player choice){
//		if(slave.is(Ghost.class) && slave.isDead() && slave.hasPuppets()){
//			if(choice.equals(slave.getPuppets().getFirst().getVoteTarget()))
//				return null;
//			return controller.ventVote(slave.getPuppets().getFirst(), choice);
//		}
//		return controller.vote(choice);
//	}

    Player skipVoteAssist(double timeLeft) {
        if(player.isAlive() && !player.isPuppeted())
            controller.doDayAction(VoteUtil.skipAction(player, timeLeft));

        Player puppet;
        for(int i = 0; i < player.getPuppets().size() && player.game.isDay(); i++){
            puppet = player.getPuppets().get(i);
            brain.computers.get(player).controller
                    .doDayAction(new Action(player, Puppet.abilityType, Vote.COMMAND, Constants.SKIP_DAY, puppet));
        }
        return null;
    }

    public Claim roleClaim() {
        if(player.is(Jester.abilityType) && player.getGameFaction().getEnemies().isEmpty())
            return null;
        Claim c = brain.getRoleClaim(player);
        if(c != null)
            return c;

        if(player.isPuppeted())
            return null;

        Set<String> safeColors = brain.getSafeColors(player.game);

        RoleClaim roleClaim;
        if(safeColors.isEmpty() || safeColors.contains(player.getGameFaction().getColor())){ // this means your color is
                                                                                             // a
            // safe one to claim
            roleClaim = new RoleClaim(player, player.gameRole.factionRole, brain);
        }else{
            String safeColor = RandomUtil.getElem(brain.random, safeColors);
            FactionRole safeRole = getSafeRole(player.game, safeColor);
            roleClaim = new RoleClaim(player, safeRole, brain);
        }
        c = brain.claim(roleClaim);
        if(c != null){
            String roleName = roleClaim.role.getName();
            String teamName = roleClaim.faction.getName();
            if(brain.talkingEnabled)
                say("Don't eliminate me. My role is " + roleName + " and I'm part of the " + teamName + ".",
                        Constants.DAY_CHAT);
        }
        return c;
    }

    private FactionRole getSafeRole(Game game, String safeColor) {
        return game.setup.getFactionByColor(safeColor).roleMap.values().iterator().next();
    }

    public static String toLetter(int num) {
        String result = "";
        while (num > 0){
            num--; // 1 => a, not 0 => a
            int remainder = num % 26;
            char digit = (char) (remainder + 65);
            result = digit + result;
            num = (num - remainder) / 26;
        }

        return result;
    }

    public RoleGroupList rolesList;
    public Memory memory;

    public class Memory {

        public PlayerList rPlayers;

        public Memory() {
            rPlayers = new PlayerList();
        }

        public void add(Player p) {
            rPlayers.add(p);
        }

        public void add(PlayerList list) {
            rPlayers.add(list);

        }
    }

    public void setRolesList() {
        rolesList = brain.publicRoles.copy();
        rolesList.crossOff(player.gameRole.factionRole);
    }

    @Override
    public String toString() {
        return player.toString();
    }

    public PlayerList[] resolveBadClaims() {
        badClaims = null;
        ArrayList<Claim> claims = brain.getClaims();
        if(rolesList == null)
            setRolesList();
        RoleGroupList rolesList = this.rolesList.copy();

        PlayerList accused = new PlayerList(), safe = new PlayerList();
        boolean hasBadClaims = false;
        for(Claim c: claims){
            if(!c.valid(rolesList, this)){
                hasBadClaims = true;
                break;
            }
            accused.add(c.getPlayersToActOn(this));
            safe.add(c.getSafePlayers(this));
        }

        if(!hasBadClaims){
            return new PlayerList[] { accused, safe };
        }

        accused = new PlayerList();
        safe = new PlayerList();

        Claim claimInQuestion;
        boolean claimIsOkay;
        for(int i = 0; i < claims.size(); i++){
            claimInQuestion = claims.remove(i);
            claimIsOkay = false;
            rolesList = this.rolesList.copy();

            for(Claim c: claims){
                if(!c.valid(rolesList, this)){
                    claimIsOkay = true; // this is because we still have a conflicting claim, and this one isn't being
                                        // considered.
                    safe.add(c.getSafePlayers(this));
                    break;
                }
            }

            if(claimIsOkay){
                i--;
            }else{
                claims.add(i, claimInQuestion);
                accused.add(claimInQuestion.getClaimer());
            }
        }

        badClaims = claims;
        return new PlayerList[] { accused, safe };
    }

    public void talkAboutBadClaims() {
        if(badClaims == null || badClaims.isEmpty())
            return;
        if(player.isSilenced() || !brain.talkingEnabled)
            return;

        for(Claim bClaim: badClaims){
            bClaim.talkAboutBadClaim(this);
        }
    }

}
