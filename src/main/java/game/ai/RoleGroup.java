package game.ai;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import game.abilities.Hidden;
import models.FactionRole;
import models.enums.AbilityType;

public class RoleGroup implements Iterable<FactionRole> {

    private Set<FactionRole> roles;

    public RoleGroup() {
        this.roles = new HashSet<>();
    }

    public void add(FactionRole role) {
        roles.add(role);
    }

    public RoleGroup add(Hidden... hiddens) {
        for(Hidden hidden: hiddens)
            this.roles.addAll(hidden.getAllFactionRoles());
        return this;
    }

    public boolean contains(FactionRole a) {
        return roles.contains(a);
    }

    public boolean contains(String color) {
        for(FactionRole role: this.roles)
            if(role.getColor().equals(color))
                return true;
        return false;
    }

    public boolean contains(AbilityType ability) {
        for(FactionRole role: this.roles)
            if(role.role.hasAbility(ability))
                return true;
        return false;
    }

    public void add(RoleGroup otherRossability) {
        for(FactionRole role: otherRossability.roles)
            this.roles.add(role);
    }

    public Set<String> getColors() {
        Set<String> colors = new HashSet<>();
        for(FactionRole role: this.roles)
            colors.add(role.getColor());
        return colors;
    }

    public RoleGroup copy() {
        RoleGroup roleGroup = new RoleGroup();
        for(FactionRole role: this.roles)
            roleGroup.roles.add(role);
        return roleGroup;
    }

    @Override
    public Iterator<FactionRole> iterator() {
        return roles.iterator();
    }

    @Override
    public String toString() {
        return this.roles.toString();
    }
}
