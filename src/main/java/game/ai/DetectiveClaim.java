package game.ai;

import java.util.ArrayList;

import game.abilities.Detective;
import game.logic.Game;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.support.Constants;
import models.Faction;

public class DetectiveClaim extends Claim {

    private ArrayList<Faction> teams;
    private PlayerList accused;
    private int claimDay;

    public DetectiveClaim(Player prosecutor, PlayerList accused, ArrayList<Faction> teams, Brain b) {
        super(prosecutor, b);
        this.teams = teams;
        this.accused = accused;
        claimDay = prosecutor.game.getDayNumber() - 1;// because this happened during the night
    }

    @Override
    public PlayerList getAccused() {
        return accused.copy();
    }

    @Override
    public boolean believable(Computer c) {
        if(!accused.hasLiving())
            return false;

        if(c.player == getClaimer())
            return true;

        Game n = c.player.game;
        // shouldn't be believed if the day of the feedback < death of possible visitor
        // day.
        // for example, if detect claimed day 5 that someone visited dead killed by sk,
        // but sk is in the graveyard, well this shouldn't be a claim.

        PlayerList deadPlayers;
        for(int i = claimDay; i < n.getDayNumber(); i++){
            deadPlayers = n.getDeadList(i);
            for(Player p: deadPlayers){
                for(Faction t: teams){
                    if(p.getColor().equals(t.getColor()))
                        return false;
                }
            }
        }

        return true;
    }

    @Override
    public boolean outlandish(Computer c) {
        PlayerList deadAccused = getAccused().getDeadPlayers();
        if(deadAccused.isEmpty())
            return false;

        for(Player acc: getAccused().getDeadPlayers()){
            if(acc.getDeathType().isHidden())
                return false;
            if(teams.contains(acc.getGameFaction().faction))
                return false;
        }
        // i did not find a dead person whos team was a team that the prosecutor claimed
        return true;
    }

    @Override
    public boolean valid(RoleGroupList rolesList, Computer c) {
        if(c.player.equals(getClaimer()))
            return true;

        if(!rolesList.contains(Detective.abilityType))
            return false;
        return true;
    }

    @Override
    public void talkAboutBadClaim(Computer c) {
        if(c.player.isSilenced() || c.player.isPuppeted())
            return;
        StringBuilder sb = new StringBuilder();
        sb.append("I don't believe " + getClaimer().getName() + "'s claim that " + accused.getStringName()
                + " is part of the ");
        if(teams.size() == 1)
            sb.append(teams.get(0).getName());
        else{
            Faction lastTeam = teams.get(teams.size() - 1);
            for(Faction t: teams){
                if(t == lastTeam){
                    sb.deleteCharAt(sb.length() - 1);
                    sb.append(" or ");
                }
                sb.append(t.getName());
                sb.append(", ");
            }
            sb.deleteCharAt(sb.length() - 1);
            sb.deleteCharAt(sb.length() - 1);
            sb.append('.');
        }
        c.say(sb.toString(), Constants.DAY_CHAT);

    }

    @Override
    public PlayerList getPlayersToActOn(Computer computer) {
        if(outlandish(computer))
            return new PlayerList(getClaimer());
        return getAccused();
    }

    @Override
    public PlayerList getSafePlayers(Computer computer) {
        // if(!believable(computer))
        return new PlayerList();

    }
}
