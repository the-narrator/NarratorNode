package game.ai;

import game.logic.Player;
import game.logic.PlayerList;

public abstract class Claim {

    private Player prosecutor;
    protected Brain brain;

    public Claim(Player prosecutor, Brain b) {
        this.prosecutor = prosecutor;
        this.brain = b;
    }

    // determines if accused was lying
    public abstract boolean believable(Computer slave);

    // determins if prosecutor is lying
    public abstract boolean outlandish(Computer slave);

    public Player getClaimer() {
        return prosecutor;
    }

    public abstract PlayerList getAccused();

    public abstract boolean valid(RoleGroupList rolesList, Computer c);

    public abstract void talkAboutBadClaim(Computer c);

    public abstract PlayerList getPlayersToActOn(Computer computer);

    public abstract PlayerList getSafePlayers(Computer computer);
}
