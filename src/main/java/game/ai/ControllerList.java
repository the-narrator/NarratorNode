package game.ai;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import game.logic.Game;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.support.Random;

public class ControllerList {

    ArrayList<Controller> list;

    public ControllerList() {
        list = new ArrayList<>();
    }

    public ControllerList(PlayerList players) {
        this();
        for(Player player: players){
            list.add(player);
        }
    }

    public ControllerList(Controller[] targets) {
        this();
        for(Controller c: targets)
            list.add(c);
    }

    public String[] getNamesToStringArray() {
        String[] ret = new String[list.size()];

        for(int i = 0; i < list.size(); i++){
            ret[i] = list.get(i).getName();
        }

        return ret;
    }

    public ControllerList copy() {
        ControllerList cList = new ControllerList();
        for(Controller c: list){
            cList.list.add(c);
        }
        return cList;
    }

    public ControllerList remove(Controller controller) {
        list.remove(controller);
        return this;
    }

    public Controller[] toArray() {
        Controller[] ret = new Controller[list.size()];

        for(int i = 0; i < list.size(); i++)
            ret[i] = list.get(i);

        return ret;
    }

    public PlayerList toPlayerList(Game narrator) {
        PlayerList pl = new PlayerList();

        Player p;
        for(Controller c: list){
            p = narrator.getPlayerByName(c.getName());
            pl.add(p);
        }

        return pl;
    }

    public void add(Controller controller) {
        list.add(controller);

    }

    public boolean isEmpty() {
        return list.isEmpty();
    }

    public static PlayerList ToPlayerList(Game n, Controller... controllers) {
        boolean isNativePlayerList = true;
        PlayerList pl = new PlayerList();
        for(Controller c: controllers){
            if(!(c instanceof Player) || ((Player) c).game != n){
                isNativePlayerList = false;
                break;
            }
            pl.add((Player) c);
        }
        if(!isNativePlayerList)
            return ControllerList.list(controllers).toPlayerList(n);

        return pl;

    }

    public ControllerList sortByName() {
        Collections.sort(list, new Comparator<Controller>() {

            @Override
            public int compare(Controller o1, Controller o2) {
                return o1.getName().compareTo(o2.getName());
            }

        });

        return this;
    }

    public static ControllerList list(Controller... cs) {
        ControllerList cl = new ControllerList();
        for(Controller c: cs){
            cl.add(c);
        }
        return cl;
    }

    public ControllerList getLivePlayers(Game n) {
        ControllerList cl = new ControllerList();
        for(Controller c: list){
            if(c.getPlayer().isAlive()){
                cl.add(c);
            }
        }
        return cl;
    }

    public Controller getFirst() {
        if(list.isEmpty())
            return null;
        return list.get(0);
    }

    public Controller getLast() {
        if(list.isEmpty())
            return null;
        return list.get(list.size() - 1);
    }

    public Controller get(int i) {
        return list.get(i);
    }

    public boolean contains(Controller p) {
        return list.contains(p);
    }

    public Controller getRandom(Random r) {
        return list.get(r.nextInt(list.size()));
    }

}
