package game.setups;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import game.abilities.FactionKill;
import game.abilities.Hidden;
import game.logic.FactionGroup;
import game.logic.Game;
import game.logic.RolesList;
import game.logic.exceptions.NarratorException;
import game.logic.support.Constants;
import models.Faction;
import models.FactionRole;
import models.Role;
import models.SetupHidden;
import models.dbo.SetupDbo;
import models.enums.AbilityType;
import models.enums.FactionModifierName;
import models.enums.GameModifierName;
import models.enums.SetupModifierName;
import models.modifiers.Modifiers;
import models.schemas.SetupSchema;
import services.FactionAbilityService;
import services.FactionModifierService;
import services.FactionService;
import services.GameSpawnsService;
import util.game.FactionRoleUtil;
import util.game.GameUtil;

public class Setup {
    public static final int MAX_PLAYER_COUNT = 255;

    public static final String TOWN_C = Constants.BLUE;
    public static final String MAFIA_C = Constants.RED;
    public static final String YAKUZA_C = Constants.YELLOW;
    public static final String OUTCAST_C = Constants.SICKLY_GREEN;
    public static final String BENIGN_C = Constants.PINK;
    public static final String CULT_C = Constants.PURPLE;
    public static final String THREAT_C = Constants.BROWN;

    public long id, ownerID;
    public String name;
    public String slogan;
    public String description;
    public boolean isEditable;

    public Modifiers<SetupModifierName> modifiers = new Modifiers<>();
    public Set<Role> roles;
    public Map<Long, Faction> factions;
    public Set<Hidden> hiddens;
    public RolesList rolesList;

    public Setup(long id, long ownerID, String name, String description, String slogan, boolean isEditable) {
        this.id = id;
        this.ownerID = ownerID;
        this.name = name;
        this.description = description;
        this.slogan = slogan;
        this.isEditable = isEditable;

        this.hiddens = new HashSet<>();
        this.rolesList = new RolesList();
        this.roles = new HashSet<>();
        this.factions = new HashMap<>();
    }

    public Setup(SetupSchema schema) {
        this(schema.id, schema.ownerID, schema.name, schema.description, schema.slogan, schema.isEditable);
    }

    public Setup(SetupDbo dbo) {
        this(dbo.id, dbo.ownerID, dbo.name, dbo.description, dbo.slogan, dbo.isEditable);
    }

    public Faction getFactionByColor(String color) {
        for(Faction faction: this.factions.values()){
            if(faction.color.equals(color))
                return faction;
        }
        throw new NullPointerException("Faction with color " + color + " not found.");
    }

    public Role getRole(long roleID) {
        for(Role role: this.roles)
            if(role.id == roleID)
                return role;
        throw new NarratorException("Role with that id not found.");
    }

    public Faction getFaction(long factionID) {
        for(Faction faction: this.factions.values())
            if(faction.id == factionID)
                return faction;
        throw new NarratorException("Faction with that id not found.");
    }

    public FactionRole getFactionRole(long factionRoleID) {
        for(Faction faction: getFactions()){
            for(FactionRole factionRole: faction.roleMap.values())
                if(factionRole.id == factionRoleID)
                    return factionRole;
        }
        throw new NarratorException("Faction role with that id not found.");
    }

    public Hidden getHidden(long hiddenID) {
        for(Hidden hidden: this.hiddens){
            if(hidden.id == hiddenID)
                return hidden;
        }
        throw new NarratorException("Hidden with that id not found.");
    }

    static final String NK_DESCRIPTION = "Shunned by all, this twisted member wants revenge by killing all civilized people";
    static final String ANTI_TOWN_DESCRIP = "Shunned by the general populace, they plot their revenge in hiding and look to ally themselves with anyone that shares their goal.";

    public static Faction Cult(Setup setup) {
        return Cult(setup, CULT_C, "Cult");
    }

    public static Faction Cult(Setup setup, String color, String name) {
        @SuppressWarnings("deprecation")
        Faction cult = FactionService.createFaction(setup, color, name,
                "A secretive organization, the cult looks to increase its ranks within the town");

        FactionModifierService.upsertModifier(cult, FactionModifierName.KNOWS_ALLIES, true);
        FactionModifierService.upsertModifier(cult, FactionModifierName.HAS_NIGHT_CHAT, true);
        FactionModifierService.upsertModifier(cult, FactionModifierName.IS_RECRUITABLE, false);
        FactionModifierService.upsertModifier(cult, FactionModifierName.PUNCH_SUCCESS, 15);

        return cult;
    }

    public static Faction Mafia(Setup setup) {
        return Mafia(setup, 1);
    }

    public static Faction Mafia(Setup setup, int i) {
        String color, name;
        if(i == 1){
            color = MAFIA_C;
            name = "Mafia";
        }else{
            color = YAKUZA_C;
            name = "Yakuza";
        }
        return Mafia(setup, color, name);
    }

    public static Faction Mafia(Setup setup, String color, String name) {
        @SuppressWarnings("deprecation")
        Faction maf = FactionService.createFaction(setup, color, name, MAF_DESCRIPTION);

        FactionAbilityService.createFactionAbility(maf, FactionKill.abilityType);
        FactionModifierService.upsertModifier(maf, FactionModifierName.HAS_NIGHT_CHAT, true);
        FactionModifierService.upsertModifier(maf, FactionModifierName.KNOWS_ALLIES, true);
        FactionModifierService.upsertModifier(maf, FactionModifierName.PUNCH_SUCCESS, 15);

        return maf;
    }

    public static Faction Town(Setup setup) {
        return Town(setup, TOWN_C, "Town");
    }

    public static Faction Town(Setup setup, String color, String name) {
        @SuppressWarnings("deprecation")
        Faction town = FactionService.createFaction(setup, color, name,
                "The uninformed majority, these roles must eliminate all Mafia factions, the cult, and all evil neutrals.");

        FactionModifierService.upsertModifier(town, FactionModifierName.PUNCH_SUCCESS, 15);

        return town;
    }

    public static Faction Benign(Setup setup) {
        return Benign(setup, BENIGN_C, "Benigns");
    }

    public static Faction Benign(Setup setup, String color, String name) {
        @SuppressWarnings("deprecation")
        Faction benign = FactionService.createFaction(setup, color, name, "Their motto is 'I do not care who wins.'");

        FactionModifierService.upsertModifier(benign, FactionModifierName.LIVE_TO_WIN, true);
        FactionModifierService.upsertModifier(benign, FactionModifierName.PUNCH_SUCCESS, 15);

        return benign;
    }

    public static Faction Outcast(Setup setup) {
        @SuppressWarnings("deprecation")
        Faction outcasts = FactionService.createFaction(setup, OUTCAST_C, "Outcast", ANTI_TOWN_DESCRIP);

        FactionModifierService.upsertModifier(outcasts, FactionModifierName.LIVE_TO_WIN, true);
        FactionModifierService.upsertModifier(outcasts, FactionModifierName.PUNCH_SUCCESS, 15);

        return outcasts;
    }

    public static Faction Killer(Setup setup, String color, String name) {
        @SuppressWarnings("deprecation")
        Faction killer = FactionService.createFaction(setup, color, name, NK_DESCRIPTION);

        FactionModifierService.upsertModifier(killer, FactionModifierName.LIVE_TO_WIN, true);
        FactionService.addEnemies(killer, killer);
        FactionModifierService.upsertModifier(killer, FactionModifierName.PUNCH_SUCCESS, 15);

        return killer;
    }

    public static Faction Killer(Setup setup) {
        return Killer(setup, THREAT_C, "Threat");
    }

    protected static void Prioritize(FactionGroup... teamGroups) {
        for(int i = teamGroups.length - 1, j = 0; i >= 0; i--, j++)
            teamGroups[j].setPriority(i);
    }

    public static final String benigns_c = "#DDA0DD";

    public static int getMinPlayerCount(Setup setup, Optional<Game> game) {
        Modifiers<GameModifierName> modifiers = game.isPresent() ? game.get().gameModifiers.modifiers
                : GameUtil.getPermissiveModifiers();
        return getMinPlayerCount(setup, modifiers);
    }

    public static int getMinPlayerCount(Setup setup, Modifiers<GameModifierName> modifiers) {
        int count = 3;

        Game game;
        while (count < setup.rolesList._setupHiddenList.size()){
            try{
                List<SetupHidden> spawningSetupHiddens = RolesList.getSpawningSetupHiddens(count,
                        setup.rolesList._setupHiddenList);
                if(!spawningSetupHiddens.isEmpty()){
                    game = GameUtil.getGeneric(setup, count, modifiers);
                    GameSpawnsService.pickRoles(game, spawningSetupHiddens);
                    break;
                }
            }catch(NarratorException e){
            }
            count++;
        }
        return count;
    }

    public static int getMaxPlayerCount(Setup setup, Optional<Game> game) {
        Modifiers<GameModifierName> modifiers = game.isPresent() ? game.get().gameModifiers.modifiers
                : GameUtil.getPermissiveModifiers();
        return getMaxPlayerCount(setup, modifiers);
    }

    public static int getMaxPlayerCount(Setup setup, Modifiers<GameModifierName> modifiers) {
        int guessMax = RolesList.getSpawningSetupHiddens(Setup.MAX_PLAYER_COUNT, setup.rolesList._setupHiddenList)
                .size();
        Game game;
        while (guessMax >= 3){
            try{
                List<SetupHidden> spawningSetupHiddens = RolesList.getSpawningSetupHiddens(guessMax,
                        setup.rolesList._setupHiddenList);

                if(!spawningSetupHiddens.isEmpty()){
                    game = GameUtil.getGeneric(setup, guessMax, modifiers);
                    GameSpawnsService.pickRoles(game, spawningSetupHiddens);
                    break;
                }
            }catch(NarratorException e){
            }
            guessMax--;
        }
        return Math.max(guessMax, 3);
    }

    public static final String MAF_DESCRIPTION = "The informed minority, these roles must eliminate the Town, other Mafia factions, the cult and evil killing neutrals.";

    public String getName() {
        return this.name;
    }

    public static Setup NoDB() {
        return new Setup(0, 0, "No DB Setup Name", "No DB slogan", "No DB description", true);
    }

    public Set<Faction> getFactions() {
        return new HashSet<>(this.factions.values());
    }

    // factions with a factionRole basically
    public Set<Faction> getFactionsWithARole() {
        Set<Faction> factions = new HashSet<>();
        for(Faction faction: this.getFactions())
            if(!faction.roleMap.isEmpty())
                factions.add(faction);
        return factions;
    }

    public Set<Faction> getSpawnableFactions(Game game) {
        Set<Faction> factions = new HashSet<>();
        for(FactionRole factionRole: this.getSpawnableFactionRoles(game))
            factions.add(factionRole.faction);
        return factions;
    }

    public Set<Faction> getSpawnableFactions(Optional<Game> game) {
        if(game.isPresent())
            return this.getSpawnableFactions(game.get());
        return getFactionsWithARole();
    }

    // I can't think of a difference at the moment, but maybe in the future
    // A role that can change into another faction? Break off into it's own faction?
    public Set<Faction> getPossibleFactions(Game game) {
        return this.getSpawnableFactions(game);
    }

    public Set<FactionRole> getFactionRoles() {
        Set<FactionRole> factionRoles = new HashSet<>();
        // todo to add
        for(Faction faction: this.getFactions())
            factionRoles.addAll(faction.roleMap.values());

        return factionRoles;
    }

    public Set<FactionRole> getPossibleFactionRoles() {
        Set<FactionRole> factionRoles = new HashSet<>();
        for(SetupHidden setupHidden: this.rolesList.iterable())
            factionRoles.addAll(setupHidden.hidden.getAllFactionRoles());
        return factionRoles;
    }

    @SafeVarargs
    final public Set<Faction> getPossibleFactionsWithRolesWithAbility(Game game, AbilityType... abilityTypes) {
        Set<Faction> factions = new HashSet<>();
        for(FactionRole factionRole: this.getPossibleFactionRolesWithAbility(game, abilityTypes))
            factions.add(factionRole.faction);

        return factions;
    }

    public Set<Faction> getPossibleFactionsWithRoles(Game game, Role role) {
        Set<Faction> factions = new HashSet<>();
        for(FactionRole factionRole: getPossibleFactionRoles(game))
            if(factionRole.role == role)
                factions.add(factionRole.faction);

        return factions;
    }

    public Set<FactionRole> getSpawnableFactionRoles(Game game) {
        Set<FactionRole> factionRoles = new HashSet<>();
        for(SetupHidden setupHidden: this.rolesList.iterable(game))
            factionRoles.addAll(setupHidden.hidden.getSpawningFactionRoles(game));
        return FactionRoleUtil.filterChatRoles(factionRoles, game);
    }

    public Set<FactionRole> getPossibleFactionRoles(Game game) {
        Set<FactionRole> factionRoles = new HashSet<>();
        for(Faction faction: this.getPossibleFactions(game))
            factionRoles.addAll(faction.getPossibleFactionRoles(game));
        return FactionRoleUtil.filterChatRoles(factionRoles, game);
    }

    public boolean allowsForFriendlyFire(Game game) {
        for(FactionRole factionRole: getPossibleFactionRoles(game))
            if(factionRole.allowsForFriendlyFire(game))
                return true;
        return false;
    }

    public boolean hasFactionRolesThatAffectSending(Game game) {
        for(FactionRole factionRole: getPossibleFactionRoles(game)){
            if(factionRole.hasMembersThatAffectSending())
                return true;
        }
        return false;
    }

    final public Set<FactionRole> getFactionRolesWithAbility(AbilityType... abilityTypes) {
        Set<FactionRole> factionRoles = new HashSet<>();
        for(FactionRole factionRole: this.getFactionRoles())
            if(factionRole.role.hasAbility(abilityTypes))
                factionRoles.add(factionRole);
        return factionRoles;
    }

    final public Set<FactionRole> getPossibleFactionRolesWithAbility(AbilityType... abilityTypes) {
        Set<FactionRole> factionRoles = new HashSet<>();
        for(FactionRole factionRole: this.getPossibleFactionRoles())
            if(factionRole.role.hasAbility(abilityTypes))
                factionRoles.add(factionRole);
        return factionRoles;
    }

    final public Set<FactionRole> getPossibleFactionRolesWithAbility(Optional<Game> game, AbilityType... abilityTypes) {
        if(game.isPresent())
            return this.getPossibleFactionRolesWithAbility(game.get(), abilityTypes);
        return this.getPossibleFactionRolesWithAbility(abilityTypes);
    }

    final public Set<FactionRole> getPossibleFactionRolesWithAbility(Game game, AbilityType... abilityTypes) {
        Set<FactionRole> factionRoles = new HashSet<>();
        for(FactionRole factionRole: this.getPossibleFactionRoles(game))
            if(factionRole.role.hasAbility(abilityTypes))
                factionRoles.add(factionRole);
        return FactionRoleUtil.filterChatRoles(factionRoles, game);
    }

    // any of these ability types
    final public boolean hasPossibleFactionRoleWithAbility(Game game, AbilityType... abilityTypes) {
        for(FactionRole factionRole: getPossibleFactionRoles(game))
            if(factionRole.role.hasAbility(abilityTypes))
                return true;
        return false;
    }

    @SafeVarargs
    final public boolean hasSpawnableFactionRoleWithAbility(Game game, AbilityType... abilityTypes) {
        for(FactionRole factionRole: getSpawnableFactionRoles(game))
            if(factionRole.role.hasAbility(abilityTypes))
                return true;
        return false;
    }

    public boolean hasFactionRoleWithAbility(AbilityType abilityType) {
        for(FactionRole factionRole: this.getFactionRoles()){
            if(factionRole.role.hasAbility(abilityType))
                return true;
        }
        return false;
    }
}
