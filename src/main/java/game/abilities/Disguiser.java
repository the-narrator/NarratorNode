package game.abilities;

import java.util.Optional;

import game.abilities.util.AbilityValidationUtil;
import game.event.NightChat;
import game.logic.Game;
import game.logic.GameFaction;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.support.Constants;
import game.logic.support.action.Action;
import game.logic.support.attacks.Attack;
import game.setups.Setup;
import models.Faction;
import models.FactionRole;
import models.Role;
import models.enums.AbilityModifierName;
import models.enums.AbilityType;
import models.enums.GamePhase;
import models.modifiers.Modifiers;
import services.FactionRoleService;
import services.RoleService;

public class Disguiser extends GameAbility {

    public static final AbilityType abilityType = AbilityType.Disguiser;
    public static final String COMMAND = abilityType.command;

    public Disguiser(Game game, Setup setup, Modifiers<AbilityModifierName> modifiers) {
        super(game, setup, modifiers);
    }

    @Override
    public AbilityType getAbilityType() {
        return abilityType;
    }

    @Override
    public String getAbilityDescription(Optional<Game> game, Setup setup) {
        return NIGHT_ACTION_DESCRIPTION;
    }

    public static final String NIGHT_ACTION_DESCRIPTION = "Kill someone and take their identity.";

    @Override
    public void onNightStart(Player p) {
        super.onNightStart(p);
        disguisedTonight = false;
    }

    @Override
    public void mainAbilityCheck(Action a) {
        AbilityValidationUtil.deadCheck(a);
    }

    public static final String DEATH_FEEDBACK = "You were killed by a Disguiser!";

    private static boolean willDie(Player p) {
        if(p.getLives() > 1)
            return false;
        if(p.getRealAutoVestCount() > 0){
            for(Attack a: p.getInjuries()){
                if(a.isCountered())
                    continue;
                if(a.getType() == Constants.JESTER_KILL_FLAG)
                    return true;
            }
        }
        return true;
    }

    public Player killedTarget;
    public Player disguised;
    boolean disguisedTonight;

    @Override
    public void doNightAction(Action a) {
        Player owner = a.owner, target = a.getTarget();
        if(owner.game.nightPhase == GamePhase.PRE_KILLING || disguisedTonight)
            return;
        if(killedTarget != null && killedTarget.disguisedPrevName == null && willDie(killedTarget)
                && owner.game.nightPhase == GamePhase.POST_KILLING){
            disguisedTonight = true;
            owner.switchNames(killedTarget);
            killedTarget.setCleaned(owner.getSkipper());
            killedTarget.disguisedPrevName = owner.getName();
            happening(owner, " disguised as ", killedTarget);

            GameFaction currentTeam = owner.getGameFaction();
            if(disguised != null){
                for(GameFaction t: owner.getFactions()){
                    if(currentTeam != t){
                        t.removeMember(owner);
                        NightChat nl = owner.game.getEventManager().getNightLog(t.getColor());
                        if(nl != null){
                            owner.removeChat(nl);
                        }
                    }
                }
            }

            if(!killedTarget.getID().equals(a.owner.getID())){
                for(GameFaction t: killedTarget.getFactions()){
                    if(t != currentTeam && t.knowsTeam())
                        t.infiltrate(a.owner);
                }
                if(Mason.IsMasonType(killedTarget))
                    killedTarget.getGameFaction().infiltrate(a.owner);
            }

            disguised = killedTarget;
            owner.stealVotePower(killedTarget);
            killedTarget = null;
            a.markCompleted();
            return;
        }else if(target == null){
            return;
        }else if(killedTarget == null){
            killedTarget = target;
            Kill(owner, target, Constants.HIDDEN_KILL_FLAG);// visiting taken care of already in kill
            return;
        }
        return;
    }

    @Override
    public boolean isChatRole() {
        return true;
    }

    @Override
    public int maxNumberOfSubmissions() {
        return 1;
    }

    @Override
    protected String getProfileChargeText() {

        int charges = getPerceivedCharges();
        if(super.charges == null)
            return ("You can disguise as many times as you want.");
        if(charges == 0)
            return ("You cannot disguise anymore.");
        if(charges != 1)
            return ("You can give disguise " + charges + " more times.");
        if(modifiers.getInt(AbilityModifierName.CHARGES, narrator) == 1)
            return ("You may only disguise once");
        return "You can disguise one more time.";

    }

    @Override
    public int nightVotePower(Player p, PlayerList pl) {
        if(disguised == null){
            return super.nightVotePower(p, pl);
        }

        int vp = 0;
        for(GameAbility a: disguised.getRoleAbilities()){
            vp = Math.max(vp, a.nightVotePower(disguised, pl.copy()));
        }
        return Math.max(super.nightVotePower(p, pl), vp);
    }

    @Override
    public boolean isFakeBlockable() {
        return false;
    }

    public static boolean hasDisguised(Player p) {
        if(p.isDead())
            return false;
        if(p.getAbility(Disguiser.class) == null)
            return false;
        return p.getAbility(Disguiser.class).disguised != null;
    }

    public static Player getDisguised(Player p) {
        if(p.isDead())
            return null;
        if(p.getAbility(Disguiser.class) == null)
            return null;
        return p.getAbility(Disguiser.class).disguised;
    }

    @Override
    public boolean allowsForFriendlyFire() {
        return true;
    }

    @Override
    public boolean affectsSending() {
        return true;
    }

    @Override
    public boolean isNegativeAbility() {
        return true;
    }

    public static FactionRole template(Faction faction) {
        Role role = template(faction.setup);
        return FactionRoleService.createFactionRole(faction, role);
    }

    public static Role template(Setup setup) {
        return RoleService.createRole(setup, "Disguiser", abilityType);
    }
}
