package game.abilities;

import java.util.ArrayList;
import java.util.Optional;

import game.abilities.support.Charge;
import game.abilities.support.Vest;
import game.logic.Game;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.support.Constants;
import game.logic.support.Random;
import game.logic.support.StringChoice;
import game.logic.support.action.Action;
import game.setups.Setup;
import models.Faction;
import models.FactionRole;
import models.enums.AbilityModifierName;
import models.enums.AbilityType;
import models.enums.SetupModifierName;
import models.modifiers.Modifiers;
import services.FactionRoleService;
import services.RoleService;
import util.game.GameUtil;

public class Survivor extends GameAbility {

    public static final AbilityType abilityType = AbilityType.Survivor;
    public static final String COMMAND = Constants.VEST_COMMAND;

    public Survivor(Game game, Setup setup, Modifiers<AbilityModifierName> modifiers) {
        super(game, setup, modifiers);
    }

    @Override
    public AbilityType getAbilityType() {
        return abilityType;
    }

    @Override
    public void doNightAction(Action action) {
        Player owner = action.owner;
        Vest v = (Vest) this.charges.get(0);
        // owner.consumedItems.add(v);
        owner.prevInvulnerability = owner.isInvulnerable();
        if(v.isReal()){
            owner.setInvulnerable(true);
            owner.setVesting(true);
        }
        action.markCompleted();
        if(action.getTarget() != null)
            owner.visit(action.getTarget());
    }

    @Override
    public Charge getCharge(boolean isFromSelf) {
        return new Vest(isFromSelf);
    }

    @Override
    public void mainAbilityCheck(Action a) {
    }

    public static Vest SurvVest() {
        return new Vest(Charge.FROM_ROLE);
    }

    @Override
    public int getDefaultTargetSize() {
        return 0;
    }

    @Override
    public boolean isAllowedFactionAbility() {
        return false;
    }

    @Override
    public int maxNumberOfSubmissions() {
        return 1;
    }

    @Override
    public boolean isSelfTargetModifiable() {
        return false;
    }

    @Override
    public String getUsage(Player p, Random r) {
        return COMMAND;
    }

    @Override
    public boolean isChargeModifiable() {
        return true;
    }

    @Override
    public boolean isBackToBackModifiable() {
        return false;
    }

    @Override
    public boolean isZeroWeightModifiable() {
        return false;
    }

    @Override
    public boolean isItemAbility() {
        return true;
    }

    @Override
    public PlayerList getAcceptableTargets(Player p) {
        return null;
    }

    @Override
    public Optional<Action> getExampleAction(Player owner) {
        return Optional.of(new Action(owner, abilityType));
    }

    @Override
    public String getRuleChargeText(Optional<Integer> playerCount, int chargeCount) {
        StringBuilder sb = new StringBuilder("Starts off with ");
        if(setup.modifiers.getInt(SetupModifierName.CHARGE_VARIABILITY, playerCount) != 0)
            sb.append("~");
        sb.append(chargeCount);
        sb.append(" ");
        sb.append(Vest.getVestName(playerCount, setup));
        if(chargeCount != 1)
            sb.append("s");
        return sb.toString();
    }

    @Override
    public ArrayList<Object> getActionDescription(ArrayList<Action> actions) {
        Player owner = actions.get(0).owner;

        ArrayList<Object> list = new ArrayList<>();
        list.add("use ");

        StringChoice scVest = new StringChoice("their");
        scVest.add(owner, "your");
        list.add(scVest);
        list.add(" " + Vest.getVestName(owner.game));

        return list;
    }

    @Override
    protected ArrayList<String> getProfileHints(Player player) {
        int vestCount = this.getPerceivedCharges();
        if(vestCount == 0)
            return super.getProfileHints(player);

        String vestName = Vest.getVestName(player.game);
        ArrayList<String> specs = new ArrayList<>();
        if(vestCount == 1)
            specs.add("You have a " + vestName + " left to protect you from death.");
        else if(vestCount > 0)
            specs.add("You have " + vestCount + " " + vestName + " left to protect you from death.");

        return specs;
    }

    @Override
    public String getAbilityDescription(Optional<Game> game, Setup setup) {
        return GetNightActionDescription(game, setup);
    }

    public static String GetNightActionDescription(Optional<Game> game, Setup setup) {
        Optional<Integer> playerCount = GameUtil.getPlayerCount(game);
        return "use a " + Vest.getVestName(playerCount, setup) + " to survive the night";
    }

    @Override
    public ArrayList<Object> getInsteadPhrase(Action action) {
        ArrayList<Object> list = new ArrayList<>();
        list.add("putting on a " + Vest.getVestName(action.owner.game) + ", ");
        return list;
    }

    public static FactionRole template(Faction faction) {
        return FactionRoleService.createFactionRole(faction,
                RoleService.createRole(faction.setup, "Survivor", abilityType));
    }
}
