package game.abilities;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import game.logic.Game;
import game.logic.GameFaction;
import game.logic.Player;
import game.logic.PlayerList;
import game.setups.Setup;
import models.Faction;
import models.FactionRole;
import models.Role;
import models.enums.AbilityModifierName;
import models.enums.AbilityType;
import models.enums.SetupModifierName;
import models.modifiers.Modifiers;
import services.FactionRoleService;
import services.RoleAbilityModifierService;
import services.RoleService;

public class Executioner extends Passive {

    public static final AbilityType abilityType = AbilityType.Executioner;

    public Executioner(Game game, Setup setup, Modifiers<AbilityModifierName> modifiers) {
        super(game, setup, modifiers);
    }

    public static final String DEATH_FEEDBACK = "You killed yourself because your target died.";

    @Override
    public AbilityType getAbilityType() {
        return abilityType;
    }

    private PlayerList targets;

    @Override
    public GameAbility initialize(Player p) {
        targets = new PlayerList();
        return super.initialize(p);
    }

    @Override
    public String getAbilityDescription(Optional<Game> game, Setup setup) {
        return NIGHT_ACTION_DESCRIPTION;
    }

    @Override
    public List<SetupModifierName> getSetupModifierNames() {
        List<SetupModifierName> names = new LinkedList<>();
        names.add(SetupModifierName.EXECUTIONER_WIN_IMMUNE);
        names.add(SetupModifierName.EXECUTIONER_TO_JESTER);
        names.add(SetupModifierName.EXEC_TOWN);
        return names;
    }

    public static final String NIGHT_ACTION_DESCRIPTION = "Sole purpose is to get a given target killed. Do it.";

    public PlayerList getTargets() {
        return targets;
    }

    public Player getTarget() {
        return targets.getLast();
    }

    public void addTarget(Player target) {
        this.targets.add(target);
    }

    public void setTarget(Player target) {
        this.targets.set(targets.size() - 1, target);
    }

    private boolean winner = false;

    @Override
    public Boolean checkWinEnemyless(Player p) {
        return winner;
    }

    public void setWon() {
        winner = true;
    }

    @Override
    public void setInitialCharges(int charge, boolean shouldAddNoise) {
        super.setInitialCharges(charge, false); // null player means no noise adding
    }

    public static Player GetTarget(Player p) {
        if(!p.hasAbility(Executioner.abilityType))
            return null;

        Executioner r_card = p.getAbility(Executioner.class);
        return r_card.getTarget();
    }

    @Override
    public void onGameStart(Player exec) {
        super.onGameStart(exec);
        assign(exec);
    }

    public static void assign(Player exec) {
        Game n = exec.game;
        PlayerList players = new PlayerList();
        if(n.getBool(SetupModifierName.EXEC_TOWN)){
            int threshold = 0;
            for(GameFaction t: n.getFactions()){
                if(t._startingSize > threshold){
                    players.clear();
                    for(Player p: t.getMembers()){
                        if(p.getGameFaction() == t && p.isAlive())
                            players.add(p);
                    }
                    threshold = t._startingSize;
                }else if(t._startingSize == threshold){
                    for(Player p: t.getMembers()){
                        if(p.getGameFaction() == t && p.isAlive())
                            players.add(p);
                    }
                }
            }
        }
        if(players.isEmpty())
            players = n.getLivePlayers();

        players.remove(exec).sortByID();
        Player target = n.getRandom().getPlayer(players);

        Executioner role = exec.getAbility(Executioner.class);
        role.useCharge();
        role.addTarget(target);
    }

    @Override
    public boolean isPowerRole() {
        return false;
    }

    public static final String UNKNOWN_TARGET = "You no longer know who your target is.";

    @Override
    protected ArrayList<String> getProfileHints(Player exec) {
        ArrayList<String> roleInfo = new ArrayList<String>();
        GameFaction t = exec.getGameFaction();
        if(winner && t.getEnemies().isEmpty()){
            roleInfo.add("You've already won!");
            return roleInfo;
        }
        Player target = getTarget();

        if(target.getName().equals(target.getID()))
            roleInfo.add("You must try to " + target.getName() + " publicly executed to win.");
        else
            roleInfo.add(UNKNOWN_TARGET);

        if(exec.game.getBool(SetupModifierName.EXEC_TOWN)){
            t = exec.game.getFaction(getTarget().getInitialColor());
            roleInfo.add("Your target will always be part of the " + t.getName());
        }else
            roleInfo.add("Your target may be of any alignment!");
        return roleInfo;
    }

    @Override
    public boolean canStartWithEnemies() {
        return false;
    }

    @Override
    public boolean isChargeModifiable() {
        return true;
    }

    @Override
    public boolean isActivePassive(Player p) {
        for(GameFaction t: p.getFactions()){
            if(t.hasEnemies())
                return false;
        }
        return true;
    }

    @Override
    public String getRuleChargeText(Optional<Integer> playerCount, int chargeCount) {
        if(chargeCount == 1)
            return "Target will not change if original target dies.";
        return "Gets " + (chargeCount - 1) + " chances to publicly execute target before loss.";
    }

    @Override
    public boolean isNightless(Optional<Integer> playerCount) {
        return true;
    }

    public static void SetTarget(Player exec, Player p) {
        exec.getAbility(Executioner.class).setTarget(p);
    }

    public static FactionRole template(Faction faction) {
        Role exec = RoleService.createRole(faction.setup, "Executioner", abilityType);
        RoleAbilityModifierService.upsert(exec, abilityType, AbilityModifierName.CHARGES, 1);
        return FactionRoleService.createFactionRole(faction, exec);
    }
}
