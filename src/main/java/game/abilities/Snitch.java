package game.abilities;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import game.abilities.util.AbilityValidationUtil;
import game.event.Announcement;
import game.event.Happening;
import game.event.SnitchAnnouncement;
import game.logic.Game;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.support.StringChoice;
import game.logic.support.action.Action;
import game.setups.Setup;
import models.Ability;
import models.Faction;
import models.FactionRole;
import models.enums.AbilityModifierName;
import models.enums.AbilityType;
import models.enums.SetupModifierName;
import models.modifiers.Modifiers;
import services.FactionRoleService;
import services.RoleService;
import util.game.ActionUtil;
import util.game.GameUtil;

public class Snitch extends GameAbility {

    public static final AbilityType abilityType = AbilityType.Snitch;
    public static final String COMMAND = abilityType.command;

    public Snitch(Game game, Setup setup, Modifiers<AbilityModifierName> modifiers) {
        super(game, setup, modifiers);
    }

    @Override
    public boolean isAllowedFactionAbility() {
        return false;
    }

    @Override
    public boolean isBackToBackModifiable() {
        return false;
    }

    @Override
    public String getAbilityDescription(Optional<Game> game, Setup setup) {
        return NIGHT_ACTION_DESCRIPTION;
    }

    @Override
    public GameAbility initialize(Player p) {
        toReveal = new PlayerList();
        return super.initialize(p);
    }

    @Override
    public AbilityType getAbilityType() {
        return abilityType;
    }

    public static final String NIGHT_ACTION_DESCRIPTION = "Upon death, the last target's alignment and role will be publicly revealed.";

    @Override
    public void mainAbilityCheck(Action a) {
        AbilityValidationUtil.deadCheck(a);
    }

    PlayerList toReveal;

    @Override
    public void doNightAction(Action a) {
        Player owner = a.owner, target = a.getTarget();
        if(target == null)
            return;
        if(first){
            first = false;
            toReveal.clear();
        }
        toReveal.add(target);
        new Happening(owner.game).add(owner, " will reveal ", target, " upon death.");
        a.markCompleted();
        owner.visit(target);
        return;
    }

    @Override
    public ArrayList<Object> getActionDescription(ArrayList<Action> actions) {
        ArrayList<Object> list = new ArrayList<Object>();

        PlayerList targets = ActionUtil.getActionTargets(actions);

        list.add("make contingency plans on ");
        list.addAll(StringChoice.YouYourself(targets));

        return list;
    }

    private boolean first = true;

    @Override
    public void onDayStart(Player p) {
        super.onDayStart(p);
        first = true;
    }

    @Override
    public void onNightStart(Player p) {
        super.onNightStart(p);
        Player last = toReveal.getLast();
        toReveal.clear();
        if(last != null)
            toReveal.add(last);
    }

    public static Optional<FactionRole> getCitizenTeam(Game game) {
        Set<FactionRole> factionRoles = game.setup.getPossibleFactionRoles(game);
        for(FactionRole factionRole: factionRoles)
            if(Citizen.hasNoAbilities(factionRole))
                return Optional.of(factionRole);
        return Optional.empty();
    }

    @Override
    public ArrayList<String> getPublicDescription(Optional<Game> game, String actionOriginatorName,
            Optional<String> nullableColor, Set<Ability> abilities) {
        ArrayList<String> list = super.getPublicDescription(game, actionOriginatorName, nullableColor, abilities);
        list.addAll(getMoreInfoText(game));
        return list;
    }

    private List<String> getMoreInfoText(Optional<Game> game) {
        List<String> list = new LinkedList<>();

        if(this.setup.modifiers.getBoolean(SetupModifierName.SNITCH_PIERCES_SUIT, GameUtil.getPlayerCount(game)))
            list.add("Players that are revealed aren't affected by suits.");
        else
            list.add("Players that are revealed might be affected by a suit.");
        list.add("Does not pierce detection immunity.");
        list.add("Ability activates, no matter the cause of death.");
        return list;
    }

    @Override
    public void onDeath(Player snitch) {
        if(toReveal.isEmpty())
            return;

        Game game = snitch.game;
        Announcement a;
        String r;
        Faction faction;
        Optional<FactionRole> m;
        for(Player revealed: toReveal){
            if(revealed.isDead() && !revealed.getDeathType().isHidden())
                continue;
            if(revealed.hasSuits() && !snitch.game.getBool(SetupModifierName.SNITCH_PIERCES_SUIT)){
                r = revealed.suits.get(0).factionRole.getName();
                faction = revealed.suits.get(0).factionRole.faction;
            }else if(revealed.isDetectable()){
                r = revealed.getRoleName();
                faction = revealed.getGameFaction().faction;
            }else{
                m = getCitizenTeam(game);
                if(m.isPresent()){
                    r = m.get().role.getName();
                    faction = m.get().faction;
                }else{
                    faction = revealed.getGameFaction().faction;
                    r = revealed.getName();
                }
            }
            a = new SnitchAnnouncement(revealed, faction, r);
            game.announcement(a);
        }
    }

    @Override
    protected ArrayList<String> getProfileHints(Player p) {
        if(!narrator.setup.hasPossibleFactionRoleWithAbility(narrator, Tailor.abilityType))
            return super.getProfileHints(p);
        ArrayList<String> ret = new ArrayList<>();

        ret.addAll(getMoreInfoText(Optional.of(p.game)));

        return ret;
    }

    @Override
    public List<SetupModifierName> getSetupModifierNames() {
        List<SetupModifierName> rules = new LinkedList<>();
        rules.add(SetupModifierName.SNITCH_PIERCES_SUIT);
        return rules;
    }

    public static FactionRole template(Faction faction) {
        return FactionRoleService.createFactionRole(faction,
                RoleService.createRole(faction.setup, "Snitch", abilityType));
    }
}
