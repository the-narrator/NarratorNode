package game.abilities;

import java.util.Optional;

import game.logic.Game;
import game.setups.Setup;
import models.enums.AbilityModifierName;
import models.enums.AbilityType;
import models.modifiers.Modifiers;

public class Infiltrator extends Passive {

    public static final AbilityType abilityType = AbilityType.Infiltrator;
    public static String ROLE_NAME = Infiltrator.class.getSimpleName();

    public Infiltrator(Game game, Setup setup, Modifiers<AbilityModifierName> modifiers) {
        super(game, setup, modifiers);
    }

    public static final String NIGHT_ACTION_DESCRIPTION = "Retain original team upon any sort of recruitment.";

    @Override
    public AbilityType getAbilityType() {
        return abilityType;
    }

    @Override
    public boolean allowsForFriendlyFire() {
        return true;
    }

    @Override
    public boolean isPowerRole() {
        return true;
    }

    @Override
    public String getAbilityDescription(Optional<Game> game, Setup setup) {
        return NIGHT_ACTION_DESCRIPTION;
    }
}
