package game.abilities;

import java.util.ArrayList;
import java.util.Optional;

import game.event.Feedback;
import game.logic.Game;
import game.logic.Player;
import game.logic.support.HTString;
import game.logic.support.action.Action;
import game.setups.Setup;
import models.Faction;
import models.FactionRole;
import models.Role;
import models.enums.AbilityModifierName;
import models.enums.AbilityType;
import models.modifiers.Modifiers;
import services.FactionRoleService;
import services.RoleService;

public class Ghost extends Passive {

    public static final AbilityType abilityType = AbilityType.Ghost;

    public Ghost(Game game, Setup setup, Modifiers<AbilityModifierName> modifiers) {
        super(game, setup, modifiers);
    }

    @Override
    public AbilityType getAbilityType() {
        return abilityType;
    }

    @Override
    public boolean isAllowedFactionAbility() {
        return false;
    }

    @Override
    public String getAbilityDescription(Optional<Game> game, Setup setup) {
        return NIGHT_ACTION_DESCRIPTION;
    }

    public static final String NIGHT_ACTION_DESCRIPTION = "Die, and be able to possess people during the day.  Make the person who killed you lose.";

    @Override
    public void doNightAction(Action a) {
        return;
    }

    @Override
    public Boolean checkWinEnemyless(Player p) {
        if(p.isAlive())
            return false;
        Player cause = p.getDeathType().getCause();
        if(cause == p && p.getCause() != null)
            return !p.getCause().isWinner();
        if(cause == null)
            return false;
        if(cause.isDead() && cause.is(Ghost.abilityType) && cause.getDeathType().getCause() == p)
            return false;
        return !cause.isWinner();
    }

    @Override
    public boolean canStartWithEnemies() {
        return false;
    }

    @Override
    public boolean isRecruitable() {
        return false;
    }

    @Override
    public boolean isActivePassive(Player p) {
        return true;
    }

    boolean total_loss = false;

    @Override
    public void onDeath(Player p) {
        Feedback f;
        Player cause = p.getDeathType().getCause();
        if(cause == null){
            total_loss = true;
            f = new Feedback(p, "You've died and lost, but don't have someone to try to make to lose.");
        }else{
            f = new Feedback(p, "You've died.  To win you must make the ");
            f.add(new HTString(cause.getGameFaction()), " lose.");
        }

        p.sendMessage(f);

    }

    @Override
    protected ArrayList<String> getProfileHints(Player p) {
        ArrayList<String> specs = new ArrayList<>();
        if(p.isAlive()){
            specs.add("You must die.  After, you will know what faction you must try and take down.");
        }else if(p.getDeathType().getCause() != null){
            specs.add("You must make the " + p.getDeathType().getCause().getGameFaction() + " lose.");
        }else{
            specs.add("You cannot possibly win and may not interact with the dead anymore.");
        }
        return specs;
    }

    @Override
    public boolean isSelfTargetModifiable() {
        return false;
    }

    public static FactionRole template(Faction faction) {
        Role role = RoleService.createRole(faction.setup, "Ghost", Ghost.abilityType, Ventriloquist.abilityType);
        return FactionRoleService.createFactionRole(faction, role);
    }
}
