package game.abilities;

import game.logic.Player;
import game.setups.Setup;
import models.Faction;
import models.FactionRole;
import models.Role;
import services.FactionRoleService;
import services.RoleService;

public class Citizen {

    public static Role template(Setup setup) {
        return RoleService.createRole(setup, "Citizen");
    }

    public static FactionRole template(Faction faction) {
        return FactionRoleService.createFactionRole(faction, template(faction.setup));
    }

    public static boolean hasNoAbilities(Player player) {
        return player.gameRole._abilities.filterNot(Vote.abilityType).size() == 0;
    }

    public static boolean hasNoAbilities(FactionRole factionRole) {
        return factionRole.role.abilityMap.isEmpty();
    }
}
