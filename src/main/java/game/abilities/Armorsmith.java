package game.abilities;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import game.abilities.support.Charge;
import game.abilities.support.Vest;
import game.abilities.util.AbilityValidationUtil;
import game.event.Feedback;
import game.logic.Game;
import game.logic.GameFaction;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.exceptions.IllegalActionException;
import game.logic.support.Option;
import game.logic.support.Random;
import game.logic.support.StringChoice;
import game.logic.support.action.Action;
import game.setups.Setup;
import models.Ability;
import models.Faction;
import models.FactionRole;
import models.Role;
import models.enums.AbilityModifierName;
import models.enums.AbilityType;
import models.modifiers.Modifiers;
import services.FactionRoleService;
import services.RoleService;
import util.Util;
import util.game.ActionUtil;
import util.game.GameUtil;

public class Armorsmith extends GameAbility {

    public static final AbilityType abilityType = AbilityType.Armorsmith;
    public static final String COMMAND = abilityType.command;

    public Armorsmith(Game game, Setup setup, Modifiers<AbilityModifierName> modifiers) {
        super(game, setup, modifiers);
    }

    public static final String[] AS_FAKE_VEST = new String[] { "vs_fake_vest",
            "#" + Vest.ALIAS + "#s can be optionally fake", };

    @Override
    public String getAbilityDescription(Optional<Game> game, Setup setup) {
        Optional<Integer> playerCount = GameUtil.getPlayerCount(game);
        return "Give " + Vest.getVestName(playerCount, setup)
                + " during the night, protecting them from attack on subsequent nights.";
    }

    public static final String FAKE = "fake";
    public static final String REAL = Gunsmith.REAL;

    public static String GetArmorReceiveMessage(Game narrator) {
        return "You received " + Vest.getVestName(narrator) + "!";
    }

    public static boolean FakeVests(Player player) {
        if(player.hasAbility(Armorsmith.abilityType))
            return FakeVests(player.getAbility(Armorsmith.abilityType));
        if(player.hasAbility(Blacksmith.abilityType))
            return FakeVests(player.getAbility(Blacksmith.abilityType));
        if(player.hasAbility(GraveDigger.abilityType)){
            for(Player q: player.game.players){
                if(q.hasAbility(Armorsmith.abilityType) || q.hasAbility(Blacksmith.abilityType)){
                    if(FakeVests(player))
                        return true;
                }
            }
            return false;
        }

        throw new IllegalActionException("Cannot test for fake " + Vest.getVestName(player.game) + " on this player");
    }

    public static boolean FakeVests(GameAbility ability) {
        Optional<Integer> playerCount = GameUtil.getPlayerCount(Optional.ofNullable(ability.narrator));
        return ability.modifiers.getBoolean(AbilityModifierName.AS_FAKE_VESTS, playerCount);
    }

    public boolean fakeVests() {
        return FakeVests(this);
    }

    @Override
    public void mainAbilityCheck(Action a) {
        AbilityValidationUtil.deadCheck(a);
        String vestName = Vest.getVestName(narrator);
        if(!fakeVests() && a.getArg1() != null && !a.getArg1().equalsIgnoreCase(REAL))
            AbilityValidationUtil
                    .Exception(Util.TitleCase(vestName) + " can only be real. Please omit any action options.");
        if(fakeVests() && a.getArg1() != null
                && (!a.getArg1().equalsIgnoreCase(REAL) && !a.getArg1().equalsIgnoreCase(FAKE)))
            AbilityValidationUtil.Exception("Unable to tell whether " + vestName + " should real or faulty");

        if(fakeVests() && a.getArg1() == null)
            a.args.add(REAL);
    }

    @Override
    public ArrayList<Option> getOptions(Player p) {
        return getOptions(fakeVests(), p.game);
    }

    public static ArrayList<Option> getOptions(boolean fakeVests, Game game) {
        ArrayList<Option> options = new ArrayList<>();
        if(!fakeVests)
            return options;
        String v_opt = Vest.getVestName(game);
        options.add(new Option(REAL, REAL + " " + v_opt));
        options.add(new Option(FAKE, FAKE + " " + v_opt));
        return options;
    }

    @Override
    public void doNightAction(Action a) {
        Player owner = a.owner, target = a.getTarget();
        if(target != null)
            a.markCompleted();
        ArmorsmithAction(owner, target, a.getArg1());
        owner.visit(target);
    }

    // visits not handled in here
    public static void ArmorsmithAction(Player as, Player target, String vOption) {
        if(target == null)
            return;
        if(as.getGameFaction().knowsTeam() && as.getGameFaction() == target.getGameFaction()){
            Visit.NoNightActionVisit(as, target);
            return;
        }

        Game narrator = as.game;

        if(!target.isSquelched()){
            Vest v = new Vest(Charge.FROM_OTHER);
            if(FakeVests(as) && FAKE.equals(vOption)){
                v.setReal(false);
                happening(as, " gave fake " + Vest.getVestName(narrator) + " to ", target);
            }else{
                happening(as, " gave " + Vest.getVestName(narrator) + " to ", target);
            }
            target.addConsumableCharge(Survivor.abilityType, v);
            FeedbackGenerator(new Feedback(target));
        }else{
            happening(as, " tried to give " + Vest.getVestName(narrator) + " to ", target);
        }
    }

    public static void FeedbackGenerator(Feedback feedback) {
        Game game = feedback.n;
        feedback.add(GetArmorReceiveMessage(game));
        feedback.addExtraInfo("You may use this at night to protect yourself from most attacks!");
        feedback.setPicture("armorsmith");
        for(Role role: game.setup.roles){
            for(Ability ability: role.getAbilities()){
                if(ability.modifiers.getBoolean(AbilityModifierName.AS_FAKE_VESTS, game)){
                    feedback.addExtraInfo("There is a chance that your vest might not work.");
                    return;
                }
            }
        }
        for(GameFaction faction: game.getFactions()){
            AbilityList factionAbilities = faction.getAbilities();
            if(factionAbilities == null)
                continue;
            for(GameAbility ability: factionAbilities){
                if(ability.modifiers.getBoolean(AbilityModifierName.AS_FAKE_VESTS, game)){
                    feedback.addExtraInfo("There is a chance that your vest might not work.");
                }
            }
        }

    }

    @Override
    public Action parseCommand(Player p, double timeLeft, ArrayList<String> commands) {
        if(!fakeVests()){
            return super.parseCommand(p, timeLeft, commands);
        }
        if(!commands.get(commands.size() - 1).equalsIgnoreCase(FAKE)){
            return super.parseCommand(p, timeLeft, commands);
        }
        commands.remove(commands.size() - 1);
        Action action = super.parseCommand(p, timeLeft, commands);
        action.args = Util.toStringList(FAKE);
        return action;
    }

    @Override
    public String getUsage(Player p, Random r) {
        if(fakeVests())
            return super.getUsage(p, r) + " " + REAL + "\n" + super.getUsage(p, r) + " " + FAKE;
        return super.getUsage(p, r);
    }

    @Override
    public ArrayList<String> getCommandParts(Action a) {
        ArrayList<String> parts = super.getCommandParts(a);
        if(fakeVests() && FAKE.equalsIgnoreCase(a.getArg1()))
            parts.add(FAKE);

        return parts;
    }

    @Override
    public ArrayList<Object> getActionDescription(ArrayList<Action> actions) {
        ArrayList<Object> list = new ArrayList<>();
        if(actions.isEmpty())
            return list;
        PlayerList targets = ActionUtil.getActionTargets(actions);

        if(actions.get(0) != null && FAKE.equalsIgnoreCase(actions.get(0).getArg1())){
            list.add("give fake " + Vest.getVestName(narrator) + " to ");
        }else{
            list.add("give " + Vest.getVestName(narrator) + " to ");
            list.addAll(StringChoice.YouYourself(targets));
        }
        return list;
    }

    @Override
    public List<AbilityModifierName> getAbilityModifiers() {
        List<AbilityModifierName> modifierNames = new LinkedList<>();
        modifierNames.add(AbilityModifierName.AS_FAKE_VESTS);
        modifierNames.addAll(super.getAbilityModifiers());
        return modifierNames;
    }

    @Override
    protected ArrayList<String> getProfileHints(Player player) {
        ArrayList<String> ret = new ArrayList<>();

        if(player.getGameFaction().knowsTeam() && player.getGameFaction().getMembers().size() > 1){
            ret.add("Your " + Vest.getVestName(player.game) + " cannot go to teammates.");
        }
        return ret;
    }

    public static FactionRole template(Faction faction) {
        Role role = RoleService.createRole(faction.setup, "Armorsmith", abilityType);
        return FactionRoleService.createFactionRole(faction, role);
    }

    @Override
    public AbilityType getAbilityType() {
        return abilityType;
    }
}
