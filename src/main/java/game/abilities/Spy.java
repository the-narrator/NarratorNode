package game.abilities;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import game.abilities.util.AbilityValidationUtil;
import game.event.Feedback;
import game.event.Happening;
import game.logic.Game;
import game.logic.GameFaction;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.exceptions.UnknownTeamException;
import game.logic.support.CommandHandler;
import game.logic.support.Constants;
import game.logic.support.Option;
import game.logic.support.Random;
import game.logic.support.action.Action;
import game.setups.Setup;
import models.Faction;
import models.FactionRole;
import models.Role;
import models.enums.AbilityModifierName;
import models.enums.AbilityType;
import models.enums.SetupModifierName;
import models.modifiers.Modifiers;
import services.FactionRoleService;
import services.RoleService;
import util.Util;
import util.game.LookupUtil;

public class Spy extends GameAbility {

    public static final AbilityType abilityType = AbilityType.Spy;
    public static final String COMMAND = abilityType.command;

    public Spy(Game game, Setup setup, Modifiers<AbilityModifierName> modifiers) {
        super(game, setup, modifiers);
    }

    @Override
    public AbilityType getAbilityType() {
        return abilityType;
    }

    @Override
    public String getAbilityDescription(Optional<Game> game, Setup setup) {
        return NIGHT_ACTION_DESCRIPTION;
    }

    public static final String ROLE_NAME = "Spy";

    public static final String NIGHT_ACTION_DESCRIPTION = "Find out all targets of a team that you select.";

    // private boolean visited = false;
    public static final String NO_VISIT = "The faction didn't visit anyone.";
    public static final String FEEDBACK = "Your spying revealed these people as targets: ";

    @Override
    public void doNightAction(Action a) {
        Player owner = a.owner;
        String teamColor = a.getArg1();
        if(teamColor == null){
            if(owner.game.pre_visiting_phase && a.getTarget() != null)
                owner.visit(a.getTarget());
            return;
        }
        if(owner.game.pre_visiting_phase){
            if(a.getTarget() != null)
                owner.visit(a.getTarget());
        }else{
            GameFaction t = owner.game.getFaction(teamColor);
            spy(owner, t);
            Happening e = new Happening(owner.game);
            e.add(owner, " spied on ", t.getName());
            a.markCompleted();
        }

    }

    @Override
    public List<SetupModifierName> getSetupModifierNames() {
        List<SetupModifierName> names = new LinkedList<>();
        names.add(SetupModifierName.SPY_TARGETS_ALLIES);
        names.add(SetupModifierName.SPY_TARGETS_ENEMIES);
        return names;
    }

    @Override
    protected ArrayList<String> getProfileHints(Player p) {
        ArrayList<String> ret = new ArrayList<>();

        StringBuilder teamsToSpyOn = new StringBuilder();
        teamsToSpyOn.append("You can spy on the following teams: ");
        for(Option team: getOptions(p)){
            teamsToSpyOn.append(team.getName());
            teamsToSpyOn.append(", ");
        }

        teamsToSpyOn.delete(teamsToSpyOn.length() - 2, teamsToSpyOn.length());
        teamsToSpyOn.append('.');

        ret.add(teamsToSpyOn.toString());

        return ret;
    }

    @Override
    protected String getProfileChargeText() {
        int charges = getPerceivedCharges();
        if(charges == 1)
            return ("You may spy one more time.");
        else if(charges == 0)
            return ("You may not spy anymore.");
        else if(charges != Constants.UNLIMITED)
            return ("You may spy " + charges + " more times.");
        else
            return ("You may spy as many times as you wish");
    }

    public static void spy(Player owner, GameFaction target) {
        Feedback f = new Feedback(owner);
        ArrayList<Object> parts = FeedbackGenerator(target.getTargets());
        f.add(parts);
        f.setPicture("lookout");
        f.addExtraInfo("As a reminder, you tracked the movements of the " + target.getName() + ".");
    }

    public static ArrayList<Object> FeedbackGenerator(PlayerList visitors) {
        visitors.sortByName();
        ArrayList<Object> parts = new ArrayList<>();
        if(visitors.isEmpty())
            parts.add(NO_VISIT);
        else{
            parts.add(FEEDBACK);
            for(Player p: visitors){
                parts.add(p);
                if(visitors.getLast() != p)
                    parts.add(", ");
            }
        }
        return parts;
    }

    @Override
    public ArrayList<Option> getOptions(Player p) {
        ArrayList<Option> teamsToSpyOn = new ArrayList<>();
        boolean canSpyOnEnemies = p.game.getBool(SetupModifierName.SPY_TARGETS_ENEMIES);
        boolean canSpyOnAllies = p.game.getBool(SetupModifierName.SPY_TARGETS_ALLIES);
        for(GameFaction t: p.game.getFactions()){
            if(t == p.getGameFaction())
                continue;
            if(!canSpyOnEnemies && t.isEnemy(p.getGameFaction())){
                continue;
            }
            if(!canSpyOnAllies && !t.isEnemy(p.getGameFaction()))
                continue;
            teamsToSpyOn.add(new Option(t));
        }
        return teamsToSpyOn;
    }

    @Override
    public PlayerList getAcceptableTargets(Player pi) {
        if(getOptions(pi).isEmpty())
            return new PlayerList();
        return null;
    }

    @Override
    public ArrayList<Object> getActionDescription(ArrayList<Action> actionList) {
        ArrayList<Object> list = new ArrayList<>();
        Action a = actionList.get(0);
        Player owner = a.owner;

        for(Action act: actionList){
            GameFaction parse = owner.game.getFaction(act.getArg1());
            if(parse != null){
                list.add("spy on the " + parse.getName());
            }
            list.add(" and ");
        }
        list.remove(list.size() - 1);
        return list;
    }

    @Override
    public ArrayList<Object> getInsteadPhrase(Action a) {
        ArrayList<Object> list = new ArrayList<>();

        list.add("spying on the ");
        list.add(a.owner.game.getFaction(a.getArg1()).getName());

        return list;
    }

    @Override
    public ArrayList<Object> getActionDescription(Action a) {
        ArrayList<Object> list = new ArrayList<>();

        list.add("spy on the ");
        list.add(a.owner.game.getFaction(a.getArg1()));

        return list;
    }

    @Override
    public void mainAbilityCheck(Action a) {
        String color = a.getArg1();
        Optional<Faction> targetFactionOpt = LookupUtil.findFactionByColor(setup, color);
        if(!targetFactionOpt.isPresent())
            throw new UnknownTeamException(color);

        Faction targetFaction = targetFactionOpt.get();
        Faction ownerFaction = a.owner.getGameFaction().faction;

        if(!targetFaction.knowsTeam(narrator) && targetFaction == ownerFaction)
            AbilityValidationUtil.Exception("Can't spy on teams that don't know their allies.");
        if(!a.owner.game.getBool(SetupModifierName.SPY_TARGETS_ENEMIES) && targetFaction.enemies.contains(ownerFaction))
            AbilityValidationUtil.Exception("Can't spy on enemy teams.");
    }

    @Override
    public void targetSizeCheck(Action a) {
        if(!a.getTargets().isEmpty())
            AbilityValidationUtil.Exception("You can't target players with this ability");
    }

    @Override
    public ArrayList<String> getCommandParts(Action a) {
        if(a.abilityType != abilityType)
            return super.getCommandParts(a);
        ArrayList<String> parsedCommands = new ArrayList<>();
        parsedCommands.add(COMMAND);
        parsedCommands.add(a.getArg1());
        return parsedCommands;
    }

    @Override
    public Action parseCommand(Player p, double timeLeft, ArrayList<String> commands) {
        commands.remove(0);
        StringBuilder teamName = new StringBuilder();
        for(String s: commands)
            teamName.append(s);

        Game n = p.game;

        String teamColor = CommandHandler.parseTeam(teamName.toString(), n);
        if(Constants.A_INVALID.equals(teamColor))
            throw new UnknownTeamException(teamName.toString());

        return new Action(p, abilityType, timeLeft, Util.toStringList(teamColor), Player.list());
    }

    @Override
    public boolean isFakeBlockable() {
        return false;
    }

    @Override
    public Optional<Action> getExampleAction(Player owner) {
        ArrayList<Option> list = getOptions(owner);
        if(list.isEmpty())
            return Optional.empty();
        Option o = list.get(0);
        return Optional.of(new Action(owner, getAbilityType(), Util.toStringList(o.getValue()), new PlayerList()));
    }

    @Override
    public String getUsage(Player p, Random r) {
        ArrayList<Option> options = getOptions(p);
        int index = r.nextInt(options.size());
        String ret = COMMAND + " *" + options.get(index) + "*";
        if(options.size() != 1){
            ret += "\nBelow are the teams that you can spy on: ";
            for(int i = 0; i < options.size(); i++){
                ret += options.get(i);
                if(i != options.size() - 1){
                    ret += ", ";
                }
            }
        }
        return ret;
    }

    public static Role template(Setup setup) {
        return RoleService.createRole(setup, "Spy", abilityType);
    }

    public static FactionRole template(Faction faction) {
        return FactionRoleService.createFactionRole(faction, template(faction.setup));
    }

    @Override
    public boolean isRetarget(Action action, Action action2) {
        return action.getArg1().equals(action2.getArg1());
    }
}
