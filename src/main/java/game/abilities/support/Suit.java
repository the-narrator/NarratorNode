package game.abilities.support;

import game.logic.Player;
import models.FactionRole;

public class Suit {

    public Player tailor;
    public FactionRole factionRole;

    public Suit(Player tailor, FactionRole factionRole) {
        this.tailor = tailor;
        this.factionRole = factionRole;
    }

    public boolean createdBy(Player tailor) {
        return tailor == this.tailor;
    }

}
