package game.abilities.support;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

import game.abilities.GameAbility;
import game.logic.support.Constants;

public class Charges implements Iterable<Charge> {

    private boolean isUnlimitedUse;
    private ArrayList<Charge> charges;
    private GameAbility gameAbility;

    public Charges(GameAbility gameAbility) {
        this.gameAbility = gameAbility;
        this.isUnlimitedUse = true;
        this.charges = new ArrayList<>();
    }

    public Charges(GameAbility gameAbility, int initialRealChargeCount) {
        this(gameAbility);
        this.isUnlimitedUse = false;
        for(int i = 0; i < initialRealChargeCount; i++)
            this.charges.add(gameAbility.getCharge(true)); // not from another player
    }

    public boolean isUnlimited() {
        return this.isUnlimitedUse;
    }

    public void setUnlimited() {
        this.isUnlimitedUse = true;
    }

    public boolean hasCharges() {
        if(this.isUnlimitedUse)
            return true;
        return !this.charges.isEmpty();
    }

    public int getPerceivedCharges() {
        if(this.isUnlimitedUse)
            return Constants.UNLIMITED;
        return charges.size();
    }

    public int getRealCharges() {
        if(this.isUnlimitedUse)
            return Constants.UNLIMITED;
        int realCharges = 0;
        for(Charge charge: this.charges)
            if(charge.isReal())
                realCharges++;
        return realCharges;
    }

    public void useCharge() {
        if(this.isUnlimitedUse || this.charges.isEmpty())
            return;
        this.charges.remove(0);
        Collections.sort(this.charges);
    }

    public void addFakeCharge() {
        if(this.isUnlimitedUse)
            return;
        // getCharge(true) is true because
        this.charges.add(gameAbility.getCharge(true).setReal(false));
        Collections.sort(this.charges);
    }

    public int getFakeCharges() {
        if(this.isUnlimitedUse)
            return 0;
        int fakeCharges = 0;
        for(Charge charge: this.charges)
            if(charge.isFake())
                fakeCharges++;
        return fakeCharges;
    }

    public void addCharge(Charge charge) {
        this.charges.add(charge);
        Collections.sort(this.charges);
    }

    public void remove(Charge charge) {
        this.charges.remove(charge);
        Collections.sort(this.charges);
    }

    @Override
    public Iterator<Charge> iterator() {
        return this.charges.iterator();
    }

    public Charge get(int i) {
        return this.charges.get(i);
    }

    @SuppressWarnings("unchecked")
    public <T extends Charge> Iterable<T> iterable(Class<T> type) {
        return (Iterable<T>) this.charges;
    }

    @Override
    public String toString() {
        return this.charges.toString();
    }
}
