package game.abilities.support;

import java.util.Iterator;
import java.util.Optional;

import game.abilities.Baker;
import game.abilities.BreadAbility;
import game.abilities.GameAbility;
import game.abilities.Visit;
import game.event.Feedback;
import game.logic.Game;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.support.action.Action;
import game.setups.Setup;
import models.enums.SetupModifierName;

public class Bread extends Charge {

    public static final String COMMAND = BreadAbility.COMMAND;
    public static final String LABEL_NAME = "bread_alias";

    private boolean initialized = false;
    private boolean expiring = false;

    public Bread(boolean isFromSelf) {
        super(isFromSelf);
    }

    @Override
    public String toString() {
        if(isReal())
            return "Bread";
        return "FakeBread";
    }

    @Override
    public int compareTo(Charge q) {
        if(!(q instanceof Bread))
            return 0;
        Bread o = (Bread) q;
        if(o.isReal() == isReal())
            return 0;
        if(o.isReal())
            return 1;
        return -1;
    }

    public Bread initialize() {
        initialized = true;
        return this;
    }

    public boolean isInitialized() {
        return initialized;
    }

    public static void PassBread(Action a) {
        Player owner = a.owner, target = a.getTarget();
        if(target == null)
            return;
        if(owner.getGameFaction().knowsTeam() && owner.getGameFaction() == target.getGameFaction()){
            Visit.NoNightActionVisit(a);
            return;
        }

        BreadAbility ba = a.owner.getAbility(BreadAbility.class);

        if(!a.owner.game.getBool(SetupModifierName.BREAD_PASSING) && !a.owner.is(Baker.abilityType)){
            Visit.NoNightActionVisit(a);
            return;
        }

        Bread b = (Bread) ba.charges.get(0);
        ba.charges.remove(b);
        ba.charges.addCharge(Bread.ExpiringBread());

        if(b.isFake() || target.isSquelched()){
            GameAbility.happening(owner, " tried to give " + GetBreadName(a) + " to ", target);
        }else{
            Bread bread = new Bread(Charge.FROM_OTHER).initialize();
            target.addConsumableCharge(BreadAbility.abilityType, bread);
            GameAbility.happening(owner, " gave " + GetBreadName(a) + " to ", target);
            FeedbackDecorator(new Feedback(target), a);
        }

        a.markCompleted();
        owner.visit(target);
    }

    public boolean isExpired() {
        return expiring;
    }

    private static Bread ExpiringBread() {
        Bread b = new Bread(Charge.FROM_OTHER);
        b.expiring = true;
        return b;
    }

    private static String GetBreadName(Action a) {
        return GetBreadName(a.owner.game);
    }

    public static String GetBreadName(Game game) {
        return GetBreadName(Optional.of(game.players.size()), game.setup);
    }

    public static String GetBreadName(Optional<Integer> optPlayerCount, Setup setup) {
        return setup.modifiers.getString(SetupModifierName.BREAD_NAME, optPlayerCount);
    }

    public static String GetFeedback(Game n) {
        return Baker.BreadReceiveMessage(n);
    }

    public String getAbilityDescription(Game game) {
        return "pass one of your " + GetBreadName(game) + " to someone else for their use";
    }

    // TODO move this breadability
    public static void removeExpiredBread(PlayerList _players) {
        BreadAbility ba;
        for(Player p: _players){
            if(!p.hasAbility(BreadAbility.abilityType))
                continue;
            ba = p.getAbility(BreadAbility.class);
            Iterator<Bread> iterator = ba.charges.iterable(Bread.class).iterator();
            while (iterator.hasNext()){
                if(iterator.next().isExpired())
                    iterator.remove();
            }
        }

    }

    public static void FeedbackDecorator(Feedback fb, Action a) {
        fb.add(Baker.BreadReceiveMessage(a.owner.game));
        fb.setPicture("baker");

        fb.addExtraInfo("You may use this on subsequent nights(or days) to do more than one of your actions.");
        fb.addExtraInfo("For example, a mafia member could carry out 2 kills instead of just 1.");
    }

}
