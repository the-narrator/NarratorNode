package game.abilities;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import game.abilities.util.AbilityValidationUtil;
import game.event.Feedback;
import game.logic.Game;
import game.logic.Player;
import game.logic.support.Constants;
import game.logic.support.action.Action;
import game.setups.Setup;
import models.Faction;
import models.FactionRole;
import models.Role;
import models.enums.AbilityModifierName;
import models.enums.AbilityType;
import models.enums.SetupModifierName;
import models.modifiers.Modifiers;
import services.FactionRoleService;
import services.RoleService;

public class Poisoner extends GameAbility {

    public static final AbilityType abilityType = AbilityType.Poisoner;
    public static final String COMMAND = abilityType.command;

    public Poisoner(Game game, Setup setup, Modifiers<AbilityModifierName> modifiers) {
        super(game, setup, modifiers);
    }

    @Override
    public AbilityType getAbilityType() {
        return abilityType;
    }

    @Override
    public String getAbilityDescription(Optional<Game> game, Setup setup) {
        return NIGHT_ACTION_DESCRIPTION;
    }

    public static final String NIGHT_ACTION_DESCRIPTION = "Poison someone at night. Poisoned targets die at the end of the next day.";
    public static final String POISONER_FEEDBACK = "You were poisoned!";
    public static final String POISONER_FAILED_FEEDBACK = "Someone tried to poison you last night!";
    public static final String DEATH_FEEDBACK = "You succumbed to last night's poison.";
    public static final String BROADCAST_MESSAGE = "'s body couldn't fight the poison anymore, and fell over dead.";

    @Override
    public void mainAbilityCheck(Action a) {
        AbilityValidationUtil.deadCheck(a);
    }

    @Override
    public void doNightAction(Action a) {
        Player owner = a.owner, target = a.getTarget();
        if(target == null)
            return;
        if(target.game.getBool(SetupModifierName.GUARD_REDIRECTS_POISON))
            target = Bodyguard.redirect(target);
        if(owner.getGameFaction().knowsTeam() && owner.getGameFaction() == target.getGameFaction())
            return;
        happening(owner, " poisoned ", target);
        a.markCompleted();
        Kill(owner, target, Constants.POISON_KILL_FLAG);
    }

    @Override
    public List<SetupModifierName> getSetupModifierNames() {
        List<SetupModifierName> names = new LinkedList<>();
        names.add(SetupModifierName.HEAL_BLOCKS_POISON);
        names.add(SetupModifierName.GUARD_REDIRECTS_POISON);
        return names;
    }

    public static void FeedbackGenerator(Feedback fb, boolean isImmune) {
        fb.setPicture("poisoner");
        if(isImmune){
            fb.add(Poisoner.POISONER_FAILED_FEEDBACK);
            fb.addExtraInfo("Whoever poisoned you might wonder why you survived...");
        }else{
            fb.add(Poisoner.POISONER_FEEDBACK);
            fb.addExtraInfo("You won't live to see the next night.");
        }
    }

    @Override
    protected ArrayList<String> getProfileHints(Player p) {
        ArrayList<String> hasBurn = new ArrayList<>();

        if(p.game.getBool(SetupModifierName.GUARD_REDIRECTS_POISON))
            hasBurn.add("Players guarding your poisoned target will absorb the poison instead.");

        return hasBurn;
    }

    // fake poisoning, change ability to make this check overrideable
    @Override
    public boolean isNegativeAbility() {
        return true;
    }

    public static FactionRole template(Faction faction) {
        return FactionRoleService.createFactionRole(faction, template(faction.setup));
    }

    public static Role template(Setup setup) {
        return RoleService.createRole(setup, "Poisoner", Poisoner.abilityType, Bulletproof.abilityType);
    }
}
