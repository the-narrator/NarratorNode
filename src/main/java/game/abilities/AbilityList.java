package game.abilities;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import game.abilities.util.AbilityUtil;
import game.logic.Game;
import game.logic.Player;
import game.logic.exceptions.IllegalRoleCombinationException;
import game.logic.templates.BasicRoles;
import models.Ability;
import models.enums.AbilityModifierName;
import models.enums.AbilityType;
import models.modifiers.Modifiers;
import util.game.ModifierUtil;

public class AbilityList implements Iterable<GameAbility> {

    private List<GameAbility> list = new LinkedList<>();
    private Game game;

    public AbilityList(Game game, Map<AbilityType, Ability> abilityMap,
            Map<AbilityType, Modifiers<AbilityModifierName>> factionRoleAbilityModifiers) {
        Map<AbilityType, Modifiers<AbilityModifierName>> mergedModifiers = ModifierUtil
                .mergeAbilityModifiers(game.players.size(), abilityMap, factionRoleAbilityModifiers);

        GameAbility gameAbility;
        for(AbilityType abilityType: abilityMap.keySet()){
            gameAbility = AbilityUtil.CREATOR(abilityType, game, mergedModifiers.get(abilityType));
            list.add(gameAbility);
        }
        this.game = game;
        checkList();
    }

    public AbilityList(GameAbility... r) {
        for(GameAbility a: r)
            add(a);
        checkList();
    }

    public AbilityList(List<GameAbility> aList) {
        for(GameAbility a: aList)
            add(a);
        checkList();
    }

    public AbilityList(AbilityList list, GameAbility... others) {
        for(GameAbility a: list)
            add(a);
        for(GameAbility a: others)
            add(a);
    }

    public AbilityList(AbilityList abilities, AbilityList attainedAbilities) {
        for(GameAbility a: abilities)
            add(a);
        for(GameAbility a: attainedAbilities)
            add(a);
    }

    private void checkList() {
        int size = 0;
        for(GameAbility a: this){
            if(a instanceof Vote)
                continue;
            size++;
        }

        if(contains(Amnesiac.abilityType) && size != 1)
            throw new IllegalRoleCombinationException("Amnesiac cannot be combined with other roles");
    }

    public static String GetTitleCaseName(String s) {
        StringBuilder sb = new StringBuilder();

        String[] split = s.split("(?=\\p{Upper})");
        for(int i = 0; i < split.length; i++){
            if(i != 0)
                sb.append(" ");
            sb.append(split[i]);
        }
        return sb.toString();
    }

    public void add(GameAbility a) {
        if(!contains(a.getAbilityType()))
            list.add(a);
    }

    @Override
    public Iterator<GameAbility> iterator() {
        List<GameAbility> sortedList = new LinkedList<>(list);
        Collections.sort(sortedList, new Comparator<GameAbility>() {

            // sorted output so sc2mafia's BBC output is consistent
            @Override
            public int compare(GameAbility o1, GameAbility o2) {
                return o1.getName().compareTo(o2.getName());
            }

        });

        return sortedList.iterator();
    }

    public int size() {
        return list.size();
    }

    public GameAbility get(int i) {
        return list.get(i);
    }

    public boolean isEmpty() {
        return list.isEmpty();
    }

    public AbilityList getNonpassive() {
        AbilityList aList = new AbilityList();
        for(GameAbility a: this)
            if(a.getAbilityType().command != null)
                aList.add(a);

        return aList;
    }

    // used for witch
    public AbilityList filterHasCharge() {
        AbilityList aList = new AbilityList();
        for(GameAbility a: this)
            if(a.getRealCharges() != 0)
                aList.add(a);

        return aList.getNonpassive();
    }

    public AbilityList filterKnown() {
        AbilityList aList = new AbilityList();
        for(GameAbility a: this){
            if(!a.isHiddenPassive())
                aList.add(a);
        }

        return aList;
    }

    public AbilityList filterNot(AbilityType abilityType) {
        AbilityList aList = new AbilityList();
        for(GameAbility a: this){
            if(!a.is(abilityType))
                aList.add(a);
        }

        return aList;
    }

    public AbilityList getNightAbilities(Player p) {
        AbilityList aList = new AbilityList();
        for(GameAbility a: this)
            if(a.isNightAbility(p))
                aList.add(a);

        return aList.getNonpassive();
    }

    public AbilityList getDayAbilities() {
        AbilityList aList = new AbilityList();
        for(GameAbility a: this)
            if(a.isDayAbility())
                aList.add(a);

        return aList.getNonpassive();
    }

    public GameAbility getFirst() {
        if(isEmpty())
            return null;
        return get(0);
    }

    public boolean contains(AbilityType abilityType) {
        for(GameAbility a: this)
            if(a.getAbilityType() == abilityType)
                return true;
        return false;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for(int i = 0; i < size(); i++){
            if(i != 0)
                sb.append(",\n");
            sb.append(get(i).getClass().getSimpleName());
        }
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if(o == null)
            return false;
        if(!o.getClass().equals(getClass()))
            return false;

        return AbilityUtil.isEqualContent(this, (AbilityList) o);
    }

    public GameAbility getAbility(String abilityName) {
        for(GameAbility a: this){
            if(a.getClass().getSimpleName().equalsIgnoreCase(abilityName))
                return a;
        }
        return null;
    }

    public GameAbility getAbility(Class<? extends GameAbility> abilityClass) {
        for(GameAbility a: this){
            if(a.getClass() == abilityClass)
                return a;
        }
        return null;
    }

    public AbilityList replace(GameAbility oldR, GameAbility newR) {
        AbilityList aList = new AbilityList(this.list);
        aList.list.add(newR);
        for(int i = 0; i < list.size(); i++){
            if(aList.list.get(i).getClass().equals(oldR.getClass())){
                aList.list.remove(i);
                break;
            }
        }
        return aList;
    }

    public String getDatabaseName() {
        StringBuilder ret = new StringBuilder();

        for(GameAbility a: list){
            if(a.is(Burn.abilityType))
                ret.append(BasicRoles.ARSONIST);
            else if(a.isDatabaseAbility())
                ret.append(a.getClass().getSimpleName());
            else
                continue;
            break;
        }

        if(ret.length() == 0 && !list.isEmpty())
            ret.append(list.get(0).getClass().getSimpleName());

        return ret.toString();
    }

    public AbilityList remove(Class<? extends GameAbility> class1) {
        AbilityList aList = new AbilityList();
        for(GameAbility a: this)
            if(a.getClass() != class1)
                aList.add(a);

        return aList;
    }

    @Override
    public int hashCode() {
        String rep = "";
        ArrayList<String> ability_s = new ArrayList<>();
        for(GameAbility a: this.list)
            ability_s.add(a.getClass().getSimpleName());

        Collections.sort(ability_s);
        rep += ability_s.hashCode();
        return rep.hashCode();
    }

    public GameAbility[] toArray() {
        GameAbility[] array = new GameAbility[this.list.size()];
        for(int i = 0; i < array.length; i++){
            array[i] = this.list.get(i);
        }
        return array;
    }

    // every role needs to be a can start with enemies for this to return true
    public boolean canStartWithEnemies() {
        for(GameAbility ability: this){
            if(!ability.canStartWithEnemies())
                return false;
        }
        return true;
    }

    public boolean isNightless() {
        for(GameAbility ability: this)
            if(!ability.isNightless(Optional.of(game.players.size())))
                return false;
        return true;
    }

}
