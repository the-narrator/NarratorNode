package game.abilities;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import game.abilities.support.Suit;
import game.abilities.util.AbilityUtil;
import game.abilities.util.AbilityValidationUtil;
import game.event.Feedback;
import game.event.Happening;
import game.logic.Game;
import game.logic.GameFaction;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.exceptions.PlayerTargetingException;
import game.logic.exceptions.UnknownRoleException;
import game.logic.support.Constants;
import game.logic.support.HTString;
import game.logic.support.Option;
import game.logic.support.Random;
import game.logic.support.StringChoice;
import game.logic.support.action.Action;
import game.setups.Setup;
import models.Faction;
import models.FactionRole;
import models.Role;
import models.enums.AbilityModifierName;
import models.enums.AbilityType;
import models.enums.SetupModifierName;
import models.modifiers.Modifiers;
import services.FactionRoleService;
import services.RoleService;
import util.Util;
import util.game.LookupUtil;

public class Tailor extends GameAbility {

    public static final AbilityType abilityType = AbilityType.Tailor;
    public static final String COMMAND = abilityType.command;

    public Tailor(Game game, Setup setup, Modifiers<AbilityModifierName> modifiers) {
        super(game, setup, modifiers);
    }

    @Override
    public AbilityType getAbilityType() {
        return abilityType;
    }

    @Override
    public String getAbilityDescription(Optional<Game> game, Setup setup) {
        return NIGHT_ACTION_DESCRIPTION;
    }

    public static final String NIGHT_ACTION_DESCRIPTION = "Give suits to people.  Upon death, they'll look like a different role in the graveyard.";

    public static final String FEEDBACK = "You have been given a suit!";

    public static ArrayList<Option> getOptions(Game game) {
        ArrayList<Option> teams = new ArrayList<>();
        for(GameFaction t: game.getFactions())
            teams.add(new Option(t));

        return teams;
    }

    public static ArrayList<Option> getOptions2(Game game, String team) {
        String color;
        if(game.getFaction(team) != null)
            color = team;
        else
            color = game.getFactionByName(team).getColor();
        Faction faction = game.setup.getFactionByColor(color);

        ArrayList<Option> roleNames = new ArrayList<>();
        for(FactionRole m: faction.getFactionRoles())
            roleNames.add(new Option(m.role.getName()).setColor(team));
        return roleNames;
    }

    @Override
    public ArrayList<Option> getOptions(Player player) {
        return getOptions(player.game);
    }

    @Override
    public ArrayList<Option> getOptions2(Player p, String team) {
        return getOptions2(p.game, team);
    }

    @Override
    public void mainAbilityCheck(Action action) {
        Game game = action.owner.game;
        AbilityValidationUtil.deadCheck(action);

        String roleName = action.getArg2();
        String factionLookup = action.getArg1();
        // throws an error if faction role isn't found
        Optional<FactionRole> factionRole = LookupUtil.findFactionRole(game.setup, roleName, factionLookup);
        if(!factionRole.isPresent())
            throw new UnknownRoleException(roleName + " is not a known role name for " + factionLookup + ".");
    }

    @Override
    public void doNightAction(Action a) {
        if(a.getTarget() == null)
            return;
        String factionArg = a.getArg1();
        String roleNameArg = a.getArg2();

        Optional<FactionRole> factionRoleOpt = LookupUtil.findFactionRole(narrator.setup, roleNameArg, factionArg);
        if(!factionRoleOpt.isPresent()){
            Visit.NoNightActionVisit(a);
            return;
        }

        FactionRole factionRole = factionRoleOpt.get();
        Player target = a.getTarget();

        target.addSuite(new Suit(a.owner, factionRole));
        if(target.game.getBool(SetupModifierName.TAILOR_FEEDBACK))
            FeedbackGenerator(new Feedback(target));

        Happening h = new Happening(a.owner.game);
        h.add(a.owner);
        h.add(" gave a ");
        h.add(new HTString(factionRole));
        h.add(" suit to ");
        h.add(target);
        h.add(".");

        a.markCompleted();
        a.owner.visit(target);
    }

    @Override
    public void onDeath(Player tailor) {
        for(Player p: tailor.game.getLivePlayers())
            p.removeSuits(tailor);
    }

    @Override
    protected String getProfileChargeText() {

        int charges = getPerceivedCharges();
        if(charges == Constants.UNLIMITED){
            return ("You can give out as many suits as you want.");
        }else if(charges == 0){
            return ("You can't give out any suits.");
        }else if(charges == 1){
            return ("You can give out " + charges + " more suit.");
        }else{
            return ("You can give out " + charges + " more suits.");
        }
    }

    @Override
    public ArrayList<String> getCommandParts(Action a) {
        ArrayList<String> parsedCommands = new ArrayList<>();
        PlayerList targets = a.getTargets().copy();

        parsedCommands.add(COMMAND);
        for(Player target: targets){
            parsedCommands.add(target.getName());
        }
        parsedCommands.add(a.getArg1());
        parsedCommands.add(a.getArg2());
        return parsedCommands;
    }

    @Override
    public Action parseCommand(Player p, double timeLeft, ArrayList<String> commands) {
        commands.remove(0);

        if(commands.isEmpty())
            throw new PlayerTargetingException("Need to target someone to give a suit");

        Player target = narrator.getPlayerByName(commands.remove(0));

        if(commands.isEmpty())
            throw new PlayerTargetingException("Need to selection what team this suit is from.");

        GameFaction t = GameFaction.GetFaction(commands, narrator);
        // if successful, commands will be edited. if not, commands will be unchanged.

        if(t == null)
            throw new PlayerTargetingException("Need to select what team this suit is from.");

        String roleName = AbilityUtil.GetRoleName(commands, setup);

        if(commands.isEmpty() && roleName == null)
            throw new PlayerTargetingException("Need to select what role this suit is");

        return new Action(p, abilityType, timeLeft, Util.toStringList(t.getColor(), roleName), Player.list(target));
    }

    @Override
    public ArrayList<Object> getActionDescription(ArrayList<Action> actionList) {
        ArrayList<Object> phrase = new ArrayList<>();
        for(Action da: actionList){
            phrase.add(COMMAND.toLowerCase());
            phrase.add(" ");
            phrase.add(StringChoice.YouYourselfSingle(da.getTarget()));

            phrase.add(" as ");
            phrase.add(
                    new HTString(da.owner.game.getFaction(da.getArg1()).getName() + " " + da.getArg2(), da.getArg1()));

            phrase.add(" and ");
        }
        phrase.remove(phrase.size() - 1);

        return phrase;
    }

    @Override
    public String getUsage(Player p, Random r) {
        ArrayList<Option> options = getOptions(p);
        int index = r.nextInt(options.size());
        ArrayList<Option> option2s = getOptions2(p, options.get(index).getValue());
        int index2 = r.nextInt(option2s.size());

        return super.getUsage(p, r) + " " + options.get(index) + " " + option2s.get(index2)
                + "\nAny team/role name combinations are valid";
    }

    @Override
    protected ArrayList<String> getProfileHints(Player p) {
        ArrayList<String> ret = new ArrayList<>();

        if(p.game.getBool(SetupModifierName.TAILOR_FEEDBACK))
            ret.add("Your targets will know they received your suit.");
        else
            ret.add("Your targets will not know they received your suit.");

        if(p.game.getBool(SetupModifierName.SNITCH_PIERCES_SUIT))
            ret.add("Players that get revealed won't show your suit.");
        else
            ret.add("Players that get revealed will show your suit instead.");

        return ret;
    }

    @Override
    public List<SetupModifierName> getSetupModifierNames() {
        List<SetupModifierName> names = new LinkedList<>();
        names.add(SetupModifierName.TAILOR_FEEDBACK);
        names.add(SetupModifierName.SNITCH_PIERCES_SUIT);
        return names;
    }

    public static void FeedbackGenerator(Feedback fb) {
        Player target = fb.player;
        fb.add(Tailor.FEEDBACK);
        fb.setPicture("mayor");
        fb.addExtraInfo("When you die, you'll probably look like something other than a " + target.getRoleName() + ".");
    }

    @Override
    public Optional<Action> getExampleAction(Player owner) {
        return Optional.of(new Action(owner, abilityType, owner.getColor(), owner.getRoleName(), owner));
    }

    @Override
    public Optional<Action> getExampleWitchAction(Player owner, Player target) {
        PlayerList targets = Player.list(target);
        Action action = new Action(owner, Tailor.abilityType, owner.getColor(), owner.getRoleName(), targets);
        return Optional.of(action);
    }

    @Override
    public boolean showSelfTargetTextDefault() {
        return true;
    }

    public static FactionRole template(Faction faction) {
        return FactionRoleService.createFactionRole(faction, template(faction.setup));
    }

    public static Role template(Setup setup) {
        return RoleService.createRole(setup, "Tailor", abilityType);
    }
}
