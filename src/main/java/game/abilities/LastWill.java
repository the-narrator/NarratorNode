package game.abilities;

import models.serviceResponse.AbilityMetadata;

public class LastWill {

    public static AbilityMetadata getMetadata() {
        return new AbilityMetadata("lw", "set your last will", "lw Looks like I'm dead", true);
    }

}
