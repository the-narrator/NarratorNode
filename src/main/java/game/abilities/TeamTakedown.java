package game.abilities;

import java.util.HashMap;
import java.util.Optional;

import game.event.Happening;
import game.logic.Game;
import game.logic.GameFaction;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.support.Constants;
import game.logic.support.attacks.Attack;
import game.logic.support.attacks.IndirectAttack;
import game.setups.Setup;
import models.enums.AbilityModifierName;
import models.enums.AbilityType;
import models.modifiers.Modifiers;

public class TeamTakedown extends Passive {

    public static final AbilityType abilityType = AbilityType.TeamTakedown;

    public TeamTakedown(Game game, Setup setup, Modifiers<AbilityModifierName> modifiers) {
        super(game, setup, modifiers);
    }

    @Override
    public AbilityType getAbilityType() {
        return abilityType;
    }

    // the reason why this is done on night phase, is so people won't know who not
    // to target, because they might be dead or not.
    // it makes sense, not to do it immediately post lynch

    @Override
    public String getAbilityDescription(Optional<Game> game, Setup setup) {
        return NIGHT_ACTION_DESCRIPTION;
    }

    public static final String NIGHT_ACTION_DESCRIPTION = "Upon death, half of your remaining team members, rounding down, will suicide.";

    @Override
    public void onDayStart(Player p) {
        super.onDayStart(p);
    }

    private static void massSuicide(HashMap<Player, GameFaction> teams, PlayerList willDie) {
        double teamSize;
        PlayerList pl;
        Player sPlayer;
        GameFaction t;
        for(Player q: teams.keySet()){
            t = teams.get(q);
            teamSize = 0.0;
            pl = t.getMembers().getLivePlayers().shuffle(q.game.getRandom(), "mass suicide");
            pl.remove(willDie);
            for(Player p: pl){
                if(p.getGameFaction() == t)
                    teamSize += 0.5;
            }
            for(int i = 0; i < (int) teamSize; i++){
                sPlayer = pl.get(i);
                sPlayer.kill(new IndirectAttack(Constants.JESTER_KILL_FLAG, sPlayer, null));
                new Happening(q.game).add(sPlayer, " suicided over the death of ", q, ".");
            }
        }
    }

    public static void handleNightSuicides(Game n) {
        int day = n.getDayNumber();

        PlayerList willDie = new PlayerList();
        for(Player player: n.players){
            if(!player.isAlive()){
                if(player.getDeathDay() == day) // day kill
                    willDie.add(player);
                continue;
            }
            for(Attack attack: player.getInjuries()){
                if(attack.isCountered() || attack.isImmune())
                    continue;
                if(player.getRealAutoVestCount() > 0 && attack.getType() != Constants.JAIL_KILL_FLAG)
                    continue;
                if(attack.getType() == Constants.POISON_KILL_FLAG)
                    continue;
                willDie.add(player);
                break;
            }
        }

        HashMap<Player, GameFaction> suicideCausers = new HashMap<>();
        for(Player q: willDie){
            if(!suicideCausers.containsValue(q.getGameFaction()) && q.getRoleAbilities().contains(TeamTakedown.abilityType))
                suicideCausers.put(q, q.getGameFaction());
        }

        massSuicide(suicideCausers, willDie);
    }

    @Override
    public boolean isDatabaseAbility() {
        return false;
    }

    @Override
    public boolean isNightless(Optional<Integer> playerCount) {
        return true;
    }

    @Override
    public boolean isHiddenModifiable() {
        return true;
    }
}
