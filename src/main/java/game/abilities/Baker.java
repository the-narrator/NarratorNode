package game.abilities;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import game.abilities.support.Bread;
import game.abilities.support.Charge;
import game.logic.Game;
import game.logic.Player;
import game.logic.support.Constants;
import game.logic.support.StringChoice;
import game.logic.support.action.Action;
import game.setups.Setup;
import models.Faction;
import models.FactionRole;
import models.Role;
import models.enums.AbilityModifierName;
import models.enums.AbilityType;
import models.enums.SetupModifierName;
import models.modifiers.Modifiers;
import services.FactionRoleService;
import services.RoleService;
import util.Util;
import util.game.GameUtil;

public class Baker extends Passive {

    public static final AbilityType abilityType = AbilityType.Baker;

    public Baker(Game game, Setup setup, Modifiers<AbilityModifierName> modifiers) {
        super(game, setup, modifiers);
    }

    @Override
    public AbilityType getAbilityType() {
        return abilityType;
    }

    @Override
    public boolean isPowerRole() {
        return true;
    }

    @Override
    public String getAbilityDescription(Optional<Game> game, Setup setup) {
        Optional<Integer> playerCount = GameUtil.getPlayerCount(game);
        return "Give " + Bread.GetBreadName(playerCount, setup)
                + " to people at night so they can perform additional actions other nights.";
    }

    public static String BreadReceiveMessage(Game n) {
        return "You received " + Bread.GetBreadName(n) + "!";
    }

    @Override
    public ArrayList<Object> getActionDescription(Action action) {
        ArrayList<Object> list = new ArrayList<>();

        list.add(" give " + Bread.GetBreadName(player.game) + " to ");
        list.add(StringChoice.YouYourself(action.getTargets()));

        return list;
    }

    @Override
    protected ArrayList<String> getProfileHints(Player player) {
        ArrayList<String> ret = new ArrayList<>();

        if(player.getGameFaction().knowsTeam() && player.getGameFaction().getMembers().size() > 1){
            ret.add(Util.TitleCase(Bread.GetBreadName(player.game)) + " that you make cannot go to teammates.");
        }

        if(player.game.getBool(SetupModifierName.SELF_BREAD_USAGE)){
            ret.add("You may use or hold onto the " + Bread.GetBreadName(player.game) + " that you make");
        }

        return ret;
    }

    @Override
    public List<SetupModifierName> getSetupModifierNames() {
        List<SetupModifierName> names = new LinkedList<>();
        names.add(SetupModifierName.BREAD_PASSING);
        names.add(SetupModifierName.SELF_BREAD_USAGE);
        return names;
    }

    @Override
    public void onNightStart(Player p) {
        super.onNightStart(p);
        if(!isOnCooldown() && getPerceivedCharges() != 0){
            Bread b = new Bread(Charge.FROM_ROLE);
            if(getRealCharges() == 0)
                b.setReal(false);
            p.addConsumableCharge(BreadAbility.abilityType, b);
        }
    }

    @Override
    public String getProfileCooldownText(Player p) {
        int cd = getAbilityCooldown();
        if(cd == 0)
            return null;
        String breadName = Bread.GetBreadName(p.game);
        if(cd == 1)
            return "You will receive a passable " + breadName + " every other day";
        return "You will receive a passable " + breadName + " that may be used every " + cd + " days";
    }

    @Override
    public void onDayStart(Player p) {
        super.onDayStart(p);

        BreadAbility ba = p.getAbility(BreadAbility.class);
        if(ba == null)
            return;
        Iterator<Bread> iterator = ba.charges.iterable(Bread.class).iterator();
        Bread b;
        while (iterator.hasNext()){
            b = iterator.next();
            if(b.isInitialized())
                continue;
            if(p.game.getBool(SetupModifierName.SELF_BREAD_USAGE)){
                b.initialize();
                triggerCooldown();
                useCharge();
            }else
                iterator.remove();

            return;
        }

        if(getRemainingCooldown() == 0){
            triggerCooldown();
            useCharge();
        }
    }

    @Override
    public boolean hasCooldownAbility() {
        return true;
    }

    @Override
    public boolean isChargeModifiable() {
        return true;
    }

    @Override
    public String getChargeDescription(Optional<Integer> playerCount, Setup setup) {
        return "Charge count for " + Bread.GetBreadName(playerCount, setup) + " making";
    }

    @Override
    public boolean isActivePassive(Player p) {
        return this.modifiers.getInt(AbilityModifierName.CHARGES, p.game) != Constants.UNLIMITED;
    }

    @Override
    public void onAbilityLoss(Player p) {
        BreadAbility ba = p.getAbility(BreadAbility.class);
        Iterator<Bread> iterator = ba.charges.iterable(Bread.class).iterator();
        while (iterator.hasNext()){
            if(!iterator.next().isInitialized())
                iterator.remove();
        }
    }

    public static FactionRole template(Faction town) {
        return FactionRoleService.createFactionRole(town, template(town.setup));
    }

    public static Role template(Setup setup) {
        return RoleService.createRole(setup, "Baker", abilityType);
    }
}
