package game.abilities;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Optional;

import game.abilities.support.Bread;
import game.abilities.support.Charge;
import game.abilities.support.Gun;
import game.abilities.support.Vest;
import game.abilities.util.AbilityValidationUtil;
import game.event.Feedback;
import game.event.Happening;
import game.logic.Game;
import game.logic.Player;
import game.logic.exceptions.PlayerTargetingException;
import game.logic.exceptions.UnknownPlayerException;
import game.logic.support.Constants;
import game.logic.support.Option;
import game.logic.support.Random;
import game.logic.support.action.Action;
import game.setups.Setup;
import models.Faction;
import models.FactionRole;
import models.Role;
import models.enums.AbilityModifierName;
import models.enums.AbilityType;
import models.enums.SetupModifierName;
import models.modifiers.Modifiers;
import services.FactionRoleService;
import services.RoleService;
import util.Util;

public class DrugDealer extends GameAbility {
    public static final AbilityType abilityType = AbilityType.DrugDealer;
    public static final String COMMAND = abilityType.command;

    public DrugDealer(Game game, Setup setup, Modifiers<AbilityModifierName> modifiers) {
        super(game, setup, modifiers);
    }

    public static final String ROLE_NAME = "Drug Dealer";
    public static boolean SAFETY = true;

    @Override
    public AbilityType getAbilityType() {
        return abilityType;
    }

    @Override
    public String getAbilityDescription(Optional<Game> game, Setup setup) {
        return NIGHT_ACTION_DESCRIPTION;
    }

    public static final String NIGHT_ACTION_DESCRIPTION = "At night, provide false feedback, giving people false clues.";

    public static final String WIPE = "wiped";
    public static final String BLOCKED = "blocked";
    public static final String DOUSED = "doused";
    public static final String DOCTOR = "healed";
    public static final String DRIVEN = "swapped";
    public static final String GUARDED = "guarded";
    public static final String POISONED = "poisoned";
    public static final String WITCH = "witched";
    public static final String VEST = "vest";
    public static final String GUN_RECEIVED = "gun";
    public static final String BREAD = "bread";
    public static final String CHARGED = "charged";
    public static final String SUITTED = "suited";
    public static final String ANNOYED = "annoyed";

    public static final String[] DRUGS = { WIPE, BLOCKED, DOUSED, DOCTOR, DRIVEN, GUARDED, POISONED, WITCH, VEST,
            GUN_RECEIVED, BREAD, CHARGED, SUITTED, ANNOYED };

    @Override
    public void mainAbilityCheck(Action a) {
        AbilityValidationUtil.deadCheck(a);

        String drug = a.getArg1();
        if(drug == null){
            AbilityValidationUtil.Exception("Must select a drug");
        }
        drug = drug.toLowerCase();
        if(!Arrays.asList(DRUGS).contains(drug)){
            AbilityValidationUtil.Exception("Invalid drug");
        }

        if(SAFETY && !possibleDrug(drug, a.owner.game))
            AbilityValidationUtil.Exception("Drug not possible!");
    }

    public static ArrayList<String> getAvailableDrugs(Game game) {
        ArrayList<String> possibleDrugs = new ArrayList<>();
        for(String drug: DRUGS){
            if(possibleDrug(drug, game)){
                possibleDrugs.add(drug);
            }
        }

        return possibleDrugs;
    }

    protected static ArrayList<Option> getDrugOptions(Game game) {
        ArrayList<Option> drugs = new ArrayList<>();
        for(String s: getAvailableDrugs(game))
            drugs.add(new Option(s));
        return drugs;
    }

    @Override
    public ArrayList<Option> getOptions(Player owner) {
        return getDrugOptions(owner.game);
    }

    public static boolean possibleDrug(String drug, Game game) {
        if(drug.equalsIgnoreCase(WIPE))
            return true;
        Setup setup = game.setup;
        if(drug.equalsIgnoreCase(BLOCKED) && game.getBool(SetupModifierName.BLOCK_FEEDBACK)
                && setup.hasPossibleFactionRoleWithAbility(game, Block.abilityType))
            return true;
        if(drug.equalsIgnoreCase(DOUSED) && game.getBool(SetupModifierName.DOUSE_FEEDBACK)
                && setup.hasPossibleFactionRoleWithAbility(game, Douse.abilityType))
            return true;
        if(drug.equalsIgnoreCase(DOCTOR) && game.getBool(SetupModifierName.HEAL_FEEDBACK)
                && setup.hasPossibleFactionRoleWithAbility(game, Doctor.abilityType))
            return true;
        if(drug.equalsIgnoreCase(DRIVEN) && setup.hasPossibleFactionRoleWithAbility(game, Driver.abilityType))
            return true;
        if(drug.equalsIgnoreCase(GUARDED) && setup.hasPossibleFactionRoleWithAbility(game, Bodyguard.abilityType))
            return true;
        if(drug.equalsIgnoreCase(POISONED) && setup.hasPossibleFactionRoleWithAbility(game, Poisoner.abilityType))
            return true;
        if(drug.equalsIgnoreCase(WITCH) && game.getBool(SetupModifierName.WITCH_FEEDBACK)
                && setup.hasPossibleFactionRoleWithAbility(game, Witch.abilityType))
            return true;
        boolean hasBlacksmith = setup.hasPossibleFactionRoleWithAbility(game, Blacksmith.abilityType);
        if(drug.equalsIgnoreCase(VEST)
                && (hasBlacksmith || setup.hasPossibleFactionRoleWithAbility(game, Armorsmith.abilityType)))
            return true;
        if(drug.equalsIgnoreCase(GUN_RECEIVED)
                && (hasBlacksmith || setup.hasPossibleFactionRoleWithAbility(game, Gunsmith.abilityType)))
            return true;
        if(drug.equalsIgnoreCase(BREAD) && setup.hasPossibleFactionRoleWithAbility(game, Baker.abilityType))
            return true;
        if(drug.equalsIgnoreCase(CHARGED) && setup.hasPossibleFactionRoleWithAbility(game, ElectroManiac.abilityType))
            return true;
        if(drug.equalsIgnoreCase(SUITTED) && game.getBool(SetupModifierName.TAILOR_FEEDBACK)
                && setup.hasPossibleFactionRoleWithAbility(game, Tailor.abilityType))
            return true;
        if(drug.equalsIgnoreCase(ANNOYED) && game.getBool(SetupModifierName.JESTER_CAN_ANNOY)
                && setup.hasPossibleFactionRoleWithAbility(game, Jester.abilityType))
            return true;
        return false;
    }

    @Override
    public void doNightAction(Action a) {
        Player owner = a.owner, target = a.getTarget();
        if(target == null)
            return;

        Game n = owner.game;

        String option = a.getArg1();
        if(option == null){
            Visit.NoNightActionVisit(a);
            return;
        }
        Feedback fb = new Feedback(target);

        String happeningText = "";
        if(option.equalsIgnoreCase(BLOCKED)){
            Block.FeedbackGenerator(fb);
            happeningText = " with a role block";
        }else if(option.equalsIgnoreCase(ANNOYED)){
            fb.add(Jester.FEEDBACK);
            happeningText = " with an annoyance";
            fb.setPicture("jester");
        }else if(option.equalsIgnoreCase(DOUSED)){
            Burn.FeedbackGenerator(fb);
            happeningText = " with a gasoline douse";
        }else if(option.equalsIgnoreCase(CHARGED)){
            ElectroManiac.FeedbackGenerator(fb);
            happeningText = " with an electrical charge";
        }else if(option.equalsIgnoreCase(SUITTED)){
            Tailor.FeedbackGenerator(fb);
            happeningText = " with a suit";
        }else if(option.equalsIgnoreCase(DOCTOR)){
            if(target.isInvulnerable())
                fb.add(Constants.NIGHT_IMMUNE_TARGET_FEEDBACK);
            else
                fb.add(Doctor.TARGET_FEEDBACK);

            happeningText = " with a doctor heal";
            fb.setPicture("doctor");
        }else if(option.equalsIgnoreCase(GUARDED)){
            fb.add(Bodyguard.TARGET_FEEDBACK);
            happeningText = " with a bodyguard save";
            fb.setPicture("bodyguard");
        }else if(option.equalsIgnoreCase(POISONED)){
            Poisoner.FeedbackGenerator(fb, target.isInvulnerable());
            if(target.isInvulnerable()){
                happeningText = " with a failed poison attempt";
            }else{
                happeningText = " with poisoned";
            }
        }else if(option.equalsIgnoreCase(VEST)){
            fb.add(Armorsmith.GetArmorReceiveMessage(n));
            happeningText = " with a " + Vest.getVestName(narrator);
            if(!target.isSquelched())
                target.addConsumableCharge(Survivor.abilityType, Vest.FakeVest(Charge.FROM_OTHER));

        }else if(option.equalsIgnoreCase(GUN_RECEIVED)){
            fb.add(Gunsmith.GetGunReceiveMessage(n));
            happeningText = " with a " + Gun.getGunName(narrator);
            if(!target.isSquelched()){
                Gun g = Gun.DrugGun();
                g.setReal(false);
                target.addConsumableCharge(Vigilante.abilityType, g);
            }
            fb.setPicture("armorsmith");
        }else if(option.equalsIgnoreCase(BREAD)){
            happeningText = " with " + Bread.GetBreadName(narrator);
            Bread.FeedbackDecorator(fb, a);
            if(!a.getTarget().isSquelched()){
                Bread bread = new Bread(Charge.FROM_OTHER);
                bread.initialize();
                bread.setReal(false);
                target.addConsumableCharge(BreadAbility.abilityType, bread);
            }

        }else if(option.equalsIgnoreCase(WIPE)){
            Happening e = new Happening(owner.game);
            e.add(owner, " wiped the feedback of ", target, ".");
            fb.add(WIPE);
            a.markCompleted();
            owner.visit(target);
            return;
        }

        Happening e = new Happening(owner.game);
        e.add(owner, " drugged ", target, happeningText, ".");
        a.markCompleted();
        owner.visit(target);
    }

    @Override
    public ArrayList<String> getCommandParts(Action a) {
        ArrayList<String> parsedCommands = new ArrayList<>();
        parsedCommands.add(COMMAND);
        parsedCommands.add(a.getTarget().getName());
        parsedCommands.add(a.getArg1());
        return parsedCommands;
    }

    @Override
    public Action parseCommand(Player p, double timeLeft, ArrayList<String> commands) {
        if(commands.size() != 3){
            throw new PlayerTargetingException("You need a player target and a drug type to deal drugs.");
        }
        commands.remove(0);
        Game n = p.game;

        String playerName = commands.remove(0);
        Player pi = n.getPlayerByName(playerName);
        if(pi == null)
            throw new UnknownPlayerException(playerName + " isn't someone in the game.");
        String drug = commands.get(0);

        return new Action(p, abilityType, timeLeft, Util.toStringList(drug), Player.list(pi));
    }

    private static final String WIPING_EXPLANATION = "Wiping stops the player from receiving almost all feedback.";

    @Override
    protected ArrayList<String> getProfileHints(Player p) {
        ArrayList<String> ret = new ArrayList<>();
        ret.add(WIPING_EXPLANATION);
        return ret;
    }

    @Override
    public boolean isFakeBlockable() {
        return false;
    }

    @Override
    public String getUsage(Player p, Random r) {
        ArrayList<Option> options = getOptions(p);
        int index = r.nextInt(options.size());
        String ret = super.getUsage(p, r) + " " + options.get(index);
        if(options.size() != 1){
            ret += "\nAvailable drugs: ";
            for(int i = 0; i < options.size(); i++){
                ret += options.get(i);
                if(i != options.size() - 1){
                    ret += ", ";
                }
            }
        }
        return ret;
    }

    public static FactionRole template(Faction faction) {
        return FactionRoleService.createFactionRole(faction, template(faction.setup));
    }

    public static Role template(Setup setup) {
        return RoleService.createRole(setup, "Drug Dealer", DrugDealer.abilityType);
    }
}
