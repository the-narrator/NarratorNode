package game.abilities;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import game.abilities.util.AbilityValidationUtil;
import game.logic.Game;
import game.logic.Player;
import game.logic.support.Constants;
import game.logic.support.action.Action;
import game.logic.support.saves.Heal;
import game.setups.Setup;
import models.Faction;
import models.FactionRole;
import models.Role;
import models.enums.AbilityModifierName;
import models.enums.AbilityType;
import models.enums.SetupModifierName;
import models.modifiers.Modifiers;
import services.FactionRoleService;
import services.RoleService;

public class Doctor extends GameAbility {

    public static final AbilityType abilityType = AbilityType.Doctor;
    public static final String COMMAND = abilityType.command;

    public Doctor(Game game, Setup setup, Modifiers<AbilityModifierName> modifiers) {
        super(game, setup, modifiers);
    }

    public static final String ROLE_DETAIL_POISON_CANNOT_STOP = "Healing can't stop poisoning";
    public static final String ROLE_DETAIL_POISON_CAN_STOP = "Healing can stop poisonging";

    @Override
    public AbilityType getAbilityType() {
        return abilityType;
    }

    @Override
    public String getAbilityDescription(Optional<Game> game, Setup setup) {
        return NIGHT_ACTION_DESCRIPTION;
    }

    public static final String NIGHT_ACTION_DESCRIPTION = "Save someone from an attack.";

    @Override
    public void mainAbilityCheck(Action a) {
        AbilityValidationUtil.deadCheck(a);
    }

    @Override
    public List<SetupModifierName> getSetupModifierNames() {
        List<SetupModifierName> names = new LinkedList<>();
        names.add(SetupModifierName.HEAL_SUCCESS_FEEDBACK);
        names.add(SetupModifierName.HEAL_FEEDBACK);
        names.add(SetupModifierName.HEAL_BLOCKS_POISON);
        return names;
    }

    @Override
    public List<SetupModifierName> getSetupModifiersForRoleDetails(Optional<Game> game, Setup setup) {
        List<SetupModifierName> modifiers = this.getSetupModifierNames();

        Set<FactionRole> poisoners = setup.getPossibleFactionRolesWithAbility(game, Poisoner.abilityType);
        if(poisoners.isEmpty())
            modifiers.remove(SetupModifierName.HEAL_BLOCKS_POISON);
        return modifiers;
    }

    public static final String SUCCESFULL_HEAL = "Your target was attacked tonight!";
    public static final String TARGET_FEEDBACK = "You were healed by a doctor!";

    @Override
    public void doNightAction(Action a) {
        Player owner = a.owner, target = a.getTarget();
        if(target == null)
            return;

        happening(owner, " healed ", target);
        // but the person is healed either way
        target.heal(new Heal(owner, Constants.DOCTOR_HEAL_FLAG));
        a.markCompleted();
        owner.visit(target);
    }

    @Override
    protected ArrayList<String> getProfileHints(Player p) {
        ArrayList<String> ret = new ArrayList<>();

        if(p.game.getBool(SetupModifierName.HEAL_SUCCESS_FEEDBACK)){
            ret.add("You will know if your target was attacked.");
        }else{
            ret.add("You won't know if your target was attacked.");
        }

        if(p.game.getBool(SetupModifierName.HEAL_FEEDBACK)){
            ret.add("Your target will know if they were attacked and healed, but not who healed them.");
        }else{
            ret.add("Your taget will not know that they were attacked and healed.");
        }

        return ret;
    }

    @Override
    public boolean isSelfTargetModifiable() {
        return false;
    }

    @Override
    public boolean showSelfTargetTextDefault() {
        return true;
    }

    public static FactionRole template(Faction faction) {
        return FactionRoleService.createFactionRole(faction, template(faction.setup));
    }

    public static Role template(Setup setup) {
        return RoleService.createRole(setup, "Doctor", Doctor.abilityType);
    }
}
