package game.abilities.util;

import java.util.ArrayList;
import java.util.Comparator;

import game.abilities.GameAbility;
import game.logic.support.action.Action;
import models.enums.AbilityType;
import util.CollectionUtil;

public class WitchOrdering {

    public static final Comparator<GameAbility> byAbilityDamageComparator = new Comparator<GameAbility>() {
        @Override
        public int compare(GameAbility arg0, GameAbility arg1) {
            return order.indexOf(arg0.getAbilityType()) - order.indexOf(arg1.getAbilityType());
        }
    };

    public static final Comparator<Action> byActionDamageComparator = new Comparator<Action>() {
        @Override
        public int compare(Action arg0, Action arg1) {
            return order.indexOf(arg0.abilityType) - order.indexOf(arg1.abilityType);
        }
    };

    private static final AbilityType[] orderDefinition = {

            AbilityType.Burn,

            AbilityType.MassMurderer,

            AbilityType.FactionKill,

            AbilityType.SerialKiller,

            AbilityType.Vigilante,

            AbilityType.Interceptor,

            AbilityType.Disguiser,

            AbilityType.Poisoner,

            AbilityType.JailExecute,

            AbilityType.ElectroManiac,

            AbilityType.Joker,

            AbilityType.Clubber,

            AbilityType.ProxyKill,

            AbilityType.Survivor,

            AbilityType.CultLeader,

            AbilityType.MasonLeader,

            AbilityType.Enforcer,

            AbilityType.Douse,

            AbilityType.Gunsmith,

            AbilityType.Blacksmith,

            AbilityType.BreadAbility,

            AbilityType.Bodyguard,

            AbilityType.Doctor,

            AbilityType.Elector,

            AbilityType.Jester,

            AbilityType.Ventriloquist,

            AbilityType.Block,

            AbilityType.Disfranchise,

            AbilityType.Blackmailer,

            AbilityType.Tailor,

            AbilityType.DrugDealer,

            AbilityType.Framer,

            AbilityType.Silence,

            AbilityType.Janitor,

            AbilityType.Commuter,

            AbilityType.Investigator,

            AbilityType.Agent,

            AbilityType.Detective,

            AbilityType.Lookout,

            AbilityType.Sheriff,

            AbilityType.ArmsDetector,

            AbilityType.Scout,

            AbilityType.Coroner,

            AbilityType.Spy,

            AbilityType.Visit,

            AbilityType.Witch,

            AbilityType.Operator,

            AbilityType.Driver,

            AbilityType.Snitch,

            AbilityType.GraveDigger,

            AbilityType.Coward,

            AbilityType.Amnesiac,

            AbilityType.Undouse,

            AbilityType.Veteran };

    public static final ArrayList<AbilityType> order = CollectionUtil.toArrayList(orderDefinition);
}
