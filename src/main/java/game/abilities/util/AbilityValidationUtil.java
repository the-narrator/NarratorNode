package game.abilities.util;

import game.logic.PlayerList;
import game.logic.exceptions.PlayerTargetingException;
import game.logic.support.action.Action;

public class AbilityValidationUtil {

    public static void aliveCheck(Action a) {
        if(a.getTargets().hasLiving())
            Exception(a.owner.getRoleName() + " can't use this ablity on living targets. "
                    + a.getTargets().getLivePlayers().getStringName() + " is/are alive.");
    }

    public static void aliveCheck(PlayerList p) {
        if(p.hasLiving())
            Exception("This can't use this ablity on living targets. ");
    }

    public static void deadCheck(Action a) {
        if(a.getTargets().hasDead())
            Exception(a.owner.getRoleName() + " can't use this ablity on dead targets. "
                    + a.getTargets().getDeadPlayers().getStringName() + " is dead.");
    }

    public static void noAcceptableTargets() {
        Exception("You don't have any role abilities");
    }

    public static void noTeamMateTargeting(Action a) {
        if(a.owner.getGameFaction().knowsTeam()){
            if(!a.getTargets().intersect(a.owner.getGameFaction().getMembers()).isEmpty())
                Exception("You can't target known allies");
        }
    
    }

    public static void Exception(String s) throws PlayerTargetingException {
        throw new PlayerTargetingException(s);
    }

    public static void defaultTargetSizeCheck(int defaultTargetSize, Action action) {
        if(action.getTargets().size() != defaultTargetSize)
            Exception("You must select " + defaultTargetSize + " and only " + defaultTargetSize + " target");
    }

}
