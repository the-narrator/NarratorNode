package game.abilities.util;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Optional;

import game.abilities.AbilityList;
import game.abilities.GameAbility;
import game.logic.Game;
import game.logic.exceptions.NarratorException;
import game.logic.exceptions.UnknownRoleException;
import game.setups.Setup;
import models.Ability;
import models.Role;
import models.enums.AbilityModifierName;
import models.enums.AbilityType;
import models.modifiers.Modifiers;
import models.requests.AbilityCreateRequest;
import models.schemas.AbilitySchema;
import util.Util;
import util.game.LookupUtil;

public class AbilityUtil {

    public static String GetRoleName(ArrayList<String> commands, Setup setup) {
        ArrayList<String> toAddBack = new ArrayList<>();

        StringBuilder sb = new StringBuilder();
        String roleName = null, s;
        Optional<Role> role;
        while (!commands.isEmpty() && roleName == null){
            s = commands.remove(0);
            toAddBack.add(s);
            sb.append(s);

            role = LookupUtil.findRole(setup, sb.toString());
            if(role.isPresent()){
                roleName = role.get().getName();
            }else{
                sb.append(" ");
            }
        }

        if(roleName == null){
            for(int i = toAddBack.size() - 1; i >= 0; i--){
                commands.add(0, toAddBack.remove(i));
            }
        }

        return roleName;

    }

    public static boolean isEqualContent(AbilityList abilities, AbilityList abilities2) {
        if(abilities == null && abilities2 == null)
            return true;
        if(abilities == null || abilities2 == null)
            return false;

        if(abilities.size() != abilities2.size())
            return false;
        for(int i = 0; i < abilities.size(); i++){
            if(!abilities.get(i).getClass().equals(abilities2.get(i).getClass()))
                return false;
        }
        return true;
    }

    public static AbilityType getAbilityByCommand(String command) {
        Optional<AbilityType> abilityType = findAbilityByCommand(command);
        if(abilityType.isPresent())
            return abilityType.get();
        throw new NarratorException(command + " is not a valid ability command.");
    }

    public static Optional<AbilityType> findAbilityByCommand(String command) {
        if(command == null)
            return Optional.empty();
        for(AbilityType abilityType: AbilityType.values())
            if(command.equalsIgnoreCase(abilityType.command))
                return Optional.of(abilityType);
        return Optional.empty();
    }

    public static AbilityType getAbilityTypeByDbName(String dbName) {
        dbName = Util.TitleCase(dbName);
        for(AbilityType abilityType: AbilityType.values()){
            if(dbName.equals(abilityType.dbName))
                return abilityType;
        }
        throw new UnknownRoleException(dbName);
    }

    public static GameAbility CREATOR(AbilityType abilityType, Game game, Modifiers<AbilityModifierName> modifiers) {
        return CREATOR(abilityType, game, game.setup, modifiers);
    }

    public static GameAbility CREATOR(AbilityType abilityType, Setup setup, Modifiers<AbilityModifierName> modifiers) {
        return CREATOR(abilityType, null, setup, modifiers);
    }

    @SuppressWarnings("unchecked")
    private static GameAbility CREATOR(AbilityType abilityType, Game nullableGame, Setup setup,
            Modifiers<AbilityModifierName> modifiers) {
        try{
            Class<? extends GameAbility> abilityClass = (Class<? extends GameAbility>) Class
                    .forName("game.abilities." + abilityType.toString());
            Constructor<? extends GameAbility> constructor = abilityClass.getDeclaredConstructor(Game.class,
                    Setup.class, Modifiers.class);
            return constructor.newInstance(nullableGame, setup, modifiers);
        }catch(NullPointerException | NoSuchMethodException | SecurityException | InstantiationException
                | IllegalAccessException | IllegalArgumentException | InvocationTargetException
                | ClassNotFoundException e){
            throw new NarratorException("Unsupported game ability instantiation: " + abilityType.toString());
        }
    }

    public static ArrayList<AbilityCreateRequest> toCreateRequests(ArrayList<Ability> roles) {
        ArrayList<AbilityCreateRequest> requests = new ArrayList<>();
        for(Ability ability: roles)
            requests.add(new AbilityCreateRequest(ability));

        return requests;
    }

    public static LinkedHashSet<AbilitySchema> getSortedByID(Collection<AbilitySchema> unsorted) {
        List<AbilitySchema> sorted = new ArrayList<>(unsorted);
        sorted.sort(new Comparator<AbilitySchema>() {
            @Override
            public int compare(AbilitySchema schema1, AbilitySchema schema2) {
                return (int) (schema1.id - schema2.id);
            }

        });
        return new LinkedHashSet<>(sorted);
    }
}
