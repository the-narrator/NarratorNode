package game.abilities;

import java.util.Optional;

import game.abilities.util.AbilityValidationUtil;
import game.logic.Game;
import game.logic.Player;
import game.logic.support.action.Action;
import game.setups.Setup;
import models.Faction;
import models.FactionRole;
import models.Role;
import models.enums.AbilityModifierName;
import models.enums.AbilityType;
import models.modifiers.Modifiers;
import services.FactionRoleService;
import services.RoleService;

public class Blackmailer extends GameAbility {

    public static final AbilityType abilityType = AbilityType.Blackmailer;
    public static final String COMMAND = abilityType.command;

    public Blackmailer(Game game, Setup setup, Modifiers<AbilityModifierName> modifiers) {
        super(game, setup, modifiers);
    }

    public static final String NIGHT_ACTION_DESCRIPTION = "Stop people from voting and talking.";

    @Override
    public String getAbilityDescription(Optional<Game> game, Setup setup) {
        return NIGHT_ACTION_DESCRIPTION;
    }

    @Override
    public AbilityType getAbilityType() {
        return abilityType;
    }

    @Override
    public void mainAbilityCheck(Action a) {
        AbilityValidationUtil.deadCheck(a);
    }

    @Override
    public void doNightAction(Action a) {
        Player owner = a.owner, target = a.getTarget();
        if(target == null)
            return;
        Silence.setSilenced(owner, target);
        Disfranchise.setDisfranchised(owner, target);
        a.markCompleted();
        owner.visit(target);
    }

    @Override
    public boolean isNegativeAbility() {
        return true;
    }

    @Override
    public boolean showSelfTargetTextDefault() {
        return true;
    }

    public static Role template(Setup setup) {
        return RoleService.createRole(setup, "Blackmailer", Blackmailer.abilityType);
    }

    public static FactionRole template(Faction faction) {
        Role role = template(faction.setup);
        return FactionRoleService.createFactionRole(faction, role);
    }
}
