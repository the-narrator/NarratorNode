package game.abilities;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Optional;

import game.abilities.support.Bread;
import game.abilities.support.Charge;
import game.abilities.util.AbilityValidationUtil;
import game.ai.Controller;
import game.ai.ControllerList;
import game.logic.Game;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.support.Random;
import game.logic.support.StringChoice;
import game.logic.support.action.Action;
import game.setups.Setup;
import models.enums.AbilityModifierName;
import models.enums.AbilityType;
import models.enums.SetupModifierName;
import models.modifiers.Modifiers;
import util.game.ActionUtil;
import util.game.GameUtil;

public class BreadAbility extends GameAbility {

    public static final AbilityType abilityType = AbilityType.BreadAbility;
    public static final String COMMAND = abilityType.command;

    public BreadAbility(Game game, Setup setup, Modifiers<AbilityModifierName> modifiers) {
        super(game, setup, modifiers);
    }

    @Override
    public AbilityType getAbilityType() {
        return abilityType;
    }

    @Override
    public String getAbilityDescription(Optional<Game> game, Setup setup) {
        Optional<Integer> playerCount = GameUtil.getPlayerCount(game);
        return "use a " + Bread.GetBreadName(playerCount, setup) + " to survive the night";
    }

    @Override
    public String getUsage(Player p, Random r) {
        return COMMAND;
    }

    @Override
    public void doNightAction(Action a) {
        Bread.PassBread(a);
    }

    @Override
    public GameAbility useCharge() {
        for(Bread b: this.charges.iterable(Bread.class)){
            b.markUsed();
            return this;
        }
        return this;
    }

    @Override
    public void onNightStart(Player player) {
        super.onNightStart(player);
        Iterator<Bread> iterator = this.charges.iterable(Bread.class).iterator();
        while (iterator.hasNext())
            if(iterator.next().isUsed())
                iterator.remove();
    }

    @Override
    public boolean isAvailableAbility(Player owner) {
        return super.isAvailableAbility(owner) && canPassBread(owner);
    }

    private static boolean canPassBread(Player owner) {
        return (owner.game.getBool(SetupModifierName.BREAD_PASSING) || owner.is(Baker.abilityType));
    }

    @Override
    public void mainAbilityCheck(Action a) {
        Player owner = a.owner;
        if(a.getTargets().size() != 1)
            AbilityValidationUtil.Exception("You must select one and only one target");
        AbilityValidationUtil.deadCheck(a);
        if(!canPassBread(a.owner))
            AbilityValidationUtil.Exception("You can't pass bread!");
        if(!a.isTeamTargeting() || !a.owner.is(Baker.abilityType))
            return;
        ArrayList<Bread> breadCopy = new ArrayList<>();
        Bread b;
        for(Charge charge: this.charges){
            b = (Bread) charge;
            breadCopy.add(b);
        }
        for(Action curAction: owner.getActions()){
            if(curAction.abilityType != abilityType)
                continue;
            if(!curAction.isTeamTargeting())
                return;

            // is team targeting, so remove this bread from the option of being passed
            for(int i = 0; i < breadCopy.size(); i++){
                if(breadCopy.get(i).isInitialized()){
                    breadCopy.remove(i);
                    break;
                }
            }
        }
        for(Charge charge: breadCopy){
            b = (Bread) charge;
            if(b.isInitialized())
                return;
        }

        AbilityValidationUtil.Exception("Cannot target known teammates with this");
    }

    @Override
    public Optional<Action> getExampleAction(Player owner) {
        return Optional.of(new Action(owner, abilityType));
    }

    @Override
    protected ArrayList<String> getProfileHints(Player p) {
        int breadCount = this.charges.getPerceivedCharges();
        if(breadCount == 0)
            return super.getProfileHints(p);

        ArrayList<String> specs = new ArrayList<>();
        if(breadCount == 1)
            specs.add("You have a piece of " + Bread.GetBreadName(p.game)
                    + ", allowing you to submit an extra night action");
        else if(breadCount > 0)
            specs.add("You have " + breadCount + " pieces of " + Bread.GetBreadName(p.game)
                    + ", allowing you to submit multiple night actions.");

        return specs;
    }

    @Override
    public boolean canAddAnotherAction(Action a) {
        int breadToGive = a.owner.getActions().getActions(BreadAbility.abilityType).size();
        int availableBread = this.charges.getPerceivedCharges();
        if(breadToGive >= availableBread && availableBread >= 0)
            return false;
        return super.canAddAnotherAction(a);
    }

    public static int getPassableBread(Player breadUser) {
        if(!breadUser.hasAbility(BreadAbility.abilityType))
            return 0;

        if(!breadUser.is(Baker.abilityType) && !breadUser.game.getBool(SetupModifierName.BREAD_PASSING))
            return 0;

        BreadAbility ba = breadUser.getAbility(BreadAbility.class);
        return ba.charges.getRealCharges();
    }

    public static int getUseableBread(Player breadUser) {
        return getUseableBread(breadUser, null);
    }

    public static int getUseableBread(Player breadUser, Boolean real) {
        if(!breadUser.hasAbility(BreadAbility.abilityType))
            return 0;
        if(breadUser.isDead())
            return 0;

        BreadAbility ba = breadUser.getAbility(BreadAbility.class);
        if(breadUser.game.getBool(SetupModifierName.SELF_BREAD_USAGE))
            return ba.charges.getPerceivedCharges();
        int bCount = 0;
        for(Bread b: ba.charges.iterable(Bread.class)){
            if(real != null && real && b.isFake())
                continue;
            if(b.isInitialized())
                bCount++;
        }
        return bCount;
    }

    @Override
    public ArrayList<Object> getActionDescription(ArrayList<Action> actions) {
        Player owner = actions.get(0).owner;
        PlayerList targets = ActionUtil.getActionTargets(actions);

        ArrayList<Object> list = new ArrayList<>();
        list.add("pass " + Bread.GetBreadName(owner.game) + " to ");
        list.addAll(StringChoice.YouYourself(targets));

        return list;
    }

    public void setMarkedBreadToFake() {
        Bread b;
        for(Charge charge: this.charges){
            b = (Bread) charge;
            if(b.isUsed()){
                b.markUnused();
                b.setReal(false);
            }
        }
    }

    @Override
    public boolean isPositiveAbility() {
        return false;
    }

    @Override
    public boolean isAllowedFactionAbility() {
        return false;
    }

    @Override
    public int maxNumberOfSubmissions() {
        return this.charges.getPerceivedCharges();
    }

    public static Action getAction(Controller breadGiver, Controller receiver, Game game) {
        return new Action(breadGiver.getPlayer(), abilityType, new LinkedList<>(),
                ControllerList.ToPlayerList(game, receiver));
    }

    @Override
    public void onDayStart(Player p) {
        super.onDayStart(p);
        Iterator<Bread> iterator = this.charges.iterable(Bread.class).iterator();
        while (iterator.hasNext()){
            if(iterator.next().isUsed())
                iterator.remove();
        }
    }
}
