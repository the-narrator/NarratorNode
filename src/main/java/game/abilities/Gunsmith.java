package game.abilities;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import game.abilities.support.Gun;
import game.abilities.util.AbilityValidationUtil;
import game.ai.Controller;
import game.ai.ControllerList;
import game.event.Feedback;
import game.logic.Game;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.exceptions.IllegalActionException;
import game.logic.support.Option;
import game.logic.support.Random;
import game.logic.support.StringChoice;
import game.logic.support.action.Action;
import game.setups.Setup;
import models.Ability;
import models.Faction;
import models.FactionRole;
import models.Role;
import models.enums.AbilityModifierName;
import models.enums.AbilityType;
import models.enums.SetupModifierName;
import models.modifiers.Modifiers;
import services.FactionRoleService;
import services.RoleService;
import util.Util;
import util.game.ActionUtil;
import util.game.GameUtil;

public class Gunsmith extends GameAbility {

    public static AbilityType abilityType = AbilityType.Gunsmith;
    public static final String COMMAND = abilityType.command;

    public Gunsmith(Game game, Setup setup, Modifiers<AbilityModifierName> modifiers) {
        super(game, setup, modifiers);
    }

    public static final String FAULTY = "faulty";
    public static final String REAL = "real";

    public static final String[] GS_FAULTY_GUNS = new String[] { "gs_faulty_guns",
            "#" + Gun.ALIAS + "#s can be optionally faulty", };

    @Override
    public AbilityType getAbilityType() {
        return abilityType;
    }

    @Override
    public String getAbilityDescription(Optional<Game> game, Setup setup) {
        Optional<Integer> playerCount = GameUtil.getPlayerCount(game);
        return "Hand out " + Gun.getGunName(playerCount, setup) + " so others can shoot them.";
    }

    public static final String GetDeathFeedback(Game n) {
        return "You were killed by a " + Gun.getGunName(n) + " wielder!";
    }

    public static final String GetFaultyDeathFeedback(Game n) {
        return "You accidentally killed yourself because your " + Gun.getGunName(n) + " was sabotged!";
    }

    @Override
    public void mainAbilityCheck(Action a) {
        AbilityValidationUtil.deadCheck(a);
        AbilityValidationUtil.noTeamMateTargeting(a);
        String gunName = Gun.getGunName(a.owner.game);
        if(!FaultyGuns(a.owner) && a.getArg1() != null && !a.getArg1().equalsIgnoreCase(REAL))
            AbilityValidationUtil
                    .Exception(Util.TitleCase(gunName) + " can only be real. Please omit any action options.");
        if(FaultyGuns(a.owner) && a.getArg1() != null
                && (!a.getArg1().equalsIgnoreCase(REAL) && !a.getArg1().equalsIgnoreCase(FAULTY)))
            AbilityValidationUtil.Exception("Unable to tell whether " + gunName + " should real or faulty");

        if(FaultyGuns(a.owner) && a.getArg1() == null)
            a.args = Util.toStringList(REAL);
    }

    @Override
    public ArrayList<Option> getOptions(Player p) {
        return getOptions(FaultyGuns(p), p.game);
    }

    protected static ArrayList<Option> getOptions(boolean isFakeGunPossible, Game game) {
        ArrayList<Option> options = new ArrayList<>();
        if(!isFakeGunPossible)
            return options;
        String g_opt = Gun.getGunName(game);
        options.add(new Option(REAL, REAL + " " + g_opt));
        options.add(new Option(FAULTY, FAULTY + " " + g_opt));
        return options;
    }

    @Override
    public ArrayList<Object> getActionDescription(ArrayList<Action> actions) {
        ArrayList<Object> list = new ArrayList<>();

        PlayerList targets = ActionUtil.getActionTargets(actions);

        if(actions.get(0).getArg1() != null && actions.get(0).getArg1().equalsIgnoreCase(FAULTY)){
            if(targets.size() == 1)
                list.add("give a faulty gun to ");
            else
                list.add("give faulty guns to ");
        }else{
            if(targets.size() == 1)
                list.add("give a gun to ");
            else
                list.add("give guns to ");
        }
        list.addAll(StringChoice.YouYourself(targets));

        return list;
    }

    @Override
    public void doNightAction(Action a) {
        Player owner = a.owner, target = a.getTarget();
        if(target != null)
            a.markCompleted();
        GunsmithAction(owner, target, a.getArg1());
    }

    public static String GetGunReceiveMessage(Game narrator) {
        String ret = "You received a ";
        if(narrator.getBool(SetupModifierName.GS_DAY_GUNS))
            ret += "day-use ";
        else
            ret += "night-use ";
        ret += Gun.getGunName(narrator) + "!";

        return ret;
    }

    public static void GunsmithAction(Player gs, Player target, String gOption) {
        if(target == null)
            return;
        if(gs.isAlive() && gs.getGameFaction().knowsTeam() && gs.getGameFaction() == target.getGameFaction()){
            Visit.NoNightActionVisit(gs, target);
            return;
        }

        Game n = gs.game;
        String gunName = Gun.getGunName(n);

        if(!target.isSquelched()){
            Gun g = new Gun(gs);
            boolean faulty = FaultyGuns(gs) && FAULTY.equalsIgnoreCase(gOption);
            if(faulty)
                g.setFaulty();
            target.addConsumableCharge(Vigilante.abilityType, g);
            FeedbackGenerator(new Feedback(target));
            if(faulty)
                happening(gs, " gave a sabotoged " + gunName + " to ", target);
            else
                happening(gs, " gave a " + gunName + " to ", target);
        }else{
            happening(gs, " tried to give a " + gunName + " to ", target);
        }
        gs.visit(target); // visited in the actual night action call, not the static one
    }

    private static void FeedbackGenerator(Feedback feedback) {
        Game game = feedback.n;
        feedback.add(GetGunReceiveMessage(feedback.player.game)).setPicture("mafioso");
        for(FactionRole factionRole: game.setup.getPossibleFactionRolesWithAbility(game, Blacksmith.abilityType,
                Gunsmith.abilityType)){
            if(hasFaultyGuns(factionRole, game.players.size())){
                feedback.addExtraInfo("There is a chance that your gun might kill you on use.");
                break;
            }
        }

        Set<FactionRole> factionRoles = game.setup.getPossibleFactionRolesWithAbility(game, DrugDealer.abilityType);
        if(!factionRoles.isEmpty())
            feedback.addExtraInfo("There is a chance that your gun won't do anything");
    }

    private static boolean hasFaultyGuns(FactionRole factionRole, int playerCount) {
        AbilityModifierName name = AbilityModifierName.GS_FAULTY_GUNS;
        Modifiers<AbilityModifierName> modifiers;
        for(Ability a: factionRole.role.getAbilities()){
            modifiers = factionRole.getAbilityModifiers(a.type, playerCount);
            if(modifiers.getBoolean(name, playerCount))
                return true;
        }
        return false;
    }

    @Override
    protected ArrayList<String> getProfileHints(Player p) {
        ArrayList<String> ret = new ArrayList<>();

        if(p.getGameFaction().knowsTeam() && p.getGameFaction().getMembers().size() > 1){
            ret.add("Your " + Gun.getGunName(p.game) + "s cannot go to teammates.");
        }
        return ret;
    }

    @Override
    public List<SetupModifierName> getSetupModifierNames() {
        List<SetupModifierName> names = new LinkedList<>();
        names.add(SetupModifierName.GS_DAY_GUNS);
        return names;
    }

    @Override
    public List<AbilityModifierName> getAbilityModifiers() {
        List<AbilityModifierName> modifierNames = new LinkedList<>();
        modifierNames.add(AbilityModifierName.GS_FAULTY_GUNS);
        modifierNames.addAll(super.getAbilityModifiers());
        return modifierNames;
    }

    @Override
    public Action parseCommand(Player p, double timeLeft, ArrayList<String> commands) {
        if(!FaultyGuns(p))
            return super.parseCommand(p, timeLeft, commands);

        if(!commands.get(commands.size() - 1).equalsIgnoreCase(FAULTY))
            return super.parseCommand(p, timeLeft, commands);

        commands.remove(commands.size() - 1);

        Action action = super.parseCommand(p, timeLeft, commands);
        action.args = Util.toStringList(FAULTY);
        return action;
    }

    @Override
    public String getUsage(Player p, Random r) {
        if(FaultyGuns(p))
            return super.getUsage(p, r) + " " + REAL + "\n" + super.getUsage(p, r) + " " + FAULTY;
        return super.getUsage(p, r);
    }

    @Override
    public ArrayList<String> getCommandParts(Action action) {
        ArrayList<String> parts = super.getCommandParts(action);
        if(faultyGuns() && FAULTY.equalsIgnoreCase(action.getArg1())){
            parts.add(FAULTY);
        }
        return parts;
    }

    private boolean faultyGuns() {
        return FaultyGuns(this);
    }

    public static boolean FaultyGuns(Player p) {
        if(p.hasAbility(Gunsmith.abilityType))
            return FaultyGuns(p.getAbility(Gunsmith.abilityType));
        if(p.hasAbility(Blacksmith.abilityType))
            return FaultyGuns(p.getAbility(Blacksmith.abilityType));
        else if(p.hasAbility(GraveDigger.abilityType)){
            for(Player q: p.game.players){
                if(q.hasAbility(Gunsmith.abilityType) || q.hasAbility(Blacksmith.abilityType)){
                    if(FaultyGuns(q))
                        return true;
                }
            }
            return false;
        }
        throw new IllegalActionException("Cannot test for faulty " + Gun.getGunName(p.game) + " on this player");
    }

    public static boolean FaultyGuns(GameAbility r) {
        // only called in the started game context
        return r.modifiers.getBoolean(AbilityModifierName.GS_FAULTY_GUNS, r.narrator);
    }

    public static FactionRole template(Faction faction) {
        Role role = RoleService.createRole(faction.setup, "Gunsmith", Gunsmith.abilityType);
        return FactionRoleService.createFactionRole(faction, role);
    }

    public static Action getFaultyAction(Controller gunsmith, Controller target, Game game) {
        List<String> args = Util.toStringList(FAULTY);
        return new Action(gunsmith.getPlayer(), abilityType, args, ControllerList.ToPlayerList(game, target));
    }
}
