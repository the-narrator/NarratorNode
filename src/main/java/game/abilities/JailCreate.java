package game.abilities;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import game.abilities.util.AbilityValidationUtil;
import game.ai.Controller;
import game.event.Feedback;
import game.event.Message;
import game.event.SelectionMessage;
import game.logic.Game;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.listeners.NarratorListener;
import game.logic.support.Constants;
import game.logic.support.StringChoice;
import game.logic.support.action.Action;
import game.setups.Setup;
import models.Faction;
import models.FactionRole;
import models.Role;
import models.enums.AbilityModifierName;
import models.enums.AbilityType;
import models.modifiers.Modifiers;
import services.FactionRoleService;
import services.RoleService;
import util.game.ActionUtil;

public class JailCreate extends GameAbility {

    public static final String COMMAND = AbilityType.JailCreate.command;
    public static final AbilityType abilityType = AbilityType.JailCreate;

    public JailCreate(Game game, Setup setup, Modifiers<AbilityModifierName> modifiers) {
        super(game, setup, modifiers);
    }

    public static final String DRAGGED_MESSAGE = "You were dragged off to jail!";

    @Override
    public boolean isAllowedFactionAbility() {
        return false;
    }

    @Override
    public String getAbilityDescription(Optional<Game> game, Setup setup) {
        return NIGHT_ACTION_DESCRIPTION;
    }

    @Override
    public GameAbility initialize(Player p) {
        jailedTargets = new PlayerList();
        return super.initialize(p);
    }

    public static final String NIGHT_ACTION_DESCRIPTION = "Capture people to talk to them privately in a jail cell at night.";
    public static final String NO_EXECUTE = "Jailed Targets";

    @Override
    public AbilityType getAbilityType() {
        return abilityType;
    }

    @Override
    public List<AbilityModifierName> getAbilityModifiers() {
        List<AbilityModifierName> modifierNames = new LinkedList<>();
        modifierNames.add(AbilityModifierName.JAILOR_CLEAN_ROLES);
        modifierNames.addAll(super.getAbilityModifiers());
        return modifierNames;
    }

    @Override
    public void targetSizeCheck(Action a) {
        if(a.owner.game.isNight()){
            if(a.getTargets().size() > jailedTargets.size())
                AbilityValidationUtil.Exception("Cannot execute someone that isn't jailed");
            if(getPerceivedCharges() != Constants.UNLIMITED)
                if(a.getTargets().size() > getPerceivedCharges())
                    AbilityValidationUtil.Exception("Not enough executions for that many targets");
        }else{
            if(a.getTargets().size() != 1)
                AbilityValidationUtil.Exception("Cannot jail more than one person at a time");
        }

    }

    @Override
    public void chargeCheck() {
        if(getPerceivedCharges() == 0 && narrator.isNight())
            AbilityValidationUtil.Exception("You cannot execute anymore.");
    }

    @Override
    public void mainAbilityCheck(Action a) {
        // wrote this weird. what i want to do is check that there's no prev day lynch
        // then make sure that i'm trying to execute someone i have jailed.
        // however i only do the first check if i can validate that i have someone in
        // jail, and
        /// its the right person

        AbilityValidationUtil.deadCheck(a);
    }

    @Override
    public boolean canUseDuringDay(Player p) {
        return !p.isPuppeted();
    }

    @Override
    public void doNightAction(Action a) {
        return;
    }

    @Override
    public ArrayList<Object> getInsteadPhrase(Action a) {
        if(a.owner.game.isDay())
            return super.getInsteadPhrase(a);
        ArrayList<Object> list = new ArrayList<>();

        list.add("excuting ");
        list.add(StringChoice.YouYourself(a.getTargets()));

        return list;
    }

    @Override
    public String getDayCommand() {
        return COMMAND;
    }

    private PlayerList jailedTargets;

    public PlayerList getJailedTargets() {
        return jailedTargets.copy();
    }

    public static final String CAPTOR = "Captor";

    public static final String JAIL = COMMAND;
    public static final String DEATH_FEEDBACK = "You were executed by a Jailor!";

    @Override
    public void doDayAction(Action action, Action parentAction) {
        Player jailor = action.owner;
        Player target = action.getTarget();
        if(jailor.getActions().isTargeting(target, abilityType))
            return;
        SelectionMessage e = new SelectionMessage(jailor, false, true);
        Action[] newOld = Action.pushCommand(e, action, jailor.getActions());
        e.dontShowPrivate();
        jailor.sendMessage(e);

        jailor.game.addActionStack(newOld[0]);// new is 0
        jailor.game.removeActionStack(newOld[1]);// old is 1

        List<NarratorListener> listeners = jailor.game.getListeners();
        for(NarratorListener listener: listeners)
            listener.onDayActionSubmit(jailor, newOld[0]);
    }

    public void onSubmit(Action action) {
        // make selection message, add it to the jail chat, have it so only the receiver
        // can view this message

    }

    @Override
    public ArrayList<Object> getActionDescription(ArrayList<Action> actions) {
        ArrayList<Object> list = new ArrayList<>();

        if(actions.isEmpty())
            return list;
        PlayerList targets = ActionUtil.getActionTargets(actions);

        list.add(JAIL.toLowerCase());
        list.add(" ");
        list.addAll(StringChoice.YouYourself(targets));

        return list;
    }

    @Override
    public boolean isDayAbility() {
        return true;
    }

    @Override
    public void resolveDayAction(Action a) {
        happening(a.owner, " jailed ", a.getTarget());
        Message m = new Feedback(a.getTarget(), DRAGGED_MESSAGE).setPicture("jailor");
        if(!a.owner.getActionableAbilities().isEmpty())
            m.addExtraInfo("You will not be able to perform any of your night actions.");
        m.addExtraInfo("Check your chats!");
        a.getTarget().sendMessage(m);
        a.owner.game.getEventManager().createJailChat(a);
        if(jailedTargets.size() > 0)
            a.owner.getAbility(BreadAbility.class).useCharge();
        jailedTargets.add(a.getTarget());
    }

    @Override
    public void onDayStart(Player player) {
        jailedTargets.clear();
    }

    @Override
    public void dayHappening(Action action) {
        happening(action.owner, " attempted to jail ", action.getTarget());
    }

    @Override
    public boolean hasCooldownAbility() {
        return false;
    }

    @Override
    public boolean isNightAbility(Player player) {
        return false;
    }

    public static String KeyCreator(Controller jailed, int dayNumber) {
        return jailed.getName() + Constants.JAIL_CHAT_KEY_SEPERATOR + dayNumber;
    }

    @Override
    public boolean isSelfTargetModifiable() {
        return false;
    }

    @Override
    public boolean isChargeModifiable() {
        return false;
    }

    public static FactionRole template(Faction faction) {
        return FactionRoleService.createFactionRole(faction, template(faction.setup));
    }

    public static Role template(Setup setup) {
        return RoleService.createRole(setup, "Jailor", JailExecute.abilityType);
    }
}
