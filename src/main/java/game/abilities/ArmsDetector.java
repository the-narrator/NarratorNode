package game.abilities;

import java.util.ArrayList;
import java.util.Optional;
import java.util.Set;

import game.abilities.util.AbilityValidationUtil;
import game.event.Feedback;
import game.event.Happening;
import game.logic.Game;
import game.logic.Player;
import game.logic.support.action.Action;
import game.setups.Setup;
import models.Ability;
import models.Faction;
import models.FactionRole;
import models.Role;
import models.enums.AbilityModifierName;
import models.enums.AbilityType;
import models.modifiers.Modifiers;
import services.FactionRoleService;
import services.RoleService;
import util.game.LookupUtil;

public class ArmsDetector extends GameAbility {

    public static final AbilityType abilityType = AbilityType.ArmsDetector;
    public static final String COMMAND = abilityType.command;

    public ArmsDetector(Game game, Setup setup, Modifiers<AbilityModifierName> modifiers) {
        super(game, setup, modifiers);
    }

    @Override
    public AbilityType getAbilityType() {
        return abilityType;
    }

    @Override
    public String getAbilityDescription(Optional<Game> game, Setup setup) {
        return NIGHT_ACTION_DESCRIPTION;
    }

    public static final String NIGHT_ACTION_DESCRIPTION = "Investigate someone to determine if they have access to weapons.";

    @Override
    public void mainAbilityCheck(Action a) {
        AbilityValidationUtil.deadCheck(a);
    }

    public static final String NO_GUNS = "Your target doesn't appear to have weapons.";
    public static final String HAS_GUNS = "Your target appears to have weapons.";

    @Override
    public void doNightAction(Action a) {
        Player owner = a.owner, target = a.getTarget();
        if(target == null)
            return;

        Feedback(owner, target);

        new Happening(owner.game).add(owner, " checked ", target, " for weapons");
        a.markCompleted();
        owner.visit(target);
    }

    public static Feedback Feedback(Player scout, Player target) {
        String feedback;
        if(!target.isDetectable())
            feedback = NO_GUNS;
        else if(target.getFrameStatus() != null)
            feedback = HAS_GUNS;
        else if(target.is(Doctor.abilityType) || target.is(SerialKiller.abilityType))
            feedback = NO_GUNS;
        else if(target.getFactions().hasAbility(FactionKill.abilityType))
            feedback = HAS_GUNS;
        else if(target.is(Sheriff.abilityType) || target.is(Veteran.abilityType))
            feedback = HAS_GUNS;
        else if(Vigilante.HasNightGuns(target))
            feedback = HAS_GUNS;
        else
            feedback = NO_GUNS;

        Feedback f = new Feedback(scout);
        f.add(feedback);
        if(feedback.equals(NO_GUNS))
            f.setPicture("citizen");
        else
            f.setPicture("mafioso");

        return f;
    }

    public static Feedback Feedback(Player armsDetector, Player target, Optional<String> nullIfJanitor,
            boolean isDead) {
        String feedbackText = "Your target is a";
        String role = null;
        if(target.getFrameStatus() != null && !isDead){
            role = target.getFrameStatus().getName();
        }else if(target.isDetectable() || isDead){
            role = target.getRoleName();
        }else{
            Optional<FactionRole> factionRole = Snitch.getCitizenTeam(armsDetector.game);
            if(factionRole.isPresent())
                role = factionRole.get().role.getName();
            else
                role = target.getRoleName();
        }

        feedbackText += role + ".";

        Feedback feedback = new Feedback(armsDetector);
        feedback.add(feedbackText);
        feedback.setPicture("lookout");
        if(nullIfJanitor.isPresent())
            feedback.addExtraInfo("As a reminder, you attempted to search " + nullIfJanitor.get() + " for arms.");

        return feedback;
    }

    @Override
    public boolean isFakeBlockable() {
        return false;
    }

    @Override
    public ArrayList<String> getPublicDescription(Optional<Game> playerList, String actionOriginatorName,
            Optional<String> nullableColor, Set<Ability> abilities) {
        ArrayList<String> list = super.getPublicDescription(playerList, actionOriginatorName, nullableColor, abilities);

        Set<Role> sheriffs = LookupUtil.findRolesWithAbility(setup, Sheriff.abilityType);
        if(!sheriffs.isEmpty()){
            StringBuilder sb = new StringBuilder();
            int i = 0;
            for(Role s: sheriffs){
                if(i != 0)
                    sb.append(", ");
                list.add(s.getName());
                i++;
            }
            list.add(" will be detected as having guns");
        }

        return list;
    }

    public static FactionRole template(Faction faction) {
        Role role = RoleService.createRole(faction.setup, "Arms Detector", ArmsDetector.abilityType);
        return FactionRoleService.createFactionRole(faction, role);
    }
}
