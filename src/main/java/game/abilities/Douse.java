package game.abilities;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import game.abilities.util.AbilityValidationUtil;
import game.logic.Game;
import game.logic.GameFaction;
import game.logic.Player;
import game.logic.support.StringChoice;
import game.logic.support.action.Action;
import game.setups.Setup;
import models.enums.AbilityModifierName;
import models.enums.AbilityType;
import models.enums.SetupModifierName;
import models.modifiers.Modifiers;

public class Douse extends GameAbility {

    public static final AbilityType abilityType = AbilityType.Douse;
    public static final String COMMAND = abilityType.command;

    public Douse(Game game, Setup setup, Modifiers<AbilityModifierName> modifiers) {
        super(game, setup, modifiers);
    }

    @Override
    public AbilityType getAbilityType() {
        return abilityType;
    }

    @Override
    public String getAbilityDescription(Optional<Game> game, Setup setup) {
        return NIGHT_ACTION_DESCRIPTION;
    }

    @Override
    public List<SetupModifierName> getSetupModifierNames() {
        List<SetupModifierName> names = new LinkedList<>();
        names.add(SetupModifierName.DOUSE_FEEDBACK);
        names.add(SetupModifierName.GUARD_REDIRECTS_DOUSE);
        return names;
    }

    public String getNightText(ArrayList<GameFaction> t) {
        return "Type " + NQuote(COMMAND) + " to ignite everyone doused.";
    }

    public static final String NIGHT_ACTION_DESCRIPTION = "Douse someone in flammable gasoline.";

    @Override
    public void mainAbilityCheck(Action a) {
        AbilityValidationUtil.deadCheck(a);
    }

    @Override
    public void doNightAction(Action a) {
        Player owner = a.owner, target = a.getTarget();

        if(target == null)
            return;

        if(target.game.getBool(SetupModifierName.GUARD_REDIRECTS_DOUSE))
            target = Bodyguard.redirect(target);

        target.setDoused(true);
        happening(owner, " doused ", target);
        a.markCompleted();
        owner.visit(target);
    }

    public static final String DOUSED_FEEDBACK = "You were doused with gasoline last night!";

    @Override
    public ArrayList<Object> getInsteadPhrase(Action a) {
        ArrayList<Object> list = new ArrayList<Object>();

        list.add("undousing ");
        list.addAll(StringChoice.YouYourself(a.getTargets()));
        return list;
    }

    @Override
    protected ArrayList<String> getProfileHints(Player p) {
        ArrayList<String> hasBurn = new ArrayList<>();

        if(p.game.getBool(SetupModifierName.DOUSE_FEEDBACK)){
            hasBurn.add("Targeting people will give them 'douse' feedback.");
        }else{
            hasBurn.add("Targets will be unaware that they are doused them.");
        }

        if(p.game.getBool(SetupModifierName.GUARD_REDIRECTS_DOUSE)){
            hasBurn.add("Players guarding your doused target will absorb the douse instead of your target.");
        }

        return hasBurn;
    }

    @Override
    public boolean isDatabaseAbility() {
        return false;
    }

    @Override
    public boolean isNegativeAbility() {
        return true;
    }
}
