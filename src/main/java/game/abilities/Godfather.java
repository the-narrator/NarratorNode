package game.abilities;

import java.util.Optional;

import game.logic.Game;
import game.logic.Player;
import game.logic.PlayerList;
import game.setups.Setup;
import models.enums.AbilityModifierName;
import models.enums.AbilityType;
import models.modifiers.Modifiers;

public class Godfather extends Passive {

    public static final AbilityType abilityType = AbilityType.Godfather;

    public Godfather(Game game, Setup setup, Modifiers<AbilityModifierName> modifiers) {
        super(game, setup, modifiers);
    }

    public static final String NIGHT_ACTION_DESCRIPTION = "The leader of the mafia!  Can override who is sent to kill";

    @Override
    public AbilityType getAbilityType() {
        return abilityType;
    }

    public static final String DEATH_FEEDBACK = "You were killed by the mafia!";

    // members is copied, as it's only
    @Override
    public int nightVotePower(Player pi, PlayerList members) {
        return Math.max(1, members.size() - 1);
    }

    @Override
    public boolean isFactionUniqueAbility() {
        return true;
    }

    @Override
    public String getAbilityDescription(Optional<Game> game, Setup setup) {
        return NIGHT_ACTION_DESCRIPTION;
    }
}
