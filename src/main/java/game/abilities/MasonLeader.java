package game.abilities;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import game.abilities.util.AbilityValidationUtil;
import game.event.Feedback;
import game.logic.Game;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.support.Constants;
import game.logic.support.action.Action;
import game.setups.Setup;
import models.Ability;
import models.FactionRole;
import models.PendingRole;
import models.enums.AbilityModifierName;
import models.enums.AbilityType;
import models.enums.SetupModifierName;
import models.modifiers.Modifiers;
import util.game.GameUtil;

public class MasonLeader extends GameAbility {

    public static final AbilityType abilityType = AbilityType.MasonLeader;
    public static final String COMMAND = abilityType.command;

    public MasonLeader(Game game, Setup setup, Modifiers<AbilityModifierName> modifiers) {
        super(game, setup, modifiers);
    }

    public static final String ROLE_NAME = "Mason Leader";
    public static final String NIGHT_ACTION_DESCRIPTION = "Recruit citizens into your masons and talk to them at night.";

    public static final String[] BLUDGEON = Constants.MASONLEADER_KILL_FLAG;
    public static final String DEAD_FEEDBACK = "You were bludgeoned to death for your heretical practices.";

    @Override
    public AbilityType getAbilityType() {
        return abilityType;
    }

    @Override
    public boolean isAllowedFactionAbility() {
        return false;
    }

    @Override
    public String getAbilityDescription(Optional<Game> game, Setup setup) {
        return NIGHT_ACTION_DESCRIPTION;
    }

    @Override
    public List<SetupModifierName> getSetupModifierNames() {
        List<SetupModifierName> names = new LinkedList<>();
        names.add(SetupModifierName.MASON_PROMOTION);
        names.add(SetupModifierName.MASON_NON_CIT_RECRUIT);
        return names;
    }

    @Override
    public void mainAbilityCheck(Action a) {
        MasonryCheck(a);
    }

    public static void MasonryCheck(Action a) {
        AbilityValidationUtil.deadCheck(a);
        Player target = a.getTarget();
        if(target.getFactions().contains(a.owner.getGameFaction()))
            if(Mason.IsMasonType(target))
                AbilityValidationUtil.Exception("Can't target masons");
    }

    private static void UnsuccessfulFeedback(Action a) {
        Feedback f = new Feedback(a.owner, "Your recruitment was unsuccessful");
        f.setPicture("cultist");
        f.hideableFeedback = false;
        Visit.NoNightActionVisit(a);
    }

    @Override
    public void doNightAction(Action a) {
        Player target = a.getTarget();
        Player ml = a.owner;
        if(target == null)
            return;

        if(target.isCulted()){
            a.markCompleted();
            Kill(ml, target, BLUDGEON);
            return;
        }

        if(!target.is(Infiltrator.abilityType)){
            if(target.isPowerRole() || target.isJailed() || target.isEnforced() || Mason.IsMasonType(target)
                    || !target.getColor().equalsIgnoreCase(ml.getColor())){
                UnsuccessfulFeedback(a);
                return;
            }

            if(!target.isPowerRole() && !Citizen.hasNoAbilities(target)
                    && !ml.game.getBool(SetupModifierName.MASON_NON_CIT_RECRUIT)){
                UnsuccessfulFeedback(a);
                return;
            }

            FactionRole factionRole = ml.getGameFaction().faction.getFactionRolesWithAbility(Mason.abilityType)
                    .iterator().next();
            target.changeRole(new PendingRole(factionRole));
        }else{
            target.addAbility(Mason.abilityType);
            ml.getGameFaction().addMember(target);
        }

        Feedback recruitResult = new Feedback(ml);
        recruitResult.hideableFeedback = false;
        recruitResult.add("Your recruitment was succesful.");
        recruitResult.setPicture("cultleader");

        PlayerList masons = new PlayerList();
        for(Player p: ml.game.getLivePlayers()){
            if(!p.getFactions().contains(ml.getGameFaction()))
                continue;

            if(Mason.IsMasonType(p)){
                masons.add(p);
            }else if(p.is(Infiltrator.abilityType)){
                masons.add(p);
            }

        }

        Feedback e = new Feedback(target);
        e.hideableFeedback = false;
        e.add(ml, " converted you to the masonry. Your other masons are : ", masons, ".");
        e.setPicture("cultist");

        happening(ml, " recruited ", target);

        a.markCompleted();
        ml.visit(target);
    }

    @Override
    protected ArrayList<String> getProfileHints(Player p) {
        return Mason.getProfileHints(p.game);
    }

    @Override
    public ArrayList<String> getPublicDescription(Optional<Game> game, String actionOriginatorName,
            Optional<String> color, Set<Ability> abilities) {
        ArrayList<String> ruleTextList = super.getPublicDescription(game, actionOriginatorName, color, abilities);
        Mason.addMasonText(GameUtil.getPlayerCount(game), setup, ruleTextList);
        return ruleTextList;
    }
}
