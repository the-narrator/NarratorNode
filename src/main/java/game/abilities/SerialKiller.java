package game.abilities;

import java.util.Optional;

import game.abilities.util.AbilityValidationUtil;
import game.logic.Game;
import game.logic.Player;
import game.logic.support.Constants;
import game.logic.support.action.Action;
import game.setups.Setup;
import models.Faction;
import models.FactionRole;
import models.Role;
import models.enums.AbilityModifierName;
import models.enums.AbilityType;
import models.modifiers.Modifiers;
import services.FactionRoleService;
import services.RoleService;
import util.Util;

public class SerialKiller extends GameAbility {

    public static final AbilityType abilityType = AbilityType.SerialKiller;
    public static final String COMMAND = abilityType.command;

    public SerialKiller(Game game, Setup setup, Modifiers<AbilityModifierName> modifiers) {
        super(game, setup, modifiers);
    }

    public static final String ROLE_NAME = "Serial Killer";

    public static final String NIGHT_ACTION_DESCRIPTION = "A crazed psychopath trying to kill in the town.";

    @Override
    public AbilityType getAbilityType() {
        return abilityType;
    }

    @Override
    public String[] getRoleBlurb(Optional<Game> game, Setup setup) {
        return Util.ToStringArray(NIGHT_ACTION_DESCRIPTION);
    }

    @Override
    public String getAbilityDescription(Optional<Game> game, Setup setup) {
        return "stab someone, killing them";
    }

    @Override
    public void mainAbilityCheck(Action a) {
        AbilityValidationUtil.deadCheck(a);
    }

    public static final String DEATH_FEEDBACK = "You were killed by a Serial Killer!";

    @Override
    public void doNightAction(Action a) {
        Player owner = a.owner, target = a.getTarget();
        if(target == null)
            return;
        a.markCompleted();
        Kill(owner, target, Constants.SK_KILL_FLAG);// visiting taken care of already in kill
    }

    @Override
    public boolean isNegativeAbility() {
        return true;
    }

    public static FactionRole template(Faction faction) {
        Role role = RoleService.createRole(faction.setup, "Serial Killer", SerialKiller.abilityType,
                Bulletproof.abilityType);
        return FactionRoleService.createFactionRole(faction, role);
    }
}
