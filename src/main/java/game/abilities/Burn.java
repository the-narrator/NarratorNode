package game.abilities;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import game.abilities.util.AbilityValidationUtil;
import game.event.DeathAnnouncement;
import game.event.Feedback;
import game.event.Happening;
import game.logic.Game;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.listeners.NarratorListener;
import game.logic.support.Constants;
import game.logic.support.Random;
import game.logic.support.action.Action;
import game.logic.support.attacks.DirectAttack;
import game.logic.support.attacks.IndirectAttack;
import game.setups.Setup;
import models.Faction;
import models.FactionRole;
import models.Role;
import models.enums.AbilityModifierName;
import models.enums.AbilityType;
import models.enums.SetupModifierName;
import models.modifiers.Modifiers;
import services.FactionRoleService;
import services.RoleService;

public class Burn extends GameAbility {

    public static final AbilityType abilityType = AbilityType.Burn;
    public static final String COMMAND = abilityType.command;
    public static final String BURN_LOWERCASE = COMMAND.toLowerCase();

    // TODO add can't burn right now if burned previous day/night phase

    public Burn(Game game, Setup setup, Modifiers<AbilityModifierName> modifiers) {
        super(game, setup, modifiers);
    }

    @Override
    public AbilityType getAbilityType() {
        return abilityType;
    }

    private boolean burnedDuringDay;
    private boolean burnedLastNight = false;
    private int dayIgnites;

    @Override
    public GameAbility initialize(Player p) {
        Game narrator = p.game;
        dayIgnites = narrator.getInt(SetupModifierName.ARSON_DAY_IGNITES);
        return super.initialize(p);
    }

    @Override
    public String getUsage(Player p, Random r) {
        return COMMAND;
    }

    @Override
    public List<SetupModifierName> getSetupModifierNames() {
        List<SetupModifierName> names = new LinkedList<>();
        names.add(SetupModifierName.ARSON_DAY_IGNITES);
        names.add(SetupModifierName.ARCH_BURN);
        return names;
    }

    public static final String NIGHT_ACTION_DESCRIPTION = "Burn everyone that is doused in gasoline.";
    public static final String DEATH_FEEDBACK = "You were burned to death by an Arsonist";

    @Override
    public ArrayList<Object> getActionDescription(ArrayList<Action> actions) {
        ArrayList<Object> list = new ArrayList<>();

        list.add("ignite those doused");

        return list;
    }

    @Override
    public void targetSizeCheck(Action a) {
        if(!a.getTargets().isEmpty())
            AbilityValidationUtil.Exception("You can't target anyone with that ability");
    }

    @Override
    public void mainAbilityCheck(Action a) {
        AbilityValidationUtil.deadCheck(a);

        if(burnedDuringDay())
            AbilityValidationUtil.Exception("You can't burn again if you burned during the day");
    }

    @Override
    public PlayerList getAcceptableTargets(Player pi) {
        if(burnedDuringDay())
            return new PlayerList();
        if(pi.isPuppeted())
            return new PlayerList();
        return null;
    }

    @Override
    public void doNightAction(Action a) {
        Player owner = a.owner, target = a.getTarget();

        if(a.abilityType == Burn.abilityType){
            burn(owner);
            if(target != null)
                owner.visit(target);
            setBurnedLastNight();
        }
        a.markCompleted();
    }

    @Override
    public void onDayStart(Player p) {
        super.onDayStart(p);
        if(burnedDuringDay())
            burnedDuringDay = false; // set burned during day false
    }

    @Override
    public void onNightStart(Player p) {
        super.onNightStart(p);
        if(getBurnedLastNight())
            this.burnedLastNight = false;
    }

    private boolean getBurnedLastNight() {
        return burnedLastNight;
    }

    private void setBurnedLastNight() {
        burnedLastNight = true;
    }

    private void setBurnedDuringDay() {
        burnedDuringDay = true;
    }

    boolean burnedDuringDay() {
        return burnedDuringDay;
    }

    private boolean hasDayIgnite() {
        return this.dayIgnites != 0;
    }

    @Override
    public boolean canUseDuringDay(Player p) {
        return hasDayIgnite() && !getBurnedLastNight();
    }

    public static final String NO_DAY_DEATHS = "There was an minor explosion but no one appeared hurt.";
    public static final String DOUSED_FEEDBACK = "You were doused with gasoline last night!";

    public static DeathAnnouncement ExplosionAnnouncement(Game n, PlayerList burned, String type) {
        DeathAnnouncement e = new DeathAnnouncement(burned, n, DeathAnnouncement.DAY_DEATH);
        e.setPicture(type);

        if(burned.isEmpty())
            e.add(NO_DAY_DEATHS);
        else if(burned.size() == 1)
            e.add("There was an explosion and ", burned.get(0), " was found twitching on the floor. Dead.");
        else if(burned.size() == 2)
            e.add("There was a huge explosion, and ", burned.get(0), " and ", burned.get(1), " were found dead.");
        else
            e.add("There was a huge explosion, and ", burned, " were found dead.");

        return e;
    }

    @Override
    public void doDayAction(Action action, Action parentAction) {
        Player owner = action.owner;
        Game game = owner.game;

        PlayerList burned = new PlayerList();
        for(Player p: game.getLivePlayers()){
            if(p.isDoused()){
                if(!new IndirectAttack(Constants.ARSON_KILL_FLAG, p, owner).isImmune())
                    ;
                burned.add(p);
            }

        }
        DeathAnnouncement e = ExplosionAnnouncement(game, burned, "survivor");

        // it might be possible that arsons wont be able to douse the next night because
        // they just burned
        dayIgnites--;
        setBurnedDuringDay();

        game.announcement(e);

        burned = burn(owner);
        game.voteSystem.removeVotersOf(burned);
        for(Player p: burned){
            p.dayKill(new DirectAttack(owner, p, Constants.ARSON_KILL_FLAG), parentAction.timeLeft,
                    Game.DAY_NOT_ENDING);
        }

        if(game.isDay()){
            List<NarratorListener> listeners = owner.game.getListeners();
            for(NarratorListener listener: listeners)
                listener.onDayBurn(owner, burned, e);
            game.parityCheck();
        }
    }

    private PlayerList burn(Player owner) {
        Game n = owner.game;
        PlayerList dousedTargets = new PlayerList();
        for(Player p: n.getLivePlayers()){
            if(!p.isInTown())
                continue;
            if(p.isDoused() || Architect.RoomFire(p))
                dousedTargets.add(p);
        }
        for(Player p: dousedTargets){
            // if your target isn't an arson, or if they're you, or if they weren't about to
            // burn
            if(!p.is(Burn.abilityType) || p == owner || !p.getActions().isTargeting(new PlayerList(), abilityType))
                p.setDoused(false);
            Kill(owner, p, Constants.ARSON_KILL_FLAG);
        }
        Happening e = new Happening(n);
        e.add(owner, " ignited, attacking ");
        if(dousedTargets.isEmpty())
            e.add("no one.");
        else
            e.add(dousedTargets, ".");

        return dousedTargets;
    }

    @Override
    public ArrayList<Object> getInsteadPhrase(Action a) {
        ArrayList<Object> list = new ArrayList<Object>();

        list.add("burning everyone");

        return list;
    }

    @Override
    public String getDayCommand() {
        if(!burnedDuringDay())
            return COMMAND;
        return null;
    }

    @Override
    public ArrayList<String> getCommandParts(Action action) {
        ArrayList<String> commands = new ArrayList<>();
        commands.add(COMMAND);
        return commands;
    }

    public static final String ARCHITECT_ROOM_SPREAD_TEXT = "Your burned targets will also kill undoused people in architect rooms.";
    public static final String ARCHITECT_ROOM_NO_SPREAD_TEXT = "The fires in your burned target's architect rooms will not spread.";

    @Override
    protected ArrayList<String> getProfileHints(Player p) {
        ArrayList<String> ret = new ArrayList<>();

        if(p.game.getInt(SetupModifierName.ARSON_DAY_IGNITES) == 0){
            ret.add("You do not have any daytime ignites.");
        }else if(hasDayIgnite()){
            if(!getBurnedLastNight()){
                ret.add("If you choose, you may ignite during the day.");
            }else{
                ret.add("You burned last night, making you unable to burn today.");
            }
        }else{
            ret.add("You cannot cause anymore daytime ignitions.");
        }

        if(narrator.setup.hasPossibleFactionRoleWithAbility(narrator, Architect.abilityType)){
            if(p.game.getBool(SetupModifierName.ARCH_BURN))
                ret.add(ARCHITECT_ROOM_SPREAD_TEXT);
            else
                ret.add(ARCHITECT_ROOM_NO_SPREAD_TEXT);
        }

        return ret;
    }

    @Override
    public String getAbilityDescription(Optional<Game> game, Setup setup) {
        return NIGHT_ACTION_DESCRIPTION;
    }

    @Override
    public boolean isDayAbility() {
        return player.game.getInt(SetupModifierName.ARSON_DAY_IGNITES) != 0 || player.game.isNight();
    }

    @Override
    public int maxNumberOfSubmissions() {
        return 1;
    }

    @Override
    public boolean stopsParity(Player p) {
        if(p.isPuppeted())
            return false;
        return hasDayIgnite() && (!p.game.isFirstDay() || !p.game.isFirstPhase()) && !burnedDuringDay();
    }

    public static void FeedbackGenerator(Feedback feedback) {
        feedback.add(Douse.DOUSED_FEEDBACK).setPicture("arsonist")
                .addExtraInfo("You are now at great risk of being burned alive!");
    }

    @Override
    public Optional<Action> getExampleAction(Player owner) {
        if(owner.game.isNight())
            return Optional.of(new Action(owner, abilityType));
        return Optional.empty();
    }

    @Override
    public boolean isSelfTargetModifiable() {
        return false;
    }

    @Override
    public boolean isBackToBackModifiable() {
        return false;
    }

    @Override
    public boolean isZeroWeightModifiable() {
        return false;
    }

    @Override
    public boolean canSelfTarget() {
        return false;
    }

    public static FactionRole template(Faction faction) {
        Role role = RoleService.createRole(faction.setup, "Arsonist", Douse.abilityType, Burn.abilityType,
                Undouse.abilityType, Bulletproof.abilityType);
        return FactionRoleService.createFactionRole(faction, role);
    }

}
