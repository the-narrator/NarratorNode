package game.abilities;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import game.logic.Game;
import game.logic.Player;
import game.setups.Setup;
import models.Faction;
import models.FactionRole;
import models.Role;
import models.enums.AbilityModifierName;
import models.enums.AbilityType;
import models.enums.SetupModifierName;
import models.modifiers.Modifiers;
import services.FactionRoleService;
import services.RoleService;

public class Bomb extends Passive {

    public static final AbilityType abilityType = AbilityType.Bomb;

    public Bomb(Game game, Setup setup, Modifiers<AbilityModifierName> modifiers) {
        super(game, setup, modifiers);
    }

    @Override
    public AbilityType getAbilityType() {
        return abilityType;
    }

    @Override
    public String getAbilityDescription(Optional<Game> game, Setup setup) {
        return NIGHT_ACTION_DESCRIPTION;
    }

    public static final String NIGHT_ACTION_DESCRIPTION = "You will kill anyone who attacks you.";
    public static final String DEATH_FEEDBACK = "You attacked a bomb, resulting in your untimely death!";

    @Override
    public List<SetupModifierName> getSetupModifierNames() {
        List<SetupModifierName> names = new LinkedList<>();
        names.add(SetupModifierName.BOMB_PIERCE);
        return names;
    }

    @Override
    protected ArrayList<String> getProfileHints(Player p) {
        ArrayList<String> ret = new ArrayList<>();

        if(p.game.getBool(SetupModifierName.BOMB_PIERCE)){
            ret.add("You will kill death immune targets.");
        }else{
            ret.add("You won't kill death immune targets.");
        }

        return ret;
    }

    @Override
    public boolean affectsSending() {
        return true;
    }

    @Override
    public boolean isPowerRole() {
        return true;
    }

    @Override
    public boolean isNightless(Optional<Integer> playerCount) {
        return true;
    }

    @Override
    public boolean isHiddenModifiable() {
        return true;
    }

    public static FactionRole template(Faction faction) {
        Role role = RoleService.createRole(faction.setup, "Bomb", Bomb.abilityType);
        return FactionRoleService.createFactionRole(faction, role);
    }
}
