package game.abilities;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Optional;

import game.ai.Controller;
import game.ai.ControllerList;
import game.event.Message;
import game.event.VoteAnnouncement;
import game.logic.Game;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.support.Constants;
import game.logic.support.action.Action;
import game.setups.Setup;
import models.enums.AbilityModifierName;
import models.enums.AbilityType;
import models.enums.GamePhase;
import models.enums.SetupModifierName;
import models.modifiers.Modifiers;
import models.serviceResponse.AbilityMetadata;
import util.Util;
import util.game.VoteUtil;

public class Vote extends GameAbility {

    public static final AbilityType abilityType = AbilityType.Vote;
    public static final String COMMAND = abilityType.command;

    public Vote(Game game, Setup setup, Modifiers<AbilityModifierName> modifiers) {
        super(game, setup, modifiers);
    }

    public static final String UNVOTE = "unvote";

    @Override
    public AbilityType getAbilityType() {
        return abilityType;
    }

    @Override
    public void mainAbilityCheck(Action action) {
        action.owner.game.voteSystem.isValidVote(action);
    }

    @Override
    public void doNightAction(Action action) {
        Visit.NoNightActionVisit(action);
    }

    @Override
    public String[] getRoleBlurb(Optional<Game> game, Setup setup) {
        // TODO Auto-generated method stub
        return new String[0];
    }

    @Override
    public String getAbilityDescription(Optional<Game> game, Setup setup) {
        return "Vote to eliminate someone during the day";
    }

    @Override
    public void doDayAction(Action infoAction, Action parentAction) {
        infoAction.owner.game.voteSystem.vote(infoAction, parentAction);
    }

    @Override
    public void pushReplaceMessage(Action oldAction, Action newAction) {

    }

    @Override
    public Message getCancelMessage(Action action) {
        return action.owner.game.voteSystem.getUnvoteMessage(action);
    }

    @Override
    public void onCancelAction(Action action, double timeLeft, Message voteAnnouncement) {
        action.owner.getActions().remove(action);
        narrator.voteSystem.onCancelVote(action, timeLeft, (VoteAnnouncement) voteAnnouncement);
    }

    @Override
    public void resolveDayAction(Action action) {
        // vote's don't do anything at the end of day
    }

    @Override
    public boolean isNightAbility(Player p) {
        return false;
    }

    @Override
    public boolean isDayAbility() {
        return true;
    }

    @Override
    public boolean canUseDuringDay(Player p) {
        return true;
    }

    @Override
    public boolean isPowerRole() {
        return false;
    }

    @Override
    public boolean isChargeModifiable() {
        return false;
    }

    @Override
    public int maxNumberOfSubmissions() {
        return 1;
    }

    @Override
    public boolean isAllowedFactionAbility() {
        return false;
    }

    @Override
    public boolean canSelfTarget() {
        return narrator.getBool(SetupModifierName.SELF_VOTE);
    }

    @Override
    public void skipDayTargetCheck(Action action) {
        if(!action.owner.game.getBool(SetupModifierName.SKIP_VOTE))
            super.skipDayTargetCheck(action);
    }

    @Override
    public void targetSizeCheck(Action action) {
        action.owner.game.voteSystem.targetSizeCheck(action);
    }

    @Override
    public PlayerList getAcceptableTargets(Player pi) {
        PlayerList targets = pi.game.voteSystem.getElligibleTargets();
        if(!pi.game.getBool(SetupModifierName.SELF_VOTE))
            targets.remove(pi);
        return targets;
    }

    @Override
    public boolean isAvailableAbility(Player p) {
        return p.game.phase == GamePhase.VOTE_PHASE;
    }

    // it is safe to edit commands
    @Override
    public Action parseCommand(Player p, double timeLeft, ArrayList<String> commands) {
        if(isSkipVote(commands))
            return VoteUtil.skipAction(p, timeLeft);
        return super.parseCommand(p, timeLeft, commands);
    }

    private static boolean isSkipVote(ArrayList<String> commands) {
        if(commands.size() != 3)
            return false;
        String possibleSkipday = Util.SpacedString(commands.get(1), commands.get(2)).replaceAll(",", "");
        return possibleSkipday.equalsIgnoreCase(Constants.SKIP_DAY);
    }

    public static Action getAction(Controller voter, Game game, Controller... targets) {
        return new Action(voter.getPlayer(), abilityType, new LinkedList<>(),
                ControllerList.ToPlayerList(game, targets));
    }

    public static Action getAction(Controller voter, double timeLeft, Game game, Controller... targets) {
        return new Action(voter.getPlayer(), abilityType, timeLeft, new LinkedList<>(),
                ControllerList.ToPlayerList(game, targets));
    }

    public static AbilityMetadata getSkipMetadata() {
        return new AbilityMetadata(Constants.SKIP_DAY, "opt to forgo public executuions, and going to the night phase",
                Constants.SKIP_DAY, true);
    }

    public static AbilityMetadata getUnvoteMetadata() {
        return new AbilityMetadata(Vote.UNVOTE, "cancel your previous vote", Vote.UNVOTE, true);
    }
}
