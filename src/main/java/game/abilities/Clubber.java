package game.abilities;

import java.util.Optional;

import game.logic.Game;
import game.logic.Player;
import game.logic.support.action.Action;
import game.setups.Setup;
import models.Faction;
import models.FactionRole;
import models.Role;
import models.enums.AbilityModifierName;
import models.enums.AbilityType;
import models.modifiers.Modifiers;
import services.FactionRoleService;
import services.RoleService;

public class Clubber extends GameAbility {
    public static final AbilityType abilityType = AbilityType.Clubber;
    public static final String COMMAND = abilityType.command;

    public Clubber(Game game, Setup setup, Modifiers<AbilityModifierName> modifiers) {
        super(game, setup, modifiers);
    }

    public static final String NIGHT_ACTION_DESCRIPTION = "An acolyte of the masonry, kill all cultic heretics.";
    public static final String ROLE_NAME = "Mason Clubber";

    @Override
    public AbilityType getAbilityType() {
        return abilityType;
    }

    @Override
    public String getAbilityDescription(Optional<Game> game, Setup setup) {
        return NIGHT_ACTION_DESCRIPTION;
    }

    @Override
    public void mainAbilityCheck(Action a) {
        MasonLeader.MasonryCheck(a);
    }

    @Override
    public void doNightAction(Action a) {
        Player target = a.getTarget();
        Player ml = a.owner;
        if(target == null)
            return;

        if(target.isCulted()){
            a.markCompleted();
            Kill(ml, target, MasonLeader.BLUDGEON);
        }else{
            Visit.NoNightActionVisit(a);
        }
    }

    @Override
    public boolean isNegativeAbility() {
        return true;
    }

    public static FactionRole template(Faction faction) {
        Role role = RoleService.createRole(faction.setup, "Clubber", Clubber.abilityType);
        return FactionRoleService.createFactionRole(faction, role);
    }
}
