package game.abilities;

import java.util.Optional;

import game.abilities.util.AbilityValidationUtil;
import game.logic.Game;
import game.logic.Player;
import game.logic.support.action.Action;
import game.setups.Setup;
import models.Faction;
import models.FactionRole;
import models.Role;
import models.enums.AbilityModifierName;
import models.enums.AbilityType;
import models.modifiers.Modifiers;
import services.FactionRoleService;
import services.RoleService;

public class Agent extends GameAbility {

    public static final AbilityType abilityType = AbilityType.Agent;
    public static final String COMMAND = abilityType.command;

    public Agent(Game game, Setup setup, Modifiers<AbilityModifierName> modifiers) {
        super(game, setup, modifiers);
    }

    public static final String NIGHT_ACTION_DESCRIPTION = "Stalk someone to find out who their target visited and who visited them.";

    @Override
    public AbilityType getAbilityType() {
        return abilityType;
    }

    @Override
    public String getAbilityDescription(Optional<Game> game, Setup setup) {
        return NIGHT_ACTION_DESCRIPTION;
    }

    public static final String FEEDBACK = "These are the people who visited your target: ";

    @Override
    public void doNightAction(Action a) {
        Player target = a.getTarget();
        if(target == null)
            return;
        Player owner = a.owner;
        if(owner.game.pre_visiting_phase){
            owner.visit(target);
            return;
        }
        Detective.follow(owner, target, a.getIntendedTarget(), null);
        Lookout.watch(owner, target, a.getIntendedTarget());
        GameAbility.happening(owner, " stalked ", target);
        a.markCompleted();

    }

    @Override
    public void mainAbilityCheck(Action a) {
        AbilityValidationUtil.deadCheck(a);
    }

    @Override
    public boolean isFakeBlockable() {
        return false;
    }

    @Override
    public boolean affectsSending() {
        return true;
    }

    @Override
    public boolean showSelfTargetTextDefault() {
        return true;
    }

    @Override
    public boolean getDefaultSelfTargetValue() {
        return true;
    }

    public static FactionRole template(Faction faction) {
        return FactionRoleService.createFactionRole(faction, template(faction.setup));
    }

    public static Role template(Setup setup) {
        return RoleService.createRole(setup, "Agent", Agent.abilityType);
    }
}
