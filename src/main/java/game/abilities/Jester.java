package game.abilities;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import game.abilities.util.AbilityValidationUtil;
import game.event.Feedback;
import game.event.Happening;
import game.event.Message;
import game.logic.Game;
import game.logic.GameFaction;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.support.Constants;
import game.logic.support.action.Action;
import game.logic.support.attacks.IndirectAttack;
import game.setups.Setup;
import models.Faction;
import models.FactionRole;
import models.Role;
import models.enums.AbilityModifierName;
import models.enums.AbilityType;
import models.enums.SetupModifierName;
import models.modifiers.Modifiers;
import services.FactionRoleService;
import services.RoleService;
import util.Util;

public class Jester extends GameAbility {

    public static final AbilityType abilityType = AbilityType.Jester;
    public static final String COMMAND = abilityType.command;

    public Jester(Game game, Setup setup, Modifiers<AbilityModifierName> modifiers) {
        super(game, setup, modifiers);
    }

    @Override
    public String[] getRoleBlurb(Optional<Game> game, Setup setup) {
        return Util.ToStringArray(NIGHT_ACTION_DESCRIPTION);
    }

    @Override
    public String getAbilityDescription(Optional<Game> game, Setup setup) {
        return "annoy people, leaving them feedback";
    }

    @Override
    public AbilityType getAbilityType() {
        return abilityType;
    }

    public static final String FEEDBACK = "You were annoyed last night by a crazy person.";
    public static final String NIGHT_ACTION_DESCRIPTION = "Goal is to die via a day execution, through any means necessary.";
    public static final String DEATH_FEEDBACK = "You killed yourself because you voted for the jester last night!";

    @Override
    public void mainAbilityCheck(Action a) {
        if(!a.owner.game.getBool(SetupModifierName.JESTER_CAN_ANNOY))
            AbilityValidationUtil.noAcceptableTargets();
    }

    @Override
    public void doNightAction(Action a) {
        Player target = a.getTarget();
        if(target == null)
            return;
        Message fb = new Feedback(a.getTarget(), FEEDBACK).setPicture("jester");
        Set<FactionRole> drugDealers = setup.getPossibleFactionRolesWithAbility(target.game, DrugDealer.abilityType);
        if(!drugDealers.isEmpty())
            fb.addExtraInfo("You know there is a confirmed Jester in the game.");

        happening(a.owner, " annoyed ", target);
        a.markCompleted();
        a.owner.visit(target);
    }

    @Override
    public List<SetupModifierName> getSetupModifierNames() {
        List<SetupModifierName> names = new LinkedList<>();
        names.add(SetupModifierName.JESTER_KILLS);
        names.add(SetupModifierName.JESTER_CAN_ANNOY);
        return names;
    }

    @Override
    protected ArrayList<String> getProfileHints(Player p) {
        ArrayList<String> ret = new ArrayList<>();

        if(p.game.getInt(SetupModifierName.JESTER_KILLS) > 0){
            ret.add("If you get publicly executed, " + p.game.getInt(SetupModifierName.JESTER_KILLS)
                    + " will commit suicide in guilt.");
        }else{
            ret.add("There are no consequences to you dying.");
        }

        return ret;
    }

    @Override
    public Boolean checkWinEnemyless(Player p) {
        if(p.isAlive())
            return false;
        return p.getDeathType().isLynch();
    }

    @Override
    public boolean canStartWithEnemies() {
        return false;
    }

    public static void handleSuicides(Game n) {
        if(n.getInt(SetupModifierName.JESTER_KILLS) > 0){
            PlayerList jesterList = new PlayerList();
            // jester kills
            for(Player p: n.players)
                if(p.getVotedForJester())
                    jesterList.add(p);
            jesterList.sortByName();
            if(jesterList.size() > 0){
                double suicides_d = (jesterList.size() * n.getInt(SetupModifierName.JESTER_KILLS)) / 100.;
                int suicides_i = (int) Math.ceil(suicides_d);
                suicides_i = Math.min(jesterList.size(), suicides_i);

                PlayerList suicides = new PlayerList();
                Player sKilled;
                while (suicides_i > 0){
                    sKilled = jesterList.getRandom(n.getRandom());
                    sKilled.votedForJester(false);
                    sKilled.kill(new IndirectAttack(Constants.JESTER_KILL_FLAG, sKilled, null));
                    jesterList.remove(sKilled);
                    suicides_i--;
                }
                if(!suicides.isEmpty())
                    new Happening(n).add(suicides, " suicided over the jester kill.");
            }
        }

    }

    @Override
    public boolean isActivePassive(Player p) {
        for(GameFaction t: p.getFactions()){
            if(t.hasEnemies())
                return false;
        }
        return true;
    }

    @Override
    public boolean isNightless(Optional<Integer> playerCount) {
        return !setup.modifiers.getBoolean(SetupModifierName.JESTER_CAN_ANNOY, playerCount)
                || super.isNightless(playerCount);
    }

    public static FactionRole template(Faction faction) {
        Role role = RoleService.createRole(faction.setup, "Jester", Jester.abilityType);
        return FactionRoleService.createFactionRole(faction, role);
    }
}
