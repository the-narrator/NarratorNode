package game.abilities;

import java.util.ArrayList;
import java.util.Optional;

import game.abilities.util.AbilityValidationUtil;
import game.logic.Game;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.support.Constants;
import game.logic.support.StringChoice;
import game.logic.support.action.Action;
import game.logic.support.attacks.DirectAttack;
import game.setups.Setup;
import models.enums.AbilityModifierName;
import models.enums.AbilityType;
import models.modifiers.Modifiers;
import util.Util;
import util.game.ActionUtil;

public class JailExecute extends GameAbility {

    public static final AbilityType abilityType = AbilityType.JailExecute;
    public static final String COMMAND = abilityType.command;
    public static final String NIGHT_ACTION_DESCRIPTION = "Execute captives in jail cells.";

    public JailExecute(Game game, Setup setup, Modifiers<AbilityModifierName> modifiers) {
        super(game, setup, modifiers);
    }

    @Override
    public AbilityType getAbilityType() {
        return abilityType;
    }

    @Override
    public GameAbility initialize(Player p) {
        remainingCleanings = modifiers.getInt(AbilityModifierName.JAILOR_CLEAN_ROLES, p.game);
        p.addAbility(JailCreate.abilityType);
        return super.initialize(p);
    }

    public int remainingCleanings;

    @Override
    public void mainAbilityCheck(Action a) {
        if(a.getArg1() != null){
            if(!Util.isInt(a.getArg1()))
                AbilityValidationUtil.Exception("Option must be a positive integer.");

            int suggestedCleanings = Integer.parseInt(a.getArg1());
            if(suggestedCleanings < 1)
                AbilityValidationUtil.Exception("Option must be a positive integer.");

            if(suggestedCleanings > remainingCleanings)
                AbilityValidationUtil.Exception("You may not clean that many people.");
        }

        if(a.getTarget() == null){
            AbilityValidationUtil.Exception("Must target someone with this ability.");
        }

        Game n = a.owner.game;
        if(lynchedHappened(n))
            AbilityValidationUtil.Exception("Cannot execute after a day execution");

        for(Player executeAttempt: a.getTargets())
            if(!executeAttempt.in(getJailedTargets(a.owner)))
                AbilityValidationUtil.Exception("Cannot execute unjailed people");

    }

    @Override
    public void targetSizeCheck(Action a) {
        if(a.getTargets().size() > getJailedTargets(a.owner).size())
            AbilityValidationUtil.Exception("Cannot execute someone that isn't jailed");
        if(getPerceivedCharges() != Constants.UNLIMITED)
            if(a.getTargets().size() > getPerceivedCharges())
                AbilityValidationUtil.Exception("Not enough executions for that many targets");
    }

    public static boolean lynchedHappened(Game n) {
        PlayerList lynchedPeople = n.getDeadList(n.getDayNumber());
        for(Player p: lynchedPeople){
            if(p.getDeathType().isLynch())
                return true;
        }
        return false;
    }

    public PlayerList getJailedTargets(Player player) {
        if(!player.hasAbility(JailCreate.abilityType))
            return new PlayerList();
        JailCreate jailAbility = player.getAbility(JailCreate.class);
        return jailAbility.getJailedTargets();
    }

    @Override
    public void doNightAction(Action action) {

        int cleanedTargets;
        if(remainingCleanings > 0 && Util.isInt(action.getArg1())){
            cleanedTargets = Integer.parseInt(action.getArg1());
        }else
            cleanedTargets = 0;
        PlayerList jailedTargets = getJailedTargets(action.owner);
        PlayerList targets = action.getTargets();
        boolean killedSomeone = false;
        for(Player target: targets){
            if(!jailedTargets.contains(target)){
                if(jailedTargets.isEmpty()){
                    Visit.NoNightActionVisit(action);
                    return;
                }
                continue;
            }
            if(getRealCharges() != 0){
                killedSomeone = true;
                happening(action.owner, " executed ", target);
                if(targets.indexOf(target) < cleanedTargets){
                    target.setCleaned(target.getSkipper());
                    remainingCleanings--;
                }
                target.kill(new DirectAttack(action.owner, target, Constants.JAIL_KILL_FLAG));
                action.owner.visit(target);
                action.getAbility().useCharge();
            }
        }
        if(killedSomeone)
            action.markCompleted();
    }

    @Override
    public Optional<Action> getExampleWitchAction(Player owner, Player target) {
        PlayerList jailedTargets = this.getJailedTargets(owner);
        if(target.in(jailedTargets))
            return Optional.of(new Action(owner, getAbilityType(), target));
        if(jailedTargets.isEmpty())
            return Optional.empty();
        return Optional.of(new Action(owner, abilityType, jailedTargets.getRandom(owner.game.getRandom())));
    }

    @Override
    public String getAbilityDescription(Optional<Game> game, Setup setup) {
        return NIGHT_ACTION_DESCRIPTION;
    }

    // null return indicates skip it.
    @Override
    protected ArrayList<String> getProfileHints(Player p) {
        int startingCleanCount = this.modifiers.getInt(AbilityModifierName.JAILOR_CLEAN_ROLES, p.game);
        if(startingCleanCount == 0)
            return null;
        ArrayList<String> ret = new ArrayList<>();

        if(remainingCleanings == 1 && 1 == startingCleanCount){
            ret.add("You may clean one execution.");
        }else if(remainingCleanings == 1){
            ret.add("You may clean one more execution.");
        }else if(remainingCleanings > 1){
            ret.add("You may clean " + remainingCleanings + " more execution.");
        }

        return ret;
    }

    @Override
    protected String getProfileChargeText() {
        int perceivedCharges = getPerceivedCharges();
        if(perceivedCharges == Constants.UNLIMITED){
            return ("You can execute as many times as you want.");
        }else if(perceivedCharges == 1){
            return ("You have " + getPerceivedCharges() + " execution left.");
        }else{
            return ("You have " + getPerceivedCharges() + " executions left.");
        }
    }

    @Override
    public ArrayList<Object> getActionDescription(ArrayList<Action> actions) {
        ArrayList<Object> list = new ArrayList<>();

        if(actions.isEmpty())
            return list;
        PlayerList targets = ActionUtil.getActionTargets(actions);
        PlayerList cleaned = new PlayerList();
        Integer i;
        for(Action a: actions){
            if(Util.isInt(a.getArg1())){
                i = Integer.parseInt(a.getArg1());
                cleaned.add(a.getTargets().subList(0, i));
            }
        }
        if(cleaned.size() == targets.size()){
            list.add("clean and execute ");
            list.add(StringChoice.YouYourself(targets));
        }else{
            list.add(COMMAND.toLowerCase());
            list.add(" ");
            list.add(StringChoice.YouYourself(targets));

            if(!cleaned.isEmpty()){
                list.add(" and hide the role");
                if(cleaned.size() > 1)
                    list.add("s");
                list.add(" of ");
                list.add(StringChoice.YouYourself(cleaned));
            }
        }
        return list;
    }

    @Override
    public int maxNumberOfSubmissions() {
        return 1;
    }

    @Override
    public boolean isAllowedFactionAbility() {
        return false;
    }

    @Override
    public boolean isFakeBlockable() {
        return false;
    }

    @Override
    public boolean stopsParity(Player p) {
        if(p.isPuppeted())
            return false;
        return getRealCharges() != 0;
    }

    @Override
    public boolean isSelfTargetModifiable() {
        return false;
    }

}
