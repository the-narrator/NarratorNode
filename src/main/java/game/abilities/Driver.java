package game.abilities;

import java.util.ArrayList;
import java.util.Optional;

import game.abilities.util.AbilityValidationUtil;
import game.event.Happening;
import game.logic.Game;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.support.Random;
import game.logic.support.StringChoice;
import game.logic.support.action.Action;
import game.setups.Setup;
import models.Faction;
import models.FactionRole;
import models.Role;
import models.enums.AbilityModifierName;
import models.enums.AbilityType;
import models.modifiers.Modifiers;
import services.FactionRoleService;
import services.RoleService;

public class Driver extends GameAbility {

    public static final AbilityType abilityType = AbilityType.Driver;
    public static final String COMMAND = abilityType.command;

    public Driver(Game game, Setup setup, Modifiers<AbilityModifierName> modifiers) {
        super(game, setup, modifiers);
    }

    public static final String NIGHT_ACTION_DESCRIPTION = "Pick up any two people to drive around.  Any action that affects one will instead affect the other.";

    @Override
    public AbilityType getAbilityType() {
        return abilityType;
    }

    @Override
    public String getAbilityDescription(Optional<Game> game, Setup setup) {
        return NIGHT_ACTION_DESCRIPTION;
    }

    @Override
    public void mainAbilityCheck(Action a) {
        AbilityValidationUtil.deadCheck(a);

        if(a.getTargets().getFirst() == a.getTargets().getLast())
            AbilityValidationUtil.Exception("This ability can't be used on the same person");
    }

    @Override
    public int getDefaultTargetSize() {
        return 2;
    }

    @Override
    public ArrayList<Object> getActionDescription(ArrayList<Action> actionList) {
        ArrayList<Object> list = new ArrayList<>();

        for(Action da: actionList){
            Player t1 = da._targets.getFirst();
            Player t2 = da._targets.getLast();
            // swaps
            if(t1 == null){
                Player temp;
                temp = t1;
                t1 = t2;
                t2 = temp;
            }

            if(t1 == null){
                list.add("pick up anyone");
            }else if(t2 == null){
                list.add("pick up ");
                list.add(StringChoice.YouYourselfSingle(t1));
            }else{
                list.add("switch ");
                list.add(StringChoice.YouYourselfSingle(t1));
                list.add(" and ");
                list.add(StringChoice.YouYourselfSingle(t2));
            }
            list.add(", ");
        }
        list.remove(list.size() - 1);
        return list;
    }

    public static final String FEEDBACK = "You were bus driven.";

    @Override
    public void doNightAction(Action da) {
        Player bd = da.owner, t1 = da._targets.getFirst(), t2 = da._targets.getLast();
        // these are the intended targets. making sure they aren't null
        if(t1 == null)
            return;
        if(!t1.isInTown() && !t2.isInTown())
            return;
        if(!t1.isInTown()){
            Visit.NoNightActionVisit(bd, t2);
            return;
        }
        if(!t2.isInTown()){
            Visit.NoNightActionVisit(bd, t1);
            return;
        }

        if(t1 == t2){
            Visit.NoNightActionVisit(da);
            return;
        }

        Player address1 = t1.getAddress();
        Player address2 = t2.getAddress();

        t1.setHouse(address2);
        t2.setHouse(address1);// feedback here

        t1.secondaryCause(bd);
        t2.secondaryCause(bd);

        new Happening(bd.game).add(bd, " switched ", t1, " and ", t2, ".");

        da.markCompleted();
        bd.visit(t1, t2);
    }

    @Override
    public ArrayList<Object> getInsteadPhrase(Action a) {
        ArrayList<Object> list = new ArrayList<>();

        list.add("swaping ");
        list.add(StringChoice.YouYourself(a.getTargets()));

        return list;
    }

    @Override
    public PlayerList getAcceptableTargets(Player p) {
        PlayerList players = p.game.getLivePlayers().sortByName();
        if(!canSelfTarget())
            players.remove(p);
        return players;
    }

    @Override
    public boolean allowsForFriendlyFire() {
        return true;
    }

    @Override
    public String getUsage(Player p, Random r) {
        PlayerList live = p.game.getLivePlayers();
        live.shuffle(r, null);
        return getCommand() + " *" + live.getFirst().getName() + " " + live.getLast().getName() + "*";
    }

    @Override
    public boolean showSelfTargetTextDefault() {
        return false;
    }

    @Override
    public boolean getDefaultSelfTargetValue() {
        return true;
    }

    public static Role template(Setup setup) {
        return RoleService.createRole(setup, "Bus Driver", Driver.abilityType);
    }

    public static FactionRole template(Faction faction) {
        return FactionRoleService.createFactionRole(faction, template(faction.setup));
    }
}
