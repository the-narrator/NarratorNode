package game.abilities;

import java.util.Optional;

import game.abilities.util.AbilityValidationUtil;
import game.logic.Game;
import game.logic.Player;
import game.logic.exceptions.IllegalActionException;
import game.logic.support.action.Action;
import game.setups.Setup;
import models.Faction;
import models.FactionRole;
import models.Role;
import models.enums.AbilityModifierName;
import models.enums.AbilityType;
import models.modifiers.Modifiers;
import services.FactionRoleService;
import services.RoleService;

public class Ventriloquist extends GameAbility {

    public static final AbilityType abilityType = AbilityType.Ventriloquist;
    public static final String COMMAND = abilityType.command;

    public Ventriloquist(Game game, Setup setup, Modifiers<AbilityModifierName> modifiers) {
        super(game, setup, modifiers);
    }

    @Override
    public AbilityType getAbilityType() {
        return abilityType;
    }

    @Override
    public boolean isAllowedFactionAbility() {
        return false;
    }

    @Override
    public String getAbilityDescription(Optional<Game> game, Setup setup) {
        return NIGHT_ACTION_DESCRIPTION;
    }

    @Override
    public GameAbility initialize(Player p) {
        Puppet.AddAbility(p);
        return super.initialize(p);
    }

    public static final String NIGHT_ACTION_DESCRIPTION = "A master puppeteer, taking control of people's words and votes during the day.";

    @Override
    public void mainAbilityCheck(Action a) {
        AbilityValidationUtil.deadCheck(a);
        if(a.owner.is(Ghost.abilityType) && a.owner.isAlive())
            AbilityValidationUtil.Exception("Can't possess while alive.");

    }

    public static final String FEEDBACK = "You suddenly lose the ability to vote and speak, horrified by the thought of not being able to control your actions.";

    @Override
    public void doNightAction(Action action) {
        Player owner = action.owner, target = action.getTarget();
        if(target == null)
            return;
        target.addVentController(owner);
        action.markCompleted();
        owner.visit(target);
    }

    @Override
    public boolean isChatRole() {
        return true;
    }

    public static void VentException() {
        throw new IllegalActionException("You cannot vote as other people.");
    }

    @Override
    public boolean isNegativeAbility() {
        return true;
    }

    public static FactionRole template(Faction faction) {
        Role role = RoleService.createRole(faction.setup, "Ventriloquist", Ventriloquist.abilityType);
        return FactionRoleService.createFactionRole(faction, role);
    }
}
