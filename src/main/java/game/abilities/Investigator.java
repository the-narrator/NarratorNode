package game.abilities;

import java.util.Optional;

import game.abilities.util.AbilityValidationUtil;
import game.event.Feedback;
import game.logic.Game;
import game.logic.Player;
import game.logic.support.action.Action;
import game.setups.Setup;
import models.Faction;
import models.FactionRole;
import models.Role;
import models.enums.AbilityModifierName;
import models.enums.AbilityType;
import models.modifiers.Modifiers;
import services.FactionRoleService;
import services.RoleService;
import util.Util;

public class Investigator extends GameAbility {

    public static final AbilityType abilityType = AbilityType.Investigator;
    public static final String COMMAND = abilityType.command;

    public Investigator(Game game, Setup setup, Modifiers<AbilityModifierName> modifiers) {
        super(game, setup, modifiers);
    }

    @Override
    public AbilityType getAbilityType() {
        return abilityType;
    }

    @Override
    public String getAbilityDescription(Optional<Game> game, Setup setup) {
        return NIGHT_ACTION_DESCRIPTION;
    }

    public static final String NIGHT_ACTION_DESCRIPTION = "Investigate someone to determine their role";

    @Override
    public void mainAbilityCheck(Action a) {
        AbilityValidationUtil.deadCheck(a);
    }

    public static final String NOT_SUSPICIOUS = "Your target is not suspicious.";

    @Override
    public void doNightAction(Action a) {
        Player owner = a.owner, target = a.getTarget();
        if(target == null)
            return;

        Feedback(owner, target, a.getIntendedTarget(), false);

        happening(owner, " investigated ", target);
        a.markCompleted();
        owner.visit(target);
    }

    public static Feedback Feedback(Player investigator, Player target, String nullIfJanitor, boolean isDead) {
        String feedbackText = "Your target is a";
        String role = null;
        if(target.getFrameStatus() != null && !isDead){
            role = target.getFrameStatus().getName();
        }else if(target.isDetectable() || isDead){
            role = target.getRoleName();
        }else{
            Optional<FactionRole> factionRole = Snitch.getCitizenTeam(investigator.game);
            if(factionRole.isPresent())
                role = factionRole.get().getName();
            else
                role = target.getRoleName();
        }

        if(Util.vowelStart(role)){
            feedbackText += "n ";
        }else
            feedbackText += " ";

        feedbackText += role + ".";

        Feedback feedback = new Feedback(investigator);
        feedback.add(feedbackText);
        feedback.setPicture("detective");
        if(nullIfJanitor != null)
            feedback.addExtraInfo("As a reminder, you attempted to investigate " + nullIfJanitor + ".");
        if(investigator.game.isDay())
            investigator.sendMessage(feedback);
        return feedback;
    }

    @Override
    public boolean isFakeBlockable() {
        return false;
    }

    public static FactionRole template(Faction faction) {
        return FactionRoleService.createFactionRole(faction, template(faction.setup));
    }

    public static Role template(Setup setup) {
        return RoleService.createRole(setup, "Investigator", Investigator.abilityType);
    }
}
