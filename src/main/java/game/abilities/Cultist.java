package game.abilities;

import java.util.Optional;

import game.logic.Game;
import game.setups.Setup;
import models.enums.AbilityModifierName;
import models.enums.AbilityType;
import models.modifiers.Modifiers;

public class Cultist extends Passive {

    public static final AbilityType abilityType = AbilityType.Cultist;

    public Cultist(Game game, Setup setup, Modifiers<AbilityModifierName> modifiers) {
        super(game, setup, modifiers);
    }

    @Override
    public AbilityType getAbilityType() {
        return abilityType;
    }

    public static final String NIGHT_ACTION_DESCRIPTION = "A member of the ever-growing cult.";

    @Override
    public boolean isNightless(Optional<Integer> playerCount) {
        return true;
    }

    @Override
    public String getAbilityDescription(Optional<Game> game, Setup setup) {
        return NIGHT_ACTION_DESCRIPTION;
    }
}
