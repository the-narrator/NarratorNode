package game.abilities;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import game.abilities.util.AbilityValidationUtil;
import game.ai.Controller;
import game.logic.Game;
import game.logic.GameFaction;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.support.StringChoice;
import game.logic.support.action.Action;
import game.setups.Setup;
import models.enums.AbilityModifierName;
import models.enums.AbilityType;
import models.modifiers.Modifiers;
import util.game.ActionUtil;

public class FactionSend extends GameAbility {

    public static final AbilityType abilityType = AbilityType.FactionSend;
    public static final String COMMAND = abilityType.command;

    public FactionSend(Game game, Setup setup, Modifiers<AbilityModifierName> modifiers) {
        super(game, setup, modifiers);
    }

    public FactionSend(Game game) {
        this(game, game.setup, new Modifiers<>());
    }

    public static final String NIGHT_ACTION_DESCRIPTION = "vote on who controls your factional kill, if there are any ties";

    @Override
    public AbilityType getAbilityType() {
        return abilityType;
    }

    @Override
    public String getAbilityDescription(Optional<Game> game, Setup setup) {
        return NIGHT_ACTION_DESCRIPTION;
    }

    @Override
    public void targetSizeCheck(Action a) {
        if(a.getTargets().size() == 0)
            AbilityValidationUtil.Exception("Can't decide that no one should use team abilities.");
        if(a.owner.isSilenced()){
            if(a._targets.size() != 1)
                AbilityValidationUtil.Exception("Can't decide who should use team abilities if blackmailed.");
        }
    }

    @Override
    public void mainAbilityCheck(Action a) {
        Player owner = a.owner;
        if(owner.isSilenced() && a.getTarget() != owner)
            AbilityValidationUtil.Exception("Cannot indicate who should control faction abilities while blackmailed.");
        AbilityValidationUtil.deadCheck(a);
        PlayerList targets = PlayerList.GetUnique(a.getTargets());
        if(targets.size() != a.getTargets().size())
            AbilityValidationUtil.Exception("Can't have two of the same person in your decision list.");
        boolean foundAlly;
        for(Player p: targets){
            foundAlly = false;
            for(GameFaction t: owner.getFactions()){
                if(t.size() == 1)
                    continue;
                if(!t.knowsTeam())
                    continue;
                if(t.getMembers().contains(p)){
                    foundAlly = true;
                    break;
                }
            }
            if(!foundAlly)
                AbilityValidationUtil.Exception("May only allow allies to use team powers");
        }
    }

    @Override
    public PlayerList getAcceptableTargets(Player p) {
        for(GameFaction t: p.getFactions()){
            if(!t.hasSharedAbilities())
                continue;
            if(t.getMembers().size() == 1)
                return new PlayerList();
            if(p.isSilenced())
                return Player.list(p);
            return t.getMembers().copy();
        }
        return new PlayerList();
    }

    @Override
    public ArrayList<Object> getActionDescription(ArrayList<Action> actions) {
        ArrayList<Object> list = new ArrayList<>();

        // TODO account for multiple people
        StringChoice scTarget = new StringChoice(actions.get(0).getTarget());
        scTarget.add(actions.get(0).getTarget(), "yourself");

        list.add(" voted to allow ");
        list.add(scTarget);
        list.add(" to use the factional kill"); // if it's just single, vs not another factional ability
        return list;
    }

    public static List<List<Set<Player>>> CondorcetOrdering(PlayerList factionMembers) {
        List<List<Set<Player>>> rankings = new ArrayList<>();
        List<Set<Player>> ranking;
        Set<Player> element;
        Action action;
        for(Player player: factionMembers){
            action = player.getAction(FactionSend.abilityType);
            if(action == null)
                continue;
            ranking = new ArrayList<>();
            for(int i = 0; i < player.nightVotePower(factionMembers); i++)
                rankings.add(ranking);
            for(Player sendPreference: action._targets){
                element = new HashSet<>();
                element.add(sendPreference);
                ranking.add(element);
            }
        }
        return rankings;
    }

    @Override
    public void doNightAction(Action a) {

    }

    @Override
    public boolean isSelfTargetModifiable() {
        return false;
    }

    @Override
    public boolean getDefaultSelfTargetValue() {
        return true;
    }

    // move to action util when controllers are gone
    public static Action getAction(Controller sender, Controller target, Game game) {
        return ActionUtil.getSimpleAction(sender.getPlayer(), abilityType, target.getPlayer());
    }
}
