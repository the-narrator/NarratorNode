package game.abilities;

import java.util.Optional;

import game.abilities.util.AbilityValidationUtil;
import game.event.Announcement;
import game.event.AnnouncementCheckCondition;
import game.event.Feedback;
import game.logic.Game;
import game.logic.Player;
import game.logic.support.action.Action;
import game.setups.Setup;
import models.Faction;
import models.FactionRole;
import models.Role;
import models.enums.AbilityModifierName;
import models.enums.AbilityType;
import models.modifiers.Modifiers;
import services.FactionRoleService;
import services.RoleService;

public class Silence extends GameAbility {

    public static final AbilityType abilityType = AbilityType.Silence;
    public static final String COMMAND = abilityType.command;

    public Silence(Game game, Setup setup, Modifiers<AbilityModifierName> modifiers) {
        super(game, setup, modifiers);
    }

    public static final String FEEDBACK = "You were silenced.  You may not talk today.";
    public static final String NIGHT_ACTION_DESCRIPTION = "Stop players from talking.";

    @Override
    public AbilityType getAbilityType() {
        return abilityType;
    }

    @Override
    public void doNightAction(Action a) {
        Player owner = a.owner, target = a.getTarget();
        if(target == null)
            return;

        setSilenced(owner, target);

        a.markCompleted();
        owner.visit(target);
    }

    static void setSilenced(Player owner, final Player target) {
        target.setSilenced();
        Feedback f = new Feedback(target, FEEDBACK);
        f.setPicture("blackmailer");
        f.addExtraInfo("You may not talk today.  Keep quiet.");
        f.hideableFeedback = false;
        happening(owner, " silenced ", target);

        if(announcedSilencings(owner)){
            Announcement announcement = new Announcement(owner.game);
            announcement.add(target, " is silenced.");
            final int silencedDay = owner.game.getDayNumber() + 1;
            announcement.setCheckCondition(new AnnouncementCheckCondition() {
                @Override
                public boolean isAnnounceable() {
                    if(target.isAlive())
                        return true;
                    return target.getDeathDay() > silencedDay;
                }
            });
            owner.game.announcement(announcement);
        }
    }

    @Override
    public void mainAbilityCheck(Action a) {
        AbilityValidationUtil.deadCheck(a);
    }

    @Override
    public String getAbilityDescription(Optional<Game> game, Setup setup) {
        return NIGHT_ACTION_DESCRIPTION;
    }

    @Override
    public boolean isNegativeAbility() {
        return true;
    }

    @Override
    public boolean showSelfTargetTextDefault() {
        return true;
    }

    public static FactionRole template(Faction faction) {
        Role role = RoleService.createRole(faction.setup, "Silencer", Silence.abilityType);
        return FactionRoleService.createFactionRole(faction, role);
    }

    private static boolean announcedSilencings(Player silencer) {
        Modifiers<AbilityModifierName> modifierMap;
        Game game = silencer.game;
        if(silencer.hasAbility(Silence.abilityType)){
            modifierMap = silencer.getAbility(Silence.class).modifiers;
            if(modifierMap.getBoolean(AbilityModifierName.SILENCE_ANNOUNCED, game))
                return true;
        }
        if(!silencer.hasAbility(Blackmailer.abilityType))
            return false;
        modifierMap = silencer.getAbility(Blackmailer.class).modifiers;
        return modifierMap.getBoolean(AbilityModifierName.SILENCE_ANNOUNCED, game);
    }
}
