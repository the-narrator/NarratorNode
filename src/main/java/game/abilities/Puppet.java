package game.abilities;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import game.abilities.util.AbilityUtil;
import game.abilities.util.AbilityValidationUtil;
import game.ai.Controller;
import game.event.Message;
import game.logic.Game;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.exceptions.PlayerTargetingException;
import game.logic.support.action.Action;
import game.setups.Setup;
import models.enums.AbilityModifierName;
import models.enums.AbilityType;
import models.modifiers.Modifiers;
import util.Util;

public class Puppet extends GameAbility {

    public static final AbilityType abilityType = AbilityType.Puppet;

    @Override
    public AbilityType getAbilityType() {
        return abilityType;
    }

    // this should never be null, right?
    public Puppet(Game game, Setup setup, Modifiers<AbilityModifierName> modifiers) {
        super(game, setup, modifiers);
    }

    @Override
    public String getAbilityDescription(Optional<Game> game, Setup setup) {
        return null;
    }

    @Override
    public void mainAbilityCheck(Action a) {
        if(a.getArg1() == null)
            AbilityValidationUtil.Exception("Must specify what puppet is trying to do.");
        AbilityType abilityType = AbilityUtil.getAbilityByCommand(a.getArg1());
        if(a.owner.getAbility(abilityType) == null)
            AbilityValidationUtil.Exception("Can only specify abilities that everyone has.");
    }

    @Override
    public void skipDayTargetCheck(Action action) {
        Action subAction = getSubAction(action);
        subAction.getAbility().skipDayTargetCheck(subAction);
    }

    public static final String COMMAND = "DayPuppet";

    @Override
    public boolean isAllowedFactionAbility() {
        return false;
    }

    @Override
    public String[] getRoleBlurb(Optional<Game> game, Setup setup) {
        return new String[0];
    }

    @Override
    public void doDayAction(Action ignored, Action parentAction) {
        PlayerList targets = parentAction.getTargets();

        AbilityType subAbilityType = AbilityUtil.getAbilityByCommand(parentAction.getArg1());
        Player puppet = targets.get(0);
        puppet.getAbility(subAbilityType).doDayAction(Puppet.getSubAction(parentAction), parentAction);
    }

    @Override
    public boolean canUseDuringDay(Player p) {
        return p.hasPuppets();
    }

    @Override
    public boolean isNightAbility(Player p) {
        return false;
    }

    @Override
    public void doNightAction(Action a) {
        return;
    }

    public static void AddAbility(Player p) {
        p.addAbility(Puppet.abilityType);
    }

    @Override
    public void targetSizeCheck(Action a) {
        if(a.getTargets().isEmpty())
            AbilityValidationUtil.Exception("You must target someone with puppet for puppet to work.");
        if(a.getArg1() == null)
            return; // handled a little later

        Action subAction = getSubAction(a);
        subAction.getAbility().targetSizeCheck(subAction);
    }

    @Override
    public void selfTargetableCheck(Action a) {
        Action subAction = getSubAction(a);
        if(!subAction.owner.in(a.owner.getPuppets()))
            AbilityValidationUtil.Exception("You may only make your puppets do actions.");
        subAction.getAbility().selfTargetableCheck(subAction);
    }

    @Override
    public ArrayList<String> getCommandParts(Action action) {
        ArrayList<String> commandParts = new ArrayList<>();
        commandParts.add(COMMAND);
        commandParts.add(action.getTargets().get(0).getName());
        Action subAction = getSubAction(action);
        commandParts.addAll(subAction.getAbility().getCommandParts(subAction));
        return commandParts;
    }

    // it is safe to edit commands
    @Override
    public Action parseCommand(Player p, double timeLeft, ArrayList<String> commands) {
        Game narrator = p.game;
        try{
            commands.remove(0);
            Player puppet = narrator.getPlayerByName(commands.remove(0));

            AbilityType subAbilityType = AbilityUtil.getAbilityByCommand(commands.get(0));
            GameAbility ability = puppet.getAbility(subAbilityType);
            Action subAbility = ability.parseCommand(puppet, timeLeft, commands);

            PlayerList targets = new PlayerList(puppet);
            targets.add(subAbility.getTargets());
            List<String> args = Util.toStringList(ability.getCommand());
            args.addAll(subAbility.args);
            return new Action(p, abilityType, timeLeft, args, targets);
        }catch(NullPointerException e){
            throw new PlayerTargetingException("Couldn't find the playerName in question.");
        }
    }

    public static Action getSubAction(Action a) {
        AbilityType abilityNumber = AbilityUtil.getAbilityByCommand(a.getArg1());
        Player puppet = a.getTargets().get(0);
        PlayerList targets = a.getTargets();
        targets.removeFirst();
        return new Action(puppet, abilityNumber, new LinkedList<>(), targets);
    }

    @Override
    public void resolveDayAction(Action action) {
        // we don't want this to throw an error
    }

    @Override
    public boolean isDayAbility() {
        return true;
    }

    @Override
    public void onCancelAction(Action action, double timeLeft, Message voteAnnouncement) {
        super.onCancelAction(action, timeLeft, voteAnnouncement);
        Action subAction = getSubAction(action);
        GameAbility subAbility = subAction.getAbility();
        Message subMessage = subAbility.getCancelMessage(subAction);
        subAbility.onCancelAction(subAction, timeLeft, subMessage);
    }

    public static Action getAction(Controller controller, Action action) {
        List<String> args = Util.toStringList(action.abilityType.command);
        args.addAll(action.args);

        PlayerList targets = action.getTargets();
        targets.add(0, action.owner);

        return new Action(controller.getPlayer(), abilityType, args, targets);
    }
}
