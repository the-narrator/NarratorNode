package game.abilities;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import game.abilities.util.AbilityValidationUtil;
import game.ai.Controller;
import game.ai.ControllerList;
import game.event.Feedback;
import game.event.Happening;
import game.event.Message;
import game.logic.Game;
import game.logic.Player;
import game.logic.exceptions.UnknownPlayerException;
import game.logic.support.HTString;
import game.logic.support.Option;
import game.logic.support.Random;
import game.logic.support.action.Action;
import game.setups.Setup;
import models.Faction;
import models.FactionRole;
import models.PendingRole;
import models.enums.AbilityModifierName;
import models.enums.AbilityType;
import models.enums.SetupModifierName;
import models.modifiers.Modifiers;
import services.RoleChangeService;
import util.Util;
import util.game.LookupUtil;

public class CultLeader extends GameAbility {

    public static final AbilityType abilityType = AbilityType.CultLeader;
    public static final String COMMAND = abilityType.command;

    public CultLeader(Game game, Setup setup, Modifiers<AbilityModifierName> modifiers) {
        super(game, setup, modifiers);
    }

    public static final String ROLE_NAME = "Cult Leader";

    @Override
    public AbilityType getAbilityType() {
        return abilityType;
    }

    @Override
    public String getAbilityDescription(Optional<Game> game, Setup setup) {
        return NIGHT_ACTION_DESCRIPTION;
    }

    @Override
    public List<SetupModifierName> getSetupModifierNames() {
        List<SetupModifierName> names = new LinkedList<>();
        names.add(SetupModifierName.CULT_KEEPS_ROLES);
        names.add(SetupModifierName.CULT_POWER_ROLE_CD);
        names.add(SetupModifierName.CONVERT_REFUSABLE);
        names.add(SetupModifierName.CULT_PROMOTION);
        names.add(SetupModifierName.GUARD_REDIRECTS_CONVERT);
        return names;
    }

    private int powerCD;

    private int getPowerCD() {
        return powerCD;
    }

    public static final String NIGHT_ACTION_DESCRIPTION = "Recruit others into your cult, converting them to your side.";

    public void setPowerCD(int i) {
        powerCD = i;
    }

    @Override
    public void mainAbilityCheck(Action a) {
        AbilityValidationUtil.deadCheck(a);

        if(getPowerCD() > 0)
            AbilityValidationUtil.Exception("You must wait " + getPowerCD() + " more days to convert again.");

        AbilityValidationUtil.noTeamMateTargeting(a);
        if(!a.owner.game.getBool(SetupModifierName.CULT_PROMOTION) && a.getArg1() != null){
            AbilityValidationUtil.Exception("No promotions are allowed");
        }else if(a.getArg1() != null){
            for(Option o: getOptions(player)){
                if(o.getValue().equalsIgnoreCase(a.getArg1()))
                    return;
            }
            AbilityValidationUtil.Exception("Unknown promotion ability");
        }
    }

    private void invitation(Player owner, Player target) {
        Message q = new Happening(target.game);
        q.add(owner, " invited ", target, " to join the " + owner.getGameFaction().getName());

        target.addCultInvitation(owner);
    }

    @Override
    public void doNightAction(Action a) {
        Player owner = a.owner, target = a.getTarget();
        Game narrator = owner.game;

        if(target == null)
            return;

        boolean actionReady = getPowerCD() <= 0;

        if(target.game.getBool(SetupModifierName.GUARD_REDIRECTS_CONVERT))
            target = Bodyguard.redirect(target);

        if(narrator.pre_recruits){
            if(actionReady){
                if(target.cultAttempt == null)
                    target.cultAttempt = Player.list(owner);
                else if(target.is(Infiltrator.abilityType))
                    target.cultAttempt.add(owner);
                else
                    target.cultAttempt = Player.list(owner.getSkipper());
            }
            return;
        }

        if(!actionReady){
            Visit.NoNightActionVisit(a);
            return;
        }

        if(target.getLives() < 0){
            Visit.NoNightActionVisit(a);
            return;
        }

        if(target.cultAttempt.contains(owner)
                && (target.getGameFaction().canRecruitFrom() || target.is(Infiltrator.abilityType))
                && (!Mason.IsMasonType(target) || target.is(Infiltrator.abilityType))
                && !target.is(CultLeader.abilityType) && target.isRecruitable() && !target.willBe(Mason.abilityType)
                && !target.isEnforced() && !target.isJailed()){
            if(narrator.getBool(SetupModifierName.CONVERT_REFUSABLE)
                    && (target.isPowerRole() && !target.is(Infiltrator.abilityType))){
                invitation(owner, target);
                a.markCompleted();
                return;
            }

            if(narrator.getBool(SetupModifierName.CULT_KEEPS_ROLES) && target.isPowerRole())
                setPowerCD(narrator.getInt(SetupModifierName.CULT_POWER_ROLE_CD) + 1);

            // reseting cooldown since its a successful cooldown

            if(!target.is(Infiltrator.abilityType))
                target.setCulted();

            String simpleName = target.getRoleAbilities().get(0).getClass().getSimpleName();
            String roleName = null;
            Faction cultLeaderFaction = owner.getGameFaction().faction;
            boolean success = true;
            if(!narrator.getBool(SetupModifierName.CULT_KEEPS_ROLES) && !target.is(Infiltrator.abilityType)){
                if(narrator.getBool(SetupModifierName.CULT_PROMOTION) && !target.isPowerRole()
                        && !target.hasActivePassive() && a.getArg1() != null){
                    roleName = a.getArg1();
                    changeRole(target, cultLeaderFaction, roleName);
                }else if(narrator.getBool(SetupModifierName.CULT_PROMOTION)
                        && target.getRoleAbilities().filterNot(Vote.abilityType).size() == 1
                        && getOptions(owner).contains(new Option(simpleName))
                        && (a.getArg1() == null || a.getArg1().equalsIgnoreCase(Cultist.class.getSimpleName()))){
                    // do nothing
                }else{
                    changeToCultist(target, cultLeaderFaction);
                }
            }else if(Citizen.hasNoAbilities(target)){
                changeToCultist(target, cultLeaderFaction);
            }else if(!target.is(Infiltrator.abilityType))
                success = setTeam(target, cultLeaderFaction);

            owner.getGameFaction().addMember(target);
            triggerCooldown();
            useCharge();

            if(owner.getGameFaction().hasNightChat())
                target.addChat(narrator.getEventManager().getNightLog(owner.getColor()));

            feedbackForRecruitment(success, roleName, owner, target);

            owner.visit(target);
            a.markCompleted();
        }else{
            Feedback recruitResult = new Feedback(owner);
            recruitResult.hideableFeedback = false;
            recruitResult.add("You failed to recruit your target");
            happening(owner, " failed to recruit ", target);

            if(target.isEnforced() && !target.isJailed()){
                Enforcer.addFeedback(target.getEnforcer(), owner);
            }

            a.markCompleted();
            owner.visit(target);
        }
    }

    private FactionRole getCultist(Faction faction) {
        return faction.getFactionRolesWithAbility(Cultist.abilityType).iterator().next();
    }

    private void changeToCultist(Player player, Faction faction) {
        FactionRole cultist = getCultist(faction);
        player.changeRole(new PendingRole(cultist));
        narrator.getFaction(faction.getColor()).addMember(player);
    }

    private void changeRole(Player player, Faction faction, String roleName) {
        Optional<FactionRole> factionRole = LookupUtil.findFactionRole(narrator.setup, roleName, faction.color);
        if(factionRole.isPresent())
            RoleChangeService.changeRoleAndTeam(player, new PendingRole(factionRole.get()));
    }

    private boolean setTeam(Player player, Faction faction) {
        FactionRole factionRole = faction.roleMap.get(player.gameRole.factionRole.role);
        if(factionRole == null)
            return false;
        player.changeRole(new PendingRole(factionRole));
        return true;
    }

    private static void feedbackForRecruitment(boolean isSuccessful, String roleName, Player owner, Player target) {
        Game game = owner.game;
        if(isSuccessful){
            HTString ht = new HTString(owner.getGameFaction().faction);

            Feedback recruitResult = new Feedback(owner);
            recruitResult.hideableFeedback = false;
            recruitResult.add("Your recruitment was succesful. ");
            recruitResult.add(target.getName() + " is now part of your following.");
            recruitResult.setPicture("mayor");

            Feedback e = new Feedback(target);
            e.hideableFeedback = false;
            e.add(owner, " converted you to the ", ht, ". Your teammates are : ", owner.getGameFaction().getMembers(),
                    ".");
            e.setPicture("cultist");

            Message q = new Happening(game);
            q.add(owner, " recruited ", target, " to the " + owner.getGameFaction().getName());
            if(roleName != null)
                q.add(" as a ", roleName);
            q.add(".");
        }else{
            Message q = new Happening(game);
            q.add(owner, " failed to recruit ", target, " because there was no valid cult faction role to conver to.");
        }
    }

    @Override
    public void onNightStart(Player p) {
        super.onNightStart(p);
        if(getPowerCD() > 0)
            setPowerCD(getPowerCD() - 1);
    }

    @Override
    public String getProfileCooldownText(Player p) {
        int cd = Math.max(this.getRemainingCooldown(), getPowerCD());
        if(cd > 0)
            return "You must wait " + cd + " more days to convert again.";
        return null;
    }

    @Override
    protected ArrayList<String> getProfileHints(Player p) {
        ArrayList<String> ret = new ArrayList<>();

        if(p.game.getBool(SetupModifierName.CULT_KEEPS_ROLES)){
            ret.add("If you convert power roles, they will keep their role and ability.");
        }else{
            ret.add("Anyone you sucessfully convert will become a " + getCultist(p.getGameFaction().faction).getName()
                    + ".");
        }

        if(p.game.getBool(SetupModifierName.CONVERT_REFUSABLE)){
            ret.add("Power roles can refuse your request.");
        }else{
            ret.add("No one can refuse your invitation.");
        }

        if(p.game.getBool(SetupModifierName.CULT_PROMOTION) && p.hasAbility(CultLeader.abilityType)){
            ArrayList<Option> remRoles = p.getAbility(CultLeader.class).getOptions(p);
            if(remRoles.isEmpty()){
                ret.add("You've exhausted your available recruits");
            }else{
                StringBuilder sb = new StringBuilder(
                        "You may try to convert recruits into one of the following roles: ");
                for(int i = 0; i < remRoles.size(); i++){
                    if(i != 0)
                        sb.append(", ");
                    sb.append(remRoles.get(i));
                }
                sb.append('.');
                ret.add(sb.toString());
            }

        }else if(!p.game.getBool(SetupModifierName.CULT_PROMOTION)){
            ret.add("You may not promote anyone in this game");
        }

        if(p.game.getBool(SetupModifierName.GUARD_REDIRECTS_CONVERT)){
            ret.add("If your target is guarded, you will recruit that person instead.");
        }

        return ret;
    }

    @Override
    public boolean isFakeBlockable() {
        return false;
    }

    @Override
    public ArrayList<String> getCommandParts(Action a) {
        ArrayList<String> parsedCommands = new ArrayList<>();
        parsedCommands.add(COMMAND);
        parsedCommands.add(a.getTarget().getName());
        if(a.owner.game.getBool(SetupModifierName.CULT_PROMOTION) && a.getArg1() != null)
            parsedCommands.add(a.getArg1());
        return parsedCommands;
    }

    @Override
    public Action parseCommand(Player p, double timeLeft, ArrayList<String> commands) {
        commands.remove(0);
        Game n = p.game;

        String playerName = commands.remove(0);
        Player pi = n.getPlayerByName(playerName);
        if(pi == null)
            throw new UnknownPlayerException(playerName);

        List<String> args;
        if(!commands.isEmpty() && n.getBool(SetupModifierName.CULT_PROMOTION))
            args = Util.toStringList(commands.get(0));
        else
            args = new LinkedList<>();

        return new Action(p, abilityType, timeLeft, args, Player.list(pi));
    }

    @Override
    public String getUsage(Player p, Random r) {
        ArrayList<Option> options = getOptions(p);
        int index = r.nextInt(options.size());
        if(p.game.getBool(SetupModifierName.CULT_PROMOTION)){
            String ret = super.getUsage(p, r) + " " + options.get(index);
            if(options.size() != 1){
                ret += "\nAll promotions are: ";
                for(int i = 0; i < options.size(); i++){
                    ret += options.get(0);
                    if(i != options.size() - 1){
                        ret += ", ";
                    }
                }
            }
            return ret;
        }
        return super.getUsage(p, r);
    }

    public static final AbilityType[] PROMOTES = { Sheriff.abilityType, Coroner.abilityType, Detective.abilityType,
            Investigator.abilityType, Lookout.abilityType };

    // this is an array list to help out cult leader
    @Override
    public ArrayList<Option> getOptions(Player p) {
        if(!p.game.getBool(SetupModifierName.CULT_PROMOTION))
            return super.getOptions(p);

        ArrayList<Option> promotes = new ArrayList<>();
        Set<FactionRole> factionRoles = getPromoteRoles(p.gameRole.factionRole.faction);

        for(Player q: p.getGameFaction().getMembers())
            factionRoles.remove(q.gameRole.factionRole);

        for(FactionRole factionRole: factionRoles)
            promotes.add(new Option(factionRole.getName()));

        if(!promotes.isEmpty()){
            Set<FactionRole> cultists = p.getGameFaction().faction.getFactionRolesWithAbility(Cultist.abilityType);
            if(!cultists.isEmpty())
                promotes.add(new Option(cultists.iterator().next().getName()));
        }

        return promotes;
    }

    private static Set<FactionRole> getPromoteRoles(Faction faction) {
        Set<FactionRole> promoteableRoles = new HashSet<>();
        Set<FactionRole> roles;
        for(AbilityType abilityType: PROMOTES){
            roles = faction.getFactionRolesWithAbility(abilityType);
            if(!roles.isEmpty())
                promoteableRoles.add(roles.iterator().next());
        }
        return promoteableRoles;
    }

    @Override
    public boolean allowsForFriendlyFire() {
        return true;
    }

    public static Action getUpgradeAction(Controller cultLeader, Controller target, String upgradeName, Game game) {
        List<String> args = Util.toStringList(upgradeName);
        return new Action(cultLeader.getPlayer(), abilityType, args, ControllerList.ToPlayerList(game, target));
    }
}
