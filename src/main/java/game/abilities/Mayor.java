package game.abilities;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import game.abilities.util.AbilityValidationUtil;
import game.event.Announcement;
import game.logic.Game;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.exceptions.IllegalActionException;
import game.logic.listeners.NarratorListener;
import game.logic.support.Constants;
import game.logic.support.HTString;
import game.logic.support.Random;
import game.logic.support.StringChoice;
import game.logic.support.action.Action;
import game.setups.Setup;
import models.Faction;
import models.FactionRole;
import models.Role;
import models.enums.AbilityModifierName;
import models.enums.AbilityType;
import models.enums.RoleModifierName;
import models.enums.SetupModifierName;
import models.modifiers.Modifiers;
import services.FactionRoleService;
import services.RoleModifierService;
import services.RoleService;

public class Mayor extends GameAbility {

    public static final AbilityType abilityType = AbilityType.Mayor;
    public static final String COMMAND = abilityType.command;
    public static final String REVEAL_LOWERCASE = COMMAND.toLowerCase();

    public Mayor(Game game, Setup setup, Modifiers<AbilityModifierName> modifiers) {
        super(game, setup, modifiers);
    }

    @Override
    public AbilityType getAbilityType() {
        return abilityType;
    }

    @Override
    public boolean isAllowedFactionAbility() {
        return false;
    }

    @Override
    public String getAbilityDescription(Optional<Game> game, Setup setup) {
        return NIGHT_ACTION_DESCRIPTION;
    }

    @Override
    public String getUsage(Player p, Random r) {
        return COMMAND;
    }

    @Override
    public PlayerList getAcceptableTargets(Player pi) {
        if(getRevealed())
            return new PlayerList();
        return null;
    }

    @Override
    public List<SetupModifierName> getSetupModifierNames() {
        List<SetupModifierName> names = new LinkedList<>();
        names.add(SetupModifierName.MAYOR_VOTE_POWER);
        return names;
    }

    public String getNightText(Game n) {
        return Constants.NO_NIGHT_ACTION;
    }

    public static final String NIGHT_ACTION_DESCRIPTION = "Reveal during the day to lead the town while also gaining extra votes.";

    @Override
    public void mainAbilityCheck(Action a) {
        if(a.owner.isSilenced())
            throw new IllegalActionException("Can't reveal if blackmailed");
    }

    @Override
    public void doNightAction(Action a) {
        Visit.NoNightActionVisit(a);
    }

    @Override
    public boolean isDayAbility() {
        return true;
    }

    @Override
    public boolean canUseDuringDay(Player p) {
        return !getRevealed() && !p.isSilenced() && !p.isPuppeted();
    }

    @Override
    public void doDayAction(Action action, Action parentAction) {
        Player mayor = action.owner;
        Game narrator = mayor.game;
        Announcement e = new Announcement(narrator);
        e.setPicture("mayor");

        StringChoice sc = new StringChoice(mayor);
        sc.add(mayor, "You");
        e.add(sc, " ");

        sc = new StringChoice("has");
        sc.add(mayor, "have");

        e.add(sc, " revealed as the ", new HTString(mayor.gameRole), "!");

        setRevealed();
        e.finalize();

        mayor.increaseVotePower(mayor.game.getInt(SetupModifierName.MAYOR_VOTE_POWER));

        narrator.checkVote(parentAction.timeLeft);

        if(narrator.isDay()){ // if its still day
            List<NarratorListener> listeners = narrator.getListeners();
            for(NarratorListener listener: listeners)
                listener.onRoleReveal(mayor, e);
            narrator.parityCheck();
        }
    }

    private void setRevealed() {
        isRevealed = true;
    }

    private boolean isRevealed = false;

    private boolean getRevealed() {
        return isRevealed;
    }

    @Override
    public String getDayCommand() {
        return COMMAND;
    }

    @Override
    protected ArrayList<String> getProfileHints(Player p) {
        ArrayList<String> ret = new ArrayList<>();

        int voteCount = p.game.getInt(SetupModifierName.MAYOR_VOTE_POWER);
        if(canUseDuringDay(p)){
            ret.add("You may reveal yourself during the day.  In addition your vote power will be increased by "
                    + voteCount + ".");
        }else{
            ret.add("Your vote power is " + voteCount + ".");
        }
        return ret;
    }

    @Override
    public boolean isChargeModifiable() {
        return false;
    }

    @Override
    public boolean isBackToBackModifiable() {
        return false;
    }

    @Override
    public boolean isNightAbility(Player p) {
        return false;
    }

    @Override
    public boolean stopsParity(Player p) {
        if(p.isPuppeted() || p.isSilenced())
            return false;
        return !getRevealed();
    }

    @Override
    public Optional<Action> getExampleAction(Player owner) {
        return Optional.empty();
    }

    @Override
    public boolean isNightless(Optional<Integer> playerCount) {
        return true;
    }

    @Override
    public boolean isSelfTargetModifiable() {
        return false;
    }

    @Override
    public void targetSizeCheck(Action a) {
        if(!a.getTargets().isEmpty())
            AbilityValidationUtil.Exception("You can't target anyone with that ability");
    }

    public static FactionRole template(Faction faction) {
        Role role = RoleService.createRole(faction.setup, "Mayor", abilityType);
        RoleModifierService.upsertModifier(role, RoleModifierName.UNIQUE, true);
        return FactionRoleService.createFactionRole(faction, role);
    }
}
