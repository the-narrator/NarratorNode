package game.abilities;

import java.util.Optional;

import game.logic.Game;
import game.setups.Setup;
import models.Faction;
import models.FactionRole;
import models.Role;
import models.enums.AbilityModifierName;
import models.enums.AbilityType;
import models.modifiers.Modifiers;
import services.FactionRoleService;
import services.RoleService;

public class Goon extends Passive {

    public static final AbilityType abilityType = AbilityType.Goon;

    public Goon(Game game, Setup setup, Modifiers<AbilityModifierName> modifiers) {
        super(game, setup, modifiers);
    }

    @Override
    public AbilityType getAbilityType() {
        return abilityType;
    }

    public static final String NIGHT_ACTION_DESCRIPTION = "You have more weight in deciding who controls factional abilities (typically the mafia kill).";

    public static final String DEATH_FEEDBACK = "You were killed by the ";
    public static final String ANONYMOUS_DEATH_FEEDBACK = "You became a victim of organized crime.";

    @Override
    public boolean isNightless(Optional<Integer> playerCount) {
        return true;
    }

    public static FactionRole template(Faction mafia) {
        return FactionRoleService.createFactionRole(mafia, template(mafia.setup));
    }

    public static Role template(Setup setup) {
        return RoleService.createRole(setup, "Goon", Goon.abilityType);
    }

    @Override
    public String getAbilityDescription(Optional<Game> game, Setup setup) {
        return NIGHT_ACTION_DESCRIPTION;
    }
}
