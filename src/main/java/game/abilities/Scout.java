package game.abilities;

import java.util.Optional;

import game.abilities.util.AbilityValidationUtil;
import game.event.Feedback;
import game.logic.Game;
import game.logic.Player;
import game.logic.support.action.Action;
import game.setups.Setup;
import models.enums.AbilityModifierName;
import models.enums.AbilityType;
import models.enums.SetupModifierName;
import models.modifiers.Modifiers;

public class Scout extends GameAbility {

    public static final AbilityType abilityType = AbilityType.Scout;

    public Scout(Game game, Setup setup, Modifiers<AbilityModifierName> modifiers) {
        super(game, setup, modifiers);
    }

    @Override
    public AbilityType getAbilityType() {
        return abilityType;
    }

    @Override
    public String getAbilityDescription(Optional<Game> game, Setup setup) {
        return NIGHT_ACTION_DESCRIPTION;
    }

    public static final String NIGHT_ACTION_DESCRIPTION = "Investigate someone, determing if they're recruitable or not.";

    @Override
    public void mainAbilityCheck(Action a) {
        AbilityValidationUtil.deadCheck(a);
        AbilityValidationUtil.noTeamMateTargeting(a);
    }

    @Override
    public void doNightAction(Action a) {
        Player owner = a.owner, target = a.getTarget();
        if(target == null)
            return;

        Feedback(owner, target);

        happening(owner, " investigated ", target);
        a.markCompleted();
        owner.visit(target);
    }

    public static Feedback Feedback(Player scout, Player target) {
        String feedback;
        if(target.getFrameStatus() != null){
            feedback = NOT_RECRUITABLE;
        }else if(target.is(Infiltrator.abilityType) || Citizen.hasNoAbilities(target)){
            feedback = RECRUITABLE;
        }else if(!scout.game.getBool(SetupModifierName.MASON_NON_CIT_RECRUIT)){
            feedback = NOT_RECRUITABLE;
        }else if(target.isPowerRole()){
            feedback = NOT_RECRUITABLE;
        }else{
            feedback = RECRUITABLE;
        }

        Feedback f = new Feedback(scout);
        f.add(feedback);
        f.setPicture("lookout");

        return f;
    }

    public static final String COMMAND = "Peek";
    public static final String NOT_RECRUITABLE = "Your target isn't recruitable.";
    public static final String RECRUITABLE = "Your target can be recruited.";
}
