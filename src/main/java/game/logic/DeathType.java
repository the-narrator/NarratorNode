package game.logic;

import java.util.ArrayList;

import game.logic.support.Constants;
import game.logic.support.StringChoice;

public class DeathType {

    public ArrayList<String[]> attacks = new ArrayList<>();
    boolean isDayDeath;
    private int day;
    private Player dead;

    public DeathType(Player dead, boolean isDayDeath, int day) {
        this.dead = dead;
        this.isDayDeath = isDayDeath;
        this.day = day;
    }

    private PlayerList lynchers;

    public PlayerList getLynchers() {
        return lynchers.copy();
    }

    public void setLynchers(PlayerList lynchers) {
        this.lynchers = lynchers;
    }

    public ArrayList<String[]> getList() {
        if(this.cleaned && !isDayDeath){
            ArrayList<String[]> ret = new ArrayList<>();
            ret.add(Constants.HIDDEN_KILL_FLAG);
            return ret;
        }
        return attacks;
    }

    public void addDeath(String[] type) {
        attacks.add(type);
    }

    public int getDeathDay() {
        return day;
    }

    public int size() {
        return attacks.size();
    }

    public String[] get(int i) {
        return attacks.get(i);
    }

    // this method is only used for TextHandler's announcing
    public String toText() {
        if(attacks.size() == 1)
            return attacks.get(0)[1] + ".";
        StringBuilder sb = new StringBuilder();

        for(int i = 0; i < attacks.size(); i++){
            if(i == attacks.size() - 1){
                sb.append(" and ");
            }
            sb.append(attacks.get(i)[1]);
            sb.append(", ");
        }
        sb.deleteCharAt(sb.length() - 1);
        sb.deleteCharAt(sb.length() - 1);
        sb.append(".");
        return sb.toString();
    }

    private boolean cleaned = false;

    public void setCleaned() {
        cleaned = true;
    }

    public boolean isHidden() {
        return cleaned;
    }

    public boolean isDayDeath() {
        return isDayDeath;
    }

    public boolean isLynch() {
        return this.attacks.contains(Constants.LYNCH_FLAG);
    }

    private Player cause;

    public Player getCause() {
        if(lynchers != null && !lynchers.isEmpty()){
            if(dead.hasSuccessfulBounty())
                return dead.getSuccessfulBounties();
            return lynchers.getLast();
        }
        if(cause == null)
            return dead.getCause();
        return cause;
    }

    public void setCause(Player cause) {
        this.cause = cause;
    }

    public static ArrayList<Object> getInfoParts(Player dead) {
        ArrayList<Object> e = new ArrayList<>();

        StringChoice sc = new StringChoice(dead);
        sc.add(dead, "You");
        e.add(sc);
        e.add(" ");

        sc = new StringChoice("was");
        sc.add(dead, "were");

        e.add(sc);
        e.add(" found ");
        ArrayList<String[]> attacks = dead.getDeathType().getList();
        String deathString;
        if(attacks.size() == 1)
            deathString = attacks.get(0)[1].toLowerCase() + ".";
        else{
            StringBuilder sb = new StringBuilder();
            for(int i = 0; i < attacks.size(); i++){
                if(i == attacks.size() - 1)
                    sb.append("and ");
                sb.append(attacks.get(i)[1].toLowerCase());
                sb.append(", ");
            }
            sb.deleteCharAt(sb.length() - 1);
            sb.deleteCharAt(sb.length() - 1);
            sb.append(".");
            deathString = sb.toString();
        }
        String lastWill = dead.getLastWill(null);
        if(lastWill != null && !lastWill.isEmpty()){
            e.add("Their last will was :\n");
            e.add(lastWill);
        }

        e.add(deathString);

        return e;
    }

}
