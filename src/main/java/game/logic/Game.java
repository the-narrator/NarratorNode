package game.logic;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import game.abilities.Agent;
import game.abilities.Amnesiac;
import game.abilities.Armorsmith;
import game.abilities.ArmsDetector;
import game.abilities.Blackmailer;
import game.abilities.Blacksmith;
import game.abilities.Block;
import game.abilities.Bodyguard;
import game.abilities.BreadAbility;
import game.abilities.Burn;
import game.abilities.Clubber;
import game.abilities.Commuter;
import game.abilities.Coroner;
import game.abilities.Coward;
import game.abilities.CultLeader;
import game.abilities.Detective;
import game.abilities.Disfranchise;
import game.abilities.Disguiser;
import game.abilities.Doctor;
import game.abilities.Douse;
import game.abilities.Driver;
import game.abilities.DrugDealer;
import game.abilities.Elector;
import game.abilities.ElectroManiac;
import game.abilities.Enforcer;
import game.abilities.Executioner;
import game.abilities.FactionKill;
import game.abilities.FactionSend;
import game.abilities.Framer;
import game.abilities.GameAbility;
import game.abilities.Ghost;
import game.abilities.GraveDigger;
import game.abilities.Gunsmith;
import game.abilities.Hidden;
import game.abilities.Infiltrator;
import game.abilities.Interceptor;
import game.abilities.Investigator;
import game.abilities.JailCreate;
import game.abilities.JailExecute;
import game.abilities.Janitor;
import game.abilities.Jester;
import game.abilities.Joker;
import game.abilities.Lookout;
import game.abilities.Mason;
import game.abilities.MasonLeader;
import game.abilities.MassMurderer;
import game.abilities.Operator;
import game.abilities.ParityCheck;
import game.abilities.Poisoner;
import game.abilities.ProxyKill;
import game.abilities.Scout;
import game.abilities.SerialKiller;
import game.abilities.Sheriff;
import game.abilities.Silence;
import game.abilities.Snitch;
import game.abilities.Spy;
import game.abilities.Survivor;
import game.abilities.Tailor;
import game.abilities.TeamTakedown;
import game.abilities.Thief;
import game.abilities.Undouse;
import game.abilities.Ventriloquist;
import game.abilities.Veteran;
import game.abilities.Vigilante;
import game.abilities.Visit;
import game.abilities.Vote;
import game.abilities.Witch;
import game.abilities.support.Bread;
import game.event.Announcement;
import game.event.ArchitectChat;
import game.event.ChatMessage;
import game.event.DeadChat;
import game.event.DeathAnnouncement;
import game.event.EventList;
import game.event.EventLog;
import game.event.EventManager;
import game.event.Feedback;
import game.event.Happening;
import game.event.Message;
import game.event.OGIMessage;
import game.event.SelectionMessage;
import game.event.VoidChat;
import game.logic.exceptions.ChatException;
import game.logic.exceptions.IllegalActionException;
import game.logic.exceptions.NarratorException;
import game.logic.exceptions.PhaseException;
import game.logic.listeners.CommandListener;
import game.logic.listeners.NarratorListener;
import game.logic.support.AlternateLookup;
import game.logic.support.CommandHandler;
import game.logic.support.Constants;
import game.logic.support.DeathDescriber;
import game.logic.support.HTString;
import game.logic.support.Random;
import game.logic.support.RolePackage;
import game.logic.support.Skipper;
import game.logic.support.action.Action;
import game.logic.support.attacks.Attack;
import game.logic.support.attacks.DirectAttack;
import game.logic.support.attacks.IndirectAttack;
import game.setups.Setup;
import json.JSONObject;
import models.Command;
import models.Faction;
import models.FactionRole;
import models.GameFactions;
import models.GameModifiers;
import models.PendingRole;
import models.enums.AbilityType;
import models.enums.GameModifierName;
import models.enums.GamePhase;
import models.enums.SetupModifierName;
import models.logic.vote_systems.VoteSystem;
import models.modifiers.Modifiers;
import services.GameService;
import services.NamingService;
import services.RoleChangeService;
import services.SetupModifierService;
import util.Util;
import util.game.ActionUtil;

// rename to Game
public class Game {
    public static final int NORMAL_HEALTH = 0;
    public static final Player NARRATOR = null;

    /**
     * Default Constructor
     */
    public Game(Setup setup) {
        this.setup = setup;
        events = new EventManager(this);

        skipper = new Skipper(this);
        random = new Random();
        setSeed(0);
        pendingAnnouncements = new ArrayList<>();

        cListeners = new ArrayList<>();

        this.unpickedRoles = null;
    }

    public Skipper skipper;
    public VoteSystem voteSystem;
    public Set<RolePackage> unpickedRoles;
    public Setup setup;

    public String phaseHash = Constants.STARTING_HASH;

    private Random random;
    public Random idRandomer = new Random();

    /**
     * Gets the random variable used for Narrator. This variable is used everywhere
     * else a random call is made, so if you ever wanted to reproduce a game with
     * the same outcomes, all you'd have to do is set the seed for this random
     * object. Do not call it for other applications.
     *
     * @return the Narrator's random object.
     */
    public Random getRandom() {
        return random;
    }

    /**
     * Sets the seed for the Narrator's internal random object.
     *
     * @param seed the value of the seed
     */
    public void setSeed(long seed) {
        this.seed = seed;
        random = new Random();
        random.setSeed(seed);
    }

    private long seed;

    /**
     * Returns the seed for the random object set by the user.
     *
     * @return the seed
     */
    public long getSeed() {
        return seed;
    }

    public GameModifiers<GameModifierName> gameModifiers = new GameModifiers<>(this, new Modifiers<>());

    public boolean getBool(SetupModifierName name) {
        return setup.modifiers.getBoolean(name, this);
    }

    public boolean getBool(GameModifierName name) {
        return (boolean) gameModifiers.getOrDefault(name, name.defaultValue);
    }

    public int getInt(SetupModifierName name) {
        return setup.modifiers.getInt(name, this);
    }

    public int getInt(GameModifierName name) {
        return (int) gameModifiers.getOrDefault(name, name.defaultValue);
    }

    public void setValue(SetupModifierName name, boolean val) {
        SetupModifierService.upsertModifier(this.setup, name, val);
    }

    public void setValue(GameModifierName name, boolean val) {
        GameService.upsertModifier(this, name, val);
    }

    public void setValue(SetupModifierName name, int val) {
        SetupModifierService.upsertModifier(this.setup, name, val);
    }

    public void setValue(GameModifierName name, int val) {
        GameService.upsertModifier(this, name, val);
    }

    public PlayerList players = new PlayerList();

    /**
     * The appropriate way to generate players for a Narrator instance
     *
     * @param name the name of the player
     * @return the Player
     */
    public Player addPlayer(String name) {
        if(isStarted)
            throw new PhaseException("Cannot add players if game has already started");

        NamingService.playerNameCheck(name, this);
        Player p = new Player(name, this);
        players.add(p);
        return p;
    }

    /**
     * Retrieves the Player using their name
     *
     * @param name the name to look up the person by
     * @return the Player found
     */
    public Player getPlayerByName(String name) {
        return getPlayerByName(name, null);
    }

    public Player getPlayerByName(String name, AlternateLookup aLookup) {
        if(Constants.SKIP_DAY.equalsIgnoreCase(name))
            return this.skipper;
        try{
            int position = Integer.parseInt(name) - 1;
            if(position >= 0 && position < players.size()){
                return players.get(position);
            }
        }catch(NumberFormatException e){
        }
        for(Player p: players){
            if(p.getName().equalsIgnoreCase(name)){
                return p;
            }
        }
        if(aLookup != null)
            return aLookup.lookup(name);

        return null;
    }

    public Player getPlayerByID(String name) {
        for(Player p: players){
            if(p.getID().equalsIgnoreCase(name)){
                return p;
            }
        }
        return null;
    }

    /**
     * Returns a list of all the players in the game. The player references are in
     * it, but the returned collection object is a copy. You are free to add or
     * remove players from it as you wish, as it won't affect it.
     *
     * @return the collection of players
     */
    public PlayerList getAllPlayers() {
        synchronized (players){
            return players.copy();
        }
    }

    /**
     * Returns a list of all the dead players in the game.
     *
     * @return the collection of dead players
     */
    public PlayerList getDeadPlayers() {
        return players.getDeadPlayers();
    }

    /**
     * Returns a list of all the live players in the game.
     *
     * @return the collection of live players
     */
    public PlayerList getLivePlayers() {
        return players.getLivePlayers();
    }

    public HashMap<String, GameFaction> _factions = null;

    /**
     * Returns all the teams in the game
     *
     * @return a list of all the Teams in the game
     */
    public Set<GameFaction> getFactions() {
        Set<GameFaction> team = new HashSet<>();
        for(GameFaction faction: _factions.values()){
            if(!Constants.A_SKIP.equals(faction.getColor()))
                team.add(faction);
        }
        return team;
    }

    public Integer roleIDCounter = 0;

    /**
     * Removes the role, whether its random or not, from the Narrator
     *
     * @param hidden the Role to be removed
     */
    public void removeSetupHidden(Hidden hidden) {
        this.setup.rolesList.remove(hidden);
    }

    public void removeAllSetupHiddens(Hidden hidden) {
        this.setup.rolesList.removeAll(hidden);
    }

    /**
     * returns the associated Team by color id. you can't get the skip team from
     * this method
     *
     * @param color the color of the team
     * @return the Team object
     */
    public GameFaction getFaction(String color) {
        if(color == null || color.equals(Constants.A_RANDOM))
            return null;
        return _factions.get(color);
    }

    public GameFaction getFactionByName(String teamToFind) {
        if(teamToFind == null)
            return null;
        teamToFind = teamToFind.replace(" ", "");
        String teamName;
        for(GameFaction t: _factions.values()){
            teamName = t.getName();
            teamName = teamName.replace(" ", "");
            if(teamName.equalsIgnoreCase(teamToFind))
                return t;
        }
        return null;
    }

    /**
     * Returns a list of players with the given role. Primarily used for gathering
     * people with the same role to do night actions with
     *
     * @param roleName the name of the role
     * @return the list of players found
     */

    public boolean isStarted = false;
    public boolean firstPhaseStarted = false;
    public ArrayList<Happening> prehappenings;

    /**
     * Assigns roles to players
     *
     * @param spawns a shuffled list of roles Passed in because if debug mode is on,
     *               it's sorted instead of shuffled
     */
    public RolesList remainingRoles;

    public int dayNumber = 0;

    /**
     * Returns the current day
     *
     * @return the day number
     */
    public int getDayNumber() {
        return dayNumber;
    }

    public static final boolean NIGHT_START = false;
    public static final boolean DAY_START = true;
    public GamePhase phase = GamePhase.UNSTARTED;
    public double timeLeft = 1;

    /**
     * Returns whether the day is going on or not
     *
     * @return whether its day and the game's going, or not
     */
    public boolean isDay() {
        if(phase == GamePhase.ROLE_PICKING_PHASE)
            return false;
        return phase != GamePhase.NIGHT_ACTION_SUBMISSION && isInProgress();
    }

    /**
     * Returns whether the night is going on or not
     *
     * @return whether its night and the game's going, or not
     */
    public boolean isNight() {
        return phase == GamePhase.NIGHT_ACTION_SUBMISSION && isInProgress();
    }

    public boolean isFirstNight() {
        if(!isInProgress() || phase == GamePhase.VOTE_PHASE)
            return false;
        else if(getBool(SetupModifierName.DAY_START))
            return dayNumber == 1;
        return dayNumber == 0;
    }

    public boolean isFirstDay() {
        if(!isInProgress() || phase == GamePhase.NIGHT_ACTION_SUBMISSION)
            return false;
        return dayNumber == 1;
    }

    public boolean isFirstPhase() {
        if(!isInProgress())
            return false;
        else if(dayNumber == 0)
            return true;
        else if(getBool(SetupModifierName.DAY_START) && dayNumber == 1)
            return true;
        else
            return false;
    }

    /**
     * Helper method that starts the night, clears the votes, resets player
     * statuses, and notifies listeners that the game has started
     *
     * @param lynchedList the players who were lynched earlier in the day
     * @param poisoned
     */
    public void startNight(PlayerList lynchedList, PlayerList poisoned, EventList dayHappenings) {
        if(this.isFirstDay())
            this.events.initializeTeamChats();

        phase = GamePhase.NIGHT_ACTION_SUBMISSION;
        nightPhase = GamePhase.NIGHT_ACTION_SUBMISSION;
        this.timeLeft = 1;
        if(!isInProgress()){
            dayNumber++;
            return;
        }

        nightList.clear();

        // have to create the architect chats so architect knows how many chat rooms
        // they created
        this.events.restoreFactionChats();
        this.events.initializeNightLogs();

        for(Player p: players){
            p.onNightStart();
        }

        for(GameFaction t: getFactions()){
            t.onNightStart();
        }

        this.events.deactivateDayChats();
        if(getInt(GameModifierName.NIGHT_LENGTH) == 0)
            endNight();
        else
            for(NarratorListener listener: listeners)
                listener.onNightPhaseStart(lynchedList, poisoned, dayHappenings);
    }

    private void resoveRolePickingPhase() {
        Thief.fillInMissingActions(this);
        doNightAction(Thief.abilityType);

        SelectionMessage e = new SelectionMessage(skipper, false);
        events.addCommand(skipper, Constants.ALL_TIME_LEFT, CommandHandler.END_PHASE);
        e.dontShowPrivate();
    }

    public void endPhase(double timeLeft) {
        if(phase == GamePhase.ROLE_PICKING_PHASE){
            resoveRolePickingPhase();
            GameService.startPlayerPhases(this);
        }else if(isNight())
            forceEndNight(timeLeft);
        else{
            voteSystem.endPhase(timeLeft);

            SelectionMessage e = new SelectionMessage(skipper, false);
            events.addCommand(skipper, timeLeft, CommandHandler.END_PHASE);
            e.dontShowPrivate();
        }
    }

    // this should go
    public void forceEndDay(double timeLeft) {
        if(!isDay())
            return;
        SelectionMessage e = new SelectionMessage(skipper, false);
        events.addCommand(skipper, timeLeft, CommandHandler.END_PHASE);
        e.dontShowPrivate();
        voteSystem.endPhase(timeLeft);
    }

    // people who have completed their night actions
    private PlayerList nightList = new PlayerList();
    private int key = 0;

    /**
     * Signals to the Narrator that the Player has completed their actions. Once
     * every live player has submitted their action, the night will auto end
     *
     * @param p the player who wants to end the night
     */
    protected void endNight(Player p, boolean forced) {

        if(!nightList.contains(p))
            nightList.add(p);

        p.setSubmissionTime(key++);

        if(everyoneCompletedNightActions())
            endNight();
        else{
            for(NarratorListener listener: listeners)
                listener.onEndNight(p, forced);
        }
    }

    /**
     * Returns the people who have signaled that they want the night to end
     *
     * @return a list of people that have ended the night
     */
    public PlayerList getEndedNightPeople() {
        return nightList.copy();
    }

    /**
     * Allows a player to change his mind and unlock their night actions
     *
     * @param p the player who no longer wants to wait for night to end right away
     */
    protected void cancelEndNight(Player p) {
        nightList.remove(p);
        for(NarratorListener listener: listeners)
            listener.onCancelEndNight(p);
    }

    /**
     * Determines if the Player is ready to start the day. Shouldn't be accessed
     * directly. Get this information through the player
     *
     * @param p the requester
     * @return
     */
    protected boolean endedNight(Player p) {
        return nightList.contains(p);
    }

    /**
     * Determines whether night actions are permitted to be done right now
     *
     * @return if game is in progress and it's nighttime
     */
    protected boolean canDoNightAction() { // rename to submit
        if(!isStarted)
            return false;

        boolean gameStillGoing = isInProgress();
        return gameStillGoing && phase == GamePhase.NIGHT_ACTION_SUBMISSION;
    }

    /**
     * Determines whether everyone's completed their night actions. This throws
     * exceptions if it's not nighttime or if something internally has gone wrong.
     *
     * @return True if everyone has completed their night actions
     */
    public boolean everyoneCompletedNightActions() {
        if(phase != GamePhase.NIGHT_ACTION_SUBMISSION)
            throw new IllegalStateException("It is not night time");

        if(players.size() < nightList.size())
            throw new IllegalStateException("Something is wrong with the input of night actions");

        int needToEndNight = 0;
        for(Player p: players){
            if(p.isParticipating())
                needToEndNight++;
        }
        return needToEndNight == nightList.size();
    }

    public PlayerList getNightWaitingOn() {
        PlayerList pl = new PlayerList();
        for(Player p: players){
            if(p.isParticipating()){
                if(!p.endedNight())
                    pl.add(p);
            }
        }
        return pl;
    }

    public EventManager events;

    /**
     * Creates a String representation of everything that's happened in the game for
     * easy vieweing and debugging
     *
     * @return String version of what has happened in the game
     */
    public String getHappenings() {
        return events.getEvents(Message.PRIVATE).access(Message.PRIVATE);
    }

    /**
     * Returns the EventManager object. This is in charge of keeping events ordered
     * and in the right chat topic.
     *
     * @return the Event Manager object
     */
    public EventManager getEventManager() {
        return events;
    }

    private ArrayList<CommandListener> cListeners;

    /**
     * The Command Listeners are notified for when things like voting, ending night,
     * day burning, etc. happen. Its used for recording games.
     *
     * @return the current objects that are listening for command inputs to the game
     */
    public ArrayList<CommandListener> getCommandListeners() {
        return cListeners;
    }

    /**
     * This returns the complete list of string commands needed to reproduce the
     * game to this point.
     *
     * @return
     */
    public ArrayList<Command> getCommands() {
        return events.getCommands();
    }

    /**
     * Helper method that resolves all night actions, sets feedback, and kills
     * people.
     *
     */

    public boolean pre_visiting_phase = true;
    public boolean pre_recruits = true;
    public boolean pre_bodyguard = true;
    public boolean healsComplete = false;
    public boolean proxyWitchVisiting = false;

    private void endNight() {
        nightPhase = GamePhase.NIGHT_ACTION_PROCESSING;
        actionStack.clear();
        pre_visiting_phase = true;
        pre_recruits = true;
        pre_bodyguard = true;
        for(NarratorListener listener: listeners)
            listener.onNightEnding();

        for(GameFaction t: _factions.values()){
            if(!t.hasSharedAbilities())
                continue;

            for(GameAbility a: t.getAbilities()){
                if(a instanceof FactionSend)
                    continue;
                t.determineNightActionController(a.getAbilityType());
            }
        }

        GameFactions whereItCameFrom;
        boolean noOneElseHas;
        // removes extra targets
        for(Player p: players){
            p.onNightEnd();
            for(Action a: p.getActions()){
                a.saveIntendedTargets();
                if(!a.isTeamAbility())
                    continue;
                whereItCameFrom = a.getFactions();
                noOneElseHas = true;

                for(GameFaction t: whereItCameFrom){
                    if(t.getFactionAbilityController(a.abilityType) == p){
                        noOneElseHas = false;
                        break;
                    }
                }
                if(noOneElseHas){
                    a.setInvalid();
                }
                a.saveIntendedTargets();
            }

        }

        nightPhase = GamePhase.PRE_KILLING;

        PlayerList newDeadList = new PlayerList();
        healsComplete = false;

        doNightAction(Commuter.abilityType);
        doNightAction(new PlayerList(), false, GraveDigger.abilityType);// no exclusions, reverse it
        doNightAction(Commuter.abilityType);
        doNightAction(ProxyKill.abilityType);
        doNightAction(Witch.abilityType);
        doNightAction(Commuter.abilityType);
        proxyWitchVisiting = true;
        doNightAction(ProxyKill.abilityType);// for the visitation
        proxyWitchVisiting = false;

        doNightAction(Snitch.abilityType);
        doNightAction(Block.abilityType);
        doNightAction(JailExecute.abilityType);
        doNightAction(Operator.abilityType);
        doNightAction(Coroner.abilityType);
        doNightAction(Driver.abilityType);
        doNightAction(Coward.abilityType);
        doNightAction(Survivor.abilityType);

        doNightAction(Undouse.abilityType);
        doNightAction(Bodyguard.abilityType);
        pre_bodyguard = false;
        doNightAction(Douse.abilityType);

        kills();
        heals();

        doNightAction(Disguiser.abilityType);
        doNightAction(Commuter.abilityType);

        doNightAction(Janitor.abilityType);
        doNightAction(Silence.abilityType, Disfranchise.abilityType, Blackmailer.abilityType, Ventriloquist.abilityType,
                Ghost.abilityType, Elector.abilityType);
        doNightAction(Framer.abilityType);

        doNightAction(Enforcer.abilityType);
        doNightAction(MasonLeader.abilityType);
        doNightAction(CultLeader.abilityType);
        pre_recruits = false;
        doNightAction(CultLeader.abilityType);

        doNightAction(DrugDealer.abilityType);
        doNightAction(Gunsmith.abilityType, Blacksmith.abilityType, Armorsmith.abilityType, BreadAbility.abilityType);
        doNightAction(Amnesiac.abilityType);
        doNightAction(Sheriff.abilityType, ParityCheck.abilityType, Investigator.abilityType, Scout.abilityType,
                ArmsDetector.abilityType);
        doNightAction(Poisoner.abilityType);
        doNightAction(Tailor.abilityType);

        doNightAction(Jester.abilityType);
        doNightAction(Visit.abilityType);
        stalkings();

        Bread.removeExpiredBread(players);

        TeamTakedown.handleNightSuicides(this);

        boolean onlyPoisoned, attacked;
        for(Player p: players){
            if(p.isDead())
                continue;
            onlyPoisoned = true;
            attacked = false;
            for(Attack a: p.getInjuries()){
                if(a.isCountered())
                    continue;
                if(a.isImmune()){
                    if(a.getType() == Constants.POISON_KILL_FLAG)
                        p.setPoisoned(a.getAttacker());
                    continue;
                }
                if(p.getRealAutoVestCount() > 0 && a.getType() != Constants.JAIL_KILL_FLAG){
                    onlyPoisoned = false;
                    if(p.isSquelched())
                        p.silentUseAutoVest();
                    else{
                        p.useAutoVest();
                        new Feedback(p, Constants.AUTO_VEST_USAGE);
                    }
                }else if(a.getType() == Constants.POISON_KILL_FLAG){
                    p.setPoisoned(a.getAttacker());
                    continue;
                }else{
                    onlyPoisoned = false;
                    p.setDead(a.getCause());
                }
                attacked = true;
                break;
            }
            if(!attacked || p.isUsingAutoVest() || onlyPoisoned)
                continue;
            if(Janitor.isBeingCleaned(p)){
                p.getDeathType().setCleaned();
                Janitor.SendOutFeedback(p);
            }
            newDeadList.add(p);
        }

        players.sortByName();

        // executioner changes
        for(Player p: players.filter(Executioner.abilityType)){
            Executioner exec = p.getAbility(Executioner.class);
            for(Player newDead: getDeadList(dayNumber)){
                if(newDead.getDeathType().isLynch())
                    continue;
                if(!newDead.equals(exec.getTarget()))
                    continue;
                if(exec.getRealCharges() != 0 && !players.getLivePlayers().remove(p).isEmpty()){
                    Executioner.assign(p);
                    new Happening(this).add(p, " got ", exec.getTarget(), " as a new target.");
                }else if(getBool(SetupModifierName.EXECUTIONER_TO_JESTER)){
                    new Happening(this).add(p, " suicided over their failure to get ", exec.getTarget(),
                            " to be democratically executed.");
                    FactionRole jester = setup.getFactionRolesWithAbility(Jester.abilityType).iterator().next();
                    p.changeRole(new PendingRole(jester));
                }else{
                    p.clearHeals();
                    new Happening(this).add(p,
                            " suicided over their failures to democratically execute their targets.");
                    p.kill(new IndirectAttack(Constants.JESTER_KILL_FLAG, p, null));
                    p.setDead(p);
                }
            }
        }

        players.sortByName();

        masonLeaderPromotion();

        // changing of roles
        for(Player p: players)
            RoleChangeService.resolveRoleChange(p);

        for(Player p: players)
            p.sendFeedback();

        // ending
        nightList.clear();

        dayNumber++;
        Announcement e = null;
        for(Player dead: newDeadList){
            e = new DeathAnnouncement(dead, Announcement.NIGHT_DEATH);
            e.setPicture("tombstone");

            if(deathDescriber == null || !deathDescriber.hasDeath(dead)){
                for(Object o: DeathType.getInfoParts(dead))
                    e.add(o);

            }else{
                deathDescriber.populateDeath(dead, e);
            }

            dead.onDeath();
            announcement(e);
        }
        if(e == null){
            PlayerList noDead = new PlayerList();
            e = new DeathAnnouncement(noDead, this, Announcement.NIGHT_DEATH);
            e.add("Everyone appears accounted for.");
            announcement(e);
        }

        pushPendingAnnouncements();

        checkProgress();
        healsComplete = false;
        actionStack.clear();
        if(isInProgress()){
            for(GameFaction t: getFactions()){
                t.onNightEnd();
            }
            startDay(newDeadList);
        }
        newDeadList = null;
    }

    private ArrayList<Announcement> pendingAnnouncements;

    public void announcement(Announcement systemMessage) {
        if(isNight() || dayStartingUp){
            pendingAnnouncements.add(systemMessage);
            return;
        }
        if(!systemMessage.isAnnounceable())
            return;
        systemMessage.finalize();
        for(NarratorListener listener: listeners)
            listener.onAnnouncement(systemMessage);
    }

    private void pushPendingAnnouncements() {
        random.shuffle(pendingAnnouncements, "Pending announcements");
        for(Announcement m: pendingAnnouncements){
            if(!m.isAnnounceable())
                continue;
            m.finalize();
            for(NarratorListener listener: listeners)
                listener.onAnnouncement(m);
        }
        pendingAnnouncements.clear();
    }

    public void masonLeaderPromotion() {
        if(!getBool(SetupModifierName.MASON_PROMOTION))
            return;

        Player newLeader;
        PlayerList masonPool;
        for(GameFaction t: this.getFactions()){
            if(!t.getMembers().filter(MasonLeader.abilityType).isEmpty())
                continue;

            masonPool = t.getMembers().getLivePlayers().filter(Mason.abilityType);
            masonPool.remove(t.getMembers().filter(Infiltrator.abilityType));
            if(masonPool.isEmpty()){
                masonPool = t.getMembers().getLivePlayers().filterWillBe(Mason.abilityType);
                masonPool.remove(t.getMembers().filter(Infiltrator.abilityType));
            }
            if(masonPool.isEmpty())
                continue;

            newLeader = masonPool.getRandom(random);
            FactionRole masonLeader = getMasonLeader(t.faction);
            newLeader.changeRole(new PendingRole(masonLeader, true));
        }
    }

    private FactionRole getMasonLeader(Faction faction) {
        for(Player deadPlayer: getDeadPlayers()){
            if(deadPlayer.getGameFaction().faction == faction && deadPlayer.is(MasonLeader.abilityType))
                return deadPlayer.gameRole.factionRole;
        }

        return faction.getFactionRolesWithAbility(MasonLeader.abilityType).iterator().next();
    }

    protected static int UNSUBMITTED = Integer.MAX_VALUE;
    /**
     * Helper method for resolving night kills, which all happen at the same time
     *
     */

    public static final AbilityType[] KILL_ABILITIES = { Veteran.abilityType, SerialKiller.abilityType,
            Disguiser.abilityType, MassMurderer.abilityType, Joker.abilityType, Interceptor.abilityType,
            ElectroManiac.abilityType, Clubber.abilityType, Burn.abilityType, Vigilante.abilityType,
            FactionKill.abilityType };
    public GamePhase nightPhase = GamePhase.NIGHT_ACTION_SUBMISSION;

    private void kills() {
        nightPhase = GamePhase.KILLING;
        doNightAction(new PlayerList(), true, KILL_ABILITIES); // don't exclude anyone, and reverse the actions

        guards();

        // apparently I wrote in that bodyguards will also not be affected by being shot
        // during ' the killing phase '
        nightPhase = GamePhase.POST_KILLING;

        Jester.handleSuicides(this);

    }

    /**
     * Helper method for Bodyguards protecting each other
     *
     */
    private void guards() {
        PlayerList bgs = players.filter(Bodyguard.abilityType);
        bgs.sortBySubmissionTime();

        boolean noOneCompleted = false;
        while (!noOneCompleted){
            noOneCompleted = true;
            for(Player bg: bgs){
                for(Action a: bg.getActions())
                    if(a.abilityType == Bodyguard.abilityType)
                        a.attemptCompletion();
                if(bg.didNightAction()){
                    bgs.remove(bg);
                    noOneCompleted = false;
                    break;
                }
            }
        }
    }

    /**
     * Helper method for doctors healing each other
     *
     */
    private void heals() {

        PlayerList healers = players.filter(Doctor.abilityType).sortBySubmissionTime();
        // should garuntee that all the healers have targets

        PlayerList injured = new PlayerList();

        while (!healers.isEmpty()){
            Player healer = healers.getFirst();

            // if the healer isn't alive
            if(healer.getLives() < NORMAL_HEALTH){
                injured.add(healer);
                healers.remove(healer);
            }

            else{
                // healer does the heal
                doNightAction(getAllPlayers().remove(healer), false, Doctor.abilityType);
                healers.remove(healer);

                PlayerList healerTargets = healer.getActions().getTargets(Doctor.abilityType);

                // injured only contains doctors
                for(Player healerTarget: healerTargets){
                    if(healerTarget.in(injured)){
                        injured.remove(healerTarget);
                        healers.add(healerTarget);
                    }
                }
            }
        }

        healsComplete = true;

    }

    // used by disguiser call, where charged disguisers shouldn't act
    public void doNightAction(PlayerList excluded, AbilityType... abilityTypes) {
        doNightAction(excluded, false, abilityTypes);
    }

    /**
     * Helper method for doing night actions concurrently for different roles.
     *
     * @param roles   the role names that are to do their night actions next
     * @param members that can't act (typically because this is a disguiser call,
     *                and disguisers can't act)
     */
    private void doNightAction(AbilityType... abilityType) {
        doNightAction(new PlayerList(), false, abilityType);
    }

    private void doNightAction(PlayerList excluded, final boolean reversed, AbilityType... abilityType) {
        ArrayList<Action> list = new ArrayList<>();
        for(Player p: players){
            for(Action a: p.getActions()){
                if(a.is(abilityType) && !p.in(excluded)){
                    list.add(a);
                }
            }
        }

        Collections.sort(list, new Comparator<Action>() {
            @Override
            public int compare(Action a1, Action a2) {
                if(a1.owner == a2.owner){
                    if(reversed)
                        return -1;
                    return 1;
                }
                return a1.owner.getSubmissionTime() - a2.owner.getSubmissionTime();
            }
        });

        if(reversed)
            Collections.reverse(list);

        for(Action a: list){
            a.completeNightAction();
        }
    }

    /**
     * Helper method for agents, lookouts, and detectives doing their night action
     *
     */
    private void stalkings() {
        for(int i = 0; i < 2; i++){
            doNightAction(Lookout.abilityType, Detective.abilityType, Spy.abilityType, Agent.abilityType);
            pre_visiting_phase = false;
        }

    }

    /**
     * Helper method for looking for people that have been attacked and not been
     * saved and setting them dead. No one is there to hear your screams.
     *
     * @return the new Dead people
     */
    DeathDescriber deathDescriber = null;

    public void setDeathDescriber(DeathDescriber dd) {
        deathDescriber = dd;
    }

    /*
     * private PlayerList setDead(){ PlayerList newDead = new PlayerList();
     *
     * }
     */

    /**
     * Returns a list of players who all died on the given day
     *
     * @param dayNumber the day number to search by
     * @return the playerrs who died on that day
     */
    public PlayerList getDeadList(int dayNumber) {
        PlayerList deadList = new PlayerList();
        for(Player p: getDeadPlayers()){
            if(p.getDeathDay() == dayNumber)
                deadList.add(p);
        }
        return deadList;
    }

    /**
     * Returns the number of dead people
     *
     * @return number of dead people
     */
    public int getDeadSize() {
        return getDeadPlayers().size();
    }

    /**
     * Returns the number of living people
     *
     * @return number of living people
     */
    public int getLiveSize() {
        return getLivePlayers().size();
    }

    /**
     * Helper method for resetting the votes and notifiying listeners that the day
     * has started
     *
     * @param newDead
     */

    private boolean dayStartingUp = false;

    public void startDay(PlayerList newDead) {
        if(getInt(GameModifierName.DISCUSSION_LENGTH) == 0)
            phase = GamePhase.VOTE_PHASE;
        else
            phase = GamePhase.DISCUSSION_PHASE;

        if(!isInProgress())
            return;

        // for joker bounties
        dayStartingUp = true;

        voteSystem.reset();
        _lynches = 1;

        for(GameFaction t: _factions.values()){
            t.onDayStart();
        }

        this.voteSystem.clearAllVotes();

        for(Player p: players){
            p.onDayStart();
        }
        // for joker bounties
        dayStartingUp = false;

        this.events.deactivateNightChats();
        this.events.grantArchitectAccess();
        this.events.restoreFactionChats();
        this.events.dayChat.setActive();
        for(NarratorListener listener: listeners)
            listener.onDayPhaseStart(newDead);

        parityCheck();
        pushPendingAnnouncements();

    }

    public GameFaction parityReached() {
        if(!this.gameModifiers.getBoolean(GameModifierName.AUTO_PARITY))
            return null;
        for(Player p: getLivePlayers()){
            if(!voteSystem.getVoteTargets(p).contains(skipper) && p.stopsParity())
                return null;
        }

        int count, otherCount, votePower;
        for(GameFaction t: getFactions()){
            if(!t.hasAbility(FactionKill.abilityType))
                continue;
            count = 0;
            otherCount = 0;
            for(Player p: getLivePlayers()){
                if(p.isPuppeted()){
                    if(p.getPuppeteer().getGameFaction() == t){
                        count += p.getVotePower();
                    }else{
                        otherCount += p.getVotePower();
                    }
                }else{
                    if(p.isSilenced())
                        votePower = 1;
                    else
                        votePower = p.getVotePower();
                    if(p.getGameFaction() == t)
                        count += votePower;
                    else
                        otherCount += votePower;
                }
            }

            // it's supposed to be == because if it was greater, then they could just vote
            // out
            if(count == otherCount)
                return t;
        }
        return null;
    }

    public void forceEndNight(double timeLeft) {
        PlayerList waitingFor = new PlayerList();
        for(Player waiting: players){
            if(waiting.isParticipating() && !waiting.endedNight())
                waitingFor.add(waiting);
        }
        for(Player waiting: waitingFor){
            if(phase == GamePhase.NIGHT_ACTION_SUBMISSION && isInProgress())
                waiting.endNight(true, timeLeft); // forced end night
        }
    }

    /**
     * Checks if the voter and target are valid and if not, throws an exception
     *
     * @param voter  the Player voting
     * @param target the Player being voted
     */

    /**
     * Meat Vote method. Shouldn't be called directly.
     *
     * @param voter  the person doing the voting
     * @param target the person that is being voted
     */

    /**
     * Unvoting helper method that removes the target from the voter's vote and the
     * voter from the voter's of people who are voting the target
     *
     * @param voter the player who wants to unvote
     * @return the voter's previous vote
     */

    /**
     * Method that indicates the player wants to skip the vote. Should not be called
     * directly
     *
     * @param p       the player who wants to skip the vote
     * @param command
     */

    /**
     * Helper method to see if minimum vote to lynch has been reached. If so, the
     * night end of day auto starts
     *
     */
    public void checkVote(double timeLeft) {
        voteSystem.checkVote(timeLeft);
    }

    public Object commandLock = new Object();

    /**
     * Adds a phrase 'said' by the player. Should be called from Player, not
     * accessed directly.
     *  @param p       the player talking
     * @param message what the player said
     * @param key     where this message is going
     * @param args
     */
    protected ChatMessage say(Player p, String message, String key, Optional<String> source, JSONObject args) {
        if(p.isSilenced() || p.isPuppeted()){
            if(p.isDead() && key.equals(Constants.DEAD_CHAT)){

            }else{
                Player ventKey = getPlayerByName(key);
                if(ventKey == null || !p.getPuppets().contains(ventKey)){
                    if(p.isSilenced())
                        throw new ChatException("Blackmailed players cannot talk.");
                    throw new ChatException("Puppetted players cannot talk.");
                }
            }
        }

        if((p.isEliminated()) && isInProgress() && !key.equals(Constants.DEAD_CHAT))
            throw new IllegalActionException("Dead players cannot talk.");
        if(isNight()){
            EventLog el = events.getEventLog(key);
            if(key.equals(VoidChat.KEY)){

            }else if(teamChatMessage(p, key)){

            }else if(p.isJailed() && key.startsWith(p.getName())){
                return ChatMessage.JailCaptiveMessage(p, message, key);
            }else if(jailorToCaptiveMessage(p, key)){
                return ChatMessage.JailCaptorMessage(p, message, key);
            }else if(el != null && el instanceof ArchitectChat && el.isActive() && el.getMembers().contains(p)){

            }else if(p.isDead() && key.equalsIgnoreCase(DeadChat.KEY)){

            }else
                throw new ChatException("Player cannot talk in this chat. [" + key + "]");
        }

        return new ChatMessage(p, message, key, source, args);
    }

    private boolean teamChatMessage(Player p, String key) {
        GameFaction t = getFaction(key);
        if(t == null)
            return false;
        if(Mason.IsMasonType(p))
            return true;
        if(Disguiser.hasDisguised(p) && Mason.IsMasonType(Disguiser.getDisguised(p)))
            return true;
        if(!t.hasNightChat())
            return false;
        return t.getMembers().contains(p);
    }

    private boolean jailorToCaptiveMessage(Player jailor, String key) {
        if(!jailor.is(JailCreate.abilityType))
            return false;
        JailCreate jailorCard = jailor.getAbility(JailCreate.class);
        for(Player p: jailorCard.getJailedTargets()){
            if(key.equals(JailCreate.KeyCreator(p, dayNumber)))
                return true;
        }
        return false;
    }

    public void addDayLynches(int lynches) {
        this._lynches += lynches;
    }

    public PlayerList getTodaysLynchedTargets() {
        PlayerList todaysLynched = new PlayerList();
        for(Player p: players){
            if(p.isDead() && p.getDeathDay() == dayNumber && p.getDeathType().isLynch())
                todaysLynched.add(p);
        }
        if(todaysLynched.isEmpty() && getLiveSize() > 2)
            todaysLynched.add(skipper);
        return todaysLynched;
    }

    public static final boolean DAY_ENDING = true;
    public static final boolean DAY_NOT_ENDING = false;

    /**
     * Helper method that auto starts the night. Announces who got lynched.
     *
     * @param lynchedTargets the people who were lynched
     */
    public int _lynches = 0; // rename to remainingVoteRounds

    public void endDay(GameFaction parity) {
        masonLeaderPromotion();
        if(phase == GamePhase.NIGHT_ACTION_SUBMISSION)
            throw new IllegalStateException("It was already nighttime");

        PlayerList lynchedTargets = getTodaysLynchedTargets();

        EventList el = new EventList();

        DeathAnnouncement e;
        PlayerList poisoned = new PlayerList();
        for(Player p: players){
            // happens before the setting dead of players because of vent onDayEnd
            p.onDayEnd();
        }

        for(Player p: players.getLivePlayers()){
            if(!p.isPoisoned())
                continue;
            if(!p.in(lynchedTargets)){
                e = new DeathAnnouncement(p);
                e.setDayEndDeath();
                e.add(p, Poisoner.BROADCAST_MESSAGE);
                e.setPicture("poisoner");
                el.add(e);
                poisoned.add(p);
                p.dayKill(new DirectAttack(p.getPoisoner(), p, Constants.POISON_KILL_FLAG), 0, DAY_ENDING);
            }
            p.sendMessage(new Feedback(p, Poisoner.DEATH_FEEDBACK));

        }

        for(Message m: voteSystem.publicizeVoteEnding(parity))
            el.add(m);

        isTransitioningToNight = true; // so far only used for marshall quick reveals
        for(Message x: el)
            announcement((Announcement) x);

        isTransitioningToNight = false;

        // cultCheck();
        checkProgress();
        if(isInProgress()){
            // jail targets for now
            resolveDayTargets();
            startNight(lynchedTargets.remove(skipper), poisoned, el);
        }

    }

    public boolean isTransitioningToNight = false;

    private void resolveDayTargets() {
        // remove fake bread first
        for(Player p: players)
            p.resolveFakeBreadActions();

        for(Action a: actionStack){
            a.dayHappening();
        }

        Collections.sort(actionStack, ActionUtil.EndDayResolveSorter);

        PlayerList immuneJailors = new PlayerList();
        Action a;
        GameAbility dayAbility;
        for(int i = 0; i < actionStack.size(); i++){
            a = actionStack.get(i);
            if(a.is(Vote.abilityType))
                continue;

            dayAbility = a.owner.getAbility(a.abilityType);
            if(a.owner.isDead() || a.getTargets().hasDead()){
                a.owner.getActions().remove(a);// .forceCancelDayAction(a);
                actionStack.remove(a);
                i--;
                continue;
            }else if(a.getTarget().in(immuneJailors)){
                a.owner.getActions().remove(a);// .forceCancelDayAction(a);
                actionStack.remove(a);
                i--;
                continue;
            }else if(a.owner.isJailed()){
                a.owner.getActions().remove(a);// .forceCancelDayAction(a);
                actionStack.remove(a);
                i--;
                continue;
            }else if(a.getTarget().isJailed()){
                a.owner.getActions().remove(a);// .forceCancelDayAction(a);
                actionStack.remove(a);
                i--;
                continue;
            }

            boolean broken = false;
            for(Player p: a.getTargets()){
                if(p.inArchChat()){
                    broken = true;
                    break;
                }
            }
            if(broken){
                a.owner.getActions().remove(a);// .forceCancelDayAction(a);
                actionStack.remove(a);
                i--;
                continue;
            }

            if(!a.owner.in(immuneJailors) && a.owner.is(JailCreate.abilityType))
                immuneJailors.add(a.owner);
            dayAbility.resolveDayAction(a);
        }
    }

    public List<NarratorListener> listeners = new ArrayList<>();

    /**
     * Adds objects as listeners to the narrator. For example, when a vote happens
     *
     * @param nL the listener that wants to respond to narrator happenings
     */
    public Game addListener(NarratorListener nL) {
        if(listeners.contains(nL))
            throw new NarratorException("Cannot have the same listener listening twice.");
        listeners.add(nL);
        return this;
    }

    /**
     * Adds object as a command listener. Whenever someone inputs a command to the
     * game, this fires.
     *
     * @param cL the listener that wants to respond to narrator inputs
     */
    public void addListener(CommandListener cL) {
        synchronized (commandLock){
            cListeners.add(cL);
        }
    }

    /**
     * Removes the object that was listening to the narrator
     *
     * @param nL the object to be removed
     */
    public void removeListener(NarratorListener nL) {
        listeners.remove(nL);
    }

    public void clearListeners() {
        listeners.clear();
    }

    /**
     * Removes the object that was listening to narrator inputs
     *
     * @param cL the object to be removed
     */
    public void removeListener(CommandListener cL) {
        synchronized (commandLock){
            cListeners.remove(cL);
        }
    }

    /**
     * Returns all the objects that are listening to this Narrator
     *
     * @return the list of listening narrator objects
     */
    public List<NarratorListener> getListeners() {
        return new LinkedList<>(listeners);
    }

    /**
     * Determines whether the game has ended or not. Returns false if the game
     * hasn't even started
     *
     * @return whether the game is still going or not
     */
    public boolean isInProgress() {
        synchronized (progSync){
            if(!isStarted)
                return false;

            return win == null;
        }
    }

    private Object progSync = new Object();

    /**
     * Does the checks on whether the game is in progress or not. These checks
     * include if its day/night and there are no opposing teams, or if there are not
     * live players anymore. Once the game is over, winString is set, so
     * isInProgress knows the game is not in progres anymore
     *
     */
    public synchronized void checkProgress() {
        synchronized (progSync){

            for(GameFaction gameFaction: _factions.values()){
                if(!gameFaction.isAlive())
                    continue;

                for(GameFaction enemyTeam: gameFaction.getEnemies()){
                    if(enemyTeam == gameFaction && gameFaction.size() == 1)
                        continue;
                    if(enemyTeam.isAlive()){
                        if(phase != GamePhase.NIGHT_ACTION_SUBMISSION || getLivePlayers().size() >= 3)
                            return;
                    }
                }
            }
            if(isDay()){
                events.initializeNightLogs();
            }
            events.getNightLog(VoidChat.KEY).setInactive();
            events.getDeadChat().setInactive();

            events.getDayChat().setActive();
            determineWinners();

            phase = GamePhase.FINISHED;

            for(Player p: getLivePlayers())
                p.onDayStart();

            for(Player p: players)
                p.addChat(events.dayChat);

            for(NarratorListener listener: listeners){
                try{
                    listener.onGameEnd();
                }catch(Exception e){
                    Util.log(e, "onGameEndError");
                }
            }

        }// end block for synchronized
    }

    /**
     * Helper method to determine the winners of the game.
     *
     */
    private void determineWinners() {
        PlayerList winningPlayers = new PlayerList();

        for(Player p: players.copy().winDetermingSort()){
            p.determineWin();
            p.getActions().clear();
            if(p.isWinner()){
                winningPlayers.add(p);
            }
        }

        win = winMessage(winningPlayers, this);
    }

    private Message win;

    /**
     * This method returns null if the game isn't over yet. If it is, it'll return
     * the message of who won, or "Nobody won! If no one won."
     *
     * @return the Event message listing who won the game.
     */
    public Message getWinMessage() {
        if(win == null)
            checkProgress();
        return win;
    }

    /**
     * Event String helper method that puts together who the winners are in the game
     *
     * @param winners the winners of the game
     * @return the Event message
     */
    public static Message winMessage(PlayerList winners, Game n) {

        n.win = new OGIMessage(n);

        if(winners.size() == 0){
            Message e = n.win = new Announcement(n);
            e.add(Constants.NO_WINNER);
            return e;
        }
        winners.sortByTeam();
        Message e = n.win = new Announcement(n);
        for(int i = 0; i < winners.size() - 1; i++){
            e.add(winners.get(i));
            e.add(", ");
        }

        if(winners.size() != 1)
            e.add("and ");

        e.add(winners.get(winners.size() - 1));
        if(winners.size() > 1){
            e.add(" have ");
        }else
            e.add(" has ");
        e.add("won!");

        return e;
    }

    public void changeRole(Player player, FactionRole factionRole, double timeLeft) {
        GameFaction gameFaction = getFaction(player.getColor());
        gameFaction.removeMember(player);

        player.changeRole(new PendingRole(factionRole));
        RoleChangeService.resolveRoleChange(player);
        gameFaction = getFaction(factionRole.getColor());
        gameFaction.addMember(player);

        /*
         * command player name color role name roles
         */
        String[] commandParts = new String[4];
        commandParts[0] = CommandHandler.MOD_CHANGE_ROLE;
        commandParts[1] = player.getName();
        commandParts[2] = factionRole.getColor();
        commandParts[3] = factionRole.role.getName();

        SelectionMessage e = new SelectionMessage(skipper, false);

        events.addCommand(skipper, timeLeft, commandParts);
        e.add(player.getID(), " has changed into a ", new HTString(factionRole), ".");
    }

    /**
     * Kills a player, out of game style
     *
     * @param players
     */
    public Message modkill(PlayerList players, double timeLeft) {
        // dayTime modkill
        DeathAnnouncement da = new DeathAnnouncement(players, this, phase != GamePhase.NIGHT_ACTION_SUBMISSION);
        String[] command = new String[players.size() + 1];
        command[0] = CommandHandler.MODKILL;
        for(int i = 0; i < players.size(); i++)
            command[i + 1] = players.get(i).getName();
        getEventManager().addCommand(timeLeft, command);
        da.add(players, " was modkilled.");
        da.setPicture("electromaniac");

        if(phase == GamePhase.NIGHT_ACTION_SUBMISSION){
            for(Player player: players)
                player.modKillHelper();
            if(everyoneCompletedNightActions())
                endNight();
        }else{
            this.voteSystem.removeVotersOf(players);
            for(Player player: players)
                player.modKillHelper();
            checkProgress();
            if(this.isInProgress())
                checkVote(timeLeft);
        }
        announcement(da);

        for(NarratorListener listener: listeners)
            listener.onModKill(players);

        return da;
    }

    /**
     * Returns whether the game started or not
     *
     * @return whether the game is going
     */
    public boolean isStarted() {
        return isStarted;
    }

    public boolean isFinished() {
        return this.phase == GamePhase.FINISHED;
    }

    public boolean firstPhaseStarted() {
        return firstPhaseStarted;
    }

    public ArrayList<Action> actionStack;

    public void addActionStack(Action a) {
        if(!actionStack.contains(a))
            actionStack.add(a);
        Collections.sort(actionStack, ActionUtil.DayVoteSorter);
    }

    public void removeActionStack(Action a) {
        actionStack.remove(a);
    }

    @Override
    public String toString() {
        if(phase == GamePhase.VOTE_PHASE)
            return "Day " + dayNumber;
        return "Night " + dayNumber;
    }

    public void parityCheck() {
        GameFaction parity = parityReached();
        if(parity != null){
            endDay(parity);
        }
    }
}
