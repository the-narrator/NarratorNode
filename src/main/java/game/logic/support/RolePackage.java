package game.logic.support;

import game.logic.Player;
import models.FactionRole;
import models.SetupHidden;

//rename to PlayerSetupHidden !!
public class RolePackage {

    public SetupHidden assignedSetupHidden;
    public FactionRole assignedRole;

    public RolePackage(SetupHidden assignedHidden, FactionRole assignedRole) {
        this.assignedRole = assignedRole;
        this.assignedSetupHidden = assignedHidden;
    }

    public void assign(Player player) {
        player.game.getFaction(assignedRole.getColor()).addMember(player);
        player.setInitialRole(this);
    }

    @Override
    public String toString() {
        if(assignedSetupHidden.hidden.size() == 1)
            return assignedRole.getName();
        return assignedSetupHidden.hidden.getName() + " " + assignedRole.getName();
    }

    @Override
    public boolean equals(Object o) {
        if(o == null)
            return false;
        if(o == this)
            return true;
        if(o.getClass() != this.getClass())
            return false;
        RolePackage other = (RolePackage) o;
        return this.assignedSetupHidden == other.assignedSetupHidden;
    }

    @Override
    public int hashCode() {
        return (this.assignedSetupHidden.hashCode() + "&" + this.assignedRole.hashCode()).hashCode();
    }
}
