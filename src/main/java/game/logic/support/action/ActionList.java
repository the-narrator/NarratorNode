package game.logic.support.action;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Optional;

import game.abilities.BreadAbility;
import game.abilities.Driver;
import game.abilities.ElectroManiac;
import game.abilities.FactionSend;
import game.abilities.GameAbility;
import game.abilities.Ghost;
import game.abilities.GraveDigger;
import game.abilities.Survivor;
import game.abilities.Witch;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.exceptions.ActionException;
import game.logic.exceptions.PlayerTargetingException;
import models.enums.AbilityType;
import util.Util;

public class ActionList implements Iterable<Action> {

    private Player owner;
    private Action sendAction;
    public Action mainAction;
    public HashMap<AbilityType, Action> freeActions;
    public ArrayList<Action> extraActions;

    public ActionList(Player owner) {
        this.owner = owner;
        freeActions = new HashMap<>();
        extraActions = new ArrayList<>();
    }

    // rename to getActionList
    public ArrayList<Action> getActions() {
        ArrayList<Action> actions = new ArrayList<>();
        if(mainAction != null)
            actions.add(mainAction);
        actions.addAll(extraActions);
        actions.addAll(freeActions.values());
        if(sendAction != null)
            actions.add(sendAction);
        return actions;
    }

    public boolean isTargeting(Player target, AbilityType abilityType) {
        return isTargeting(Player.list(target), abilityType);
    }

    public boolean isTargeting(PlayerList targets, AbilityType abilityType) {
        for(Action a: getActions()){
            if(a.isTargeting(targets, abilityType)){
                return true;
            }
        }
        return false;
    }

    public boolean canAddAnotherAction(Action a) {
        GameAbility gameAbility = a.getAbility();
        if(gameAbility == null)
            return false;
        if(!gameAbility.canAddAnotherAction(a))
            return false;
        if(a.is(FactionSend.abilityType))
            return true;
        if(gameAbility.canSubmitFree()){
            if(this.freeActions.containsKey(a.abilityType))
                return BreadAbility.getUseableBread(a.owner) > extraActions.size();
            return true;
        }
        if(mainAction == null)
            return true;
        return BreadAbility.getUseableBread(a.owner) > extraActions.size();
    }

    public boolean canAddAnotherAction(AbilityType abilityType, Player p) {
        if(owner.getAbility(abilityType) == null)
            return false;
        Optional<Action> witchAction = owner.getAbility(abilityType).getExampleWitchAction(owner, p);
        if(witchAction.isPresent())
            return canAddAnotherAction(witchAction.get());
        return false;
    }

    public boolean canAddAnotherAction(AbilityType abilityType) {
        Optional<Action> exampleAction = owner.getAbility(abilityType).getExampleAction(owner);
        if(exampleAction.isPresent())
            return canAddAnotherAction(exampleAction.get());
        return false;
    }

    public ArrayList<Action> getActions(AbilityType abilityType) {
        ArrayList<Action> actions = new ArrayList<>();
        for(Action a: getActions()){
            if(a.is(abilityType))
                actions.add(a);
        }
        return actions;
    }

    private boolean isSend(Action a) {
        if(a.abilityType != FactionSend.abilityType)
            return false;
        if(sendAction == null)
            sendAction = a;
        else{
            Action originalSend = this.sendAction;
            originalSend.setTargets(PlayerList.GetUnique(a.getTargets().add(originalSend.getTargets())));
        }
        // reorder targets here
        return true;
    }

    public Action addAction(Action newAction) {
        if(isSend(newAction))
            return sendAction;

        int maxActionTypeCount = newAction.getAbility().maxNumberOfSubmissions();

        Action oldAction = null;
        GameAbility ability = newAction.getAbility();

        if(maxActionTypeCount == 1 && this.getActions(newAction.abilityType).size() != 0){
            if(mainAction != null && mainAction.is(newAction.abilityType)){
                oldAction = mainAction;
                mainAction = newAction;
                return newAction;
            }else if(newAction.getAbility().canSubmitFree() && freeActions.containsKey(newAction.abilityType)){
                oldAction = freeActions.get(newAction.abilityType);
                freeActions.put(newAction.abilityType, newAction);
                return newAction;
            }else{
                throw new Error();
            }
        }else if(ability.canSubmitWithoutBread(newAction)){
            if(ability.canSubmitFree() && !freeActions.containsKey(newAction.abilityType)){
                freeActions.put(newAction.abilityType, newAction);
                return newAction;
            }else if(mainAction == null
                    && (!ability.canSubmitFree() || !freeActions.containsKey(newAction.abilityType))){
                return mainAction = newAction;
            }
        }

        if(newAction.is(ElectroManiac.abilityType) && newAction._targets.size() == 2){
            for(Action eAction: getActions(ElectroManiac.abilityType)){
                replace(eAction, newAction);
                throw new ActionException(eAction, newAction);
            }
        }

        // this doesn't care about whether there are enough charges or not, and it
        // shouldn't
        int useableBreadCount = BreadAbility.getUseableBread(owner);
        if(useableBreadCount > extraActions.size()){
            extraActions.add(newAction);
            return newAction;
        }

        // have to push something off

        if(useableBreadCount == extraActions.size() && !extraActions.isEmpty()){
            oldAction = extraActions.remove(extraActions.size() - 1);
            extraActions.add(newAction);
        }else if(ability.canSubmitFree()){
            oldAction = addFreeAction(newAction);
        }else{
            oldAction = mainAction;
            mainAction = newAction;
        }

        // if i can't submit without bread, and i don't have bread, i shouldn't get to
        // this point ever. its an unacceptable action
        if(owner.isAlive() || owner.is(Ghost.abilityType))
            throw new ActionException(oldAction, newAction);

        return newAction;
    }

    /*
     * what is this used for?
     *
     * doctors and bgs to know who they're targeting witch, to know who the previous
     * targets were
     *
     */
    public PlayerList getTargets(AbilityType abilityType) {
        PlayerList list = new PlayerList();
        for(Action a: getActions()){
            if(abilityType == a.abilityType && a.isDoubleTargeter()
                    && (owner.is(Witch.abilityType) || owner.is(Driver.abilityType))){

                if(owner.is(Driver.abilityType) || owner.is(Witch.abilityType)){
                    list.add(a._targets);
                }else if(owner.is(GraveDigger.abilityType)){

                }
            }else if(a.isSameAbility(abilityType)){
                list.add(a.getTargets());
            }
        }
        return list;
    }

    public Action remove(PlayerList target, AbilityType abilityType, double timeLeft, boolean setCommand) {
        Action toRemove = null;
        ArrayList<Action> actions = getActions();
        for(Action a: actions){
            if(a.has(target, abilityType)){
                toRemove = a;
                break;
            }
        }
        if(toRemove != null){
            toRemove.cancel(actions.indexOf(toRemove), timeLeft, setCommand);
            remove(toRemove);
        }
        return toRemove;
    }

    public Action cancelActionID(int actionPosition, double timeLeft, boolean setCommand) {
        ArrayList<Action> actions = getActions();
        if(actionPosition >= actions.size() || actionPosition < 0)
            throw new PlayerTargetingException("Action not found");

        Action toCancel = actions.get(actionPosition);
        remove(toCancel);
        toCancel.cancel(actionPosition, timeLeft, setCommand);
        if(owner.game.isDay())
            owner.game.actionStack.remove(toCancel);
        return toCancel;
    }

    public Action getFirst() {
        if(mainAction != null)
            return mainAction;
        if(!freeActions.isEmpty())
            return freeActions.values().iterator().next();
        if(sendAction != null)
            return sendAction;
        return null;
    }

    public boolean isEmpty() {
        return mainAction == null && sendAction == null && extraActions.isEmpty() && freeActions.isEmpty();
    }

    public boolean visitedHome() {
        return visited(owner);
    }

    public boolean visited(Player target) {
        for(Action a: getActions()){
            if(a.visited(target))
                return true;
        }
        return false;
    }

    public void cleanup() {
        for(Action a: getActions()){
            a.cleanup();
        }
    }

    public boolean isVesting() {
        for(Action a: getActions()){
            if(a.is(Survivor.abilityType))
                return true;
        }
        return false;
    }

    @Override
    public boolean equals(Object o) {
        if(o == null)
            return false;
        if(o.getClass() != getClass())
            return false;
        ActionList al = (ActionList) o;

        if(!al.owner.getName().equals(owner.getName()))
            return false;
        if(Util.notEqual(mainAction, al.mainAction))
            return false;
        if(Util.notEqual(sendAction, al.sendAction))
            return false;
        if(Util.notEqual(freeActions, al.freeActions))
            return false;
        if(Util.notEqual(extraActions, al.extraActions))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return getActions().toString();
    }

    /*
     * public void sortActions() { Collections.sort(actions, new
     * Comparator<Action>() { public int compare(Action a1, Action a2){
     * if(a1.ability == Ability.NIGHT_SEND || a2.ability == Ability.NIGHT_SEND){
     * if(a1.ability == Ability.NIGHT_SEND){ return 1; } if(a2.ability ==
     * Ability.NIGHT_SEND) return -1; }
     *
     * if(a1.ability == Ability.BREAD_ABILITY || a2.ability ==
     * Ability.BREAD_ABILITY){ if(a1.ability == Ability.BREAD_ABILITY){ return 1; }
     * if(a2.ability == Ability.BREAD_ABILITY) return -1; }
     *
     * return 0; } }); }
     */

    public int size() {
        return getActions().size();
    }

    public void remove(Action a) {
        if(sendAction == a){
            sendAction = null;
            return;
        }
        if(extraActions.contains(a)){
            extraActions.remove(a);
            return;
        }
        if(freeActions.containsKey(a.abilityType)){
            freeActions.remove(a.abilityType);
            Action toMove = null;
            for(Action extra: extraActions){
                if(extra.abilityType == a.abilityType){
                    toMove = extra;
                    break;
                }
            }
            if(toMove != null){
                extraActions.remove(toMove);
                freeActions.put(toMove.abilityType, toMove);
            }
        }else if(mainAction == a){
            mainAction = null;
            Action toMove = null;
            for(Action extra: extraActions){
                if(!extra.getAbility().canSubmitFree()){
                    toMove = extra;
                    break;
                }
            }
            mainAction = toMove;
            if(mainAction != null)
                extraActions.remove(mainAction);
        }
    }

    // was private. making public for adding vote
    public Action addFreeAction(Action a) {
        return freeActions.put(a.abilityType, a);
    }

    public void replace(Action oldAction, Action newAction) {
        owner.targetingPreconditionChecks(newAction);

        GameAbility oldAbility = oldAction.getAbility();
        GameAbility newAbility = newAction.getAbility();

        oldAbility.onReplaceCancel(oldAction);
        newAbility.onReplaceAdd(newAction);

        if(newAction.isTeamAbility() && (sendAction == null || !sendAction.isTargeting(owner)))
            addAction(new Action(owner, FactionSend.abilityType, owner));

        newAbility.pushReplaceMessage(oldAction, newAction);
    }

    public void clear() {
        mainAction = sendAction = null;
        freeActions.clear();
        extraActions.clear();
    }

    @Override
    public Iterator<Action> iterator() {
        return getActions().iterator();
    }
}
