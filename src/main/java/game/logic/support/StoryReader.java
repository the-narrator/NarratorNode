package game.logic.support;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.Set;

import models.Command;
import models.enums.GameModifierName;
import models.idtypes.PlayerDBID;
import models.modifiers.Modifier;
import models.schemas.GameOverviewSchema;
import models.schemas.PlayerSchema;

public interface StoryReader {

    GameOverviewSchema getGameOverview();

    Set<Long> getModeratorIDs();

    Collection<Modifier<GameModifierName>> getGameModifiers();

    Map<Long, Long> getSetupHiddenSpawns();

    Set<PlayerSchema> getPlayers();

    Map<PlayerDBID, Long> getPlayerRoles();

    ArrayList<Command> getCommands();

}
