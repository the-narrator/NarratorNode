package game.logic.support;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import game.logic.Player;
import game.logic.PlayerList;

public class Random {

    private static final boolean logging = false;
    public ArrayList<String> log;
    private java.util.Random r;

    public Random() {
        r = new java.util.Random();
        if(logging)
            log = new ArrayList<>();
    }

    private Long seed;

    public Random setSeed(long seed) {
        r = new java.util.Random();
        this.seed = seed;
        r.setSeed(seed);
        log("setting seed : " + seed);
        return this;
    }

    private void log(String string) {
        if(log != null)
            log.add(string);
    }

    public void shuffle(List<?> list, String reason) {
        Collections.shuffle(list, r);
        if(reason != null)
            log("shuffling list " + reason);
        else
            log("shuffling list");
    }

    public long nextLong() {
        log("getting long");
        return r.nextLong();
    }

    public int nextInt() {
        int i = r.nextInt();
        log("getting int: " + i);
        return i;
    }

    public int nextInt(int bound) {
        int i;
        if(bound == 0)
            i = 0;
        else
            i = r.nextInt(bound);
        log("getting int : " + i);
        return i;
    }

    public double nextDouble() {
        double i = r.nextDouble();
        log("getting double : " + i);
        return i;
    }

    public String getLog() {
        StringBuilder sb = new StringBuilder();
        for(String s: log)
            sb.append(s + "\n");
        return sb.toString();
    }

    public boolean nextBoolean() {
        boolean b = r.nextBoolean();
        log("getting boolean : " + b);
        return b;
    }

    PlayerList toPick;

    public Player getPlayer(PlayerList players) {
        if(toPick != null && !toPick.isEmpty() && toPick.getFirst().in(players)){
            return toPick.remove(0);
        }
        if(players.isEmpty())
            return null;
        if(players.size() == 1)
            return players.get(0);
        return players.get(this.nextInt(players.size()));
    }

    public Player getPlayer(Set<Player> players) {
        return getPlayer(new PlayerList(players).sortByID());
    }

    public void queuePlayer(Player p) {
        if(toPick == null)
            toPick = new PlayerList();
        toPick.add(p);
    }

    public void reset() {
        setSeed(seed);
        if(log != null)
            log.clear();
    }

    public int nextGaussian() {
        double i = r.nextGaussian();
        log("getting gaussian : " + i);
        return (int) i;
    }
}
