package game.logic.support;

import game.logic.GameFaction;

public class Option {

    public String value, name, color;

    public Option(String value) {
        this.value = value;
    }

    public Option(String value, String name) {
        this.value = value;
        this.name = name;
    }

    public Option(GameFaction t) {
        this(t.getColor(), t.getName());
        this.color = t.getColor();
    }

    public String getName() {
        if(name != null)
            return name;
        return value;
    }

    public Option setColor(String color) {
        this.color = color;
        return this;
    }

    public String getValue() {
        return value;
    }

    public String getColor() {
        return color;
    }

    public int hashCode() {
        return value.hashCode();
    }

    public boolean equals(Object o) {
        if(o == null)
            return false;
        if(!(o instanceof Option))
            return false;

        return ((Option) o).value.equals(this.value);
    }

    public String toString() {
        return getName();
    }
}
