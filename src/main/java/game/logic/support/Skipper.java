package game.logic.support;

import game.logic.Game;
import game.logic.Player;
import game.logic.support.action.Action;

public class Skipper extends Player {

    public Skipper(Game n) {
        super(Constants.SKIP_VOTE_TITLE, n);
    }

    @Override
    public void doDayAction(Action action) {
        throw new Error();
    }

}
