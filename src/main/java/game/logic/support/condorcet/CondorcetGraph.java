package game.logic.support.condorcet;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class CondorcetGraph<T> {

    private HashMap<T, CondorcetNode<T>> acceptedGraph;

    public CondorcetGraph(List<Pairing<T>> iter) {
        acceptedGraph = new HashMap<>();
        for(Pairing<T> ranking: iter){
            addEdge(ranking);
            if(!acceptableGraph(get(ranking.winner), get(ranking.loser)))
                flipEdge(ranking);
        }
    }

    private void addEdge(Pairing<T> ranking) {
        if(!acceptedGraph.containsKey(ranking.winner))
            acceptedGraph.put(ranking.winner, new CondorcetNode<T>(ranking.winner));
        if(!acceptedGraph.containsKey(ranking.loser))
            acceptedGraph.put(ranking.loser, new CondorcetNode<T>(ranking.loser));

        CondorcetNode<T> winner = get(ranking.winner);
        CondorcetNode<T> loser = get(ranking.loser);

        if(!ranking.isTie()){
            winner.addLoser(loser);
            loser.addWinner(winner);
            return;
        }

        if(!winner.isSame(loser))
            return;

        if(winner.getPlayers().size() < loser.getPlayers().size()){
            winner = get(ranking.loser);
            loser = get(ranking.winner);
        }
        for(T p: winner.getPlayers())
            acceptedGraph.remove(p);

        for(T p: loser.getPlayers())
            acceptedGraph.remove(p);

        CondorcetNode<T> mash = winner.mash(loser);
        for(T p: mash.getPlayers())
            acceptedGraph.put(p, mash);
    }

    public CondorcetNode<T> get(T p) {
        return acceptedGraph.get(p);
    }

    private void flipEdge(Pairing<T> ranking) {
        CondorcetNode<T> winner = get(ranking.winner);
        CondorcetNode<T> loser = get(ranking.loser);
        winner.removeLoser(loser);
        loser.removeWinner(winner);

        winner.addWinner(loser);
        loser.addLoser(winner);
    }

    private boolean acceptableGraph(CondorcetNode<T> winner, CondorcetNode<T> loser) {
        if(loser.losers == null)
            return true;
        for(CondorcetNode<T> loser_loser: loser.losers){
            if(loser_loser == winner)
                return false;
            if(!acceptableGraph(winner, loser_loser))
                return false;
        }
        return true;
    }

    public List<Set<T>> getOrderings() {
        ArrayList<Set<T>> ps = new ArrayList<>();
        Set<CondorcetNode<T>> loserSet;
        Set<T> loserElements;

        Set<CondorcetNode<T>> cNodes = new HashSet<>();
        for(CondorcetNode<T> cNode: acceptedGraph.values())
            if(!cNodes.contains(cNode))
                cNodes.add(cNode);
        while (!cNodes.isEmpty()){
            loserSet = new HashSet<>();
            for(CondorcetNode<T> cNode: cNodes){
                if(!cNode.hasLosers())
                    loserSet.add(cNode);
            }

            loserElements = new HashSet<>();
            for(CondorcetNode<T> loser: loserSet)
                loserElements.addAll(loser.getPlayers());

            ps.add(0, loserElements);

            for(CondorcetNode<T> loser: loserSet)
                cNodes.remove(loser);

            for(CondorcetNode<T> loser: loserSet)
                for(CondorcetNode<T> cNode: cNodes)
                    cNode.removeLoser(loser);
        }

        return ps;
    }

}
