package game.logic.support;

import game.event.Announcement;
import game.logic.Player;

public interface DeathDescriber {

    void populateDeath(Player dead, Announcement e);

    boolean hasDeath(Player dead);

    void cleanup();

    void setGenre(String genre);

}
