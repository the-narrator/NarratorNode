package game.logic.support;

import java.util.ArrayList;
import java.util.HashMap;

import game.event.Message;
import game.logic.Player;
import game.logic.PlayerList;

public class StringChoice {

    private Object def;
    private HashMap<String, Object> idToString = new HashMap<>();
    private Player player;

    public StringChoice(Object def) {
        if(def instanceof Player){
            player = (Player) def;
            def = new Message.PlayerWrapper((Player) def);
        }
        this.def = def;
    }

    public Object getString(String level) {
        Object ret = idToString.get(level);
        if(ret == null)
            ret = def;
        return ret;
    }

    public StringChoice add(String level, String word) {
        idToString.put(level, word);
        return this;
    }

    public StringChoice add(Player p, String word) {
        return add(p.getName(), word);
    }

    public StringChoice add(String level, HTString ht) {
        idToString.put(level, ht);
        return this;
    }

    @Override
    public String toString() {
        return def.toString();
    }

    public static StringChoice YouYourselfSingle(Player target) {
        return YouYourselfSingle("yourself", target);
    }

    public static StringChoice YouYourselfSingle(String youText, Player target) {
        if(target != target.getSkipper()){
            StringChoice youYourself = new StringChoice(target);
            youYourself.add(target, youText);
            return youYourself;
        }
        return new StringChoice("eliminating no one");
    }

    public static ArrayList<Object> YouYourself(PlayerList targets) {
        return YouYourself("yourself", targets);
    }

    public static ArrayList<Object> YouYourself(String youText, PlayerList targets) {
        ArrayList<Object> list = new ArrayList<>();
        if(targets.size() == 1){
            list.add(YouYourselfSingle(youText, targets.getFirst()));
        }else if(targets.size() == 2){
            list.add(YouYourselfSingle(youText, targets.get(0)));
            list.add(" and ");
            list.add(YouYourselfSingle(youText, targets.get(1)));
        }else{
            targets = targets.copy();
            Player postAnd = targets.remove(targets.size() - 1);
            for(Player p: targets){
                list.add(YouYourselfSingle(youText, p));
                list.add(", ");
            }
            list.add("and ");
            list.add(YouYourselfSingle(youText, postAnd));
        }
        return list;
    }

    public Player getPlayer() {
        return player;
    }
}
