package game.logic.support.attacks;

import game.logic.Player;
import game.logic.PlayerList;
import game.logic.support.saves.Heal;
import models.enums.GamePhase;

public abstract class Attack {

    public boolean countered = false;
    public boolean sustainedBeforeAttackPhase;

    protected Attack(Player injured) {
        sustainedBeforeAttackPhase = injured.game.nightPhase == GamePhase.PRE_KILLING;
    }

    public abstract boolean isHealable(Heal type);

    public abstract String[] getType();

    public abstract boolean isImmune();

    public boolean isCountered() {
        return countered;
    }

    public void counterAttack(Heal type) {
        type.onHeal(getInjured().getFirst());
        countered = true;
        this.heal = type;
    }

    public abstract PlayerList getInjured();

    public abstract Player getAttacker();

    protected Heal heal;

    public Heal getHeal() {
        return heal;
    }

    public abstract Player getCause();

}
