package game.logic.listeners;

import game.logic.Player;

public interface CommandListener {

    void onCommand(Player player, double timeLeft, String s);

}
