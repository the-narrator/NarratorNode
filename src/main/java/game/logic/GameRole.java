package game.logic;

import java.util.ArrayList;
import java.util.Optional;

import game.abilities.AbilityList;
import game.abilities.GameAbility;
import models.FactionRole;
import models.GameModifiers;
import models.enums.AbilityType;
import models.enums.RoleModifierName;
import models.modifiers.Modifiers;
import util.game.ModifierUtil;

//role creator
public class GameRole {

    public AbilityList _abilities;
    public GameModifiers<RoleModifierName> modifiers;
    public Game game;
    public FactionRole factionRole;

    // every property doesn't directly apply to the role itself except for charges
    // for charges, we'll use the charges name, and the index to indicate which one
    // should have the charges modifier

    public GameRole(Game game, FactionRole factionRole) {
        this.game = game;
        this.factionRole = factionRole;
        this._abilities = new AbilityList(game, factionRole.role.abilityMap, factionRole.abilityModifiers);
        Modifiers<RoleModifierName> roleModifiers = ModifierUtil.mergeModifiers(game.players.size(),
                factionRole.role.modifiers, factionRole.roleModifiers);
        this.modifiers = new GameModifiers<>(game, roleModifiers);
    }

    public GameFaction getFaction() {
        return game.getFaction(factionRole.getColor());
    }

    public final boolean is(GameAbility checkRole) {
        for(GameAbility r: _abilities){
            if(r.getClass().equals(checkRole.getClass()))
                return true;
        }
        return false;
    }

    public void removeAbility(Class<? extends GameAbility> a) {
        ArrayList<GameAbility> list = new ArrayList<>();
        for(GameAbility b: _abilities){
            if(b.getClass() == a)
                continue;
            list.add(b);
        }
        _abilities = new AbilityList(list);
    }

    public GameAbility getAbility(AbilityType abilityClass) {
        for(GameAbility a: this._abilities){
            if(a.getAbilityType() == abilityClass)
                return a;
        }
        return null;
    }

    public boolean hasAbility(AbilityType abilityClass) {
        return getAbility(abilityClass) != null;
    }

    public static ArrayList<String> GetModifierRuleTexts(Modifiers<RoleModifierName> modifiers,
            Optional<Integer> playerCount) {
        ArrayList<String> ruleTextList = new ArrayList<String>();
        if(modifiers.getBoolean(RoleModifierName.UNDETECTABLE, playerCount, false))
            ruleTextList.add("Immune to detection.");
        if(modifiers.getBoolean(RoleModifierName.UNBLOCKABLE, playerCount, false))
            ruleTextList.add("Immune to roleblocks.");
        if(modifiers.getBoolean(RoleModifierName.UNCONVERTABLE, playerCount, false))
            ruleTextList.add("Cannot be converted.");
        int autoVestCount = modifiers.getInt(RoleModifierName.AUTO_VEST, playerCount);
        if(autoVestCount != 0)
            ruleTextList.add("Starts off with " + autoVestCount + " auto vests.");
        return ruleTextList;
    }

    public static void initialize(GameRole newRole, Player player) {
        for(GameAbility ability: newRole._abilities)
            ability.initialize(player);

        GameModifiers<RoleModifierName> modifiers = newRole.modifiers;
        if(modifiers.getOrDefault(RoleModifierName.UNDETECTABLE, false)){
            player.setDetectable(false);
            GameAbility.happening(player, " is immune to detection roles.");
        }
        if(modifiers.getOrDefault(RoleModifierName.UNBLOCKABLE, false)){
            player.setBlockable(false);
            GameAbility.happening(player, " is immune to role blocks");
        }

        int vests = modifiers.getInt(RoleModifierName.AUTO_VEST);
        if(vests != 0){
            player.setAutoVestCount(vests);
            GameAbility.happening(player, " starts off with " + vests + " auto vests");
        }

    }
}
