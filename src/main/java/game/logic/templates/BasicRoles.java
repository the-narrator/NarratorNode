package game.logic.templates;

import java.util.Set;

import game.abilities.Agent;
import game.abilities.Amnesiac;
import game.abilities.Architect;
import game.abilities.Armorsmith;
import game.abilities.ArmsDetector;
import game.abilities.Assassin;
import game.abilities.Baker;
import game.abilities.Blackmailer;
import game.abilities.Blacksmith;
import game.abilities.Block;
import game.abilities.Bodyguard;
import game.abilities.Bomb;
import game.abilities.Bulletproof;
import game.abilities.Burn;
import game.abilities.Clubber;
import game.abilities.Commuter;
import game.abilities.Coroner;
import game.abilities.Coward;
import game.abilities.CultLeader;
import game.abilities.Cultist;
import game.abilities.Detective;
import game.abilities.Disfranchise;
import game.abilities.Disguiser;
import game.abilities.Doctor;
import game.abilities.Douse;
import game.abilities.Driver;
import game.abilities.DrugDealer;
import game.abilities.Elector;
import game.abilities.ElectroManiac;
import game.abilities.Enforcer;
import game.abilities.Executioner;
import game.abilities.Framer;
import game.abilities.Ghost;
import game.abilities.Godfather;
import game.abilities.Goon;
import game.abilities.GraveDigger;
import game.abilities.Gunsmith;
import game.abilities.Interceptor;
import game.abilities.Investigator;
import game.abilities.JailExecute;
import game.abilities.Janitor;
import game.abilities.Jester;
import game.abilities.Joker;
import game.abilities.Lookout;
import game.abilities.Marshall;
import game.abilities.Mason;
import game.abilities.MasonLeader;
import game.abilities.MassMurderer;
import game.abilities.Mayor;
import game.abilities.Miller;
import game.abilities.Operator;
import game.abilities.ParityCheck;
import game.abilities.Poisoner;
import game.abilities.SerialKiller;
import game.abilities.Sheriff;
import game.abilities.Silence;
import game.abilities.Sleepwalker;
import game.abilities.Snitch;
import game.abilities.Spy;
import game.abilities.Survivor;
import game.abilities.Tailor;
import game.abilities.Thief;
import game.abilities.Undouse;
import game.abilities.Ventriloquist;
import game.abilities.Veteran;
import game.abilities.Vigilante;
import game.abilities.Witch;
import game.setups.Setup;
import models.Faction;
import models.FactionRole;
import models.enums.AbilityType;
import util.CollectionUtil;

public class BasicRoles {

    public static Setup setup;
    public static final String BUS_DRIVER = "Bus Driver";
    public static final String BULLETPROOF = "BulletProof";
    public static final String CHAUFFEUR = "Chauffeur";
    public static final String KIDNAPPER = "Kidnapper";
    public static final String ESCORT = "Escort";
    public static final String CONSORT = "Consort";
    public static final String ARSONIST = "Arsonist";

    public static FactionRole getMember(String teamColor, AbilityType... requiredAbilities) {
        Faction faction = setup.getFactionByColor(teamColor);
        if(faction == null)
            throw new NullPointerException();
        Set<AbilityType> requiredAbilitySet = CollectionUtil.toSet(requiredAbilities);
        for(FactionRole factionRole: faction.roleMap.values()){
            if(factionRole.role.abilityMap.keySet().equals(requiredAbilitySet))
                return factionRole;
        }
        throw new NullPointerException();
    }

    public static FactionRole Citizen() {
        return getMember(Setup.TOWN_C);
    }

    public static FactionRole Bulletproof() {
        return getMember(Setup.TOWN_C, Bulletproof.abilityType);
    }

    public static FactionRole Miller() {
        return getMember(Setup.TOWN_C, Miller.abilityType);
    }

    public static FactionRole Sleepwalker() {
        return getMember(Setup.TOWN_C, Sleepwalker.abilityType);
    }

    public static FactionRole Bomb() {
        return getMember(Setup.TOWN_C, Bomb.abilityType);
    }

    public static FactionRole Sheriff() {
        return getMember(Setup.TOWN_C, Sheriff.abilityType);
    }

    public static FactionRole ArmsDetector() {
        return getMember(Setup.TOWN_C, ArmsDetector.abilityType);
    }

    public static FactionRole ParityCop() {
        return getMember(Setup.TOWN_C, ParityCheck.abilityType);
    }

    public static FactionRole Detective() {
        return getMember(Setup.TOWN_C, Detective.abilityType);
    }

    public static FactionRole Lookout() {
        return getMember(Setup.TOWN_C, Lookout.abilityType);
    }

    public static FactionRole Spy() {
        return getMember(Setup.TOWN_C, Spy.abilityType);
    }

    public static FactionRole Coroner() {
        return getMember(Setup.TOWN_C, Coroner.abilityType);
    }

    public static FactionRole Doctor() {
        return getMember(Setup.TOWN_C, Doctor.abilityType);
    }

    public static FactionRole Bodyguard() {
        return getMember(Setup.TOWN_C, Bodyguard.abilityType);
    }

    public static FactionRole Armorsmith() {
        return getMember(Setup.TOWN_C, Armorsmith.abilityType);
    }

    public static FactionRole Blacksmith() {
        return getMember(Setup.TOWN_C, Blacksmith.abilityType);
    }

    public static FactionRole EvilBlacksmith() {
        return getMember(Setup.OUTCAST_C, Blacksmith.abilityType);
    }

    public static FactionRole Escort() {
        return getMember(Setup.TOWN_C, Block.abilityType);
    }

    public static FactionRole Stripper(String color) {
        return getMember(color, Block.abilityType);
    }

    public static FactionRole BusDriver() {
        return getMember(Setup.TOWN_C, Driver.abilityType);
    }

    public static FactionRole Operator() {
        return getMember(Setup.TOWN_C, Operator.abilityType);
    }

    public static FactionRole Gunsmith() {
        return getMember(Setup.TOWN_C, Gunsmith.abilityType);
    }

    public static FactionRole Vigilante() {
        return getMember(Setup.TOWN_C, Vigilante.abilityType);
    }

    public static FactionRole Jailor() {
        return getMember(Setup.TOWN_C, JailExecute.abilityType);
    }

    public static FactionRole Architect() {
        return getMember(Setup.TOWN_C, Architect.abilityType);
    }

    public static FactionRole Veteran() {
        return getMember(Setup.TOWN_C, Veteran.abilityType);
    }

    public static FactionRole Baker() {
        return getMember(Setup.TOWN_C, Baker.abilityType);
    }

    public static FactionRole Snitch() {
        return getMember(Setup.TOWN_C, Snitch.abilityType);
    }

    public static FactionRole Mayor() {
        return getMember(Setup.TOWN_C, Mayor.abilityType);
    }

    public static FactionRole Marshall() {
        return getMember(Setup.TOWN_C, Marshall.abilityType);
    }

    public static FactionRole Mason() {
        return getMember(Setup.TOWN_C, Mason.abilityType);
    }

    public static FactionRole MasonLeader() {
        return getMember(Setup.TOWN_C, MasonLeader.abilityType);
    }

    public static FactionRole Clubber() {
        return getMember(Setup.TOWN_C, Clubber.abilityType);
    }

    public static FactionRole Enforcer() {
        return getMember(Setup.TOWN_C, Enforcer.abilityType);
    }

    public static FactionRole Commuter() {
        return getMember(Setup.TOWN_C, Commuter.abilityType);
    }

    public static FactionRole Godfather() {
        return Godfather(Setup.MAFIA_C);
    }

    public static FactionRole Godfather(String color) {
        return getMember(color, Godfather.abilityType, Bulletproof.abilityType);
    }

    public static FactionRole Goon() {
        return Goon(Setup.MAFIA_C);
    }

    public static FactionRole Goon(String color) {
        return getMember(color, Goon.abilityType);
    }

    public static FactionRole Consort() {
        return getMember(Setup.MAFIA_C, Block.abilityType);
    }

    public static FactionRole Blackmailer() {
        return getMember(Setup.MAFIA_C, Blackmailer.abilityType);
    }

    public static FactionRole Silencer() {
        return getMember(Setup.MAFIA_C, Silence.abilityType);
    }

    public static FactionRole Disfranchiser() {
        return getMember(Setup.MAFIA_C, Disfranchise.abilityType);
    }

    public static FactionRole Janitor() {
        return getMember(Setup.MAFIA_C, Janitor.abilityType);
    }

    public static FactionRole Framer() {
        return getMember(Setup.MAFIA_C, Framer.abilityType);
    }

    public static FactionRole Disguiser() {
        return getMember(Setup.MAFIA_C, Disguiser.abilityType);
    }

    public static FactionRole DrugDealer() {
        return getMember(Setup.MAFIA_C, DrugDealer.abilityType);
    }

    public static FactionRole Tailor() {
        return getMember(Setup.MAFIA_C, Tailor.abilityType);
    }

    public static FactionRole Agent() {
        return getMember(Setup.MAFIA_C, Agent.abilityType);
    }

    public static FactionRole Investigator() {
        return getMember(Setup.MAFIA_C, Investigator.abilityType);
    }

    public static FactionRole Chauffeur() {
        return getMember(Setup.MAFIA_C, Driver.abilityType);
    }

    public static FactionRole Kidnapper() {
        return getMember(Setup.MAFIA_C, JailExecute.abilityType);
    }

    public static FactionRole Coward() {
        return getMember(Setup.MAFIA_C, Coward.abilityType);
    }

    public static FactionRole Assassin() {
        return getMember(Setup.MAFIA_C, Assassin.abilityType);
    }

    public static FactionRole Chauffeur2() {
        return getMember(Setup.YAKUZA_C, Driver.abilityType);
    }

    public static FactionRole Jester() {
        return getMember(Setup.BENIGN_C, Jester.abilityType);
    }

    public static FactionRole Ghost() {
        return getMember(Setup.BENIGN_C, Ventriloquist.abilityType, Ghost.abilityType);
    }

    public static FactionRole Executioner() {
        return getMember(Setup.BENIGN_C, Executioner.abilityType);
    }

    public static FactionRole Survivor() {
        return getMember(Setup.BENIGN_C, Survivor.abilityType);
    }

    public static FactionRole Amnesiac() {
        return getMember(Setup.BENIGN_C, Amnesiac.abilityType);
    }

    public static FactionRole CultLeader() {
        return getMember(Setup.CULT_C, CultLeader.abilityType);
    }

    public static FactionRole Cultist() {
        return getMember(Setup.CULT_C, Cultist.abilityType);
    }

    public static FactionRole Witch() {
        return getMember(Setup.OUTCAST_C, Witch.abilityType);
    }

    public static FactionRole GraveDigger() {
        return getMember(Setup.OUTCAST_C, GraveDigger.abilityType);
    }

    public static FactionRole Interceptor() {
        return getMember(Setup.OUTCAST_C, Interceptor.abilityType);
    }

    public static FactionRole Elector() {
        return getMember(Setup.OUTCAST_C, Elector.abilityType);
    }

    public static FactionRole Ventriloquist() {
        return getMember(Setup.OUTCAST_C, Ventriloquist.abilityType);
    }

    public static FactionRole SerialKiller() {
        return getMember(Setup.THREAT_C, SerialKiller.abilityType, Bulletproof.abilityType);
    }

    public static FactionRole Arsonist() {
        return getMember(Setup.THREAT_C, Burn.abilityType, Douse.abilityType, Undouse.abilityType,
                Bulletproof.abilityType);
    }

    public static FactionRole Poisoner() {
        return getMember(Setup.THREAT_C, Poisoner.abilityType, Bulletproof.abilityType);
    }

    public static FactionRole ElectroManiac() {
        return getMember(Setup.THREAT_C, ElectroManiac.abilityType, Bulletproof.abilityType);
    }

    public static FactionRole MassMurderer() {
        return getMember(Setup.THREAT_C, MassMurderer.abilityType, Bulletproof.abilityType);
    }

    public static FactionRole Joker() {
        return getMember(Setup.THREAT_C, Joker.abilityType, Bulletproof.abilityType);
    }

    public static FactionRole Thief() {
        return getMember(Setup.BENIGN_C, Thief.abilityType);
    }

}
