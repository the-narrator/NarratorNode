package game.logic.templates;

import game.ai.Controller;
import game.logic.Game;
import game.setups.Setup;

public class NativeControllerGenerator {

    Game game;

    public NativeControllerGenerator() {
        game = new Game(Setup.NoDB());
    }

    public Controller getActionController(String name) {
        return game.addPlayer(name).setComputer();
    }

    public NativeSetupController getSetupController() {
        return new NativeSetupController(game.setup);
    }

    public Game getNarrator() {
        return game;
    }

    public void reset() {
        game = new Game(Setup.NoDB());
    }

    public boolean cleanup() {
        return false;
    }

}
