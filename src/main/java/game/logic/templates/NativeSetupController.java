package game.logic.templates;

import game.abilities.Hidden;
import game.setups.Setup;
import models.Faction;
import models.Role;
import models.enums.AbilityModifierName;
import models.enums.AbilityType;
import models.enums.RoleModifierName;
import models.enums.SetupModifierName;
import services.FactionAbilityModifierService;
import services.FactionAbilityService;
import services.FactionService;
import services.HiddenService;
import services.RoleAbilityModifierService;
import services.RoleAbilityService;
import services.RoleModifierService;
import services.RoleService;
import services.SetupModifierService;
import util.CollectionUtil;

public class NativeSetupController {

    Setup setup;

    public NativeSetupController(Setup setup) {
        this.setup = setup;
    }

    public void deleteRole(Role role) {
        RoleService.delete(role);
    }

    public void deleteHidden(Hidden hidden) {
        HiddenService.delete(hidden);
    }

    public void setAllies(String t1, String t2) {
        Faction faction = setup.getFactionByColor(t1);
        Faction enemyFaction = setup.getFactionByColor(t2);
        FactionService.removeEnemy(faction, enemyFaction);
    }

    public void setEnemies(String t1, String t2) {
        Faction faction = setup.getFactionByColor(t1);
        Faction enemyFaction = setup.getFactionByColor(t2);
        FactionService.addEnemies(faction, enemyFaction);
    }

    public void addSheriffDetectable(String t1, String t2) {
        Faction sheriffFaction = setup.getFactionByColor(t1);
        Faction detectableFaction = setup.getFactionByColor(t2);
        FactionService.addSheriffCheckable(sheriffFaction, detectableFaction);
    }

    public void removeSheriffDetectable(String t1, String t2) {
        Faction sheriffFaction = setup.getFactionByColor(t1);
        Faction uncheckableFaction = setup.getFactionByColor(t2);
        FactionService.removeDetectableFaction(sheriffFaction, uncheckableFaction);
    }

    public void changeRule(SetupModifierName ruleArr, int i) {
        SetupModifierService.upsertModifier(setup, ruleArr, i);
    }

    public void changeRule(SetupModifierName ruleArr, boolean b) {
        SetupModifierService.upsertModifier(setup, ruleArr, b);
    }

    public void addRoleAbility(Role role, AbilityType abilityType) {
        RoleAbilityService.createRoleAbilities(role, CollectionUtil.toSet(abilityType));
    }

    public void removeRoleAbility(Role role, AbilityType abilityType) {
        RoleAbilityService.removeRoleAbility(role, abilityType);
    }

    public void upsertRoleModifier(Role role, RoleModifierName modifier, int value) {
        RoleModifierService.upsertModifier(role, modifier, value);
    }

    public void upsertRoleModifier(Role role, RoleModifierName modifier, boolean value) {
        RoleModifierService.upsertModifier(role, modifier, value);
    }

    public void upsertRoleAbilityModifier(Role role, AbilityModifierName modifierID, AbilityType abilityType,
            boolean value) {
        RoleAbilityModifierService.upsertModifier(role, abilityType, modifierID, value, 0, Setup.MAX_PLAYER_COUNT);
    }

    public void upsertRoleAbilityModifier(Role role, AbilityModifierName modifierID, AbilityType abilityType,
            int value) {
        RoleAbilityModifierService.upsertModifier(role, abilityType, modifierID, value, 0, Setup.MAX_PLAYER_COUNT);
    }

    public void addTeamAbility(String color, AbilityType abilityType) {
        Faction faction = setup.getFactionByColor(color);
        FactionAbilityService.createFactionAbility(faction, abilityType);
    }

    public void removeTeamAbility(String color, AbilityType abilityType) {
        Faction faction = setup.getFactionByColor(color);
        FactionAbilityService.deleteFactionAbility(faction, abilityType);
    }

    public void upsertFactionAbilityModifier(String color, AbilityModifierName modifier, AbilityType abilityClass,
            Object value) {
        Faction faction = setup.getFactionByColor(color);
        FactionAbilityModifierService.upsertModifier(faction, abilityClass, modifier, value);
    }

}
