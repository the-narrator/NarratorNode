package game.logic.templates;

import java.util.ArrayList;
import java.util.Optional;

import game.abilities.GameAbility;
import game.ai.Controller;
import game.event.ChatMessage;
import game.logic.Player;
import game.logic.support.CommandHandler;
import game.logic.support.Constants;
import game.logic.support.action.Action;
import json.JSONObject;
import models.Role;
import models.enums.AbilityType;
import util.Util;

public class TextController implements Controller {

    protected TextInput texter;
    public String playerID;

    public TextController(TextInput ti, String player_id) {
        this.texter = ti;
        this.playerID = player_id;
    }

    public TextController(TextInput ti) {
        this.texter = ti;
    }

    public TextController() {
        // must set a text input right away
    }

    @Override
    public void log(String string) {

    }

    @Override
    public void endNight(double timeLeft) {
        texter.text(playerID, Constants.END_NIGHT, timeLeft);
    }

    @Override
    public void cancelEndNight(double timeLeft) {
        endNight(timeLeft);
    }

    @Override
    public void cancelAction(int actionIndex, double timeLeft) {
        // I'm pretty sure the actionIndex coming in is 0-based. CommandHandler reads it
        // as 1-based
        texter.text(playerID, Constants.CANCEL + " " + Integer.toString(actionIndex + 1), timeLeft);
    }

    public ChatMessage say(String text, String key) {
        return say(text, key, Optional.empty(), new JSONObject());
    }

    @Override
    public ChatMessage say(String text, String key, Optional<String> source, JSONObject args) {
        String messageInput = Constants.SAY + " " + key + " " + text;
        texter.text(playerID, messageInput, 1);
        return null;
    }

    @Override
    public void doDayAction(Action action) {
        GameAbility ability = action.getAbility();
        ArrayList<String> commandParts = ability.getCommandParts(action);
        String command = Util.SpacedString(commandParts).replace(",", "");
        texter.text(playerID, command, action.timeLeft);
    }

    public interface TextInput {
        void text(String player_id, String message, double timeLeft);
    }

    @Override
    public Controller setName(String string) {
        throw new Error("Unsupported method");
    }

    @Override
    public void setNightTarget(Action action) {
        ArrayList<String> commandParts = action.getAbility().getCommandParts(action);

        texter.text(playerID, action + " " + Util.SpacedString(commandParts), action.timeLeft);
    }

    @Override
    public void setLastWill(String string) {
        texter.text(playerID, Constants.LAST_WILL + " " + string, Constants.ALL_TIME_LEFT);
    }

    @Override
    public void setTeamLastWill(String string) {
        texter.text(playerID, Constants.TEAM_LAST_WILL + " " + string, Constants.ALL_TIME_LEFT);
    }

    @Override
    public void rolePrefer(Role role) {
        texter.text(playerID, CommandHandler.PREFER + " " + role.getName(), Constants.ALL_TIME_LEFT);
    }

    @Override
    public void clearTargets() {
        throw new Error("Unsupported method");
    }

    @Override
    public String getName() {
        throw new Error("Unsupported method");
    }

    @Override
    public boolean isTargeting(AbilityType ability, Controller... targets) {
        throw new Error("Unsupported method");
    }

    @Override
    public Player getPlayer() {
        throw new Error("Unsupported method");
    }

    @Override
    public boolean is(AbilityType classes) {
        throw new Error("Unsupported method");
    }

    @Override
    public String getColor() {
        throw new Error("Unsupported method");
    }

    @Override
    public ArrayList<String> getChatKeys() {
        throw new Error("Unsupported method");
    }

    @Override
    public ArrayList<AbilityType> getCommands() {
        throw new Error("Unsupported method");
    }

    @Override
    public String getRoleName() {
        throw new Error("Unsupported method");
    }

}
