package game.logic;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import game.abilities.GameAbility;
import game.event.Message;
import game.event.OGIMessage;
import game.logic.support.Random;
import models.enums.AbilityType;
import models.idtypes.PlayerDBID;

public class PlayerList implements Collection<Player> {

    ArrayList<Player> list;

    public PlayerList() {
        list = new ArrayList<>();
    }

    public PlayerList(Player... all) {
        this();
        for(Player p: all)
            add(p);
    }

    public PlayerList(Set<Player> set) {
        this();
        for(Player p: set)
            add(p);
    }

    public PlayerList(List<Player> list) {
        this();
        for(Player p: list)
            add(p);
    }

    public PlayerList getLivePlayers() {
        PlayerList live = new PlayerList();
        for(Player p: list){
            if(p.isAlive())
                live.add(p);
        }
        return live;
    }

    public PlayerList getDeadPlayers() {
        PlayerList dead = new PlayerList();
        for(Player p: list){
            if(p.isDead())
                dead.add(p);
        }
        return dead;
    }

    public PlayerList getComputers() {
        PlayerList pl = new PlayerList();

        for(Player c: list){
            if(c.isComputer())
                pl.add(c);
        }

        return pl;
    }

    public boolean hasComputers() {
        for(Player c: list){
            if(c.isComputer())
                return true;
        }
        return false;
    }

    @Override
    public boolean add(Player player) {
        if(player == null)
            throw new NullPointerException();
        list.add(player);
        return true;
    }

    public PlayerList add(Player... ps) {
        for(Player p: ps){
            if(p == null)
                throw new NullPointerException();
            list.add(p);
        }
        return this;
    }

    public PlayerList add(PlayerList toImport) {
        for(Player p: toImport){
            add(p);
        }
        return this;
    }

    public void set(int i, Player p) {
        list.set(i, p);
    }

    @Override
    public void clear() {
        list.clear();
    }

    public boolean contains(Player p) {
        if(p == null)
            return false;
        return list.contains(p);
    }

    public PlayerList shuffle(Random rand, String reason) {
        if(list.isEmpty())
            return this;
        PlayerList pl = copy();
        rand.shuffle(pl.list, reason);
        return pl;
    }

    public Player remove(int i) {
        return list.remove(i);
    }

    @Override
    public int size() {
        return list.size();
    }

    @Override
    public Iterator<Player> iterator() {
        return list.iterator();
    }

    @Override
    public boolean isEmpty() {
        return list.isEmpty();
    }

    public boolean isNonempty() {
        return list.size() > 0;
    }

    public PlayerList remove(Player p) {
        if(p != null)
            list.remove(p);
        return this;
    }

    public PlayerList remove(PlayerList toRemove) {
        for(Player p: toRemove)
            remove(p);

        return this;
    }

    public Player getByID(PlayerDBID id) {
        for(Player p: list){
            if(p.databaseID.equals(id))
                return p;
        }
        return null;
    }

    public Player getByName(String s) {
        for(Player p: list){
            if(p.getName().equalsIgnoreCase(s))
                return p;
        }
        return null;
    }

    public Player getByID(String s) {
        for(Player p: list){
            if(p.getID().equalsIgnoreCase(s))
                return p;
        }
        return null;
    }

    public static PlayerList clone(PlayerList list) {
        PlayerList cList = new PlayerList();
        for(Player p: list){
            cList.add(p);
        }
        return cList;
    }

    public static ArrayList<PlayerList> permutations(PlayerList list) {
        ArrayList<PlayerList> perms = new ArrayList<PlayerList>();
        if(list.size() == 0)
            return perms;
        if(list.size() == 1){
            perms.add(list);
            return perms;
        }
        Player begin = list.remove(0);
        ArrayList<PlayerList> prev = permutations(list);

        for(PlayerList list_: prev)
            for(int i = 0; i <= list_.size(); i++){
                PlayerList perm = clone(list_);
                perm.add(i, begin);
                perms.add(perm);
            }

        return perms;
    }

    public PlayerList add(int i, Player p) {
        if(p == null)
            throw new NullPointerException();
        list.add(i, p);
        return this;
    }

    public Player getRandom(Random rand) {
        return rand.getPlayer(this);
    }

    public void sendMessage(Message e) {
        for(Player p: list)
            p.sendMessage(e);
    }

    public void warn(Message e) {
        for(Player p: list)
            p.warn(e);
    }

    public void warn(Object... o) {
        for(Player p: list){
            p.warn(new OGIMessage().add(o));
        }
    }

    public Player get(int i) {
        return list.get(i);
    }

    public int indexOf(Player p) {
        return list.indexOf(p);
    }

    @Override
    public String toString() {
        StringBuilder s = new StringBuilder("");
        for(Player p: copy()){
            s.append(p + "\n ");
        }
        if(!list.isEmpty()){
            s.deleteCharAt(s.length() - 1);
            s.deleteCharAt(s.length() - 1);
        }
        return s.toString();
    }

    public Player removeLast() {
        return list.remove(list.size() - 1);
    }

    public Player removeFirst() {
        return list.remove(0);
    }

    public PlayerList sortBySubmissionTime() {
        Collections.sort(list, Player.SubmissionTime);
        return this;
    }

    public PlayerList sortByName() {
        Collections.sort(list, Player.NameSort);
        return this;
    }

    public PlayerList winDetermingSort() {
        Collections.sort(list, Player.WinDetermineSort);
        return this;
    }

    public void sortByTeam() {
        Collections.sort(list, Player.TeamSort);
    }

    public PlayerList sortByID() {
        Collections.sort(list, Player.IDSort);
        return this;
    }

    public PlayerList sortByDeath() {
        Collections.sort(list, Player.Deaths);
        return this;
    }

    public String getStringName() {
        String name = "";
        if(list.size() == 0){
            return name;
        }

        if(list.size() == 1){
            return list.get(0).getDescription();
        }

        if(list.size() == 2){
            return list.get(0).getDescription() + " and " + list.get(1).getDescription();
        }

        for(int i = 0; i < list.size(); i++){
            Player p = list.get(i);
            if(i + 2 == list.size()){
                name += p.getName() + ", and ";
            }else if(i + 1 == list.size()){
                name += p.getDescription();
            }else
                name += (p.getDescription() + ", ");
        }
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if(o == null)
            return false;
        if(o == this)
            return true;
        if(o.getClass() != getClass())
            return false;

        PlayerList plist = (PlayerList) o;

        PlayerList p1 = this.copy().sortByName();
        PlayerList p2 = plist.copy().sortByName();

        return p1.list.equals(p2.list);
    }

    public void remove(GameAbility r) {
        for(int i = 0; i < size(); i++){
            if(get(i).hasAbility(r.getAbilityType())){
                remove(i);
                i--;
            }
        }
    }

    public PlayerList copy() {
        PlayerList newList = new PlayerList();
        for(Player p: list)
            newList.add(p);

        return newList;
    }

    public boolean hasName(String s) {
        for(Player p: list){
            if(p.getName().toLowerCase().equals(s.toLowerCase()))
                return true;
        }

        return false;
    }

    public String[] getNamesToStringArray() {
        String[] arrayOfNames = new String[list.size()];

        for(int i = 0; i < list.size(); i++)
            arrayOfNames[i] = list.get(i).getName();

        return arrayOfNames;

    }

    public ArrayList<String> getNamesToStringList() {
        ArrayList<String> arrayOfNames = new ArrayList<String>();

        for(Player p: list)
            arrayOfNames.add(p.getName());

        return arrayOfNames;
    }

    public boolean has(Player a2) {
        if(a2 == null)
            return false;

        for(Player p: list){
            if(p == a2)
                return true;
        }
        return false;
    }

    public PlayerList intersect(PlayerList inter) {
        PlayerList sect = new PlayerList();
        for(Player i: inter){
            if(contains(i))
                sect.add(i);
        }
        return sect;
    }

    public PlayerList compliment(PlayerList inter) {
        PlayerList sect = new PlayerList();
        for(Player p: list){
            if(!inter.contains(p))
                sect.add(p);
        }
        return sect;
    }

    public PlayerList reverse() {
        Collections.reverse(list);
        return this;
    }

    public Player getFirst() {
        if(isEmpty())
            return null;
        return list.get(0);
    }

    public Player getLast() {
        if(isEmpty())
            return null;
        return list.get(list.size() - 1);
    }

    public boolean hasLiving() {
        for(Player p: list){
            if(p.isAlive())
                return true;
        }
        return false;
    }

    public boolean hasDead() {
        for(Player p: list){
            if(p.isDead())
                return true;
        }
        return false;
    }

    public ArrayList<Player> toArrayList() {
        ArrayList<Player> list = new ArrayList<>();

        for(Player p: this.list)
            list.add(p);

        return list;
    }

    @Override
    public Player[] toArray() {
        Player[] list = new Player[size()];

        for(int i = 0; i < size(); i++)
            list[i] = get(i);

        return list;
    }

    public PlayerList softRemove(PlayerList unwanted) {
        if(unwanted == null)
            return this;
        PlayerList inBoth = new PlayerList();
        for(Player p: unwanted){
            if(p.in(this)){
                inBoth.add(p);
            }
            remove(p);
        }
        if(isEmpty())
            add(inBoth);

        return this;
    }

    public PlayerList softIntersect(PlayerList wanted) {
        if(wanted == null)
            return this;
        PlayerList intersection = new PlayerList();
        for(Player p: wanted){
            if(p.in(this) && !p.in(intersection)){
                intersection.add(p);
            }
        }
        if(!intersection.isEmpty()){
            clear();
            add(intersection);
        }
        return this;
    }

    public Player[] getArray() {
        Player[] arr = new Player[list.size()];
        for(int i = 0; i < list.size(); i++){
            arr[i] = list.get(i);
        }
        return arr;
    }

    public PlayerList filter(AbilityType abilityType) {
        PlayerList pl = new PlayerList();

        for(Player p: this){
            if(p.is(abilityType))
                pl.add(p);
        }

        return pl;
    }

    public PlayerList filterWillBe(AbilityType abilityType) {
        PlayerList pl = new PlayerList();

        for(Player p: this){
            if(p.willBe(abilityType))
                pl.add(p);
        }

        return pl;
    }

    public ArrayList<GameFaction> getFactions() {
        ArrayList<GameFaction> teams = new ArrayList<>();

        for(Player p: this){
            if(!teams.contains(p.getGameFaction()))
                teams.add(p.getGameFaction());
        }

        return teams;
    }

    public static PlayerList GetUnique(PlayerList unique) {
        PlayerList pl = new PlayerList();
        for(Player p: unique)
            if(!p.in(pl))
                pl.add(p);
        return pl;
    }

    public void sort(Comparator<Player> comparator) {
        Collections.sort(list, comparator);
    }

    public PlayerList filterUnquarantined() {
        PlayerList pl = new PlayerList();

        for(Player p: this){
            if(!p.isQuarintined())
                pl.add(p);
        }

        return pl;
    }

    public PlayerList subList(int i, Integer i2) {
        return new PlayerList(list.subList(i, i2));
    }

    public PlayerList subList(int i) {
        return new PlayerList(list.subList(i, list.size()));
    }

    public PlayerList filterIsTargeting(Player target, AbilityType abilityType) {
        PlayerList ret = new PlayerList();

        for(Player p: this){
            if(p.getActions().isTargeting(target, abilityType))
                ret.add(p);
        }

        return ret;
    }

    public PlayerList filterSubmittingAction(AbilityType... mainAbility) {
        PlayerList pl = new PlayerList();
        for(Player p: this){
            if(p.isSubmitting(mainAbility))
                pl.add(p);
        }
        return pl;
    }

    public PlayerList filterHiddenDeath() {
        PlayerList pl = new PlayerList();
        for(Player p: this){
            if(p.isAlive())
                continue;
            if(p.getDeathType().isHidden())
                pl.add(p);
        }
        return pl;
    }

    public static boolean SameShuffle(PlayerList l1, PlayerList l2) {
        if(l1.size() != l2.size())
            return false;
        for(Player p: l1)
            if(!l2.contains(p))
                return false;
        return true;
    }

    public Set<Player> toSet() {
        Set<Player> set = new HashSet<>();
        for(Player player: this)
            set.add(player);
        return set;
    }

    public Map<PlayerDBID, Player> getDatabaseMap() {
        Map<PlayerDBID, Player> map = new HashMap<>();
        for(Player player: this.list)
            map.put(player.databaseID, player);
        return map;
    }

    @Override
    public boolean contains(Object o) {
        if(o instanceof Player)
            return this.contains((Player) o);
        return false;
    }

    @Override
    public <T> T[] toArray(T[] a) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public boolean remove(Object o) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean addAll(Collection<? extends Player> c) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        // TODO Auto-generated method stub
        return false;
    }

    /*
     * public static PlayerList FromNames(ArrayList<String> targets, Narrator local)
     * { PlayerList pl = new PlayerList();
     *
     * for(String s: targets){ pl.add(local.getPlayerByName_(s)); }
     *
     * return pl; }
     */

}
