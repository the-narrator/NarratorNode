package game.logic.exceptions;

import java.util.Set;

import models.enums.InvalidSetupHiddensCode;

@SuppressWarnings("serial")
public class InvalidSetupHiddenCombination extends NarratorException {

    Set<InvalidSetupHiddensCode> reasons;

    public InvalidSetupHiddenCombination(Set<InvalidSetupHiddensCode> reasons) {
        super(getReasonStrings(reasons));
        this.reasons = reasons;
    }

    public static String getReasonStrings(Set<InvalidSetupHiddensCode> reasons) {
        StringBuilder sb = new StringBuilder();
        for(InvalidSetupHiddensCode reason: reasons)
            sb.append(reason.toString());
        return sb.toString();
    }

}
