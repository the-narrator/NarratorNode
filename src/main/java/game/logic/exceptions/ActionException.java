package game.logic.exceptions;

import game.logic.support.action.Action;

@SuppressWarnings("serial")
public class ActionException extends Error {

    public Action newAction, oldAction;

    public ActionException(Action oldAction, Action newAction) {
        super();
        if(oldAction == null)
            throw new NullPointerException();
        this.oldAction = oldAction;
        this.newAction = newAction;
    }

}
