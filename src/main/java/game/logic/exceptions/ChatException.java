package game.logic.exceptions;

@SuppressWarnings("serial")
public class ChatException extends NarratorException {

    public ChatException(String key) {
        super("Unknown key " + key);
    }

}
