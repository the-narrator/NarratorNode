package game.logic.exceptions;

@SuppressWarnings("serial")
public class IllegalRoleCombinationException extends NarratorException {
    public IllegalRoleCombinationException(String string) {
        super(string);
    }

}
