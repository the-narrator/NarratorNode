package game.logic.exceptions;

@SuppressWarnings("serial")
public class IllegalGameSettingsException extends NarratorException {

    public IllegalGameSettingsException(String string) {
        super(string);
    }

}
