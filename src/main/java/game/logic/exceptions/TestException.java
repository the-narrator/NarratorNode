package game.logic.exceptions;

@SuppressWarnings("serial")
public class TestException extends Error {

    public TestException() {
        super("");
    }

    public TestException(String id) {
        super(id);
    }

}
