package game.logic.exceptions;

@SuppressWarnings("serial")
public class UnknownPlayerException extends NarratorException {

    public UnknownPlayerException(String s) {
        super(s);
    }

    public UnknownPlayerException() {
        this("");
    }

}
