package game.logic.exceptions;

import game.logic.Player;

@SuppressWarnings("serial")
public class PlayerTargetingException extends NarratorException {

    public PlayerTargetingException(String string) {
        super(string);
    }

    public PlayerTargetingException(Player owner, Player target, int ability) {
        this(owner.getName() + " is trying to target " + target.getName() + " using ability " + ability);
    }

}
