package game.logic.exceptions;

@SuppressWarnings("serial")
public class UnknownRoleException extends NarratorException {

    public String role;

    public UnknownRoleException(String role) {
        super(role);
        this.role = role;
    }

}
