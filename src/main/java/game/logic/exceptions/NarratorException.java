package game.logic.exceptions;

@SuppressWarnings("serial")
public class NarratorException extends Error {

    public NarratorException(String message) {
        super(message);
    }

}
