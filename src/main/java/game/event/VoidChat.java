package game.event;

import game.logic.Game;
import game.logic.Player;
import game.logic.support.Constants;

public class VoidChat extends NightChat {

    public static final String NAME = "Night";
    public static final String KEY = Constants.VOID_CHAT;
    private Game n;

    public VoidChat(Game n) {
        super(n, n.getLivePlayers());
        this.n = n;
    }

    @Override
    public String getName() {
        int dayNumber;
        if(n.isDay() || !n.isInProgress()){
            dayNumber = n.getDayNumber() - 1;
        }else{
            dayNumber = n.getDayNumber();
        }
        return NAME + " " + dayNumber;
    }

    @Override
    public String getKey(Player p) {
        if(p == null)
            return null;
        if(p.isParticipating())
            return Constants.VOID_CHAT;
        return null;
    }

    @Override
    public String toString() {
        return getName();
    }

    @Override
    public String getType() {
        return "fas fa-moon";
    }
}
