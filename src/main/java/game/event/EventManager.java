package game.event;

import java.util.ArrayList;

import game.abilities.Mason;
import game.logic.Game;
import game.logic.GameFaction;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.listeners.CommandListener;
import game.logic.support.Constants;
import game.logic.support.action.Action;
import models.Command;
import models.enums.GamePhase;
import models.enums.SetupModifierName;

public class EventManager {

    public ArrayList<Command> commands;

    public DayChat dayChat;
    public VoidChat voidChat;
    public DeadChat deadChat;
    public ArrayList<FactionChat> factionChats;
    public ArrayList<RoleCreatedChat> tempChats;

    public ArrayList<EventLog> getAllChats() {
        ArrayList<EventLog> list = new ArrayList<>();

        list.add(dayChat);
        list.add(voidChat);
        list.addAll(factionChats);
        list.addAll(tempChats);
        list.add(deadChat);
        return list;
    }

    private Game n;

    public EventManager(Game n) {
        this.n = n;
        deadChat = new DeadChat(n);
        dayChat = new DayChat(n);
        voidChat = new VoidChat(n);
        commands = new ArrayList<>();
        factionChats = new ArrayList<>();
        tempChats = new ArrayList<>();
    }

    // public HashMap<String, ArrayList<Message>> nightParts = new HashMap<>();

    public DayChat getDayChat() {
        return dayChat;
    }

    public synchronized EventList getEvents(Player p) {
        return getEvents(p.getName());
    }

    public EventList getEvents(String... keys) {
        return getEvents(null, keys);
    }

    // this will throw null pointer exceptions if keys length is null
    public synchronized EventList getEvents(Integer dayFilter, String... keys) {
        EventList sb = new EventList();

        if(!n.isStarted())
            sb.add(dayChat.getEvents(0), keys);

        int day = 0;
        if(n.getBool(SetupModifierName.DAY_START) == Game.DAY_START)
            day++;
        for(; day <= n.getDayNumber(); day++){
            if(dayFilter != null && day != dayFilter){

            }else if(n.getBool(SetupModifierName.DAY_START) || day != 0){
                sb.add(new Header(day, GamePhase.VOTE_PHASE, n));
                sb.add(dayChat.getEvents(day), keys);
            }else if(day == 0){
                for(Message e: dayChat.getEvents(0)){
                    if(e instanceof Feedback && e.hasAccess(keys)){
                        sb.add(e);
                    }
                }
            }

            if(dayFilter != null && day + 1 != dayFilter){

            }else if(day < n.getDayNumber() || (n.isInProgress() && n.phase == GamePhase.NIGHT_ACTION_SUBMISSION)){
                sb.add(new Header(day, GamePhase.NIGHT_ACTION_SUBMISSION, n));
                sb.add(accessNightChats(day, keys), keys);
            }
        }

        if(deadChat.hasAccess(keys))
            for(Message m: deadChat.getEvents()){
                sb.add(m);
            }

        if(!n.isInProgress() && n.isStarted() && !sb.events.contains(n.getWinMessage()))
            sb.add(n.getWinMessage());

        return sb;
    }

    public void addCommand(Player p, double timeLeft, String... commands) {
        String command = Message.CommandCreator(commands);
        addCommand(p, timeLeft, command);
    }

    public void addCommand(double timeLeft, String... strings) {
        addCommand(n.skipper, timeLeft, strings);
    }

    public void addCommand(Player player, double timeLeft, String text) {
        Command command = new Command(player, text, timeLeft);
        commands.add(command);
        if(command.text.length() != 0)
            synchronized (n.commandLock){
                for(CommandListener cl: n.getCommandListeners())
                    cl.onCommand(player, timeLeft, text);
            }
    }

    public void deactivateDayChats() {
        dayChat.setInactive();
    }

    public void deactivateNightChats() {
        voidChat.setInactive();
        for(RoleCreatedChat temp: tempChats){
            temp.setInactive();
        }
        for(FactionChat fc: factionChats){
            fc.setInactive();
        }
    }

    public EventList accessNightChats(int dayNumber, String... nameKey) {
        if(nameKey == null || nameKey.length == 0)
            throw new NullPointerException();
        EventList eList = new EventList();

        eList.add(voidChat.getEvents(dayNumber), nameKey);
        for(RoleCreatedChat temp: tempChats){
            eList.add(temp.getEvents(dayNumber), nameKey);
        }

        for(FactionChat fc: factionChats){
            eList.add(fc.getEvents(dayNumber), nameKey);
        }

        return eList;
    }

    public NightChat getNightLog(String key) {
        if(VoidChat.KEY.equalsIgnoreCase(key))
            return voidChat;

        for(RoleCreatedChat temp: tempChats){
            if(key.equalsIgnoreCase(temp.getKey(null)))
                return temp;
        }

        for(FactionChat fc: factionChats){
            if(key.equalsIgnoreCase(fc.getKey(null)))
                return fc;
        }
        return null;
    }

    public ArrayList<Command> getCommands() {
        return commands;
    }

    public EventLog getDeadChat() {
        return deadChat;
    }

    public JailChat createJailChat(Action a) {
        JailChat jc = new JailChat(a.owner, a.getTarget());
        this.tempChats.add(jc);
        return jc;
    }

    public ArchitectChat createArchitectChat(Action a) {
        ArchitectChat ac;
        String key = ArchitectChat.GetKey(a.getTargets().getFirst(), a.getTargets().getLast(), n.getDayNumber());

        NightChat nl = getNightLog(key);
        if(nl != null){
            ac = (ArchitectChat) nl;
            ac.addObserverArchitect(a.owner);
        }else{
            ac = new ArchitectChat(a.owner, a.getTargets().get(0), a.getTargets().get(1));
            this.tempChats.add(ac);
        }
        return ac;

    }

    public void restoreFactionChats() {

        for(FactionChat fc: factionChats){
            fc.refreshVisibility();
        }
    }

    public void grantArchitectAccess() {
        for(RoleCreatedChat temp: tempChats){
            if(temp instanceof ArchitectChat){
                ((ArchitectChat) temp).addArchitect();
            }
        }
    }

    public EventLog getEventLog(String key) {
        if(Constants.DEAD_CHAT.equals(key))
            return deadChat;
        else if(Constants.DAY_CHAT.equals(key))
            return dayChat;
        else if(n.getPlayerByID(key) != null)
            key = Constants.VOID_CHAT;// triggers void chat
        return getNightLog(key);
    }

    public void initializeNightLogs() {
        for(FactionChat fc: factionChats){
            fc.refreshVisibility();
            fc.setActive();
        }

        voidChat.setActive();
    }

    public void initializeTeamChats() {
        ArrayList<String> teamsWithChats = new ArrayList<>();
        PlayerList masons;
        for(GameFaction t: n.getFactions()){

            // some factions might be empty bc
            // no more empty faction cleanup on game start
            if(t.getMembers().isEmpty())
                continue;

            if(t.hasNightChat()){
                factionChats.add(new FactionChat(n, t));
                teamsWithChats.add(t.getColor());
            }else{
                masons = new PlayerList();
                for(Player p: t.getMembers()){
                    if(Mason.IsMasonType(p))
                        masons.add(p);
                }
                if(!masons.isEmpty()){
                    factionChats.add(new FactionChat(n, masons));
                    teamsWithChats.add(t.getColor());
                }
            }
        }

    }

}
