package game.event;

public interface NumberedChat {
    public int getDayNumber();
}
