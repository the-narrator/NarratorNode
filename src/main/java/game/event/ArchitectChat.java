package game.event;

import game.ai.Controller;
import game.logic.Player;
import game.logic.PlayerList;

public class ArchitectChat extends RoleCreatedChat {

    public static final String KEY_SEPERATOR = "^";
    private PlayerList architects;
    public String name;
    public String key;

    public ArchitectChat(Player architect, Player p1, Player p2) {
        super(new PlayerList(p1, p2), architect.game.getDayNumber());
        if(p1.getName().compareTo(p2.getName()) > 1){
            Player temp = p1;
            p1 = p2;
            p2 = temp;
        }
        this.architects = new PlayerList(architect);

        name = p1.getName() + "/" + p2.getName() + " Room [N" + getDay() + "]";

        key = GetKey(p1, p2, getDay());

        p1.addArchChat(this);
        p2.addArchChat(this);
    }

    public static String GetKey(Controller p1, Controller p2, int day) {
        if(p1.getName().compareTo(p2.getName()) > 1){
            Controller temp = p1;
            p1 = p2;
            p2 = temp;
        }

        return p1.getName() + KEY_SEPERATOR + p2.getName() + KEY_SEPERATOR + day;
    }

    @Override
    public boolean hasAccess(String... names) {
        for(String name: names){
            if(getMembers().getNamesToStringList().contains(name))
                return true;
        }

        return super.hasAccess(names);
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getKey(Player p) {
        return key;
    }

    @Override
    public String toString() {
        return getName();
    }

    public void addArchitect() {
        for(Player arch: architects){
            arch.addArchChat(this);
            addAccess(arch);
        }
    }

    public void addObserverArchitect(Player owner) {
        architects.add(owner);
    }

    @Override
    public String getType() {
        return "fab fa-fort-awesome";
    }

    public PlayerList getCreator() {
        return architects.copy();
    }
}
