package game.event;

import java.util.ArrayList;
import java.util.Optional;

import game.ai.Controller;
import game.logic.Game;
import game.logic.GameFaction;
import game.logic.GameRole;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.support.Constants;
import game.logic.support.HTString;
import game.logic.support.StringChoice;
import models.enums.GamePhase;
import util.Util;

public abstract class Message {

    public static final int DAY_LABEL = 0;
    public static final int JESTER_CHANGE = 1;
    public static final int DEATH_DESCRIPTION = 2;// " changed into a Jester."

    // VISIBLITY
    public static final String PRIVATE = Constants.PRIVATE;
    public static final String PUBLIC = Constants.PUBLIC;

    public static int NORMAL_EVENT = 0;
    public static int PLAYER_EVENT = 1;

    private ArrayList<String> access;
    private ArrayList<Object> eventParts;

    public Game n;

    private int id = 0;

    public Message(Game narrator) {
        this.n = narrator;

        eventParts = new ArrayList<>();
        access = new ArrayList<>();

        if(n != null){ // only null when ogimessage without a constructor is called. //TODO remove this
            this.day = n.getDayNumber();
            this.phase = n.phase;
            if(this instanceof Feedback && !n.isDay())
                day++;
        }

        if(narrator != null && (id == 0 || id == -1)){
            id = narrator.idRandomer.nextInt();
        }
    }

    @Override
    public String toString() {
        boolean b = showInPrivate;
        showInPrivate = true;
        String m = access(Message.PRIVATE, null);
        showInPrivate = b;
        return m;
    }

    public boolean hasAccess(Controller c, Game n) {
        return hasAccess(c.getPlayer());
    }

    public boolean hasAccess(Player p) {
        return hasAccess(p.getID());
    }

    public boolean hasAccess(String... level) {
        for(String k: level){
            if(hasAccess(k))
                return true;
        }
        return false;
    }

    private boolean hasAccess(String level) {
        boolean access;
        if(n == null)// regular message
            access = true;
        else if(level.equals(PRIVATE))// all private queries are taken care of for
            access = showInPrivate;
        else if(!n.isStarted())
            access = true;
        else if(!n.isInProgress()){// game over. show all private events, or show
            access = showInPrivate || this.access.contains(level);
        }else{ // if started and in progress
            if(level.equals(PRIVATE))
                access = showInPrivate;
            else if(isPublic())
                access = true;
            else
                access = this.access.contains(level);
        }
        return access;
    }

    public String access(Controller c, Game n) {
        return access(c.getPlayer());
    }

    public String access(Player p) {
        return access(p.getID(), null);
    }

    public String access(Player p, EventDecoder html) {
        return access(p.getID(), html);
    }

    public String access(String level) {
        return access(level, null);
    }

    public String access(String level, EventDecoder decoder) {
        if(!hasAccess(level))
            return "";

        StringBuilder sb = new StringBuilder("");
        for(Object o: eventParts){
            if(o instanceof HTString){
                HTString ht = (HTString) o;
                sb.append(ht.access(decoder));
            }else if(o instanceof PlayerWrapper){
                sb.append(accessHelper((PlayerWrapper) o, level, day, decoder));
            }else if(o instanceof StringChoice){
                StringChoice sc = (StringChoice) o;
                Object object = sc.getString(level);
                String ret;
                if(object instanceof String)
                    ret = (object.toString());
                else if(object instanceof HTString){
                    HTString ht = (HTString) object;
                    ret = ht.access(decoder);
                }else
                    ret = (accessHelper((PlayerWrapper) object, level, day, decoder));
                if(o == eventParts.get(0) && decoder != null){
                    ret = decoder.bold(ret);
                }
                sb.append(ret);
            }else if(o instanceof PlayerListWrapper){
                for(PlayerWrapper p: ((PlayerListWrapper) o).list){
                    sb.append(accessHelper(p, level, day, decoder));
                    sb.append(", ");
                }
                sb.deleteCharAt(sb.length() - 1);
                sb.deleteCharAt(sb.length() - 1);
            }else{
                sb.append(o.toString());
            }
        }

        String ret = sb.toString();
        return ret;
    }

    public static String accessHelper(StringChoice sc, String level, int day, EventDecoder html, Game n) {
        PlayerWrapper wp;
        Object o = sc.getString(level);
        if(o instanceof Player)
            wp = new PlayerWrapper((Player) o);
        else if(o instanceof PlayerWrapper){
            wp = (PlayerWrapper) o;
        }else
            wp = new PlayerWrapper((String) o, sc.getPlayer());
        return accessHelper(wp, level, day, html);
    }

    public static String accessHelper(Player p, String level, int day, EventDecoder html) {
        return accessHelper(new PlayerWrapper(p), level, day, html);
    }

    private int day;
    private GamePhase phase;

    public static String accessHelper(PlayerWrapper p, String level, int day, EventDecoder html) {
        Game n = p.player.game;
        if(html != null){
            return html.decode(p, day, level);
        }else if(level.equals(PRIVATE) || !n.isInProgress() && n.isStarted()){
            return p.getHiddenDescription();
        }else{
            return p.player.getDescription(p.getName(), p.role);
        }
    }

    public boolean isPublic() {
        return access.isEmpty();
    }

    // because access is no longer empty, you need a key to access this event's
    // contents. setting private and adding someone is redundant.
    public void setPrivate() {
        access.add(PRIVATE);
    }

    protected void setVisibility(PlayerList players) {
        for(Player p: players)
            setVisibility(p);

    }

    public void removeVisiblity(Player p) {
        access.remove(p.getID());
    }

    public void setVisibility(String v) {
        access.add(v);
    }

    public Message setVisibility(Player p) {
        if(!access.contains(p.getID()))
            access.add(p.getID());
        return this;
    }

    @SuppressWarnings("unchecked")
    public Message add(Object part) {
        if(part == null)
            throw new NullPointerException();
        if(part instanceof ArrayList)
            for(Object o: (ArrayList<Object>) part)
                add(o);
        else if(part instanceof Player){
            eventParts.add(new PlayerWrapper((Player) part));
        }else if(part instanceof PlayerList){
            eventParts.add(new PlayerListWrapper((PlayerList) part));
        }else{
            eventParts.add(part);
        }
        return this;
    }

    public static final String INITIAL = Constants.INITIAL;

    public static class PlayerWrapper {
        public Player player;
        Optional<GameFaction> team;
        public GameRole role;
        Game n;
        String oGname;

        public PlayerWrapper(Player p) {
            this.player = p;
            this.n = p.game;
            this.team = Optional.ofNullable(p.getGameFaction());
            this.role = p.gameRole;
            this.oGname = p.getName();
        }

        public String name;// used for jailor and probably architect

        public PlayerWrapper(String name, Player p) {
            this.name = name;
            this.n = p.game;
            this.player = p;
            this.role = p.gameRole;
            this.team = Optional.ofNullable(p.getGameFaction());
        }

        public String getName() {
            if(oGname == null)
                return player.getName();
            return oGname;
        }

        public String getColor() {
            if(team.isPresent())
                return team.get().getColor();
            return INITIAL;
        }

        public String getHiddenDescription() {
            if(player == null)
                return name;
            return player.getDescription(player.getID(), role);
        }
    }

    public class PlayerListWrapper {

        private ArrayList<PlayerWrapper> list;

        public PlayerListWrapper(PlayerList part) {
            list = new ArrayList<>();
            for(Player p: part){
                list.add(new PlayerWrapper(p));
            }
        }
    }

    public Message add(Object... objects) {
        for(Object o: objects)
            add(o);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if(o == null)
            return false;
        if(!(o instanceof Message))
            return false;

        Message e = (Message) o;
        if(Util.notEqual(e.access, access))
            return false;
        if(showInPrivate != e.showInPrivate)
            return false;
        if(eventParts.size() != e.eventParts.size())
            return false;
        for(int i = 0; i < eventParts.size(); i++){
            Object o1 = eventParts.get(i);
            Object o2 = e.eventParts.get(i);
            if(o2 instanceof PlayerWrapper && o1 instanceof PlayerWrapper){
                if(o2.equals(o1))
                    continue;
            }
            if(o2 instanceof PlayerListWrapper && o1 instanceof PlayerListWrapper){
                if(o1.equals(o2))
                    continue;
            }
            if(o2 instanceof String && o1 instanceof String){
                if(o1.equals(o2))
                    continue;
            }
            if(o1 instanceof StringChoice && o2 instanceof StringChoice)
                if(o1.equals(o2))
                    continue;
            if(o1 instanceof HTString && o2 instanceof HTString)
                if(o1.equals(o2))
                    continue;
            return false;
        }

        return true;
    }

    private boolean showInPrivate = true;

    public Message dontShowPrivate() {
        showInPrivate = false;
        return this;
    }

    public static String CommandCreator(String... command) {
        StringBuilder sb = new StringBuilder();
        for(String s: command)
            sb.append(s + " ");
        sb.deleteCharAt(sb.length() - 1);
        return sb.toString();
    }

    public ArrayList<HTString> getHTStrings() {
        ArrayList<HTString> hts = new ArrayList<HTString>();
        for(Object o: eventParts){
            if(o instanceof HTString)
                hts.add((HTString) o);
        }
        return hts;
    }

    public PlayerList getPlayers() {
        PlayerList pList = new PlayerList();
        StringChoice scChoice;
        for(Object o: eventParts){
            if(o instanceof PlayerWrapper)
                pList.add(((PlayerWrapper) o).player);
            else if(o instanceof PlayerListWrapper){
                PlayerListWrapper pWrap = (PlayerListWrapper) o;
                for(PlayerWrapper p: pWrap.list)
                    pList.add(p.player);
            }else if(o instanceof StringChoice){
                scChoice = (StringChoice) o;
                if(scChoice.getPlayer() != null)
                    pList.add(scChoice.getPlayer());
            }
        }

        return pList;
    }

    public void setDay(int dayNumber) {
        this.day = dayNumber;
    }

    public int getDay() {
        return day;
    }

    public GamePhase getPhase() {
        return phase;
    }

    public void setPhase(GamePhase phase) {
        this.phase = phase;
    }

    public abstract ArrayList<String> getEnclosingChats(Game n, Player p);

    protected static ArrayList<String> addHeaders(String... strings) {
        return addHeaders(null, strings);
    }

    protected static ArrayList<String> addHeaders(ArrayList<String> ret, String... strings) {
        if(ret == null)
            ret = new ArrayList<>();
        Util.mash(ret, strings);
        return ret;
    }

    protected String getSpecificDayChat() {
        return "Day " + day;
    }

    public Integer getID() {
        return id;
    }

    public Message setPicture(String s) {
        this.picture = s;
        return this;
    }

    private String picture;

    public String getPicture() {
        return picture;
    }

    private ArrayList<String> messageExtras;

    public Message addExtraInfo(String string) {
        if(messageExtras == null)
            messageExtras = new ArrayList<>();
        messageExtras.add(string);
        return this;
    }

    public ArrayList<String> getExtras() {
        return messageExtras;
    }

    public boolean isNightToDayAnnouncement() {
        return false;
    }
}
