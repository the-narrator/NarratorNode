package game.event;

import game.logic.Player;
import game.logic.PlayerList;

public abstract class EventLog {

    private boolean active;

    public EventLog(PlayerList members) {
        this.members = members;
        active = true;
    }

    public boolean hasAccess(String... names) {
        for(String name: names)
            if(name.equals(Message.PRIVATE))
                return true;
        return false;
    }

    private PlayerList members;
    private EventList events = new EventList();

    public PlayerList getMembers() {
        return members.copy();
    }

    public EventLog add(Message e) {
        // if(e instanceof Feedback && this instanceof DayChat)
        // events.addFront(e);
        // else
        events.add(e);
        return this;
    }

    public EventList getEvents() {
        return events;
    }

    public EventList getEvents(int dayNumber) {
        EventList eLog = new EventList();
        for(Message e: events){
            if(e.getDay() == dayNumber){
                eLog.add(e);
            }
        }
        return eLog;
    }

    public static String UnderLine(String ret) {
        return "<u>" + ret + "</u>";
    }

    public abstract String getName();

    public abstract String getKey(Player p);

    public abstract String getType();

    public boolean isActive() {
        return active;
    }

    public void setInactive() {
        active = false;
    }

    public void setActive() {
        active = true;
    }

    public void addAccess(Player player) {
        for(Message m: this.events){
            m.setVisibility(player);
        }
        if(members != null && !members.contains(player))
            members.add(player);
    }

    public void removeAccess(Player p) {
        boolean isPublic;
        for(Message m: this.events){
            isPublic = m.isPublic();
            m.removeVisiblity(p);
            if(!isPublic && m.isPublic())
                m.setPrivate();
        }
        if(members != null && members.contains(p))
            members.remove(p);
    }
}
