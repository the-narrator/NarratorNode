package game.event;

import java.util.ArrayList;
import java.util.Iterator;

import game.logic.Player;
import game.logic.support.Random;

public class EventList implements Iterable<Message> {

    public ArrayList<Message> events;

    public EventList() {
        events = new ArrayList<>();
    }

    public String access(String key) {
        return access(key, null);
    }

    public String access(String key, EventDecoder html) {
        StringBuffer sb = new StringBuffer();
        String toAdd;
        for(Message e: events){
            toAdd = e.access(key, html);
            if(toAdd.length() != 0){
                sb.append(toAdd);
                sb.append("\n");
            }
        }
        return trim(sb.toString());
    }

    public String access(Player p, EventDecoder html) {
        return access(p.getName(), html);
    }

    public void add(Message e, String key) {
        if(e.hasAccess(key))
            events.add(e);
    }

    public EventList add(Message e) {
        events.add(e);
        return this;
    }

    public void addFront(Message e) {
        events.add(0, e);
    }

    public void add(EventList events, String... keys) {
        for(Message e: events)
            if(e.hasAccess(keys))
                this.events.add(e);
    }

    public void clear() {
        events.clear();
    }

    public void shuffle(Random random) {
        random.shuffle(events, null);
    }

    private String trim(String s) {
        while (s.contains("\n\n\n\n")){
            s = s.replace("\n\n\n\n", "\n\n\n");
        }
        return s;
    }

    public Iterator<Message> iterator() {
        return events.iterator();
    }

    public boolean isEmpty() {
        return events.isEmpty();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("[");
        String access;
        for(Message m: events){
            access = m.access(Message.PRIVATE);
            if(access.length() > 1){
                sb.append(access);
                sb.append("\n");
            }
        }
        sb.append("]");
        return sb.toString();
    }

    public int size() {
        return events.size();
    }

    public Message getLast() {
        if(events == null || events.isEmpty())
            return null;
        return events.get(events.size() - 1);
    }

    public EventList filterDayNumber(int dayNumber) {
        EventList el = new EventList();
        for(Message m: this.events){
            if(m.getDay() == dayNumber)
                el.add(m);
        }
        return el;
    }
}
