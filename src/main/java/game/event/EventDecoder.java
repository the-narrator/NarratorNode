package game.event;

import game.event.Message.PlayerWrapper;

public interface EventDecoder {

    String decode(PlayerWrapper p, int day, String level);

    String coloredText(String data, String color);

    String feedbackDecode(String access);

    String bold(String ret);

    String headerDecode(String s);

}
