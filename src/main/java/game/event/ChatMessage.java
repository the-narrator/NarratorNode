package game.event;

import java.util.ArrayList;
import java.util.Optional;

import game.abilities.Disguiser;
import game.abilities.JailCreate;
import game.abilities.Mason;
import game.logic.Game;
import game.logic.GameFaction;
import game.logic.Player;
import game.logic.exceptions.ChatException;
import game.logic.listeners.NarratorListener;
import game.logic.support.CommandHandler;
import game.logic.support.Constants;
import game.logic.support.StringChoice;
import json.JSONObject;

public class ChatMessage extends Message implements DaySpecificMessage {

    public String message, speakerName;
    private StringChoice sender;
    public Player speaker; // this is only used for lobby messages. it's not valid for in game messages

    public EventLog eventLog;

    public ChatMessage(Game n) {
        super(n);
    }

    public ChatMessage(Player p, String message, String key, Optional<String> source, JSONObject args) {
        super(p.game);
        this.speakerName = p.getName();
        Player originalPlayer = p;
        String originalKey = key;
        if(n.isDay() && n.getPlayerByName(key) != null){
            p = n.getPlayerByName(key);
            key = DayChat.KEY;
        }
        sender = new StringChoice(p);
        this.message = message;

        StringChoice sc = new StringChoice(p);
        sc.add(p, "You");
        add(sc, ": ", message);

        boolean selfTalking = false;

        EventManager events = n.getEventManager();
        if(!n.isStarted() || !n.isInProgress())
            eventLog = events.getDayChat();
        else if(p.isDead()){
            eventLog = events.getDeadChat();
            setVisibility(eventLog.getMembers());
        }else if(key.equals(VoidChat.KEY)){
            setVisibility(p);
            dontShowPrivate();
            eventLog = events.getNightLog(key);
            selfTalking = true;
        }else if(n.isDay()){
            if(key.equalsIgnoreCase(Constants.DAY_CHAT))
                eventLog = events.getDayChat();
            else
                throw new ChatException(key);

        }else{
            eventLog = events.getNightLog(key);
            if(eventLog instanceof ArchitectChat){
                setVisibility(eventLog.getMembers());
            }else{
                GameFaction t = n.getFaction(key);
                if(t == null)
                    throw new ChatException(key);

                Player disguisedTarget;
                for(Player teamMate: t.getMembers()){
                    if(teamMate.isQuarintined())
                        continue;
                    disguisedTarget = Disguiser.getDisguised(teamMate);
                    if(t.hasNightChat() || Mason.IsMasonType(teamMate)
                            || (disguisedTarget != null && Mason.IsMasonType(disguisedTarget)))
                        setVisibility(teamMate);
                }
                eventLog = events.getNightLog(key);
            }

        }
        eventLog.add(this);
        if(p.game.isStarted())
            p.game.getEventManager().addCommand(originalPlayer, Constants.ALL_TIME_LEFT, CommandHandler.SAY,
                    originalKey, message);

        if(selfTalking){
            p.sendMessage(this);
        }else{
            for(Player receiver: eventLog.getMembers())
                receiver.sendMessage(this);
            for(NarratorListener nListner: p.game.getListeners())
                nListner.onChatMessageSend(this, source, args);
        }
    }

    @Override
    public void finalize() {
        for(Player p: n.players){
            if(hasAccess(p.getName()))
                p.sendMessage(this);
        }
    }

    public StringChoice getSender() {
        return sender;
    }

    @Override
    public String getName() {
        return eventLog.getName();
    }

    private static JailChat getJailChat(Game n, String jailKey) {
        return (JailChat) n.getEventManager().getNightLog(jailKey);
    }

    private static ChatMessage JailChatMessage(Player p, String message, String jailKey) {
        ChatMessage cm = new ChatMessage(p.game);
        cm.message = message;

        JailChat jc = getJailChat(p.game, jailKey);

        jc.add(cm);
        cm.eventLog = jc;

        cm.setVisibility(jc.getMembers());

        if(p.game.isStarted())
            p.game.getEventManager().addCommand(p, Constants.ALL_TIME_LEFT, CommandHandler.SAY, jailKey, message);
        else{
            cm.toString();
        }
        return cm;
    }

    public static ChatMessage JailCaptiveMessage(Player captive, String message, String jailKey) {
        ChatMessage cm = JailChatMessage(captive, message, jailKey);

        StringChoice sc = new StringChoice(captive);
        sc.add(captive, "You");
        cm.add(sc, ": ", message);
        cm.sender = sc;

        for(Player receiver: cm.eventLog.getMembers()){
            receiver.sendMessage(cm);
        }

        return cm;
    }

    public static ChatMessage JailCaptorMessage(Player jailor, String message, String jailKey) {
        JailChat jc = getJailChat(jailor.game, jailKey);

        StringChoice sc = new StringChoice(jailor);
        sc.add(jailor, "You");
        sc.add(jc.getCaptive(), JailCreate.CAPTOR);

        ChatMessage cm = JailChatMessage(jailor, message, jailKey);
        cm.add(sc, ": ", message);

        cm.sender = sc;

        for(Player receiver: cm.eventLog.getMembers()){
            receiver.sendMessage(cm);
        }

        return cm;
    }

    @Override
    public ArrayList<String> getEnclosingChats(Game n, Player p) {
        if(eventLog instanceof FactionChat && p != null && p.isQuarintined() && n.isInProgress())
            return new ArrayList<>();
        if(eventLog instanceof DayChat)
            return addHeaders(getSpecificDayChat());
        return addHeaders(eventLog.getName());
    }

    @Override
    public Integer getID() {
        return null;
    }

}
