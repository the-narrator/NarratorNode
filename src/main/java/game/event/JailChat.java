package game.event;

import game.ai.Controller;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.support.Constants;

public class JailChat extends RoleCreatedChat {

    private Player jailor, captive;

    public JailChat(Player jailor, Player captive) {
        super(new PlayerList(jailor, captive), jailor.game.getDayNumber());
        this.jailor = jailor;
        this.captive = captive;

        jailor.setJailed(this);
        captive.setJailed(this);
    }

    @Override
    public boolean hasAccess(String... names) {
        for(String name: names){
            if(jailor.getName().equals(name) || captive.getName().equals(name))
                return true;
        }

        return super.hasAccess(names);
    }

    @Override
    public String getName() {
        String name = GetName(captive);
        if(jailor.game.getDayNumber() != getDay())
            name += " {N" + getDay() + "}";
        return name;
    }

    public static String GetName(Controller captive) {
        return GetName(captive.getName());
    }

    public static String GetName(String captiveName) {
        return captiveName + "'s Cell";
    }

    @Override
    public String getKey(Player p) {
        return GetKey(captive.getName(), getDay());
    }

    public static String GetKey(Controller c, int day) {
        return GetKey(c.getName(), day);
    }

    public static String GetKey(String playerName, int day) {
        return playerName + Constants.JAIL_CHAT_KEY_SEPERATOR + day;
    }

    public Player getCaptive() {
        return captive;
    }

    @Override
    public String toString() {
        return getName();
    }

    @Override
    public String getType() {
        return "fas fa-bars";
    }
}
