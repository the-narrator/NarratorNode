package game.event;

import java.util.ArrayList;

import game.logic.Game;
import game.logic.Player;
import models.enums.GamePhase;

public class Header extends Message implements DaySpecificMessage {

    private int day;
    private GamePhase phase;

    public Header(int day, GamePhase phase, Game n) {
        super(n);
        this.day = day;
        this.phase = phase;
        super.n = n;
    }

    @Override
    public String access(String s, EventDecoder html) {
        String toRet = phase == GamePhase.NIGHT_ACTION_SUBMISSION ? "Night " : "Day ";
        toRet += day;
        if(html == null)
            return "\n" + toRet + "\n";
        return html.headerDecode(toRet);
    }

    @Override
    public String getName() {
        if(phase != GamePhase.NIGHT_ACTION_SUBMISSION)
            return "Day " + day;
        return n.getEventManager().getNightLog(VoidChat.KEY).getName();
    }

    @Override
    public ArrayList<String> getEnclosingChats(Game n, Player p) {
        if(phase != GamePhase.NIGHT_ACTION_SUBMISSION)
            return addHeaders(n.getEventManager().dayChat.getName());
        return addHeaders(n.getEventManager().voidChat.getName());
    }
}
