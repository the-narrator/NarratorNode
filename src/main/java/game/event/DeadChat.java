package game.event;

import game.logic.Game;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.support.Constants;

public class DeadChat extends EventLog {

    public static final String KEY = Constants.DEAD_CHAT;

    private Game n;

    public DeadChat(Game n) {
        super(null);
        this.n = n;
    }

    @Override
    public boolean hasAccess(String... keys) {
        for(String name: keys){
            Player p = n.getPlayerByID(name);
            if(p != null && p.isEliminated())
                return true;
        }
        return false;
    }

    @Override
    public String getName() {
        return "Dead Chat";
    }

    @Override
    public PlayerList getMembers() {
        return n.getDeadPlayers();
    }

    @Override
    public String getKey(Player p) {
        return KEY;
    }

    @Override
    public String getType() {
        return "fab fa-snapchat-ghost";
    }
}
