# TheNarrator
Library for the popular Mafia game!

This is the java API for running 'The Narrator'.  Pretty much used in every other project with ^*Narrator*$ here.

Current features include:
-70+ different abilities
-Custom teams
-String command parsing
-Unlimited players

