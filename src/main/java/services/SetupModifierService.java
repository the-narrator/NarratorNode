package services;

import java.sql.SQLException;
import java.time.Instant;

import game.setups.Setup;
import models.LobbySetupSchema;
import models.ModifierValue;
import models.dbo.SetupDbo;
import models.enums.SetupModifierName;
import models.modifiers.Modifier;
import models.modifiers.SetupModifier;
import models.serviceResponse.ModifierServiceResponse;
import repositories.SetupModifierRepo;
import util.game.ModifierUtil;

public class SetupModifierService {

    public static void upsertModifier(Setup setup, SetupModifierName name, Object val) {
        upsertModifier(setup, name, val, 0, Setup.MAX_PLAYER_COUNT);
    }

    public static SetupModifier upsertModifier(Setup setup, SetupModifierName name, Object value, int minPlayerCount,
            int maxPlayerCount) {
        SetupModifier modifier = new SetupModifier(name, new ModifierValue(value), minPlayerCount, maxPlayerCount,
                Instant.now());
        ModifierUtil.checkModifierValueType(modifier);
        checkBounds(modifier);
        setup.modifiers.add(modifier);
        return modifier;
    }

    public static ModifierServiceResponse<SetupModifierName> upsertSetupModifier(long userID,
            Modifier<SetupModifierName> newModifier) throws SQLException {
        LobbySetupSchema lobbySetup = SetupService.assertModeratorAndOwner(userID);

        ModifierUtil.checkModifierValueType(newModifier);
        checkBounds(newModifier);

        SetupModifierRepo.upsertSetupModifier(newModifier, lobbySetup.setup.id);

        return new ModifierServiceResponse<>(lobbySetup.game, newModifier);
    }

    private static void checkBounds(Modifier<SetupModifierName> modifier) {
        if(!ModifierUtil.IsIntModifier(modifier.name))
            return;
        int value = modifier.value.getIntValue();
        SetupModifierName name = modifier.name;
        switch (name) {
        case CHARGE_VARIABILITY:
            ModifierUtil.minOutOfBoundsException(name, value, 0);
            return;
        case MARSHALL_EXECUTIONS:
        case BOUNTY_INCUBATION:
            ModifierUtil.minOutOfBoundsException(name, value, 1);
            break;
        case JESTER_KILLS:
            ModifierUtil.minOutOfBoundsException(name, value, 0);
            ModifierUtil.maxOutOfBoundsException(name, value, 100);
            break;
        default:
            return;
        }
    }

    public static void loadSetupModifiers(Setup setup, SetupDbo setupDbo) {
        for(Modifier<SetupModifierName> modifier: setupDbo.modifiers)
            setup.modifiers.add(modifier);
    }

}
