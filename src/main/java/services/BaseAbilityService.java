package services;

import java.util.HashSet;
import java.util.Set;

import game.abilities.util.AbilityUtil;
import game.setups.Setup;
import models.BaseAbility;
import models.enums.AbilityType;
import models.modifiers.Modifiers;

public class BaseAbilityService {

    public static Set<BaseAbility> getBaseAbilities() {
        Set<BaseAbility> abilities = new HashSet<>();
        Setup setup = Setup.NoDB();
        for(AbilityType abilityType: AbilityType.values())
            if(abilityType.dbName != null)
                abilities.add(new BaseAbility(abilityType, isFactionAllowed(setup, abilityType)));

        return abilities;
    }

    private static boolean isFactionAllowed(Setup setup, AbilityType abilityType) {
        return AbilityUtil.CREATOR(abilityType, setup, new Modifiers<>()).isAllowedFactionAbility();
    }

}
