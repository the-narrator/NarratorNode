package services;

import java.sql.SQLException;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import game.abilities.Hidden;
import game.logic.Game;
import game.logic.Player;
import game.logic.exceptions.UnknownRoleException;
import game.setups.Setup;
import models.Faction;
import models.PreferenceSchema;
import models.Role;
import models.enums.PreferType;
import models.schemas.PlayerSchema;
import repositories.PreferRepo;
import util.PlayerUtil;
import util.game.FactionRoleUtil;
import util.game.LookupUtil;

public class PreferService {

    public static void applyPrefers() {

    }

    public static void createPreference(long userID, long setupID, String preference) throws SQLException {
        Setup setup = SetupService.getSetup(setupID);

        Optional<Faction> faction = LookupUtil.findFactionByName(setup, preference.toLowerCase());
        if(faction.isPresent()){
            PreferRepo.createPreference(userID, setupID, faction.get().id, PreferType.FACTION);
            return;
        }
        faction = LookupUtil.findFactionByColor(setup, preference);
        if(faction.isPresent()){
            PreferRepo.createPreference(userID, setupID, faction.get().id, PreferType.FACTION);
            return;
        }

        Optional<Hidden> hidden = LookupUtil.findHidden(setup, preference);
        if(hidden.isPresent()){
            preferHidden(userID, setupID, hidden.get());
            return;
        }

        Optional<Role> role = LookupUtil.findRole(setup, preference);
        if(role.isPresent()){
            PreferRepo.createPreference(userID, setupID, role.get().id, PreferType.ROLE);
            return;
        }

        throw new UnknownRoleException(preference);
    }

    private static void preferHidden(long userID, long setupID, Hidden hidden) throws SQLException {
        Set<Long> roleIDs = FactionRoleUtil.getRoleIDs(hidden.getAllFactionRoles());
        PreferRepo.createPreference(userID, setupID, roleIDs, PreferType.ROLE);
    }

    public static void loadPreferences(Set<PlayerSchema> players, Game game) throws SQLException {
        Set<Long> userIDs = PlayerUtil.getUserIDs(players);
        Map<Long, Set<PreferenceSchema>> preferenceMap = PreferRepo.get(userIDs, game.setup.id);

        Set<PreferenceSchema> preferences;
        Player player;
        for(PlayerSchema playerSchema: players){
            if(!playerSchema.userID.isPresent())
                continue;

            preferences = preferenceMap.getOrDefault(playerSchema.userID.get(), new HashSet<>());
            player = game.getPlayerByName(playerSchema.name);
            addPrefers(preferences, player);
        }
    }

    private static void addPrefers(Set<PreferenceSchema> preferences, Player player) {
        Setup setup = player.game.setup;
        for(PreferenceSchema preference: preferences){
            switch (preference.preferenceType) {
            case ROLE:
                Optional<Role> role = LookupUtil.findRole(setup, preference.entityID);
                if(role.isPresent())
                    player.rolePrefer(role.get());
                break;
            case FACTION:
                Optional<Faction> faction = LookupUtil.findFaction(setup, preference.entityID);
                if(faction.isPresent())
                    player.teamPrefer(faction.get());
                break;
            }
        }
    }
}
