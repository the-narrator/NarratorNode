package services;

import java.sql.SQLException;
import java.time.Instant;
import java.util.Map;
import java.util.Set;

import game.abilities.GameAbility;
import game.logic.exceptions.IllegalGameSettingsException;
import game.logic.exceptions.NarratorException;
import game.logic.support.Constants;
import game.setups.Setup;
import models.Ability;
import models.LobbySetupSchema;
import models.ModifierValue;
import models.Role;
import models.dbo.SetupDbo;
import models.enums.AbilityModifierName;
import models.enums.AbilityType;
import models.modifiers.AbilityModifier;
import models.modifiers.Modifier;
import models.schemas.AbilityModifierSchema;
import models.serviceResponse.ModifierServiceResponse;
import repositories.RoleAbilityModifierRepo;
import util.game.ModifierUtil;

public class RoleAbilityModifierService {

    public static void upsert(Role role, AbilityType abilityType, AbilityModifierName modifier, Object object) {
        upsertModifier(role, abilityType, modifier, object, 0, Setup.MAX_PLAYER_COUNT);
    }

    public static void upsertModifier(Role role, AbilityType abilityType, AbilityModifierName modifier, Object object,
            int minPlayerCount, int maxPlayerCount) {
        upsertModifier(role, abilityType, new AbilityModifier(modifier, new ModifierValue(object), minPlayerCount,
                maxPlayerCount, Instant.now()));
    }

    public static void upsertModifier(Role role, AbilityType abilityType, Modifier<AbilityModifierName> modifier) {
        Ability ability = role.getAbility(abilityType);

        ModifierUtil.checkModifierValueType(modifier);
        if(ModifierUtil.IsBoolModifier(modifier.name))
            validateAbilityModifierBool(modifier.name, ability);
        else if(ModifierUtil.IsIntModifier(modifier.name))
            validateAbilityModifierInt(modifier.name, ability, modifier.value.getIntValue());

        ability.modifiers.add(modifier);
    }

    public static ModifierServiceResponse<AbilityModifierName> upsertModifier(long userID, long roleID, long abilityID,
            AbilityModifier newModifier) throws SQLException {
        LobbySetupSchema lobbySetup = SetupService.assertModeratorAndOwner(userID);
        Role role = RoleService.get(userID, roleID).role;
        Ability ability = role.getAbility(abilityID);

        ModifierUtil.checkModifierValueType(newModifier);

        RoleAbilityModifierRepo.upsert(abilityID, newModifier);
        upsertModifier(role, ability.type, newModifier);

        return new ModifierServiceResponse<>(lobbySetup.game, newModifier);
    }

    public static void validateAbilityModifierBool(AbilityModifierName name, Ability ability)
            throws IllegalGameSettingsException {
        if(!ModifierUtil.IsBoolModifier(name))
            throw new IllegalGameSettingsException(
                    "Modifier of type " + name.toString() + " is not a boolean modifier.");

        GameAbility gameAbility = ability.getGameAbility();
        String abilityName = ability.type.toString();
        if(name == AbilityModifierName.SELF_TARGET && !gameAbility.isSelfTargetModifiable())
            throw new IllegalGameSettingsException(abilityName + "'s self targetable attribute is not modifiabe.");
        if(name == AbilityModifierName.BACK_TO_BACK && !gameAbility.isBackToBackModifiable())
            throw new IllegalGameSettingsException(abilityName + "'s self targetable attribute is not modifiable.");
        if(name == AbilityModifierName.ZERO_WEIGHTED && !gameAbility.isZeroWeightModifiable())
            throw new IllegalGameSettingsException(abilityName + " cannot be set to submit freely.");
    }

    public static void validateAbilityModifierInt(AbilityModifierName name, Ability ability, int value)
            throws IllegalGameSettingsException, NarratorException {
        if(!ModifierUtil.IsIntModifier(name))
            throw new IllegalGameSettingsException(
                    "Modifier of type " + name.toString() + " is not an integer modifier.");

        if(value < Constants.UNLIMITED || value > Constants.MAX_INT_MODIFIER)
            throw new NarratorException("Integer value out of bounds.");
        if(value == Constants.UNLIMITED && name == AbilityModifierName.COOLDOWN)
            throw new NarratorException("Integer value out of bounds");
        if(value == 0 && name == AbilityModifierName.CHARGES)
            throw new NarratorException("Integer value out of bounds");

        GameAbility gameAbility = ability.getGameAbility();
        String abilityName = ability.type.toString();
        if(name == AbilityModifierName.CHARGES && !gameAbility.isChargeModifiable())
            throw new IllegalGameSettingsException(abilityName + " cannot have their charges modified.");
        if(name == AbilityModifierName.COOLDOWN && !gameAbility.hasCooldownAbility())
            throw new IllegalGameSettingsException(abilityName + " cannot have their cooldown modified.");
        if(name == AbilityModifierName.HIDDEN && !gameAbility.isHiddenModifiable())
            throw new IllegalGameSettingsException(abilityName + " cannot be set to hidden.");
        if(name == AbilityModifierName.SELF_TARGET && !gameAbility.isSelfTargetModifiable())
            throw new IllegalGameSettingsException(abilityName + " cannot have their self targetable modifier edited.");
    }

    static void loadModifiers(Setup setup, SetupDbo setupDbo) {
        Role role;
        Map<Long, Map<Long, Set<AbilityModifierSchema>>> abilityModifiers = setupDbo.roleAbilityModifiers;
        Map<Long, Set<AbilityModifierSchema>> roleAbilityModifiersMap;
        Set<AbilityModifierSchema> modifiers;
        Ability ability;
        for(long roleID: abilityModifiers.keySet()){
            role = setup.getRole(roleID);
            roleAbilityModifiersMap = abilityModifiers.get(roleID);
            for(long abilityID: roleAbilityModifiersMap.keySet()){
                ability = role.getAbility(abilityID);
                modifiers = roleAbilityModifiersMap.get(abilityID);
                for(AbilityModifierSchema abilityModifier: modifiers){
                    upsertModifier(role, ability.type, new AbilityModifier(abilityModifier));
                }
            }
        }
    }
}
