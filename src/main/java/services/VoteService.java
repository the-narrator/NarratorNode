package services;

import game.logic.Game;
import game.logic.exceptions.NarratorException;
import models.idtypes.GameID;
import models.json_output.VoteData;

public class VoteService {

    public static VoteData getVotes(GameID gameID) {
        Game game = GameService.gamesMap.get(gameID);
        if(game == null)
            throw new NarratorException("Active game with that ID not found.");
        return getVotes(game);
    }

    public static VoteData getVotes(Game game) {
        return new VoteData(game);
    }
}
