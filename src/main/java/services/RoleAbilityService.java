package services;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;

import game.abilities.util.AbilityUtil;
import game.logic.exceptions.NarratorException;
import game.setups.Setup;
import models.Ability;
import models.Role;
import models.dbo.SetupDbo;
import models.enums.AbilityType;
import models.requests.AbilityCreateRequest;
import models.schemas.AbilitySchema;
import repositories.RoleAbilityRepo;
import util.Util;

public class RoleAbilityService {

    // lobby permissions are already checked
    public static void createRoleAbilities(Setup setup, long roleID, LinkedHashSet<AbilityType> abilityTypes)
            throws SQLException {
        Role role = setup.getRole(roleID);
        createRoleAbilities(role, abilityTypes);
        ArrayList<Long> abilityIDs = RoleAbilityRepo.create(roleID, AbilityCreateRequest.from(abilityTypes));

        Iterator<Ability> abilityIterator = role.abilityMap.values().iterator();
        Iterator<Long> abilityIDIterator = abilityIDs.iterator();
        while (abilityIterator.hasNext())
            abilityIterator.next().id = abilityIDIterator.next();
    }

    public static void createRoleAbilities(Role role, LinkedHashSet<AbilityType> abilityTypes) {
        for(AbilityType abilityType: abilityTypes)
            createRoleAbility(role, new AbilitySchema(Util.getID(), abilityType));
    }

    public static void createRoleAbility(Role role, AbilitySchema schema) {
        if(role.hasAbility(schema.type))
            throw new NarratorException("Role already has this ability");
        if(schema.type.dbName == null)
            throw new NarratorException("Can't create ability with this type");
        Ability ability = new Ability(role.setup, schema);
        role.abilityMap.put(schema.type, ability);
    }

    public static void removeRoleAbility(Role role, AbilityType abilityType) {
        role.abilityMap.remove(abilityType);
    }

    public static void loadAbilities(Setup setup, SetupDbo setupDbo) {
        Map<Long, LinkedHashMap<Long, AbilitySchema>> allAbilities = setupDbo.roleAbilities;
        Role role;
        Map<Long, AbilitySchema> abilities;
        for(long roleID: allAbilities.keySet()){
            role = setup.getRole(roleID);
            abilities = allAbilities.get(roleID);
            for(AbilitySchema schema: AbilityUtil.getSortedByID(abilities.values()))
                createRoleAbility(role, schema);
        }
        RoleAbilityModifierService.loadModifiers(setup, setupDbo);
    }

}
