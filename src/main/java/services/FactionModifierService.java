package services;

import java.sql.SQLException;
import java.time.Instant;
import java.util.Map;
import java.util.Set;

import game.logic.exceptions.NarratorException;
import game.setups.Setup;
import models.Faction;
import models.LobbySetupSchema;
import models.ModifierValue;
import models.dbo.SetupDbo;
import models.enums.FactionModifierName;
import models.modifiers.FactionModifier;
import models.modifiers.Modifier;
import models.schemas.FactionModifierSchema;
import models.serviceResponse.ModifierServiceResponse;
import repositories.FactionModifierRepo;
import util.game.ModifierUtil;

public class FactionModifierService {

    public static void upsertModifier(Faction faction, FactionModifierName modifierName, Object val) {
        upsertModifier(faction, modifierName, val, 0, Setup.MAX_PLAYER_COUNT);
    }

    public static void upsertModifier(Faction faction, FactionModifierName modifierName, Object val, int minPlayerCount,
            int maxPlayerCount) {
        upsertModifier(faction, new FactionModifier(modifierName, new ModifierValue(val), minPlayerCount,
                maxPlayerCount, Instant.now()));
    }

    public static void upsertModifier(Faction faction, Modifier<FactionModifierName> modifier) {
        ModifierUtil.checkModifierValueType(modifier);
        checkBounds(modifier);
        faction.modifiers.add(modifier);
    }

    public static ModifierServiceResponse<FactionModifierName> upsertModifier(long userID, long factionID,
            Modifier<FactionModifierName> modifier) throws SQLException {
        LobbySetupSchema lobbySetup = SetupService.assertModeratorAndOwner(userID);
        Setup setup = SetupService.getSetup(lobbySetup.setup.id);
        Faction faction = setup.getFaction(factionID);

        ModifierUtil.checkModifierValueType(modifier);
        checkBounds(modifier);

        FactionModifierRepo.upsertModifier(modifier, factionID);
        upsertModifier(faction, modifier.name, modifier.value.internalValue);

        return new ModifierServiceResponse<>(lobbySetup.game, modifier);
    }

    private static void checkBounds(Modifier<FactionModifierName> modifier) {
        if(!ModifierUtil.IsIntModifier(modifier.name))
            return;
        int val = modifier.value.getIntValue();
        if(FactionModifierName.maxValue(modifier.name) < val || FactionModifierName.minValue(modifier.name) > val)
            throw new NarratorException("Value out of range.");
    }

    public static void loadModifiers(Setup setup, SetupDbo setupDbo) {
        Map<Long, Set<FactionModifierSchema>> allModifiers = setupDbo.factionModifiers;

        Set<FactionModifierSchema> modifiers;
        Faction faction;
        for(long factionID: allModifiers.keySet()){
            modifiers = allModifiers.get(factionID);
            faction = setup.factions.get(factionID);
            for(FactionModifierSchema fRule: modifiers)
                upsertModifier(faction, new FactionModifier(fRule));
        }
    }

}
