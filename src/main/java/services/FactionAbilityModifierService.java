package services;

import java.sql.SQLException;
import java.time.Instant;
import java.util.Map;
import java.util.Set;

import game.logic.exceptions.NarratorException;
import game.setups.Setup;
import models.Ability;
import models.Faction;
import models.LobbySetupSchema;
import models.ModifierValue;
import models.dbo.SetupDbo;
import models.enums.AbilityModifierName;
import models.enums.AbilityType;
import models.modifiers.AbilityModifier;
import models.modifiers.Modifiers;
import models.schemas.FactionAbilityModifierSchema;
import models.serviceResponse.ModifierServiceResponse;
import repositories.FactionAbilityModifierRepo;
import util.game.LookupUtil;
import util.game.ModifierUtil;

public class FactionAbilityModifierService {

    public static ModifierServiceResponse<AbilityModifierName> upsertModifier(long userID, long factionAbilityID,
            AbilityModifier modifier) throws SQLException {
        LobbySetupSchema lobbySetup = SetupService.assertModeratorAndOwner(userID);
        Setup setup = SetupService.getSetup(lobbySetup.setup.id);
        Faction faction = LookupUtil.findFactionByAbilityID(setup, factionAbilityID);
        Ability ability = LookupUtil.findFactionAbilityByID(setup, factionAbilityID);
        if(ability == null || faction == null)
            throw new NarratorException("ability not found with that id.");

        ModifierUtil.checkModifierValueType(modifier);
        FactionAbilityModifierRepo.upsertModifier(factionAbilityID, modifier);

        return new ModifierServiceResponse<>(lobbySetup.game, modifier);
    }

    public static void upsertModifier(Faction faction, AbilityType abilityType, AbilityModifier modifier) {
        ModifierUtil.checkModifierValueType(modifier);
        Ability ability = faction.getAbility(abilityType);
        if(ability == null)
            throw new NarratorException("This faction doesn't have that ability");
        Modifiers<AbilityModifierName> modifiers = faction.getAbility(abilityType).modifiers;
        modifiers.add(modifier);
    }

    public static void upsertModifier(Faction faction, AbilityType abilityType, AbilityModifierName modifier,
            Object val) {
        upsertModifier(faction, abilityType,
                new AbilityModifier(modifier, new ModifierValue(val), 0, Setup.MAX_PLAYER_COUNT, Instant.now()));
    }

    static void loadFactionModifiers(Setup setup, SetupDbo setupDbo) {
        Map<Long, Map<Long, Set<FactionAbilityModifierSchema>>> abilityModifiers = setupDbo.factionAbilityModifiers;

        Faction faction;
        Map<Long, Set<FactionAbilityModifierSchema>> factionAbilityModifiersMap;
        Set<FactionAbilityModifierSchema> modifiers;
        Ability ability;
        for(long factionID: abilityModifiers.keySet()){
            faction = setup.factions.get(factionID);
            factionAbilityModifiersMap = abilityModifiers.get(factionID);
            for(long abilityID: factionAbilityModifiersMap.keySet()){
                ability = faction.getAbility(abilityID);
                modifiers = factionAbilityModifiersMap.get(abilityID);
                for(FactionAbilityModifierSchema modifier: modifiers)
                    upsertModifier(faction, ability.type, new AbilityModifier(modifier));
            }
        }
    }

}
