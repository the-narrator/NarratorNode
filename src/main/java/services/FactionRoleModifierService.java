package services;

import java.sql.SQLException;
import java.time.Instant;
import java.util.Map;
import java.util.Set;

import game.setups.Setup;
import models.Ability;
import models.FactionRole;
import models.LobbySetupSchema;
import models.ModifierValue;
import models.dbo.SetupDbo;
import models.enums.AbilityModifierName;
import models.enums.AbilityType;
import models.enums.RoleModifierName;
import models.modifiers.AbilityModifier;
import models.modifiers.Modifier;
import models.modifiers.Modifiers;
import models.modifiers.RoleModifier;
import models.schemas.AbilityModifierSchema;
import models.schemas.RoleModifierSchema;
import models.serviceResponse.ModifierServiceResponse;
import repositories.FactionRoleAbilityModifierRepo;
import util.game.ModifierUtil;

public class FactionRoleModifierService {

    static void loadFactionRoleModifiers(Setup setup, SetupDbo setupDbo) {
        Map<Long, Set<RoleModifierSchema>> factionRoleIDToModifiers = setupDbo.factionRoleModifiers;

        FactionRole factionRole;
        for(long factionRoleID: factionRoleIDToModifiers.keySet()){
            factionRole = setup.getFactionRole(factionRoleID);
            for(RoleModifierSchema roleModifierSchema: factionRoleIDToModifiers.get(factionRoleID))
                upsertModifier(factionRole, new RoleModifier(roleModifierSchema));
        }

        Map<Long, Map<Long, Set<AbilityModifierSchema>>> allAbilityModifiers = setupDbo.factionRoleAbilityModifiers;
        Ability ability;
        Map<Long, Set<AbilityModifierSchema>> modifiers;
        for(long factionRoleID: allAbilityModifiers.keySet()){
            factionRole = setup.getFactionRole(factionRoleID);
            modifiers = allAbilityModifiers.get(factionRoleID);
            for(long abilityID: modifiers.keySet()){
                for(AbilityModifierSchema modifierSchema: modifiers.get(abilityID)){
                    ability = factionRole.role.getAbility(abilityID);
                    FactionRoleModifierService.addModifier(factionRole, ability.type,
                            new AbilityModifier(modifierSchema));
                }
            }
        }
    }

    public static ModifierServiceResponse<AbilityModifierName> upsertAbilityModifier(long userID, long factionRoleID,
            long abilityID, Modifier<AbilityModifierName> newModifier) throws SQLException {
        LobbySetupSchema lobbySetup = SetupService.assertModeratorAndOwner(userID);
        FactionRole factionRole = FactionRoleService.get(userID, factionRoleID).factionRole;
        // asserts abilityID exists
        factionRole.role.getAbility(abilityID);

        ModifierUtil.checkModifierValueType(newModifier);
        FactionRoleAbilityModifierRepo.upsertAbilityModifier(factionRoleID, abilityID, newModifier);

        return new ModifierServiceResponse<AbilityModifierName>(lobbySetup.game, newModifier);
    }

    public static void addModifier(FactionRole factionRole, AbilityModifierName name, AbilityType abilityType,
            Object value) {
        addModifier(factionRole, name, abilityType, value, 0, Setup.MAX_PLAYER_COUNT);
    }

    public static void addModifier(FactionRole factionRole, AbilityModifierName modifierName, AbilityType abilityType,
            Object value, int minPlayerCount, int maxPlayerCount) {
        AbilityModifier modifier = new AbilityModifier(modifierName, value, minPlayerCount, maxPlayerCount,
                Instant.now());
        addModifier(factionRole, abilityType, modifier);
    }

    public static void addModifier(FactionRole factionRole, AbilityType abilityType, AbilityModifier modifier) {
        Ability ability = factionRole.role.getAbility(abilityType);
        ModifierUtil.checkModifierValueType(modifier);
        if(ModifierUtil.IsBoolModifier(modifier.name))
            RoleAbilityModifierService.validateAbilityModifierBool(modifier.name, ability);
        else
            RoleAbilityModifierService.validateAbilityModifierInt(modifier.name, ability, modifier.value.getIntValue());
        if(!factionRole.abilityModifiers.containsKey(abilityType))
            factionRole.abilityModifiers.put(abilityType, new Modifiers<>());
        factionRole.abilityModifiers.get(abilityType).add(modifier);
    }

    public static void upsertModifier(FactionRole factionRole, RoleModifierName modifierName, Object value) {
        FactionRoleModifierService.upsertModifier(factionRole,
                new RoleModifier(modifierName, new ModifierValue(value), 0, Setup.MAX_PLAYER_COUNT, Instant.now()));
    }

    public static void upsertAbilityModifier(FactionRole factionRole, AbilityType abilityType,
            Modifier<AbilityModifierName> modifier) {
        if(!factionRole.abilityModifiers.containsKey(abilityType))
            factionRole.abilityModifiers.put(abilityType, new Modifiers<>());
        factionRole.abilityModifiers.get(abilityType).add(modifier);
    }

    public static void upsertModifier(FactionRole factionRole, Modifier<RoleModifierName> modifier) {
        factionRole.roleModifiers.add(modifier);
    }

}
