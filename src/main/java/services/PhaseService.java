package services;

import game.logic.exceptions.NarratorException;
import nnode.Lobby;
import nnode.LobbyManager;
import util.Util;

public class PhaseService {

    public static void setPhaseTimeout(Lobby lobby) {
        lobby.game.phaseHash = Util.getRandomString(12);
    }

    public static void endPhase(String joinID, String phaseHash, double timeLeft) {
        Lobby lobby = LobbyManager.idToLobby.get(joinID);
        if(lobby == null)
            throw new NarratorException("Game not found");
        if(lobby.game.phaseHash.equals(phaseHash))
            lobby.game.endPhase(timeLeft);
        GameService.endGameIfNoParticipatingPlayersAlive(lobby.game);
    }

    public static void checkVotes(String joinID, String phaseHash, double timeLeft) {
        Lobby lobby = LobbyManager.idToLobby.get(joinID);
        if(lobby == null)
            throw new NarratorException("Game not found");
        if(lobby.game.phaseHash.equals(phaseHash))
            lobby.game.checkVote(timeLeft);
    }

}
