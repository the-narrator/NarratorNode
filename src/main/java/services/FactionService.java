package services;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.ExecutionException;

import game.logic.Game;
import game.logic.exceptions.NarratorException;
import game.logic.exceptions.TeamSettingsException;
import game.logic.support.Constants;
import game.setups.Setup;
import models.Ability;
import models.Faction;
import models.LobbySetupSchema;
import models.dbo.SetupDbo;
import models.enums.AbilityModifierName;
import models.modifiers.Modifiers;
import models.requests.AbilityCreateRequest;
import models.requests.FactionCreateRequest;
import models.requests.FactionUpdateRequest;
import models.schemas.FactionSchema;
import models.schemas.SheriffCheckableSchema;
import models.serviceResponse.FactionCreateResponse;
import models.serviceResponse.FactionDeleteResponse;
import repositories.FactionAbilityModifierRepo;
import repositories.FactionAbilityRepo;
import repositories.FactionEnemyRepo;
import repositories.FactionModifierRepo;
import repositories.FactionRepo;
import repositories.SheriffCheckableRepo;
import util.CollectionUtil;
import util.Util;
import util.game.LookupUtil;

public class FactionService {
    // public until we get faction editing in here.

    public static FactionCreateResponse createFaction(long userID, long setupID, FactionCreateRequest request)
            throws SQLException {
        LobbySetupSchema lobbySetup = SetupService.assertModeratorAndOwner(userID);
        if(setupID != lobbySetup.setup.id)
            throw new NarratorException("Setup id does not match the game's setup id.");

        Setup setup = SetupService.getSetup(setupID);
        validateFaction(setup, request.name, request.color);

        long factionID = FactionRepo.insertFaction(setupID, request);
        FactionSchema schema = new FactionSchema(factionID, request);
        Faction faction = createValidatedFaction(setup, schema);
        try{
            FactionModifierRepo.create(Arrays.asList(faction));
        }catch(SQLException e){
            try{
                FactionRepo.delete(setupID, factionID);
            }catch(SQLException f){
                Util.log(f);
            }
            throw e;
        }

        Game game = GameService.getGame(lobbySetup.game.id, setup);
        return new FactionCreateResponse(game, faction, lobbySetup.game.lobbyID);
    }

    // only used by default setup factions like cult, mafia, town
    // deprecated
    @Deprecated
    public static Faction createFaction(Setup setup, String color, String name, String description) {
        return createFaction(setup, new FactionSchema(Util.getID(), color, name, description));
    }

    public static Faction createFaction(Setup setup, FactionSchema schema) {
        validateFaction(setup, schema.name, schema.color);
        return createValidatedFaction(setup, schema);
    }

    private static void validateFaction(Setup setup, String name, String color) {
        checkColor(color);
        Optional<Faction> dupColorFaction = LookupUtil.findFactionByColor(setup, color);
        if(dupColorFaction.isPresent())
            throw new NarratorException("Faction with that color already exists.");
        NamingService.factionNameCheck(name, setup);
    }

    private static Faction createValidatedFaction(Setup setup, FactionSchema schema) {
        Faction faction = new Faction(setup, schema);
        setup.factions.put(schema.id, faction);
        return faction;
    }

    public static void addEnemy(long userID, long factionID, long enemyID) throws SQLException {
        LobbySetupSchema lobbySetup = SetupService.assertModeratorAndOwner(userID);
        assertIDsExistInSetup(lobbySetup.setup.id, factionID, enemyID);
        FactionEnemyRepo.create(lobbySetup.setup.id, factionID, enemyID);
    }

    private static void assertIDsExistInSetup(long setupID, Long... factionIDArray) throws SQLException {
        Set<Long> factionIDs = CollectionUtil.toSet(factionIDArray);
        try{
            for(FactionSchema faction: FactionRepo.getBySetupID(setupID).get()){
                factionIDs.remove(faction.id);
            }
        }catch(InterruptedException | ExecutionException e){
            throw new NarratorException("Unable to fetch setupIDs");
        }
        if(!factionIDs.isEmpty())
            throw new NarratorException("Those faction ids do not belong to the setup.");
    }

    public static void addEnemies(Faction faction, Faction... enemies) {
        addEnemies(faction, CollectionUtil.toSet(enemies));
    }

    public static void addEnemies(Faction faction, Set<Faction> enemies) {
        for(Faction enemy: enemies){
            faction.enemies.add(enemy);
            enemy.enemies.add(faction);
        }
    }

    public static void deleteEnemy(long setupID, long factionID, long enemyFactionID) throws SQLException {
        FactionEnemyRepo.delete(setupID, factionID, enemyFactionID);
    }

    public static void removeEnemy(Faction faction, Faction enemyFaction) {
        faction.enemies.remove(enemyFaction);
    }

    public static void addSheriffCheckable(long userID, long factionID, long checkableID) throws SQLException {
        LobbySetupSchema lobbySetup = SetupService.assertModeratorAndOwner(userID);
        assertIDsExistInSetup(lobbySetup.setup.id, factionID, checkableID);
        SheriffCheckableRepo.create(factionID, checkableID);
    }

    public static void addSheriffCheckable(Faction faction, Faction... detectableFactions) {
        for(Faction detectableFaction: detectableFactions)
            faction.sheriffCheckables.add(detectableFaction);
    }

    public static void removeDetectableFaction(Faction sheriffFaction, Faction uncheckableFaction) {
        sheriffFaction.sheriffCheckables.remove(uncheckableFaction);
    }

    public static void saveFactions(long setupID, ArrayList<Faction> factions) throws SQLException {
        if(factions.isEmpty())
            return;

        ArrayList<Long> factionIDs = new ArrayList<>();
        try{

            // create factions
            ArrayList<FactionCreateRequest> schemas = FactionCreateRequest.fromFactions(factions);
            factionIDs.addAll(FactionRepo.insertFactions(setupID, schemas));
            for(int i = 0; i < factions.size(); i++)
                factions.get(i).id = factionIDs.get(i);

            // add faction modifiers
            FactionModifierRepo.create(factions);

            // add faction abilities
            ArrayList<Ability> factionAbilities = new ArrayList<>();
            ArrayList<AbilityCreateRequest> factionCreateRequests = new ArrayList<>();
            ArrayList<Long> factionAbilityFactionIDs = new ArrayList<>();
            for(Faction faction: factions)
                for(Ability ability: faction.abilities){
                    if(ability.type.dbName != null){
                        factionCreateRequests.add(new AbilityCreateRequest(ability));
                        factionAbilities.add(ability);
                        factionAbilityFactionIDs.add(faction.id);
                    }
                }
            ArrayList<Long> factionAbilityIDs = FactionAbilityRepo.create(factionAbilityFactionIDs,
                    factionCreateRequests);
            for(int i = 0; i < factionAbilities.size(); i++)
                factionAbilities.get(i).id = factionAbilityIDs.get(i);

            // add faction ability modifiers
            ArrayList<Modifiers<AbilityModifierName>> modifiersList = new ArrayList<>();
            for(Faction faction: factions)
                if(faction.abilities != null)
                    for(Ability ability: faction.abilities)
                        if(ability.type.dbName != null)
                            modifiersList.add(ability.modifiers);
            FactionAbilityModifierRepo.create(factionAbilityIDs, modifiersList);
        }catch(SQLException | IndexOutOfBoundsException e){
            try{
                FactionRepo.deleteByIDs(setupID, factionIDs);
            }catch(SQLException f){
                Util.log(f, "Failed to cleanup factions after a bad insert.");
            }
            throw e;
        }
    }

    public static void loadFactions(Setup setup, SetupDbo setupDbo) {
        for(FactionSchema faction: setupDbo.factionDbos)
            createFaction(setup, faction);
        FactionModifierService.loadModifiers(setup, setupDbo);
        FactionAbilityService.loadFactionAbilities(setup, setupDbo);
        loadSheriffCheckables(setup, setupDbo);
        loadEnemies(setup, setupDbo);
    }

    private static void loadSheriffCheckables(Setup setup, SetupDbo setupDbo) {
        ArrayList<SheriffCheckableSchema> factionEnemies = setupDbo.sheriffCheckables;
        Faction sheriffFaction, checkableFaction;
        for(SheriffCheckableSchema schema: factionEnemies){
            sheriffFaction = setup.getFactionByColor(schema.sheriffColor);
            checkableFaction = setup.getFactionByColor(schema.detectableColor);
            addSheriffCheckable(sheriffFaction, checkableFaction);
        }
    }

    private static void loadEnemies(Setup setup, SetupDbo setupDbo) {
        ArrayList<String[]> factionEnemies = setupDbo.factionEnemies;
        Faction faction, enemyFaction;
        for(String[] enemyPairing: factionEnemies){
            faction = setup.getFactionByColor(enemyPairing[0]);
            enemyFaction = setup.getFactionByColor(enemyPairing[1]);
            FactionService.addEnemies(faction, enemyFaction);
        }
    }

    public static void update(long setupID, long factionID, FactionUpdateRequest request) throws SQLException {
        Setup setup = SetupService.getSetup(setupID);
        Faction faction = setup.factions.get(factionID);
        if(faction == null)
            throw new NarratorException("Faction not found.");

        nameValidation(faction.setup, request.name, faction);
        FactionRepo.update(faction.id, request);

        update(faction, request);
    }

    public static void update(Faction faction, FactionUpdateRequest request) {
        nameValidation(faction.setup, request.name, faction);
        faction.name = request.name;
        faction.description = request.description;
    }

    public static FactionDeleteResponse deleteFaction(long userID, long factionID) throws SQLException {
        LobbySetupSchema lobbySetup = SetupService.assertModeratorAndOwner(userID);
        long setupID = lobbySetup.setup.id;
        FactionRepo.delete(setupID, factionID);
        return new FactionDeleteResponse(setupID, lobbySetup.game.lobbyID);
    }

    private static void checkColor(String color) {
        if(color == null)
            throw new TeamSettingsException("Color cannot be null");
        color = color.toUpperCase();
        if(!color.startsWith("#") || color.length() != 7)
            throw new TeamSettingsException("Team color must look like #A0B1C2");
        if(!color.matches("#[0-9A-F]+"))
            throw new TeamSettingsException("Color can only contain hex characters");
        if(Constants.A_RANDOM.equals(color) || Constants.A_SKIP.equals(color))
            throw new TeamSettingsException("This is a reserved color.");
    }

    public static void nameValidation(Setup setup, String name, Faction editedFaction) {
        if(editedFaction.name.equals(name))
            return;
        NamingService.factionNameCheck(name, setup);
    }
}
