package services;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import game.abilities.FactionSend;
import game.abilities.GameAbility;
import game.abilities.LastWill;
import game.abilities.Vote;
import game.event.Message;
import game.logic.Game;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.exceptions.NarratorException;
import game.logic.support.Random;
import game.logic.support.action.Action;
import json.JSONException;
import models.enums.AbilityType;
import models.enums.GamePhase;
import models.enums.SetupModifierName;
import models.idtypes.GameID;
import models.schemas.PlayerSchema;
import models.serviceResponse.AbilityMetadata;
import models.serviceResponse.ActionResponse;
import nnode.Lobby;
import nnode.Permission;
import nnode.StateObject;
import repositories.GameUserRepo;
import repositories.PlayerRepo;
import util.Util;

public class ActionService {

    public static Set<AbilityMetadata> getAbilityMetadata(long userID, GameID gameID) throws SQLException {
        Set<AbilityMetadata> abilities = new HashSet<>();

        Optional<PlayerSchema> playerSchema = PlayerRepo.getByUserIDGameID(userID, gameID);
        Game game = GameService.getByGameID(gameID);
        Random random = new Random();

        if(playerSchema.isPresent() && game.isStarted()){
            Player player = game.players.getByID(playerSchema.get().id);
            if(player.isParticipating()){
                if(game.phase == GamePhase.NIGHT_ACTION_SUBMISSION)
                    abilities.add(AbilityMetadata.getEndNight());
                if(game.getBool(SetupModifierName.LAST_WILL))
                    abilities.add(LastWill.getMetadata());
            }
            for(GameAbility ability: player.getActionableAbilities()){
                abilities.add(ability.getMetadata(player, random));
                if(ability.getAbilityType() == Vote.abilityType){
                    if(game.getBool(SetupModifierName.SKIP_VOTE))
                        abilities.add(Vote.getSkipMetadata());
                    abilities.add(Vote.getUnvoteMetadata());
                }
            }
        }

        boolean isModerator = GameUserRepo.getModeratorIDs(gameID).contains(userID);
        if(isModerator){
            String exampleName = game.getLivePlayers().getRandom(random).getName();
            abilities.add(AbilityMetadata.getModkill(exampleName));
            abilities.add(AbilityMetadata.endPhase());
        }

        return abilities;
    }

    public static ActionResponse submitAction(long userID, String message, double timeLeft)
            throws JSONException, SQLException {
        Optional<PlayerSchema> playerSchema = PlayerRepo.getActiveByUserID(userID);

        GameID gameID;
        if(playerSchema.isPresent())
            gameID = playerSchema.get().gameID;
        else{
            Optional<GameID> optGameID = GameUserRepo.getActiveGameIDByUserID(userID);
            if(!optGameID.isPresent())
                throw new NarratorException("User may not submit actions");
            gameID = optGameID.get();
        }
        Lobby lobby = LobbyService.getByGameID(gameID);
        Game game = lobby.game;

        Set<Long> moderatorIDs = ModeratorService.getModeratorIDs(gameID);
        boolean isModerator = moderatorIDs.contains(userID);

        Player player;
        if(playerSchema.isPresent())
            player = game.players.getDatabaseMap().get(playerSchema.get().id);
        else
            player = null;

        boolean modSubmission = isModerator || lobby.hasPermission(userID, Permission.GAME_EDITING);
        Message actionConfirmationMessage = lobby.ch.command(player, message, "", lobby.nodeSwitchLookup, modSubmission,
                timeLeft);
        GameService.endGameIfNoParticipatingPlayersAlive(game);
        return getActionResponse(Optional.ofNullable(player), game, Optional.ofNullable(actionConfirmationMessage));
    }

    public static ActionResponse submitAction(long userID, AbilityType abilityType, List<String> targets,
            List<String> args, double timeLeft, Optional<Integer> replacingActionIndex)
            throws JSONException, SQLException {
        GameID gameID = LobbyService.assertInLobby(userID);
        Player player = PlayerService.assertUserInStartedGame(userID);
        Game game = GameService.getByGameID(gameID);
        PlayerList pList = new PlayerList();
        for(String playerName: targets)
            pList.add(game.getPlayerByName(playerName));

        Action action = new Action(player, abilityType, timeLeft, args, pList);
        if(replacingActionIndex.isPresent()){
            replaceAction(action, replacingActionIndex.get());
        }else{
            if(game.isDay() && player.hasDayAction(abilityType))
                player.doDayAction(action);
            else
                player.setNightTarget(action);
        }
        GameService.endGameIfNoParticipatingPlayersAlive(game);
        return getActionResponse(Optional.of(player), game, Optional.empty());
    }

    public static void replaceAction(Action newAction, int actionIndex) {
        Player player = newAction.owner;
        Action a = player.getActions().getActions().get(actionIndex);
        player.getActions().replace(a, newAction);
    }

    public static void deleteAction(long userID, AbilityType abilityType, int actionIndex, double timeLeft)
            throws SQLException {
        Player player = PlayerService.assertUserInStartedGame(userID);
        if(abilityType == FactionSend.abilityType)
            player.cancelTarget(FactionSend.abilityType, timeLeft);
        else
            player.cancelAction(actionIndex, timeLeft);
        GameService.endGameIfNoParticipatingPlayersAlive(player.game);
    }

    private static ActionResponse getActionResponse(Optional<Player> player, Game game,
            Optional<Message> newActionConfirmMessage) throws JSONException {
        ActionResponse actionResponse = new ActionResponse();
        if(game.phase == GamePhase.VOTE_PHASE)
            actionResponse.voteInfo = VoteService.getVotes(game);

        actionResponse.actions = StateObject.getStateObjectActions(player);
        actionResponse.newActionConfirmMessage = newActionConfirmMessage;
        return actionResponse;
    }

    public static void recordCommand(Player p, double timeLeft, ArrayList<String> commandParts) {
        String[] commandArr = Util.toStringArray(commandParts);
        p.game.getEventManager().addCommand(p, timeLeft, commandArr);
    }

}
