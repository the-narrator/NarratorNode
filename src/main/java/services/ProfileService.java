package services;

import java.sql.SQLException;
import java.util.Optional;

import game.logic.Player;
import models.User;
import models.idtypes.GameID;
import models.schemas.GameOverviewSchema;
import models.schemas.GameUserSchema;
import models.schemas.PlayerSchema;
import models.serviceResponse.ProfileResponse;
import repositories.GameRepo;
import repositories.GameUserRepo;
import repositories.PlayerRepo;
import repositories.UserRepo;
import util.PlayerUtil;

public class ProfileService {
    public static ProfileResponse getProfile(long userID) throws SQLException {
        String userName = UserRepo.getName(userID);
        User user = new User(userID, userName);
        Optional<GameUserSchema> userOpt = GameUserRepo.getActiveByUserID(userID);
        if(!userOpt.isPresent())
            return new ProfileResponse(Optional.empty(), user, Optional.empty(), Optional.empty(), Optional.empty());

        GameID gameID = userOpt.get().gameID;
        GameOverviewSchema game = GameRepo.getOverview(gameID);
        Optional<PlayerSchema> playerSchema = PlayerRepo.getActiveByUserID(userID);
        if(game.isInProgress()){
            Optional<Player> player = PlayerService.getByUserID(playerSchema);
            return new ProfileResponse(Optional.of(gameID), user, PlayerUtil.getDBID(player),
                    PlayerUtil.getName(playerSchema), player);
        }

        return new ProfileResponse(Optional.of(gameID), user, PlayerUtil.getDBIDFromSchema(playerSchema),
                PlayerUtil.getName(playerSchema), Optional.empty());
    }
}
