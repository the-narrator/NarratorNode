package services;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Optional;
import java.util.Set;

import game.abilities.Hidden;
import game.logic.Game;
import game.logic.exceptions.NarratorException;
import game.setups.Setup;
import models.Faction;
import models.FactionRole;
import models.LobbySetupSchema;
import models.Role;
import models.dbo.SetupDbo;
import models.enums.AbilityModifierName;
import models.enums.AbilityType;
import models.enums.RoleModifierName;
import models.idtypes.GameID;
import models.modifiers.Modifier;
import models.modifiers.Modifiers;
import models.schemas.FactionRoleSchema;
import models.schemas.GameOverviewSchema;
import models.serviceResponse.FactionRoleCreateResponse;
import models.serviceResponse.FactionRoleGetResponse;
import models.serviceResponse.ModifierServiceResponse;
import repositories.FactionRoleAbilityModifierRepo;
import repositories.FactionRoleModifierRepo;
import repositories.FactionRoleRepo;
import repositories.GameRepo;
import repositories.PlayerRepo;
import util.Util;
import util.game.ModifierUtil;

public class FactionRoleService {

    public static void saveFactionRoles(long setupID, ArrayList<Faction> factions) throws SQLException {
        ArrayList<Long> factionRoleIDs = new ArrayList<>();

        try{
            // insert faction roles
            ArrayList<FactionRole> factionRoles = new ArrayList<>();
            for(Faction faction: factions)
                factionRoles.addAll(faction.roleMap.values());
            factionRoleIDs.addAll(FactionRoleRepo.create(setupID, factionRoles));
            for(int i = 0; i < factionRoles.size(); i++){
                FactionRole factionRole = factionRoles.get(i);
                factionRole.id = factionRoleIDs.get(i);
            }

            // save roleReceived
            for(FactionRole factionRole: factionRoles)
                if(factionRole.receivedRole != null)
                    FactionRoleRepo.updateRoleReceived(setupID, factionRole.id, factionRole.receivedRole.id);

            // insert faction role role modifiers
            FactionRoleModifierRepo.createRoleModifiers(factionRoles);

            // insert faction role ability modifiers
            ArrayList<Long> roleAbilityIDs = new ArrayList<>();
            ArrayList<Modifiers<AbilityModifierName>> modifierNamesList = new ArrayList<>();
            ArrayList<Long> factionRoleIDsForModifier = new ArrayList<>();
            for(FactionRole factionRole: factionRoles)
                for(AbilityType abilityType: factionRole.abilityModifiers.keySet()){
                    roleAbilityIDs.add(factionRole.role.getAbility(abilityType).id);
                    factionRoleIDsForModifier.add(factionRole.id);
                    modifierNamesList.add(factionRole.abilityModifiers.get(abilityType));
                }
            FactionRoleAbilityModifierRepo.create(factionRoleIDsForModifier, roleAbilityIDs, modifierNamesList);
        }catch(SQLException | IndexOutOfBoundsException e){
            try{
                FactionRoleRepo.deleteByIDs(setupID, factionRoleIDs);
            }catch(SQLException f){
                Util.log(f, "Failed to clean up faction roles after bad inserts.");
            }
            throw e;
        }
    }

    public static FactionRoleGetResponse get(long userId, long factionRoleID) throws SQLException {
        GameID gameID = LobbyService.assertInLobby(userId);
        GameOverviewSchema gameOverview = GameRepo.getOverview(gameID);

        Setup setup;
        Optional<Game> optGame;
        int playerCount;
        if(gameOverview.isInProgress()){
            Game game = GameService.getByGameID(gameID);
            setup = game.setup;
            optGame = Optional.of(game);
            playerCount = game.players.size();
        }else{
            setup = SetupService.getSetup(gameOverview.setupID);
            optGame = Optional.empty();
            playerCount = PlayerRepo.getByGameID(gameID).size();
        }
        FactionRole factionRole = setup.getFactionRole(factionRoleID);
        return new FactionRoleGetResponse(factionRole, optGame, playerCount);
    }

    public static void setRoleReceive(long userID, long factionRoleID, Long receivedFactionRoleID) throws SQLException {
        LobbySetupSchema lobbySetup = SetupService.assertModeratorAndOwner(userID);
        FactionRole factionRole = get(userID, factionRoleID).factionRole;
        FactionRole receivedFactionRole = receivedFactionRoleID == null ? null
                : get(userID, receivedFactionRoleID).factionRole;

        long setupID = lobbySetup.game.setupID;
        if(receivedFactionRole == null)
            FactionRoleRepo.removeRoleReceived(setupID, factionRoleID);
        else
            FactionRoleRepo.updateRoleReceived(setupID, factionRoleID, receivedFactionRoleID);

        setRoleReceive(factionRole, receivedFactionRole);
    }

    // assumes that factionRole and receivedRole are properly added into the setup
    public static void setRoleReceive(FactionRole factionRole, FactionRole receivedRole) {
        factionRole.receivedRole = receivedRole;
    }

    public static ModifierServiceResponse<RoleModifierName> upsertModifier(long userID, long factionRoleID,
            Modifier<RoleModifierName> newModifier) throws SQLException {
        LobbySetupSchema lobbySetup = SetupService.assertModeratorAndOwner(userID);
        FactionRole factionRole = get(userID, factionRoleID).factionRole;

        ModifierUtil.checkModifierValueType(newModifier);

        FactionRoleModifierRepo.upsertRoleModifier(factionRoleID, newModifier);
        FactionRoleModifierService.upsertModifier(factionRole, newModifier);

        return new ModifierServiceResponse<>(lobbySetup.game, newModifier);
    }

    public static void deleteRoleReceived(long userID, long factionRoleID) throws SQLException {
        LobbySetupSchema lobbySetup = SetupService.assertModeratorAndOwner(userID);
        FactionRoleRepo.deleteSetRoleReceive(factionRoleID, lobbySetup.setup.id);
    }

    public static void deleteRoleReceived(FactionRole factionRole) {
        factionRole.receivedRole = null;
    }

    public static FactionRoleCreateResponse createFactionRole(long userID, long factionID, long roleID)
            throws SQLException {
        LobbySetupSchema lobbySetup = SetupService.assertModeratorAndOwner(userID);
        Setup setup = SetupService.getSetup(lobbySetup.setup.id);
        Role role = setup.getRole(roleID);

        Faction faction = setup.getFaction(factionID);
        Game game = GameService.getGame(lobbySetup.game.id, setup);
        if(faction.roleMap.containsKey(role))
            return new FactionRoleCreateResponse(faction.roleMap.get(role), game);

        long factionRoleID = FactionRoleRepo.create(roleID, factionID, setup.id);

        FactionRole factionRole = createValidatedFactionRole(factionRoleID, faction, role);
        return new FactionRoleCreateResponse(factionRole, game);
    }

    public static FactionRole createFactionRole(Faction faction, Role role) {
        if(!faction.setup.roles.contains(role))
            throw new NarratorException("Role does not exist");
        return createValidatedFactionRole(Util.getID(), faction, role);
    }

    private static FactionRole createValidatedFactionRole(long factionRoleID, Faction faction, Role role) {
        FactionRole factionRole = new FactionRole(factionRoleID, faction, role);
        faction.roleMap.put(role, factionRole);
        return factionRole;
    }

    public static void loadFactionRoles(Setup setup, SetupDbo setupDbo) {
        Set<FactionRoleSchema> factionRoles = setupDbo.factionRoleDbos;
        Faction faction;
        Role role;
        for(FactionRoleSchema schema: factionRoles){
            faction = setup.factions.get(schema.factionID);
            role = setup.getRole(schema.roleID);
            createValidatedFactionRole(schema.id, faction, role);
        }

        FactionRole factionRole;
        FactionRole receivedRole;
        for(FactionRoleSchema schema: factionRoles){
            if(!schema.receivedFactionRoleID.isPresent())
                continue;
            factionRole = setup.getFactionRole(schema.id);
            receivedRole = setup.getFactionRole(schema.receivedFactionRoleID.get());
            setRoleReceive(factionRole, receivedRole);
        }

        FactionRoleModifierService.loadFactionRoleModifiers(setup, setupDbo);
    }

    public static void deleteFactionRole(long userID, long factionRoleID) throws SQLException {
        LobbySetupSchema lobbySetup = SetupService.assertModeratorAndOwner(userID);
        deleteRoleReceived(userID, factionRoleID);
        FactionRoleRepo.delete(lobbySetup.setup.id, factionRoleID);
    }

    public static void deleteFactionRole(FactionRole factionRole) {
        for(Hidden hidden: factionRole.faction.setup.hiddens)
            HiddenSpawnService.deleteHiddenSpawn(hidden, factionRole);
        factionRole.faction.roleMap.remove(factionRole.role);
    }
}
