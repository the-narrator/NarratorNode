package services;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;

import game.event.ChatMessage;
import game.event.EventList;
import game.event.Message;
import game.logic.Game;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.exceptions.NarratorException;
import json.JSONArray;
import json.JSONException;
import json.JSONObject;
import models.PlayerUserIDMap;
import models.idtypes.GameID;
import models.schemas.PlayerSchema;
import nnode.Lobby;
import nnode.StateObject;
import nnode.UnreadManager;
import nnode.WebCommunicator;
import repositories.GameUserRepo;
import repositories.PlayerRepo;
import util.Util;

public class ChatService {

    private static HashMap<GameID, HashMap<String, UnreadManager>> gameUnreads = new HashMap<>();

    public static JSONObject getUserChats(long userID, GameID gameID) throws SQLException {
        JSONObject jo = new JSONObject();

        Game game;
        try{
            game = GameService.getByGameID(gameID);
        }catch(NarratorException e){
            return new JSONObject();
        }
        Optional<Player> optPlayer = Optional.empty();
        for(PlayerSchema player: PlayerRepo.getByGameID(gameID)){
            if(player.userID.isPresent() && player.userID.get().equals(userID))
                optPlayer = Optional.of(game.players.getByID(player.id));
        }

        JSONArray chatLog = new JSONArray();
        EventList eList;
        String key;
        if(!optPlayer.isPresent())
            return jo;

        Player player = optPlayer.get();
        if(player.isEliminated()){
            key = Message.PRIVATE;
            eList = player.getEvents();
        }else{
            key = player.getName();
            eList = player.getEvents();
        }

        PlayerUserIDMap playerUserMap = UserService.getPlayerUserMap(player.game);

        try{
            for(Message e: eList)
                Lobby.AppendMessage(chatLog, e, player, key, getSpeakingUser(playerUserMap, e));

            jo.put(StateObject.message, chatLog);
            jo.put(StateObject.chatReset, StateObject.getChatKeys(player.game, player));
            return jo;
        }catch(JSONException e){
            Util.log(e, "Failed to create chat object for user.");
            return null;
        }
    }

    private static Optional<Long> getSpeakingUser(PlayerUserIDMap playerUserMap, Message message) {
        if(message instanceof ChatMessage){
            ChatMessage cm = (ChatMessage) message;
            return playerUserMap.getUserID(cm.speaker);
        }
        return Optional.empty();
    }

    public static void submit(long userID, String chatKey, String messageText, Optional<String> source, JSONObject args)
            throws SQLException {
        Optional<Player> player = PlayerService.getByUserID(userID);
        if(!player.isPresent())
            throw new NarratorException("Only players may submit actions.");

        submit(player.get(), chatKey, messageText, source, args);
    }

    public static void submit(Player player, String chatKey, String messageText){
        submit(player, chatKey, messageText, Optional.empty(), new JSONObject());
    }

    public static void submit(Player player, String chatKey, String messageText, Optional<String> source, JSONObject args) {
        player.say(messageText, chatKey, source, args);
    }

    public static void removeLog(GameID gameID, String logID) {
        Map<String, UnreadManager> map = gameUnreads.get(gameID);
        if(map == null)
            return;
        map.remove(logID);
    }

    public static UnreadManager getUnreadManager(GameID gameID, String logID, PlayerList players) throws SQLException {
        if(!gameUnreads.containsKey(gameID))
            gameUnreads.put(gameID, new HashMap<>());
        Map<String, UnreadManager> map = gameUnreads.get(gameID);
        UnreadManager manager = map.get(logID);
        if(manager != null)
            return manager;
        manager = new UnreadManager(logID, UserService.getUserIDs(players));
        map.put(logID, manager);
        return manager;
    }

    public static void pushUnreads(long userID) throws SQLException {
        Optional<GameID> gameID = GameUserRepo.getActiveGameIDByUserID(userID);
        if(gameID.isPresent())
            pushUnreads(gameID.get(), userID);
    }

    public static void pushUnreads(GameID gameID, long userID) {
        Map<String, UnreadManager> map = gameUnreads.get(gameID);
        if(map == null)
            return;
        UnreadManager alreadyRead;
        for(String chatName: map.keySet()){
            alreadyRead = map.get(chatName);
            if(alreadyRead.getUserIDs().contains(userID))
                alreadyRead.sendUnread(userID);
        }
    }

    public static void pushAllChats(long userID) throws SQLException, JSONException {
        Optional<GameID> gameID = GameUserRepo.getActiveGameIDByUserID(userID);
        if(gameID.isPresent())
            pushAllChats(userID, gameID.get());
    }

    public static void pushAllChats(long userID, GameID gameID) throws SQLException, JSONException {
        JSONObject jo = ChatService.getUserChats(userID, gameID);
        WebCommunicator.pushToUser(jo, userID);
    }

    public static void setRead(GameID gameID, long userID, String logID) {
        Map<String, UnreadManager> map = gameUnreads.get(gameID);
        if(map == null)
            return;
        UnreadManager manager = map.get(logID);
        if(manager != null)
            manager.setRead(userID);
    }

    public static void deleteUserFromGame(GameID gameID, long userID) {
        Map<String, UnreadManager> map = gameUnreads.get(gameID);
        if(map == null)
            return;
        for(UnreadManager manager: map.values()){
            manager.remove(userID);
        }
    }

    public static void addUserToGame(GameID gameID, long userID, String logID) {
        if(!gameUnreads.containsKey(gameID))
            gameUnreads.put(gameID, new HashMap<>());
        Map<String, UnreadManager> map = gameUnreads.get(gameID);
        if(!map.containsKey(logID))
            map.put(logID, new UnreadManager(logID, new HashSet<>()));
        UnreadManager manager = map.get(logID);
        manager.addUser(userID);
    }

    public static void deleteUnreads(GameID gameID) {
        gameUnreads.remove(gameID);
    }
}
