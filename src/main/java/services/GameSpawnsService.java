package services;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import game.abilities.Citizen;
import game.abilities.Hidden;
import game.abilities.Thief;
import game.logic.Game;
import game.logic.Player;
import game.logic.exceptions.IllegalRoleCombinationException;
import game.logic.exceptions.InvalidSetupHiddenCombination;
import game.logic.exceptions.NarratorException;
import game.logic.support.Random;
import game.logic.support.RolePackage;
import models.Faction;
import models.FactionRole;
import models.GeneratedRole;
import models.InvalidSetupHiddensReason;
import models.SetupHidden;
import models.enums.InvalidSetupHiddensCode;
import models.enums.SetupModifierName;
import models.idtypes.GameID;
import repositories.GameSpawnsRepo;

public class GameSpawnsService {

    public static ArrayList<GeneratedRole> pickRoles(Game game, List<SetupHidden> setupHiddens) {
        if(setupHiddens.isEmpty())
            throw new NarratorException("Cannot pick roles if input list is empty.");
        ArrayList<GeneratedRole> generatedRoles = new ArrayList<>();
        for(SetupHidden setupHidden: setupHiddens)
            generatedRoles.add(generateRole(setupHidden, game));

        Collections.sort(generatedRoles, new Comparator<GeneratedRole>() {
            @Override
            public int compare(GeneratedRole o2, GeneratedRole o1) {
                return o1.factionRoles.size() - o2.factionRoles.size();
            }
        });

        Set<InvalidSetupHiddensCode> codes = new HashSet<>();
        InvalidSetupHiddensReason reason;
        while (true){
            reason = isValid(game.players.size(), generatedRoles, new LinkedList<>());
            if(reason == null)
                break;
            codes.add(reason.code);
            if(reason.resetIndex == null){
                if(!hasMoreOptionsAfterShuffle(generatedRoles))
                    throw new InvalidSetupHiddenCombination(codes);
            }else{
                if(!hasMoreOptionsAfterSelectedUptick(generatedRoles, reason.resetIndex))
                    throw new InvalidSetupHiddenCombination(codes);
            }
        }

        Random random = game.getRandom();
        random.shuffle(generatedRoles, "shuffling order of generated roles validation");

        return generatedRoles;
    }

    public static ArrayList<GeneratedRole> getPassOutRoles(int playerCount, ArrayList<GeneratedRole> generatedRoles) {
        boolean[] shouldInclude = new boolean[generatedRoles.size()];
        for(int i = 0; i < generatedRoles.size(); ++i)
            shouldInclude[i] = i < playerCount;

        InvalidSetupHiddensReason reason = null;
        ArrayList<GeneratedRole> roleSubset;
        ArrayList<GeneratedRole> thiefRoles;
        Set<InvalidSetupHiddensCode> codes = new HashSet<>();
        while (shouldInclude != null){
            roleSubset = getPassOutSubset(shouldInclude, generatedRoles);
            thiefRoles = getUnpickedRoles(generatedRoles, roleSubset);
            reason = isValid(playerCount, roleSubset, thiefRoles);
            if(reason == null)
                return roleSubset;
            codes.add(reason.code);

            shouldInclude = decrementPassOutTracker(shouldInclude);
        }
        throw new InvalidSetupHiddenCombination(codes);
    }

    public static InvalidSetupHiddensReason isValid(int playerCount, ArrayList<GeneratedRole> generatedRoles,
            List<GeneratedRole> thiefRoles) {
        int tickIndex = SetupValidationService.allMustSpawnedRolesSpawned(thiefRoles);
        if(tickIndex != -1){
            InvalidSetupHiddensReason reason = new InvalidSetupHiddensReason(
                    InvalidSetupHiddensCode.MUST_SPAWNS_NOT_SPAWNED);
            reason.resetIndex = tickIndex;
            return reason;
        }

        if(!hasOpposingWinConditions(generatedRoles, thiefRoles))
            return new InvalidSetupHiddensReason(InvalidSetupHiddensCode.NO_OPPOSING_FACTIONS);

        tickIndex = SetupValidationService.hasMultipleUniques(generatedRoles, playerCount);
        if(tickIndex != -1){
            InvalidSetupHiddensReason reason = new InvalidSetupHiddensReason(InvalidSetupHiddensCode.UNIQUES_CLASHING);
            reason.resetIndex = tickIndex;
            return reason;
        }

        tickIndex = SetupValidationService.hasSingleMason(playerCount, generatedRoles, thiefRoles);
        if(tickIndex != -1){
            InvalidSetupHiddensReason reason = new InvalidSetupHiddensReason(
                    InvalidSetupHiddensCode.SINGLE_SPAWNING_MASONS);
            reason.resetIndex = tickIndex;
            return reason;
        }

        tickIndex = SetupValidationService.hasThiefWithNotEnoughChoices(playerCount, generatedRoles, thiefRoles);
        if(tickIndex != -1){
            InvalidSetupHiddensReason reason = new InvalidSetupHiddensReason(
                    InvalidSetupHiddensCode.NOT_ENOUGH_ROLES_WITH_THIEF);
            reason.resetIndex = tickIndex;
            return reason;
        }
        return null;
    }

    private static boolean hasOpposingWinConditions(ArrayList<GeneratedRole> generatedRoles,
            List<GeneratedRole> thiefRoles) {
        Set<String> thiefColors = new HashSet<>();
        if(!thiefRoles.isEmpty())
            for(GeneratedRole role: thiefRoles)
                thiefColors.add(role.getFactionRole().getColor());
        Optional<String> opposingThief = thiefColors.size() == 1 ? Optional.of(thiefColors.iterator().next())
                : Optional.empty();
        for(GeneratedRole role1: generatedRoles){
            for(GeneratedRole role2: generatedRoles){
                if(hasOppositionBetweenRoles(role1, role2, opposingThief))
                    return true;
            }
        }
        return false;
    }

    private static boolean hasOppositionBetweenRoles(GeneratedRole role1, GeneratedRole role2, Optional<String> color) {
        if(role1 == role2)
            return false;
        FactionRole factionRole1 = role1.getFactionRole();
        FactionRole factionRole2 = role2.getFactionRole();

        if(color.isPresent()){
            Faction faction = factionRole1.faction.setup.getFactionByColor(color.get());
            if(factionRole1.role.hasAbility(Thief.abilityType))
                return factionRole2.faction.enemies.contains(faction);
            if(factionRole2.role.hasAbility(Thief.abilityType))
                return factionRole1.faction.enemies.contains(faction);
        }
        return factionRole1.faction.enemies.contains(factionRole2.faction);
    }

    private static ArrayList<GeneratedRole> getUnpickedRoles(ArrayList<GeneratedRole> generatedRoles,
            ArrayList<GeneratedRole> assignableRoles) {
        generatedRoles = new ArrayList<>(generatedRoles);
        for(GeneratedRole generatedRole: assignableRoles)
            generatedRoles.remove(generatedRole);

        return generatedRoles;
    }

    public static boolean[] decrementPassOutTracker(boolean[] arr) {
        boolean[] result = new boolean[arr.length];

        boolean found0 = false;
        boolean madeSwitch = false;
        int oneCount = 0;
        int i;
        for(i = arr.length - 1; i >= 0; --i){
            if(!found0){
                if(!arr[i])
                    found0 = true;
                else
                    oneCount++;
                continue;
            }
            if(!arr[i])
                continue;
            result[i] = false;
            oneCount++;
            madeSwitch = true;
            for(int j = i + 1; j < arr.length; ++j){
                result[j] = oneCount > 0;
                oneCount--;
            }
            break;
        }
        if(!madeSwitch)
            return null;
        for(int j = i - 1; j >= 0; --j)
            result[j] = arr[j];
        return result;
    }

    private static boolean hasMoreOptionsAfterShuffle(ArrayList<GeneratedRole> generatedRoles) {
        GeneratedRole generatedRole;
        boolean hasMoreOptions = false;
        for(int i = 0; i < generatedRoles.size(); ++i){
            generatedRole = generatedRoles.get(i);
            if(generatedRole.isAtEndOfOptions())
                continue;
            ++generatedRole.index;
            for(int j = 0; j != i; ++j)
                generatedRoles.get(j).index = 0;
            hasMoreOptions = true;
            break;
        }
        return hasMoreOptions;
    }

    private static boolean hasMoreOptionsAfterSelectedUptick(ArrayList<GeneratedRole> generatedRoles, int resetIndex) {
        GeneratedRole generatedRole;
        for(int i = resetIndex; i < generatedRoles.size(); ++i){
            generatedRole = generatedRoles.get(i);
            if(generatedRole.isAtEndOfOptions())
                continue;
            generatedRole.index++;
            for(int j = 0; j < i; j++)
                generatedRoles.get(j).index = 0;
            return true;
        }
        return false;
    }

    private static GeneratedRole generateRole(SetupHidden setupHidden, Game game) {
        Hidden hidden = setupHidden.hidden;
        Set<FactionRole> spawningFactionRoles = hidden.getSpawningFactionRoles(game);
        if(spawningFactionRoles.isEmpty())
            throw new IllegalRoleCombinationException("No roles in this random slot");
        GeneratedRole spawn = getSpawn(setupHidden);
        if(spawn != null)
            return spawn;

        List<FactionRole> rList = new ArrayList<>();
        List<FactionRole> cits = new ArrayList<>();

        for(FactionRole factionRole: spawningFactionRoles){
            if(Citizen.hasNoAbilities(factionRole))
                cits.add(factionRole);
            else
                rList.add(factionRole);
        }

        Random random = game.getRandom();
        random.shuffle(rList, "setup hidden member shuffling");
        random.shuffle(cits, "setup hidden cit shuffling");

        GeneratedRole generatedRole = new GeneratedRole(setupHidden);

        int citChance = setupHidden.setup.modifiers.getInt(SetupModifierName.CIT_RATIO, game.players.size());
        boolean shouldGenerateCit = !cits.isEmpty() && random.nextInt(100) < citChance;
        if(shouldGenerateCit || rList.isEmpty()){
            generatedRole.factionRoles.addAll(cits);
            generatedRole.factionRoles.addAll(rList);
        }else{
            generatedRole.factionRoles.addAll(rList);
            generatedRole.factionRoles.addAll(cits);
        }

        return generatedRole;
    }

    private static GeneratedRole getSpawn(SetupHidden setupHidden) {
        GeneratedRole generatedRole = new GeneratedRole(setupHidden);
        if(setupHidden.spawn == null)
            return null;
        generatedRole.factionRoles.add(setupHidden.spawn);
        return generatedRole;
    }

    private static ArrayList<GeneratedRole> getPassOutSubset(boolean[] shouldInclude,
            ArrayList<GeneratedRole> generatedRoles) {
        ArrayList<GeneratedRole> list = new ArrayList<>();
        for(int i = 0; i < shouldInclude.length; i++)
            if(shouldInclude[i])
                list.add(generatedRoles.get(i));
        return list;
    }

    public static Map<Long, Long> getByGameID(GameID gameID) throws SQLException {
        return GameSpawnsRepo.getByGameID(gameID);
    }

    public static void saveSpawns(GameID id, Game narrator) throws SQLException {
        Map<Long, Long> spawns = new HashMap<>();
        for(RolePackage setupHidden: narrator.unpickedRoles)
            spawns.put(setupHidden.assignedSetupHidden.id, setupHidden.assignedRole.id);

        for(Player player: narrator.players)
            spawns.put(player.initialAssignedRole.assignedSetupHidden.id, player.initialAssignedRole.assignedRole.id);

        GameSpawnsRepo.create(id, spawns);
    }

}
