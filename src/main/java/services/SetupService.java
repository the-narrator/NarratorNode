package services;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import game.logic.Game;
import game.logic.RolesList;
import game.logic.exceptions.NarratorException;
import game.logic.support.RoleAssigner;
import game.setups.Default;
import game.setups.Setup;
import models.Faction;
import models.GeneratedRole;
import models.LobbySetupSchema;
import models.OpenLobby;
import models.SetupHidden;
import models.dbo.SetupDbo;
import models.enums.SetupModifierName;
import models.idtypes.GameID;
import models.modifiers.Modifier;
import models.schemas.AbilityModifierSchema;
import models.schemas.AbilitySchema;
import models.schemas.FactionAbilityModifierSchema;
import models.schemas.FactionModifierSchema;
import models.schemas.FactionRoleSchema;
import models.schemas.FactionSchema;
import models.schemas.GameOverviewSchema;
import models.schemas.HiddenSchema;
import models.schemas.HiddenSpawnSchema;
import models.schemas.RoleModifierSchema;
import models.schemas.RoleSchema;
import models.schemas.SetupHiddenSchema;
import models.schemas.SetupSchema;
import models.schemas.SheriffCheckableSchema;
import repositories.FactionAbilityModifierRepo;
import repositories.FactionAbilityRepo;
import repositories.FactionEnemyRepo;
import repositories.FactionModifierRepo;
import repositories.FactionRepo;
import repositories.FactionRoleAbilityModifierRepo;
import repositories.FactionRoleModifierRepo;
import repositories.FactionRoleRepo;
import repositories.GameRepo;
import repositories.HiddenRepo;
import repositories.HiddenSpawnsRepo;
import repositories.RoleAbilityModifierRepo;
import repositories.RoleAbilityRepo;
import repositories.RoleModifierRepo;
import repositories.RoleRepo;
import repositories.SetupHiddenRepo;
import repositories.SetupModifierRepo;
import repositories.SetupRepo;
import repositories.SheriffCheckableRepo;
import util.game.GameUtil;

public class SetupService {

    public static Setup createSetup(SetupSchema setupSchema, boolean isDefault) throws SQLException {
        Setup narrator = new Setup(setupSchema);

        if(isDefault)
            Default.Default(narrator);

        insert(narrator);
        return narrator;
    }

    public static void insert(Setup setup) throws SQLException {
        SetupRepo.create(setup);
        long setupID = setup.id;

        try{
            ArrayList<Faction> factionList = new ArrayList<>(setup.factions.values());
            RoleService.insert(setupID, new ArrayList<>(setup.roles));

            FactionService.saveFactions(setupID, factionList);

            for(Faction faction: setup.factions.values()){
                for(Faction enemyFaction: faction.enemies)
                    FactionEnemyRepo.create(setupID, faction.id, enemyFaction.id);

                for(Faction checkableFaction: faction.sheriffCheckables)
                    SheriffCheckableRepo.create(faction.id, checkableFaction.id);
            }

            FactionRoleService.saveFactionRoles(setupID, factionList);

            HiddenService.insertHiddens(setupID, new ArrayList<>(setup.hiddens));

            for(SetupHidden setupHidden: setup.rolesList._setupHiddenList)
                setupHidden.id = SetupHiddenRepo.create(setupID, setupHidden.hidden.id, setupHidden.mustSpawn,
                        setupHidden.isExposed, setupHidden.minPlayerCount, setupHidden.maxPlayerCount);

            SetupModifierRepo.insert(setupID, setup.modifiers);
        }catch(SQLException e){
            try{
                SetupRepo.deleteByID(setupID);
            }catch(SQLException f){
                throw f;
            }
        }
    }

    public static Setup getSetup(long setupID) throws SQLException {
        SetupDbo setupDbo;
        try{
            setupDbo = getSetupDBO(setupID);
        }catch(InterruptedException | ExecutionException e){
            throw new NarratorException("Unable to get setup");
        }
        return loadSetup(setupDbo);
    }

    private static Setup loadSetup(SetupDbo setupDbo) {
        Setup setup = new Setup(setupDbo);

        SetupModifierService.loadSetupModifiers(setup, setupDbo);
        FactionService.loadFactions(setup, setupDbo);
        RoleService.loadRoles(setup, setupDbo);
        FactionRoleService.loadFactionRoles(setup, setupDbo);
        HiddenService.loadHiddens(setup, setupDbo);
        SetupHiddenService.loadSetupHiddens(setup, setupDbo);

        return setup;
    }

    public static Setup cloneSetup(long setupID, long userID) throws SQLException {
        Setup setup = getSetup(setupID);
        setup.ownerID = userID;
        insert(setup);
        return getSetup(setup.id);
    }

    public static SetupDbo getSetupDBO(long setupID) throws InterruptedException, ExecutionException {
        Future<SetupSchema> setupSchemaFuture = SetupRepo.getSchema(setupID);

        Future<Collection<Modifier<SetupModifierName>>> setupModifiersFuture = SetupModifierRepo.getBySetupID(setupID);

        Future<Set<FactionSchema>> factionDboFuture = FactionRepo.getBySetupID(setupID);
        Future<Map<Long, Set<FactionModifierSchema>>> factionModifiersFuture = FactionModifierRepo
                .getBySetupID(setupID);
        Future<Map<Long, Set<AbilitySchema>>> factionAbilitiesFuture = FactionAbilityRepo.getBySetupID(setupID);
        Future<Map<Long, Map<Long, Set<FactionAbilityModifierSchema>>>> factionAbilityModifiersFuture = FactionAbilityModifierRepo
                .getBySetupID(setupID);
        Future<ArrayList<SheriffCheckableSchema>> sheriffCheckablesFuture = SheriffCheckableRepo
                .getSheriffCheckableColors(setupID);
        Future<ArrayList<String[]>> factionEnemiesFuture = FactionEnemyRepo.getEnemyColors(setupID);

        Future<Set<RoleSchema>> roleDbosFuture = RoleRepo.getBySetupID(setupID);
        Future<Map<Long, Set<RoleModifierSchema>>> roleModifiersFuture = RoleModifierRepo.getBySetupID(setupID);
        Future<Map<Long, LinkedHashMap<Long, AbilitySchema>>> roleAbilitiesFuture = RoleAbilityRepo
                .getBySetupID(setupID);
        Future<Map<Long, Map<Long, Set<AbilityModifierSchema>>>> roleAbilityModifiersFuture = RoleAbilityModifierRepo
                .getBySetupID(setupID);

        Future<Set<FactionRoleSchema>> factionRoleDbosFuture = FactionRoleRepo.getBySetupID(setupID);
        Future<Map<Long, Set<RoleModifierSchema>>> factionRoleModifiersFuture = FactionRoleModifierRepo
                .getRoleModifiers(setupID);
        Future<Map<Long, Map<Long, Set<AbilityModifierSchema>>>> factionRoleAbilityModifiersFuture = FactionRoleAbilityModifierRepo
                .getAbilityModifiers(setupID);

        Future<Set<HiddenSchema>> hiddenDbosFuture = HiddenRepo.getBySetupID(setupID);
        Future<Set<HiddenSpawnSchema>> hiddenSpawnsFuture = HiddenSpawnsRepo.getBySetupID(setupID);

        Future<ArrayList<SetupHiddenSchema>> setupHiddenDbosFuture = SetupHiddenRepo.getBySetupID(setupID);

        SetupDbo setupDbo = new SetupDbo(setupSchemaFuture.get());
        setupDbo.modifiers = setupModifiersFuture.get();

        setupDbo.factionDbos = factionDboFuture.get();
        setupDbo.factionModifiers = factionModifiersFuture.get();
        setupDbo.factionAbilities = factionAbilitiesFuture.get();
        setupDbo.factionAbilityModifiers = factionAbilityModifiersFuture.get();
        setupDbo.sheriffCheckables = sheriffCheckablesFuture.get();
        setupDbo.factionEnemies = factionEnemiesFuture.get();

        setupDbo.roleDbos = roleDbosFuture.get();
        setupDbo.roleModifiers = roleModifiersFuture.get();
        setupDbo.roleAbilities = roleAbilitiesFuture.get();
        setupDbo.roleAbilityModifiers = roleAbilityModifiersFuture.get();

        setupDbo.factionRoleDbos = factionRoleDbosFuture.get();
        setupDbo.factionRoleModifiers = factionRoleModifiersFuture.get();
        setupDbo.factionRoleAbilityModifiers = factionRoleAbilityModifiersFuture.get();

        setupDbo.hiddenDbos = hiddenDbosFuture.get();
        setupDbo.hiddenSpawns = hiddenSpawnsFuture.get();

        setupDbo.setupHiddenDbos = setupHiddenDbosFuture.get();

        return setupDbo;
    }

    public static ArrayList<GeneratedRole> getIterations(long setupID, int playerCount) throws SQLException {
        Setup setup = getSetup(setupID);
        RoleAssigner roleAssigner = new RoleAssigner();
        List<SetupHidden> rolesListByPlayerCount = RolesList.getSpawningSetupHiddens(playerCount,
                setup.rolesList._setupHiddenList);
        Game game = GameUtil.getGeneric(setup, playerCount, GameUtil.getPermissiveModifiers());
        ArrayList<GeneratedRole> generatedRoles = roleAssigner.pickRoles(game, rolesListByPlayerCount);
        return roleAssigner.getPassOutRoles(playerCount, generatedRoles);
    }

    public static void loadDefault(Setup setup) {
        Default.Default(setup);
    }

    public static OpenLobby setSetup(GameID gameID, long setupID) throws SQLException {
        GameOverviewSchema game = GameRepo.getOverview(gameID);
        return setSetup(game, setupID);
    }

    public static OpenLobby setSetup(String joinID, long setupID) throws SQLException {
        Optional<GameOverviewSchema> gameOpt = GameRepo.getByLobbyID(joinID);
        if(!gameOpt.isPresent())
            throw new NarratorException("Game with that ID not found.");
        GameOverviewSchema game = gameOpt.get();
        return setSetup(game, setupID);
    }

    private static OpenLobby setSetup(GameOverviewSchema game, long setupID) throws SQLException {
        if(game.setupID != setupID)
            GameRepo.setSetupID(game.id, setupID);
        game.setupID = setupID;
        return LobbyService.getOpenLobby(game);
    }

    public static LobbySetupSchema assertModeratorAndOwner(long userID) throws SQLException {
        GameID gameID = LobbyService.assertIsModerator(userID);

        GameOverviewSchema gameOverview = GameRepo.getOverview(gameID);
        if(gameOverview.isStarted)
            throw new NarratorException("Cannot edit games that are in progress.");

        SetupSchema setupOverview = SetupRepo.getSchemaSync(gameOverview.setupID);
        if(!setupOverview.isEditable)
            throw new NarratorException("This setup is frozen and cannot be edited.");
        if(setupOverview.ownerID != userID)
            throw new NarratorException("Cant manipulate other player's setups.");

        return new LobbySetupSchema(gameOverview, setupOverview);
    }

    public static boolean isFull(Game game) throws SQLException {
        int playerSize = game.players.size();
        Setup setup = game.setup;
        int playerLimit;
        if(setup.rolesList.size(playerSize) < 3)
            playerLimit = 16;
        else
            playerLimit = Setup.getMaxPlayerCount(setup, Optional.of(game));
        return playerSize >= playerLimit;
    }

}
