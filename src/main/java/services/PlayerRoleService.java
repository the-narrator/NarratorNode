package services;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import game.logic.Game;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.support.RolePackage;
import models.idtypes.GameID;
import models.idtypes.PlayerDBID;
import repositories.PlayerRoleRepo;

public class PlayerRoleService {

    public static Map<String, Set<RolePackage>> getRoleLookupMap(List<RolePackage> generatedRoles) {
        HashMap<String, Set<RolePackage>> preferenceMap = new HashMap<>();
        Set<RolePackage> copies, teams;
        String roleName, color;
        for(RolePackage gen: generatedRoles){
            roleName = gen.assignedRole.getPlayerVisibleFactionRole().getName();

            if(preferenceMap.containsKey(roleName))
                copies = preferenceMap.get(roleName);
            else{
                copies = new HashSet<>();
                preferenceMap.put(roleName, copies);
            }
            copies.add(gen);

            color = gen.assignedRole.getColor();
            if(preferenceMap.containsKey(color))
                teams = preferenceMap.get(color);
            else{
                teams = new HashSet<>();
                preferenceMap.put(color, teams);
            }
            teams.add(gen);
        }
        return preferenceMap;
    }

    public static Map<Player, RolePackage> giveRolesToPlayers(Game narrator,
            Map<Player, List<RolePackage>> playerToRoles, HashMap<RolePackage, PlayerList> roleToPlayer,
            List<RolePackage> generatedRoles) {
        HashMap<Player, RolePackage> assignedRoles = new HashMap<>();
        List<RolePackage> preferredRoles;
        PlayerList unassignedPlayers = narrator.players.copy();
        Player player;
        RolePackage role;
        Player savedPlayer = null; // for the first one to have more than one preference. gets priority of
                                   // assigning over someone with none
        PlayerList playersLosingPreference;
        int i = 0;
        while (!unassignedPlayers.isEmpty()){
            player = unassignedPlayers.get(i);
            preferredRoles = playerToRoles.get(player);
            if(preferredRoles.size() == 1){

                role = preferredRoles.iterator().next();
                playersLosingPreference = giveRoleToPlayer(player, role, playerToRoles, roleToPlayer, assignedRoles,
                        unassignedPlayers, generatedRoles);

                // now that this role has been assigned, i have to remove it from all the other
                // player's pools

                for(Player luckedOut: playersLosingPreference)
                    i = Math.min(i, unassignedPlayers.indexOf(luckedOut));

                i = Math.max(i, 0);
                if(i + 1 >= unassignedPlayers.size())
                    i = 0;
                if(i == 0)
                    savedPlayer = null;
                continue;
            }
            if(player.hasPreferences() && savedPlayer == null)
                savedPlayer = player;

            if(i == unassignedPlayers.size() - 1){
                if(savedPlayer != null){
                    player = savedPlayer;
                    preferredRoles = playerToRoles.get(player);
                    if(!preferredRoles.isEmpty())
                        role = preferredRoles.iterator().next();
                    else
                        role = generatedRoles.get(0);
                }else{
                    player = unassignedPlayers.get(0);
                    role = generatedRoles.get(0);
                }
                giveRoleToPlayer(player, role, playerToRoles, roleToPlayer, assignedRoles, unassignedPlayers,
                        generatedRoles);
                i = 0;
                savedPlayer = null;
            }else{
                i++;
            }
        }
        return assignedRoles;
    }

    private static PlayerList giveRoleToPlayer(Player player, RolePackage role,
            Map<Player, List<RolePackage>> playerToRoles, HashMap<RolePackage, PlayerList> roleToPlayer,
            HashMap<Player, RolePackage> assignedRoles, PlayerList unassignedPlayers,
            List<RolePackage> generatedRoles) {
        generatedRoles.remove(role);
        assignedRoles.put(player, role);
        unassignedPlayers.remove(player);

        playerToRoles.remove(player);
        roleToPlayer.remove(role);
        PlayerList lostPreferPlayers = new PlayerList();
        for(Player p: playerToRoles.keySet()){
            if(playerToRoles.get(p).contains(role))
                lostPreferPlayers.add(p);
        }

        for(List<RolePackage> roles: playerToRoles.values())
            roles.remove(role);
        for(PlayerList players: roleToPlayer.values())
            players.remove(player);

        return lostPreferPlayers;
    }

    public static Map<PlayerDBID, Long> getByGameID(GameID replayID) throws SQLException {
        return PlayerRoleRepo.getByGameID(replayID);
    }
}
