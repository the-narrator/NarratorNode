package models;

public class User {
    public long id;
    public String name;

    public User(long id, String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if(o == null)
            return false;
        if(!User.class.equals(o.getClass()))
            return false;
        User user = (User) o;
        return user.id == this.id;
    }

    @Override
    public int hashCode() {
        return Long.hashCode(this.id);
    }
}
