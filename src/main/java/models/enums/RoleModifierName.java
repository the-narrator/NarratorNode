package models.enums;

import game.abilities.support.Vest;
import game.logic.support.rules.ModifierName;

public enum RoleModifierName implements ModifierName {
    AUTO_VEST(0, "Starts off with # auto-#" + Vest.ALIAS + "#", "Unlimited auto-#" + Vest.ALIAS + "#",
            "Auto-#" + Vest.ALIAS + "# count depends on initial player count."),
    UNIQUE(false, "Is not unique", "Is unique", "Uniqueness of this role depends on initial player count."),
    UNBLOCKABLE(false, "Is blockable", "Is not blockable",
            "Depending on initial player count, this role might be unblockable."),
    UNDETECTABLE(false, "Is detectable by investigative abilities", "Is undetectable by investigative abilities",
            "Depending on initial player count, this role might be undetectable."),
    UNCONVERTABLE(false, "Is convertable", "Is not convertable",
            "Depending on initial player count, this role might not be convertable."),
    HIDDEN_FLIP(false, "Death flip will be public", "Death flip will be hidden",
            "Depending on initial player count, death flip might be hidden.");

    private final String inactiveLimitedFormat, activeUnlimitedFormat, unsureFormat;
    private final Object defaultValue;

    RoleModifierName(Object defaultValue, String inactiveLimitedFormat, String acttiveUnlimitedFormat,
            String unsureFormat) {
        this.inactiveLimitedFormat = inactiveLimitedFormat;
        this.activeUnlimitedFormat = acttiveUnlimitedFormat;
        this.unsureFormat = unsureFormat;
        this.defaultValue = defaultValue;
    }

    @Override
    public String getActiveFormat() {
        return activeUnlimitedFormat;
    }

    @Override
    public String getInactiveFormat() {
        return inactiveLimitedFormat;
    }

    @Override
    public String getUnlimitedFormat() {
        return activeUnlimitedFormat;
    }

    @Override
    public String getLimitedFormat() {
        return inactiveLimitedFormat;
    }

    @Override
    public String getLabelFormat() {
        return activeUnlimitedFormat;
    }

    @Override
    public String getUnsureFormat() {
        return unsureFormat;
    }

    @Override
    public Object getDefaultValue() {
        return defaultValue;
    }

}
