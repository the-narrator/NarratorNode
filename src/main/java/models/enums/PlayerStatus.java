package models.enums;

public enum PlayerStatus {
    BLOCKED, SILENCED, DISFRANCHISED,

    BUS_DRIVEN,

    JOKER_PLAYER_STATUS_KEY, SUCCESSFUL_BOUNTY
}
