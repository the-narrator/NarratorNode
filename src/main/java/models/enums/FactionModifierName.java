package models.enums;

import java.util.Optional;

import game.logic.support.Constants;
import game.logic.support.rules.ModifierName;
import models.Faction;
import models.modifiers.Modifiers;
import util.game.ModifierUtil;

public enum FactionModifierName implements ModifierName {
    HAS_NIGHT_CHAT(false, "Can't talk to teammates at night", "Can talk to teammates at night",
            "Can talk to allies at night",
            "Depending on initial player count, might be able to talk to allies at night."),
    IS_RECRUITABLE(true, "Team members cannot be recruited", "Team members can be taken by a Cult Leader",
            "Can be recruited", "Depending on initial player count, might be recruitable."),
    KNOWS_ALLIES(false, "Doesn't know the identity of teammates", "Knows the identity of teammates",
            "Knows who allies are", "Depending on initial player count, might know the identity of teammates."),
    LAST_WILL(false, "No team last will of any sort",
            "Can write a message that will be sent to team members upon death", "Can send a last will upon death",
            "Depending on initial player count, might be able to send a team last will."),
    LIVE_TO_WIN(false, "Only one person from this faction needs to be alive for all faction members to win",
            "Members of this faction must be alive at the end of the game to win", "Must be alive to win",
            "Depending on initial player count, might need to be alive to win."),
    PUNCH_SUCCESS(0, "", "", "Punch success rate", ""), WIN_PRIORITY(0, "", "", "", "");

    private final String inactiveLimitedFormat, activeUnlimitedFormat, label, unsureFormat;
    public final Object defaultValue;

    FactionModifierName(Object defaultValue, String string1, String string2, String label, String unsureFormat) {
        this.defaultValue = defaultValue;
        this.inactiveLimitedFormat = string1;
        this.activeUnlimitedFormat = string2;
        this.unsureFormat = unsureFormat;
        this.label = label;
    }

    public static int maxValue(FactionModifierName fRule) {
        switch (fRule) {
        case PUNCH_SUCCESS:
            return 100;
        default:
            return Constants.MAX_INT_MODIFIER;

        }
    }

    public static int minValue(FactionModifierName fRule) {
        switch (fRule) {
        case PUNCH_SUCCESS:
            return 0;
        default:
            return Constants.UNLIMITED;
        }
    }

    @Override
    public String getActiveFormat() {
        return activeUnlimitedFormat;
    }

    @Override
    public String getInactiveFormat() {
        return inactiveLimitedFormat;
    }

    @Override
    public String getUnlimitedFormat() {
        return activeUnlimitedFormat;
    }

    @Override
    public String getLimitedFormat() {
        return inactiveLimitedFormat;
    }

    @Override
    public String getUnsureFormat() {
        return this.unsureFormat;
    }

    @Override
    public String getLabelFormat() {
        return label;
    }

    @Override
    public Object getDefaultValue() {
        return this.defaultValue;
    }

    public String getTextValue(Optional<Integer> game, Faction faction) {
        return getTextValue(game, faction.setup.modifiers, faction.modifiers);
    }

    public String getTextValue(Optional<Integer> game, Modifiers<SetupModifierName> setupModifiers,
            Modifiers<FactionModifierName> factionModifiers) {
        return ModifierUtil.getTextValue(this, game, setupModifiers, factionModifiers);
    }

    public String getLabel(Optional<Integer> game, Modifiers<SetupModifierName> setupModifiers) {
        return ModifierUtil.replaceFormats(game, setupModifiers, label);
    }
}
