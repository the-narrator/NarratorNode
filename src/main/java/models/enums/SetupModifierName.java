package models.enums;

import java.util.Optional;

import game.abilities.Doctor;
import game.abilities.support.Bread;
import game.abilities.support.Gun;
import game.abilities.support.Vest;
import game.logic.Game;
import game.logic.support.rules.ModifierName;
import models.modifiers.Modifiers;
import util.game.ModifierUtil;

public enum SetupModifierName implements ModifierName {

    SELF_VOTE(false, "You may not self vote.", "You may self vote.", "",
            "Self voting depends on initial player count."),
    SKIP_VOTE(true, "You may not skip vote.", "You may skip vote.", "",
            "Skipping vote depends on initial player count."),
    SECRET_VOTES(false, "Votes will be hidden.", "Votes will be announced.", "",
            "Vote announcement depends on initial player count."),

    AMNESIAC_KEEPS_CHARGES(true, "Will only get a single charge from the role, if the role is a limited use role",
            "Will keep remaining charges, if role is a limited use role.", "Amnesiac keeps charges",
            "Starting charges vary on initial player count."),
    AMNESIAC_MUST_REMEMBER(false, "Amnesiac can win without changing role", "Amnesiac must remember to win",
            "Amnesiac must remember role to win", "Amnesiac winning depends on initial player count."),
    ARCH_BURN(false, "Burn deaths do not spread in architect chats",
            "Burned targets will also kill everyone else in architect chat", "Fires spread in architect chats",
            "Burn deaths killing other in architect rooms depends on initial player count."),
    ARCH_QUARANTINE(false, "Chat participants may talk in other chat rooms",
            "Chat participants are removed from all other night chats", "Architect removes from other night chats",
            "Chat participants being removed from other chat rooms depends on initial player count."),
    ARSON_DAY_IGNITES(1, "Limited to %s day ignite(s)", "Can day ignite every day", "Day Ignite Count",
            "Day ignite count depends on initial player count."),
    BLOCK_FEEDBACK(true, "Targets won't know they were slept with", "Targets will know they were slept with",
            "Targets know they were slept with", "Block feedback depends on initial player count."),
    BOMB_PIERCE(false, "Doesn't pierce death immunity", "Pierces death immunity", "Pierces death immunity",
            "Bomb piercing death immunity depends on initial player count."),
    BOUNTY_INCUBATION(2, "People with a bounty on them can go %s day(s) until consequences hit", "", "Days of bounty",
            "Bounty expiration depends on initial player count."),
    BOUNTY_FAIL_REWARD(2, "Gets %s retaliatory kill(s) if bounty fails", "", "Kill charges after bounty failure",
            "Bounty kill count depends on initial player count."),
    BREAD_PASSING(true, "#" + Bread.LABEL_NAME + "# cannot be passed.", "#" + Bread.LABEL_NAME + "# can be passed.",
            "#" + Bread.LABEL_NAME + "# can be passed", "Whether bread can be passed depends on initial player count"),
    BREAD_NAME(Bread.class.getSimpleName().toLowerCase(), "", "", "", Bread.class.getSimpleName().toLowerCase()),
    CORONER_EXHUMES(true, "Coroners won't stop others from robbing bodies",
            "Coroner will stop others from robbing bodies", "Coroner exhumes body",
            "Coroner stopping grave robberies depends on player count."),
    CHARGE_VARIABILITY(0, "", "", "", ""),
    CHECK_DIFFERENTIATION(true, "Only sees teams as bad or not suspicious",
            "Can pick out the difference between one team and another",
            "Can pick out the difference between different factions",
            "Whether checks return exact alignment depends on initial player count."),
    CONVERT_REFUSABLE(false, "Refusing a recruitment invitation is not a choice", "Power roles can refuse recruitment.",
            "Power Roles can refuse conversion", "The ability to refuse conversions depends on initial player count."),
    CORONER_LEARNS_ROLES(false, "Coroners won't learn what roles visited the corpse while living.",
            "Coroners will learn what roles visited the corpse while living.", "Coroner learns roles visited",
            "Whether autopsys return visiting roles depends on initial player count."),
    CULT_POWER_ROLE_CD(0, "After recruiting a power role, must wait %s nights before next conversion",
            "No wait time between power role recruits", "Power Role conversion delay",
            "Power role conversion cooldown depends on initial player count."),
    CULT_PROMOTION(false, "Cannot promote minions", "May promote minions to another ability",
            "Cult can promote powerless roles", "The ability to promote minions depends on initial player count."),
    CIT_RATIO(40, "The percentage of citizens to non citizens is about %s%.", "", "Ratio of citizens to non-citizens",
            "Citizen ratio depends on initial player count."),
    CULT_KEEPS_ROLES(true, "Recruited members change role upon successful recruitment",
            "Recruited members keep their original role upon recruitment", "Converted keep roles",
            "Whether converted players keep their roles depends on player count."),
    DAY_START(Game.DAY_START, "Game starts on nighttime", "Game starts at daytime", "",
            "Game phase start depends on initial player count."),
    DIFFERENTIATED_FACTION_KILLS(false, "You will not know which faction made this kill.",
            "You will know which faction made this kill.", "",
            "Whether faction kill information is revealed depeneds on player count."),
    DOUSE_FEEDBACK(true, "Doused targets are unaware they were doued", "Doused targets are aware they were doused",
            "Doused targets informed", "Knowing whether or not one is doused depends on initial player count."),
    ENFORCER_LEARNS_NAMES(true, "Won't learn attempted recruiter's name", "Will learn attempted recruiter's name",
            "Will learn attempted recruiter's name",
            "Depending on initial player count, enforcing might also uncover recruiter's name."),
    EXEC_TOWN(false, "Target can be of any faction", "Target is always of the majority faction",
            "Target always majority faction", "Target faction could depend on initial player count."),
    EXECUTIONER_TO_JESTER(true, "Suicides if target(s) is night killed", "Turns into jester upon target night death",
            "Turns into jester upon loss", "Might turn into jester upon loss, depending on initial player count."),
    EXECUTIONER_WIN_IMMUNE(false, "Invulnerable after winning", "Not invulnerable after winning",
            "Invulnerable after winning", "Depending on initial player count, could be invulnerable after winning."),
    FOLLOW_GETS_ALL(false, "Can only see one target", "Can see all targets visited by the followed person",
            "Can see all targets of the followed person",
            "Depending on initial player count, might see all targets of the followed person."),
    FOLLOW_PIERCES_IMMUNITY(false, "Can't follow detection immune targets", "Ignored detection immunity",
            "Pierces detectability immunity",
            "Depending on initial player count, following might pierce detection immunity."),
    GD_REANIMATE(true, "May not target the same corpse multiple times", "May target the same corpse multiple times",
            "May target the same corpse multiple times.",
            "Depending on initial player count, corpses might be able to be used more than once."),
    GS_DAY_GUNS(false, "#" + Gun.ALIAS + "#s are used during the night", "#" + Gun.ALIAS + "#s are shot during the day",
            "#" + Gun.ALIAS + "# given are used during the day",
            "Whether #\" + Gun.ALIAS + \"# are used during the day or night, depends on initial player count."),
    GUN_NAME(Gun.class.getSimpleName().toLowerCase(), "", "", "", ""),
    GUARD_REDIRECTS_CONVERT(false, "Conversion is unaffected by bodyguard",
            "Conversion will redirect to bodyguard if bodyguard is guarding target",
            "Conversion redirected if being guarded",
            "Whether guarding protects from conversion depends on initial player count."),
    GUARD_REDIRECTS_DOUSE(false, "Douse is unaffected by bodyguard",
            "Douse will redirect to bodyguard if bodyguard is guarding target", "Douse redirected if being guarded",
            "Douse redirection depends on initial player count."),
    GUARD_REDIRECTS_POISON(false, "Poison is unaffected by bodyguard",
            "Poison will redirect to bodyguard if bodyguard is guarding target", "Poison redirected if being guarded.",
            "Whether poisons redirect on guard attempts depends on initial player count."),
    HEAL_BLOCKS_POISON(false, Doctor.ROLE_DETAIL_POISON_CANNOT_STOP, Doctor.ROLE_DETAIL_POISON_CAN_STOP,
            "Doctors heal poisonings", "Heals could stop poison attempts, depending on initial player count."),
    HEAL_FEEDBACK(true, "Target doesn't know if they were saved", "Target knows if they were saved, but not by who",
            "Target knows if successfully healed",
            "Targets might know whether they were attacked and saved, depending on initial player count."),
    HEAL_SUCCESS_FEEDBACK(true, "Will not know if target was successfully saved",
            "Will know if target was successfully saved", "Knows if successful",
            "The healer might know whether the target was attacked, depending on initial player count."),
    JANITOR_GETS_ROLES(false, "Doesn't receive target's role", "Receive targets role", "Receives target's role",
            "Cleaning might return a target's role, depending on initial player count."),
    JESTER_CAN_ANNOY(true, "Cannot annoy people at night", "May annoy people at night", "May annoy people at night",
            "Might be able to annoy people at night, depending on initial player count."),
    JESTER_KILLS(1, "Upon day elimination, %s% will suicide", "", "% of guilty voters die",
            "What percentage of guilty voters die depends on initial player count."),
    LAST_WILL(true, "Last wills are not allowed", "Last wills are allowed", "",
            "Depending on initial player count, One might be able to leave last wills."),
    MARSHALL_EXECUTIONS(2, "Adds %s more public executions to the day",
            "Nighttime phase is skipped until game is over, or faction has a majority", "Marshall Execution Count",
            "Marshall law execution count depends on initial player count."),
    MARSHALL_QUICK_REVEAL(false, "Executed people are revealed immediately.",
            "Executed people are revealed at the end of the day.", "Marshall Executions Revealed Immediately",
            "Depending on initial player count, players executed during marshall law might have their role revealed."),
    MASON_NON_CIT_RECRUIT(true, "Can only recruit citizens", "May recruit citizens without powers",
            "Can recruit powerless non-citizens",
            "Depending on initial player count, powerless non-citizens could be converted."),
    MASON_PROMOTION(true, "Masons do not get promoted",
            "A random mason will replace Mason Leader upon original Mason Leader death", "Masons get promoted",
            "Depending on initial player count, masons might replace dying mason leaders."),
    MAYOR_VOTE_POWER(2, "After revealing, vote power will increase by %s", "", "Vote Power increase",
            "After reveals, new vote power depends on initial player count."),
    MILLER_SUITED(false, "Will not look evil on death", "Will look evil on death", "Will look evil on death",
            "Depending on initial player count, might look evil on death."),
    MM_SPREE_DELAY(0, "After killing more than one person, must wait %s day(s) to use ability again",
            "No wait time between ability uses", "Spree Delay",
            "The cool down triggered by killing two or more players depends on initial player count."),
    PROXY_UPGRADE(true, "Faction won't get kill ability on this role card's death",
            "Faction will get kill ability on this role card's death", "On death, team receives faction kill",
            "Depending on initial player count, team might have a shared faction ability."),
    PUNCH_ALLOWED(false, "Day punches are disabled", "Day punches are enabled", "",
            "Day punches might be enabled, depending on initial player count."),
    SHERIFF_PREPEEK(false, "Has no pregame check", "Knows a team member at game start",
            "Knows team member at game start",
            "Depending on initial player count, Might get a known team member at game start."),
    SELF_BREAD_USAGE(false, "#" + Bread.LABEL_NAME + "# cannot immediately be used by maker",
            "#" + Bread.LABEL_NAME + "# can be immediately used by maker.",
            "#" + Bread.LABEL_NAME + "# useable by maker.",
            "Depending on initial player count, #\" + Bread.LABEL_NAME + \"# might be able to be used by the baker."),
    SNITCH_PIERCES_SUIT(false, "Does not pierce tailoring", "Pierces tailoring", "Pierces tailoring",
            "Might piece tailoring, depending on initial player count."),
    SPY_TARGETS_ALLIES(false, "Cannot target allied factions", "Can target allied factions", "Can spy on allies",
            "Depending on initial player count, might be able to spy on allies."),
    SPY_TARGETS_ENEMIES(true, "Cannot target enemy factions", "Can target enemy factions", "Can spy on enemies",
            "Depending on initial player count, might be able to spy on allies."),
    TAILOR_FEEDBACK(true, "Suit receivers aren't informed of suit", "Suit receivers are informed of suit",
            "Suit receivers get notification.",
            "Depending on initial player count, suit receivers might know they were suited."),
    VEST_NAME(Vest.class.getSimpleName().toLowerCase(), "", "", "", ""),
    WITCH_FEEDBACK(false, "Controlled targets dont know they were targeted",
            "Controlled targets will be informed that they were manipulated", "Leaves Feedback",
            "Depending on initial player count, controlled targets might know whether they were manipulated.");

    private final String inactiveLimitedFormat, activeUnlimitedFormat, uiLabel, unsureFormat;
    public final Object defaultValue;

    SetupModifierName(Object defaultValue, String inactiveLimitedFormat, String activeUnlimitedFormat, String uiLabel,
            String unsureFormat) {
        this.defaultValue = defaultValue;
        this.inactiveLimitedFormat = inactiveLimitedFormat;
        this.activeUnlimitedFormat = activeUnlimitedFormat;
        this.uiLabel = uiLabel;
        this.unsureFormat = unsureFormat;
    }

    @Override
    public String getActiveFormat() {
        return activeUnlimitedFormat;
    }

    @Override
    public String getInactiveFormat() {
        return inactiveLimitedFormat;
    }

    @Override
    public String getUnlimitedFormat() {
        return activeUnlimitedFormat;
    }

    @Override
    public String getUnsureFormat() {
        return unsureFormat;
    }

    @Override
    public String getLimitedFormat() {
        return inactiveLimitedFormat;
    }

    @Override
    public String getLabelFormat() {
        return uiLabel;
    }

    @Override
    public Object getDefaultValue() {
        return this.defaultValue;
    }

    public String getLabel(Optional<Integer> game, Modifiers<SetupModifierName> modifiers) {
        return ModifierUtil.replaceFormats(game, modifiers, uiLabel);
    }

    public String getTextValue(Optional<Integer> game, Modifiers<SetupModifierName> modifiers) {
        return ModifierUtil.getTextValue(this, game, modifiers, modifiers);
    }
}
