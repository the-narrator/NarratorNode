package models.enums;

import java.util.Arrays;
import java.util.Collection;
import java.util.Optional;

import game.abilities.support.Gun;
import game.abilities.support.Vest;
import game.logic.support.Constants;
import game.logic.support.rules.ModifierName;
import game.setups.Setup;
import models.modifiers.Modifiers;
import util.game.ModifierUtil;

public enum AbilityModifierName implements ModifierName {
    BACK_TO_BACK(true, "May not target the same person/people with this ability",
            "May target the same person/people with this ability",
            "Depending on initial player count, might be able to target the same person/people with this ability."),
    CHARGES(Constants.UNLIMITED, "Limited to % charges", "No limits to how many times this ability may be used",
            "Charges dependent on initial player count."),
    COOLDOWN(0, "Must wait % days before using the ability again", "No limits to how often this ability may be used.",
            "Cooldown depends on initial player count."),
    HIDDEN(false, "This ability will be known to the player.", "This ability will not be known to the player.",
            "Whether this ability is hidden to the player depends on the initial player count."),
    FACTION_COUNT_MIN(0, "Faction size must be % large to use.", "No minimum limits to when this may be used.",
            "Whether a faction can use this ability regardless of minimum faction size depends on initial player count."),
    FACTION_COUNT_MAX(Setup.MAX_PLAYER_COUNT, "Faction size must be less than to use.",
            "No max limits to when this may be used.",
            "Whether a faction can use this ability regardless of maximum faction size depends on initial player count."),
    SELF_TARGET(false, "May not self target", "May self target",
            "Depending on initial player count, might be able to self target."),
    ZERO_WEIGHTED(false, "This action may not be submitted for free", "This action may be submitted for free",
            "Depending on initial player count, player might submit this action for free."),

    AS_FAKE_VESTS(false, "#" + Vest.ALIAS + "#s passed out cannot be fake",
            "#" + Vest.ALIAS + "#s passed out can be fake",
            "Depending on initial player count, #\" + Vest.ALIAS + \"#s passed out might be fake."),
    GS_FAULTY_GUNS(false, "#" + Gun.ALIAS + "#s passed out are always safe to use",
            "#" + Gun.ALIAS + "# passed out can be faulty",
            "Depending on initial player count, #\" + Gun.ALIAS + \"#s might be faulty."),
    JAILOR_CLEAN_ROLES(0, "Limited to % cleanings", "May clean every execution",
            "Depending on initial player count, some executions might be cleaned."),
    SECRET_KILL(false, "Day killer name will be announced.", "Day killer name will not be announced.",
            "Depending on initial player count, day killer might not be announced."),
    SILENCE_ANNOUNCED(false, "Silenced players will not be announced.", "Silenced players will be announced.",
            "Depending on initial player count, silenced players might be announced."),
    DISFRANCHISED_ANNOUNCED(false, "Disfranchised players will not be announced.",
            "Disfranchised players will be announced.",
            "Depending on initial player count, disfranchised players might be announced.");

    private final String inactiveLimitedFormat, activeUnlimitedFormat, unsureFormat;
    public final Object defaultValue;

    public static Collection<AbilityModifierName> GENERIC_MODIFIERS = Arrays.asList(BACK_TO_BACK, CHARGES, COOLDOWN,
            HIDDEN, SELF_TARGET, ZERO_WEIGHTED);

    AbilityModifierName(Object defaultValue, String inactiveLimitedFormat, String activeUnlimitedFormat,
            String unsureFormat) {
        this.inactiveLimitedFormat = inactiveLimitedFormat;
        this.activeUnlimitedFormat = activeUnlimitedFormat;
        this.unsureFormat = unsureFormat;
        this.defaultValue = defaultValue;
    }

    @Override
    public String getActiveFormat() {
        return activeUnlimitedFormat;
    }

    @Override
    public String getInactiveFormat() {
        return inactiveLimitedFormat;
    }

    @Override
    public String getUnlimitedFormat() {
        return activeUnlimitedFormat;
    }

    @Override
    public String getLimitedFormat() {
        return inactiveLimitedFormat;
    }

    @Override
    public String getLabelFormat() {
        return activeUnlimitedFormat;
    }

    @Override
    public String getUnsureFormat() {
        return this.unsureFormat;
    }

    @Override
    public Object getDefaultValue() {
        return defaultValue;
    }

    public String getTextValue(Optional<Integer> playerCount, Modifiers<SetupModifierName> setupModifiers,
            Modifiers<AbilityModifierName> modifiers) {
        return ModifierUtil.getTextValue(this, playerCount, setupModifiers, modifiers);
    }
}
