package models.schemas;

import java.time.Instant;

import models.enums.AbilityModifierName;
import models.modifiers.Modifier;

public class FactionAbilityModifierSchema extends ModifierSchema<AbilityModifierName> {
    public FactionAbilityModifierSchema(Modifier<AbilityModifierName> modifier) {
        super(modifier);
    }

    public FactionAbilityModifierSchema(long id, AbilityModifierName name, Object value, int minPlayerCount,
            int maxPlayerCount, Instant upsertedAt) {
        super(id, name, value, minPlayerCount, maxPlayerCount, upsertedAt);
    }
}
