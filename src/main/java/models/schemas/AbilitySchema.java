package models.schemas;

import models.Ability;
import models.enums.AbilityType;

public class AbilitySchema {
    public long id;
    public AbilityType type;

    public AbilitySchema(long id, AbilityType type) {
        this.id = id;
        this.type = type;
    }

    public AbilitySchema(Ability ability) {
        this.id = ability.id;
        this.type = ability.type;
    }

    @Override
    public String toString() {
        return type.toString();
    }
}
