package models.schemas;

import models.idtypes.GameID;

public class GameUserSchema {
    public long id;
    public GameID gameID;
    public boolean isModerator;
    public boolean isExited;

    public GameUserSchema(long id, GameID gameID, boolean isModerator, boolean isExited) {
        this.id = id;
        this.gameID = gameID;
        this.isModerator = isModerator;
        this.isExited = isExited;
    }
}
