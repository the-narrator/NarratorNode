package models.schemas;

import models.idtypes.GameID;

public class GameOverviewSchema {

    public GameID id;
    public long setupID;
    public String lobbyID;
    public boolean isPrivate;
    public long seed;
    public boolean isStarted;
    public boolean isFinished;

    public GameOverviewSchema(GameID id, long setupID, String instanceID, boolean isStarted, boolean isFinished,
            boolean isPrivate, long seed) {
        this.id = id;
        this.setupID = setupID;
        this.lobbyID = instanceID;
        this.isPrivate = isPrivate;
        this.seed = seed;
        this.isStarted = isStarted;
        this.isFinished = isFinished;
    }

    public boolean isInProgress() {
        return this.isStarted && !this.isFinished;
    }

}
