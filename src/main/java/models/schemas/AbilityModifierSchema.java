package models.schemas;

import java.time.Instant;

import models.enums.AbilityModifierName;
import models.modifiers.AbilityModifier;
import models.modifiers.Modifier;

public class AbilityModifierSchema extends ModifierSchema<AbilityModifierName> {

    public AbilityModifierSchema(long id, AbilityModifierName name, Object value, int minPlayerCount,
            int maxPlayerCount, Instant upsertedAt) {
        super(id, name, value, minPlayerCount, maxPlayerCount, upsertedAt);
    }

    public AbilityModifierSchema(AbilityModifier modifier) {
        super(modifier);
    }

    public AbilityModifierSchema(Modifier<AbilityModifierName> modifier) {
        super(modifier);
    }

}
