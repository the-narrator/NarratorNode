package models.schemas;

import java.time.Instant;

import models.enums.FactionModifierName;
import models.modifiers.Modifier;

public class FactionModifierSchema extends ModifierSchema<FactionModifierName> {

    public FactionModifierSchema(long id, FactionModifierName name, Object value, int minPlayerCount,
            int maxPlayerCount, Instant upsertedAt) {
        super(id, name, value, minPlayerCount, maxPlayerCount, upsertedAt);
    }

    public FactionModifierSchema(Modifier<FactionModifierName> modifier) {
        super(modifier);
    }
}
