package models.schemas;

import models.Role;

public class RoleSchema {
    public long id;
    public String name;

    public RoleSchema(Role role) {
        this.id = role.id;
        this.name = role.getName();
    }

    public RoleSchema(long id, String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
