package models.schemas;

import java.time.Instant;

import game.logic.support.rules.ModifierName;
import models.modifiers.Modifier;
import util.game.ModifierUtil;

public abstract class ModifierSchema<T extends ModifierName> {

    public long id;
    public Object value;
    public T name;
    public int minPlayerCount;
    public int maxPlayerCount;
    public Instant upsertedAt; // change to upsertedAt

    public ModifierSchema(long id, T name, Object value, int minPlayerCount, int maxPlayerCount, Instant upsertedAt) {
        this.id = id;
        this.name = name;
        this.value = value;
        this.minPlayerCount = minPlayerCount;
        this.maxPlayerCount = maxPlayerCount;
        this.upsertedAt = upsertedAt;
    }

    public ModifierSchema(Modifier<T> modifier) {
        this.id = modifier.id;
        this.name = modifier.name;
        this.value = modifier.value.internalValue;
        this.minPlayerCount = modifier.minPlayerCount;
        this.maxPlayerCount = modifier.maxPlayerCount;
        this.upsertedAt = modifier.upsertedAt;
    }

    public boolean isInt() {
        return ModifierUtil.IsIntModifier(name);
    }

    @Override
    public String toString() {
        return name.toString() + " " + value.toString() + " (" + minPlayerCount + "->" + maxPlayerCount + ")";
    }

}
