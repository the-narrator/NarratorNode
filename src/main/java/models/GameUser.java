package models;

import java.util.Optional;
import java.util.Set;

import models.idtypes.PlayerDBID;

public class GameUser {
    public long id;
    public Optional<PlayerDBID> playerID;
    public boolean isModerator;

    public GameUser(long userID, boolean isModerator, Optional<PlayerDBID> playerID) {
        this.id = userID;
        this.playerID = playerID;
        this.isModerator = isModerator;
    }

    public GameUser(long userID, Set<Long> moderatorIDs, Optional<PlayerDBID> playerID) {
        this(userID, moderatorIDs.contains(userID), playerID);
    }
}
