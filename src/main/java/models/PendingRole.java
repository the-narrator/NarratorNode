package models;

import java.util.Optional;

import game.logic.GameRole;

public class PendingRole {
    public FactionRole factionRole;
    public boolean isPromotion;
    public Optional<GameRole> sourceRole;

    public PendingRole(FactionRole factionRole) {
        this.factionRole = factionRole;
        this.isPromotion = false;
        this.sourceRole = Optional.empty();
    }

    public PendingRole(GameRole gameRole) {
        this.factionRole = gameRole.factionRole;
        this.isPromotion = false;
        this.sourceRole = Optional.of(gameRole);
    }

    public PendingRole(FactionRole factionRole, boolean isPromotion) {
        this(factionRole);
        this.isPromotion = isPromotion;
    }
}
