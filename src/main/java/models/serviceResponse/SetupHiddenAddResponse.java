package models.serviceResponse;

import models.SetupHidden;
import models.idtypes.GameID;

public class SetupHiddenAddResponse {
    public SetupHidden setupHidden;
    public GameID gameID;
    public String joinID;

    public SetupHiddenAddResponse(SetupHidden setupHidden, GameID id, String joinID) {
        this.setupHidden = setupHidden;
        this.gameID = id;
        this.joinID = joinID;
    }
}
