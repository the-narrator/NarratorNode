package models.serviceResponse;

import java.util.Optional;

import game.logic.Game;
import models.Role;

public class RoleGetResponse {

    public Optional<Game> game;
    public int playerCount;
    public Role role;

    public RoleGetResponse(Role role, Optional<Game> optGame, int playerCount) {
        this.role = role;
        this.playerCount = playerCount;
        this.game = optGame;
    }
}
