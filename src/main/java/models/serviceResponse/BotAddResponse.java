package models.serviceResponse;

import java.util.Set;

import game.logic.Game;
import models.schemas.PlayerSchema;

public class BotAddResponse {
    public Game game;
    public Set<PlayerSchema> players;

    public BotAddResponse(Game game, Set<PlayerSchema> players) {
        this.game = game;
        this.players = players;
    }
}
