package models.serviceResponse;

import game.logic.support.rules.ModifierName;
import models.modifiers.Modifier;
import models.schemas.GameOverviewSchema;

public class ModifierServiceResponse<T extends ModifierName> {
    public long setupID;
    public Modifier<T> modifier;
    public String joinID;

    public ModifierServiceResponse(GameOverviewSchema game, Modifier<T> modifier) {
        this.setupID = game.setupID;
        this.modifier = modifier;
        this.joinID = game.lobbyID;
    }
}
