package models.serviceResponse;

import game.logic.Game;
import models.FactionRole;

public class FactionRoleCreateResponse {

    public FactionRole factionRole;
    public Game game;

    public FactionRoleCreateResponse(FactionRole factionRole, Game game) {
        this.factionRole = factionRole;
        this.game = game;
    }
}
