package models.serviceResponse;

import game.logic.Game;
import models.Role;

public class RoleCreateResponse {
    public Game game;
    public Role role;

    public RoleCreateResponse(Role role, Game game) {
        this.role = role;
        this.game = game;
    }
}
