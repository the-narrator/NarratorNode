package models.serviceResponse;

import java.util.Optional;

import models.GameLobby;
import models.OpenLobby;

public class GameLobbyGetResponse {

    public Optional<GameLobby> gameLobby;
    public Optional<OpenLobby> openLobby;

    public GameLobbyGetResponse(GameLobby gameLobby) {
        this.gameLobby = Optional.of(gameLobby);
        this.openLobby = Optional.empty();
    }

    public GameLobbyGetResponse(OpenLobby openLobby) {
        this.gameLobby = Optional.empty();
        this.openLobby = Optional.of(openLobby);
    }

}
