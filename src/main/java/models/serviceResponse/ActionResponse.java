package models.serviceResponse;

import java.util.Optional;

import game.event.Message;
import json.JSONObject;
import models.json_output.VoteData;

public class ActionResponse {
    public VoteData voteInfo;
    public JSONObject actions;
    public Optional<Message> newActionConfirmMessage;
}
