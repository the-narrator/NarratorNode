package models.serviceResponse;

import game.logic.Game;
import models.Faction;

public class FactionCreateResponse {
    public Faction faction;
    public String lobbyID;
    public Game game;

    public FactionCreateResponse(Game game, Faction faction, String lobbyID) {
        this.faction = faction;
        this.game = game;
        this.lobbyID = lobbyID;
    }
}
