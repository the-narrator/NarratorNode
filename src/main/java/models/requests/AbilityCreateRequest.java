package models.requests;

import java.util.ArrayList;
import java.util.Set;

import models.Ability;
import models.enums.AbilityType;

public class AbilityCreateRequest {
    public AbilityType abilityType;

    public AbilityCreateRequest(AbilityType abilityType) {
        this.abilityType = abilityType;
    }

    public AbilityCreateRequest(Ability ability) {
        this(ability.type);
    }

    public static ArrayList<AbilityCreateRequest> from(Set<AbilityType> abilityTypes) {
        ArrayList<AbilityCreateRequest> requests = new ArrayList<>();
        for(AbilityType abilityType: abilityTypes)
            requests.add(new AbilityCreateRequest(abilityType));
        return requests;
    }
}
