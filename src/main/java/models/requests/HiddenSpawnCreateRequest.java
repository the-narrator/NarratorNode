package models.requests;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import models.schemas.HiddenSpawnSchema;

public class HiddenSpawnCreateRequest {

    public long factionRoleID;
    public long hiddenID;
    public int maxPlayerCount;
    public int minPlayerCount;

    public HiddenSpawnCreateRequest(long hiddenID, long factionRoleID, int minPlayerCount, int maxPlayerCount) {
        this.hiddenID = hiddenID;
        this.factionRoleID = factionRoleID;
        this.minPlayerCount = minPlayerCount;
        this.maxPlayerCount = maxPlayerCount;
    }

    public HiddenSpawnCreateRequest(HiddenSpawnSchema schema) {
        this.factionRoleID = schema.factionRoleID;
        this.hiddenID = schema.hiddenID;
        this.minPlayerCount = schema.minPlayerCount;
        this.maxPlayerCount = schema.maxPlayerCount;
    }

    public static List<HiddenSpawnCreateRequest> from(ArrayList<HiddenSpawnSchema> hiddenSpawnSchemas) {
        List<HiddenSpawnCreateRequest> requests = new LinkedList<>();
        for(HiddenSpawnSchema schema: hiddenSpawnSchemas)
            requests.add(new HiddenSpawnCreateRequest(schema));
        return requests;
    }

}
