package models;

import models.schemas.GameOverviewSchema;
import models.schemas.SetupSchema;

public class LobbySetupSchema {

    public GameOverviewSchema game;
    public SetupSchema setup;

    public LobbySetupSchema(GameOverviewSchema gameOverview, SetupSchema setupOverview) {
        this.game = gameOverview;
        this.setup = setupOverview;
    }

}
