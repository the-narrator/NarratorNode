package models;

import java.time.Instant;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import game.abilities.CultLeader;
import game.abilities.Cultist;
import game.abilities.Executioner;
import game.abilities.Jester;
import game.abilities.Mason;
import game.abilities.MasonLeader;
import game.logic.FactionGroup;
import game.logic.Game;
import game.logic.exceptions.NarratorException;
import game.setups.Setup;
import models.enums.AbilityType;
import models.enums.FactionModifierName;
import models.enums.SetupModifierName;
import models.modifiers.FactionModifier;
import models.modifiers.Modifiers;
import models.schemas.FactionSchema;
import util.game.FactionRoleUtil;

public class Faction implements FactionGroup {
    public long id;
    public String name;
    public String color;
    public String description;

    // linked bc first ability will be shown first to user
    public LinkedHashSet<Ability> abilities;
    public Map<Role, FactionRole> roleMap;
    public Modifiers<FactionModifierName> modifiers;
    // this is improperly edited by
    // GameFaction.clearSheriffDetectables
    // GameFaction.removeSheriffDetectableTeam
    // GameFaction.addAlly
    public Set<Faction> sheriffCheckables;
    public Set<Faction> enemies;
    public Setup setup;

    public Faction(Setup setup, long id, String name, String color, String description) {
        this.setup = setup;
        this.id = id;
        this.name = name;
        this.color = color;
        this.description = description;

        this.abilities = new LinkedHashSet<>();
        this.roleMap = new HashMap<>();
        this.modifiers = new Modifiers<>();
        this.sheriffCheckables = new HashSet<>();
        this.enemies = new HashSet<>();
    }

    public Faction(Setup setup, FactionSchema schema) {
        this(setup, schema.id, schema.name, schema.color, schema.description);
    }

    public String getColor() {
        return this.color;
    }

    public String getName() {
        return name;
    }

    @Override
    public void setPriority(int i) {
        this.modifiers.add(new FactionModifier(FactionModifierName.WIN_PRIORITY, new ModifierValue(i), 0,
                Setup.MAX_PLAYER_COUNT, Instant.now()));
    }

    @Override
    public String toString() {
        return this.name;
    }

    public Ability getAbility(AbilityType abilityType) {
        for(Ability ability: this.abilities){
            if(ability.type.equals(abilityType))
                return ability;
        }
        return null;
    }

    public boolean hasAbility(AbilityType abilityType) {
        for(Ability ability: this.abilities){
            if(ability.type.equals(abilityType))
                return true;
        }
        return false;
    }

    @SafeVarargs
    final public boolean hasPossibleRolesWithAbility(AbilityType... abilityType) {
        for(Role role: this.roleMap.keySet())
            if(role.hasAbility(abilityType))
                return true;

        return false;
    }

    public Set<FactionRole> getFactionRoles() {
        return new HashSet<>(this.roleMap.values());
    }

    public Set<FactionRole> getFactionRolesWithAbility(AbilityType abilityType) {
        Set<FactionRole> factionRoles = new HashSet<>();
        for(FactionRole factionRole: this.roleMap.values())
            if(factionRole.role.hasAbility(abilityType))
                factionRoles.add(factionRole);

        return factionRoles;
    }

    public Set<FactionRole> getPossibleFactionRolesWithAbility(Game game, AbilityType abilityType) {
        Set<FactionRole> factionRoles = new HashSet<>();
        for(FactionRole factionRole: getPossibleFactionRoles(game))
            if(factionRole.role.hasAbility(abilityType))
                factionRoles.add(factionRole);

        return FactionRoleUtil.filterChatRoles(factionRoles, game);
    }

    public Set<FactionRole> getSpawnableFactionRoles(Game game) {
        Set<FactionRole> factionRoles = new HashSet<>();
        for(SetupHidden setupHidden: setup.rolesList.iterable(game))
            for(FactionRole factionRole: setupHidden.hidden.getSpawningFactionRoles(game))
                if(factionRole.faction == this)
                    factionRoles.add(factionRole);

        return FactionRoleUtil.filterChatRoles(factionRoles, game);
    }

    public Set<FactionRole> getPossibleFactionRoles(Game game) {
        Set<FactionRole> factionRoles = this.getSpawnableFactionRoles(game);
        if(game.getBool(SetupModifierName.EXECUTIONER_TO_JESTER)
                && FactionRoleUtil.hasFactionRoleWithAbility(factionRoles, Executioner.abilityType)){
            factionRoles.addAll(getFactionRolesWithAbility(Jester.abilityType));
        }
        if(FactionRoleUtil.hasFactionRoleWithAbility(factionRoles, CultLeader.abilityType)){
            if(game.getBool(SetupModifierName.CULT_KEEPS_ROLES)){
                factionRoles.addAll(getConvertedableFactionRoles(game));
            }else{
                factionRoles.addAll(this.getFactionRolesWithAbility(Cultist.abilityType));
            }
        }
        boolean hasSpawnableMasonLeader = FactionRoleUtil.hasFactionRoleWithAbility(factionRoles,
                MasonLeader.abilityType);
        if(hasSpawnableMasonLeader)
            factionRoles.addAll(this.getFactionRolesWithAbility(Mason.abilityType));
        return FactionRoleUtil.filterChatRoles(factionRoles, game);
    }

    private Set<FactionRole> getConvertedableFactionRoles(Game game) {
        Set<FactionRole> factionRoles = new HashSet<>();
        for(FactionRole factionRole: setup.getFactionRoles()){
            if(factionRole.faction != this && factionRole.isConvertable(game))
                factionRoles.add(factionRole);
        }
        return factionRoles;
    }

    public boolean hasPossibleRolesWithAbility(Optional<Game> game, AbilityType abilityType) {
        if(game.isPresent())
            return !getPossibleFactionRolesWithAbility(game.get(), abilityType).isEmpty();
        return hasPossibleRolesWithAbility(abilityType);
    }

    @SafeVarargs
    final public boolean hasSpawnableRolesWithAbility(Game game, AbilityType... abilityType) {
        for(FactionRole factionRole: this.setup.getSpawnableFactionRoles(game))
            if(factionRole.faction == this && factionRole.role.hasAbility(abilityType))
                return true;
        return false;
    }

    public Ability getAbility(long abilityID) {
        for(Ability ability: this.abilities)
            if(ability.id == abilityID)
                return ability;
        throw new NarratorException("No ability with that id found.");
    }

    public boolean hasNightChat(Game game) {
        return this.modifiers.getBoolean(FactionModifierName.HAS_NIGHT_CHAT, game);
    }

    public boolean knowsTeam(Game game) {
        return this.modifiers.getBoolean(FactionModifierName.KNOWS_ALLIES, game);
    }

    public boolean hasSharedAbilities() {
        return !this.abilities.isEmpty();
    }

}
