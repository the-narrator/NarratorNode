package models;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import game.logic.Player;
import models.idtypes.PlayerDBID;

public class Command {

    public Optional<PlayerDBID> playerID;
    public String text;
    public double timeLeft;

    public Command(Player player, String text, double timeLeft) {
        if(player != null && player != player.getSkipper())
            this.playerID = Optional.of(player.databaseID);
        else
            this.playerID = Optional.empty();
        this.text = text;
        this.timeLeft = timeLeft;
    }

    public Command(Optional<PlayerDBID> playerID, String text, double timeLeft) {
        this.playerID = playerID;
        this.text = text;
        this.timeLeft = timeLeft;
    }

    @Override
    public boolean equals(Object o) {
        if(o == null)
            return false;
        if(!Command.class.equals(o.getClass()))
            return false;
        Command command = (Command) o;
        return this.playerID.equals(command.playerID) && this.text.equals(command.text);
    }

    @Override
    public int hashCode() {
        return (playerID + " " + text).hashCode();
    }

    @Override
    public String toString() {
        return playerID + ":\t" + text + "\n";
    }

    public static List<String> getText(ArrayList<Command> commands) {
        List<String> texts = new ArrayList<>();
        for(Command command: commands)
            texts.add(command.text);
        return texts;
    }
}
