package models.idtypes;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class GameID extends IDType implements Comparable<GameID> {
    public GameID(long id) {
        super(id);
    }

    public static ArrayList<GameID> toIDList(ArrayList<Long> ids) {
        ArrayList<GameID> idList = new ArrayList<>();
        for(long id: ids)
            idList.add(new GameID(id));
        return idList;
    }

    public static Set<GameID> toIDSet(Set<Long> gameIDs) {
        Set<GameID> gameIDSet = new HashSet<>();
        for(long gameID: gameIDs)
            gameIDSet.add(new GameID(gameID));
        return gameIDSet;
    }

    @Override
    public int compareTo(GameID other) {
        return Long.compare(this.getValue(), other.getValue());
    }
}
