package models.modifiers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import game.logic.Game;
import game.logic.support.rules.ModifierName;
import models.ModifierValue;

public class Modifiers<T extends ModifierName> implements Iterable<Modifier<T>> {

    private List<Modifier<T>> modifierMap = new ArrayList<>();

    public Modifiers() {
    }

    public Modifiers(Collection<Modifier<T>> inputModifiers) {
        for(Modifier<T> modifier: inputModifiers)
            add(modifier);
    }

    public Set<Modifier<T>> getInRange(int playerSize) {
        Map<T, Modifier<T>> modifiers = new HashMap<>();
        for(Modifier<T> modifier: modifierMap)
            if(modifier.isInRange(playerSize))
                modifiers.put(modifier.name, modifier);

        return new HashSet<>(modifiers.values());
    }

    public void add(Modifier<T> newModifier) {
        Modifier<T> oldModifier;
        for(int i = 0; i < modifierMap.size(); i++){
            oldModifier = modifierMap.get(i);
            if(isSameModifier(oldModifier, newModifier)){
                modifierMap.remove(i);
                modifierMap.add(newModifier);
                return;
            }
        }
        modifierMap.add(newModifier);
    }

    public void removeAll(T name) {
        for(int i = 0; i < modifierMap.size(); i++)
            if(modifierMap.get(i).name == name)
                modifierMap.remove(i);
    }

    public Optional<Modifier<T>> getModifier(T modifierName, int playerSize) {
        Optional<Modifier<T>> matchedModifier = Optional.empty();
        for(Modifier<T> modifier: this.modifierMap){
            if(modifier.name != modifierName)
                continue;
            if(!modifier.isInRange(playerSize))
                continue;
            if(!matchedModifier.isPresent() || matchedModifier.get().upsertedAt.isBefore(modifier.upsertedAt))
                matchedModifier = Optional.of(modifier);
        }
        return matchedModifier;
    }

    private Optional<ModifierValue> getModifier(T modifierName) {
        Modifier<T> matchedModifier = null;
        for(Modifier<T> modifier: this.modifierMap){
            if(modifier.name != modifierName)
                continue;
            if(matchedModifier != null)
                return Optional.empty();
            matchedModifier = modifier;
        }
        if(matchedModifier == null)
            return Optional.empty();
        return Optional.of(matchedModifier.value);
    }

    public int getInt(T modifierName, Optional<Integer> optPlayerCount) {
        Optional<ModifierValue> modifier = getIfSingle(modifierName, optPlayerCount);
        if(modifier.isPresent())
            return modifier.get().getIntValue();
        return (int) modifierName.getDefaultValue();
    }

    public boolean getBoolean(T name, Optional<Integer> optPlayerCount) {
        return getBoolean(name, optPlayerCount, (boolean) name.getDefaultValue());
    }

    public boolean getBoolean(T modifierName, Optional<Integer> optPlayerCount, boolean defaultValue) {
        Optional<ModifierValue> modifier = getIfSingle(modifierName, optPlayerCount, defaultValue);
        if(modifier.isPresent())
            return modifier.get().getBoolValue();
        return defaultValue;
    }

    public String getString(T modifierName, Optional<Integer> optPlayerCount) {
        Optional<ModifierValue> modifier = getIfSingle(modifierName, optPlayerCount);
        if(modifier.isPresent())
            return modifier.get().getStringValue();
        return (String) modifierName.getDefaultValue();
    }

    public Optional<ModifierValue> getIfSingle(T modifierName, Optional<Integer> optPlayerCount) {
        return this.getIfSingle(modifierName, optPlayerCount, modifierName.getDefaultValue());
    }

    public Optional<ModifierValue> getIfSingle(T modifierName, Optional<Integer> optPlayerCount, Object defaultValue) {
        if(optPlayerCount.isPresent()){
            Optional<Modifier<T>> modifier = getModifier(modifierName, optPlayerCount.get());
            if(modifier.isPresent())
                return Optional.of(modifier.get().value);
            return Optional.of(new ModifierValue(defaultValue));
        }
        return getModifier(modifierName);
    }

    public int getInt(T modifierName, Game game) {
        return getInt(modifierName, game.players.size());
    }

    public int getInt(T modifierName, int playerSize) {
        Optional<Modifier<T>> modifier = getModifier(modifierName, playerSize);
        if(modifier.isPresent())
            return modifier.get().value.getIntValue();
        return (int) modifierName.getDefaultValue();
    }

    public boolean getBoolean(T modifierName, Game game) {
        return getBoolean(modifierName, game.players.size());
    }

    public boolean getBoolean(T modifierName, int playerSize) {
        Optional<Modifier<T>> modifier = getModifier(modifierName, playerSize);
        if(modifier.isPresent())
            return modifier.get().value.getBoolValue();
        return (boolean) modifierName.getDefaultValue();
    }

    public boolean hasKey(T modifierName) {
        for(Modifier<T> modifier: modifierMap)
            if(modifier.name == modifierName)
                return true;
        return false;
    }

    @Override
    public boolean equals(Object o) {
        if(o == null || !o.getClass().equals(getClass()))
            return false;
        if(o == this)
            return true;
        Modifiers<?> other = (Modifiers<?>) o;
        return other.modifierMap.equals(this.modifierMap);
    }

    @Override
    public Iterator<Modifier<T>> iterator() {
        return this.modifierMap.iterator();
    }

    private static <T extends ModifierName> boolean isSameModifier(Modifier<T> modifier1, Modifier<T> modifier2) {
        return modifier1.name == modifier2.name && modifier1.maxPlayerCount == modifier2.maxPlayerCount
                && modifier1.minPlayerCount == modifier2.minPlayerCount;
    }

    @Override
    public String toString() {
        return this.modifierMap.toString();
    }
}
