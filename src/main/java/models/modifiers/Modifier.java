package models.modifiers;

import java.time.Instant;

import game.logic.support.rules.ModifierName;
import models.ModifierValue;
import models.schemas.ModifierSchema;

public abstract class Modifier<T extends ModifierName> {
    public long id;
    public T name;
    public int minPlayerCount;
    public int maxPlayerCount;
    public ModifierValue value;
    public Instant upsertedAt;

    public Modifier(T name, ModifierValue value, int minPlayerCount, int maxPlayerCount, Instant updatedAt) {
        this.name = name;
        this.value = value;
        this.minPlayerCount = minPlayerCount;
        this.maxPlayerCount = maxPlayerCount;
        this.upsertedAt = updatedAt;
    }

    public Modifier(long id, T name, ModifierValue value, int minPlayerCount, int maxPlayerCount, Instant updatedAt) {
        this(name, value, minPlayerCount, maxPlayerCount, updatedAt);
        this.id = id;
    }

    public Modifier(ModifierSchema<T> schema) {
        this.id = schema.id;
        this.name = schema.name;
        this.value = new ModifierValue(schema.value);
        this.minPlayerCount = schema.minPlayerCount;
        this.maxPlayerCount = schema.maxPlayerCount;
        this.upsertedAt = schema.upsertedAt;
    }

    public Modifier(Modifier<T> modifier) {
        this.id = modifier.id;
        this.name = modifier.name;
        this.value = modifier.value;
        this.minPlayerCount = modifier.minPlayerCount;
        this.maxPlayerCount = modifier.maxPlayerCount;
        this.upsertedAt = modifier.upsertedAt;
    }

    @Override
    public String toString() {
        return id + "\t" + name.toString() + " : " + value.toString() + " (" + minPlayerCount + "->" + maxPlayerCount
                + ")";
    }

    public boolean isInRange(int playerSize) {
        return this.minPlayerCount <= playerSize && playerSize <= this.maxPlayerCount;
    }
}
