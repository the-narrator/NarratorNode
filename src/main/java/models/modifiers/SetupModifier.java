package models.modifiers;

import java.time.Instant;

import models.ModifierValue;
import models.enums.SetupModifierName;

public class SetupModifier extends Modifier<SetupModifierName> {

    public SetupModifier(long id, SetupModifierName name, ModifierValue value, int minPlayerCount, int maxPlayerCount,
            Instant updatedAt) {
        super(id, name, value, minPlayerCount, maxPlayerCount, updatedAt);
    }

    public SetupModifier(SetupModifierName name, ModifierValue modifierValue, int minPlayerCount, int maxPlayerCount,
            Instant updatedAt) {
        super(name, modifierValue, minPlayerCount, maxPlayerCount, updatedAt);
    }

    public SetupModifier(Modifier<SetupModifierName> modifier) {
        super(modifier);
    }

}
