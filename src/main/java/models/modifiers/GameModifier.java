package models.modifiers;

import java.time.Instant;

import game.setups.Setup;
import models.ModifierValue;
import models.enums.GameModifierName;

public class GameModifier extends Modifier<GameModifierName> {

    public GameModifier(GameModifierName modifierName, ModifierValue modifierValue) {
        super(modifierName, modifierValue, 0, Setup.MAX_PLAYER_COUNT, Instant.MIN);
    }

    public GameModifier(long id, GameModifierName name, ModifierValue value) {
        super(id, name, value, 0, Setup.MAX_PLAYER_COUNT, Instant.MIN);
    }
}
