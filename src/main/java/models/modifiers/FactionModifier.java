package models.modifiers;

import java.time.Instant;

import models.ModifierValue;
import models.enums.FactionModifierName;
import models.schemas.FactionModifierSchema;

public class FactionModifier extends Modifier<FactionModifierName> {

    public FactionModifier(FactionModifierName modifierName, ModifierValue modifierValue, int minPlayerCount,
            int maxPlayerCount, Instant upsertedAt) {
        super(modifierName, modifierValue, minPlayerCount, maxPlayerCount, upsertedAt);
    }

    public FactionModifier(FactionModifierSchema schema) {
        super(schema);
    }

}
