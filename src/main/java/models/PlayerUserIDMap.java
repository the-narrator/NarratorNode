package models;

import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import game.logic.Player;

public class PlayerUserIDMap {

    private Map<Player, Optional<Long>> map;

    public PlayerUserIDMap(Map<Player, Optional<Long>> playerUserMap) {
        this.map = playerUserMap;
    }

    public Optional<Long> getUserID(Player player) {
        Optional<Long> optUser = map.get(player);
        if(optUser == null || !optUser.isPresent())
            return Optional.empty();
        return Optional.of(optUser.get());
    }

    public Set<Long> getUserIDs() {
        Set<Long> ids = new HashSet<>();
        for(Optional<Long> userID: map.values()){
            if(userID.isPresent())
                ids.add(userID.get());
        }
        return ids;
    }

    public Set<Player> getPlayers() {
        return map.keySet();
    }

    public boolean hasUserID(Player player) {
        if(!map.containsKey(player))
            return false;
        return map.get(player).isPresent();
    }
}
