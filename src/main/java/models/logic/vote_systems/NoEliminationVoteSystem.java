package models.logic.vote_systems;

import game.event.VoteAnnouncement;
import game.logic.GameFaction;
import game.logic.Game;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.support.action.Action;
import models.enums.GamePhase;
import util.game.VoteUtil;

public class NoEliminationVoteSystem extends PluralityVoteSystem {

    public NoEliminationVoteSystem(Game narrator) {
        super(narrator);
    }

    @Override
    public int getMinAutoLynchVote(Player player, double timeLeft) {
        return 0;
    }

    @Override
    public VoteAnnouncement getUnvoteMessage(Action action) {
        return VoteUtil.getUnvoteAnnouncement(action);
    }

    @Override
    public VoteAnnouncement getVoteAnnouncement(Player voter, PlayerList targets, PlayerList prevTargets) {
        return VoteUtil.getVoteAnnouncement(voter, targets, prevTargets);
    }

    @Override
    public void checkVote(double timeLeft) {
    }

    @Override
    public void endPhase(double timeLeft) {
        if(game.phase == GamePhase.DISCUSSION_PHASE){
            endDiscussion();
            return;
        }
        GameFaction t = game.parityReached();
        game.endDay(t);
    }
}
