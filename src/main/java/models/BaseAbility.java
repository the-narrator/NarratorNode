package models;

import models.enums.AbilityType;

public class BaseAbility {
    public AbilityType type;
    public boolean isFactionAllowed;

    public BaseAbility(AbilityType type, boolean isFactionAllowed) {
        this.isFactionAllowed = isFactionAllowed;
        this.type = type;
    }

}
