package models.json_output.profile;

import java.util.ArrayList;
import java.util.Optional;

import game.abilities.GameAbility;
import game.logic.Player;
import game.logic.support.Option;
import json.JSONException;
import json.JSONObject;
import models.view.Jsonifiable;

public class OptionJson extends Jsonifiable {

    private Optional<String> color;
    private Optional<String> name;
    private Optional<OptionsTreeJson> map;
    private String value;

    OptionJson(Player player, GameAbility gameAbility, Option option, ArrayList<String> path) {
        this(option);

        ArrayList<String> nextPath = new ArrayList<>(path);
        nextPath.add(option.getValue());
        ArrayList<Option> subOptions = getNextOptions(player, gameAbility, nextPath);

        if(subOptions.isEmpty())
            this.map = Optional.empty();
        else{
            this.map = Optional.of(new OptionsTreeJson(player, gameAbility, nextPath, subOptions));
        }
    }

    public static ArrayList<Option> getNextOptions(Player player, GameAbility gameAbility, ArrayList<String> path) {
        switch (path.size()) {
        case 0:
            return gameAbility.getOptions(player);
        case 1:
            return gameAbility.getOptions2(player, path.get(0));
        case 2:
            return gameAbility.getOptions3(player, path.get(0), path.get(1));
        }
        return new ArrayList<>();
    }

    private OptionJson(Option option) {
        this.color = Optional.ofNullable(option.getColor());
        this.name = Optional.ofNullable(option.getName());
        this.value = option.getValue();
    }

    @Override
    public JSONObject toJson() throws JSONException {
        JSONObject jo = new JSONObject();
        jo.put("color", this.color);

        if(this.map.isPresent())
            jo.put("map", this.map.get());
        else
            jo.put("map", 0);

        jo.put("name", this.name);
        jo.put("val", this.value);
        return jo;
    }
}
