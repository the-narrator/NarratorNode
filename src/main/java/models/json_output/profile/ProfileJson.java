package models.json_output.profile;

import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import game.logic.Player;
import json.JSONException;
import json.JSONObject;
import models.PlayerUserIDMap;
import models.User;
import models.idtypes.GameID;
import models.idtypes.PlayerDBID;
import models.json_output.GraveJson;
import models.serviceResponse.ProfileResponse;
import models.view.Jsonifiable;

public class ProfileJson extends Jsonifiable {

    private Optional<Set<ProfileAllyJson>> allies;
    private Optional<GameID> gameID;
    private Optional<GraveJson> flip;
    private Optional<Boolean> isJailed;
    private Optional<Boolean> isPuppeted;
    private Optional<Boolean> isSilenced;
    private Optional<String> lastWill;
    private Optional<String> name; // player name
    private Optional<ProfileRoleCardJson> roleCard;
    private Optional<PlayerDBID> playerID;
    private Optional<String> startingPlayerName;
    private long userID;
    private String userName;

    public ProfileJson(ProfileResponse profile) {
        this(profile.gameID, profile.user, profile.playerID, profile.playerName, profile.player);
    }

    public ProfileJson(Optional<GameID> gameID, User user, Optional<PlayerDBID> playerID, Optional<String> playerName,
            Optional<Player> player) {
        this.allies = getAllies(player);
        this.gameID = gameID;
        this.flip = getFlip(player);
        this.isJailed = getIsJailed(player);
        this.isPuppeted = getIsPuppeted(player);
        this.isSilenced = getIsSilenced(player);
        this.lastWill = getLastWill(player);
        this.name = playerName;
        this.roleCard = getRoleCard(player);
        this.startingPlayerName = getStartingPlayerName(player);
        this.playerID = playerID;
        this.userID = user.id;
        this.userName = user.name;
    }

    public ProfileJson(GameID gameID, User user, Player player) {
        this(Optional.of(gameID), user, Optional.of(player.databaseID), Optional.of(player.getName()),
                Optional.of(player));
    }

    public static Set<ProfileJson> toSet(GameID gameID, PlayerUserIDMap playerUserMap, Map<Long, String> userIDtoName) {
        Set<ProfileJson> profiles = new HashSet<>();

        long userID;
        for(Player player: playerUserMap.getPlayers())
            if(playerUserMap.hasUserID(player)){
                userID = playerUserMap.getUserID(player).get();
                profiles.add(new ProfileJson(gameID, new User(userID, userIDtoName.get(userID)), player));
            }

        return profiles;
    }

    private static Optional<ProfileRoleCardJson> getRoleCard(Optional<Player> player) {
        if(player.isPresent())
            return Optional.of(new ProfileRoleCardJson(player.get()));
        return Optional.empty();
    }

    private static Optional<String> getStartingPlayerName(Optional<Player> player) {
        if(player.isPresent())
            return Optional.of(player.get().getID());
        return Optional.empty();
    }

    private static Optional<GraveJson> getFlip(Optional<Player> playerOpt) {
        if(!playerOpt.isPresent())
            return Optional.empty();
        Player player = playerOpt.get();
        if(player.isAlive())
            return Optional.empty();
        return Optional.of(new GraveJson(player));
    }

    private static Optional<Boolean> getIsJailed(Optional<Player> player) {
        if(player.isPresent())
            return Optional.of(player.get().isJailed());
        return Optional.empty();
    }

    private static Optional<String> getLastWill(Optional<Player> player) {
        if(player.isPresent())
            return Optional.ofNullable(player.get()._lastWill);
        return Optional.empty();
    }

    private static Optional<Boolean> getIsSilenced(Optional<Player> player) {
        if(player.isPresent())
            return Optional.of(player.get().isSilenced());
        return Optional.empty();
    }

    private static Optional<Boolean> getIsPuppeted(Optional<Player> player) {
        if(player.isPresent())
            return Optional.of(player.get().isPuppeted());
        return Optional.empty();
    }

    public static Optional<Set<ProfileAllyJson>> getAllies(Optional<Player> player) {
        if(player.isPresent())
            return Optional.of(ProfileAllyJson.getSet(player.get()));
        return Optional.empty();
    }

    @Override
    public JSONObject toJson() throws JSONException {
        JSONObject response = new JSONObject();

        response.put("allies", this.allies);
        response.put("flip", this.flip);
        response.put("gameID", this.gameID);
        response.put("isJailed", this.isJailed);
        response.put("isPuppeted", this.isPuppeted);
        response.put("isSilenced", this.isSilenced);
        response.put("lastWill", this.lastWill);
        response.put("name", this.name);
        response.put("playerID", this.playerID);
        response.put("roleCard", this.roleCard);
        response.put("startingPlayerName", this.startingPlayerName);
        response.put("userID", this.userID);
        response.put("userName", this.userName);

        return response;
    }
}
