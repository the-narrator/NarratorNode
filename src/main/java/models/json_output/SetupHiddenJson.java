package models.json_output;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import game.logic.Game;
import game.logic.RolesList;
import json.JSONException;
import json.JSONObject;
import models.SetupHidden;
import models.view.Jsonifiable;
import util.game.SetupHiddenUtil;

public class SetupHiddenJson extends Jsonifiable {

    private Optional<Long> factionRoleID;
    private long hiddenID;
    private long id;
    private boolean isExposed;
    private int maxPlayerCount;
    private int minPlayerCount;
    private boolean mustSpawn;
    private long setupID;

    public SetupHiddenJson(SetupHidden setupHidden, Optional<Long> factionRoleID) {
        this.factionRoleID = factionRoleID;
        this.hiddenID = setupHidden.hidden.id;
        this.id = setupHidden.id;
        this.isExposed = setupHidden.isExposed;
        this.mustSpawn = setupHidden.mustSpawn;
        this.maxPlayerCount = setupHidden.maxPlayerCount;
        this.minPlayerCount = setupHidden.minPlayerCount;
        this.setupID = setupHidden.setup.id;
    }

    public static Set<SetupHiddenJson> getSet(RolesList setupHiddens, Optional<Game> game) {
        Set<SetupHiddenJson> setupHiddenViews = new HashSet<>();

        Iterable<SetupHidden> iterable = game.isPresent() && game.get().isStarted ? setupHiddens.iterable(game.get())
                : setupHiddens.iterable();

        SetupHiddenJson setupHiddenView;
        for(SetupHidden setupHidden: iterable){
            setupHiddenView = new SetupHiddenJson(setupHidden, getFactionRoleID(setupHidden, game));
            setupHiddenViews.add(setupHiddenView);
        }

        return setupHiddenViews;
    }

    private static Optional<Long> getFactionRoleID(SetupHidden setupHidden, Optional<Game> game) {
        if(game.isPresent() && game.get().isStarted())
            return Optional.of(SetupHiddenUtil.getSpawnedFactionRole(game.get(), setupHidden).id);
        return Optional.empty();
    }

    @Override
    public JSONObject toJson() throws JSONException {
        JSONObject response = new JSONObject();
        response.put("factionRoleID", this.factionRoleID);
        response.put("hiddenID", this.hiddenID);
        response.put("id", this.id);
        response.put("setupID", this.setupID);
        response.put("isExposed", this.isExposed);
        response.put("mustSpawn", this.mustSpawn);
        response.put("minPlayerCount", this.minPlayerCount);
        response.put("maxPlayerCount", this.maxPlayerCount);
        return response;
    }
}
