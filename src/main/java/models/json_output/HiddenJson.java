package models.json_output;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import game.abilities.Hidden;
import game.logic.Game;
import json.JSONException;
import json.JSONObject;
import models.view.Jsonifiable;

public class HiddenJson extends Jsonifiable {

    private long id;
    private String name;
    private Set<HiddenSpawnJson> spawns;

    public HiddenJson(Hidden hidden, Optional<Game> game) {
        this.id = hidden.id;
        this.name = hidden.name;
        this.spawns = HiddenSpawnJson.getSet(hidden, hidden.getSpawningSpawns(game));
    }

    public static Set<HiddenJson> getSet(Set<Hidden> hiddens, Optional<Game> game) {
        Set<HiddenJson> hiddenViews = new HashSet<>();

        HiddenJson hiddenView;
        for(Hidden hidden: hiddens){
            hiddenView = new HiddenJson(hidden, game);
            hiddenViews.add(hiddenView);
        }

        return hiddenViews;
    }

    @Override
    public JSONObject toJson() throws JSONException {
        JSONObject hiddenJson = new JSONObject();
        hiddenJson.put("id", this.id);
        hiddenJson.put("name", this.name);
        hiddenJson.put("spawns", toJson(this.spawns));
        return hiddenJson;
    }
}
