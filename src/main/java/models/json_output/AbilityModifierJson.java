package models.json_output;

import java.time.Instant;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import game.abilities.GameAbility;
import game.logic.Game;
import game.setups.Setup;
import json.JSONException;
import json.JSONObject;
import models.Ability;
import models.ModifierValue;
import models.enums.AbilityModifierName;
import models.modifiers.Modifier;
import models.modifiers.Modifiers;
import models.view.Jsonifiable;
import util.game.GameUtil;
import util.game.ModifierUtil;

public class AbilityModifierJson extends Jsonifiable {
    private AbilityModifierName name;
    private String label;
    private boolean isOverride;
    private ModifierValue value;
    private int minPlayerCount;
    private int maxPlayerCount;
    private Instant upsertedAt;

    public AbilityModifierJson(AbilityModifierName name, Modifiers<AbilityModifierName> modifiers, boolean isOverride,
            ModifierValue value, Ability ability, Optional<Game> game, Setup setup, boolean singleAbilityRole,
            int minPlayerCount, int maxPlayerCount, Instant upsertedAt) {
        this.name = name;
        this.label = getLabel(ability, modifiers, game, setup, name, singleAbilityRole);
        this.isOverride = isOverride;
        this.value = value;
        this.minPlayerCount = minPlayerCount;
        this.maxPlayerCount = maxPlayerCount;
        this.upsertedAt = upsertedAt;
    }

    public static Set<AbilityModifierJson> getSet(Ability ability, Modifiers<AbilityModifierName> resolvedModifiers,
            Optional<Game> game, boolean singleAbilityRole) {

        Setup setup = ability.setup;
        Set<AbilityModifierJson> modifierViews = new HashSet<>();
        Set<AbilityModifierName> seenModifierNames = new HashSet<>();

        for(Modifier<AbilityModifierName> modifier: resolvedModifiers){
            modifierViews.add(new AbilityModifierJson(modifier.name, resolvedModifiers, true, modifier.value, ability, game,
                    setup, singleAbilityRole, modifier.minPlayerCount, modifier.maxPlayerCount, modifier.upsertedAt));
            if(ModifierUtil.isDefault(modifier))
                seenModifierNames.add(modifier.name);
        }
        for(AbilityModifierName modifierName: ability.getAbilityModifiers()){
            if(seenModifierNames.contains(modifierName))
                continue;
            modifierViews.add(new AbilityModifierJson(modifierName, resolvedModifiers, false,
                    new ModifierValue(modifierName.defaultValue), ability, game, setup, singleAbilityRole, 0,
                    Setup.MAX_PLAYER_COUNT, Instant.MIN));

        }
        return modifierViews;
    }

    private static String getLabel(Ability ability, Modifiers<AbilityModifierName> modifiers, Optional<Game> game,
            Setup setup, AbilityModifierName modifierName, boolean singleAbility) {
        GameAbility gameAbility = ability.getGameAbility(game, modifiers);
        Optional<Integer> playerCount = GameUtil.getPlayerCount(game);
        switch (modifierName) {
        case BACK_TO_BACK:
            return gameAbility.getBackToBackLabel();
        case CHARGES:
            return gameAbility.getChargeDescription(playerCount, setup);
        case COOLDOWN:
            return gameAbility.getCooldownDescription();
        case HIDDEN:
            return gameAbility.getHiddenStatusRuleLabel();
        case SELF_TARGET:
            return gameAbility.getSelfTargetMemberDescription(singleAbility, true);
        case ZERO_WEIGHTED:
            return gameAbility.getZeroWeightDescription();
        default:
            return modifierName.getTextValue(playerCount, setup.modifiers, ability.modifiers);
        }
    }

    @Override
    public JSONObject toJson() throws JSONException {
        JSONObject jModifier = new JSONObject();
        jModifier.put("name", this.name.toString());
        jModifier.put("label", this.label);
        jModifier.put("isOverride", this.isOverride);
        jModifier.put("value", this.value.internalValue);
        jModifier.put("minPlayerCount", this.minPlayerCount);
        jModifier.put("maxPlayerCount", this.maxPlayerCount);
        jModifier.put("upsertedAt", this.upsertedAt);

        return jModifier;
    }
}
