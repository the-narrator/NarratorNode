package models.json_output;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import game.logic.Game;
import game.logic.Player;
import game.logic.PlayerList;
import json.JSONException;
import json.JSONObject;
import models.OpenLobby;
import models.enums.GamePhase;
import models.view.Jsonifiable;

public class VoteData extends Jsonifiable {
    private Set<String> allowedTargets;
    private VoteSettingsJson settings;
    private Optional<String> trialedPlayerName;
    private Map<String, Set<String>> voterToVotes;

    public VoteData(Game game) {
        this.allowedTargets = getElligibleTargets(game);
        this.settings = new VoteSettingsJson(game);
        this.trialedPlayerName = getTrialedPlayerName(game);
        this.voterToVotes = getVoterToVotesMap(game);
    }

    public VoteData(OpenLobby lobby) {
        this.allowedTargets = new HashSet<>();
        this.settings = new VoteSettingsJson(lobby);
        this.trialedPlayerName = Optional.empty();
        this.voterToVotes = new HashMap<>();
    }

    private static Set<String> getElligibleTargets(Game game) {
        Set<String> elligibleTargets = new HashSet<>();
        if(game.isStarted())
            for(Player p: game.voteSystem.getElligibleTargets())
                elligibleTargets.add(p.getName());
        return elligibleTargets;
    }

    private static Optional<String> getTrialedPlayerName(Game game) {
        if(game.phase != GamePhase.TRIAL_PHASE)
            return Optional.empty();
        return Optional.of(game.voteSystem.getPlayerOnTrial().getName());
    }

    private static Map<String, Set<String>> getVoterToVotesMap(Game game) {
        Map<String, Set<String>> votersToVotes = new HashMap<>();
        if(!game.isStarted())
            return votersToVotes;
        PlayerList voteTargets;
        Set<String> jVotes;
        for(Player votingPlayer: game.getLivePlayers().add(new PlayerList(game.skipper))){
            voteTargets = game.voteSystem.getVoteTargets(votingPlayer);
            jVotes = new HashSet<>();
            for(Player votedTarget: voteTargets)
                jVotes.add(votedTarget.getName());

            votersToVotes.put(votingPlayer.getName(), jVotes);
        }
        return votersToVotes;
    }

    @Override
    public JSONObject toJson() throws JSONException {
        JSONObject voteHolder = new JSONObject();

        voteHolder.put("allowedTargets", this.allowedTargets);
        voteHolder.put("settings", this.settings);
        voteHolder.put("trialedPlayerName", this.trialedPlayerName);
        voteHolder.put("voterToVotes", this.voterToVotes);

        return voteHolder;
    }
}
