package models.json_output;

import game.logic.Game;
import game.logic.support.Constants;
import json.JSONException;
import json.JSONObject;
import models.OpenLobby;
import models.enums.GameModifierName;
import models.enums.GamePhase;
import models.view.Jsonifiable;
import util.game.PhaseUtil;

public class PhaseJson extends Jsonifiable {

    private int dayNumber;
    private String hash;
    private long length;
    private int livePlayerCount;
    private GamePhase name;
    private double timeLeft;

    public PhaseJson(Game game) {
        this.dayNumber = game.dayNumber;
        this.hash = game.phaseHash;
        this.length = getLength(game);
        this.livePlayerCount = game.players.getLivePlayers().size();
        this.name = game.phase;
        this.timeLeft = game.timeLeft;
    }

    public PhaseJson(OpenLobby lobby) {
        this.dayNumber = 0;
        this.hash = Constants.STARTING_HASH;
        this.length = 0;
        this.livePlayerCount = lobby.players.size();
        this.name = GamePhase.UNSTARTED;
        this.timeLeft = Constants.ALL_TIME_LEFT;
    }

    @Override
    public JSONObject toJson() throws JSONException {
        JSONObject request = new JSONObject();
        request.put("dayNumber", this.dayNumber);
        request.put("hash", this.hash);
        request.put("length", this.length);
        request.put("livePlayerCount", this.livePlayerCount);
        request.put("name", this.name.toString());
        request.put("timeLeft", this.timeLeft);
        return request;
    }

    private static long getLength(Game game) {
        switch (game.phase) {
        case ROLE_PICKING_PHASE:
            return game.getInt(GameModifierName.ROLE_PICKING_LENGTH);
        case DISCUSSION_PHASE:
            return game.getInt(GameModifierName.DISCUSSION_LENGTH);
        case TRIAL_PHASE:
            return game.getInt(GameModifierName.TRIAL_LENGTH);
        case VOTE_PHASE:
            return PhaseUtil.getDayLength(game);
        case NIGHT_ACTION_SUBMISSION:
            return game.getInt(GameModifierName.NIGHT_LENGTH);
        default:
            return 0;
        }
    }

}
