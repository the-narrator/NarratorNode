package models.json_output;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import game.abilities.FactionSend;
import game.abilities.Spy;
import game.event.Message;
import game.event.SelectionMessage;
import game.logic.Game;
import game.logic.GameFaction;
import game.logic.Player;
import game.logic.support.action.Action;
import game.logic.support.action.ActionList;
import game.logic.templates.HTMLDecoder;
import json.JSONException;
import json.JSONObject;
import models.view.Jsonifiable;
import util.CollectionUtil;

public class ActionJson extends Jsonifiable {

    private String command;
    private Optional<String> option;
    private Optional<String> option2;
    private Optional<String> option3;
    private String simpleText;
    private String text;
    private List<String> targets;

    private ActionJson(Action action) {
        this.command = action.abilityType.command.toLowerCase();
        this.targets = getTargets(action);
        this.option = Optional.ofNullable(action.getArg1());
        this.option2 = Optional.ofNullable(action.getArg2());
        this.option3 = Optional.ofNullable(action.getArg3());

        Player player = action.owner;
        SelectionMessage sm = new SelectionMessage(player, true, false);
        Message actionDescription = sm.add(action.getAbility().getActionDescription(action));
        this.text = "You will " + actionDescription.access(player, new HTMLDecoder()) + ".";
        this.simpleText = "Will " + actionDescription.access(Message.PRIVATE);
    }

    public static Set<ActionJson> getSet(Player player) {
        Set<ActionJson> actionViews = new HashSet<>();
        ActionList actionList = player.getActions();
        if(actionList == null || player.isEliminated())
            return actionViews;
        if(player.isEliminated())
            return actionViews;

        Game game = player.game;
        for(Action action: actionList){
            if(action.is(FactionSend.abilityType) && !game.setup.hasFactionRolesThatAffectSending(game))
                continue;
            if(action.abilityType.command == null)
                continue;

            actionViews.add(new ActionJson(action));
        }
        return actionViews;
    }

    private static List<String> getTargets(Action action) {
        if(action.is(Spy.abilityType)){
            Game game = action.owner.game;
            GameFaction t = game.getFaction(action.getArg1());
            return CollectionUtil.toArrayList(t.getName());
        }

        LinkedList<String> targets = new LinkedList<>();
        for(Player target: action.getTargets())
            targets.add(target.getName());
        return targets;
    }

    @Override
    public JSONObject toJson() throws JSONException {
        JSONObject jAction = new JSONObject();

        jAction.put("command", this.command);
        jAction.put("option", this.option);
        jAction.put("option2", this.option2);
        jAction.put("option3", this.option3);
        jAction.put("playerNames", this.targets);
        jAction.put("simpleText", this.simpleText);
        jAction.put("text", this.text);

        return jAction;
    }
}
