package models.json_output;

import java.util.Optional;

import game.event.DayChat;
import game.event.EventLog;
import game.event.VoidChat;
import game.logic.Game;
import game.logic.Player;
import json.JSONArray;
import json.JSONException;
import json.JSONObject;
import models.PlayerUserIDMap;

public class ChatsJson {

    public static JSONArray toJson(Game game, PlayerUserIDMap playerUserIDMap) throws JSONException {
        JSONArray jo = new JSONArray();

        JSONObject chatO;
        JSONObject members;
        Optional<Long> userID;
        for(EventLog el: game.getEventManager().getAllChats()){
            if(el instanceof DayChat)
                continue;
            if(el instanceof VoidChat)
                continue;

            chatO = new JSONObject();
            chatO.put("name", el.getName());
            chatO.put("type", el.getClass().getSimpleName());
            chatO.put("active", el.isActive());

            members = new JSONObject();
            for(Player player: el.getMembers()){
                userID = playerUserIDMap.getUserID(player);
                if(userID.isPresent())
                    members.put(Long.toString(userID.get()), el.getKey(player));
            }
            chatO.put("members", members);

            jo.put(chatO);
        }
        return jo;
    }

}
