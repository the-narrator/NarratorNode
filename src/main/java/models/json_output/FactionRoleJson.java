package models.json_output;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import game.logic.Game;
import game.setups.Setup;
import json.JSONException;
import json.JSONObject;
import models.Ability;
import models.FactionRole;
import models.enums.AbilityModifierName;
import models.enums.RoleModifierName;
import models.modifiers.Modifiers;
import models.view.Jsonifiable;
import util.game.GameUtil;
import util.game.ModifierUtil;

public class FactionRoleJson extends Jsonifiable {

    // linked bc first ability will be shown first to user
    private LinkedHashSet<AbilityJson> abilities;
    private List<String> details;
    private long factionID;
    private long id;
    private Set<ModifierJson<RoleModifierName>> modifiers;
    private String name;
    private Optional<Long> receivedFactionRoleID;
    private long roleID;
    private FactionOverviewJson team;

    public FactionRoleJson(FactionRole factionRole, Optional<Game> game) {
        Optional<Integer> playerCount = GameUtil.getPlayerCount(game);
        Modifiers<RoleModifierName> mergedRoleModifiers = ModifierUtil.mergeModifiers(playerCount,
                factionRole.role.modifiers, factionRole.roleModifiers);

        this.abilities = AbilityJson.getSet(factionRole.role.getAbilities(), Optional.of(factionRole.getColor()), name,
                game, factionRole.abilityModifiers);
        this.details = getDetails(factionRole, game);
        this.factionID = factionRole.faction.id;
        this.id = factionRole.id;
        this.modifiers = ModifierJson.getRoleModifiers(playerCount, factionRole.faction.setup.modifiers,
                mergedRoleModifiers);
        this.name = factionRole.getName(); // deprecated. not all roles have ids yet
        this.receivedFactionRoleID = factionRole.receivedRole == null ? Optional.empty()
                : Optional.of(factionRole.receivedRole.id);
        this.roleID = factionRole.role.id;
        this.team = new FactionOverviewJson(factionRole.faction);
    }

    public FactionRoleJson(FactionRole factionRole, Game game) {
        this(factionRole, Optional.of(game));
    }

    public static Set<FactionRoleJson> getSet(Set<FactionRole> factionRoles, Optional<Game> game) {
        Set<FactionRoleJson> factionRoleViews = new HashSet<>();

        FactionRoleJson factionRoleView;
        for(FactionRole factionRole: factionRoles){
            factionRoleView = new FactionRoleJson(factionRole, game);
            factionRoleViews.add(factionRoleView);
        }

        return factionRoleViews;
    }

    private static ArrayList<String> getDetails(FactionRole factionRole, Optional<Game> game) {
        ArrayList<String> list = new ArrayList<>();
        Setup setup = factionRole.faction.setup;
        Modifiers<AbilityModifierName> modifiers;
        for(Ability a: factionRole.role.getAbilities()){
            modifiers = factionRole.getAbilityModifiers(a.type);
            for(String s: a.getGameAbility(game, modifiers).getRoleBlurb(game, setup))
                list.add(s);
        }
        list.addAll(factionRole.getPublicDescription(game));
        return list;
    }

    @Override
    public JSONObject toJson() throws JSONException {
        JSONObject factionRoleJson = new JSONObject();
        factionRoleJson.put("abilities", this.abilities);
        factionRoleJson.put("details", details);
        factionRoleJson.put("factionID", this.factionID);
        factionRoleJson.put("id", this.id);
        factionRoleJson.put("modifiers", modifiers);
        factionRoleJson.put("name", this.name);
        factionRoleJson.put("receivedFactionRoleID", this.receivedFactionRoleID);
        factionRoleJson.put("roleID", this.roleID);
        factionRoleJson.put("team", this.team);
        return factionRoleJson;
    }

    public static long getID(JSONObject object) throws JSONException {
        return object.getLong("id");
    }

}
