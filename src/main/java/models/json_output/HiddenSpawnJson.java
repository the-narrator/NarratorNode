package models.json_output;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import game.abilities.Hidden;
import json.JSONException;
import json.JSONObject;
import models.HiddenSpawn;
import models.schemas.HiddenSpawnSchema;
import models.view.Jsonifiable;

public class HiddenSpawnJson extends Jsonifiable {

    private long factionRoleID;
    private long hiddenID;
    private long id;
    private int maxPlayerCount;
    private int minPlayerCount;

    public HiddenSpawnJson(Hidden hidden, HiddenSpawn spawn) {
        this.id = spawn.id;
        this.factionRoleID = spawn.factionRole.id;
        this.hiddenID = hidden.id;
        this.maxPlayerCount = spawn.maxPlayerCount;
        this.minPlayerCount = spawn.minPlayerCount;
    }

    public HiddenSpawnJson(HiddenSpawnSchema spawn) {
        this.factionRoleID = spawn.factionRoleID;
        this.hiddenID = spawn.hiddenID;
        this.id = spawn.id;
        this.maxPlayerCount = spawn.maxPlayerCount;
        this.minPlayerCount = spawn.minPlayerCount;
    }

    public static Set<HiddenSpawnJson> getSet(Collection<HiddenSpawnSchema> spawns) {
        Set<HiddenSpawnJson> views = new HashSet<>();
        for(HiddenSpawnSchema spawn: spawns)
            views.add(new HiddenSpawnJson(spawn));
        return views;
    }

    public static Set<HiddenSpawnJson> getSet(Hidden hidden, Collection<HiddenSpawn> spawns) {
        Set<HiddenSpawnJson> output = new HashSet<>();
        for(HiddenSpawn hiddenSpawnSchema: spawns)
            output.add(new HiddenSpawnJson(hidden, hiddenSpawnSchema));
        return output;
    }

    @Override
    public JSONObject toJson() throws JSONException {
        JSONObject output = new JSONObject();
        output.put("id", this.id);
        output.put("hiddenID", this.hiddenID);
        output.put("factionRoleID", this.factionRoleID);
        output.put("minPlayerCount", this.minPlayerCount);
        output.put("maxPlayerCount", this.maxPlayerCount);
        return output;
    }
}
