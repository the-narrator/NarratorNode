package models.json_output;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import game.logic.Game;
import json.JSONException;
import json.JSONObject;
import models.Role;
import models.enums.RoleModifierName;
import models.view.Jsonifiable;
import util.game.GameUtil;

public class RoleJson extends Jsonifiable {

    // linked bc first ability will be shown first to user
    private LinkedHashSet<AbilityJson> abilities;
    private List<String> details;
    private long id;
    private Set<ModifierJson<RoleModifierName>> modifiers;
    private String name;

    public RoleJson(Role role, Optional<Game> game) {
        this.abilities = AbilityJson.getSet(role.getAbilities(), Optional.empty(), role.name, game, new HashMap<>());
        this.details = role.getPublicDescription(game);
        this.id = role.id;
        this.modifiers = ModifierJson.getRoleModifiers(GameUtil.getPlayerCount(game), role.setup.modifiers,
                role.modifiers);
        this.name = role.name;
    }

    public static Set<RoleJson> getSet(Set<Role> roles, Optional<Game> game) {
        Set<RoleJson> roleViews = new HashSet<>();

        RoleJson roleView;
        for(Role role: roles){
            roleView = new RoleJson(role, game);
            roleViews.add(roleView);
        }

        return roleViews;
    }

    @Override
    public JSONObject toJson() throws JSONException {
        JSONObject roleJson = new JSONObject();
        roleJson.put("abilities", toJson(this.abilities));
        roleJson.put("id", this.id);
        roleJson.put("details", this.details);
        roleJson.put("modifiers", toJson(this.modifiers));
        roleJson.put("name", this.name);
        return roleJson;
    }
}
