package models.json_output;

import java.util.Optional;

import game.logic.Game;
import json.JSONException;
import json.JSONObject;
import models.OpenLobby;
import models.enums.GameModifierName;
import models.enums.SetupModifierName;
import models.enums.VoteSystemTypes;
import models.view.Jsonifiable;

public class VoteSettingsJson extends Jsonifiable {

    private boolean secretVoting;
    private Optional<VoteSystemTypes> voteSystemType;

    public VoteSettingsJson(Game game) {
        this.secretVoting = game.getBool(SetupModifierName.SECRET_VOTES);
        VoteSystemTypes voteSystem = VoteSystemTypes.getByValue(game.getInt(GameModifierName.VOTE_SYSTEM));
        if(voteSystem == VoteSystemTypes.PLURALITY || voteSystem == VoteSystemTypes.DIMINISHING_POOL)
            this.voteSystemType = Optional.of(voteSystem);
        else
            this.voteSystemType = Optional.empty();
    }

    public VoteSettingsJson(OpenLobby lobby) {
        this.secretVoting = false;
        this.voteSystemType = Optional.empty();
    }

    public static JSONObject toJson(Game game) throws JSONException {
        JSONObject settings = new JSONObject();
        int voteSystem = game.getInt(GameModifierName.VOTE_SYSTEM);
        if(voteSystem == VoteSystemTypes.PLURALITY.value)
            settings.put("voteSystemType", "plurality");
        else if(voteSystem == VoteSystemTypes.DIMINISHING_POOL.value)
            settings.put("voteSystemType", "diminishing_pool");
        settings.put("secretVoting", game.getBool(SetupModifierName.SECRET_VOTES));
        return settings;
    }

    @Override
    public JSONObject toJson() throws JSONException {
        JSONObject settings = new JSONObject();
        if(this.voteSystemType.isPresent())
            settings.put("voteSystemType", this.voteSystemType.get().name);
        settings.put("secretVoting", this.secretVoting);
        return settings;
    }
}
