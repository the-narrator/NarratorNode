package models.json_output;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import game.abilities.CultLeader;
import game.logic.Game;
import json.JSONException;
import json.JSONObject;
import models.Faction;
import models.FactionRole;
import models.ModifierValue;
import models.enums.FactionModifierName;
import models.enums.SetupModifierName;
import models.view.Jsonifiable;
import util.game.GameUtil;

public class FactionJson extends Jsonifiable {

    private Set<AbilityJson> abilities;
    private Set<Long> checkableIDs;
    private String color;
    private String description;
    private List<String> details;
    private Set<FactionOverviewJson> enemies;
    private Set<Long> enemyIDs;
    private Set<FactionRoleJson> factionRoles;
    private long factionID;
    private Set<FactionModifierJson> modifiers;
    private String name;

    public FactionJson(Faction faction, Optional<Game> game) {
        this.abilities = AbilityJson.getSet(faction.abilities, Optional.of(faction.color), faction.name, game,
                new HashMap<>());
        this.checkableIDs = getCheckableIDs(faction);
        this.color = faction.color;
        this.description = faction.description;
        this.details = getRules(faction, game);
        this.enemies = FactionOverviewJson.getSet(faction.enemies);
        this.enemyIDs = getEnemyIDs(faction);
        this.factionRoles = getFactionRoles(faction, game);
        this.factionID = faction.id;
        this.modifiers = FactionModifierJson.getSet(faction, GameUtil.getPlayerCount(game));
        this.name = faction.name;
    }

    private static Set<FactionRoleJson> getFactionRoles(Faction faction, Optional<Game> game) {
        Set<FactionRole> factionRoles;

        boolean isGameStarted = game.isPresent() ? game.get().isStarted() : false;
        if(isGameStarted)
            factionRoles = faction.getPossibleFactionRoles(game.get());
        else{
            factionRoles = faction.getFactionRoles();
        }

        return FactionRoleJson.getSet(factionRoles, game);
    }

    public static Set<FactionJson> getSet(Set<Faction> factions, Optional<Game> game) {
        Set<FactionJson> factionViews = new HashSet<>();

        FactionJson factionView;
        for(Faction faction: factions){
            factionView = new FactionJson(faction, game);
            factionViews.add(factionView);
        }

        return factionViews;
    }

    private static Set<Long> getEnemyIDs(Faction faction) {
        Set<Long> enemyIDs = new HashSet<>();
        for(Faction enemyFaction: faction.enemies)
            enemyIDs.add(enemyFaction.id);
        return enemyIDs;
    }

    private static Set<Long> getCheckableIDs(Faction faction) {
        Set<Long> checkableIDs = new HashSet<>();
        for(Faction checkableFaction: faction.sheriffCheckables)
            checkableIDs.add(checkableFaction.id);
        return checkableIDs;
    }

    @Override
    public JSONObject toJson() throws JSONException {
        JSONObject factionJson = new JSONObject();
        factionJson.put("abilities", toJson(this.abilities));
        factionJson.put("checkableIDs", this.checkableIDs);
        factionJson.put("color", this.color);
        factionJson.put("description", this.description);
        factionJson.put("details", this.details);
        factionJson.put("enemies", toJson(this.enemies));
        factionJson.put("enemyIDs", this.enemyIDs);
        factionJson.put("factionRoles", toJson(this.factionRoles));
        factionJson.put("id", this.factionID);
        factionJson.put("modifiers", toJson(this.modifiers));
        factionJson.put("name", this.name);
        return factionJson;
    }

    public static ArrayList<String> getRules(Faction faction, Optional<Game> game) {
        ArrayList<String> teamRules = new ArrayList<>();

        Optional<Integer> playerCount = GameUtil.getPlayerCount(game);
        Optional<ModifierValue> knowsAlliesModifier = faction.modifiers.getIfSingle(FactionModifierName.KNOWS_ALLIES,
                playerCount);
        boolean ifMightKnowAllies = !knowsAlliesModifier.isPresent() || knowsAlliesModifier.get().getBoolValue();
        if(ifMightKnowAllies){
            String nightChatDescription = FactionModifierName.HAS_NIGHT_CHAT.getTextValue(playerCount, faction);
            teamRules.add(nightChatDescription);
        }

        String knowsAlliesDescription = FactionModifierName.KNOWS_ALLIES.getTextValue(playerCount, faction);
        teamRules.add(knowsAlliesDescription);

        Optional<ModifierValue> factionalLastWillModifier = faction.modifiers.getIfSingle(FactionModifierName.LAST_WILL,
                playerCount);
        boolean ifMightHaveLastWill = !factionalLastWillModifier.isPresent()
                || factionalLastWillModifier.get().getBoolValue();
        if(ifMightHaveLastWill)
            teamRules.add(FactionModifierName.LAST_WILL.getTextValue(playerCount, faction));

        String aliveToWinDescription = FactionModifierName.LIVE_TO_WIN.getTextValue(playerCount, faction);
        teamRules.add(aliveToWinDescription);

        boolean ifPunchMaybeAllowed = faction.setup.modifiers.getBoolean(SetupModifierName.PUNCH_ALLOWED, playerCount,
                true);
        if(ifPunchMaybeAllowed){
            Optional<ModifierValue> punchModifier = faction.modifiers.getIfSingle(FactionModifierName.PUNCH_SUCCESS,
                    playerCount);
            if(punchModifier.isPresent()){
                int success = punchModifier.get().getIntValue();
                teamRules.add(faction.getName() + " members have a " + success + "% chance to punch out their target.");
            }else{
                teamRules.add("The success of " + faction.getName()
                        + " members to kill their target with a punch varies on initial player count.");
            }
        }

        Set<FactionRole> cultLeaders = faction.setup.getPossibleFactionRolesWithAbility(game, CultLeader.abilityType);
        if(!cultLeaders.isEmpty()){
            String isRecruitableText = FactionModifierName.IS_RECRUITABLE.getTextValue(playerCount, faction);
            teamRules.add(isRecruitableText);
        }

        return teamRules;
    }
}
