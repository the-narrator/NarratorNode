package models.json_output;

import java.time.Instant;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import game.setups.Setup;
import json.JSONException;
import json.JSONObject;
import models.ModifierValue;
import models.enums.SetupModifierName;
import models.modifiers.Modifier;
import models.modifiers.SetupModifier;
import models.view.Jsonifiable;

public class SetupModifierJson extends Jsonifiable {

    private String label;
    private SetupModifierName name;
    private ModifierValue value;
    private int minPlayerCount;
    private int maxPlayerCount;
    private Instant upsertedAt;

    public SetupModifierJson(Optional<Integer> playerCount, Setup setup, Modifier<SetupModifierName> modifier) {
        this.label = modifier.name.getLabel(playerCount, setup.modifiers);
        this.name = modifier.name;
        this.value = modifier.value;
        this.minPlayerCount = modifier.minPlayerCount;
        this.maxPlayerCount = modifier.maxPlayerCount;
        this.upsertedAt = modifier.upsertedAt;
    }

    public static Set<SetupModifierJson> getSet(Setup setup, Optional<Integer> playerCount) {
        Set<SetupModifierJson> modifierViews = new HashSet<>();
        Set<SetupModifierName> seenModifierNames = new HashSet<>();
        for(Modifier<SetupModifierName> modifier: setup.modifiers){
            modifierViews.add(new SetupModifierJson(playerCount, setup, modifier));
            seenModifierNames.add(modifier.name);
        }
        Object value;
        for(SetupModifierName modifierName: SetupModifierName.values()){
            if(seenModifierNames.contains(modifierName))
                continue;
            value = modifierName.defaultValue;
            modifierViews.add(new SetupModifierJson(playerCount, setup,
                    new SetupModifier(modifierName, new ModifierValue(value), 0, Setup.MAX_PLAYER_COUNT, Instant.MIN)));
        }

        return modifierViews;
    }

    @Override
    public JSONObject toJson() throws JSONException {
        JSONObject jSetupModifier = new JSONObject();
        jSetupModifier.put("label", this.label);
        jSetupModifier.put("name", this.name.toString());
        jSetupModifier.put("value", this.value.internalValue);
        jSetupModifier.put("minPlayerCount", this.minPlayerCount);
        jSetupModifier.put("maxPlayerCount", this.maxPlayerCount);
        jSetupModifier.put("upsertedAt", this.upsertedAt);
        return jSetupModifier;
    }

}
