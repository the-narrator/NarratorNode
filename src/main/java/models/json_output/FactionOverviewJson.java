package models.json_output;

import java.util.HashSet;
import java.util.Set;

import json.JSONException;
import json.JSONObject;
import models.Faction;
import models.view.Jsonifiable;

public class FactionOverviewJson extends Jsonifiable {

    private String color;
    private long factionID;
    private String name;

    public FactionOverviewJson(Faction faction) {
        this.color = faction.color;
        this.factionID = faction.id;
        this.name = faction.name;
    }

    public static Set<FactionOverviewJson> getSet(Set<Faction> enemyFactions) {
        Set<FactionOverviewJson> factionOverviewJson = new HashSet<>();

        for(Faction faction: enemyFactions)
            factionOverviewJson.add(new FactionOverviewJson(faction));

        return factionOverviewJson;
    }

    @Override
    public JSONObject toJson() throws JSONException {
        JSONObject teamJson = new JSONObject();
        teamJson.put("color", this.color);
        teamJson.put("id", this.factionID);
        teamJson.put("name", this.name);
        return teamJson;
    }
}
