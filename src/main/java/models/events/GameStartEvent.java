package models.events;

import java.util.Map;

import json.JSONArray;
import json.JSONException;
import json.JSONObject;
import models.GameLobby;
import models.json_output.GameUserJson;
import models.json_output.PlayerJson;
import models.json_output.SetupJson;
import models.json_output.profile.ProfileJson;

public class GameStartEvent {
    public static JSONObject request(GameLobby gameLobby, Map<Long, String> userIDtoNameMap) throws JSONException {
        JSONObject request = new JSONObject();

        JSONArray userIDs = new JSONArray();
        for(long userID: gameLobby.userMap.keySet())
            userIDs.put(userID);

        request.put("event", "gameStart");
        request.put("gameID", gameLobby.id);
        request.put("isDay", gameLobby.game.isDay());
        request.put("joinID", gameLobby.lobbyID);
        request.put("living", userIDs); // this NEEDS to be playerIDs or something
        request.put("players", PlayerJson.getSet(gameLobby.game.players.sortByName(), gameLobby.playerUserIDMap));
        request.put("profiles", ProfileJson.toSet(gameLobby.id, gameLobby.playerUserIDMap, userIDtoNameMap));
        request.put("setup", new SetupJson(gameLobby.game));
        request.put("users", GameUserJson.getSet(gameLobby.userMap.values()));

        return request;
    }
}
