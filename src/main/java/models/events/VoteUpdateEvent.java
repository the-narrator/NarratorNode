package models.events;

import java.util.Optional;

import game.event.Message;
import game.event.VoteAnnouncement;
import json.JSONArray;
import json.JSONException;
import json.JSONObject;
import models.GameLobby;
import models.json_output.GameUserJson;
import models.json_output.VoteSettingsJson;

public class VoteUpdateEvent {

    public static JSONObject request(GameLobby game, VoteAnnouncement event) throws JSONException {
        JSONObject voteJson = new JSONObject();
        voteJson.put("event", "newVote");
        voteJson.put("gameID", game.id);
        voteJson.put("joinID", game.lobbyID);
        Optional<Long> voterUserID = game.playerUserIDMap.getUserID(event.voter);
        if(voterUserID.isPresent())
            voteJson.put("voter", new GameUserJson(game.userMap.get(voterUserID.get())));

        voteJson.put("playerUserIDs", new JSONArray(game.playerUserIDMap.getUserIDs()));
        voteJson.put("settings", VoteSettingsJson.toJson(game.game));
        voteJson.put("users", GameUserJson.getSet(game.userMap.values()));
        voteJson.put("voterName", event.voter.getName());
        voteJson.put("voteTarget", event.voteTarget); // deprecated
        voteJson.put("voteTargets", event.voteTarget);
        voteJson.put("voteText", event.access(Message.PUBLIC));
        return voteJson;
    }

}
