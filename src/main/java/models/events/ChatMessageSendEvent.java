package models.events;

import java.util.Optional;

import game.event.ChatMessage;
import json.JSONException;
import json.JSONObject;
import models.idtypes.GameID;
import models.view.Jsonifiable;

public class ChatMessageSendEvent extends Jsonifiable {

    private JSONObject args;
    private String chatName;
    private GameID gameID;
    private Optional<String> source;
    private String speakerName;
    private String text;

    public ChatMessageSendEvent(ChatMessage chatMessage, Optional<String> source, GameID gameID, JSONObject args) {
        this.args = args;
        this.chatName = chatMessage.eventLog.getName();
        this.gameID = gameID;
        this.source = source;
        this.speakerName = chatMessage.speakerName;
        this.text = chatMessage.message;
    }

    @Override
    public JSONObject toJson() throws JSONException {
        JSONObject request = new JSONObject();
        request.put("args", this.args);
        request.put("chatName", this.chatName);
        request.put("event", "gameChatMessage");
        request.put("gameID", this.gameID);
        request.put("source", this.source);
        request.put("speakerName", this.speakerName);
        request.put("text", this.text);
        return request;
    }
}
