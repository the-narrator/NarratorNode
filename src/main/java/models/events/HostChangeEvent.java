package models.events;

import json.JSONException;
import json.JSONObject;

public class HostChangeEvent {
    public static JSONObject request(String lobbyID, long hostID) throws JSONException {
        JSONObject request = new JSONObject();
        request.put("event", "hostChange");
        request.put("hostID", hostID);
        request.put("joinID", lobbyID);

        return request;
    }
}
