package models.events;

import game.logic.Game;
import json.JSONException;
import json.JSONObject;
import models.json_output.PhaseJson;
import nnode.Lobby;
import services.VoteService;

public class VoteResetEvent {

    public static JSONObject request(Lobby lobby, int resetsRemaining) throws JSONException {
        Game game = lobby.game;
        JSONObject request = new JSONObject();
        request.put("event", "votePhaseReset");
        request.put("joinID", lobby.id);
        request.put("gameID", lobby.gameID);
        request.put("phase", new PhaseJson(game));
        request.put("resetsRemaining", resetsRemaining);
        request.put("voteInfo", VoteService.getVotes(game));
        request.put("votes", VoteService.getVotes(game));
        return request;
    }

}
