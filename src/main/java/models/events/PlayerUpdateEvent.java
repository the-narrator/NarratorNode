package models.events;

import java.util.Optional;

import game.logic.Player;
import json.JSONException;
import json.JSONObject;
import models.json_output.PlayerJson;
import models.schemas.PlayerSchema;
import models.view.Jsonifiable;

public class PlayerUpdateEvent extends Jsonifiable {

    private String lobbyID;
    private PlayerJson player;

    public PlayerUpdateEvent(PlayerSchema player, String lobbyID) {
        this.lobbyID = lobbyID;
        this.player = new PlayerJson(player);
    }

    public PlayerUpdateEvent(Player player, String lobbyID, long userID) {
        this.lobbyID = lobbyID;
        this.player = new PlayerJson(player, Optional.of(userID));
    }

    @Override
    public JSONObject toJson() throws JSONException {
        JSONObject request = new JSONObject();
        request.put("event", "playerUpdate");
        request.put("joinID", this.lobbyID);
        request.put("player", this.player);
        return request;
    }

}
