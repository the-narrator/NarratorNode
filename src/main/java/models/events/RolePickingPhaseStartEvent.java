package models.events;

import game.logic.Game;
import json.JSONException;
import json.JSONObject;
import models.json_output.PhaseJson;

public class RolePickingPhaseStartEvent {

    public static JSONObject request(Game game, String lobbyID) throws JSONException {
        JSONObject request = new JSONObject();
        request.put("event", "rolePickingStart");
        request.put("phase", new PhaseJson(game));
        request.put("joinID", lobbyID);
        return request;
    }

}
