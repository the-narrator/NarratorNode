package models.events;

import game.event.Message;
import json.JSONException;
import json.JSONObject;
import models.idtypes.GameID;

public class BroadcastEvent {
    public static JSONObject request(String lobbyID, GameID gameID, String s, Message e) throws JSONException {
        if(e != null)
            s = e.access(Message.PUBLIC);

        JSONObject request = new JSONObject();
        request.put("event", "broadcast");
        request.put("gameID", gameID);
        request.put("joinID", lobbyID);
        request.put("pmContents", s);

        return request;
    }
}
