package models.events;

import json.JSONException;
import json.JSONObject;
import models.PlayerUserIDMap;
import models.json_output.PlayerJson;
import nnode.Lobby;

public class PhaseEndBidEvent {
    public static JSONObject request(Lobby game, PlayerUserIDMap playerUserIDMap) throws JSONException {
        JSONObject request = new JSONObject();
        request.put("event", "phaseEndBid");
        request.put("gameID", game.getGameID());
        request.put("isStarted", game.game.isStarted());
        request.put("joinID", game.id);
        request.put("players", PlayerJson.getSet(game.game.players, playerUserIDMap));
        return request;
    }
}
