package models.events;

import game.event.DeathAnnouncement;
import game.event.Message;
import game.logic.Player;
import game.logic.PlayerList;
import json.JSONArray;
import json.JSONException;
import json.JSONObject;
import models.GameLobby;

public class DayDeathEvent {

    public static JSONObject request(GameLobby gameLobby, PlayerList deadPlayers, String announcement)
            throws JSONException {
        JSONArray userIDs = new JSONArray();
        for(Player player: deadPlayers){
            if(gameLobby.playerUserIDMap.hasUserID(player))
                userIDs.put(gameLobby.playerUserIDMap.getUserID(player).get());
        }

        JSONObject jo = new JSONObject();
        jo.put("contents", announcement);
        jo.put("event", "dayDeath");
        jo.put("gameID", gameLobby.id);
        jo.put("joinID", gameLobby.lobbyID);
        jo.put("userIDs", userIDs);
        return jo;
    }

    public static JSONObject request(GameLobby game, DeathAnnouncement announcement) throws JSONException {
        return request(game, announcement.dead, announcement.access(Message.PUBLIC));
    }
}
