package models.events;

import java.util.Set;

import game.logic.Game;
import json.JSONException;
import json.JSONObject;
import models.idtypes.GameID;
import models.idtypes.PlayerDBID;
import models.json_output.PlayerJson;
import models.json_output.SetupJson;
import models.schemas.PlayerSchema;
import models.view.Jsonifiable;

public class PlayerAddedEvent extends Jsonifiable {
    private String event;
    private GameID gameID;
    private String lobbyID;
    private PlayerJson player;
    private Set<PlayerJson> players;
    private String playerName;// deprecated (i think its just discord)
    private SetupJson setup;
    private long userID;

    public PlayerAddedEvent(Game game, String lobbyID, GameID gameID, long userID, PlayerDBID id, String name,
            Set<PlayerSchema> players) {
        this.event = "newPlayer";
        this.lobbyID = lobbyID;
        this.gameID = gameID;
        this.player = new PlayerJson(id, name, userID);
        this.playerName = name;
        this.players = PlayerJson.getSet(players);
        this.setup = new SetupJson(game);
        this.userID = userID;
    }

    @Override
    public JSONObject toJson() throws JSONException {
        JSONObject request = new JSONObject();
        request.put("event", this.event);
        request.put("gameID", this.gameID);
        request.put("joinID", this.lobbyID);
        request.put("player", this.player.toJson());
        request.put("playerName", this.playerName);
        request.put("players", toJson(this.players));
        request.put("setup", this.setup);
        request.put("userID", this.userID);

        return request;
    }
}
