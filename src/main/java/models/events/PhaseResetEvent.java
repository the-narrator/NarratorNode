package models.events;

import json.JSONException;
import json.JSONObject;
import models.json_output.PhaseJson;
import nnode.Lobby;

public class PhaseResetEvent {

    public static JSONObject request(Lobby lobby) throws JSONException {
        JSONObject request = new JSONObject();
        request.put("event", "phaseReset");
        request.put("joinID", lobby.id);
        request.put("phase", new PhaseJson(lobby.game));
        return request;
    }

}
