package models;

public class HiddenSpawn {
    public long id;
    public FactionRole factionRole;
    public int minPlayerCount;
    public int maxPlayerCount;

    public HiddenSpawn(long id, FactionRole factionRole, int minPlayerCount, int maxPlayerCount) {
        this.id = id;
        this.factionRole = factionRole;
        this.minPlayerCount = minPlayerCount;
        this.maxPlayerCount = maxPlayerCount;
    }
}
