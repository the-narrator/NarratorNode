package models.controllerResponse;

import java.util.HashSet;
import java.util.Set;

import json.JSONException;
import json.JSONObject;
import models.serviceResponse.AbilityMetadata;
import models.view.Jsonifiable;

public class AbilityMetadataJson extends Jsonifiable {
    private String command;
    private String description;
    public boolean publicCommand;
    private String usage;

    public AbilityMetadataJson(AbilityMetadata ability) {
        this.command = ability.command.toLowerCase();
        this.description = ability.description;
        this.publicCommand = ability.publicCommand;
        this.usage = ability.usage;
    }

    public static Set<AbilityMetadataJson> toSet(Set<AbilityMetadata> abilities) {
        Set<AbilityMetadataJson> array = new HashSet<>();
        for(AbilityMetadata ability: abilities)
            array.add(new AbilityMetadataJson(ability));
        return array;
    }

    @Override
    public JSONObject toJson() throws JSONException {
        JSONObject jo = new JSONObject();
        jo.put("command", this.command);
        jo.put("description", this.description);
        jo.put("publicCommand", this.publicCommand);
        jo.put("usage", this.usage);
        return jo;
    }
}
