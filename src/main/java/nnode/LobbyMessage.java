package nnode;

import json.JSONException;
import json.JSONObject;

public class LobbyMessage {

    protected long userID;
    protected String message;

    public LobbyMessage(long userID, String message) {
        this.userID = userID;
        this.message = message;
    }

    public JSONObject access(long accessorUserID) {
        JSONObject jo = new JSONObject();
        String name;
        if(accessorUserID == userID)
            name = "You";
        else
            name = "User " + accessorUserID;
        try{
            jo.put("text", message);
            jo.put("sender", "<b>" + name + "</b>");
        }catch(JSONException e){
            e.printStackTrace();
        }
        return jo;
    }

}
