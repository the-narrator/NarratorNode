package nnode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import game.logic.support.condorcet.Condorcet;
import json.JSONArray;
import json.JSONException;
import json.JSONObject;

public class CondorcetController {

    public static void parse(JSONObject request, JSONObject response) throws JSONException {
        if(request.getString("method").contentEquals("GET"))
            resolveCondorcetController(request, response);
    }

    private static void resolveCondorcetController(JSONObject request, JSONObject response) throws JSONException {
        JSONObject body = request.getJSONObject("body");
        JSONObject votes = body.getJSONObject("votes");
        Map<String, List<Set<String>>> input = new HashMap<>();
        Iterator<?> keyIterator = votes.keys();
        String key;
        while (keyIterator.hasNext()){
            key = keyIterator.next().toString();
            List<Set<String>> personPreference = new ArrayList<>();
            JSONArray personPreferenceJson = votes.getJSONArray(key);
            for(int j = 0; j < personPreferenceJson.length(); j++){
                Object listOrInt = personPreferenceJson.get(j);
                Set<String> choiceRankingGroup = new HashSet<>();
                if(listOrInt instanceof JSONArray){
                    JSONArray choiceRankingGroupJson = (JSONArray) listOrInt;
                    for(int k = 0; k < choiceRankingGroupJson.length(); k++){
                        choiceRankingGroup.add(choiceRankingGroupJson.getString(k));
                    }
                }else{
                    choiceRankingGroup.add((String) listOrInt);
                }
                personPreference.add(choiceRankingGroup);
            }
            input.put(key, personPreference);
        }
        List<Set<String>> condorcetOrderings = Condorcet.getOrderings(input.values());

        JSONArray nestedArrayResponse = new JSONArray(), arrayResponse;
        for(Set<String> set: condorcetOrderings){
            arrayResponse = new JSONArray();
            for(String string: set)
                arrayResponse.put(string);
            nestedArrayResponse.put(arrayResponse);
        }
        response.put("response", nestedArrayResponse);
    }
}
