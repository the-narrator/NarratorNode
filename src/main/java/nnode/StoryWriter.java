package nnode;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;
import java.util.Optional;
import java.util.regex.Pattern;

import game.abilities.Survivor;
import game.logic.DeathType;
import game.logic.Game;
import game.logic.Player;
import game.logic.exceptions.NarratorException;
import game.logic.listeners.CommandListener;
import game.logic.support.Constants;
import models.Command;
import models.idtypes.GameID;
import models.idtypes.PlayerDBID;
import repositories.CommandRepo;
import repositories.Connection;
import repositories.GameRepo;
import repositories.PlayerDeathRepo;
import repositories.PlayerRepo;
import services.UserService;
import util.Util;

public class StoryWriter implements CommandListener {

    private GameID gameID;
    boolean erroredOut = false;
    private ArrayList<Command> initCommands;

    public boolean logJSONInput = true;

    public StoryWriter(ArrayList<Command> initialCommands) {
        this.initCommands = initialCommands;
    }

    public void setGameID(GameID gameID, Map<PlayerDBID, Player> databaseMap) {
        this.gameID = gameID;
        if(this.initCommands != null)
            for(Command s: initCommands)
                onCommand(databaseMap.get(s.playerID.get()), s.timeLeft, s.text);
        this.initCommands = null;
    }

    public GameID getGameID() { // rename to getGameID
        return this.gameID;
    }

    long counter = 0;

    @Override
    public void onCommand(Player player, double timeLeft, String text) {
        if(erroredOut)
            return;
        text = text.substring(0, Math.min(550, text.length()));
        try{
            Optional<PlayerDBID> databaseID = player == null ? Optional.empty() : Optional.of(player.databaseID);
            CommandRepo.create(++counter, databaseID, text, timeLeft, this.gameID);
        }catch(SQLException e){
            Util.log(e, "Command insert failed");
        }
    }

    public static Short deathDayToInt(Player p) {
        DeathType death = p.getDeathType();
        if(death == null)
            return (short) (p.game.getDayNumber() * 10);
        short d = (short) (death.getDeathDay() * 10);
        if(!death.isDayDeath()){
            d += 5;
        }
        return d;
    }

    public static String GetToken(String name, PreparedStatement ps) throws SQLException {
        ps.setString(2, name);
        ResultSet rs = ps.executeQuery();
        String token;
        if(rs.next())
            token = rs.getString(1);
        else
            token = null;
        rs.close();
        return token;
    }

    public static int GetPoints(Player p) {
        if(p.isWinner())
            return 10;
        return 5;
    }

    public static void UpdateStats(Game n, GameID gameID) throws SQLException {
        String deathType = null;
        for(Player p: n.players){
            if(p.isDead()){
                for(String[] dType: p.getDeathType().attacks){
                    if(dType[0].startsWith(Constants.FACTION_KILL_FLAG[0]))
                        deathType = Constants.FACTION_KILL_FLAG[0];
                    else
                        deathType = dType[0];
                    break;
                }
            }else{
                deathType = Survivor.class.getSimpleName().toLowerCase();
            }
            if(deathType == null)
                throw new NarratorException(p.getID() + " is dead with no attacks.");

            PlayerRepo.updateDeathDayAndIsWinner(gameID, p.getID(), deathDayToInt(p), p.isWinner());
            PlayerDeathRepo.createDeath(gameID, p.getID(), deathType);
            UserService.updatePointsAndWinRate(gameID, p.getID(), GetPoints(p));
        }
    }

    public void endGame(Game n) throws SQLException {
        UpdateStats(n, gameID);
        GameRepo.setFinished(gameID);
    }

    private ArrayList<String> commands;

    public ArrayList<String> enableLogging() {
        commands = new ArrayList<>();
        return commands;
    }

    public static void swapName(Connection c, long storyID, String oldName, String newName) throws SQLException {
        // update playerNames
        String infoQuery, updateQuery;
        infoQuery = "SELECT game_id, name FROM player_saved_games";
        updateQuery = "UPDATE player_saved_games SET name = ? WHERE game_id = ? and name = ?";
        PreparedStatement commandQuery = c.prepareStatement(updateQuery);
        commandQuery.setString(1, newName);
        commandQuery.setLong(2, storyID);
        commandQuery.setString(3, oldName);
        commandQuery.execute();
        commandQuery.close();

        // update roles_list_association
        updateQuery = "UPDATE roles_list_saved_games SET playerAssigned = ? WHERE game_id = ? and playerAssigned = ?";
        commandQuery = c.prepareStatement(updateQuery);
        commandQuery.setString(1, newName);
        commandQuery.setLong(2, storyID);
        commandQuery.setString(3, oldName);
        commandQuery.execute();
        commandQuery.close();

        infoQuery = "SELECT command_id, command FROM commands_saved_games where game_id = ?;";
        updateQuery = "UPDATE commands_saved_games SET command = ? WHERE command_id = ? and command = ?";

        commandQuery = c.prepareStatement(infoQuery);
        commandQuery.setLong(1, storyID);
        PreparedStatement commandUpdater = c.prepareStatement(updateQuery);

        String text, newText;
        long comand_id;
        ResultSet commandResult = commandQuery.executeQuery();
        while (commandResult.next()){
            comand_id = commandResult.getLong(1);

            text = commandResult.getString(2);
            newText = text.replaceAll(Pattern.quote(oldName), newName);

            if(text.equals(newText))
                continue;

            commandUpdater.setString(1, newText);
            commandUpdater.setLong(2, comand_id);
            commandUpdater.setString(3, text);
            commandUpdater.execute();
        }

        commandUpdater.close();

        commandResult.close();
        commandQuery.close();
    }

}
