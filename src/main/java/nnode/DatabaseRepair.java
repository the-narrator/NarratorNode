package nnode;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Optional;
import java.util.function.Consumer;

import game.logic.support.CommandHandler;
import game.logic.support.Constants;
import models.idtypes.GameID;
import models.schemas.PlayerSchema;
import repositories.BaseRepo;
import repositories.CommandRepo;
import repositories.Connection;
import repositories.PlayerRepo;

public class DatabaseRepair {

    static final boolean executing = true;
    static GameID gameID;
    static Connection connection;

    public static void main(String[] args) throws IOException, SQLException {
        BaseRepo.connection = new Connection();
        try{
            runMain(args);
        }catch(Throwable e){
        }
        BaseRepo.connection.close();
    }

    public static void runMain(String[] args) throws IOException, SQLException {
        Files.lines(Paths.get(Config.getString("db_repair_location"))).forEach(new Consumer<String>() {
            @Override
            public void accept(String arg0) {
                try{
                    doWork(arg0);
                }catch(SQLException e){
                    e.printStackTrace();
                }
            }
        });
        connection.close();

        StatRefresher.main(args);
    }

    protected static void doWork(String arg0) throws SQLException {

        String[] split = arg0.split(" ");
        String key = split[0];
        if(key.startsWith("#"))
            return;
        switch (key) {
        case "use":
            gameID = new GameID(Long.parseLong(split[1]));
            break;
        case "dremove":
            int toRemove;
            ArrayList<Integer> list = new ArrayList<>();
            for(int i = 1; i < split.length; i++){
                toRemove = Integer.parseInt(split[i]);
                list.add(toRemove);
            }
            Collections.sort(list);
            Collections.reverse(list);
            for(int i: list)
                dremove(i);
            break;
        case "rremove":
            split = split[1].split("-");
            int min = Integer.parseInt(split[0]);
            int max = Integer.parseInt(split[1]);
            rangeRemove(min, max);
            break;

        case "remove":
            list = new ArrayList<>();
            for(int i = 1; i < split.length; i++){
                toRemove = Integer.parseInt(split[i]);
                list.add(toRemove);
            }
            Collections.sort(list);
            Collections.reverse(list);
            for(int i: list)
                remove(i);
            break;

        case "move":
            int toMove = Integer.parseInt(split[1]);
            int where = Integer.parseInt(split[2]);
            move(toMove, where);
            break;

        case "endNight":
            int insert = Integer.parseInt(split[1]);
            insertEndNight(insert);
            break;

        case "insert":
            insert = Integer.parseInt(split[1]);
            StringBuilder sb = new StringBuilder();
            for(int i = 3; i < split.length; i++){
                if(i != 3)
                    sb.append(" ");
                sb.append(split[i]);
            }
            insertCommand(insert, split[2], Constants.ALL_TIME_LEFT, sb.toString());
            break;
        }
    }

    protected static void rangeRemove(int min, int max) {
        // translate index to counter
        try{
            long minCounter = getCounter(min);
            long maxCounter = getCounter(max);
            String s = "DELETE FROM commands_saved_games WHERE game_id = " + gameID + " AND counter >= " + minCounter
                    + " AND counter <= " + maxCounter + ";";

            PreparedStatement ps = connection.prepareStatement(s);
            ps.execute();
            ps.close();
        }catch(SQLException e){
            e.printStackTrace();
        }

    }

    protected static long getCounter(int index) throws SQLException {
        String s = "SELECT counter FROM commands_saved_games WHERE game_id = " + gameID + " ORDER BY counter LIMIT "
                + index + ", 1";
        PreparedStatement query = connection.prepareStatement(s);
        ResultSet rs = query.executeQuery();
        rs.next();
        long location = rs.getLong(1);

        rs.close();
        query.close();

        return location;
    }

    protected static long makeRoom(int insert) throws SQLException {
        long location = getCounter(insert);

        String s = "UPDATE commands_saved_games SET counter = counter + 1 WHERE counter >= " + location
                + " AND game_id = " + gameID + ";";
        System.err.println(s);
        PreparedStatement query = connection.prepareStatement(s);
        query.execute();
        query.close();

        return location;
    }

    protected static void insertCommand(int insert, String playerName, double timeLeft, String command)
            throws SQLException {
        PlayerSchema player = PlayerRepo.getByNameAndGameID(playerName, gameID).get();
        long location = makeRoom(insert);
        CommandRepo.create(location, Optional.of(player.id), command, timeLeft, player.gameID);
    }

    protected static void insertEndNight(int insert) throws SQLException {
        String command = CommandHandler.END_PHASE;
        insertCommand(insert, null, Constants.NO_TIME_LEFT, command);
    }

    public static void remove(int id) {
        String s = "DELETE FROM commands_saved_games WHERE command_id = (SELECT command_id FROM (SELECT command_id FROM commands_saved_games WHERE game_id = "
                + gameID + " ORDER BY counter LIMIT " + id + ", 1) as t);";
        System.err.println(s);
        PreparedStatement query;
        try{
            query = connection.prepareStatement(s);
            if(executing)
                query.execute();
            query.close();
        }catch(SQLException e){
            e.printStackTrace();
        }
    }

    public static void dremove(int id) {
        String s = "DELETE FROM commands_saved_games WHERE counter = " + id + " and game_id = " + gameID + ";";
        System.err.println(s);
        PreparedStatement query;
        try{
            query = connection.prepareStatement(s);
            if(executing)
                query.execute();
            query.close();
        }catch(SQLException e){
            e.printStackTrace();
        }
    }

    public static long getCount() {
        String s = "SELECT COUNT(1) FROM commands_saved_games where game_id = 7;";
        PreparedStatement query;
        try{
            query = connection.prepareStatement(s);
            ResultSet rs = query.executeQuery();
            rs.next();
            long ret = rs.getLong(1);
            query.close();
            return ret;
        }catch(SQLException e){
            e.printStackTrace();
        }
        return -1;
    }

    public static void move(int toMove, int toWhere) {
        long temp = getCount() * 2;
        try{
            String s = "UPDATE commands_saved_games SET counter = " + temp
                    + " WHERE command_id = (SELECT command_id FROM (SELECT command_id FROM commands_saved_games WHERE game_id = "
                    + gameID + " ORDER BY counter LIMIT " + toMove + ", 1) as t);";
            System.err.println(s);
            PreparedStatement query;
            query = connection.prepareStatement(s);
            if(executing)
                query.execute();
            query.close();

            temp++;

            long location = makeRoom(toWhere);

            s = "UPDATE commands_saved_games SET counter = " + location + " WHERE counter = " + temp + " AND game_id = "
                    + gameID + ";";
            System.err.println(s);
            query = connection.prepareStatement(s);
            query.execute();
            query.close();

        }catch(SQLException e){
            e.printStackTrace();
        }
    }
}
