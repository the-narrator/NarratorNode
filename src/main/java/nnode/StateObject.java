package nnode;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import game.abilities.BreadAbility;
import game.abilities.ElectroManiac;
import game.abilities.GameAbility;
import game.abilities.Ghost;
import game.abilities.JailCreate;
import game.abilities.JailExecute;
import game.abilities.Joker;
import game.abilities.Marshall;
import game.abilities.Miller;
import game.abilities.Vote;
import game.abilities.support.CultInvitation;
import game.event.DayChat;
import game.event.EventLog;
import game.event.Feedback;
import game.logic.DeathType;
import game.logic.Game;
import game.logic.GameFaction;
import game.logic.Player;
import game.logic.PlayerList;
import game.logic.support.Constants;
import game.logic.support.action.ActionList;
import game.setups.Setup;
import json.JSONArray;
import json.JSONException;
import json.JSONObject;
import models.Faction;
import models.FactionRole;
import models.PlayerUserIDMap;
import models.enums.AbilityType;
import models.enums.FactionModifierName;
import models.enums.GamePhase;
import models.enums.SetupModifierName;
import models.json_output.ActionJson;
import models.json_output.FactionJson;
import models.json_output.SetupJson;
import models.json_output.profile.OptionsTreeJson;
import services.UserService;

public class StateObject {

    public static final String FACTIONS = "Rules";
    public static final String PLAYERLISTS = "PlayerLists";
    public static final String DAYLABEL = "DayLabel";
    public static final String GRAVEYARD = "Graveyard";
    public static final String ROLEINFO = "RoleInfo";
    public static final String ACTIONS = "Actions";
    public static final String SETUP = "setup";

    public static final String warning = "warning";
    public static final String message = "message";
    public static final String userID = "userID";
    public static final String NAME = "name";
    public static final String httpRequest = "httpRequest";
    public static final String httpResponse = "httpResponse";
    public static final String requestID = "requestID";
    public static final String joinID = "joinID";

    private Game game;
    private HashMap<String, Object> extraKeys;
    private JSONObject baseJson;

    public StateObject(Lobby lobby) {
        states = new ArrayList<String>();
        this.game = lobby.game;
        extraKeys = new HashMap<>();

        baseJson = new JSONObject();
        try{
            baseJson.put(StateObject.type, new JSONArray());
            baseJson.put(StateObject.guiUpdate, true);
        }catch(JSONException e){
            e.printStackTrace();
        }
    }

    ArrayList<String> states;

    public StateObject addState(String state) {
        states.add(state);
        return this;
    }

//    private FactionRole getSpawn(Hidden setupHidden, int count) {
//        if(!narrator.rolesList.spawns.containsKey(setupHidden))
//            return null;
//        ArrayList<FactionRole> spawns = narrator.rolesList.spawns.get(setupHidden);
//        if(spawns.isEmpty())
//            return null;
//        return spawns.get(count);
//    }

    private void addJRolesList(Player requester, JSONObject state) {
        return;
//        JSONArray roles = new JSONArray();
//        JSONObject role, subRole;
//        JSONArray jPossibleRandoms;
//        Hidden rm;
//        Player givenTo;
//        boolean omnDead = requester != null && narrator.getBool(SetupModifier.OMNISCIENT_DEAD) && requester.isDead();
//        omnDead = omnDead || (!narrator.isInProgress() && narrator.isStarted());
//        PlayerList seenPlayers = new PlayerList();
//        HashMap<Hidden, Integer> givenSpawns = new HashMap<>();
//
//        Hidden r;
//        for(SetupHidden setupHidden: narrator.getRolesList().sort()){
//            role = new JSONObject();
//            givenTo = getGivenTo(setupHidden.hidden, seenPlayers);
//            if(givenTo != null)
//                seenPlayers.add(givenTo);
//
//            if(givenTo == null){
//                r = setupHidden.hidden;
//            }else if(omnDead){
//                r = new Role(narrator, givenTo.getColor(), givenTo.getRoleName(), givenTo.getAbilities());
//                role.put(StateObject.playerName, givenTo.getName());
//            }else if(setupHidden.hidden.isRandom()){
//                if(setupHidden.isExposed && narrator.isStarted())
//                    r = givenTo.initialAssignedRole.assignedRole;
//                else if(narrator.isStarted() && !narrator.isInProgress())
//                    r = givenTo.initialAssignedRole.assignedRole;
//                else
//                    r = setupHidden.hidden;
//            }else{
//                r = setupHidden.hidden;
//            }
//
//            role.put(StateObject.roleType, r.getName());
//            role.put(StateObject.color, r.getColors().iterator().next());
//
//            if(r.isRandom()){
//                role.put(StateObject.roleID, r.getID());
//                rm = r;
//                jPossibleRandoms = new JSONArray();
//
//                String key;
//                for(FactionRole factionRole: rm.getFactionRoles()){
//                    key = factionRole.toString();
//                    subRole = new JSONObject();
//                    subRole.put(StateObject.roleType, factionRole.role.getName());
//                    subRole.put(StateObject.color, factionRole.faction.getColor());
//                    subRole.put(StateObject.key, key);
//                    jPossibleRandoms.put(subRole);
//                }
//                FactionRole m;
//
//                if(!givenSpawns.containsKey(setupHidden.hidden))
//                    givenSpawns.put(setupHidden.hidden, 0);
//                m = getSpawn(setupHidden.hidden, givenSpawns.get(setupHidden.hidden));
//                if(m != null)
//                    givenSpawns.put(setupHidden.hidden, givenSpawns.get(setupHidden.hidden) + 1);
//
//                if(m != null && showHiddenRoles){
//                    subRole = new JSONObject();
//                    subRole.put(StateObject.roleType, m.getName());
//                    subRole.put(StateObject.color, r.getColors().iterator().next());
//                    role.put(StateObject.spawn, subRole);
//                }
//
//                role.put(StateObject.possibleRandoms, jPossibleRandoms);
//            }
//
//            roles.put(role);
//        }
//        state.getJSONArray(StateObject.type).put(StateObject.roles);
//        state.put(StateObject.roles, roles);
    }

    private void addJDayLabel(JSONObject state) throws JSONException {
        String dayLabel;
        if(!game.isStarted()){
            dayLabel = "Night 0";
        }else if(game.isDay()){
            dayLabel = "Day " + game.getDayNumber();
        }else{
            dayLabel = "Night " + game.getDayNumber();
        }
        state.put("dayNumber", game.getDayNumber());
        state.getJSONArray(StateObject.type).put(StateObject.dayLabel);
        state.put(StateObject.dayLabel, dayLabel);
    }

    private void addNullRoleInfo(JSONObject state) throws JSONException {
        JSONObject roleInfo = new JSONObject();
        roleInfo.put(StateObject.roleColor, "#FFFFFF");
        roleInfo.put(StateObject.roleName, "");
        roleInfo.put(StateObject.roleBaseName, "");
        roleInfo.put(StateObject.roleDescription, "");

        state.getJSONArray(StateObject.type).put(StateObject.roleInfo);
        state.put(StateObject.roleInfo, roleInfo);
    }

    private void addJRoleInfo(Optional<Player> optPlayer, JSONObject state) throws JSONException {
        if(!optPlayer.isPresent()){
            addNullRoleInfo(state);
            return;
        }
        Player player = optPlayer.get();

        JSONObject roleInfo = new JSONObject();
        roleInfo.put(StateObject.displayName, player.getName());

        state.getJSONArray(StateObject.type).put(StateObject.roleInfo);
        state.put(StateObject.roleInfo, roleInfo);
        if(!player.game.isStarted())
            return;

        List<String> description = player.getRoleCardDetails();

        roleInfo.put(StateObject.roleColor, player.getGameFaction().getColor());
        roleInfo.put(StateObject.roleName, player.gameRole.factionRole.getPlayerVisibleFactionRole().getName());
        roleInfo.put(StateObject.roleDescription, description);

        ArrayList<String> abilityNames = new ArrayList<>();
        for(GameAbility a: player.getRoleAbilities()){
            if(a.isHiddenPassive())
                continue;
            abilityNames.add(a.getClass().getSimpleName());
        }
        roleInfo.put(StateObject.abilities, abilityNames);

        roleInfo.put(StateObject.breadCount, BreadAbility.getUseableBread(player));
        roleInfo.put(StateObject.isPuppeted, player.isPuppeted());
        roleInfo.put(StateObject.isBlackmailed, player.isSilenced());

        if(player.hasPuppets()){
            JSONArray jPuppets = new JSONArray();
            for(Player puppet: player.getPuppets()){
                jPuppets.put(puppet.getName());
            }
            roleInfo.put(StateObject.puppets, jPuppets);
        }

        if(player.is(Ghost.abilityType)){
            String ghostWinCondition;
            if(player.isAlive()){
                ghostWinCondition = "The first step to winning is to die";
            }else{
                Player cause = player.getDeathType().getCause();
                if(cause == player && player.getCause() != null)
                    cause = player.getCause();
                if(cause == null || cause == player){
                    ghostWinCondition = "You've lost the game";
                }else{
                    GameFaction t = cause.getGameFaction();
                    if(t.isEnemy(t)){
                        ghostWinCondition = "You must assure the defeat of " + player.getName() + " to win.";
                    }else if(t.knowsTeam() || t._startingSize > 0)
                        ghostWinCondition = "You must assure the defeat of " + t.getName() + " to win.";
                    else{
                        ghostWinCondition = "You must assure the defeat of " + player.getName() + " to win.";
                    }
                }
            }
            roleInfo.put("ghostWinCondition", ghostWinCondition);
            if(player.isAlive())
                roleInfo.put(StateObject.isAlive, true);
            else
                roleInfo.put(StateObject.isAlive, "Ghost");
        }else{
            roleInfo.put(StateObject.isAlive, player.isAlive());
        }

        List<String> roleSpecs = player.getRoleCardDetails();
        if(!roleSpecs.isEmpty())
            roleInfo.put("roleSpecs", roleSpecs);

        if(player.hasDayAction(CultInvitation.abilityType))
            roleInfo.put("acceptingRecruitment", player.ci.accepted);

        boolean electroExtraCharge = false;
        if(player.is(ElectroManiac.abilityType)){
            ElectroManiac em = player.getAbility(ElectroManiac.class);
            electroExtraCharge = em.usedDoubleAction == null;
        }
        roleInfo.put("electroExtraCharge", electroExtraCharge);
        if(player.is(Joker.abilityType)){
            roleInfo.put("jokerActiveBounty", player.getAbility(Joker.class).activeBounty != null);
            roleInfo.put("jokerKills", player.getAbility(Joker.class).bounty_kills);
        }
        if(player.is(JailExecute.abilityType) && player.getAbility(JailExecute.class).remainingCleanings > 0){
            roleInfo.put("jailCleanings", player.getAbility(JailExecute.class).remainingCleanings);
        }
    }

    private void addJGraveYard(JSONObject state) throws JSONException {
        JSONArray graveYard = jGraveYard(game);
        state.getJSONArray(StateObject.type).put(StateObject.graveYard);
        state.put(StateObject.graveYard, graveYard);
    }

    public static JSONArray jGraveYard(Game game) throws JSONException {
        Setup setup = game.setup;
        JSONArray graveYard = new JSONArray(), deathTypes;

        JSONObject graveMarker;
        String color, roleName;
        Long factionID;
        FactionRole factionRole;
        DeathType dt;
        for(Player p: game.getDeadPlayers().sortByDeath()){
            graveMarker = new JSONObject();
            if(p.getDeathType().isHidden() || Marshall.isCurrentMarshallLynch(p)){
                color = Constants.A_CLEANED;
                factionID = null;
                roleName = "????";
            }else if(p.hasSuits()){
                factionRole = p.suits.get(0).factionRole;
                color = factionRole.faction.getColor();
                factionID = setup.getFactionByColor(color).id;
                roleName = factionRole.role.getName();
            }else if(p.is(Miller.abilityType) && game.getBool(SetupModifierName.MILLER_SUITED)){
                color = Miller.MillerColor(game);
                factionID = setup.getFactionByColor(color).id;
                roleName = Miller.getRoleFlip(p).getName();
            }else{
                color = p.getGameFaction().getColor();
                factionID = setup.getFactionByColor(color).id;
                roleName = p.getRoleName();
                // TODO again, i think i'll need to specify the abilities in this card
                if(p._lastWill != null && !p._lastWill.isEmpty())
                    graveMarker.put("lastWill", p._lastWill);
            }
            graveMarker.put(StateObject.roleName, roleName);
            graveMarker.put(StateObject.color, color);
            graveMarker.put("factionID", factionID);
            graveMarker.put("name", p.getName());
            graveYard.put(graveMarker);

            dt = p.getDeathType();
            graveMarker.put("phase", dt.isDayDeath());
            graveMarker.put("day", dt.getDeathDay());

            deathTypes = new JSONArray();
            for(String[] s: dt.getList())
                deathTypes.put(s[1]);// 0 is the id, 1 is the description

            graveMarker.put("deathTypes", deathTypes);
        }

        return graveYard;
    }

    public static JSONObject getStateObjectActions(Optional<Player> player) throws JSONException {
        JSONObject jActions = new JSONObject();
        ActionList actions = null;
        if(player.isPresent())
            actions = player.get().getActions();
        if(actions == null || !player.isPresent() || (!player.get().isAlive() && !player.get().is(Ghost.abilityType)))
            jActions.put("canAddAction", false);

        if(player.isPresent())
            jActions.put(StateObject.actionList, ActionJson.getSet(player.get()));
        else
            jActions.put(StateObject.actionList, new JSONArray());
        return jActions;
    }

    private void addJActions(Optional<Player> player, JSONObject state) throws JSONException {
        JSONObject jActions = getStateObjectActions(player);
        state.getJSONArray(StateObject.type).put(StateObject.actions);
        state.put(StateObject.actions, jActions);
    }

    /*
     * all factions object
     *
     * (faction name) -> faction object (faction color) -> faction object
     * factionName -> list of faction names
     *
     */

    /*
     * single faction object
     *
     * name -> faction name color -> faction color description -> faction
     * description isEditable -> can editFaction
     */

    private void addJFactions(JSONObject state) throws JSONException {
        JSONArray allies, enemies, factionNames = new JSONArray();
        JSONObject jFaction, allyInfo, jFactions = new JSONObject();

        for(Faction f: game.setup.getFactions()){
            jFaction = new JSONObject();

            jFaction.put(StateObject.color, f.getColor());
            jFaction.put("name", f.getName());
            jFaction.put("description", f.description);

            jFaction.put("rules", StateObject.getRules(game, f));

            allies = new JSONArray();
            enemies = new JSONArray();
            for(Faction faction: game.setup.getFactions()){
                allyInfo = new JSONObject();
                allyInfo.put("color", faction.getColor());
                allyInfo.put("name", faction.getName());
                if(faction.enemies.contains(f))
                    enemies.put(allyInfo);
                else
                    allies.put(allyInfo);
            }
            jFaction.put("allies", allies);
            jFaction.put("enemies", enemies);

            factionNames.put(f.getColor());

            jFactions.put(f.getColor(), jFaction);
        }

        jFactions.put(StateObject.factionNames, factionNames);

        state.getJSONArray(StateObject.type).put(StateObject.factions);
        state.put(StateObject.factions, jFactions);
    }

    public static JSONArray getChatKeys(Game n, Player p) throws JSONException {
        JSONObject chat;
        JSONArray chats = new JSONArray();

        ArrayList<EventLog> logs;
        if(!n.isInProgress() && n.isStarted()){
            logs = n.getEventManager().getAllChats();
        }else if(p == null){
            logs = new ArrayList<>();
            logs.add(n.getEventManager().dayChat);
            logs.add(n.getEventManager().voidChat);
        }else{
            logs = p.getChats();
        }

        for(EventLog el: logs){
            chat = new JSONObject();

            chat.put(StateObject.chatName, el.getName());
            if(el.isActive() && el.getKey(p) != null)
                chat.put(StateObject.chatKey, el.getKey(p));

            chat.put(StateObject.chatType, el.getType());

            if(el instanceof DayChat){
                chat.put(StateObject.isDay, true);
            }

            chats.put(chat);
        }

        if(n.isInProgress() && p != null && (p.isAlive() || p.hasAbility(Ghost.abilityType))){
            chat = new JSONObject();
            chat.put(StateObject.chatName, Feedback.CHAT_NAME);
            chat.put(StateObject.chatType, Feedback.TYPE);
            chats.put(chat);
        }else if(!n.isInProgress() && !n.isStarted()){
            chat = new JSONObject();
            chat.put(StateObject.chatName, "Pregame");
            chat.put(StateObject.chatType, Feedback.TYPE);
            chats.put(chat);
        }

        return chats;
    }

    private JSONObject getJPlayerArray(PlayerUserIDMap playerUserIDMap, PlayerList input, boolean canAddAnother,
            JSONObject optionTree) throws JSONException {
        JSONObject jAll = new JSONObject();
        JSONArray arr = new JSONArray();
        jAll.put("options", optionTree);
        jAll.put("canAddAction", canAddAnother);
        if(input.isEmpty())
            return jAll;

        JSONObject jo;
        for(Player pi: input){
            jo = new JSONObject();
            jo.put(StateObject.playerName, pi.getName());

            jo.put("isComputer", pi.isComputer());
            if(playerUserIDMap.hasUserID(pi))
                jo.put("userID", playerUserIDMap.getUserID(pi).get());
            arr.put(jo);
        }

        jAll.put("players", arr);

        return jAll;
    }

    private static PlayerUserIDMap getPlayerUserMap(Lobby lobby) {
        if(lobby == null)
            return new PlayerUserIDMap(new HashMap<>());
        try{
            return UserService.getPlayerUserMap(lobby.game);
        }catch(SQLException e){
            return new PlayerUserIDMap(new HashMap<>());
        }
    }

    private void addJPlayerLists(JSONObject state, Lobby lobby, Optional<Player> optPlayer) throws JSONException {
        JSONObject playerLists = new JSONObject();
        playerLists.put(StateObject.type, new JSONArray());

        state.getJSONArray(StateObject.type).put(StateObject.playerLists);
        state.put(StateObject.playerLists, playerLists);

        if(!game.isStarted() || !optPlayer.isPresent() || game.isFinished())
            return;

        PlayerUserIDMap playerUserIDMap = getPlayerUserMap(lobby);
        Player player = optPlayer.get();
        if(game.phase != GamePhase.NIGHT_ACTION_SUBMISSION){
            PlayerList votes;
            if(game.isInProgress())
                votes = game.getLivePlayers();
            else
                votes = game.getAllPlayers();
            votes.sortByName();
            JSONObject names = getJPlayerArray(playerUserIDMap, votes, false, null), optionTree;

            PlayerList acceptableTargets;
            for(AbilityType abilityType: player.getDayCommands()){
                if(abilityType == Vote.abilityType)
                    continue;
                acceptableTargets = player.getAcceptableTargets(abilityType);
                if(acceptableTargets == null){// triggered by arson burn by having null acceptable targets
                    if(abilityType != CultInvitation.abilityType){
                        optionTree = new OptionsTreeJson(player, player.getAbility(abilityType)).toJson();
                        names = getJPlayerArray(playerUserIDMap, new PlayerList(),
                                player.getActions().canAddAnotherAction(abilityType), optionTree);
                    }else{
                        optionTree = new JSONObject();
                        names = getJPlayerArray(playerUserIDMap, new PlayerList(), false, optionTree);
                    }
                    playerLists.put(abilityType.command, names);
                    playerLists.getJSONArray(StateObject.type).put(abilityType.command);
                    continue;
                }
                if(acceptableTargets.size() == 1 && acceptableTargets.getFirst() == player)
                    // pretty sure this isn't used anymore
                    continue;
                if(acceptableTargets.isEmpty())// && ability == Ability.MAIN_ABILITY)
                    continue;
                names = getJPlayerArray(playerUserIDMap, acceptableTargets,
                        player.getActions().canAddAnotherAction(abilityType), null);
                playerLists.put(abilityType.command, names);
                playerLists.getJSONArray(StateObject.type).put(abilityType.command);
            }
            return;
        } // if is night
        if(game.isInProgress() && player.isParticipating()){
            ArrayList<GameAbility> abilities = player.getActionableAbilities();
            JSONObject names, optionTree;
            for(GameAbility ability: abilities){
                if(!ability.isNightAbility(player))
                    continue;
                PlayerList acceptableTargets = ability.getAcceptableTargets(player);
                optionTree = new OptionsTreeJson(player, ability).toJson();
                if(acceptableTargets == null){
                    names = getJPlayerArray(playerUserIDMap, new PlayerList(),
                            player.getActions().canAddAnotherAction(ability.getAbilityType()), optionTree);
                }else{
                    if(acceptableTargets.isEmpty())
                        continue;
                    names = getJPlayerArray(playerUserIDMap, acceptableTargets,
                            player.getActions().canAddAnotherAction(ability.getAbilityType()), optionTree);
                }

                playerLists.put(ability.getCommand(), names);
                playerLists.getJSONArray(StateObject.type).put(ability.getCommand());
            }
            if(player.is(JailCreate.abilityType)){
                JailCreate jCard = player.getAbility(JailCreate.class);
                PlayerList jailedTargets = jCard.getJailedTargets();
                if(!jailedTargets.isEmpty() && JailExecute.lynchedHappened(game)){
                    names = getJPlayerArray(playerUserIDMap, jailedTargets, false, null);
                    playerLists.put(JailCreate.NO_EXECUTE, names);
                    playerLists.getJSONArray(StateObject.type).put(JailCreate.NO_EXECUTE);
                }
            }
        }
    }

    public void addSetup(JSONObject state, Game game) throws JSONException {
        state.put("setup", new SetupJson(game));
    }

    public JSONObject getObject() {
        return baseJson;
    }

    public void write(long userID, JSONObject jo) {
        try{
            WebCommunicator.pushToUser(jo, userID);
        }catch(JSONException e){
            e.printStackTrace();
        }
    }

    public JSONObject construct(Optional<Player> player, Lobby lobby) throws JSONException {
        JSONObject obj = getObject();
        for(String state: states){
            if(state.equals(FACTIONS))
                addJFactions(obj);
            else if(state.equals(PLAYERLISTS))
                addJPlayerLists(obj, lobby, player);
            else if(state.equals(DAYLABEL))
                addJDayLabel(obj);
            else if(state.equals(GRAVEYARD))
                addJGraveYard(obj);
            else if(state.equals(ROLEINFO))
                addJRoleInfo(player, obj);
            else if(state.equals(ACTIONS))
                addJActions(player, obj);
            else if(state.equals(SETUP))
                addSetup(obj, lobby.game);

        }
        for(String key: extraKeys.keySet())
            obj.put(key, extraKeys.get(key));
        return obj;
    }

    public JSONObject constructAndSend(long userID, Optional<Player> player, Lobby game) {
        try{
            JSONObject obj = construct(player, game);
            write(userID, obj);
            return obj;
        }catch(JSONException e){
            e.printStackTrace();
        }
        return null;
    }

    public StateObject addKey(String key, Object val) {
        extraKeys.put(key, val);
        return this;
    }

    public void removeKey(String key) {
        extraKeys.remove(key);
    }

    public static ArrayList<String> getRules(Game game, Faction faction) {
        if(game.isStarted())
            return FactionJson.getRules(faction, Optional.of(game));
        ArrayList<String> list = new ArrayList<>();

        for(FactionModifierName name: FactionModifierName.values())
            list.add(faction.getColor() + name);
        return list;
    }

    public static final String guiUpdate = "guiUpdate";
    public static final String messageType = "messageType";

    public static final String isPrivateInstance = "isPrivateInstance";

    public static final String unreadUpdate = "unreadUpdate";
    public static final String count = "count";
    public static final String chatReset = "chatReset";
    public static final String chatKey = "chatKey";
    public static final String chatDay = "chatDay";
    public static final String chatType = "chatType";
    public static final String chatClue = "chatClue";
    public static final String chatName = "chatName";
    public static final String sender = "sender";

    public static final String dayLabel = "dayLabel";

    public static final String type = "type";

    public static final String playerList = "playerList";

    public static final String playerLists = "playerLists";

    public static final String requestGameState = "requestGameState";
    public static final String requestChat = "requestChat";
    public static final String requestUnreads = "requestUnreads";
    public static final String setReadChat = "setReadChat";

    public static final String roleType = "roleType";
    public static final String color = "color";
    public static final String description = "description";

    public static final String activeTeams = "activeTeams";

    public static final String displayName = "displayName";
    public static final String roleInfo = "roleInfo";
    public static final String roleColor = "roleColor";
    public static final String roleName = "roleName";
    public static final String roleBaseName = "roleBaseName"; // pretty sure this is unused now
    public static final String abilities = "abilities";
    public static final String roleDescription = "roleDescription";
    public static final String roleModifier = "roleModifier";
    public static final String puppets = "puppets";
    public static final String isAlive = "isAlive";

    public static final String isPuppeted = "isPuppeted";
    public static final String isBlackmailed = "isBlackmailed";
    public static final String breadCount = "breadCount";

    public static final String teamName = "teamName";
    public static final String teamDescription = "teamDescription";
    public static final String teamAllyColor = "teamAllyColor";
    public static final String teamAllyName = "teamAllyName";
    public static final String teamAllyRole = "teamAllyRole";

    public static final String teamChangeName = "teamChangeName";
    public static final String teamChangeColor = "teamChangeColor";
    public static final String teamChangeDescription = "teamChangeDescription";

    public static final String possibleRandoms = "possibleRandoms";
    public static final String spawn = "spawn";
    public static final String roleID = "roleID";
    public static final String setRandom = "setRandom";
    public static final String key = "key";

    public static final String gameStart = "gameStart";

    public static final String isDay = "isDay";
    public static final String dayNumber = "dayNumber";
    public static final String getDayActions = "getDayActions";

    public static final String graveYard = "graveYard";

    public static final String addAbility = "addAbility";
    public static final String hostName = "hostName";

    public static final String ruleID = "ruleID";
    public static final String val = "val";
    public static final String memberRuleExtras = "memberRuleExtras";
    public static final String modifierMap = "modifierMap";

    public static final String playerName = "playerName";

    public static final String factions = "factions";
    public static final String factionNames = "factionNames";

    public static final String enemy = "enemy";

    public static final String actions = "actions";

    public static final String oldAction = "oldAction";
    public static final String newAction = "newAction";
    public static final String targets = "targets";
    public static final String command = "command";
    public static final String option = "option";
    public static final String option2 = "option2";
    public static final String option3 = "option3";
    public static final String actionList = "actionList";
    public static final String playerNames = "playerNames";

    public static final String action = "action";

    public static final String setupKey = "setupKey";
    public static final String customization = "customization";

    public static final String teamLastWill = "teamLastWill";
    public static final String willText = "willText";

    public static final String releaseToken = "releaseToken";
    public static final String token = "token";

    public static final String speechContent = "speechContent";
}
