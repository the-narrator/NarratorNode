package nnode;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import game.ai.Brain;
import game.ai.Computer;
import game.logic.Game;
import game.logic.Player;
import game.setups.Setup;
import repositories.BaseRepo;
import repositories.Connection;

public class SeedFinder {

    static final int FAIL = -1;

    static final int NEW_GAME_COST = 32;

    static ArrayList<String> setupKeys;
    static HashMap<String, Integer> setupWeights;
    static HashMap<String, Integer> setupMins;
    static HashMap<String, Integer> setupMaxs;
    static int totalSetupWeight;
    static Random r;
    static Connection c;

    public static void main(String[] args) throws SQLException {
        BaseRepo.connection = new Connection();
        try{
            run();
        }catch(SQLException e){
        }
        BaseRepo.connection.close();
    }

    public static void run() throws SQLException {
        instantiate();

        int playerCount, max, min;
        String setupKey;
        long game_seed;
        Key k;
        boolean willRefine;

        while (true){
            setupKey = getSetupKey(); // larger ones are weighted higher

            willRefine = willRefine();

            if(willRefine)
                k = getKey(setupKey);
            else
                k = null;

            if(k == null){
                // System.err.println("new seed!");
                min = setupMins.get(setupKey);
                max = setupMaxs.get(setupKey);
                playerCount = r.nextInt(max - min + 1) + min;
                game_seed = r.nextLong();
            }else{
                playerCount = k.playerCount;
                game_seed = k.seed;
            }

            // System.err.println(setupKey + "\t" + game_seed + "\t" + playerCount);
            for(int i = 0; i < NEW_GAME_COST;){
                if(scopeGame(setupKey, playerCount, game_seed))
                    i++;
            }

            updateVariability(playerCount, setupKey);
        }
    }

    private static boolean willRefine() {
        return r.nextBoolean();
    }

    static void instantiate() throws SQLException {
        setupKeys = new ArrayList<>();
//        for(String s: Setup.SETUP_LIST){
//            setupKeys.add(s);
//        }

        r = new Random();

        setupWeights = new HashMap<>();
        setupMins = new HashMap<>();
        setupMaxs = new HashMap<>();
        totalSetupWeight = 0;
        Setup s;
        int weight;
        for(String key: setupKeys){
            s = null;// Setup.GetSetup(new Narrator(), key);
            weight = 0;
//            for(int i = s.getMinPlayerCount(); i <= s.getMaxPlayerCount(); i++){
//                weight += i;
//            }
            setupWeights.put(key, weight);
            totalSetupWeight += weight;

//            setupMins.put(key, s.getMinPlayerCount());
//            setupMaxs.put(key, s.getMaxPlayerCount());
        }

    }

    static Key getKey(String setup) throws SQLException {
        PreparedStatement ps = c.prepareStatement(
                "SELECT seed, playerCount FROM seed_counts WHERE setup = ? ORDER BY attempts LIMIT 1;");
        ps.setString(1, setup);

        ResultSet rs = ps.executeQuery();
        long seed;
        int playerCount;
        if(rs.next()){
            seed = rs.getLong(1);
            playerCount = rs.getInt(2);
            return new Key(seed, playerCount);
        }
        return null;

    }

    static String getSetupKey() {
        int weightToBeat = r.nextInt(totalSetupWeight);
        for(String s: setupKeys){
            if(setupWeights.get(s) >= weightToBeat)
                return s;
            weightToBeat -= setupWeights.get(s);
        }
        return null;
    }

    static boolean scopeGame(String key, int playerCount, long game_seed) throws SQLException {
        Setup s = null;// Setup.GetSetup(new Narrator(), key);
        Game narrator = null;

        for(int i = 0; i < playerCount; i++)
            narrator.addPlayer(Computer.toLetter(i + 1));

        if(narrator.setup.hiddens.isEmpty())
            return true;

        narrator.setSeed(game_seed);

        long brainSeed = r.nextLong();
        try{
            Brain.EndGame(narrator, brainSeed, null, 3500);
        }catch(Throwable t){
            saveBadSeed(game_seed, brainSeed, key, playerCount);
            return false;
        }
        if(narrator.isInProgress()){
            // no longer recording this
            // saveBadSeed(game_seed, brainSeed, key, playerCount);
            return false;
        }

        incrementTeamWinRate(narrator, game_seed, playerCount, key);
        incrementParticularSetupCount(game_seed, playerCount, key);

        return true;
    }

    private static void updateVariability(int playerCount, String key) throws SQLException {
        String qString = "INSERT INTO seed_variability (seed, playerCount, setup, variability) VALUES (?, ?, ?, ?) ON DUPLICATE KEY UPDATE variability = ?;";
        PreparedStatement update_query = c.prepareStatement(qString);
        update_query.setInt(2, playerCount);
        update_query.setString(3, key);

        qString = "SELECT DISTINCT seed FROM seed_team_win_counts WHERE setup = ? AND playerCount = ?;";
        PreparedStatement seed_query = c.prepareStatement(qString);
        seed_query.setString(1, key);
        seed_query.setInt(2, playerCount);

        qString = "SELECT SUM(team_totals) AS vari FROM (SELECT ABS(((SELECT SUM(success) FROM seed_team_win_counts t_win_inner WHERE t_win.color = t_win_inner.color AND setup = ? AND playerCount = ? ) / (SELECT SUM(sc_inner3.attempts) FROM seed_counts sc_inner3 WHERE setup = ? AND playerCount = ?)) - ((SELECT SUM(t_win_inner2.success) FROM seed_team_win_counts t_win_inner2 WHERE t_win.color = t_win_inner2.color AND t_win_inner2.seed = ? AND setup = ? AND playerCount = ?) / (SELECT SUM(sc_inner2.attempts) FROM seed_counts sc_inner2 WHERE sc_inner2.seed = ? AND setup = ? AND playerCount = ?))) AS team_totals FROM seed_counts sc, seed_team_win_counts t_win WHERE sc.seed = t_win.seed GROUP BY t_win.color) AS x;";
        PreparedStatement vari_query = c.prepareStatement(qString);
        vari_query.setString(1, key);
        vari_query.setInt(2, playerCount);
        vari_query.setString(3, key);
        vari_query.setInt(4, playerCount);
        // vari_query.setLong(5, game_seed);
        vari_query.setString(6, key);
        vari_query.setInt(7, playerCount);
        // vari_query.setLong(8, game_seed);
        vari_query.setString(9, key);
        vari_query.setInt(10, playerCount);

        ResultSet rs = seed_query.executeQuery(), rs_vari;
        BigDecimal vari;
        Long seed;
        while (rs.next()){
            seed = rs.getLong(1);
            vari_query.setLong(5, seed);
            vari_query.setLong(8, seed);

            rs_vari = vari_query.executeQuery();
            rs_vari.next();

            vari = rs_vari.getBigDecimal(1);

            if(vari == null){
                System.err.println(vari_query);
                continue;
            }

            update_query.setLong(1, seed);
            update_query.setBigDecimal(4, vari);
            update_query.setBigDecimal(5, vari);
            update_query.execute();

            rs_vari.close();
        }
        rs.close();

        update_query.close();
        seed_query.close();
        vari_query.close();

    }

    private static void incrementTeamWinRate(Game n, long game_seed, int playerCount, String key) throws SQLException {
        String qString = "INSERT INTO seed_team_win_counts (seed, playerCount, setup, color, success) VALUES (?, ?, ?, ?, 1) ON DUPLICATE KEY UPDATE success = success + 1;";
        PreparedStatement ps = c.prepareStatement(qString);
        ps.setLong(1, game_seed);
        ps.setInt(2, playerCount);
        ps.setString(3, key);

        ArrayList<String> colors = new ArrayList<>();
        for(Player p: n.getAllPlayers()){
            if(p.getGameFaction().getEnemies().isEmpty())
                continue;
            if(colors.contains(p.getColor()))
                continue;
            colors.add(p.getColor());

            if(p.isWinner()){
                ps.setString(4, p.getColor());
                ps.execute();
            }
        }

        ps.close();
    }

    private static void incrementParticularSetupCount(long game_seed, int playerCount, String key) throws SQLException {
        String qString = "INSERT INTO seed_counts (seed, playerCount, setup, attempts) VALUES (?, ?, ?, 1) ON DUPLICATE KEY UPDATE attempts = attempts + 1;";
        PreparedStatement ps = c.prepareStatement(qString);
        ps.setLong(1, game_seed);
        ps.setInt(2, playerCount);
        ps.setString(3, key);
        ps.execute();

        ps.close();
    }

    private static void saveBadSeed(long game_seed, long brainSeed, String key, int playerCount) throws SQLException {
        PreparedStatement ps = c.prepareStatement(
                "INSERT INTO bad_seeds (game_seed, brain_seed, playerCount, setup) VALUES (?, ?, ?, ?);");
        ps.setLong(1, game_seed);
        ps.setLong(2, brainSeed);
        ps.setInt(3, playerCount);
        ps.setString(4, key);
        ps.execute();
        ps.close();
    }

    static class Key {

        int playerCount;
        long seed;

        public Key(long seed, int playerCount) {
            this.seed = seed;
            this.playerCount = playerCount;
        }
    }
}
