package repositories.util;

import java.util.Collection;
import java.util.Iterator;

import models.idtypes.IDType;

public class QueryHelpers {
    public static String ToInClause(String columnName, Collection<? extends Object> ids) {
        StringBuilder clause = new StringBuilder(columnName);
        clause.append(" IN (");

        Iterator<? extends Object> idIterator = ids.iterator();
        while (idIterator.hasNext()){
            clause.append(getValue(idIterator.next()));
            if(idIterator.hasNext())
                clause.append(", ");
            else
                break;
        }

        clause.append(")");
        return clause.toString();
    }

    public static Object getValue(Object o) {
        if(o instanceof Long)
            return o;
        if(o instanceof IDType)
            return ((IDType) o).getValue();
        throw new Error("Unsupported value");
    }
}
