package repositories;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;

import models.schemas.HiddenSchema;
import util.CollectionUtil;

public class HiddenRepo extends BaseRepo {

    public static Future<Set<HiddenSchema>> getBySetupID(long setupID) {
        return executorService.submit(new Callable<Set<HiddenSchema>>() {
            @Override
            public Set<HiddenSchema> call() throws Exception {
                return getBySetupIDSync(setupID);
            }
        });
    }

    public static Set<HiddenSchema> getBySetupIDSync(long setupID) throws SQLException {
        ResultSet rs = executeQuery("SELECT id, name FROM hiddens WHERE setup_id = ?;", setupID);
        Set<HiddenSchema> hiddens = new HashSet<>();
        HiddenSchema hidden;
        while (rs.next()){
            hidden = new HiddenSchema(rs.getLong("id"), rs.getString("name"));
            hiddens.add(hidden);
        }
        rs.close();
        return hiddens;
    }

    public static HiddenSchema getByID(Connection conn, long hiddenID) throws SQLException {
        ResultSet rs = executeQuery("SELECT id, name FROM hiddens WHERE id = ?;", hiddenID);
        HiddenSchema hidden = null;
        while (rs.next()){
            hidden = new HiddenSchema(rs.getLong("id"), rs.getString("name"));
            break;
        }
        rs.close();
        return hidden;
    }

    public static long create(long setupID, String name) throws SQLException {
        return create(setupID, CollectionUtil.toArrayList(name)).get(0);
    }

    public static ArrayList<Long> create(long setupID, ArrayList<String> hiddenNames) throws SQLException {
        if(hiddenNames.isEmpty())
            return new ArrayList<>();
        Object[] params = new Object[hiddenNames.size() * 2];
        StringBuilder query = new StringBuilder();
        query.append("INSERT INTO hiddens (setup_id, name) VALUES");

        for(int i = 0; i < hiddenNames.size(); i++){
            if(i != 0)
                query.append(", ");
            params[i * 2] = setupID;
            params[(i * 2) + 1] = hiddenNames.get(i);
            query.append("(?, ?)");
        }
        query.append(";");
        return BaseRepo.executeInsertQuery(query.toString(), params);
    }

    public static void deleteByID(long setupID, long hiddenID) throws SQLException {
        deleteByIDs(setupID, new ArrayList<>(Arrays.asList(hiddenID)));
    }

    public static void deleteByIDs(long setupID, ArrayList<Long> hiddenIDs) throws SQLException {
        if(hiddenIDs.isEmpty())
            return;
        StringBuilder query = new StringBuilder("");
        query.append("DELETE FROM hiddens WHERE setup_id = ? AND (");

        long hiddenID;
        for(int i = 0; i < hiddenIDs.size(); i++){
            if(i != 0)
                query.append(" OR ");
            hiddenID = hiddenIDs.get(i);
            query.append("id = ");
            query.append(hiddenID);
        }

        query.append(");");
        execute(query.toString(), setupID);
    }

}
