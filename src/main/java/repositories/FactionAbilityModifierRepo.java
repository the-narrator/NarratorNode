package repositories;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;

import models.dbo.ModifierTableNames;
import models.enums.AbilityModifierName;
import models.modifiers.Modifier;
import models.modifiers.Modifiers;
import models.schemas.FactionAbilityModifierSchema;
import util.game.ModifierUtil;

public class FactionAbilityModifierRepo extends ModifierRepo {

    private static ModifierTableNames tableNames = new ModifierTableNames("faction_ability_int_modifiers",
            "faction_ability_bool_modifiers", "faction_ability_string_modifiers");

    private static StringBuilder getQuery(String tableName) {
        StringBuilder query = new StringBuilder();
        query.append(
                "SELECT faim.id, faction_id, faction_ability_id, faim.name, value, min_player_count, max_player_count, upserted_at ");
        query.append("FROM " + tableName + " AS faim ");
        query.append("INNER JOIN faction_abilities ");
        query.append("ON faction_abilities.id = faim.faction_ability_id ");
        query.append("INNER JOIN factions ");
        query.append("ON factions.id = faction_abilities.faction_id ");
        query.append("WHERE setup_id = ?;");
        return query;
    }

    private static FactionAbilityModifierSchema getFromResult(ResultSet rs) throws SQLException {
        long id = rs.getLong("id");
        AbilityModifierName name = AbilityModifierName.valueOf(rs.getString("name").toUpperCase());
        Object value = rs.getObject("value");
        int minPlayerCount = rs.getInt("min_player_count");
        int maxPlayerCount = rs.getInt("max_player_count");
        Instant upsertedAt = rs.getTimestamp("upserted_at").toInstant();
        return new FactionAbilityModifierSchema(id, name, value, minPlayerCount, maxPlayerCount, upsertedAt);
    }

    public static Future<Map<Long, Map<Long, Set<FactionAbilityModifierSchema>>>> getBySetupID(long setupID) {
        return executorService.submit(new Callable<Map<Long, Map<Long, Set<FactionAbilityModifierSchema>>>>() {
            @Override
            public Map<Long, Map<Long, Set<FactionAbilityModifierSchema>>> call() throws Exception {
                return getBySetupIDSync(setupID);
            }
        });
    }

    public static Map<Long, Map<Long, Set<FactionAbilityModifierSchema>>> getBySetupIDSync(long setupID)
            throws SQLException {
        Map<Long, Map<Long, Set<FactionAbilityModifierSchema>>> modifiers = new HashMap<>();

        StringBuilder query;
        ResultSet rs;
        long factionID, factionAbilityID;
        FactionAbilityModifierSchema modifier;
        for(String tableName: tableNames.getNames()){
            query = getQuery(tableName);
            rs = executeQuery(query.toString(), setupID);
            while (rs.next()){
                factionID = rs.getLong("faction_id");
                factionAbilityID = rs.getLong("faction_ability_id");

                modifier = getFromResult(rs);
                makeSpaceInMap(modifiers, factionID, factionAbilityID);
                modifiers.get(factionID).get(factionAbilityID).add(modifier);
            }
            rs.close();
        }

        return modifiers;
    }

    private static void makeSpaceInMap(Map<Long, Map<Long, Set<FactionAbilityModifierSchema>>> modifiers,
            long factionID, long factionAbilityID) {
        if(!modifiers.containsKey(factionID))
            modifiers.put(factionID, new HashMap<>());
        Map<Long, Set<FactionAbilityModifierSchema>> factionAbilities = modifiers.get(factionID);
        if(!factionAbilities.containsKey(factionAbilityID))
            factionAbilities.put(factionAbilityID, new HashSet<>());
    }

    public static void create(ArrayList<Long> factionAbilityIDs,
            ArrayList<Modifiers<AbilityModifierName>> modifiersList) throws SQLException {
        insertModifiers(tableNames, "faction_ability_id", factionAbilityIDs, modifiersList);
    }

    public static void upsertModifier(long factionAbilityID, Modifier<AbilityModifierName> modifier)
            throws SQLException {
        Object value = modifier.value.internalValue;
        StringBuilder query = new StringBuilder();
        query.append("INSERT INTO " + getTableName(modifier.name) + " ");
        query.append("(faction_ability_id, name, value, min_player_count, max_player_count, upserted_at) ");
        query.append("VALUES (?, ?, ?, ?, ?, ?) ON DUPLICATE KEY UPDATE value = ?, upserted_at = ?;");
        execute(query.toString(), factionAbilityID, modifier.name.toString(), value, modifier.minPlayerCount,
                modifier.maxPlayerCount, modifier.upsertedAt, value, modifier.upsertedAt);
    }

    private static String getTableName(AbilityModifierName name) {
        if(ModifierUtil.IsIntModifier(name))
            return tableNames.tableIntName;
        if(ModifierUtil.IsBoolModifier(name))
            return tableNames.tableBoolName;
        return tableNames.tableStringName;

    }

}
