package repositories;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Optional;

import models.Command;
import models.idtypes.GameID;
import models.idtypes.PlayerDBID;

public class CommandRepo extends BaseRepo {

    public static ArrayList<Command> getByReplayID(GameID gameID) throws SQLException {
        ArrayList<Command> commands = new ArrayList<>();
        ResultSet rs = executeQuery(
                "SELECT command, player_id, time_left from commands WHERE replay_id = ? ORDER BY counter;", gameID);
        String command;
        Optional<PlayerDBID> playerID;
        double timeLeft;
        while (rs.next()){
            playerID = Optional.of(new PlayerDBID(rs.getLong("player_id")));
            if(rs.wasNull())
                playerID = Optional.empty();
            command = rs.getString("command");
            timeLeft = rs.getDouble("time_left");
            commands.add(new Command(playerID, command, timeLeft));
        }
        rs.close();
        return commands;
    }

    public static void create(long counter, Optional<PlayerDBID> playerID, String command, double timeLeft,
            GameID gameID) throws SQLException {
        if(playerID.isPresent())
            execute("INSERT INTO commands (counter, player_id, command, replay_id, time_left) VALUES (?, ?, ?, ?, ?);",
                    counter, playerID.get(), command, gameID, timeLeft);
        else
            execute("INSERT INTO commands (counter, player_id, command, replay_id, time_left) VALUES (?, NULL, ?, ?, ?);",
                    counter, command, gameID, timeLeft);
    }

    public static void deleteByReplayID(Connection connection, long replayID) throws SQLException {
        execute("DELETE FROM commands WHERE replay_id = ?;", replayID);
    }

}
