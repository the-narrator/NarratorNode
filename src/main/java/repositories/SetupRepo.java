package repositories;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;

import game.logic.exceptions.NarratorException;
import game.setups.Setup;
import models.schemas.SetupSchema;

public class SetupRepo extends BaseRepo {

    public static Setup create(Setup setup) throws SQLException {
        long setupID = executeInsertQuery(
                "INSERT INTO setups (owner_id, name, slogan, description, is_active) VALUES (?, ?, ?, ?, true)",
                setup.ownerID, setup.getName(), setup.slogan, setup.description).get(0);
        setup.id = setupID;
        return setup;
    }

    public static boolean isEditable(long setupID) throws SQLException {
        ResultSet rs = executeQuery("SELECT is_editable FROM setups WHERE id = ?;", setupID);
        boolean isEditable = rs.next() && rs.getBoolean("is_editable");
        rs.close();
        return isEditable;
    }

    public static void setNotEditable(long setupID) throws SQLException {
        execute("UPDATE setups SET is_editable = false WHERE id = ?", setupID);
    }

    public static void deleteAll() throws SQLException {
        execute("DELETE FROM setups;");
    }

    public static void deleteByID(Long setupID) throws SQLException {
        execute("DELETE FROM setups WHERE id = ?;", setupID);
    }

    public static int getCount() throws SQLException {
        ResultSet rs = executeQuery("SELECT count(*) as count FROM setups;");
        Integer count = null;
        while (rs.next()){
            count = rs.getInt("count");
        }
        rs.close();
        return count;
    }

    public static SetupSchema getSchemaSync(long setupID) throws SQLException {
        SetupSchema setup = new SetupSchema();
        ResultSet rs = executeQuery("SELECT owner_id, is_editable, name, description, slogan FROM setups WHERE id = ?;",
                setupID);
        if(!rs.next()){
            rs.close();
            throw new NarratorException("Setup with that id not found.");
        }

        setup.description = rs.getString("description");
        setup.id = setupID;
        setup.isEditable = rs.getBoolean("is_editable");
        setup.name = rs.getString("name");
        setup.ownerID = rs.getLong("owner_id");
        setup.slogan = rs.getString("slogan");
        rs.close();
        return setup;
    }

    public static Future<SetupSchema> getSchema(long setupID) {
        return executorService.submit(new Callable<SetupSchema>() {
            @Override
            public SetupSchema call() throws Exception {
                return getSchemaSync(setupID);
            }
        });
    }

}
