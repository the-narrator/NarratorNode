package repositories;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;

import models.Role;
import models.dbo.ModifierTableNames;
import models.enums.RoleModifierName;
import models.modifiers.Modifier;
import models.modifiers.Modifiers;
import models.schemas.RoleModifierSchema;
import util.game.ModifierUtil;

public class RoleModifierRepo extends ModifierRepo {

    private static ModifierTableNames tableNames = new ModifierTableNames("role_int_modifiers", "role_bool_modifiers",
            "role_string_modifiers");

    private static RoleModifierSchema getFromResult(ResultSet rs) throws SQLException {
        RoleModifierName name = RoleModifierName.valueOf(rs.getString("name"));
        long id = rs.getLong("id");
        Object value = rs.getObject("value");
        int minPlayerCount = rs.getInt("min_player_count");
        int maxPlayerCount = rs.getInt("max_player_count");
        Instant upsertedAt = rs.getTimestamp("upserted_at").toInstant();
        return new RoleModifierSchema(id, name, value, minPlayerCount, maxPlayerCount, upsertedAt);
    }

    private static String getBySetupIDQuery(String tableName) {
        StringBuilder query = new StringBuilder();
        query.append("SELECT rim.id, role_id, rim.name, value, min_player_count, max_player_count, upserted_at ");
        query.append("FROM " + tableName + " AS rim ");
        query.append("LEFT JOIN roles ON roles.id = rim.role_id ");
        query.append("WHERE setup_id = ?;");
        return query.toString();
    }

    public static Future<Map<Long, Set<RoleModifierSchema>>> getBySetupID(long setupID) {
        return executorService.submit(new Callable<Map<Long, Set<RoleModifierSchema>>>() {
            @Override
            public Map<Long, Set<RoleModifierSchema>> call() throws Exception {
                return getBySetupIDSync(setupID);
            }
        });
    }

    public static Map<Long, Set<RoleModifierSchema>> getBySetupIDSync(long setupID) throws SQLException {
        Map<Long, Set<RoleModifierSchema>> modifiers = new HashMap<>();

        ResultSet rs;
        long roleID;
        for(String tableName: tableNames.getNames()){
            rs = executeQuery(getBySetupIDQuery(tableName), setupID);
            while (rs.next()){
                roleID = rs.getLong("role_id");
                if(!modifiers.containsKey(roleID))
                    modifiers.put(roleID, new HashSet<>());
                modifiers.get(roleID).add(getFromResult(rs));
            }
            rs.close();
        }

        return modifiers;
    }

    public static void create(List<Role> roles) throws SQLException {
        ArrayList<Long> roleIDs = new ArrayList<>();
        ArrayList<Modifiers<RoleModifierName>> modifiersList = new ArrayList<>();
        for(Role role: roles){
            roleIDs.add(role.id);
            modifiersList.add(role.modifiers);
        }
        insertModifiers(tableNames, "role_id", roleIDs, modifiersList);
    }

    public static void upsertModifier(long role_id, Modifier<RoleModifierName> modifier) throws SQLException {
        Object value = modifier.value.internalValue;
        String query = "INSERT INTO " + getTableName(modifier.name)
                + " (role_id, name, value, min_player_count, max_player_count, upserted_at) VALUES (?, ?, ?, ?, ?, ?) ON DUPLICATE KEY UPDATE value = ?, upserted_at = ?;";
        execute(query, role_id, modifier.name.toString(), value, modifier.minPlayerCount, modifier.maxPlayerCount,
                modifier.upsertedAt, value, modifier.upsertedAt);
    }

    private static String getTableName(RoleModifierName name) {
        if(ModifierUtil.IsBoolModifier(name))
            return tableNames.tableBoolName;
        if(ModifierUtil.IsIntModifier(name))
            return "role_int_modifiers";
        return tableNames.tableStringName;
    }

}
