package repositories;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLTimeoutException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;

import models.json_output.SlowGretelSqlQueryJson;
import nnode.Config;
import nnode.NarratorThreadPool;
import nnode.WebCommunicator;
import util.Util;

public class BaseRepo {

    public static ExecutorService executorService = NarratorThreadPool.narratorThreadPool;

    public static void StartDriver() throws ClassNotFoundException {
        Class.forName("org.mariadb.jdbc.Driver");
    }

    public static Connection connection;

    protected static ResultSet executeQuery(String sqlQuery, Object... parameters) throws SQLException {
        long startTime = System.currentTimeMillis();
        ResultSet resultSet = _executeQuery(sqlQuery, parameters);
        long endTime = System.currentTimeMillis();
        logSlowQuery(startTime, endTime, sqlQuery);
        return resultSet;
    }

    private static ResultSet _executeQuery(String sqlQuery, Object... parameters) throws SQLException {
        PreparedStatement s = null;
        try{
            s = connection.prepareStatement(sqlQuery, parameters);
            ResultSet rs = s.executeQuery();
            s.close();
            return rs;
        }catch(SQLException sq){
            if(!(sq instanceof SQLTimeoutException))
                throw sq;
        }

        connection.attemptRestart();
        s = connection.prepareStatement(sqlQuery, parameters);
        ResultSet rs = s.executeQuery();
        s.close();
        return rs;
    }

    protected static void execute(String sqlQuery, List<Object> parameters) throws SQLException {
        execute(sqlQuery, Util.toObjectArray(parameters));
    }

    protected static void execute(String sqlQuery, Object... parameters) throws SQLException {
        long startTime = System.currentTimeMillis();
        _execute(sqlQuery, parameters);
        long endTime = System.currentTimeMillis();
        logSlowQuery(startTime, endTime, sqlQuery);
    }

    private static void logSlowQuery(long startTime, long endTime, String sqlQuery) {
        long length = endTime - startTime;
        if(length > Config.getLong("query_max_time"))
            WebCommunicator.pushOut(new SlowGretelSqlQueryJson(length, sqlQuery));
    }

    private static void _execute(String sqlQuery, Object... parameters) throws SQLException {
        PreparedStatement statement = null;
        try{
            statement = connection.prepareStatement(sqlQuery, parameters);
            statement.execute();
            statement.close();
            return;
        }catch(SQLException sq){
            if(!(sq instanceof SQLTimeoutException))
                throw sq;
        }

        connection.attemptRestart();
        statement = connection.prepareStatement(sqlQuery, parameters);
        statement.execute();
        statement.close();
    }

    protected static ArrayList<Long> executeInsertQuery(String sqlQuery, List<Object> parameters) throws SQLException {
        return executeInsertQuery(sqlQuery, Util.toObjectArray(parameters));
    }

    protected static ArrayList<Long> executeInsertQuery(String sqlQuery, Object... parameters) throws SQLException {
        long startTime = System.currentTimeMillis();
        ArrayList<Long> results = _executeInsertQuery(sqlQuery, parameters);
        long endTime = System.currentTimeMillis();
        logSlowQuery(startTime, endTime, sqlQuery);
        return results;
    }

    private static ArrayList<Long> _executeInsertQuery(String sqlQuery, Object... parameters) throws SQLException {
        PreparedStatement statement = null;
        try{
            statement = connection.prepareInsertStatement(sqlQuery, parameters);
            return insertAndGetIDs(statement);

        }catch(SQLException sq){
            if(!(sq instanceof SQLTimeoutException))
                throw sq;
        }

        connection.attemptRestart();
        statement = connection.prepareInsertStatement(sqlQuery, parameters);
        return insertAndGetIDs(statement);
    }

    private static ArrayList<Long> insertAndGetIDs(PreparedStatement statement) throws SQLException {
        ArrayList<Long> queryIDs = new ArrayList<>();
        statement.executeUpdate();
        ResultSet resultSet = statement.getGeneratedKeys();
        statement.close();

        while (resultSet.next())
            queryIDs.add((long) resultSet.getInt(1));

        resultSet.close();

        return queryIDs;
    }
}
