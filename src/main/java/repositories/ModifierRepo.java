package repositories;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import game.logic.support.rules.ModifierName;
import models.dbo.ModifierTableNames;
import models.modifiers.Modifier;
import models.modifiers.Modifiers;
import util.Util;
import util.game.ModifierUtil;

public abstract class ModifierRepo extends BaseRepo {

    private static StringBuilder getInsertQuery(String tableName, String columnName) {
        StringBuilder query = new StringBuilder();
        query.append("INSERT INTO ");
        query.append(tableName);
        query.append(" (");
        query.append(columnName);
        query.append(", name, value, min_player_count, max_player_count, upserted_at) VALUES ");
        return query;
    }

    protected static <T extends ModifierName> void insertModifiers(ModifierTableNames tableNames, String columnName,
            ArrayList<Long> entityIDs, ArrayList<Modifiers<T>> modifiersList) throws SQLException {
        StringBuilder intQuery = getInsertQuery(tableNames.tableIntName, columnName);
        StringBuilder booleanQuery = getInsertQuery(tableNames.tableBoolName, columnName);
        StringBuilder stringQuery = getInsertQuery(tableNames.tableStringName, columnName);

        List<Object> boolModifierArgs = new LinkedList<>();
        List<Object> intModifierArgs = new LinkedList<>();
        List<Object> stringModifierArgs = new LinkedList<>();

        StringBuilder query;
        List<Object> modifierArgs;
        long entityID;
        Modifiers<T> modifiers;
        for(int i = 0; i < modifiersList.size(); i++){
            entityID = entityIDs.get(i);
            modifiers = modifiersList.get(i);
            for(Modifier<T> modifier: modifiers){
                query = ModifierUtil.IsStringModifier(modifier.name) ? stringQuery
                        : ModifierUtil.IsBoolModifier(modifier.name) ? booleanQuery : intQuery;
                query.append("(?, ?, ?, ?, ?, ?), ");

                modifierArgs = ModifierUtil.IsStringModifier(modifier.name) ? stringModifierArgs
                        : ModifierUtil.IsBoolModifier(modifier.name) ? boolModifierArgs : intModifierArgs;
                modifierArgs.add(entityID);
                modifierArgs.add(modifier.name.toString());
                modifierArgs.add(modifier.value.internalValue);
                modifierArgs.add(modifier.minPlayerCount);
                modifierArgs.add(modifier.maxPlayerCount);
                modifierArgs.add(modifier.upsertedAt);
            }
        }

        executeIfArgs(intQuery, intModifierArgs);
        executeIfArgs(booleanQuery, boolModifierArgs);
        executeIfArgs(stringQuery, stringModifierArgs);
    }

    protected static void executeIfArgs(StringBuilder query, List<Object> args) throws SQLException {
        if(args.isEmpty())
            return;
        query.delete(query.length() - 2, query.length() - 1);
        query.append(";");
        execute(query.toString(), Util.toObjectArray(args));

    }

    public static <T extends ModifierName> void updateModifier(Connection connection, String tableName,
            Modifier<T> modifier, long gameID) throws SQLException {
        execute("UPDATE " + tableName + " SET value = ? WHERE id = ? AND game_id = ?;", modifier.value.internalValue,
                modifier.id, gameID);
    }
}
