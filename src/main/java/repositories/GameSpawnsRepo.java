package repositories;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import models.idtypes.GameID;

public class GameSpawnsRepo extends BaseRepo {

    public static void create(GameID id, Map<Long, Long> spawns) throws SQLException {
        StringBuilder query = new StringBuilder();
        query.append("INSERT INTO game_spawns (game_id, setup_hidden_id, faction_role_id) VALUES ");
        List<Object> params = new LinkedList<>();

        Iterator<Long> setupHiddenIterator = spawns.keySet().iterator();
        long setupHiddenID;
        for(int i = 0; i < spawns.size(); i++){
            if(i != 0)
                query.append(", ");
            query.append("(?, ?, ?)");
            setupHiddenID = setupHiddenIterator.next();
            params.add(id);
            params.add(setupHiddenID);
            params.add(spawns.get(setupHiddenID));
        }

        execute(query.toString(), params);
    }

    public static Map<Long, Long> getByGameID(GameID gameID) throws SQLException {
        Map<Long, Long> assignments = new HashMap<>();
        String query = "SELECT setup_hidden_id, faction_role_id FROM game_spawns WHERE game_id = ?;";
        ResultSet results = executeQuery(query, gameID);
        long setupHiddenID, factionRoleID;
        while (results.next()){
            setupHiddenID = results.getLong("setup_hidden_id");
            factionRoleID = results.getLong("faction_role_id");
            assignments.put(setupHiddenID, factionRoleID);
        }
        results.close();
        return assignments;
    }

    public static void deleteByGameID(Connection connection, long gameID) throws SQLException {
        String query = "DELETE FROM game_spawns WHERE game_id = ?;";
        execute(query, gameID);
    }
}
