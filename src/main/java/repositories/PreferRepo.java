package repositories;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import models.PreferenceSchema;
import models.enums.PreferType;
import repositories.util.QueryHelpers;
import util.CollectionUtil;

public class PreferRepo extends BaseRepo {

    public static void createPreference(long userID, long setupID, long entityID, PreferType preferType)
            throws SQLException {
        createPreference(userID, setupID, CollectionUtil.toSet(entityID), preferType);
    }

    public static void createPreference(long userID, long setupID, Set<Long> entityIDs, PreferType preferType)
            throws SQLException {
        if(entityIDs.isEmpty())
            return;
        StringBuilder query = new StringBuilder();
        query.append("INSERT IGNORE INTO player_preferences (user_id, setup_id, entity_id, prefer_type) VALUES");
        List<Object> queryArgs = new LinkedList<>();

        Iterator<Long> iterator = entityIDs.iterator();
        long entityID;
        for(int i = 0; i < entityIDs.size(); i++){
            if(i != 0)
                query.append(", ");
            entityID = iterator.next();
            query.append("(?, ?, ?, ?)");
            queryArgs.add(userID);
            queryArgs.add(setupID);
            queryArgs.add(entityID);
            queryArgs.add(preferType.toString());
        }
        query.append(";");
        execute(query.toString(), queryArgs);
    }

    public static Map<Long, Set<PreferenceSchema>> get(Set<Long> userIDs, long setupID) throws SQLException {
        Map<Long, Set<PreferenceSchema>> preferMap = new HashMap<>();
        StringBuilder query = new StringBuilder();
        query.append("SELECT user_id, entity_id, prefer_type ");
        query.append("FROM player_preferences ");
        query.append("WHERE setup_id = ? ");
        query.append("AND ");
        query.append(QueryHelpers.ToInClause("user_id", userIDs));
        query.append(";");

        ResultSet rs = BaseRepo.executeQuery(query.toString(), setupID);

        long userID;
        Set<PreferenceSchema> schemas;
        PreferType preferType;
        while (rs.next()){
            userID = rs.getLong("user_id");
            if(!preferMap.containsKey(userID))
                preferMap.put(userID, new HashSet<>());
            schemas = preferMap.get(userID);
            preferType = PreferType.valueOf(rs.getString("prefer_type"));
            schemas.add(new PreferenceSchema(rs.getLong("entity_id"), preferType));
        }

        rs.close();
        return preferMap;
    }

    public static void delete(long setupID, long roleID, PreferType preferType) throws SQLException {
        StringBuilder query = new StringBuilder();
        query.append("DELETE FROM player_preferences ");
        query.append("WHERE setup_id = ? AND entity_id = ? AND prefer_type = ?;");
        execute(query.toString(), setupID, roleID, preferType.toString());
    }

}
