package repositories;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Set;

import repositories.util.QueryHelpers;

public class UserPermissionRepo extends BaseRepo {

    public static boolean containsPermission(Set<Long> userIDs, int permission) throws SQLException {
        if(userIDs.isEmpty())
            return false;
        StringBuilder query = new StringBuilder();
        query.append("SELECT 1 FROM user_permissions ");
        query.append("WHERE permission_type = ? ");
        query.append("AND ");
        query.append(QueryHelpers.ToInClause("user_id", userIDs));
        query.append(";");

        ResultSet rs = executeQuery(query.toString(), permission);
        boolean hasPermission = rs.isBeforeFirst();

        rs.close();
        return hasPermission;
    }

    public static boolean hasPermission(long userID, int permission) throws SQLException {
        String sqlQuery = "SELECT 1 FROM user_permissions WHERE user_id = ? AND permission_type = ?;";

        ResultSet rs = executeQuery(sqlQuery, userID, permission);
        boolean hasPermission = rs.isBeforeFirst();

        rs.close();
        return hasPermission;
    }

    public static void addPermission(long userID, int permissionType) throws SQLException {
        String sqlQuery = "INSERT IGNORE INTO user_permissions (user_id, permission_type) VALUES (?, ?);";
        execute(sqlQuery, userID, permissionType);
    }

    public static void removeAllPermissions(long userID) throws SQLException {
        String sqlQuery = "DELETE FROM user_permissions WHERE user_id = ?;";
        execute(sqlQuery, userID);
    }

    public static ArrayList<Long> getPermissions(long userID) throws SQLException {
        ArrayList<Long> permissions = new ArrayList<>();
        String sqlQuery = "SELECT permission_type FROM user_permissions WHERE user_id = ?;";
        ResultSet rs = executeQuery(sqlQuery, userID);
        while (rs.next()){
            permissions.add(rs.getLong("permission_type"));
        }

        rs.close();

        return permissions;
    }

}
