package repositories;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.ArrayList;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;

import game.logic.exceptions.NarratorException;
import models.enums.SqlErrorCodes;

public class FactionEnemyRepo extends BaseRepo {

    public static Future<ArrayList<String[]>> getEnemyColors(long setupID) {
        return executorService.submit(new Callable<ArrayList<String[]>>() {
            @Override
            public ArrayList<String[]> call() throws Exception {
                return getEnemyColorsSync(setupID);
            }
        });
    }

    public static ArrayList<String[]> getEnemyColorsSync(long setupID) throws SQLException {
        String query = "SELECT a.color enemy1, b.color enemy2 " + "FROM faction_enemies " + "LEFT JOIN factions a "
                + "ON a.id = faction_enemies.enemy_a " + "LEFT JOIN factions b " + "ON b.id = faction_enemies.enemy_b "
                + "WHERE a.setup_id = ?;";
        ResultSet rs = executeQuery(query, setupID);
        ArrayList<String[]> enemyPairings = new ArrayList<>();
        String[] enemyPairing;
        while (rs.next()){
            enemyPairing = new String[] { rs.getString("enemy1"), rs.getString("enemy2") };
            enemyPairings.add(enemyPairing);
        }
        rs.close();
        return enemyPairings;
    }

    public static void create(long setupID, long factionID, long factionEnemyID) throws SQLException {
        if(factionID > factionEnemyID){
            create(setupID, factionEnemyID, factionID);
            return;
        }
        try{
            execute("INSERT INTO faction_enemies (setup_id, enemy_a, enemy_b) VALUES (?, ?, ?);", setupID, factionID,
                    factionEnemyID);
        }catch(SQLIntegrityConstraintViolationException e){
            if(e.getErrorCode() != SqlErrorCodes.DUPLICATE_ENTRY)
                throw new NarratorException("Faction id does not exist.");
        }
    }

    public static void delete(long setupID, long factionID, long factionEnemyID) throws SQLException {
        if(factionID > factionEnemyID){
            delete(setupID, factionEnemyID, factionID);
            return;
        }
        execute("DELETE FROM faction_enemies WHERE setup_id = ? AND enemy_a = ? AND enemy_b = ?;", setupID, factionID,
                factionEnemyID);
    }

}
