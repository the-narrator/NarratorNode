package repositories;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.ArrayList;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;

import game.logic.exceptions.NarratorException;
import models.enums.SqlErrorCodes;
import models.schemas.SheriffCheckableSchema;

public class SheriffCheckableRepo extends BaseRepo {

    public static Future<ArrayList<SheriffCheckableSchema>> getSheriffCheckableColors(long setupID) {
        return executorService.submit(new Callable<ArrayList<SheriffCheckableSchema>>() {
            @Override
            public ArrayList<SheriffCheckableSchema> call() throws Exception {
                return getSheriffCheckableColorsSync(setupID);
            }
        });
    }

    public static ArrayList<SheriffCheckableSchema> getSheriffCheckableColorsSync(long setupID) throws SQLException {
        String query = "SELECT a.color sheriff_color, b.color detectable_color " + "FROM sheriff_checkables "
                + "LEFT JOIN factions a " + "ON a.id = sheriff_checkables.sheriff_faction_id " + "LEFT JOIN factions b "
                + "ON b.id = sheriff_checkables.detectable_faction_id " + "WHERE a.setup_id = ?;";
        ArrayList<SheriffCheckableSchema> sheriffCheckables = new ArrayList<>();
        ResultSet rs = executeQuery(query, setupID);
        SheriffCheckableSchema sheriffCheckable;
        while (rs.next()){
            sheriffCheckable = new SheriffCheckableSchema();
            sheriffCheckable.sheriffColor = rs.getString("sheriff_color");
            sheriffCheckable.detectableColor = rs.getString("detectable_color");

            sheriffCheckables.add(sheriffCheckable);
        }
        rs.close();
        return sheriffCheckables;
    }

    public static void create(long factionID, long factionCheckableID) throws SQLException {
        try{
            execute("INSERT INTO sheriff_checkables (sheriff_faction_id, detectable_faction_id) VALUES (?, ?);",
                    factionID, factionCheckableID);
        }catch(SQLIntegrityConstraintViolationException e){
            if(e.getErrorCode() != SqlErrorCodes.DUPLICATE_ENTRY)
                throw new NarratorException("Faction id does not exist.");
        }
    }
}
