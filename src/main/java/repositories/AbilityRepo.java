package repositories;

import java.sql.SQLException;
import java.util.ArrayList;

import models.requests.AbilityCreateRequest;

public class AbilityRepo extends BaseRepo {
    public static ArrayList<Long> insertAbilities(String abilityTableName, String entityColumnName,
            ArrayList<Long> abilityEntityIDs, ArrayList<AbilityCreateRequest> roleAbilities) throws SQLException {
        if(roleAbilities.isEmpty())
            return new ArrayList<>();

        Object[] queryArgs = new Object[roleAbilities.size() * 2];
        StringBuilder query = new StringBuilder();
        query.append("INSERT INTO ");
        query.append(abilityTableName);
        query.append(" (");
        query.append(entityColumnName);
        query.append(", name) VALUES");

        for(int i = 0; i < roleAbilities.size(); i++){
            if(i != 0)
                query.append(',');
            query.append(" (?, ?)");
            queryArgs[i * 2] = abilityEntityIDs.get(i);
            queryArgs[(i * 2) + 1] = roleAbilities.get(i).abilityType.dbName;
        }
        query.append(";");
        return executeInsertQuery(query.toString(), queryArgs);
    }
}
