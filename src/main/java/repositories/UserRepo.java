package repositories;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import game.logic.exceptions.NarratorException;
import repositories.util.QueryHelpers;
import util.CollectionUtil;

public class UserRepo extends BaseRepo {

    public static void updatePointsAndWinRate(long userID, int pointIncrease) throws SQLException {
        double userWinRatio = getUserWinRatio(userID);
        execute("UPDATE users SET points = points + ?, win_rate = ? WHERE id = ?;", pointIncrease, userWinRatio,
                userID);
    }

    private static double getUserWinRatio(long userID) throws SQLException {
        String query = "SELECT COUNT(1), is_winner FROM players WHERE user_id = ? and is_winner is not NULL GROUP BY is_winner;"; // todo

        ResultSet rs = executeQuery(query, userID);
        Double wins = null;
        Double losses = null;
        while (rs.next()){
            if(rs.getBoolean(2))
                wins = (double) rs.getInt(1);
            else
                losses = (double) rs.getInt(1);
        }
        rs.close();

        if(wins == null)
            wins = 0.0;
        if(losses == null)
            losses = 0.0;
        if(wins.equals(wins + losses))
            losses++;
        return (wins / (losses + wins)) * 100;
    }

    public static void clearPointsAndWinRate() throws SQLException {
        execute("UPDATE users SET points = 0, win_rate = 0;");
    }

    public static Long getByName(Connection c, String name) throws SQLException {
        ResultSet rs = executeQuery("SELECT id FROM users WHERE token = ?;", name);
        if(!rs.next()){
            rs.close();
            return null;
        }
        long userID = rs.getLong("id");
        rs.close();
        return userID;
    }

    public static long create(String name) throws SQLException {
        StringBuilder query = new StringBuilder();
        query.append("INSERT INTO users (name) VALUES (?);");
        return executeInsertQuery(query.toString(), name).get(0);
    }

    public static void deleteIDs(Collection<Long> ids) throws SQLException {
        if(ids.isEmpty())
            return;
        StringBuilder query = new StringBuilder();
        Object[] queryArgs = new Long[ids.size()];
        query.append("DELETE FROM users WHERE ID IN (");

        Iterator<Long> iterator = ids.iterator();
        for(int i = 0; i < ids.size(); i++){
            if(i != 0)
                query.append(",");
            query.append("?");
            queryArgs[i] = iterator.next();
        }
        query.append(");");
        execute(query.toString(), queryArgs);
    }

    public static void deleteAll() throws SQLException {
        execute("DELETE FROM users;");
    }

    public static String getName(long userID) throws SQLException {
        Map<Long, String> map = getNames(CollectionUtil.toSet(userID));
        String name = map.get(userID);
        if(name != null)
            return name;
        throw new NarratorException("No user with that ID found.");
    }

    public static Map<Long, String> getNames(Set<Long> userIDs) throws SQLException {
        StringBuilder query = new StringBuilder();
        query.append("SELECT id, name FROM users WHERE ");
        query.append(QueryHelpers.ToInClause("id", userIDs));
        query.append(";");
        ResultSet rs = executeQuery(query.toString());
        Map<Long, String> map = new HashMap<>();
        while (rs.next())
            map.put(rs.getLong("id"), rs.getString("name"));

        rs.close();
        return map;
    }

}
