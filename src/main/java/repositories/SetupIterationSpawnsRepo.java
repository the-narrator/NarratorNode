package repositories;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import models.schemas.SetupHiddenSpawnSchema;

public class SetupIterationSpawnsRepo extends BaseRepo {
    public static List<SetupHiddenSpawnSchema> getByIterationID(long setupIterationID)
            throws SQLException {
        List<SetupHiddenSpawnSchema> schemas = new LinkedList<>();
        ResultSet resultSet = executeQuery("SELECT setup_hidden_id, faction_role_id FROM setup_iteration_spawns WHERE setup_iteration_id = ?;",
                setupIterationID);

        while (resultSet.next()){
            schemas.add(new SetupHiddenSpawnSchema(resultSet.getLong("setup_hidden_id"),
                    resultSet.getLong("faction_role_id")));
        }
        resultSet.close();
        return schemas;
    }
}
