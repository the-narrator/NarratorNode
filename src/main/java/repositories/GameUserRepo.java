package repositories;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import models.idtypes.GameID;
import models.schemas.GameUserSchema;
import repositories.util.QueryHelpers;

public class GameUserRepo extends BaseRepo {

    public static Set<Long> getIDs(GameID gameID) throws SQLException {
        StringBuilder query = new StringBuilder();
        query.append("SELECT user_id FROM game_users ");
        query.append("WHERE game_id = ?;");

        Set<Long> userIDs = new HashSet<>();
        ResultSet rs = executeQuery(query.toString(), gameID);
        while (rs.next())
            userIDs.add(rs.getLong("user_id"));

        rs.close();
        return userIDs;
    }

    public static Set<Long> getActiveIDsByGameID(GameID gameID) throws SQLException {
        StringBuilder query = new StringBuilder();
        query.append("SELECT user_id FROM game_users ");
        query.append("WHERE game_id = ? ");
        query.append("AND is_exited = false;");

        Set<Long> userIDs = new HashSet<>();
        ResultSet rs = executeQuery(query.toString(), gameID);
        while (rs.next())
            userIDs.add(rs.getLong("user_id"));

        rs.close();
        return userIDs;
    }

    public static Map<GameID, Set<Long>> getModeratorIDs(Set<GameID> gameIDs) throws SQLException {
        Map<GameID, Set<Long>> map = new HashMap<>();
        if(gameIDs.isEmpty())
            return map;
        StringBuilder query = new StringBuilder();
        query.append("SELECT game_id, user_id FROM game_users ");
        query.append("WHERE ");
        query.append(QueryHelpers.ToInClause("game_id", gameIDs));
        query.append(" AND is_moderator = true;");

        ResultSet rs = executeQuery(query.toString());
        GameID gameID;
        long userID;
        while (rs.next()){
            gameID = new GameID(rs.getLong("game_id"));
            if(!map.containsKey(gameID))
                map.put(gameID, new HashSet<>());
            userID = rs.getLong("user_id");
            map.get(gameID).add(userID);
        }
        rs.close();

        return map;
    }

    public static Optional<GameID> getActiveGameIDByUserID(long userID) throws SQLException {
        StringBuilder query = new StringBuilder();
        query.append("SELECT game_id FROM game_users ");
        query.append("WHERE user_id = ? AND is_exited = false;");

        ResultSet rs = executeQuery(query.toString(), userID);
        Optional<GameID> gameID = rs.next() ? Optional.of(new GameID(rs.getLong("game_id"))) : Optional.empty();
        rs.close();
        return gameID;
    }

    public static Optional<GameUserSchema> getActiveByUserID(long userID) throws SQLException {
        StringBuilder query = new StringBuilder();
        query.append("SELECT is_moderator, game_id FROM game_users ");
        query.append("WHERE user_id = ? AND is_exited = false;");

        ResultSet rs = executeQuery(query.toString(), userID);
        if(!rs.next()){
            rs.close();
            return Optional.empty();
        }
        long gameID = rs.getLong("game_id");
        boolean isModerator = rs.getBoolean("is_moderator");
        GameUserSchema user = new GameUserSchema(userID, new GameID(gameID), isModerator, false);
        rs.close();
        return Optional.of(user);
    }

    public static Set<Long> getModeratorIDs(GameID gameID) throws SQLException {
        StringBuilder query = new StringBuilder();
        query.append("SELECT user_id FROM game_users ");
        query.append("WHERE game_id = ? AND is_moderator = true;");

        ResultSet rs = executeQuery(query.toString(), gameID);
        Set<Long> moderatorIDs = new HashSet<>();
        while (rs.next())
            moderatorIDs.add(rs.getLong("user_id"));

        rs.close();
        return moderatorIDs;
    }

    public static void insert(GameUserSchema gameUserSchema) throws SQLException {
        StringBuilder query = new StringBuilder();
        query.append("INSERT IGNORE INTO game_users (user_id, game_id, is_moderator, is_exited) ");
        query.append("VALUES (?, ?, ?, ?);");

        executeInsertQuery(query.toString(), gameUserSchema.id, gameUserSchema.gameID, gameUserSchema.isModerator,
                gameUserSchema.isExited);
    }

    public static void setModerator(long userID, GameID gameID) throws SQLException {
        StringBuilder query = new StringBuilder();
        query.append("UPDATE game_users ");
        query.append("SET is_moderator = (user_id = ?) ");
        query.append("WHERE game_id = ?");

        executeQuery(query.toString(), userID, gameID);
    }

    public static void setExitedExcept(long userID, GameID gameID) throws SQLException {
        execute("UPDATE game_users SET is_exited = true WHERE user_id = ? AND game_id != ?;", userID, gameID);
    }

    public static void setExited(GameID gameID, long userID) throws SQLException {
        execute("UPDATE game_users SET is_exited = true WHERE game_id = ? and user_id = ?;", gameID, userID);
    }

    public static void setExitedForAllPlayers(GameID gameID) throws SQLException {
        execute("UPDATE game_users SET is_exited = true WHERE game_id = ?;", gameID);
    }

    public static void deleteByUserIDGameID(long userID, GameID gameID) throws SQLException {
        StringBuilder query = new StringBuilder();
        query.append("DELETE FROM game_users ");
        query.append("WHERE game_id = ? ");
        query.append("AND user_id = ?");

        executeQuery(query.toString(), gameID, userID);
    }

    public static Set<GameUserSchema> getByGameID(GameID gameID) throws SQLException {
        StringBuilder query = new StringBuilder();
        query.append("SELECT user_id, is_moderator, is_exited FROM game_users ");
        query.append("WHERE game_id = ?;");

        ResultSet rs = executeQuery(query.toString(), gameID);
        Set<GameUserSchema> users = new HashSet<>();
        GameUserSchema user;
        while (rs.next()){
            user = new GameUserSchema(rs.getLong("user_id"), gameID, rs.getBoolean("is_moderator"),
                    rs.getBoolean("is_exited"));
            users.add(user);
        }

        rs.close();
        return users;
    }
}
