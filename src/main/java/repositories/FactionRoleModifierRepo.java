package repositories;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;

import models.FactionRole;
import models.dbo.ModifierTableNames;
import models.enums.RoleModifierName;
import models.modifiers.Modifier;
import models.modifiers.Modifiers;
import models.schemas.RoleModifierSchema;
import util.CollectionUtil;
import util.game.ModifierUtil;

public class FactionRoleModifierRepo extends ModifierRepo {

    private static ModifierTableNames tableNames = new ModifierTableNames("faction_role_role_int_modifiers",
            "faction_role_role_bool_modifiers", "faction_role_role_string_modifiers");

    public static void createRoleModifiers(ArrayList<FactionRole> factionRoles) throws SQLException {
        ArrayList<Long> factionRoleIDs = new ArrayList<>();
        ArrayList<Modifiers<RoleModifierName>> modifiersList = new ArrayList<>();
        for(FactionRole factionRole: factionRoles){
            factionRoleIDs.add(factionRole.id);
            modifiersList.add(factionRole.roleModifiers);
        }
        insertModifiers(tableNames, "faction_role_id", factionRoleIDs, modifiersList);
    }

    private static String getBySetupIDQuery(String tableName) {
        StringBuilder query = new StringBuilder();
        query.append("SELECT " + tableName
                + ".id, faction_role_id, name, value, min_player_count, max_player_count, upserted_at  ");
        query.append("FROM " + tableName + " ");
        query.append("LEFT JOIN faction_roles ");
        query.append("  ON " + tableName + ".faction_role_id = faction_roles.id ");
        query.append("WHERE setup_id = ?;");
        return query.toString();
    }

    private static RoleModifierSchema getFromResult(ResultSet results) throws SQLException {
        long id = results.getLong("id");
        RoleModifierName name = RoleModifierName.valueOf(results.getString("name"));
        Object value = results.getObject("value");
        int minPlayerCount = results.getInt("min_player_count");
        int maxPlayerCount = results.getInt("max_player_count");
        Instant upsertedAt = results.getTimestamp("upserted_at").toInstant();
        return new RoleModifierSchema(id, name, value, minPlayerCount, maxPlayerCount, upsertedAt);
    }

    public static Future<Map<Long, Set<RoleModifierSchema>>> getRoleModifiers(long setupID) {
        return executorService.submit(new Callable<Map<Long, Set<RoleModifierSchema>>>() {
            @Override
            public Map<Long, Set<RoleModifierSchema>> call() throws Exception {
                return getRoleModifiersSync(setupID);
            }
        });
    }

    public static Map<Long, Set<RoleModifierSchema>> getRoleModifiersSync(long setupID) throws SQLException {
        Map<Long, Set<RoleModifierSchema>> roleModifierMap = new HashMap<>();

        ResultSet rs;
        long factionRoleID;
        RoleModifierSchema modifierSchema;
        for(String tableName: tableNames.getNames()){
            rs = executeQuery(getBySetupIDQuery(tableName), setupID);
            while (rs.next()){
                factionRoleID = rs.getLong("faction_role_id");

                modifierSchema = getFromResult(rs);
                if(roleModifierMap.containsKey(factionRoleID))
                    roleModifierMap.get(factionRoleID).add(modifierSchema);
                else
                    roleModifierMap.put(factionRoleID, CollectionUtil.toSet(modifierSchema));
            }
            rs.close();
        }

        return roleModifierMap;
    }

    public static void upsertRoleModifier(long factionRoleID, Modifier<RoleModifierName> modifier) throws SQLException {
        String tableName = getTableName(modifier.name);
        Object value = modifier.value.internalValue;
        StringBuilder query = new StringBuilder();
        query.append("INSERT INTO " + tableName + " ");
        query.append("(faction_role_id, name, value, min_player_count, max_player_count, upserted_at) ");
        query.append("VALUES (?, ?, ?, ?, ?, ?) ");
        query.append("ON DUPLICATE KEY UPDATE value = ?, upserted_at = ?;");
        execute(query.toString(), factionRoleID, modifier.name.toString(), value, modifier.minPlayerCount,
                modifier.maxPlayerCount, modifier.upsertedAt, value, modifier.upsertedAt);
    }

    private static String getTableName(RoleModifierName name) {
        if(ModifierUtil.IsIntModifier(name))
            return tableNames.tableIntName;
        if(ModifierUtil.IsBoolModifier(name))
            return tableNames.tableBoolName;
        return tableNames.tableStringName;
    }

}
