package repositories;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;

import models.schemas.SetupHiddenSchema;

public class SetupHiddenRepo extends BaseRepo {

    public static Future<ArrayList<SetupHiddenSchema>> getBySetupID(long setupID) {
        return executorService.submit(new Callable<ArrayList<SetupHiddenSchema>>() {
            @Override
            public ArrayList<SetupHiddenSchema> call() throws Exception {
                return getBySetupIDSync(setupID);
            }
        });
    }

    public static ArrayList<SetupHiddenSchema> getBySetupIDSync(long setupID) throws SQLException {
        SetupHiddenSchema setupHiddenSchema;
        ArrayList<SetupHiddenSchema> selectedHiddens = new ArrayList<>();
        ResultSet rs = executeQuery(
                "SELECT id, hidden_id, is_exposed, must_spawn, min_player_count, max_player_count FROM setup_hiddens WHERE setup_id = ?;",
                setupID);
        while (rs.next()){
            setupHiddenSchema = new SetupHiddenSchema(rs.getLong("id"), rs.getLong("hidden_id"),
                    rs.getBoolean("is_exposed"), rs.getBoolean("must_spawn"), rs.getInt("min_player_count"),
                    rs.getInt("max_player_count"));
            selectedHiddens.add(setupHiddenSchema);
        }
        rs.close();
        return selectedHiddens;

    }

    public static long create(long setupID, long hiddenID, boolean isExposed, boolean mustSpawn, int minPlayerCount,
            int maxPlayerCount) throws SQLException {
        return executeInsertQuery(
                "INSERT INTO setup_hiddens (hidden_id, setup_id, min_player_count, max_player_count, must_spawn, is_exposed) VALUES (?, ?, ?, ?, ?, ?);",
                hiddenID, setupID, minPlayerCount, maxPlayerCount, mustSpawn, isExposed).get(0);
    }

    public static void delete(long setupHiddenID, long setupID) throws SQLException {
        execute("DELETE FROM setup_hiddens WHERE setup_id = ? AND id = ?;", setupID, setupHiddenID);
    }

    public static void setExposed(long setupHiddenID, boolean isExposed) throws SQLException {
        execute("UPDATE setup_hiddens SET is_exposed = ? WHERE id = ?;", isExposed, setupHiddenID);
    }
}
