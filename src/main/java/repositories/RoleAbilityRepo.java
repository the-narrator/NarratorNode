package repositories;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;

import game.abilities.util.AbilityUtil;
import models.requests.AbilityCreateRequest;
import models.schemas.AbilitySchema;

public class RoleAbilityRepo extends AbilityRepo {

    public static ArrayList<Long> create(long roleID, ArrayList<AbilityCreateRequest> roleAbilities)
            throws SQLException {
        ArrayList<Long> roleIDs = new ArrayList<>();
        for(@SuppressWarnings("unused")
        AbilityCreateRequest element: roleAbilities)
            roleIDs.add(roleID);
        return create(roleIDs, roleAbilities);
    }

    public static ArrayList<Long> create(ArrayList<Long> roleAbilityRoleIDs,
            ArrayList<AbilityCreateRequest> roleAbilities) throws SQLException {
        return insertAbilities("role_abilities", "role_id", roleAbilityRoleIDs, roleAbilities);
    }

    public static Future<Map<Long, LinkedHashMap<Long, AbilitySchema>>> getBySetupID(long setupID) {
        return executorService.submit(new Callable<Map<Long, LinkedHashMap<Long, AbilitySchema>>>() {
            @Override
            public Map<Long, LinkedHashMap<Long, AbilitySchema>> call() throws Exception {
                return getBySetupIDSync(setupID);
            }
        });
    }

    public static Map<Long, LinkedHashMap<Long, AbilitySchema>> getBySetupIDSync(long setupID) throws SQLException {
        Map<Long, LinkedHashMap<Long, AbilitySchema>> abilities = new HashMap<>();

        StringBuilder query = new StringBuilder();
        query.append("SELECT role_id, ra.id, ra.name FROM role_abilities AS ra ");
        query.append("LEFT JOIN roles ON roles.id = ra.role_id ");
        query.append("WHERE setup_id = ?;");
        ResultSet rs = executeQuery(query.toString(), setupID);

        long roleID;
        AbilitySchema roleAbility;
        while (rs.next()){
            roleID = rs.getLong("role_id");
            if(!abilities.containsKey(roleID))
                abilities.put(roleID, new LinkedHashMap<>());

            roleAbility = new AbilitySchema(rs.getLong("id"), AbilityUtil.getAbilityTypeByDbName(rs.getString("name")));
            abilities.get(roleID).put(roleAbility.id, roleAbility);
        }

        return abilities;
    }

}
