package repositories;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;

import models.requests.FactionCreateRequest;
import models.requests.FactionUpdateRequest;
import models.schemas.FactionSchema;
import util.CollectionUtil;

public class FactionRepo extends BaseRepo {

    public static Future<Set<FactionSchema>> getBySetupID(long setupID) {
        return executorService.submit(new Callable<Set<FactionSchema>>() {
            @Override
            public Set<FactionSchema> call() throws Exception {
                Set<FactionSchema> factions = new HashSet<>();
                ResultSet rs = executeQuery("SELECT id, name, color, description FROM factions WHERE setup_id=?;",
                        setupID);
                FactionSchema faction;
                while (rs.next()){
                    faction = new FactionSchema(rs.getLong("id"), rs.getString("color"), rs.getString("name"),
                            rs.getString("description"));
                    factions.add(faction);
                }
                rs.close();
                return factions;
            }
        });
    }

    public static Long getIDBySetupIDAndColor(long setupID, String color) throws SQLException {
        ResultSet rs = executeQuery("SELECT id FROM factions WHERE setup_id=? AND color = ?;", setupID, color);
        Long id = null;
        while (rs.next()){
            id = rs.getLong("id");
            break;
        }
        rs.close();
        return id;
    }

    public static void update(long factionID, FactionUpdateRequest request) throws SQLException {
        execute("UPDATE factions SET name = ?, description = ? WHERE id = ?;", request.name, request.description,
                factionID);
    }

    public static long insertFaction(long setupID, FactionCreateRequest faction) throws SQLException {
        return insertFactions(setupID, Arrays.asList(faction)).get(0);
    }

    public static List<Long> insertFactions(long setupID, List<FactionCreateRequest> factions) throws SQLException {
        StringBuilder sb = new StringBuilder("");
        Object queryArgs[] = new Object[factions.size() * 4];
        int counter = 0;
        sb.append("INSERT INTO factions (setup_id, color, name, description) VALUES");

        FactionCreateRequest faction;
        for(int i = 0; i < factions.size(); i++){
            if(i != 0)
                sb.append(", ");
            faction = factions.get(i);
            sb.append("(?, ?, ?, ?)");
            queryArgs[counter++] = setupID;
            queryArgs[counter++] = faction.color;
            queryArgs[counter++] = faction.name;
            queryArgs[counter++] = faction.description;
        }

        sb.append(";");
        return executeInsertQuery(sb.toString(), queryArgs);
    }

    public static void delete(long setupID, long factionID) throws SQLException {
        deleteByIDs(setupID, CollectionUtil.toArrayList(factionID));
    }

    public static void deleteByIDs(long setupID, ArrayList<Long> factionIDs) throws SQLException {
        if(factionIDs.isEmpty())
            return;
        StringBuilder sb = new StringBuilder("");
        sb.append("DELETE FROM factions WHERE setup_id = ? AND (");

        long factionID;
        for(int i = 0; i < factionIDs.size(); i++){
            if(i != 0)
                sb.append(" OR ");
            factionID = factionIDs.get(i);
            sb.append("id = ");
            sb.append(factionID);
        }

        sb.append(");");
        execute(sb.toString(), setupID);
    }

}
