package controllers;

import json.JSONException;
import json.JSONObject;
import models.idtypes.GameID;
import services.ChatService;

import java.sql.SQLException;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

public class ChatController {

    public static void parse(JSONObject request, JSONObject response) throws JSONException, SQLException {
        if(request.getString("method").equals("GET")){
            getChatController(request.getJSONObject("body"), response);
        }else if(request.getString("method").equals("POST")){
            submitGameChatMessage(request.getJSONObject("body"));
        }
    }

    private static void getChatController(JSONObject args, JSONObject response) throws JSONException, SQLException {
        long userID = args.getLong("userID");
        GameID gameID = new GameID(args.getLong("gameID"));
        JSONObject chatsObject = ChatService.getUserChats(userID, gameID);
        if(chatsObject == null){
            List<String> errors = Collections.singletonList("Failed to retrieve chats.");
            response.put("errors", errors);
        }else
            response.put("response", chatsObject);
    }

    private static void submitGameChatMessage(JSONObject args) throws JSONException, SQLException {
        String chatKey = args.getString("chatKey");
        String source = args.getString("source");
        String text = args.getString("text");
        long userID = args.getLong("userID");
        JSONObject chatArgs = args.getJSONObject("args");
        ChatService.submit(userID, chatKey, text, Optional.of(source), chatArgs);
    }

}
