package controllers;

import java.sql.SQLException;
import java.time.Instant;

import json.JSONException;
import json.JSONObject;
import models.enums.AbilityModifierName;
import models.modifiers.AbilityModifier;
import models.requests.FactionRoleAbilityModifierRequest;
import models.serviceResponse.ModifierServiceResponse;
import services.FactionRoleModifierService;

public class FactionRoleAbilityModifierController {

    public static void parse(JSONObject request, JSONObject response) throws JSONException, SQLException {
        if(request.getString("method").equals("POST"))
            upsert(request, response);
    }

    private static void upsert(JSONObject request, JSONObject response) throws JSONException, SQLException {
        JSONObject args = request.getJSONObject("body");

        FactionRoleAbilityModifierRequest modifierRequest = FactionRoleAbilityModifierRequest.fromJson(args);
        AbilityModifier modifier = new AbilityModifier(modifierRequest.name, modifierRequest.value,
                modifierRequest.minPlayerCount, modifierRequest.maxPlayerCount, Instant.now());

        ModifierServiceResponse<AbilityModifierName> serviceResponse = FactionRoleModifierService.upsertAbilityModifier(
                modifierRequest.userID, modifierRequest.factionRoleID, modifierRequest.abilityID, modifier);

        JSONObject modifierJSON = new JSONObject();
        modifierJSON.put("name", modifierRequest.name.toString());
        modifierJSON.put("value", serviceResponse.modifier.value.internalValue);
        JSONObject updateResponse = new JSONObject();
        updateResponse.put("setupID", serviceResponse.setupID);
        updateResponse.put("joinID", serviceResponse.joinID);
        updateResponse.put("modifier", modifierJSON);
        response.put("response", updateResponse);
    }
}
