package controllers;

import java.sql.SQLException;
import java.util.Optional;

import game.logic.Game;
import game.logic.exceptions.NarratorException;
import json.JSONArray;
import json.JSONException;
import json.JSONObject;
import models.OpenLobby;
import models.controllerResponse.BotAddResponseJson;
import models.controllerResponse.PlayerDeleteResponseJson;
import models.idtypes.GameID;
import models.idtypes.PlayerDBID;
import models.json_output.GameJson;
import models.json_output.SetupJson;
import models.serviceResponse.BotAddResponse;
import models.serviceResponse.PlayerKickResponse;
import models.serviceResponse.PlayerNameUpdateResponse;
import services.LobbyService;
import services.PlayerService;

public class PlayerController {

    public static void parse(JSONObject request, JSONObject response) throws JSONException, SQLException {
        if(request.getString("method").equals("POST")){
            createController(request, response);
        }else if(request.getString("method").equals("PUT")){
            JSONObject args = request.getJSONObject("body");
            if(args.has("playerName"))
                updatePlayerName(request, response);
            else
                modKillController(request);
        }else if(request.getString("method").equals("DELETE")){
            deleteUserOrPlayerController(request, response);
        }
    }

    private static void addBotsController(JSONObject request, JSONObject response) throws JSONException, SQLException {
        JSONObject args = request.getJSONObject("body");
        int botCount = args.getInt("botCount");
        if(botCount < 1)
            throw new NarratorException("Bot count must be a positive number");
        long userID = args.getLong("userID");

        BotAddResponse botAddResponse = PlayerService.addBots(userID, botCount);

        response.put("response", new BotAddResponseJson(botAddResponse));
    }

    private static void createController(JSONObject request, JSONObject response) throws JSONException, SQLException {
        JSONObject args = request.getJSONObject("body");
        if(args.has("botCount")){
            addBotsController(request, response);
            return;
        }

        long userID = args.getLong("userID");
        String playerName = args.getString("playerName");

        OpenLobby game;
        if(args.has("joinID"))
            game = PlayerService.joinSpecific(userID, args.getString("joinID"), playerName);
        else
            game = PlayerService.joinAny(userID, playerName);
        response.put("response", new GameJson(game));
    }

    private static void deleteUserOrPlayerController(JSONObject request, JSONObject response)
            throws JSONException, SQLException {
        JSONObject args = request.getJSONObject("body");
        if(args.has("playerKickID")){
            GameID gameID = new GameID(args.getLong("gameID"));
            PlayerDBID botID = new PlayerDBID(args.getLong("playerKickID"));
            Game game = PlayerService.deletePlayer(gameID, botID);
            response.put("response", new PlayerDeleteResponseJson(game));
            return;
        }

        long kickerID = args.getLong("kickerID");
        if(args.has("userID")){
            long kickedPlayerID = args.getLong("userID");
            PlayerKickResponse kickResponse = PlayerService.deleteGameUser(kickerID, kickedPlayerID);

            JSONArray moderatorIDs = new JSONArray();
            for(long moderatorID: kickResponse.moderatorIDs)
                moderatorIDs.put(moderatorID);

            JSONObject leaveResponse = new JSONObject();
            leaveResponse.put("moderatorIDs", moderatorIDs);
            leaveResponse.put("setup", new SetupJson(kickResponse.game));

            response.put("response", leaveResponse);
        }else{
            long kickedUserID = args.getLong("kickedUserID");
            LobbyService.kickGameUserPlayer(kickerID, kickedUserID);
        }
    }

    private static void updatePlayerName(JSONObject request, JSONObject response) throws JSONException, SQLException {
        JSONObject args = request.getJSONObject("body");
        long userID = args.getLong("userID");
        String playerName = args.getString("playerName");
        Optional<PlayerNameUpdateResponse> serviceResponse = PlayerService.updateName(userID, playerName);
        response.put("response", serviceResponse);
    }

    private static void modKillController(JSONObject request) throws JSONException, SQLException {
        JSONObject args = request.getJSONObject("body");

        long userID = args.getLong("userID");
        double timeLeft = args.getDouble("timeLeft");
        PlayerService.modkill(userID, timeLeft);
    }

}
