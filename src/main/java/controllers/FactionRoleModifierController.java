package controllers;

import java.sql.SQLException;
import java.time.Instant;

import game.logic.exceptions.NarratorException;
import json.JSONException;
import json.JSONObject;
import models.ModifierValue;
import models.enums.RoleModifierName;
import models.modifiers.RoleModifier;
import models.serviceResponse.ModifierServiceResponse;
import services.FactionRoleService;

public class FactionRoleModifierController {

    public static void parse(JSONObject request, JSONObject response) throws JSONException, SQLException {
        if(request.getString("method").equals("POST"))
            upsertModifier(request, response);
    }

    private static void upsertModifier(JSONObject request, JSONObject response) throws JSONException, SQLException {
        JSONObject args = request.getJSONObject("body");
        String modifierName = args.getString("name");
        RoleModifierName name;
        try{
            name = RoleModifierName.valueOf(modifierName);
        }catch(IllegalArgumentException e){
            throw new NarratorException("Unknown role modifier name: " + modifierName);
        }
        long userID = args.getLong("userID");
        long factionRoleID = args.getLong("factionRoleID");
        int minPlayerCount = args.getInt("minPlayerCount");
        int maxPlayerCount = args.getInt("maxPlayerCount");
        ModifierValue value = new ModifierValue(args.get("value"));
        RoleModifier modifier = new RoleModifier(name, value, minPlayerCount, maxPlayerCount, Instant.now());

        ModifierServiceResponse<RoleModifierName> serviceResponse = FactionRoleService.upsertModifier(userID,
                factionRoleID, modifier);

        JSONObject modifierJSON = new JSONObject();
        modifierJSON.put("name", modifierName);
        modifierJSON.put("value", serviceResponse.modifier.value.internalValue);
        JSONObject updateResponse = new JSONObject();
        updateResponse.put("joinID", serviceResponse.joinID);
        updateResponse.put("setupID", serviceResponse.setupID);
        updateResponse.put("modifier", modifierJSON);
        response.put("response", updateResponse);
    }
}
