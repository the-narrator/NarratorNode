package controllers;

import json.JSONException;
import json.JSONObject;
import models.idtypes.GameID;
import models.json_output.VoteData;
import services.VoteService;

public class VoteController {

    public static void parse(JSONObject request, JSONObject response) throws JSONException {
        if(request.getString("method").equals("GET")){
            getVotes(request, response);
        }
    }

    private static void getVotes(JSONObject request, JSONObject response) throws JSONException {
        JSONObject args = request.getJSONObject("body");
        GameID gameID = new GameID(args.getLong("gameID"));
        VoteData votes = VoteService.getVotes(gameID);
        response.put("response", votes);
    }
}
