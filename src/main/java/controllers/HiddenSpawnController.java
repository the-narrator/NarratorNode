package controllers;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Set;

import json.JSONArray;
import json.JSONException;
import json.JSONObject;
import models.json_output.HiddenSpawnJson;
import models.requests.HiddenSpawnCreateRequest;
import models.schemas.HiddenSpawnSchema;
import services.HiddenSpawnService;

public class HiddenSpawnController {

    public static void parse(JSONObject request, JSONObject response) throws JSONException, SQLException {
        if(request.getString("method").equals("POST"))
            createHiddenSpawn(request, response);
        else if(request.getString("method").equals("DELETE"))
            deleteHiddenSpawn(request);
    }

    private static void createHiddenSpawn(JSONObject request, JSONObject response) throws JSONException, SQLException {
        JSONObject args = request.getJSONObject("body"), jSpawn;
        long userID = args.getLong("userID");
        JSONArray jSpawns = args.getJSONArray("spawns");

        long hiddenID, factionRoleID;
        int minPlayerCount, maxPlayerCount;
        HiddenSpawnCreateRequest schema;
        ArrayList<HiddenSpawnCreateRequest> spawns = new ArrayList<>();
        for(int i = 0; i < jSpawns.length(); i++){
            jSpawn = jSpawns.getJSONObject(i);
            hiddenID = jSpawn.getLong("hiddenID");
            factionRoleID = jSpawn.getLong("factionRoleID");
            minPlayerCount = jSpawn.getInt("minPlayerCount");
            maxPlayerCount = jSpawn.getInt("maxPlayerCount");
            schema = new HiddenSpawnCreateRequest(hiddenID, factionRoleID, minPlayerCount, maxPlayerCount);
            spawns.add(schema);
        }
        Collection<HiddenSpawnSchema> hiddenSpawns = HiddenSpawnService.addSpawnable(userID, spawns);
        Set<HiddenSpawnJson> hiddenSpawnViews = HiddenSpawnJson.getSet(hiddenSpawns);
        response.put("response", hiddenSpawnViews);
    }

    private static void deleteHiddenSpawn(JSONObject request) throws JSONException, SQLException {
        JSONObject args = request.getJSONObject("body");
        long userID = args.getLong("userID");
        long hiddenSpawnID = args.getLong("hiddenSpawnID");
        HiddenSpawnService.deleteHiddenSpawn(userID, hiddenSpawnID);
    }

}
