package controllers;

import java.sql.SQLException;

import game.logic.exceptions.NarratorException;
import json.JSONException;
import json.JSONObject;
import models.ModifierValue;
import models.enums.GameModifierJson;
import models.enums.GameModifierName;
import models.serviceResponse.ModifierServiceResponse;
import services.GameService;

public class GameModifierController {

    public static void parse(JSONObject request, JSONObject response) throws JSONException, SQLException {
        if(request.getString("method").equals("POST"))
            upsertGameModifier(request, response);
    }

    private static void upsertGameModifier(JSONObject request, JSONObject response) throws JSONException, SQLException {
        JSONObject args = request.getJSONObject("body");
        long userID = args.getLong("userID");
        ModifierValue value = new ModifierValue(args.get("value"));
        String modifierName = args.getString("name");

        GameModifierName name;
        try{
            name = GameModifierName.valueOf(modifierName);
        }catch(IllegalArgumentException e){
            throw new NarratorException("Unknown game modifier name: " + modifierName);
        }

        ModifierServiceResponse<GameModifierName> serviceResponse = GameService.upsertModifier(userID, name, value);

        JSONObject updateResponse = new JSONObject();
        updateResponse.put("joinID", serviceResponse.joinID);
        updateResponse.put("setupID", serviceResponse.setupID);
        updateResponse.put("modifier", new GameModifierJson(serviceResponse.modifier));
        response.put("response", updateResponse);
    }

}
