package controllers;

import java.sql.SQLException;
import java.time.Instant;

import game.logic.exceptions.NarratorException;
import json.JSONException;
import json.JSONObject;
import models.ModifierValue;
import models.enums.SetupModifierName;
import models.modifiers.SetupModifier;
import models.serviceResponse.ModifierServiceResponse;
import services.SetupModifierService;

public class SetupModifierController {

    public static void parse(JSONObject request, JSONObject response) throws JSONException, SQLException {
        if(request.getString("method").equals("POST"))
            upsertSetupModifier(request, response);
    }

    private static void upsertSetupModifier(JSONObject request, JSONObject response)
            throws JSONException, SQLException {
        JSONObject args = request.getJSONObject("body");
        long userID = args.getLong("userID");
        ModifierValue value = new ModifierValue(args.get("value"));
        String modifierName = args.getString("name");

        SetupModifierName name;
        try{
            name = SetupModifierName.valueOf(modifierName);
        }catch(IllegalArgumentException e){
            throw new NarratorException("Unknown setup modifier name: " + modifierName);
        }
        int maxPlayerCount = args.getInt("maxPlayerCount");
        int minPlayerCount = args.getInt("minPlayerCount");
        SetupModifier modifier = new SetupModifier(name, value, minPlayerCount, maxPlayerCount, Instant.now());

        ModifierServiceResponse<SetupModifierName> serviceResponse = SetupModifierService.upsertSetupModifier(userID,
                modifier);

        JSONObject modifierJSON = new JSONObject();
        modifierJSON.put("name", modifierName);
        modifierJSON.put("value", serviceResponse.modifier.value.internalValue);
        JSONObject updateResponse = new JSONObject();
        updateResponse.put("joinID", serviceResponse.joinID);
        updateResponse.put("setupID", serviceResponse.setupID);
        updateResponse.put("modifier", modifierJSON);
        response.put("response", updateResponse);
    }
}
