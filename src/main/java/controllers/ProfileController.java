package controllers;

import java.sql.SQLException;

import json.JSONException;
import json.JSONObject;
import models.json_output.profile.ProfileJson;
import models.serviceResponse.ProfileResponse;
import services.ProfileService;

public class ProfileController {
    public static void parse(JSONObject request, JSONObject response) throws JSONException, SQLException {
        if(request.getString("method").equals("GET"))
            getProfile(request, response);

    }

    private static void getProfile(JSONObject request, JSONObject response) throws JSONException, SQLException {
        JSONObject args = request.getJSONObject("body");
        long userID = args.getLong("userID");

        ProfileResponse user = ProfileService.getProfile(userID);
        response.put("response", new ProfileJson(user));
    }
}
