const BROWSER = 'browser';
const DISCORD = 'discord';
const SC2MAFIA = 'sc2mafia';


function getAll(){
    return [BROWSER, DISCORD, SC2MAFIA];
}

module.exports = {
    getAll,
    BROWSER,
    DISCORD,
    SC2MAFIA,
};
