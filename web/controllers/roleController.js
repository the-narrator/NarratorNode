const intValidators = require('../validators/intValidator');
const roleValidators = require('../validators/roleValidators');

const roleService = require('../services/setup/roleService');
const roleAbilityService = require('../services/setup/roleAbilityService');
const userService = require('../services/userService');

const httpHelpers = require('../utils/httpHelpers');


async function createRole(request){
    const [user, json] = await Promise.all([
        userService.getByAuthToken({
            token: request.headers.auth,
            wrapperType: request.headers.authtype, // headers are lowercased
        }),
        httpHelpers.getJson(request),
    ]);
    roleValidators.createRoleRequest(json);
    return roleService.create(user.id, json);
}

async function getByID(request){
    const user = await userService.getByAuthToken({
        token: request.headers.auth,
        wrapperType: request.headers.authtype, // headers are lowercased
    });
    const roleID = parseInt(request.params.roleID, 10);
    intValidators.idRequest(roleID);
    return roleService.get(user.id, roleID);
}

async function upsertAbilityModifier(request){
    const userPromise = userService.getByAuthToken({
        token: request.headers.auth,
        wrapperType: request.headers.authtype, // headers are lowercased
    });
    const dataPromise = httpHelpers.getJson(request);

    const roleID = parseInt(request.params.roleID, 10);
    intValidators.idRequest(roleID);
    const abilityID = parseInt(request.params.abilityID, 10);
    intValidators.idRequest(abilityID);

    const [user, args] = await Promise.all([userPromise, dataPromise]);
    roleValidators.upsertRoleAbilityModifierRequest(args);
    return roleAbilityService.createModifier(user.id, roleID, abilityID, args);
}

async function upsertModifier(request){
    const [user, args] = await Promise.all([
        userService.getByAuthToken({
            token: request.headers.auth,
            wrapperType: request.headers.authtype, // headers are lowercased
        }),
        httpHelpers.getJson(request)]);
    roleValidators.upsertRoleModifierRequest(args);

    const roleID = parseInt(request.params.roleID, 10);
    intValidators.idRequest(roleID);
    return roleService.createModifier(user.id, roleID, args);
}

async function deleteRole(request){
    const user = await userService.getByAuthToken({
        token: request.headers.auth,
        wrapperType: request.headers.authtype, // headers are lowercased
    });
    const roleID = parseInt(request.params.roleID, 10);
    intValidators.idRequest(roleID);
    await roleService.deleteRole(user.id, roleID);
}

module.exports = {
    createRole,
    getByID,
    upsertAbilityModifier,
    upsertModifier,
    deleteRole,
};
