const helpers = require('../utils/helpers');
const httpHelpers = require('../utils/httpHelpers');

const userService = require('../services/userService');

const intValidator = require('../validators/intValidator');


async function getOnline(){
    return userService.getOnlineUsers();
}

function getAll(request){
    if(isAuthUserLookup(request))
        return getAuthTokenUserID(request);
    return userService.getUsers();
}

function addTempAuth(request){
    const userID = intValidator.idRequest(request.params.userID);
    return userService.addTempAuth(userID);
}

module.exports = {
    getOnline,
    getAll,
    addTempAuth,
};

function isAuthUserLookup(request){
    if(!request.query.auth_token)
        return;

    const authToken = request.query.auth_token;
    if(authToken.length !== 128)
        return false;
    return helpers.isAlphaNumeric(authToken);
}

function getAuthTokenUserID(request){
    const userID = userService.getIDByAuthToken(request.query.auth_token);
    if(!userID)
        throw httpHelpers.httpError(['Auth token not found.'], 422);
    return { userID };
}
