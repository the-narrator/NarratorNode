const setupModifierValidator = require('../validators/setupModifierValidator');
const setupModifierService = require('../services/setup/setupModifierService');
const userService = require('../services/userService');

const httpHelpers = require('../utils/httpHelpers');


async function upsert(request){
    const [user, args] = await Promise.all([
        userService.getByAuthToken({
            token: request.headers.auth,
            wrapperType: request.headers.authtype, // headers are lowercased
        }),
        httpHelpers.getJson(request),
    ]);
    setupModifierValidator.updateSetupModifierRequest(args);
    return setupModifierService.create(user.id, args);
}

module.exports = {
    upsert,
};
