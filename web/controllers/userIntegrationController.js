const userIntegrationService = require('../services/userIntegrationService');


async function addAuthToken(request){
    const { auth, authtype, auth_token: authToken } = request.headers;
    return userIntegrationService.addAuthToken(auth, authtype, authToken);
}

module.exports = {
    addAuthToken,
};
