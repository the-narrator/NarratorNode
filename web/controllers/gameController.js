const userPermissions = require('../models/enums/userPermissions');

const authService = require('../services/authService');
const gameService = require('../services/gameService');
const gameUserService = require('../services/gameUserService');
const userService = require('../services/userService');

const httpHelpers = require('../utils/httpHelpers');

const gameValidators = require('../validators/gameValidators');
const intValidator = require('../validators/intValidator');


async function getDeprecated(request){
    const user = await userService.getByAuthToken({
        token: request.headers.auth,
        wrapperType: request.headers.authtype, // headers are lowercased
    });
    return gameService.getUserState(user.id);
}

async function getActive(request){
    if(request.query.join_id)
        return getByJoinID(request);
    const games = await gameService.getAll();
    return Promise.all(games.map(game => gameService.filterNeededUserInfo(game)));
}

async function getByID(request){
    const gameID = intValidator.idRequest(request.params.gameID);

    let userID;
    try{
        userID = (await userService.getByAuthToken({
            token: request.headers.auth,
            wrapperType: request.headers.authtype, // headers are lowercased
        })).id;
    }catch(err){
        userID = -1;
    }

    const game = await gameService.getByID(gameID, userID);
    return gameService.filterNeededUserInfo(game, userID);
}

async function start(request){
    const user = await userService.getByAuthToken({
        token: request.headers.auth,
        wrapperType: request.headers.authtype, // headers are lowercased
    });
    const gameID = intValidator.idRequest(request.params.gameID);
    await gameUserService.ensureModerator(user.id, gameID);
    return gameService.start(gameID);
}

async function create(request){
    const [user, json] = await Promise.all([
        userService.getByAuthToken({
            token: request.headers.auth,
            wrapperType: request.headers.authtype, // headers are lowercased
        }),
        httpHelpers.getJson(request),
    ]);
    gameValidators.createRequest(json);
    return gameService.create(user.id, json.name, json.setupID);
}

async function deleteGame(request){
    const gameID = intValidator.idRequest(request.params.gameID);
    await authService.validateModRequest(request, userPermissions.GAME_EDITING);
    await gameService.deleteGame(gameID);
}

module.exports = {
    getDeprecated,
    getActive,
    getByID,
    start,
    create,
    deleteGame,
};

async function getByJoinID(request){
    const joinID = request.query.join_id;
    let userID;
    try{
        userID = (await userService.getByAuthToken({
            token: request.headers.auth,
            wrapperType: request.headers.authtype, // headers are lowercased
        })).id;
    }catch(err){
        userID = -1;
    }
    try{
        gameValidators.joinIDRequest(joinID);
    }catch(err){
        return {};
    }
    const game = await gameService.getByLobbyID(joinID);
    return gameService.filterNeededUserInfo(game, userID);
}
