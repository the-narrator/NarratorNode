const moderatorService = require('../services/moderatorService');
const moderatorValidators = require('../validators/moderatorValidators');
const userService = require('../services/userService');

const httpHelpers = require('../utils/httpHelpers');


async function create(request){
    const user = await userService.getByAuthToken({
        token: request.headers.auth,
        wrapperType: request.headers.authtype, // headers are lowercased
    });
    await moderatorService.create(user.id);
}

async function repick(request){
    const [user, args] = await Promise.all([
        userService.getByAuthToken({
            token: request.headers.auth,
            wrapperType: request.headers.authtype, // headers are lowercased
        }),
        httpHelpers.getJson(request),
    ]);
    const repickTarget = args && args.repickTarget;
    moderatorValidators.repickRequest(args);
    return moderatorService.repick(args.gameID, user.id, repickTarget);
}

async function deleteModerator(request){
    const user = await userService.getByAuthToken({
        token: request.headers.auth,
        wrapperType: request.headers.authtype, // headers are lowercased
    });
    await moderatorService.delete(user.id);
}

module.exports = {
    create,
    repick,
    deleteModerator,
};
