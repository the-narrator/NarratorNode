const gameService = require('../services/gameService');
const gameModifierValidator = require('../validators/gameModifierValidator');
const userService = require('../services/userService');

const httpHelpers = require('../utils/httpHelpers');


async function upsert(request){
    const [user, args] = await Promise.all([
        userService.getByAuthToken({
            token: request.headers.auth,
            wrapperType: request.headers.authtype, // headers are lowercased
        }),
        httpHelpers.getJson(request),
    ]);
    gameModifierValidator.updateGameModifierRequest(args);
    return gameService.updateModifier(user.id, args);
}

module.exports = {
    upsert,
};
