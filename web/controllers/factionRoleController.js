const factionRoleService = require('../services/setup/factionRoleService');
// eslint-disable-next-line max-len
const factionRoleAbilityModifierService = require('../services/setup/factionRoleAbilityModifierService');
const factionRoleModifierService = require('../services/setup/factionRoleModifierService');
const userService = require('../services/userService');

const factionRoleValidator = require('../validators/factionRoleValidator');
const factionRoleModifierValidator = require('../validators/factionRoleModifierValidator');
const factionRoleAbilityModifierValidator = require('../validators/factionRoleAbilityModifierValidator'); // eslint-disable-line max-len
const intValidator = require('../validators/intValidator');

const httpHelpers = require('../utils/httpHelpers');


async function create(request){
    const [user, args] = await Promise.all([
        userService.getByAuthToken({
            token: request.headers.auth,
            wrapperType: request.headers.authtype, // headers are lowercased
        }),
        httpHelpers.getJson(request)]);
    factionRoleValidator.createFactionRoleRequest(args);
    return factionRoleService.create(user.id, args);
}

async function get(request){
    const factionRoleID = intValidator.idRequest(request.params.factionRoleID);
    const user = await userService.getByAuthToken({
        token: request.headers.auth,
        wrapperType: request.headers.authtype, // headers are lowercased
    });
    return factionRoleService.get(user.id, factionRoleID);
}

async function upsertAbilityModifier(request){
    const userPromise = userService.getByAuthToken({
        token: request.headers.auth,
        wrapperType: request.headers.authtype, // headers are lowercased
    });
    const dataPromise = httpHelpers.getJson(request);

    const [user, args] = await Promise.all([userPromise, dataPromise]);
    factionRoleAbilityModifierValidator.upsertFactionRoleAbilityModifierRequest(args);
    const factionRoleID = intValidator.idRequest(request.params.factionRoleID);
    const abilityID = intValidator.idRequest(request.params.abilityID);
    return factionRoleAbilityModifierService.upsert(user.id,
        factionRoleID, abilityID, args);
}

async function upsertModifier(request){
    const [user, args] = await Promise.all([
        userService.getByAuthToken({
            token: request.headers.auth,
            wrapperType: request.headers.authtype, // headers are lowercased
        }),
        httpHelpers.getJson(request)]);
    factionRoleModifierValidator.createFactionRoleModifierRequest(args);

    const factionRoleID = intValidator.idRequest(request.params.factionRoleID);
    return factionRoleModifierService.create(user.id, factionRoleID, args);
}

async function update(request){
    const [user, args] = await Promise.all([
        userService.getByAuthToken({
            token: request.headers.auth,
            wrapperType: request.headers.authtype, // headers are lowercased
        }),
        httpHelpers.getJson(request),
    ]);
    factionRoleValidator.updateFactionRoleRequest(args);

    const factionRoleID = intValidator.idRequest(request.params.factionRoleID);
    await factionRoleService.update(user.id, factionRoleID, args);
}

async function deleteFactionRole(request){
    const user = await userService.getByAuthToken({
        token: request.headers.auth,
        wrapperType: request.headers.authtype, // headers are lowercased
    });
    const factionRoleID = intValidator.idRequest(request.params.factionRoleID);
    await factionRoleService.delete(user.id, factionRoleID);
}

module.exports = {
    create,
    get,
    upsertAbilityModifier,
    upsertModifier,
    update,
    deleteFactionRole,
};
