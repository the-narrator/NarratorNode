const factionValidators = require('../validators/factionValidators');
const intValidator = require('../validators/intValidator');

const factionService = require('../services/setup/factionService');
const userService = require('../services/userService');

const httpHelpers = require('../utils/httpHelpers');


async function addAbility(request){
    const factionID = intValidator.idRequest(request.params.factionID);
    const [user, args] = await Promise.all([
        userService.getByAuthToken({
            token: request.headers.auth,
            wrapperType: request.headers.authtype, // headers are lowercased
        }),
        httpHelpers.getJson(request),
    ]);
    factionValidators.addAbilityRequest(args);
    return factionService.addAbility(user.id, factionID, args.ability);
}

async function addEnemy({ headers, params }){
    const factionID = intValidator.idRequest(params.factionID);
    const enemyID = intValidator.idRequest(params.enemyID);
    const user = await userService.getByAuthToken({
        token: headers.auth,
        wrapperType: headers.authtype, // headers are lowercased
    });
    await factionService.addEnemy(user.id, factionID, enemyID);
}

async function addSheriffCheckable(request){
    const factionID = intValidator.idRequest(request.params.factionID);
    const checkableID = intValidator.idRequest(request.params.checkableID);
    const user = await userService.getByAuthToken({
        token: request.headers.auth,
        wrapperType: request.headers.authtype, // headers are lowercased
    });
    await factionService.addSheriffCheckable(user.id, factionID, checkableID);
}

async function deleteAbility(request){
    const factionID = intValidator.idRequest(request.params.factionID);

    const abilityID = intValidator.idRequest(request.params.abilityID);

    const user = await userService.getByAuthToken({
        token: request.headers.auth,
        wrapperType: request.headers.authtype, // headers are lowercased
    });

    await factionService.ensureEditableByUser(user.id, factionID);
    await factionService.deleteAbility({ factionID, abilityID });
}

async function createFaction(request){
    const [user, json] = await Promise.all([
        userService.getByAuthToken({
            token: request.headers.auth,
            wrapperType: request.headers.authtype, // headers are lowercased
        }),
        httpHelpers.getJson(request),
    ]);
    factionValidators.createFactionRequest(json);
    return factionService.create(user.id, json);
}

async function deleteFaction(request){
    const user = await userService.getByAuthToken({
        token: request.headers.auth,
        wrapperType: request.headers.authtype, // headers are lowercased
    });
    const factionID = intValidator.idRequest(request.params.factionID);
    await factionService.deleteFaction({ factionID, userID: user.id });
}

module.exports = {
    addAbility,
    addEnemy,
    addSheriffCheckable,
    deleteAbility,
    createFaction,
    deleteFaction,
};
