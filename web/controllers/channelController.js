const channelService = require('../services/channelService');

const channelValidators = require('../validators/channelValidators');
const intValidators = require('../validators/intValidator');

const httpHelpers = require('../utils/httpHelpers');


async function sc2mafiaNewPost(request){
    const json = await httpHelpers.getJson(request);
    const promises = [];
    if(json.threadID)
        promises.push(channelService.sc2mafiaNewPost(json.threadID));
    if(json.waitForCompletion)
        await Promise.all(promises);
}

async function getActiveBrowserUserIDs(request){
    const gameID = parseInt(request.query.game_id, 10);

    intValidators.idRequest(gameID);
    channelValidators.getActiveUserIDsRequest(gameID);
    const userIDs = await channelService.getActiveBrowserUserIDs(gameID);
    return [...userIDs];
}

async function getCondorcetVotes(request){
    const threadID = parseInt(request.query.threadID, 10);
    channelValidators.getCondorcetVotesRequest(threadID);
    return channelService.getCondorcetVotes(threadID);
}

async function clearCache(request){
    await channelService.clearCache(request.query.threadid);
}

module.exports = {
    sc2mafiaNewPost,
    getActiveBrowserUserIDs,
    getCondorcetVotes,
    clearCache,
};
