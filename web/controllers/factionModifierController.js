const factionModifierValidator = require('../validators/factionModifierValidator');
const intValidator = require('../validators/intValidator');

const factionModifierService = require('../services/setup/factionModifierService');
const userService = require('../services/userService');

const httpHelpers = require('../utils/httpHelpers');


async function createModifier(request){
    const userPromise = userService.getByAuthToken({
        token: request.headers.auth,
        wrapperType: request.headers.authtype, // headers are lowercased
    });
    const dataPromise = httpHelpers.getJson(request);

    const factionID = intValidator.idRequest(request.params.factionID);
    const [user, args] = await Promise.all([userPromise, dataPromise]);
    factionModifierValidator.createFactionModifierRequest(args);
    return factionModifierService.create(user.id, factionID, args);
}

module.exports = {
    createModifier,
};
