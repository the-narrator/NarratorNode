const integrationTypes = require('../models/enums/integrationTypes');

const chatService = require('../services/chatService');
const userService = require('../services/userService');

const chatValidator = require('../validators/chatValidator');
const intValidator = require('../validators/intValidator');

const httpHelpers = require('../utils/httpHelpers');


function getLobbyMessages(request){
    const gameID = intValidator.idRequest(request.params.gameID);
    return chatService.getLobbyMessages(gameID);
}

async function sendLobbyMessage(request){
    const [user, messageRequest] = await Promise.all([
        userService.getByAuthToken({
            token: request.headers.auth,
            wrapperType: request.headers.authtype, // headers are lowercased
        }),
        httpHelpers.getJson(request),
    ]);
    const gameID = intValidator.idRequest(request.params.gameID, 404);
    chatValidator.sendLobbyMessage(messageRequest);
    return chatService.sendLobbyMessage(gameID, user.id, messageRequest,
        integrationTypes.BROWSER);
}

async function getUserChat(request){
    const user = await userService.getByAuthToken({
        token: request.headers.auth,
        wrapperType: request.headers.authtype, // headers are lowercased
    });
    const gameID = intValidator.idRequest(request.query.gameID);
    return chatService.getUserChat(gameID, user.id);
}

module.exports = {
    getLobbyMessages,
    sendLobbyMessage,
    getUserChat,
};
