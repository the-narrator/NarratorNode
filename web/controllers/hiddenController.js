const hiddenValidators = require('../validators/hiddenValidators');
const intValidator = require('../validators/intValidator');

const hiddenService = require('../services/setup/hiddenService');
const userService = require('../services/userService');

const httpHelpers = require('../utils/httpHelpers');


async function create(request){
    const [user, json] = await Promise.all([
        userService.getByAuthToken({
            token: request.headers.auth,
            wrapperType: request.headers.authtype, // headers are lowercased
        }),
        httpHelpers.getJson(request),
    ]);
    hiddenValidators.createHiddenRequest(json);
    return hiddenService.create(user.id, json);
}

async function deleteHidden(request){
    const user = await userService.getByAuthToken({
        token: request.headers.auth,
        wrapperType: request.headers.authtype, // headers are lowercased
    });
    const hiddenID = intValidator.idRequest(request.params.hiddenID);
    await hiddenService.deleteHidden(user.id, hiddenID);
}

module.exports = {
    create,
    deleteHidden,
};
