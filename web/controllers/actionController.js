const actionService = require('../services/actionService');
const userService = require('../services/userService');

const actionValidator = require('../validators/actionValidators');

const httpHelpers = require('../utils/httpHelpers');


async function submitActionMessage(request){
    const [user, data] = await Promise.all([
        userService.getByAuthToken({
            token: request.headers.auth,
            wrapperType: request.headers.authtype, // headers are lowercased
        }),
        httpHelpers.getJson(request),
    ]);
    return actionService.submitActionMessage(user.id, data);
}

async function deleteAction(request){
    const user = await userService.getByAuthToken({
        token: request.headers.auth,
        wrapperType: request.headers.authtype, // headers are lowercased
    });
    request.query.actionIndex = parseInt(request.query.actionIndex, 10);
    actionValidator.cancelActionRequest(request.query);
    await actionService.cancelAction(user.id, request.query.actionIndex, request.query.command);
}

module.exports = {
    submitActionMessage,
    deleteAction,
};
