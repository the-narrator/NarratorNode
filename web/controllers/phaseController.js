const phaseValidator = require('../validators/phaseValidtator');
const phaseService = require('../services/phaseService');
const userService = require('../services/userService');
const userPermissionService = require('../services/userPermissionService');

const httpHelpers = require('../utils/httpHelpers');


async function setExpirationViaUser(request){
    const [user, args] = await Promise.all([
        userService.getByAuthToken({
            token: request.headers.auth,
            wrapperType: request.headers.authtype, // headers are lowercased
        }),
        httpHelpers.getJson(request),
    ]);
    phaseValidator.updatePhaseRemainingTimeRequest(args);
    await userPermissionService.verifyAdmin(user.id);
    return phaseService.setExpiration(args.joinID, args.seconds);
}

module.exports = {
    setExpirationViaUser,
};
