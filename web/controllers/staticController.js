const express = require('express');
const path = require('path');

const { app } = require('../routes');
const helpers = require('../utils/helpers');


const publicLoc = path.join(__dirname, '../../public');

const staticRoute = express.static(publicLoc, {
    dotfiles: 'deny',
});

app.use('/', staticRoute);

app.get('/:gameJoinID', (request, response, next) => {
    const { gameJoinID } = request.params;
    if(!gameJoinID || gameJoinID.length !== 4 || !helpers.isAlpha(gameJoinID))
        return next();

    sendIndex(request, response);
});

app.get('/condorcet', sendIndex);

app.get('/everest', sendIndex);

function sendIndex(_, response){
    response.sendFile(path.join(publicLoc, 'index.html'));
}
