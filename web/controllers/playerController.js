const gameService = require('../services/gameService');
const gameUserService = require('../services/gameUserService');
const playerService = require('../services/playerService');
const userService = require('../services/userService');

const httpHelpers = require('../utils/httpHelpers');

const gameValidators = require('../validators/gameValidators');
const intValidator = require('../validators/intValidator');
const playerValidator = require('../validators/playerValidator');


async function addBots(request){
    const [user, data] = await Promise.all([
        userService.getByAuthToken({
            token: request.headers.auth,
            wrapperType: request.headers.authtype, // headers are lowercased
        }),
        httpHelpers.getJson(request),
    ]);
    playerValidator.addBotsRequest(data);
    return playerService.addBots(user.id, data.botCount);
}

async function deletePlayer(request){
    const playerID = intValidator.idRequest(request.query.playerID);

    const gameID = intValidator.idRequest(request.query.gameID);

    const user = await userService.getByAuthToken({
        token: request.headers.auth,
        wrapperType: request.headers.authtype, // headers are lowercased
    });
    await gameUserService.assertIsModerator(gameID, user.id,
        'Only moderators can remove bots.');
    return playerService.deletePlayer(gameID, playerID);
}

async function create(request){
    const [user, json] = await Promise.all([userService.getByAuthToken({
        token: request.headers.auth,
        wrapperType: request.headers.authtype, // headers are lowercased
    }), httpHelpers.getJson(request)]);
    gameValidators.joinIDRequest(json.joinID);
    const game = await playerService.create(user.id, json.playerName, json.joinID);
    return gameService.filterNeededUserInfo(game, user.id);
}

async function updatePlayerName(request){
    const [user, json] = await Promise.all([userService.getByAuthToken({
        token: request.headers.auth,
        wrapperType: request.headers.authtype, // headers are lowercased
    }), httpHelpers.getJson(request)]);
    playerValidator.updatePlayerRequest(json);
    await playerService.updatePlayerName(user.id, json.name);
}

async function kick(request){
    const user = await userService.getByAuthToken({
        token: request.headers.auth,
        wrapperType: request.headers.authtype, // headers are lowercased
    });
    const userID = parseInt(request.query.userID, 10);
    playerValidator.kickUserRequest(userID);
    return playerService.kick(user.id, userID);
}

async function leave(request){
    const user = await userService.getByAuthToken({
        token: request.headers.auth,
        wrapperType: request.headers.authtype, // headers are lowercased
    });
    await playerService.leave(user.id);
}

module.exports = {
    addBots,
    deletePlayer,
    create,
    updatePlayerName,
    kick,
    leave,
};
