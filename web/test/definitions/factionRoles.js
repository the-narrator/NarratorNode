const MAFIA = 'Mafia';
const TOWN = 'Town';
const BENIGNS = 'Benigns';

const ASSASSIN = {
    name: 'Assassin',
    factionName: MAFIA,
};

const CITIZEN = {
    name: 'Citizen',
    factionName: TOWN,
};

const EXECUTIONER = {
    name: 'Executioner',
    factionName: BENIGNS,
};

const GOON = {
    name: 'Goon',
    factionName: MAFIA,
};

const PARITY_COP = {
    name: 'Parity Cop',
    factionName: TOWN,
};

module.exports = {
    ASSASSIN,
    EXECUTIONER,
    CITIZEN,
    GOON,
    PARITY_COP,
};
