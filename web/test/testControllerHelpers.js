const { expect } = require('chai');


function assertUneditableSetupErrors(responseObj){
    expect(responseObj.statusCode).to.be.equal(422);
    expect(responseObj.json.errors[0]).to.be.equal('This setup is frozen and cannot be edited.');
}

module.exports = {
    assertUneditableSetupErrors,
};
