function apiInit(){
    return Promise.resolve();
}

module.exports = {
    apiInit,
    login: apiInit,
    getPrivateMessages: apiInit,
    getPrivateMessage: apiInit,
    newThread: apiInit,
    newPost: apiInit,
    sendPrivateMessage: apiInit,
    getThread: apiInit,
    editPost: apiInit,
};
