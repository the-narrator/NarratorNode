const { ManagerReference } = require('./managerReference');


function QuasiDiscordGuildReference(client, guild){
    this.id = guild.id;
    this.channels = new ManagerReference((name, options) => createChannel(this, name, options));
    this.client = client;
    this.roles = guild.roles;
    this.guild = guild;
    this.members = guild.members;
}

function createChannel(ref, channelName, options){
    const channel = ref.guild._createChannel(ref, channelName, options);
    const channelRef = channel._join(ref.client, channelName, options);
    ref.channels.put(channelRef.id, channelRef);
    return Promise.resolve(channelRef);
}

module.exports = {
    QuasiDiscordGuildReference,
};
