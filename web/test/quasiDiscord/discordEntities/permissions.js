function Permissions(collection){
    this.collection = collection;
}

Permissions.prototype.has = function(permission){
    const value = this.collection.get(permission);
    return value === undefined ? true : value;
};

module.exports = {
    Permissions,
};
