const { Collections: Collection } = require('./collections');

function Manager(){
    this.cache = new Collection();
}

Manager.prototype.fetch = async function(key){
    return this.cache.get(key);
};

Manager.prototype._put = function(key, value){
    this.cache.put(key, value);
};

module.exports = {
    Manager,
};
