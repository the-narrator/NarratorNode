const helpers = require('../../../utils/helpers');

const { Manager } = require('./manager');


function QuasiDiscordGuild(guildID){
    if(!guildID)
        guildID = `guild_${helpers.getRandomString(5)}`;
    this.channels = new Manager();
    this.roles = { everyone: { id: `role_${helpers.getRandomString(5)}` } };
    this.id = guildID;
    this.members = new Manager();
}

QuasiDiscordGuild.prototype._createChannel = function(_reference, name, options){
    const quasiDiscordChannel = require('./quasiDiscordChannel');
    const { QuasiDiscordChannel } = quasiDiscordChannel;

    const channelID = `channel_${helpers.getRandomString(5)}`;
    const channel = new QuasiDiscordChannel(channelID, this, name, options);
    this.channels.cache.put(channelID, channel);
    this.members.cache.array().forEach(member => {
        const channelRef = channel._join(member, name, options);
        member.guilds.cache.get(this.id).channels.put(channelID, channelRef);
    });
    return channel;
};

QuasiDiscordGuild.prototype._join = function(client){
    const quasiDiscordGuildReference = require('./quasiDiscordGuildReference');
    const { QuasiDiscordGuildReference } = quasiDiscordGuildReference;

    const guildReference = new QuasiDiscordGuildReference(client, this);
    client.guilds._put(this.id, guildReference);
    this.members.cache.put(client.id, client);
    this.channels.cache.array().forEach(channel => channel._join(client, channel.name, {
        topic: channel.topic,
    }));

    const messageUpdateListeners = this.members.cache.array()
        .map(x => x.listeners.guildMemberAdd || []).flat();
    const promises = messageUpdateListeners
        .map(clientListenerFunction => clientListenerFunction({
            guild: guildReference,
            id: client.id,
        }));
    return Promise.all(promises);
};

module.exports = {
    QuasiDiscordGuild,
};
