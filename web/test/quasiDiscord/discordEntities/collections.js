function Collection(){
    this.dict = {};
}

Collection.prototype.array = function(){
    return Object.values(this.dict);
};

Collection.prototype.deleteAll = function(){
    this.dict = {};
};

Collection.prototype.filter = function(f){
    const collection = new Collection();
    Object.entries(this.dict)
        .forEach(entry => {
            if(f(entry[1]))
                collection.put(entry[0], entry[1]);
        });
    return collection;
};

Collection.prototype.find = function(findFunc){
    return Object.values(this.dict).find(findFunc);
};

Collection.prototype.get = function(key){
    return this.dict[key];
};

Collection.prototype.keyArray = function(){
    return Object.keys(this.dict);
};

Collection.prototype.map = function(f){
    return Object.values(this.dict).map(f);
};

Collection.prototype.put = function(key, value){
    this.dict[key] = value;
};

module.exports = {
    Collections: Collection,
};
