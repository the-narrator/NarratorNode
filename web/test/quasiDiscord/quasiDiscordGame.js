const gameService = require('../../services/gameService');

const quasiSetup = require('../quasiModels/quasiSetup');
const quasiUser = require('../quasiModels/quasiUser');


async function get(args){
    const host = quasiUser.createDiscordUser();
    const ownerID = await host.getUserID();
    if(args.setup)
        await quasiSetup.create({ ...args.setup, ownerID });
    const game = await host.host();
    const playerCount = 'playerCount' in args ? args.playerCount - 1 : 0;
    const players = quasiUser.createDiscordUsers(playerCount);
    await Promise.all(players.map(player => player.join()));
    await Promise.all(
        (args.modifiers || [])
            .map(modifier => gameService.updateModifier(ownerID, modifier)),
    );
    if(args.setup)
        await host.setSetup('custom');
    if(args.isStarted)
        await host.start();
    return { host, players: [host, ...players], game };
}

module.exports = {
    get,
};
