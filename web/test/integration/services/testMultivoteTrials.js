require('../../init');
const { expect } = require('chai');
const sinon = require('sinon');

const gamePhase = require('../../../models/enums/gamePhase');
const setupModifiers = require('../../../models/enums/setupModifiers');
const voteSystemTypes = require('../../../models/enums/voteSystemTypes');

const actionService = require('../../../services/actionService');
const gameService = require('../../../services/gameService');
const phaseService = require('../../../services/phaseService');
const voteService = require('../../../services/voteService');

const quasiGame = require('../../quasiModels/quasiGame');
const quasiSetup = require('../../quasiModels/quasiSetup');
const quasiUser = require('../../quasiModels/quasiUser');


const SECONDS = 60;
const DAY_LEGNTH = 10;
const TRIAL_LENGTH = 1;
const PLAYER_COUNT = 11;

describe('Multivote trials', () => {
    let clock;
    let setup;
    let game;
    let players;
    beforeEach(async() => {
        clock = sinon.useFakeTimers();
        const mod = await quasiUser.createMod();
        setup = await quasiSetup.create({
            modifiers: [{
                name: setupModifiers.DAY_START,
                value: true,
            }],
            ownerID: mod.id,
            setupHiddenCount: PLAYER_COUNT,
        });
        game = await quasiGame.create({
            hostID: setup.ownerID,
            isStarted: true,
            modifiers: [{
                name: 'VOTE_SYSTEM',
                value: voteSystemTypes.MULTIVOTE_PLURALITY,
            }, {
                name: 'DAY_LENGTH_START',
                value: DAY_LEGNTH * SECONDS,
            }, {
                name: 'TRIAL_LENGTH',
                value: TRIAL_LENGTH * SECONDS,
            }],
            playerCount: PLAYER_COUNT,
            setupID: setup.id,
        });
        players = game.players;
        advanceSecond();
    });

    afterEach(() => {
        clock.restore();
    });

    it('Should immediately put one to trial if all others are voting', async() => {
        // given in before each

        await vote(players[0], players.slice(1, PLAYER_COUNT));

        await assertOnTrialPhase();
    });

    it('Should move someone into trial if enough people are voting this person', async() => {
        await vote(players[0], players.slice(1, PLAYER_COUNT - 1));

        advanceGame(1 / (PLAYER_COUNT - 1));

        await assertOnTrialPhase();
    });

    it('Should put someone into trial after a vote', async() => {
        advanceGame((PLAYER_COUNT - 1) / PLAYER_COUNT);

        await vote(players[0], players[1]);

        await assertOnTrialPhase();
    });

    it('Should register change votes', async() => {
        await vote([players[1], players[2]], players[0]);
        await actionService.submitActionMessage(
            players[0].userID, {
                command: 'vote',
                targets: [players[1].name],
                oldAction: 0,
            },
        );

        const votes = await voteService.getVotes(game.id);

        expect(votes.voterToVotes[players[0].name].length).to.be.equal(1);
    });

    it('Should go into vote phase if tie exists', async() => {
        await vote(players.slice(0, 2), players.slice(2, PLAYER_COUNT - 1));

        advanceGame(1);

        await assertOnVotePhase();

        await vote(players[0], players[1]);

        await assertOnTrialPhase();

        await vote(players[1], players[0]);
        endTrial();

        await assertOnVotePhase();
    });

    it('Should continue days with less time', async() => {
        const percentage = 1 / (PLAYER_COUNT - 1);
        const voters = players.slice(2, PLAYER_COUNT);
        advanceGame(percentage);
        await vote(players[0], voters);

        await assertOnTrialPhase();

        await unvote(voters);
        await assertOnTrialPhase();

        endTrial();

        await assertOnVotePhase();
        const endTime = phaseService.getEndTime(game.joinID);
        expect(endTime - new Date().getTime() + 1000)
            .to.be.equal(DAY_LEGNTH * (1 - percentage) * 60 * 1000);
    });

    function advanceSecond(){
        clock.tick(1000);
    }

    function advanceGame(percentage){
        clock.tick(DAY_LEGNTH * percentage * 60 * 1000);
    }

    function endTrial(){
        clock.tick(TRIAL_LENGTH * 60 * 1000);
    }

    function unvote(voters){
        return Promise.all(voters
            .map(voter => actionService.cancelAction(voter.userID, 0, 'vote')));
    }

    function vote(voted, voters){
        if(!Array.isArray((voted)))
            voted = [voted];
        if(!Array.isArray(voters))
            voters = [voters];
        return Promise.all(voters.map(voter => actionService.submitActionMessage(
            voter.userID, {
                command: 'vote',
                targets: voted.map(v => v.name),
            },
        )));
    }

    async function assertOnTrialPhase(){
        const newGame = await gameService.getByID(game.id);
        expect(newGame.phase.name).to.be.equal(gamePhase.TRIALS);
    }

    async function assertOnVotePhase(){
        const newGame = await gameService.getByID(game.id);
        expect(newGame.phase.name).to.be.equal(gamePhase.VOTES_OPEN);
    }

    // TODO handle self voting calculations and the minus one, plus one logic it might add
});
