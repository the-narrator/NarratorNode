require('../../init');

const { expect } = require('chai');
const sinon = require('sinon');

const sc2mafiaClient = require('../../../channels/sc2mafia/sc2mafiaClient');
const vBulletinClient = require('../../../channels/sc2mafia/vBulletinClient');

const sc2mafiaGameRepo = require('../../../channels/sc2mafia/repos/sc2mafiaGameRepo');

const quasiGame = require('../../quasiModels/quasiGame');
const fakeConstants = require('../../fakeConstants');


describe('Sc2mafia Player left', async() => {
    let getThreadStub;
    let editPostStub;
    afterEach(() => {
        getThreadStub.restore();
        editPostStub.restore();
    });

    it('Should update the setup when onPlayerRemove is hit', async() => {
        getThreadStub = sinon.stub(vBulletinClient, 'getThread');
        getThreadStub.onCall(0).returns({
            FIRSTPOSTID: fakeConstants.SC2MAFIA_POST_ID,
        });
        editPostStub = sinon.stub(vBulletinClient, 'editPost');
        const game = await quasiGame.create();
        await sc2mafiaGameRepo.insert(game.joinID,
            fakeConstants.SC2MAFIA_SETUP_THREAD_ID,
            fakeConstants.SC2MAFIA_SIGNUP_THREAD_ID);

        await sc2mafiaClient.onPlayerRemove(game);

        expect(getThreadStub.calledWithExactly({
            threadid: fakeConstants.SC2MAFIA_SIGNUP_THREAD_ID,
        })).to.equal(true);
        expect(editPostStub.getCall(0).args[0].postid).to.be.equal(fakeConstants.SC2MAFIA_POST_ID);
    });
});
