/* eslint-disable max-len */
require('../../init');

const { expect } = require('chai');
const sinon = require('sinon');

const sc2mafiaClient = require('../../../channels/sc2mafia/sc2mafiaClient');
const vBulletinClient = require('../../../channels/sc2mafia/vBulletinClient');

const sc2mafiaGameRepo = require('../../../channels/sc2mafia/repos/sc2mafiaGameRepo');

const quasiGame = require('../../quasiModels/quasiGame');
const {
    SC2MAFIA_POST_ID,
    SC2MAFIA_SETUP_THREAD_ID,
    SC2MAFIA_SIGNUP_THREAD_ID,
} = require('../../fakeConstants');


describe('Sc2mafia Setup Changed', async() => {
    let getThreadStub;
    let editPostStub;
    afterEach(() => {
        getThreadStub.restore();
        editPostStub.restore();
    });

    it('Should update the setup when onLobbyClosed is hit', async() => {
        getThreadStub = sinon.stub(vBulletinClient, 'getThread');
        getThreadStub.onCall(0).returns({
            FIRSTPOSTID: SC2MAFIA_POST_ID,
        });
        editPostStub = sinon.stub(vBulletinClient, 'editPost');
        const game = await quasiGame.create();
        await sc2mafiaGameRepo.insert(game.joinID,
            SC2MAFIA_SETUP_THREAD_ID,
            SC2MAFIA_SIGNUP_THREAD_ID);
        const expectedMessage = getExpectedMessage();

        await sc2mafiaClient.onSetupChange(game);
        const message = editPostStub.getCall(0).args[0].message;

        expect(getThreadStub.calledWithExactly({
            threadid: SC2MAFIA_SETUP_THREAD_ID,
        })).to.equal(true);
        expect(editPostStub.getCall(0).args[0].postid).to.be.equal(SC2MAFIA_POST_ID);
        expect(message).to.be.equal(expectedMessage);
    });
});

function getExpectedMessage(){
    return `[COLOR=#FFD700][SIZE=5][B][U][CENTER]userName's Test setup name Setup[/CENTER][/U][/B]
[/SIZE][/COLOR][CENTER][COLOR=#FF0000]Goon[/COLOR][/CENTER]
[COLOR=white][COLOR=white][SIZE=4]General Settings[/SIZE][/COLOR]

[LIST][*]Game will start with a Night 0.

[*]Day lengths will be 240 seconds.

[*]Night lengths will be 180 seconds  .
[/LIST][/COLOR]
[SPOILER=Benigns Faction]
[COLOR=white][COLOR=#DDA0DD][SIZE=5]Benigns[/SIZE]
[/COLOR]
[LIST][*]Their motto is 'I do not care who wins.'

[*]Must eliminate .
[/LIST]
[SIZE=4]Available Roles[/SIZE]

[U][COLOR=#DDA0DD]Amnesiac[/COLOR][/U]
[LIST][*]Permanently change  your role and team to one in the graveyard.

[*]Will keep remaining charges, if role is a limited use role.

[*]Amnesiac can win without changing role
[/LIST]
[U][COLOR=#DDA0DD]Executioner[/COLOR][/U]
[LIST][*]Sole purpose is to get a given target killed. Do it.

[*]Invulnerable after winning

[*]Turns into jester upon target night death

[*]Target can be of any faction

[*]Target will not change if original target dies.
[/LIST]
[U][COLOR=#DDA0DD]Ghost[/COLOR][/U]
[LIST][*]Die, and be able to possess people during the day.  Make the person who killed you lose.

[*]A master puppeteer, taking control of people's words and votes during the day.
[/LIST]
[U][COLOR=#DDA0DD]Jester[/COLOR][/U]
[LIST][*]Goal is to die via a day execution, through any means necessary.

[*]Upon day elimination, 1% will suicide

[*]May annoy people at night
[/LIST]
[U][COLOR=#DDA0DD]Survivor[/COLOR][/U]
[LIST][*]use a vest to survive the night

[*]Starts off with 4 vests
[/LIST][/COLOR][/SPOILER]
[SPOILER=Cult Faction]
[COLOR=white][COLOR=#880BFC][SIZE=5]Cult[/SIZE]
[/COLOR]
[LIST][*]A secretive organization, the cult looks to increase its ranks within the town

[*]Must eliminate [COLOR=#FF0000]Mafia[/COLOR], [COLOR=#CD853E]Threat[/COLOR], [COLOR=#6495ED]Town[/COLOR], [COLOR=#FFFF00]Yakuza[/COLOR].
[/LIST]
[SIZE=4]Available Roles[/SIZE]

[U][COLOR=#880BFC]Cult Leader[/COLOR][/U]
[LIST][*]Recruit others into your cult, converting them to your side.

[*]Recruited members keep their original role upon recruitment

[*]After recruiting a power role, must wait 0 nights before next conversion

[*]Refusing a recruitment invitation is not a choice

[*]Cannot promote minions

[*]Conversion is unaffected by bodyguard

[*]Can only use convert every other night.
[/LIST]
[U][COLOR=#880BFC]Cultist[/COLOR][/U]
[LIST][*]A member of the ever-growing cult.
[/LIST][/COLOR][/SPOILER]
[SPOILER=Mafia Faction]
[COLOR=white][COLOR=#FF0000][SIZE=5]Mafia[/SIZE]
[/COLOR]
[LIST][*]The informed minority, these roles must eliminate the Town, other Mafia factions, the cult and evil killing neutrals.

[*]Must eliminate [COLOR=#880BFC]Cult[/COLOR], [COLOR=#CD853E]Threat[/COLOR], [COLOR=#6495ED]Town[/COLOR], [COLOR=#FFFF00]Yakuza[/COLOR].
[/LIST]
[SIZE=4]Available Abilities[/SIZE]

[U][COLOR=#FF0000]FactionKill[/COLOR][/U]
[LIST][*]Ability to kill at night.

[*]You will not know which faction made this kill.
[/LIST]
[SIZE=4]Available Roles[/SIZE]

[U][COLOR=#FF0000]Agent[/COLOR][/U]
[LIST][*]Stalk someone to find out who their target visited and who visited them.
[/LIST]
[U][COLOR=#FF0000]Architect[/COLOR][/U]
[LIST][*]Construct rooms at night so others can converse securely.

[*]Chat participants may talk in other chat rooms

[*]Burn deaths do not spread in architect chats
[/LIST]
[U][COLOR=#FF0000]Assassin[/COLOR][/U]
[LIST][*]Day-kill someone once during the day!

[*]Day killer name will be announced.
[/LIST]
[U][COLOR=#FF0000]Blackmailer[/COLOR][/U]
[LIST][*]Stop people from voting and talking.
[/LIST]
[U][COLOR=#FF0000]Bus Driver[/COLOR][/U]
[LIST][*]Pick up any two people to drive around.  Any action that affects one will instead affect the other.
[/LIST]
[U][COLOR=#FF0000]Coward[/COLOR][/U]
[LIST][*]Hide behind someone, redirecting all actions to the target

[*]Limited to 3 uses of hide.
[/LIST]
[U][COLOR=#FF0000]Disguiser[/COLOR][/U]
[LIST][*]Kill someone and take their identity.

[*]Limited to 1 use of disguise.
[/LIST]
[U][COLOR=#FF0000]Drug Dealer[/COLOR][/U]
[LIST][*]At night, provide false feedback, giving people false clues.
[/LIST]
[U][COLOR=#FF0000]Framer[/COLOR][/U]
[LIST][*]Frame other people for murder, changing most investigation results on them.

[*]May choose to frame as any alignment
[/LIST]
[U][COLOR=#FF0000]Godfather[/COLOR][/U]
[LIST][*]The leader of the mafia!  Can override who is sent to kill

[*]Invulnerable at night

[*]Immune to detection.
[/LIST]
[U][COLOR=#FF0000]Goon[/COLOR][/U]
[LIST][*]You have more weight in deciding who controls factional abilities (typically the mafia kill).
[/LIST]
[U][COLOR=#FF0000]Investigator[/COLOR][/U]
[LIST][*]Investigate someone to determine their role
[/LIST]
[U][COLOR=#FF0000]Jailor[/COLOR][/U]
[LIST][*]Execute captives in jail cells.

[*]Limited to 3 uses of execute.
[/LIST]
[U][COLOR=#FF0000]Janitor[/COLOR][/U]
[LIST][*]Hide the role of a person from being revealed to everyone.

[*]Doesn't receive target's role
[/LIST]
[U][COLOR=#FF0000]Silencer[/COLOR][/U]
[LIST][*]Stop players from talking.
[/LIST]
[U][COLOR=#FF0000]Stripper[/COLOR][/U]
[LIST][*]Entertain at night. Targets will not be able to complete any pending night actions.

[*]Targets will know they were slept with
[/LIST]
[U][COLOR=#FF0000]Tailor[/COLOR][/U]
[LIST][*]Give suits to people.  Upon death, they'll look like a different role in the graveyard.

[*]Suit receivers are informed of suit

[*]Does not pierce tailoring

[*]Limited to 3 uses of suit.
[/LIST]
[U][COLOR=#FF0000]Vote Remover[/COLOR][/U]
[LIST][*]Stop players from voting.
[/LIST][/COLOR][/SPOILER]
[SPOILER=Outcast Faction]
[COLOR=white][COLOR=#D5E68C][SIZE=5]Outcast[/SIZE]
[/COLOR]
[LIST][*]Shunned by the general populace, they plot their revenge in hiding and look to ally themselves with anyone that shares their goal.

[*]Must eliminate [COLOR=#6495ED]Town[/COLOR].
[/LIST]
[SIZE=4]Available Roles[/SIZE]

[U][COLOR=#D5E68C]Blacksmith[/COLOR][/U]
[LIST][*]Hand out gun or vest so others can use them.

[*]#gun_alias# passed out can be faulty

[*]#vest_alias#s passed out can be fake

[*]#gun_alias#s are used during the night
[/LIST]
[U][COLOR=#D5E68C]Elector[/COLOR][/U]
[LIST][*]Steal vote power from one person, giving it to another
[/LIST]
[U][COLOR=#D5E68C]Grave Digger[/COLOR][/U]
[LIST][*]Reanimate a dead person to target living people with their actions
[/LIST]
[U][COLOR=#D5E68C]Interceptor[/COLOR][/U]
[LIST][*]Kill the first person who visits your target.

[*]Will only kill the first person to visit the target.

[*]Won't kill non-movers like citizens.

[*]Identity is revealed to all visitors of Interceptor target
[/LIST]
[U][COLOR=#D5E68C]Operator[/COLOR][/U]
[LIST][*]Switch the intended targets of two people.
[/LIST]
[U][COLOR=#D5E68C]Ventriloquist[/COLOR][/U]
[LIST][*]A master puppeteer, taking control of people's words and votes during the day.
[/LIST]
[U][COLOR=#D5E68C]Witch[/COLOR][/U]
[LIST][*]Manipulate someone else's action target to do your bidding.

[*]Controlled targets dont know they were targeted
[/LIST][/COLOR][/SPOILER]
[SPOILER=Threat Faction]
[COLOR=white][COLOR=#CD853E][SIZE=5]Threat[/SIZE]
[/COLOR]
[LIST][*]Shunned by all, this twisted member wants revenge by killing all civilized people

[*]Must eliminate [COLOR=#880BFC]Cult[/COLOR], [COLOR=#FF0000]Mafia[/COLOR], [COLOR=#CD853E]Threat[/COLOR], [COLOR=#6495ED]Town[/COLOR], [COLOR=#FFFF00]Yakuza[/COLOR].
[/LIST]
[SIZE=4]Available Roles[/SIZE]

[U][COLOR=#CD853E]Arsonist[/COLOR][/U]
[LIST][*]Douse someone in flammable gasoline.

[*]Burn everyone that is doused in gasoline.

[*]Remove dangerous flammable liquid from your target.

[*]Invulnerable at night

[*]Doused targets are aware they were doused

[*]Douse is unaffected by bodyguard

[*]Limited to 1 day ignite(s)

[*]Burn deaths do not spread in architect chats
[/LIST]
[U][COLOR=#CD853E]Electromaniac[/COLOR][/U]
[LIST][*]Polarize people, so that they will die when visiting each other.

[*]Invulnerable at night
[/LIST]
[U][COLOR=#CD853E]Joker[/COLOR][/U]
[LIST][*]Place a bounty every night. Your target will be publicly revealed. If your bounty is still alive after a delay, you may kill.

[*]Invulnerable at night

[*]People with a bounty on them can go 2 day(s) until consequences hit

[*]Gets 2 retaliatory kill(s) if bounty fails
[/LIST]
[U][COLOR=#CD853E]Mass Murderer[/COLOR][/U]
[LIST][*]Kill everyone who performs an action at a chosen person's house.

[*]Invulnerable at night

[*]After killing more than one person, must wait 0 day(s) to use ability again
[/LIST]
[U][COLOR=#CD853E]Poisoner[/COLOR][/U]
[LIST][*]Poison someone at night. Poisoned targets die at the end of the next day.

[*]Invulnerable at night

[*]Healing can't stop poisoning

[*]Poison is unaffected by bodyguard
[/LIST]
[U][COLOR=#CD853E]Serial Killer[/COLOR][/U]
[LIST][*]A crazed psychopath trying to kill in the town.

[*]Invulnerable at night
[/LIST][/COLOR][/SPOILER]
[SPOILER=Town Faction]
[COLOR=white][COLOR=#6495ED][SIZE=5]Town[/SIZE]
[/COLOR]
[LIST][*]The uninformed majority, these roles must eliminate all Mafia factions, the cult, and all evil neutrals.

[*]Must eliminate [COLOR=#880BFC]Cult[/COLOR], [COLOR=#FF0000]Mafia[/COLOR], [COLOR=#D5E68C]Outcast[/COLOR], [COLOR=#CD853E]Threat[/COLOR], [COLOR=#FFFF00]Yakuza[/COLOR].
[/LIST]
[SIZE=4]Available Roles[/SIZE]

[U][COLOR=#6495ED]Architect[/COLOR][/U]
[LIST][*]Construct rooms at night so others can converse securely.

[*]Chat participants may talk in other chat rooms

[*]Burn deaths do not spread in architect chats
[/LIST]
[U][COLOR=#6495ED]Armorsmith[/COLOR][/U]
[LIST][*]Give vest during the night, protecting them from attack on subsequent nights.

[*]#vest_alias#s passed out cannot be fake
[/LIST]
[U][COLOR=#6495ED]Baker[/COLOR][/U]
[LIST][*]Give bread to people at night so they can perform additional actions other nights.

[*]#bread_alias# can be passed.

[*]#bread_alias# cannot immediately be used by maker
[/LIST]
[U][COLOR=#6495ED]Blacksmith[/COLOR][/U]
[LIST][*]Hand out gun or vest so others can use them.

[*]#gun_alias#s passed out are always safe to use

[*]#vest_alias#s passed out cannot be fake

[*]#gun_alias#s are used during the night
[/LIST]
[U][COLOR=#6495ED]Bodyguard[/COLOR][/U]
[LIST][*]Guard someone from death, dying and killing the attacker.

[*]Conversion is unaffected by bodyguard

[*]Poison is unaffected by bodyguard

[*]Douse is unaffected by bodyguard

[*]Target will know that they were protected, if they are attacked
[/LIST]
[U][COLOR=#6495ED]Bomb[/COLOR][/U]
[LIST][*]You will kill anyone who attacks you.

[*]Doesn't pierce death immunity
[/LIST]
[U][COLOR=#6495ED]Bulletproof[/COLOR][/U]
[LIST][*]Invulnerable at night
[/LIST]
[U][COLOR=#6495ED]Bus Driver[/COLOR][/U]
[LIST][*]Pick up any two people to drive around.  Any action that affects one will instead affect the other.
[/LIST]
[U][COLOR=#6495ED]Citizen[/COLOR][/U]
[LIST][/LIST]
[U][COLOR=#6495ED]Clubber[/COLOR][/U]
[LIST][*]An acolyte of the masonry, kill all cultic heretics.
[/LIST]
[U][COLOR=#6495ED]Commuter[/COLOR][/U]
[LIST][*]Leave town at night, making you untargetable.

[*]Limited to 3 uses of commute.
[/LIST]
[U][COLOR=#6495ED]Coroner[/COLOR][/U]
[LIST][*]Target a dead player to learn their true identity, and who they visited each night.

[*]Coroner will stop others from robbing bodies

[*]Coroners won't learn what roles visited the corpse while living.
[/LIST]
[U][COLOR=#6495ED]Detective[/COLOR][/U]
[LIST][*]Find out who someone visits at night.

[*]Can only see one target

[*]Can't follow detection immune targets
[/LIST]
[U][COLOR=#6495ED]Doctor[/COLOR][/U]
[LIST][*]Save someone from an attack.

[*]Will know if target was successfully saved

[*]Target knows if they were saved, but not by who
[/LIST]
[U][COLOR=#6495ED]Enforcer[/COLOR][/U]
[LIST][*]An acolyte of the masonry, prevent someone from being recruited.

[*]Will learn attempted recruiter's name
[/LIST]
[U][COLOR=#6495ED]Gunsmith[/COLOR][/U]
[LIST][*]Hand out gun so others can shoot them.

[*]#gun_alias#s passed out are always safe to use

[*]#gun_alias#s are used during the night
[/LIST]
[U][COLOR=#6495ED]Jailor[/COLOR][/U]
[LIST][*]Execute captives in jail cells.

[*]Limited to 3 uses of execute.
[/LIST]
[U][COLOR=#6495ED]Lookout[/COLOR][/U]
[LIST][*]Find out all who visit someone.
[/LIST]
[U][COLOR=#6495ED]Marshall[/COLOR][/U]
[LIST][*]Call the town to order to enable multiple public executions.

[*]Adds 2 more public executions to the day

[*]Executed people are revealed immediately.

[*]Limited to 1 use of marshalllaw.
[/LIST]
[U][COLOR=#6495ED]Mason[/COLOR][/U]
[LIST][*]You are part of the masons and may communicate with others like you at night.

[*]A random mason will replace Mason Leader upon original Mason Leader death

[*]May recruit citizens without powers

[*]This will never spawn by itself.

[*]Can recruit powerless non-citizens
[/LIST]
[U][COLOR=#6495ED]Mason Leader[/COLOR][/U]
[LIST][*]Recruit citizens into your masons and talk to them at night.

[*]A random mason will replace Mason Leader upon original Mason Leader death

[*]May recruit citizens without powers

[*]Limited to 4 uses of recruit.

[*]This will never spawn by itself.

[*]Can recruit powerless non-citizens
[/LIST]
[U][COLOR=#6495ED]Mayor[/COLOR][/U]
[LIST][*]Reveal during the day to lead the town while also gaining extra votes.

[*]After revealing, vote power will increase by 2
[/LIST]
[U][COLOR=#6495ED]Miller[/COLOR][/U]
[LIST][*]Looks evil to investigative actions

[*]Will not look evil on death
[/LIST]
[U][COLOR=#6495ED]Operator[/COLOR][/U]
[LIST][*]Switch the intended targets of two people.
[/LIST]
[U][COLOR=#6495ED]Parity Cop[/COLOR][/U]
[LIST][*]Compare player allegiances each night for differences.
[/LIST]
[U][COLOR=#6495ED]Sheriff[/COLOR][/U]
[LIST][*]Investigate someone to determine if they are suspicious or not.

[*]Can pick out the difference between one team and another

[*]Has no pregame check

[*]Sheriffs can detect the following team(s): Cult, Mafia, Threat, Yakuza
[/LIST]
[U][COLOR=#6495ED]Sleepwalker[/COLOR][/U]
[LIST][*]Wander around at night visiting people.
[/LIST]
[U][COLOR=#6495ED]Snitch[/COLOR][/U]
[LIST][*]Upon death, the last target's alignment and role will be publicly revealed.

[*]Does not pierce tailoring

[*]Players that are revealed might be affected by a suit.

[*]Does not pierce detection immunity.

[*]Ability activates, no matter the cause of death.
[/LIST]
[U][COLOR=#6495ED]Spy[/COLOR][/U]
[LIST][*]Find out all targets of a team that you select.

[*]Cannot target allied factions

[*]Can target enemy factions
[/LIST]
[U][COLOR=#6495ED]Stripper[/COLOR][/U]
[LIST][*]Entertain at night. Targets will not be able to complete any pending night actions.

[*]Targets will know they were slept with
[/LIST]
[U][COLOR=#6495ED]Veteran[/COLOR][/U]
[LIST][*]Kill everyone who visits this person when on alert.

[*]Limited to 3 uses of alert.
[/LIST]
[U][COLOR=#6495ED]Vigilante[/COLOR][/U]
[LIST][*]use a gun to attack someone.

[*]Starts off with 2 guns
[/LIST][/COLOR][/SPOILER]
[SPOILER=Yakuza Faction]
[COLOR=white][COLOR=#FFFF00][SIZE=5]Yakuza[/SIZE]
[/COLOR]
[LIST][*]The informed minority, these roles must eliminate the Town, other Mafia factions, the cult and evil killing neutrals.

[*]Must eliminate [COLOR=#880BFC]Cult[/COLOR], [COLOR=#FF0000]Mafia[/COLOR], [COLOR=#CD853E]Threat[/COLOR], [COLOR=#6495ED]Town[/COLOR].
[/LIST]
[SIZE=4]Available Abilities[/SIZE]

[U][COLOR=#FFFF00]FactionKill[/COLOR][/U]
[LIST][*]Ability to kill at night.

[*]You will not know which faction made this kill.
[/LIST]
[SIZE=4]Available Roles[/SIZE]

[U][COLOR=#FFFF00]Agent[/COLOR][/U]
[LIST][*]Stalk someone to find out who their target visited and who visited them.
[/LIST]
[U][COLOR=#FFFF00]Architect[/COLOR][/U]
[LIST][*]Construct rooms at night so others can converse securely.

[*]Chat participants may talk in other chat rooms

[*]Burn deaths do not spread in architect chats
[/LIST]
[U][COLOR=#FFFF00]Assassin[/COLOR][/U]
[LIST][*]Day-kill someone once during the day!

[*]Day killer name will be announced.
[/LIST]
[U][COLOR=#FFFF00]Blackmailer[/COLOR][/U]
[LIST][*]Stop people from voting and talking.
[/LIST]
[U][COLOR=#FFFF00]Bus Driver[/COLOR][/U]
[LIST][*]Pick up any two people to drive around.  Any action that affects one will instead affect the other.
[/LIST]
[U][COLOR=#FFFF00]Coward[/COLOR][/U]
[LIST][*]Hide behind someone, redirecting all actions to the target

[*]Limited to 3 uses of hide.
[/LIST]
[U][COLOR=#FFFF00]Disguiser[/COLOR][/U]
[LIST][*]Kill someone and take their identity.

[*]Limited to 1 use of disguise.
[/LIST]
[U][COLOR=#FFFF00]Drug Dealer[/COLOR][/U]
[LIST][*]At night, provide false feedback, giving people false clues.
[/LIST]
[U][COLOR=#FFFF00]Framer[/COLOR][/U]
[LIST][*]Frame other people for murder, changing most investigation results on them.

[*]May choose to frame as any alignment
[/LIST]
[U][COLOR=#FFFF00]Godfather[/COLOR][/U]
[LIST][*]The leader of the mafia!  Can override who is sent to kill

[*]Invulnerable at night

[*]Immune to detection.
[/LIST]
[U][COLOR=#FFFF00]Goon[/COLOR][/U]
[LIST][*]You have more weight in deciding who controls factional abilities (typically the mafia kill).
[/LIST]
[U][COLOR=#FFFF00]Investigator[/COLOR][/U]
[LIST][*]Investigate someone to determine their role
[/LIST]
[U][COLOR=#FFFF00]Jailor[/COLOR][/U]
[LIST][*]Execute captives in jail cells.

[*]Limited to 3 uses of execute.
[/LIST]
[U][COLOR=#FFFF00]Janitor[/COLOR][/U]
[LIST][*]Hide the role of a person from being revealed to everyone.

[*]Doesn't receive target's role
[/LIST]
[U][COLOR=#FFFF00]Silencer[/COLOR][/U]
[LIST][*]Stop players from talking.
[/LIST]
[U][COLOR=#FFFF00]Stripper[/COLOR][/U]
[LIST][*]Entertain at night. Targets will not be able to complete any pending night actions.

[*]Targets will know they were slept with
[/LIST]
[U][COLOR=#FFFF00]Tailor[/COLOR][/U]
[LIST][*]Give suits to people.  Upon death, they'll look like a different role in the graveyard.

[*]Suit receivers are informed of suit

[*]Does not pierce tailoring

[*]Limited to 3 uses of suit.
[/LIST]
[U][COLOR=#FFFF00]Vote Remover[/COLOR][/U]
[LIST][*]Stop players from voting.
[/LIST][/COLOR][/SPOILER][SPOILER=Order of Operations]
[COLOR=white]
[SIZE=4]Order of Operations[/SIZE]

[LIST][*]Commuting

[*]Grave Diggers, in reverse order

[*]Sending other people to do actions for you

[*]Witch manipulation

[*]Commuting again, if missed the first round

[*]Vistations for sending other people to do actions for you

[*]Oracle/Snitch action

[*]Role Blocking

[*]Jailor Executions

[*]Operator/Intended target switching

[*]Coroner

[*]Bus Driving

[*]Coward hiding

[*]Vesting

[*]Undousing

[*]Bodyguard moving

[*]Dousing

[*]All kill abilities at once: Veteran alerting, Serial Killer stabbing, Disguiser killing, Mass Murderer spree killing, Joker killing, Interceptor, Electromaniac charging, Cult clubbing, Burning, Gun Shooting, Mafia kill

[*]Bodyguard attacks

[*]Jester suicides

[*]Doctors

[*]Disguiser takes persona

[*]Commuting if applicable

[*]Cleaning

[*]Silencing, Disfranchising, Blackmailing, Puppeteering, and Ghost controlling

[*]Framing

[*]Enforcing

[*]Mason Recruiting

[*]Cult visiting

[*]Cult recruiting

[*]Drug Dealing

[*]Items passed out: guns, vests, breadd

[*]Amnesiac remembers

[*]Investigations: Sheriff, Investigator, Scout, Arms Detector

[*]Poisoning

[*]Suiting

[*]Jester annoys

[*]Normal visitations

[*]Role watching: spy, lookout, detective, agent
[/LIST][/COLOR]
[/SPOILER][COLOR=#FFD700][B][U]Rules of Conduct[/U][/B]
[/COLOR]1. [URL="http://www.sc2mafia.com/forum/showthread.php/42151-Forum-Mafia-Rules"]Forum Mafia Rules[/URL]
2. Inactive players will be replaced by another player, if necessary, they will be modkilled.
3. To ask the Host a question, you must highlight the question [COLOR="#00FF00"]in green[/COLOR], and mention the host [MENTION=6527]userName[/MENTION].  You may also pm me on Discord.
4. Minimum of 5 posts per game day.
5. English shall be the only language used.
6. Do not post links to other websites.
7. Pictures are allowed in moderation.
8. Videos are not allowed.
9. Directly quoting any feedback or PM by me is forbidden.
10. Have Fun.`;
}
