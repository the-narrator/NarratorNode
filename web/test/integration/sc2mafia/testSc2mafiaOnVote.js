require('../../init');
const { expect } = require('chai');
const sinon = require('sinon');

const setupModifiers = require('../../../models/enums/setupModifiers');

const actionService = require('../../../services/actionService');

const quasiGame = require('../../quasiModels/quasiGame');
const quasiSetup = require('../../quasiModels/quasiSetup');

const vBulletinClient = require('../../../channels/sc2mafia/vBulletinClient');

const sc2mafiaGameRepo = require('../../../channels/sc2mafia/repos/sc2mafiaGameRepo');
const {
    SC2MAFIA_GAME_THREAD_ID,
    SC2MAFIA_SETUP_THREAD_ID,
    SC2MAFIA_SIGNUP_THREAD_ID,
} = require('../../fakeConstants');


describe('Sc2mafia On Vote', async() => {
    let newPostStub;
    afterEach(() => {
        newPostStub.restore();
    });

    it('Should post a vote indicator after a vote', async() => {
        newPostStub = sinon.stub(vBulletinClient, 'newPost');
        const setup = await quasiSetup.create({
            modifiers: [{
                name: setupModifiers.DAY_START,
                value: true,
            }],
            setupHiddenCount: 7,
        });
        const game = await quasiGame.create({
            isStarted: true,
            setupID: setup.id,
            playerCount: 7,
        });
        await sc2mafiaGameRepo.insert(game.joinID,
            SC2MAFIA_SETUP_THREAD_ID,
            SC2MAFIA_SIGNUP_THREAD_ID);
        await sc2mafiaGameRepo.updateGameThreadID(game.joinID, SC2MAFIA_GAME_THREAD_ID);
        const [voter, voteTarget] = game.users;
        const expectedMessage = getExpectedMessage(voter.name, voteTarget.name);

        await actionService.submitActionMessage(voter.id, { message: `vote ${voteTarget.name}` });
        await new Promise(resolve => {
            setTimeout(resolve, 200);
        });
        const message = newPostStub.getCall(0).args[0].message;

        expect(message).to.be.equal(expectedMessage);
    });
});

function getExpectedMessage(voter, voteTarget){
    return `[B]${voter}[/B] voted for [B]${voteTarget}[/B].   (L - 3)`;
}
