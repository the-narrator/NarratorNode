require('../../init');

const { expect } = require('chai');
const sinon = require('sinon');

const { THREAD_THROTTLE } = require('../../../channels/sc2mafia/constants');

const sc2mafiaClient = require('../../../channels/sc2mafia/sc2mafiaClient');
const vBulletinClient = require('../../../channels/sc2mafia/vBulletinClient');

const postModel = require('../../quasiSc2mafia/postModel');
const threadModel = require('../../quasiSc2mafia/threadModel');

const {
    EXTERNAL_USER_ID, EXTERNAL_USER_ID2, EXTERNAL_USER_ID3,
    SC2MAFIA_GAME_THREAD_ID,
} = require('../../fakeConstants');


const baseVoteOptions = ['A', 'B', 'C'];
const baseVoteOutput = baseVoteOptions.map(option => [option]);

describe('Sc2mafia Get Condorcet Votes', () => {
    let getThreadStub;

    beforeEach(() => {
        getThreadStub = sinon.stub(vBulletinClient, 'getThread');
    });

    afterEach(() => {
        getThreadStub.restore();
    });

    it('Should get no votes if only the opening thread exists', async() => {
        applyStub(getThreadStub, threadModel.get({
            posts: [getHostOpeningPost()],
        }));

        const results = await sc2mafiaClient.getCondorcetVotes(SC2MAFIA_GAME_THREAD_ID);

        const args = getThreadStub.getCall(0).args[0];
        expect(args.threadid).to.be.equal(SC2MAFIA_GAME_THREAD_ID);
        expect(results.options).to.deep.equal(baseVoteOptions);
    });

    it('Should register a vote', async() => {
        applyStub(getThreadStub, threadModel.get({
            posts: [
                getHostOpeningPost(),
                postModel.get({
                    externalUserID: EXTERNAL_USER_ID2,
                    text: getVoteOptionsSpoiler(baseVoteOptions),
                })],
        }));

        const results = await sc2mafiaClient.getCondorcetVotes(SC2MAFIA_GAME_THREAD_ID);

        expect(results.voterMetadata[EXTERNAL_USER_ID2].ranking).to.deep.equal(baseVoteOutput);
    });

    it('Should look at results on the second page', async() => {
        const posts = [
            getHostOpeningPost(),
            ...getFillerPosts(threadModel.POSTS_PER_PAGE - 1),
            getVoteSubmissionPost(),
        ];
        applyStub(getThreadStub, threadModel.get({
            posts,
        }));

        const results = await sc2mafiaClient.getCondorcetVotes(SC2MAFIA_GAME_THREAD_ID);

        const secondCall = getThreadStub.getCall(1).args[0];
        expect(secondCall.page).to.be.equal(2);
        expect(results.voterMetadata[EXTERNAL_USER_ID2].ranking).to.deep.equal(baseVoteOutput);
    });

    it('Should look at results after the throttle', async() => {
        const fillerPostCount = (threadModel.POSTS_PER_PAGE * THREAD_THROTTLE) - 2;
        const posts = [
            getHostOpeningPost(),
            ...getFillerPosts(fillerPostCount),
            postModel.get({
                externalUserID: EXTERNAL_USER_ID3,
                text: getVoteOptionsSpoiler([]),
            }),
            getVoteSubmissionPost(),
        ];
        applyStub(getThreadStub, threadModel.get({
            posts,
        }));

        const results = await sc2mafiaClient.getCondorcetVotes(SC2MAFIA_GAME_THREAD_ID);

        expect(results.voterMetadata[EXTERNAL_USER_ID3].ranking).to.deep.equal([]);
        expect(results.voterMetadata[EXTERNAL_USER_ID2].ranking).to.deep.equal(baseVoteOutput);
    });

    it('Should go past the third throttle', async() => {
        const fillerPostCount = (threadModel.POSTS_PER_PAGE * THREAD_THROTTLE) * 3;
        const posts = [
            getHostOpeningPost(),
            ...getFillerPosts(fillerPostCount - 1),
            getVoteSubmissionPost(),
        ];
        applyStub(getThreadStub, threadModel.get({
            posts,
        }));

        const results = await sc2mafiaClient.getCondorcetVotes(SC2MAFIA_GAME_THREAD_ID);

        expect(results.voterMetadata[EXTERNAL_USER_ID2].ranking).to.deep.equal(baseVoteOutput);
    });

    it('Should cache results', async() => {
        const fillerPostCount = (threadModel.POSTS_PER_PAGE * THREAD_THROTTLE) - 1;
        const thread = threadModel.get({
            posts: [
                getHostOpeningPost(),
                ...getFillerPosts(fillerPostCount),
                getVoteSubmissionPost(),
            ],
        });
        applyStub(getThreadStub, thread);
        const pageCount = thread.getPageCount();
        getThreadStub.onCall(pageCount).returns(thread.getPage(pageCount));

        await sc2mafiaClient.getCondorcetVotes(SC2MAFIA_GAME_THREAD_ID);
        const results = await sc2mafiaClient.getCondorcetVotes(SC2MAFIA_GAME_THREAD_ID);

        expect(results.voterMetadata[EXTERNAL_USER_ID2].ranking).to.deep.equal(baseVoteOutput);
    });

    it('Should not names with spaces', async() => {
        const spacedName = 'Space Space';
        applyStub(getThreadStub, threadModel.get({
            posts: [
                getHostOpeningPost([spacedName]),
            ],
        }));

        const results = await sc2mafiaClient.getCondorcetVotes(SC2MAFIA_GAME_THREAD_ID);

        expect(results.options).to.deep.equal([...baseVoteOptions].concat([spacedName]));
    });

    it('Should find votes by comma', async() => {
        applyStub(getThreadStub, threadModel.get({
            posts: [
                getHostOpeningPost(),
                postModel.get({
                    externalUserID: EXTERNAL_USER_ID2,
                    text: '[spoiler=cvote]A,B,C,[/spoiler],', // note has extra comma
                }),
            ],
        }));

        const results = await sc2mafiaClient.getCondorcetVotes(SC2MAFIA_GAME_THREAD_ID);

        expect(results.voterMetadata[EXTERNAL_USER_ID2].ranking).to.deep.equal(baseVoteOutput);
    });

    it('Should update the host options and clear votes', async() => {
        applyStub(getThreadStub, threadModel.get({
            posts: [
                getHostOpeningPost(),
                getVoteSubmissionPost(),
                getHostOpeningPost(),
            ],
        }));

        const results = await sc2mafiaClient.getCondorcetVotes(SC2MAFIA_GAME_THREAD_ID);

        expect(results.voterMetadata[EXTERNAL_USER_ID2]).to.equal(undefined);
    });

    it('Should filter out duplicate votes', async() => {
        applyStub(getThreadStub, threadModel.get({
            posts: [
                getHostOpeningPost(),
                postModel.get({
                    externalUserID: EXTERNAL_USER_ID2,
                    text: getVoteOptionsSpoiler(['A', 'B', 'A', 'C']),
                }),
            ],
        }));

        const results = await sc2mafiaClient.getCondorcetVotes(SC2MAFIA_GAME_THREAD_ID);

        expect(results.voterMetadata[EXTERNAL_USER_ID2].ranking).to.deep.equal(baseVoteOutput);
    });

    it('Should take votes in posts with other tags', async() => {
        applyStub(getThreadStub, threadModel.get({
            posts: [
                getHostOpeningPost(),
                postModel.get({
                    externalUserID: EXTERNAL_USER_ID2,
                    text: `[][/]${getVoteOptionsSpoiler()}[][/]`,
                }),
            ],
        }));

        const results = await sc2mafiaClient.getCondorcetVotes(SC2MAFIA_GAME_THREAD_ID);

        expect(results.voterMetadata[EXTERNAL_USER_ID2].ranking).to.deep.equal(baseVoteOutput);
    });

    it('Should take votes in if text is before the tag', async() => {
        applyStub(getThreadStub, threadModel.get({
            posts: [
                getHostOpeningPost(),
                postModel.get({
                    externalUserID: EXTERNAL_USER_ID2,
                    text: 'So I don\'t forget and to prevent this from getting too swingy, '
                        + `here's my vote\n\n${getVoteOptionsSpoiler()}`,
                }),
            ],
        }));

        const results = await sc2mafiaClient.getCondorcetVotes(SC2MAFIA_GAME_THREAD_ID);

        expect(results.voterMetadata[EXTERNAL_USER_ID2].ranking).to.deep.equal(baseVoteOutput);
    });


    it('Should not take votes wrapped in anything', async() => {
        applyStub(getThreadStub, threadModel.get({
            posts: [
                getHostOpeningPost(),
                postModel.get({
                    externalUserID: EXTERNAL_USER_ID2,
                    text: `[][]${getVoteOptionsSpoiler()}[/][/]`,
                }),
            ],
        }));

        const results = await sc2mafiaClient.getCondorcetVotes(SC2MAFIA_GAME_THREAD_ID);

        expect(results.voterMetadata).to.deep.equal({});
    });

    it('Should not parse weird wrapped votes and not count them', async() => {
        applyStub(getThreadStub, threadModel.get({
            posts: [
                getHostOpeningPost(),
                postModel.get({
                    externalUserID: EXTERNAL_USER_ID2,
                    text: `[quote=abc;239]${getVoteOptionsSpoiler()}[/QuOtE]`,
                }),
            ],
        }));

        const results = await sc2mafiaClient.getCondorcetVotes(SC2MAFIA_GAME_THREAD_ID);

        expect(results.voterMetadata).to.deep.equal({});
    });
});

function applyStub(stub, thread){
    for(let i = 0; i < thread.getPageCount(); ++i)
        stub.onCall(i).returns(thread.getPage(i + 1)); // pages are 1 indexed, not 0
}

function getFillerPosts(postCount){
    const posts = [];
    for(let i = 0; i < postCount; i++)
        posts.push(postModel.get({
            externalUserID: EXTERNAL_USER_ID2,
        }));
    return posts;
}

function getHostOpeningPost(options = []){
    return postModel.get({
        externalUserID: EXTERNAL_USER_ID,
        text: getVoteOptionsSpoiler(baseVoteOptions.concat(...options)),
    });
}

function getVoteSubmissionPost(){
    return postModel.get({
        externalUserID: EXTERNAL_USER_ID2,
        text: getVoteOptionsSpoiler(),
    });
}

function getVoteOptionsSpoiler(voteOptions = baseVoteOptions, splitCharacter = '\n'){
    return `[spoiler=cvote]${voteOptions.join(splitCharacter)}[/spoiler]`;
}
