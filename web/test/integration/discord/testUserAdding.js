require('../../init');
const { expect } = require('chai');

const config = require('../../../../config');

const gamePhase = require('../../../models/enums/gamePhase');
const setupModifiers = require('../../../models/enums/setupModifiers');
const voteSystemTypes = require('../../../models/enums/voteSystemTypes');

const gameService = require('../../../services/gameService');

const quasiSetup = require('../../quasiModels/quasiSetup');
const quasiUser = require('../../quasiModels/quasiUser');

const quasiDiscordUtils = require('../../quasiDiscord/quasiDiscordUtils');

const Constants = require('../../../channels/discord/Constants');

const testChannel2 = config.discord.test_channel_id2;


describe('User adding', async() => {
    let host;
    beforeEach(async() => {
        host = quasiUser.createDiscordUser();
        const ownerID = await host.getUserID();
        await quasiSetup.create({
            modifiers: [{
                name: setupModifiers.DAY_START,
                value: true,
            }],
            ownerID,
            setupHiddenCount: 8,
        });
    });

    it('Should start on a basic host of a game', async() => {
        const host2 = await quasiUser.createDiscordUser();
        const instanceCount = await getLobbyCount();
        await host.host();
        let newInstanceCount = await getLobbyCount();
        expect(newInstanceCount).to.equal(instanceCount + 1);

        await host.say(Constants.host,
            `<@!${host.getDiscordID()}>, You are already hosting a game!`);
        await host2.say(Constants.host, `<@!${host2.getDiscordID()}>, `
            + `${host.getName()} is already hosting a game.`);

        newInstanceCount = await getLobbyCount();
        expect(newInstanceCount).to.equal(instanceCount + 1);

        await host.leave(null, true); // host leaving
        newInstanceCount = await getLobbyCount();
        expect(newInstanceCount).to.equal(instanceCount);
    });

    it('Should pull the nickname for joining and hosting', async() => {
        const user = quasiUser.createDiscordUser();

        await host.host();
        await user.join();
        const infoMessage = await user.say(Constants.info);
        const gameObject = await host.getGameObj();
        const hostNickname = host.discordClient.nickname;
        const hostUsername = host.discordClient.username;
        const playerNickname = user.discordClient.nickname;
        const playerUsername = user.discordClient.username;

        expect(infoMessage.content.embed.fields[1].value).to.include(hostNickname);
        expect(infoMessage.content.embed.fields[1].value).to.not.include(hostUsername);
        expect(infoMessage.content.embed.fields[1].value).to.include(playerNickname);
        expect(infoMessage.content.embed.fields[1].value).to.not.include(playerUsername);
        expect(gameObject.players.map(player => player.name)).to.include(hostNickname);
        expect(gameObject.players.map(player => player.name)).to.not.include(hostUsername);
        expect(gameObject.players.map(player => player.name)).to.include(playerNickname);
        expect(gameObject.players.map(player => player.name)).to.not.include(playerUsername);

        await user.leave();
        await host.leave(null, true); // host leaving
    });

    it('Should allow users to rehost', async() => {
        await host.host();
        await host.leave(null, true);
        await host.host();
        await host.leave(null, true);
    });

    it('Should allow users to join and leave games', async() => {
        const user2 = await quasiUser.createDiscordUser();

        await host.host();
        await user2.join();
        await user2.leave();
        await user2.join();
        await user2.leave();
        await host.leave(null, true);
    });

    it('Should not allow users to join two games', async() => {
        const user1 = await quasiUser.createDiscordUser();
        const user2 = await quasiUser.createDiscordUser();

        await user1.host();
        await user2.host(testChannel2);

        await user2.say(Constants.join, `<@!${user2.getDiscordID()}>, You're already in a game!`);

        await Promise.all([user1.leave(null, true), user2.leave(testChannel2, true)]);
    });

    it('Should allow new games to form after games complete', async() => {
        await host.host();
        const players = quasiUser.createDiscordUsers(3);
        await Promise.all(players.map(player => player.join()));
        await Promise.all([
            host.setSetup('custom'), // ffa is day start
            host.moderate(),
            host.setDiscussionLength(1),
            host.setVoteSystem(voteSystemTypes.DIMINISHING),
            host.setTrialLength(1),
        ]);

        await host.say(Constants.start, '**Day 1**');
        let gameObj = await host.getGameObj();
        expect(gameObj.phase.name).to.be.eql(gamePhase.VOTES_CLOSED);

        await host.say('end phase', 'Voting polls are now open');
        gameObj = await host.getGameObj();
        expect(gameObj.phase.name).to.be.eql(gamePhase.VOTES_OPEN);

        const [voteOut] = await quasiDiscordUtils.filterColor(players, '#6495ED');
        await Promise.all(players.filter(p => p !== voteOut).map(p => p.vote(voteOut)));

        await host.say('end phase', ' now give their defense');
        await host.say('end phase', 'Voting polls are now open');

        await Promise.all(players.filter(p => p !== voteOut).map(p => p.vote(voteOut)));

        await host.say('end phase', 'It is now nighttime.');
        await host.say('end phase', 'Game canceled');

        await host.host();
        await host.leave(null, true); // host leaving
    });
});

async function getLobbyCount(){
    return (await gameService.getAll()).length;
}
