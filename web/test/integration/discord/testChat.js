require('../../init');
const { expect } = require('chai');
const config = require('../../../../config.json');
const helpers = require('../../../utils/helpers');

const integrationTypes = require('../../../models/enums/integrationTypes');
const setupModifiers = require('../../../models/enums/setupModifiers');

const { MANAGE_WEBHOOKS } = require('../../../channels/discord/enums/permissions');

const lobbyChatRepo = require('../../../repos/lobbyChatRepo');

const channelService = require('../../../services/channelService');
const chatService = require('../../../services/chatService');

const quasiDiscordGame = require('../../quasiDiscord/quasiDiscordGame');
const quasiSetup = require('../../quasiModels/quasiSetup');
const quasiUser = require('../../quasiModels/quasiUser');

const { removeChannelPermission } = require('../../quasiDiscord/quasiDiscordUtils');

const { CITIZEN, GOON } = require('../../definitions/factionRoles');

const { pause } = require('../../testHelpers');


describe('Chat', () => {
    let text;

    beforeEach(() => {
        text = `myMessage ${helpers.getRandomString(10)}`;
    });

    describe('Lobby Chat', () => {
        let host;
        beforeEach(async() => {
            host = await quasiUser.createDiscordUser();
        });

        afterEach(async() => {
            await host.killGame();
        });

        describe('Message forwarding', () => {
            let hostUserID;
            beforeEach(async() => {
                hostUserID = await host.getUserID();
                await quasiSetup.create({
                    ownerID: hostUserID,
                    setupHiddenCount: 3,
                });
                await host.host();
                const players = quasiUser.createDiscordUsers(1);
                await Promise.all(players.map(player => player.join()));
            });

            // if you're finding too many messages found in channelMessages
            // introduce cleanup logic to remove all messages from channels
            it('Will forward lobby chat messages from discord', async() => {
                await host.chatter(text);
                const gameID = await getGameID(host);

                const messages = await lobbyChatRepo.get(gameID);
                const channelMessages = getChannelMessages(host);

                const discordMessageChatter = channelMessages.filter(m => m.content === text);
                expect(messages.length).to.be.equal(1);
                expect(messages[0].text).to.be.equal(text);
                expect(discordMessageChatter.length).to.be.equal(1);
            });

            it('Will forward messages from lobby to discord', async() => {
                const promiseMessage = host.waitForMessage(m => m.content.includes(text));
                const gameID = await getGameID(host);

                await chatService.sendLobbyMessage(gameID, hostUserID, { text });
                const message = await promiseMessage;

                expect(message.content).to.be.equal(text);
            });

            it('Will forward messages from one discord to another', async() => {
                const gameID = await getGameID(host);
                await channelService.createIntegration(gameID, integrationTypes.DISCORD, {
                    channelID: config.discord.test_channel_id2,
                });

                const m = await new Promise(resolve => {
                    host.waitForMessage(mes => {
                        resolve(mes);
                        return true;
                    });
                    host.chatter(text, config.discord.test_channel_id);
                });

                expect(m.author.bot).to.be.equal(true);
                expect(m.content).to.be.equal(text);
                expect(m.author.name).to.be.equal(host.getName());
            });

            it('Will not error if lacking webhook permissions', async() => {
                setNoWebhookPermissions();
                const gameID = await getGameID(host);

                await chatService.sendLobbyMessage(gameID, hostUserID, { text });
            });

            it('Will handle emojis', async() => {
                const emojiText = String.fromCodePoint(0x1F525);
                const gameID = await getGameID(host);

                await host.chatter(emojiText);
                await pause(100);

                const messages = await lobbyChatRepo.get(gameID);
                expect(messages[0].text).to.be.equal(emojiText);
            });
        });

        it('Will announce no chat forwarding if lacking permissions', async() => {
            setNoWebhookPermissions();
            const expectedText = 'No messages will be forwarded to this channel.';
            const promiseMessage = host.waitForMessage(m => m.content.includes(expectedText));

            host.host();

            await promiseMessage;
        });

        function setNoWebhookPermissions(){
            removeChannelPermission(host, MANAGE_WEBHOOKS);
        }
    });

    describe('Game chat', () => {
        let players;
        beforeEach(async() => {
            const createResult = await quasiDiscordGame.get({
                isStarted: true,
                setup: {
                    setupHiddens: [CITIZEN, CITIZEN, CITIZEN, GOON, GOON],
                    modifiers: [{ name: setupModifiers.DAY_START, value: true }],
                },
                playerCount: 5,
            });
            players = createResult.players;
        });

        it('Will forward day messages from game', async() => {
            const player = players[0];
            const userID = await player.getUserID();
            const promiseMessage = player.waitForMessage(m => m.content.includes(text));

            await chatService.say(userID, 'everyone', text);
            const message = await promiseMessage;

            expect(message.content).to.be.equal(text);
        });

        it('Will handle emojis', async() => {
            const emojiText = String.fromCodePoint(0x1F525);
            const player = players[0];
            const userID = await player.getUserID();
            const promiseMessage = player.waitForMessage(m => m.content.includes(emojiText));

            await chatService.say(userID, 'everyone', emojiText);
            const message = await promiseMessage;

            expect(message.content).to.be.equal(emojiText);
        });

        it('Will forward day messages from discord', async() => {
            const player = players[0];
            await player.chatter(text);

            await new Promise(resolve => setTimeout(resolve, 100));
            const channelMessages = getChannelMessages(player);

            const discordMessageChatter = channelMessages.filter(m => m.content === text);
            expect(discordMessageChatter.length).to.be.equal(1);
        });

        it('Will forward day messages from one discord to another', async() => {
            const player = players[0];
            const gameID = await getGameID(player);
            await channelService.createIntegration(gameID, integrationTypes.DISCORD, {
                channelID: config.discord.test_channel_id2,
            });

            const m = await new Promise(resolve => {
                player.waitForMessage(mes => {
                    resolve(mes);
                    return true;
                });
                player.chatter(text);
            });

            expect(m.author.bot).to.be.equal(true);
            expect(m.content).to.be.equal(text);
            expect(m.author.name).to.be.equal(player.getName());
        });
    });

    function getChannelMessages(player){
        const channels = player.discordClient.channels.cache;
        const channel = channels.get(config.discord.reboot_channel_id).channel;
        return Object.values(channel.messageHistory);
    }

    async function getGameID(user){
        return (await user.getGameObj()).id;
    }
});
