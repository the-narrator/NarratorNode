function infoPregame(joinID){
    return {
        embed: {
            title: `#${joinID} Status`,
            description: 'Waiting for game to start',
            fields: [{
                name: 'Roles List',
                value: ':blue_car: Town Random\n:blue_car: Town Random\n:blue_car: Town Random\n'
                    + ':blue_car: Town Random\n:blue_car: Town Random\n:blue_car: Town Random\n'
                    + ':red_circle: Mafia Random\n:red_circle: Mafia Random',
                inline: true,
            }, {
                name: 'Player Lobby',
                value: '\n*3 more to start*',
                inline: true,
            }, {
                name: 'Setup',
                value: 'Default setup',
                inline: true,
            }],
            footer: {
                text: 'Hosted by The Narrator',
            },
        },
    };
}

// not ready yet
function voteOverview(votes, players){
    // `**[${getPlayerIndex(player)}] ${player.getName()}`,
    const fields = players.map(() => '');
    return {
        embed: {
            description: '**Vote Counts**',
            fields,
            footer: {
                text: 'Hosted by The Narrator',
            },
        },
    };
}

module.exports = {
    infoPregame,
    voteOverview,
};
