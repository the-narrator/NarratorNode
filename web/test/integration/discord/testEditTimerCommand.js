require('../../init');
const { expect } = require('chai');

const setupModifiers = require('../../../models/enums/setupModifiers');

const quasiSetup = require('../../quasiModels/quasiSetup');
const quasiUser = require('../../quasiModels/quasiUser');

const Constants = require('../../../channels/discord/Constants');


describe('Edit Timer command', () => {
    let host;
    beforeEach(async() => {
        host = await quasiUser.createDiscordUser();
        const ownerID = await host.getUserID();
        await quasiSetup.create({
            ownerID,
            setupHiddenCount: 3,
            modifiers: [{ name: setupModifiers.DAY_START, value: false }],
        });
        await host.host();
        await host.moderate();
        await host.setSetup('custom');
    });

    it('Should edit the timer when command is given', async() => {
        const players = quasiUser.createDiscordUsers(3);
        await Promise.all(players.map(player => player.join()));
        await host.say(Constants.start, 'It is now nighttime');

        await host.whisper(`${Constants.edittimer} 4`, 'Phase will end in 4 minutes.');

        const message = await players[0].whisper(`${Constants.edittimer} 5`);
        expect(message.content).to.include('Only moderators can use this command.');

        await host.whisper(Constants.commands, 'Edit phase timer');
    });
});
