require('../../init');
const { expect } = require('chai');

const gameIntegrationService = require('../../../services/gameIntegrationService');

const integrationTypes = require('../../../models/enums/integrationTypes');

const quasiUser = require('../../quasiModels/quasiUser');


describe('Lobby', () => {
    let host;
    afterEach(async() => {
        if(host)
            await host.killGame();
        host = null;
    });


    it('Should register discord game integrations', async() => {
        host = quasiUser.createDiscordUser();

        const gameObj = await host.host();
        const integrations = await gameIntegrationService.getIntegrations(gameObj.id);

        expect(integrations).to.include(integrationTypes.DISCORD);
    });
});
