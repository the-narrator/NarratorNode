require('../../init');
const { expect } = require('chai');

const sinon = require('sinon');

const config = require('../../../../config');

const gameService = require('../../../services/gameService');

const quasiUser = require('../../quasiModels/quasiUser');

const timeoutValue = config.discord.timeout;


describe.skip('Game expiration', () => {
    let clock;
    beforeEach(() => {
        clock = sinon.useFakeTimers();
    });

    afterEach(() => {
        clock.restore();
    });

    it('Should kill the lobby upon timeout', async() => {
        const activeGameCount = await getActiveGameCount();
        const host = await quasiUser.createDiscordUser();
        const user2 = await quasiUser.createDiscordUser();
        const user3 = await quasiUser.createDiscordUser();

        await host.host();
        clock.tick(timeoutValue / 2);

        await user2.join();

        clock.tick(timeoutValue * 0.75);

        await user3.say('garbage command');

        clock.tick(timeoutValue * 0.75);

        const newActiveGameCount = await getActiveGameCount();
        expect(newActiveGameCount).to.equal(activeGameCount);
    });
});

async function getActiveGameCount(){
    return (await gameService.getAll()).length;
}
