require('../../init');
const { expect } = require('chai');

const quasiUser = require('../../quasiModels/quasiUser');

const Constants = require('../../../channels/discord/Constants');


describe('Help', async() => {
    it('shows help command', async() => {
        const user = await quasiUser.createDiscordUser();

        const m = await user.say(`${Constants.help}`, 'This bot uses a web interface as well');
        expect(m.content).to.be.equal(getHelpMessage());
    });

    it('shows help command without the prefix', async() => {
        const player = quasiUser.createDiscordUser();

        await new Promise(resolve => {
            player.waitForMessage(m => m.content
                .includes('This bot uses a web interface as well')).then(resolve);
            player.chatter('help');
        });
    });
});

function getHelpMessage(){
    // eslint-disable-next-line max-len
    return '*This bot uses a web interface as well as discord to host the game. When you join, the bot will DM you a link - click the link after game start, and you can do night actions or chat there instead. You can also DM the bot your night actions in discord if you do not wish to use the web interface.*\n'
        + '\n'
        + 'To quickly start a game:\n'
        + '- ** !in** to create/join a game\n'
        + '- ** !setup <setupName>** to pick a setup *ex: !setup mash*\n'
        + '- ** !daylength 3** to set the length of the day *!nightlength for night*\n'
        + '- ** !start** to start, duH\n'
        + '\n'
        + 'In game\n'
        + '- ** !vote** to vote people\n'
        + '- ** !commands** as a pm *to the bot* to get a list of all your available actions\n'
        + '\n'
        + 'Lastly\n'
        + '- ** !commands** to see more available commands\n'
        + '- ** !subscribe** in a channel to get notified when a game\'s starting!';
}
