require('../../../init');
const { expect } = require('chai');

const quasiUser = require('../../../quasiModels/quasiUser');

const Constants = require('../../../../channels/discord/Constants');


describe('Auto Parity modifier', () => {
    let host;
    beforeEach(() => {
        host = quasiUser.createDiscordUser();
    });

    afterEach(async() => {
        await host.killGame();
    });

    describe('Within lobby', () => {
        beforeEach(async() => {
            await host.host();
        });

        it('Will show usage', async() => {
            // given in beforeEach

            const message = await host.say(Constants.autoparity);

            const expectedMessage = 'Usage: _autoParity **on**_ or _autoParity **off**_';
            expect(message.content).to.include(expectedMessage);
        });

        it('Will set auto parity on', async() => {
            // given in beforeEach

            const message = await host.say(`${Constants.autoparity} on`);

            expect(message.content).to.be.equal('Auto day parity was turned on.');
        });

        it('Will set auto parity off', async() => {
            // given in beforeEach

            const message = await host.say(`${Constants.autoparity} off`);

            expect(message.content).to.be.equal('Auto day parity was turned off.');
        });

        it('Will handle attempts after game start', async() => {
            const players = quasiUser.createDiscordUsers(2);
            await Promise.all(players.map(player => player.join()));
            await host.start();

            const message = await host.whisper(`${Constants.autoparity} on`, '');

            expect(message.content).to.include('No updating modifiers after game start.');
        });
    });

    it('Will handle not being in a game', async() => {
        // given in beforeEach

        const message = await host.say(`${Constants.autoparity} on`, '');

        expect(message.content).to.include('You must be part of the game to submit this command.');
    });
});
