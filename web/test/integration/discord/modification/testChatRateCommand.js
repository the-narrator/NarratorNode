require('../../../init');
const { expect } = require('chai');

const quasiUser = require('../../../quasiModels/quasiUser');

const Constants = require('../../../../channels/discord/Constants');


describe('Chat rate modifier', () => {
    let host;
    beforeEach(() => {
        host = quasiUser.createDiscordUser();
    });

    afterEach(async() => {
        await host.killGame();
    });

    describe('Within lobby', () => {
        beforeEach(async() => {
            await host.host();
        });

        it('Will show usage', async() => {
            // given in beforeEach

            const message = await host.say(Constants.chatrate);

            // eslint-disable-next-line max-len
            const expectedMessage = 'Usage: _autoParity **2**_ to limit someone to one message per 2 seconds.';
            expect(message.content).to.include(expectedMessage);
        });

        it('Will set chat rate', async() => {
            // given in beforeEach

            const message = await host.say(`${Constants.chatrate} 10`);

            expect(message.content).to.be.equal('Allowed chat rate was set to 1 per 10 second(s).');
        });

        it('Will handle attempts after game start', async() => {
            const players = quasiUser.createDiscordUsers(2);
            await Promise.all(players.map(player => player.join()));
            await host.start();

            const message = await host.whisper(`${Constants.autoparity} on`, '');

            expect(message.content).to.include('No updating modifiers after game start.');
        });
    });

    it('Will handle not being in a game', async() => {
        // given in beforeEach

        const message = await host.say(`${Constants.autoparity} on`, '');

        expect(message.content).to.include('You must be part of the game to submit this command.');
    });
});
