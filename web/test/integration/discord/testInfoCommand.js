require('../../init');
const { expect } = require('chai');

const quasiDiscordUtils = require('../../quasiDiscord/quasiDiscordUtils');
const quasiUser = require('../../quasiModels/quasiUser');

const Constants = require('../../../channels/discord/Constants');


describe('Info command', () => {
    let host;
    let gameObj;
    let players;
    beforeEach(async() => {
        host = quasiUser.createDiscordUser();
        gameObj = await host.host();
        players = quasiUser.createDiscordUsers(6);
        await Promise.all(players.map(player => player.join()));
    });

    afterEach(async() => {
        if(host)
            await host.killGame();
        host = null;
    });

    it('Should show info', async() => {
        players.push(host);
        await host.start();

        await quasiDiscordUtils.massEndNight(players);
        const message = await host.say('info', m => m.content.embed);

        expect(message.content).to.eql(expectedInfoMessage(gameObj.joinID));
    });

    it('Will show info to moderators in a game that has not started', async() => {
        await host.moderate();

        const message = await host.whisper(Constants.info);

        expect(message.content).to.be.an('object');
    });

    it('Will not show player roles to a participating moderator', async() => {
        // no moderation, but still is host
        await host.start();

        const message = await host.whisper(Constants.info,
            m => m.content.embed && m.content.embed.description === 'Game in progress');

        const playerList = message.content.embed.fields.find(f => f.name === 'Player List');
        expect(playerList).to.deep.eq({
            inline: true,
            name: 'Player List',
            // eslint-disable-next-line max-len
            value: '**[1]** Player1\n**[2]** Player2\n**[3]** Player3\n**[4]** Player4\n**[5]** Player5\n**[6]** Player6\n**[7]** Player7\n',
        });
    });

    it('Setup should show the current setup ingame', async() => {
        players.push(host);
        await host.start();

        await quasiDiscordUtils.massEndNight(players);
        const message = await host.say('setup', m => m.content);

        expect(message.content).to.eql(expectedInfoMessage(gameObj.joinID));
    });
});

function expectedInfoMessage(joinID){
    return {
        embed: {
            description: 'Game in progress',
            fields: [
                {
                    inline: true,
                    name: 'Roles List',
                    // eslint-disable-next-line max-len
                    value: ':blue_car: Town Random\n:blue_car: Town Random\n:blue_car: Town Random\n:blue_car: Town Random\n:blue_car: Town Random\n:blue_car: Town Random\n:red_circle: Mafia Random\n:red_circle: Mafia Random',
                },
                {
                    inline: true,
                    name: 'Player List',
                    // eslint-disable-next-line max-len
                    value: '**[1]** Player1\n**[2]** Player2\n**[3]** Player3\n**[4]** Player4\n**[5]** Player5\n**[6]** Player6\n**[7]** Player7\n',
                },
            ],
            footer: {
                text: 'Hosted by The Narrator',
            },
            title: `#${joinID} Status`,
        },
    };
}
