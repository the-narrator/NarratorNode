require('../../init');
const { expect } = require('chai');

const setupModifiers = require('../../../models/enums/setupModifiers');

const quasiSetup = require('../../quasiModels/quasiSetup');
const quasiUser = require('../../quasiModels/quasiUser');

const Constants = require('../../../channels/discord/Constants');


const expectedErrorMsg = 'Only participating moderators can use this command.';

describe('Player Roles', () => {
    let host;
    beforeEach(async() => {
        host = await quasiUser.createDiscordUser();
        const ownerID = await host.getUserID();
        await quasiSetup.create({
            ownerID,
            setupHiddenCount: 3,
            modifiers: [{ name: setupModifiers.DAY_START, value: true }],
        });
        await host.host();
        await host.setSetup('custom');
    });

    it('Should get players roles when command is given', async() => {
        await host.moderate();
        const players = quasiUser.createDiscordUsers(3);
        await Promise.all(players.map(player => player.join()));
        await players[0].say(`${Constants.prefer} Goon`);
        await host.say(Constants.start, '**Day 1**');

        let message = await host.say(Constants.playerroles, m => m.content.embed);
        expect(message.content).to.deep.equal(getPlayerRoleMessage());

        message = await players[0].whisper(Constants.playerroles);
        expect(message.content).to.include(expectedErrorMsg);

        await host.say(Constants.commands, 'View player roles');
    });

    it('Should not give participating moderators player roles', async() => {
        const players = quasiUser.createDiscordUsers(2);
        await Promise.all(players.map(player => player.join()));
        await host.start();

        const message = await host.whisper(Constants.playerroles);

        expect(message.content).to.include(expectedErrorMsg);
    });
});

function getPlayerRoleMessage(){
    return {
        embed: {
            fields: [
                {
                    inline: false,
                    name: '**Player Roles**',
                    // eslint-disable-next-line max-len
                    value: '**:red_circle: Goon** - *Player2*\n**:blue_car: Citizen** - *Player3*\n**:blue_car: Citizen** - *Player4*',
                },
            ],
            footer: {
                text: 'Hosted by The Narrator',
            },
        },
    };
}
