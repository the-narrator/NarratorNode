require('../../init');

const quasiSetup = require('../../quasiModels/quasiSetup');
const quasiUser = require('../../quasiModels/quasiUser');

const quasiDiscordUtils = require('../../quasiDiscord/quasiDiscordUtils');

const { CITIZEN, GOON, PARITY_COP } = require('../../definitions/factionRoles');


describe('Night Feedback', () => {
    let host;
    it('Should send parity cop feedback', async() => {
        host = await quasiUser.createDiscordUser();
        const ownerID = await host.getUserID();
        await quasiSetup.create({
            ownerID,
            setupHiddens: [CITIZEN, GOON, PARITY_COP],
        });
        await host.host();
        const players = quasiUser.createDiscordUsers(2);
        await Promise.all(players.map(player => player.join()));
        players.push(host);
        await host.setSetup('custom');
        await host.start();

        const [parityCop] = await quasiDiscordUtils.filterRole(players, PARITY_COP.name);
        const others = players.filter(p => p !== parityCop);
        await parityCop.whisper(`compare ${others[0].getName()}`, 'You will compare');
        await quasiDiscordUtils.massEndNight(players);
        await quasiDiscordUtils.massSkipDay(players);
        await parityCop.whisper(`compare ${others[1].getName()}`, 'You will compare');
        await others[0].whisper('end night');
        await others[1].whisper('end night');

        await parityCop.whisper('end night', 'Your target\'s ');
    });
});
