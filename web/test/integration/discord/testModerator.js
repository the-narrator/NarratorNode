require('../../init');
const { expect } = require('chai');

const config = require('../../../../config');

const setupModifiers = require('../../../models/enums/setupModifiers');
const { PRIVATE } = require('../../../channels/discord/enums/channelTypes');

const gameService = require('../../../services/gameService');

const quasiSetup = require('../../quasiModels/quasiSetup');
const quasiUser = require('../../quasiModels/quasiUser');

const Constants = require('../../../channels/discord/Constants');
const outputs = require('./outputs');

const testChannel2 = config.discord.test_channel_id2;


describe('Moderators', () => {
    let host;
    beforeEach(async() => {
        host = await quasiUser.createDiscordUser();
        const ownerID = await host.getUserID();
        await quasiSetup.create({
            ownerID,
            setupHiddenCount: 9,
            modifiers: [{
                name: setupModifiers.DAY_START,
                value: true,
            }, {
                name: setupModifiers.SECRET_VOTES,
                value: true,
            }],
        });
        await host.host();
    });

    afterEach(async() => {
        if(host)
            await host.killGame();
    });

    it('Should allow moderators to leave games that have not started', async() => {
        await host.say(`${Constants.moderate} on`, 'will be moderating this game.');

        await host.leave(null, true);

        expect((await gameService.getAll()).length).to.equal(0);

        // tests to make sure moderators can still host after leaving game
        await host.host();

        host = null;
    });

    it('Should allow moderators to leave games that have started', async() => {
        await host.moderate();
        const players = quasiUser.createDiscordUsers(7);
        await Promise.all(players.map(player => player.join()));
        await host.setPhaseStart(false); // DAY START
        await host.say(Constants.start, 'It is now nighttime.  Day will resume in');
        let gameObject = await host.getGameObj();

        await host.leave(null, false);
        gameObject = await gameService.getByID(gameObject.id);

        await host.host(testChannel2);
        const newGameObject = await host.getGameObj();

        expect(gameObject.joinID).to.not.equal(newGameObject.joinID);
        await players[0].killGame();
    });

    it('Should allow moderators to ask for info', async() => {
        await host.say(`${Constants.moderate} on`, 'will be moderating this game.');
        const gameObject = await host.getGameObj();

        const response = await host.say(`${Constants.info}`,
            m => JSON.stringify(m.content).includes('Waiting for game to start'));

        expect(outputs.infoPregame(gameObject.joinID)).to.deep.equal(response.content);
    });

    it('Should have "end_phase" as a command for moderators', async() => {
        host.say(`${Constants.moderate} on`, 'will be moderating this game.');
        const players = quasiUser.createDiscordUsers(7);
        await Promise.all(players.map(player => player.join()));
        await host.setSetup('custom');

        await host.say(Constants.start, '**Day 1**');

        await host.say(`${Constants.commands}`, 'end phase');
    });

    it('Should allow moderators to rejoin as players', async() => {
        await host.say(`${Constants.moderate} on`, 'will be moderating this game.');

        await host.say(`${Constants.moderate} off`, 'will no longer be moderating this game.');
        const gameObject = await host.getGameObj();

        expect(gameObject.players.length).to.equal(1);
    });

    it('Should allow games to be started with less users than roles', async() => {
        host.say(`${Constants.moderate} on`, 'will be moderating this game.');
        const players = quasiUser.createDiscordUsers(7);
        await Promise.all(players.map(player => player.join()));
        await host.setSetup('custom');

        await host.say(Constants.start, '**Day 1**');
        const gameObject = await host.getGameObj();

        expect(gameObject.setup.setupHiddens.length).to.be.above(gameObject.players.length);
        expect(gameObject.isStarted).to.equal(true);
    });

    it('Should not allow the moderate command to be used in game', async() => {
        host.say(`${Constants.moderate} on`, 'will be moderating this game.');
        const players = quasiUser.createDiscordUsers(7);
        await Promise.all(players.map(player => player.join()));
        await host.setSetup('custom');
        await host.say(Constants.start, '**Day 1**');

        const waitMessage = 'You can\'t edit game settings after game start.';
        await host.say(`${Constants.moderate} off`, waitMessage);

        // assertion is the given
    });

    it('Should tell moderators who voted during secret voting', async() => {
        await Promise.all([host.setSetup('custom'), host.moderate()]);
        const players = quasiUser.createDiscordUsers(7);
        await Promise.all(players.map(player => player.join()));
        await host.setDiscussionLength(100);

        await host.say(Constants.start, '**Day 1**');
        await host.say('end phase', 'Voting polls are now open');

        return new Promise(resolve => {
            host.waitForMessage(m => {
                if(m.channel.type !== PRIVATE)
                    return;
                const message = `${players[1].getName()} voted for ${players[2].getName()}.`;
                return m.content.includes(message);
            }).then(resolve);
            players[1].vote(players[2]);
        });
    });
});
