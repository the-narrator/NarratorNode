require('../../init');
const { expect } = require('chai');

const setupModifiers = require('../../../models/enums/setupModifiers');

const quasiDiscordUtils = require('../../quasiDiscord/quasiDiscordUtils');
const quasiDiscordGame = require('../../quasiDiscord/quasiDiscordGame');
const quasiUser = require('../../quasiModels/quasiUser');

const { CITIZEN, GOON } = require('../../definitions/factionRoles');


describe('Message manipulation', async() => {
    let host;

    afterEach(async() => {
        if(host)
            await host.killGame();
        host = null;
    });

    it('Should not warn people when editing a message in pregame', async() => {
        host = quasiUser.createDiscordUser();
        await host.host();

        const chatterMessage = await host.chatter('I will edit this message.');
        const textToWaitFor = getEditWarningMessage(host);
        await new Promise((resolve, reject) => {
            host.waitForMessage(m => {
                if(typeof m.content !== 'string')
                    return;
                if(m.content === textToWaitFor)
                    reject('No warning should be given');
            });
            chatterMessage.edit('This was changed!');
            setTimeout(resolve, 1500);
        });
    });

    it('Should not warn moderators for talking in a started game', async() => {
        const { host: hostPlayer, players } = await quasiDiscordGame.get({
            isStarted: false,
            setup: {
                setupHiddens: [CITIZEN, CITIZEN, CITIZEN, GOON, GOON],
                modifiers: [{ name: setupModifiers.DAY_START, value: false }],
            },
            playerCount: 4,
        });
        await hostPlayer.moderate();
        expect(players[0].mListeners.length).to.be.equal(0);
        await players[1].start();

        const game = await hostPlayer.getGameObj();
        expect(game.isStarted).to.be.equal(true);

        const textToWaitFor = 'You shouldn\'t be talking in that channel';
        await new Promise(async(resolve, reject) => {
            hostPlayer.waitForMessage(m => {
                if(typeof m.content !== 'string')
                    return;
                if(m.content.includes(textToWaitFor))
                    reject('No warning should be given');
            });
            hostPlayer.chatter('yo');
            setTimeout(resolve, 1500);
        });
    });

    it('Should warn people when they edit a message for the first time', async() => {
        host = quasiUser.createDiscordUser();
        await host.host();
        const players = quasiUser.createDiscordUsers(6);
        await Promise.all(players.map(player => player.join()));
        players.push(host);
        await host.start();

        await quasiDiscordUtils.massEndNight(players);
        const chatterMessage = await host.chatter('I will edit this message.');
        const textToWaitFor = getEditWarningMessage(host);
        await new Promise(resolve => {
            host.waitForMessage(m => {
                if(typeof m.content !== 'string')
                    return;
                return m.content === textToWaitFor;
            }).then(resolve);
            chatterMessage.edit('This was changed!');
        });

        // assertion is the received warning
    });

    it('Should modkill people who edit their message twice', async() => {
        host = quasiUser.createDiscordUser();
        await host.host();
        const players = quasiUser.createDiscordUsers(6);
        await Promise.all(players.map(player => player.join()));
        players.push(host);
        await host.start();
        await quasiDiscordUtils.massEndNight(players);
        const chatterMessage = await host.chatter('I will edit this message.');
        const textToWaitFor = `<@!${host.getDiscordID()}>, stop editing your messages.  `
            + '**If you edit or delete your messages again, I will modkill you.**';
        await new Promise(resolve => {
            host.waitForMessage(m => {
                if(typeof m.content !== 'string')
                    return;
                return m.content === textToWaitFor;
            }).then(resolve);
            chatterMessage.edit('This was changed!');
        });

        await new Promise(resolve => {
            host.waitForMessage(m => {
                if(typeof m.content !== 'string')
                    return;
                if(m.content.includes(host.getName()))
                    return m.content.includes(' has/have inexplicably died.');
            }).then(resolve);
            chatterMessage.edit('This was changed!');
        });
        const gameObject = await host.getGameObj();

        expect(host.getName()).to.equal(gameObject.graveyard[0].name);
    });

    it('Should not warn if the edit consists of a single-character edit', async() => {
        host = quasiUser.createDiscordUser();
        await host.host();
        const players = quasiUser.createDiscordUsers(6);
        await Promise.all(players.map(player => player.join()));
        players.push(host);
        await host.start();
        await quasiDiscordUtils.massEndNight(players);
        const chatterMessage = await host.chatter('I made a typp !');
        const textToWaitFor = getEditWarningMessage(host);
        await new Promise((resolve, reject) => {
            host.waitForMessage(m => {
                if(typeof m.content !== 'string')
                    return;
                if(m.content === textToWaitFor)
                    reject('No warning should be given');
            });
            chatterMessage.edit('I made a typo !');
            setTimeout(resolve, 1500);
        });
    });

    function getEditWarningMessage(warnedPlayer){
        return `<@!${warnedPlayer.getDiscordID()}>, stop editing your messages.  `
            + '**If you edit or delete your messages again, I will modkill you.**';
    }
});
