require('../../init');
const { expect } = require('chai');

const quasiDiscordGame = require('../../quasiDiscord/quasiDiscordGame');
const quasiUser = require('../../quasiModels/quasiUser');

const setupRepo = require('../../../repos/setupRepo');

const setupService = require('../../../services/setup/setupService');

const Constants = require('../../../channels/discord/Constants');

const { CITIZEN, GOON } = require('../../definitions/factionRoles');


describe('Commands', async() => {
    describe('Out of started game', () => {
        let host;
        afterEach(async() => {
            await host.killGame();
        });

        beforeEach(() => {
            host = quasiUser.createDiscordUser();
        });

        it('Will handle commands when no game in channel', async() => {
            const { content } = await host.say(Constants.commands);

            expect(content).to.be.equal(getNoGameCommandsContent());
        });

        it('Will show commands when game is not started', async() => {
            await host.host();

            const message = await host.say(Constants.commands, '');

            expect(message.content).to.include('__Bot commands__');
            expect(message.content).to.include(Constants.autoparity);
            expect(message.content).to.include(Constants.chatrate);
        });

        it('Will show lobby commands in lobby', async() => {
            await host.host();

            const { content } = await host.say(Constants.commands, '');

            expect(content).to.include(Constants.prefer);
            expect(content).to.include(Constants.clearprefers);
        });

        it('Will show setup modifier commands if setup is editable', async() => {
            const gameObj = await host.host();
            await setupService.removeFeatured(gameObj.setup.id);

            const message = await host.say(Constants.commands, '');

            expect(message.content).to.include(Constants.daystart);
            expect(message.content).to.include(Constants.nightstart);
            expect(message.content).to.include(`${Constants.skip} <on`);
            expect(message.content).to.include(Constants.punch);
        });

        it('Will show setup modifier commands if setup is not editable', async() => {
            const gameObj = await host.host();
            await setupRepo.setUneditable(gameObj.setup.id);

            const message = await host.say(Constants.commands, '');

            expect(message.content).to.not.include(Constants.daystart);
            expect(message.content).to.not.include(Constants.nightstart);
            expect(message.content).to.not.include(`${Constants.skip} <on`);
            expect(message.content).to.not.include(Constants.ita);
        });
    });

    it('Will show game commands', async() => {
        const { players } = await quasiDiscordGame.get({
            isStarted: true,
            setup: {
                setupHiddens: [CITIZEN, CITIZEN, GOON, CITIZEN, GOON],
            },
            playerCount: 5,
        });

        const { content } = await players[0].whisper(Constants.commands, '');

        expect(content).to.include('roleCard');
    });
});

function getNoGameCommandsContent(){
    // eslint-disable-next-line max-len
    return "__Bot commands__\n**!info** - Displays current game status\n**!invite** - Instructions on inviting this bot to your server\n**!changeCommand <command>** - changes the command prefix for the bot\n**!stats** - view your basic stats with Narrator\n**!leaderboard** - view the global leaderboard within Narrator\n**!subscribe** - subscribe to this channel's started games\n**!unsubscribe** - stop being notified of channel's started games\n**!host** - Hosts a new game in the current channel\n";
}
