require('../../../init');
const { expect } = require('chai');

const preferRepo = require('../../../../repos/preferRepo');

const quasiUser = require('../../../quasiModels/quasiUser');

const Constants = require('../../../../channels/discord/Constants');


describe('Clear Prefers', () => {
    it('Will clear prefers', async() => {
        const host = await quasiUser.createDiscordUser();
        const gameObj = await host.host();
        await host.prefer(gameObj.setup.roles[0].name);

        const { content } = await host.say(Constants.clearprefers);
        const userID = gameObj.users[0].id;
        const prefers = await preferRepo.getBySetupIDUserID(gameObj.setup.id, userID);

        expect(content).to.be.equal('You have cleared all your prefers for this setup.');
        expect(prefers.length).to.be.equal(0);
    });
});
