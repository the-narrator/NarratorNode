require('../../../init');
const { expect } = require('chai');
const sinon = require('sinon');
const config = require('../../../../../config');

const integrationTypes = require('../../../../models/enums/integrationTypes');

const { MANAGE_MESSAGES } = require('../../../../channels/discord/enums/permissions');

// eslint-disable-next-line max-len
const { MANAGE_MESSAGES_ERROR_TEXT } = require('../../../../channels/discord/discordServices/permissionService');

const channelService = require('../../../../services/channelService');
const phaseService = require('../../../../services/phaseService');

const setupModifiers = require('../../../../models/enums/setupModifiers');
const voteSystemTypes = require('../../../../models/enums/voteSystemTypes');

const discordJsonClient = require('../../../../channels/discord/discordJsonClient');

const quasiGame = require('../../../quasiModels/quasiGame');
const quasiSetup = require('../../../quasiModels/quasiSetup');
const quasiUser = require('../../../quasiModels/quasiUser');

const { removeChannelPermission } = require('../../../quasiDiscord/quasiDiscordUtils');

const Constants = require('../../../../channels/discord/Constants');


describe('Voting', async() => {
    const setupArgs = hostUserID => ({
        modifiers: [{
            name: setupModifiers.DAY_START,
            value: true,
        }],
        ownerID: hostUserID,
        setupHiddenCount: 8,
    });

    describe('Within discord started game', () => {
        let host;
        let players;
        let spy;
        beforeEach(async() => {
            host = await quasiUser.createDiscordUser();
            players = quasiUser.createDiscordUsers(6);
            const hostUserID = await host.getUserID();
            await quasiSetup.create(setupArgs(hostUserID));
            spy = sinon.spy(discordJsonClient, 'deleteMessage');
            await host.host();
            await Promise.all([
                ...players.map(player => player.join()),
                host.setSetup('custom'),
            ]);
        });

        afterEach(async() => {
            discordJsonClient.deleteMessage.restore();
            if(host)
                await host.killGame();
        });

        it('Should allow skip votes', async() => {
            // given done in before each

            await host.start();

            await host.say('skip', `${host.getName()} voted for eliminating no one.`);
        });

        it('Should announce votes on vote phase starts', async() => {
            await Promise.all([
                host.moderate(),
                host.setDiscussionLength(1),
            ]);

            await host.say(Constants.start, '**Day 1**');

            await host.say('end phase', m => {
                if(typeof m.content === 'string')
                    return false;
                return m.content.embed.description === '**Vote Counts**';
            });
        });

        it('should submit votes even without the manage_messages permission', async() => {
            removeChannelPermission(host, MANAGE_MESSAGES);
            await host.say(Constants.start, '**Day 1**');

            await host.vote(players[0]);
        });

        it('Should tell people how to vote every day', async() => {
            await Promise.all([
                host.moderate(),
                host.setDiscussionLength(1),
                host.setTrialLength(1),
                host.setVoteSystem(voteSystemTypes.DIMINISHING),
            ]);

            await host.say(Constants.start, '**Day 1**');

            await host.say('end phase', m => {
                if(typeof m.content !== 'string')
                    return false;
                return m.content.includes(`For example, !vote ${host.getMention()}`);
            });

            let votes = [
                players[4].vote(players[1]),
                players[2].vote(players[1]),
            ];
            await Promise.all(votes);

            const deletedVoteMessages = spy.args
                .reduce((hasStart, message) => {
                    if(hasStart)
                        return true;
                    if(!message[0].content.includes)
                        return false;
                    return message[0].content.includes('vote');
                }, false);
            expect(deletedVoteMessages).to.equal(true);


            await host.say('end phase', 'will now give their defense');
            await host.say('end phase', m => {
                if(typeof m.content !== 'string')
                    return false;
                return m.content.includes(`For example, !vote ${host.getMention()}`);
            });

            votes = [
                players[4].vote(players[1]),
                players[2].vote(players[1]),
            ];
            await Promise.all(votes);

            await host.say('end phase', 'It is now nighttime');

            await host.say('end phase', '**Day 2**');
            await host.say('end phase', m => {
                if(typeof m.content !== 'string')
                    return false;
                return m.content.includes(`For example, !vote ${host.getMention()}`);
            });
        });
    });

    it('Should forward vote events after game integration add', async() => {
        const setup = await quasiSetup.create({
            setupHiddenCount: 3,
            modifiers: [{ name: setupModifiers.DAY_START, value: false }],
        });
        const game = await quasiGame.create({
            isStarted: true,
            playerCount: 3,
            setupID: setup.id,
        });
        const discordUser = await quasiUser.createDiscordUser();

        await channelService.createIntegration(
            game.id, integrationTypes.DISCORD, { channelID: config.discord.test_channel_id },
        );

        phaseService.setExpiration(game.lobbyID, 0);
        await discordUser.waitForMessage(m => {
            if(typeof m.content !== 'string')
                return false;
            return m.content.startsWith('**Day 1**');
        });
    });

    it('Will announce no chat forwarding if lacking permissions', async() => {
        const host = await quasiUser.createDiscordUser();
        removeChannelPermission(host, MANAGE_MESSAGES);
        const promiseMessage = host.waitForMessage(
            m => m.content.includes && m.content.includes(MANAGE_MESSAGES_ERROR_TEXT),
        );

        host.host();

        await promiseMessage;
    });
});
