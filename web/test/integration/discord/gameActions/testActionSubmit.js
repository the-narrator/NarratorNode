require('../../../init');
const { expect } = require('chai');

const gameModifiers = require('../../../../models/enums/gameModifiers');
const setupModifiers = require('../../../../models/enums/setupModifiers');
const voteSystemTypes = require('../../../../models/enums/voteSystemTypes');

const quasiDiscordGame = require('../../../quasiDiscord/quasiDiscordGame');
const quasiDisordUtils = require('../../../quasiDiscord/quasiDiscordUtils');

const Constants = require('../../../../channels/discord/Constants');

const { CITIZEN, GOON } = require('../../../definitions/factionRoles');


describe('Action submitting', () => {
    it('Will give feedback when submitting last will', async() => {
        const { players } = await quasiDiscordGame.get({
            isStarted: true,
            setup: {
                setupHiddens: [CITIZEN, CITIZEN, CITIZEN, GOON, GOON],
                modifiers: [
                    { name: setupModifiers.DAY_START, value: false },
                    { name: setupModifiers.LAST_WILL, value: true },
                ],
            },
            playerCount: 5,
        });

        const message = await players[0].whisper(`${Constants.lw} new last will`);

        expect(message.content).to.be.equal('Last will updated.');
    });

    it('Will give feedback when ending night', async() => {
        const { players } = await quasiDiscordGame.get({
            isStarted: true,
            setup: {
                setupHiddens: [CITIZEN, CITIZEN, CITIZEN, GOON, GOON],
                modifiers: [
                    { name: setupModifiers.DAY_START, value: false },
                ],
            },
            playerCount: 5,
        });

        const message = await players[0].whisper('end night');

        expect(message.content).to.be.equal('You opted to end night early.');
    });

    it('Will submit night actions when vote is multivote', async() => {
        const { players } = await quasiDiscordGame.get({
            isStarted: true,
            modifiers: [{
                name: gameModifiers.VOTE_SYSTEM, value: voteSystemTypes.MULTIVOTE_PLURALITY,
            }],
            playerCount: 5,
            setup: {
                setupHiddens: [CITIZEN, CITIZEN, CITIZEN, GOON, GOON],
                modifiers: [
                    { name: setupModifiers.DAY_START, value: false },
                ],
            },
        });
        const [citizen] = await quasiDisordUtils.filterRole(players, 'Citizen');
        const [goon] = await quasiDisordUtils.filterRole(players, 'Goon');

        const message = await goon.whisper(`kill ${citizen.getName()}`);

        expect(message.content).to.be.equal(`You will kill ${citizen.getName()}.`);
    });
});
