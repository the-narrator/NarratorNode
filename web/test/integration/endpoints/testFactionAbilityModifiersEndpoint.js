require('../../init');
const { expect } = require('chai');
const { expectStatusCode } = require('../../assertions');

const authHelpers = require('../../authHelpers');
const requests = require('../../requests');

const setupService = require('../../../services/setup/setupService');

const factionAbilityRepo = require('../../../repos/factionAbilityRepo');

const quasiSetup = require('../../quasiModels/quasiSetup');
const quasiGame = require('../../quasiModels/quasiGame');

const testControllerHelpers = require('../../testControllerHelpers');

const { FEATURED_PRIORITY, FEATURED_KEY } = require('../../fakeConstants');

const { MAX_PLAYER_COUNT } = require('../../../utils/constants');


describe('Faction Ability Modifiers Endpoint', () => {
    let headers;
    let setup;
    let factionAbility;
    let factionID;
    const defaultFactionAbility = {
        name: 'ZERO_WEIGHTED',
        value: true,
    };
    beforeEach(async() => {
        setup = await quasiSetup.create();
        await quasiGame.create({ setupID: setup.id, hostID: setup.ownerID });
        headers = authHelpers.fakeTempAuthHeader(setup.ownerID);
        const faction = setup.factions.find(f => f.abilities.length);
        factionID = faction.id;
        [factionAbility] = faction.abilities;
    });

    it('Will update a faction ability modifier', async() => {
        // given setup in beforeEach

        const responseObj = await upsertFactionAbilityModifier();
        const factionAbilityModifiers = await factionAbilityRepo.getModifiersBySetupID(setup.id);

        expect(responseObj.statusCode).to.be.equal(200);
        const updatedModifier = factionAbilityModifiers
            .find(m => m.name === defaultFactionAbility.name
                && m.value === defaultFactionAbility.value);
        expect(updatedModifier).to.be.an('object');
    });

    // eslint-disable-next-line max-len
    it('Will have multiple bool modifiers of the same name if the player counts differ', async() => {
        await upsertFactionAbilityModifier(defaultFactionAbility);
        const responseObj = await upsertFactionAbilityModifier({
            ...defaultFactionAbility,
            maxPlayerCount: MAX_PLAYER_COUNT - 1,
        });
        setup = await setupService.getByID(setup.id);

        expect(responseObj.statusCode).to.be.equal(200);
        const modifiedFactionAbility = setup.factions.find(f => f.id === factionID).abilities
            .find(ability => ability.id === factionAbility.id);
        const modifiers = modifiedFactionAbility.modifiers
            .filter(modifier => modifier.name === defaultFactionAbility.name);
        expect(modifiers.length).to.be.equal(2);
    });

    // eslint-disable-next-line max-len
    it('Will have multiple int modifiers of the same name if the player counts differ', async() => {
        const args = { name: 'CHARGES', value: 3 };

        await upsertFactionAbilityModifier(args);
        const responseObj = await upsertFactionAbilityModifier({
            ...args,
            maxPlayerCount: MAX_PLAYER_COUNT - 1,
        });
        setup = await setupService.getByID(setup.id);

        expectStatusCode(200, responseObj);
        const modifiedFactionAbility = setup.factions.find(f => f.id === factionID).abilities
            .find(ability => ability.id === factionAbility.id);
        const modifiers = modifiedFactionAbility.modifiers
            .filter(modifier => modifier.name === args.name);
        expect(modifiers.length).to.be.equal(2);
    });

    it('Will not create factions in featured setups', async() => {
        await setupService.setPriority(setup.id, FEATURED_PRIORITY, FEATURED_KEY);

        const responseObj = await upsertFactionAbilityModifier();

        testControllerHelpers.assertUneditableSetupErrors(responseObj);
    });

    it('Will not insert duplicate rows into the database for boolean modifiers', async() => {
        await upsertFactionAbilityModifier();

        const responseObj = await upsertFactionAbilityModifier();
        const factionAbilityModifiers = await factionAbilityRepo.getModifiersBySetupID(setup.id);

        expect(responseObj.statusCode).to.be.equal(200);
        const updatedModifier = factionAbilityModifiers
            .filter(m => m.name === defaultFactionAbility.name
                && m.value === defaultFactionAbility.value);
        expect(updatedModifier.length).to.be.eql(1);
    });

    it('Will not insert duplicate rows into the database for int modifiers', async() => {
        const args = { name: 'CHARGES', value: 50 };
        await upsertFactionAbilityModifier(args);

        const responseObj = await upsertFactionAbilityModifier(args);
        const factionAbilityModifiers = await factionAbilityRepo.getModifiersBySetupID(setup.id);

        expect(responseObj.statusCode).to.be.equal(200);
        const updatedModifier = factionAbilityModifiers
            .filter(m => m.name === args.name
                && m.value === args.value);
        expect(updatedModifier.length).to.be.eql(1);
    });

    it('Will fail gracefully when the value is supposed to be a boolean', async() => {
        const args = { name: 'ZERO_WEIGHTED', value: 50 };

        const responseObj = await upsertFactionAbilityModifier(args);

        expect(responseObj.statusCode).to.be.equal(422);
    });

    it('Will fail gracefully when the value is supposed to be a boolean', async() => {
        const args = { name: 'CHARGES', value: true };

        const responseObj = await upsertFactionAbilityModifier(args);

        expect(responseObj.statusCode).to.be.equal(422);
    });

    it('Will fail gracefully when the modifier is not specified', async() => {
        const args = { name: undefined };

        const responseObj = await upsertFactionAbilityModifier(args);

        expect(responseObj.statusCode).to.be.equal(422);
    });

    it('Will fail gracefully when the modifier is invalid', async() => {
        const args = { name: 'garbage_name' };

        const responseObj = await upsertFactionAbilityModifier(args);

        expect(responseObj.statusCode).to.be.equal(422);
    });

    it('Will fail gracefully when value is not specified', async() => {
        const args = { value: undefined };

        const responseObj = await upsertFactionAbilityModifier(args);

        expect(responseObj.statusCode).to.be.equal(422);
    });

    it('Will fail gracefully when minPlayerCount is not specified', async() => {
        const args = { maxPlayerCount: undefined };

        const responseObj = await upsertFactionAbilityModifier(args);

        expect(responseObj.statusCode).to.be.equal(422);
    });

    it('Will fail gracefully when maxPlayerCount is not specified', async() => {
        const args = { minPlayerCount: undefined };

        const responseObj = await upsertFactionAbilityModifier(args);

        expect(responseObj.statusCode).to.be.equal(422);
    });

    it('Will fail gracefully when the faction ability id is not recognized', async() => {
        const factionAbilityID = factionAbility.id + (setup.factions.length * 70) + 1;

        const responseObj = await upsertFactionAbilityModifier({ factionAbilityID });

        expect(responseObj.statusCode).to.be.equal(422);
    });

    function upsertFactionAbilityModifier(args = {}){
        const keys = Object.keys(args);
        const modifierArgs = {
            name: keys.includes('name') ? args.name : defaultFactionAbility.name,
            value: keys.includes('value') ? args.value : defaultFactionAbility.value,
            minPlayerCount: 'minPlayerCount' in args ? args.minPlayerCount : 0,
            maxPlayerCount: 'maxPlayerCount' in args ? args.maxPlayerCount : MAX_PLAYER_COUNT,
        };
        const factionAbilityID = args.factionAbilityID || factionAbility.id;
        return requests.post(
            `api/factionAbilities/${factionAbilityID}/modifiers`, modifierArgs, headers,
        );
    }
});
