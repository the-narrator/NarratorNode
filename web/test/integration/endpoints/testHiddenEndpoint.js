require('../../init');
const { expect } = require('chai');

const authHelpers = require('../../authHelpers');
const requests = require('../../requests');

const gameService = require('../../../services/gameService');
const setupService = require('../../../services/setup/setupService');

const quasiGame = require('../../quasiModels/quasiGame');
const quasiSetup = require('../../quasiModels/quasiSetup');
const quasiUser = require('../../quasiModels/quasiUser');

const testControllerHelpers = require('../../testControllerHelpers');

const { FEATURED_KEY, FEATURED_PRIORITY, HIDDEN_NAME } = require('../../fakeConstants');


describe('Hidden Endpoint', () => {
    let headers;
    let user;
    let setup;
    let gameObject;
    let hidden;
    beforeEach(async() => {
        user = await quasiUser.createUser();
        setup = await quasiSetup.create({ ownerID: user.id });
        headers = authHelpers.fakeTempAuthHeader(setup.ownerID);
        gameObject = await quasiGame.create({ setupID: setup.id, hostID: setup.ownerID });
        [hidden] = setup.hiddens;
    });

    it('Will create a hidden', async() => {
        const hiddenLength = setup.hiddens.length;
        const hiddenBody = {
            name: HIDDEN_NAME,
            setupID: setup.id,
        };

        const responseObj = await requests.post('api/hiddens', hiddenBody, headers);
        gameObject = await gameService.getByLobbyID(gameObject.joinID);

        expect(responseObj.statusCode).to.be.equal(200);
        expect(!!responseObj.json.response.id).to.be.equal(true);
        expect(gameObject.setup.hiddens.length).to.be.equal(hiddenLength + 1);
        expect(!!gameObject.setup.hiddens[0].id).to.be.equal(true);
    });

    it('Will not add hidden if the setup id does not match the game setup id', async() => {
        const setup2 = await quasiSetup.create({ ownerID: user.id });
        const hiddenBody = {
            name: HIDDEN_NAME,
            setupID: setup2.id,
        };

        const responseObj = await requests.post('api/hiddens', hiddenBody, headers);

        expect(responseObj.statusCode).to.be.equal(422);
    });

    it('Will not create a hidden if the hidden doesn\'t belong to the user', async() => {
        const user2 = await quasiUser.createUser();
        const hiddenLength = gameObject.setup.hiddens.length;
        const user2Headers = authHelpers.fakeTempAuthHeader(user2.id);
        const hiddenBody = {
            name: HIDDEN_NAME,
            setupID: setup.id,
        };

        const responseObj = await requests.post('api/hiddens', hiddenBody, user2Headers);
        gameObject = await gameService.getByLobbyID(gameObject.joinID);

        expect(responseObj.statusCode).to.be.equal(422);
        expect(gameObject.setup.hiddens.length).to.be.equal(hiddenLength);
    });

    it('Will not create a hidden if the request is missing a name', async() => {
        const args = {
            setupID: setup.id,
        };

        const responseObj = await requests.post('api/hiddens', args, headers);

        expect(responseObj.statusCode).to.be.equal(422);
    });

    it('Will not create a hidden if the request is missing setup id', async() => {
        const args = {
            name: HIDDEN_NAME,
        };

        const responseObj = await requests.post('api/hiddens', args, headers);

        expect(responseObj.statusCode).to.be.equal(422);
    });

    it('Will not create a hidden if the request\'s name is too short', async() => {
        const args = {
            name: '',
        };

        const responseObj = await requests.post('api/hiddens', args, headers);

        expect(responseObj.statusCode).to.be.equal(422);
    });

    it('Will not allow creates if the setup is featured', async() => {
        await setupService.setPriority(setup.id, FEATURED_PRIORITY, FEATURED_KEY);
        const hiddenBody = {
            name: HIDDEN_NAME,
            setupID: setup.id,
        };

        const responseObj = await requests.post('api/hiddens', hiddenBody, headers);

        testControllerHelpers.assertUneditableSetupErrors(responseObj);
    });

    it('Will delete a hidden', async() => {
        const hiddenLength = setup.hiddens.length;

        const responseObj = await requests.delete(`api/hiddens/${hidden.id}`, headers);
        setup = await setupService.getByID(setup.id);

        expect(responseObj.statusCode).to.be.equal(204);
        expect(setup.hiddens.length).to.be.equal(hiddenLength - 1);
    });

    it('Will not delete hiddens of a featured setup', async() => {
        await setupService.setPriority(setup.id, FEATURED_PRIORITY, FEATURED_KEY);

        const responseObj = await requests.delete(`api/hiddens/${hidden.id}`, headers);

        expect(responseObj.statusCode).to.be.equal(422);
    });
});
