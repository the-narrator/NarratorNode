require('../../init');
const { expect } = require('chai');

const authHelpers = require('../../authHelpers');
const requests = require('../../requests');

const gameService = require('../../../services/gameService');
const setupService = require('../../../services/setup/setupService');

const quasiGame = require('../../quasiModels/quasiGame');
const quasiSetup = require('../../quasiModels/quasiSetup');
const quasiUser = require('../../quasiModels/quasiUser');

const testControllerHelpers = require('../../testControllerHelpers');

const { MAX_PLAYER_COUNT } = require('../../../utils/constants');


describe('Faction Modifier Endpoint', () => {
    let headers;
    let faction;
    let modifierName;
    let oldModifierValue;
    let newModifierValue;
    let gameID;
    let setupID;
    beforeEach(async() => {
        const setup = await quasiSetup.create();
        setupID = setup.id;
        gameID = (await quasiGame.create({ setupID: setup.id, hostID: setup.ownerID })).id;
        headers = authHelpers.fakeTempAuthHeader(setup.ownerID);
        faction = setup.factions.find(f => f.modifiers.length);
        const modifier = faction.modifiers
            .find(m => typeof(m.value) === 'boolean');
        modifierName = modifier.name;
        oldModifierValue = modifier.value;
        newModifierValue = !modifier.value;
    });

    it('Will update the faction\'s modifier (boolean)', async() => {
        // given in before each

        const responseObj = await createRequest();
        const gameObj = await gameService.getByID(gameID);

        expect(responseObj.statusCode).to.be.equal(200);
        faction = gameObj.setup.factions.filter(newFaction => newFaction.id === faction.id)[0];
        const newModifier = faction.modifiers
            .filter(modifier => modifier.name === modifierName)[0];
        expect(newModifier.value).to.be.equal(newModifierValue);
        expect(newModifier.value).to.be.equal(responseObj.json.response.value);
    });

    it('Will have multiple int modifiers of the same name if the player counts differ', async() => {
        const prevModifier = faction.modifiers
            .find(modifier => typeof(modifier.value) === 'number');
        const newValue = prevModifier.value + 1;
        const args = {
            modifierName: prevModifier.name,
            modifierValue: newValue,
        };

        await createRequest(args);
        const responseObj = await createRequest({
            ...args,
            maxPlayerCount: MAX_PLAYER_COUNT - 1,
        });
        const setup = await setupService.getByID(setupID);

        expect(responseObj.statusCode).to.be.equal(200);
        faction = setup.factions.find(newFaction => newFaction.id === faction.id);
        const modifiers = faction.modifiers.filter(modifier => modifier.name === args.modifierName);
        expect(modifiers.length).to.be.equal(2);
    });

    // eslint-disable-next-line max-len
    it('Will have multiple bool modifiers of the same name if the player counts differ', async() => {
        const prevModifier = faction.modifiers
            .find(modifier => typeof(modifier.value) === 'boolean');
        const newValue = !prevModifier.value;
        const args = {
            modifierName: prevModifier.name,
            modifierValue: newValue,
        };

        await createRequest(args);
        const responseObj = await createRequest({
            ...args,
            maxPlayerCount: MAX_PLAYER_COUNT - 1,
        });
        const setup = await setupService.getByID(setupID);

        expect(responseObj.statusCode).to.be.equal(200);
        faction = setup.factions.find(newFaction => newFaction.id === faction.id);
        const modifiers = faction.modifiers.filter(modifier => modifier.name === args.modifierName);
        expect(modifiers.length).to.be.equal(2);
    });

    it('Will update the faction\'s modifier (integer)', async() => {
        const prevModifier = faction.modifiers
            .find(modifier => typeof(modifier.value) === 'number');
        const newValue = prevModifier.value + 1;

        const responseObj = await createRequest({
            modifierName: prevModifier.name,
            modifierValue: newValue,
        });
        const gameObject = await gameService.getByID(gameID);

        expect(responseObj.statusCode).to.be.equal(200);
        faction = gameObject.setup.factions.find(newFaction => newFaction.id === faction.id);
        const newModifier = faction.modifiers
            .filter(modifier => modifier.name === prevModifier.name)[0];
        expect(newModifier.value).to.be.equal(newValue);
        expect(newModifier.value).to.be.equal(responseObj.json.response.value);
    });

    it('Will not allow updates if the user doesn\'t own the setup', async() => {
        const user = await quasiUser.createUser();

        const responseObj = await createRequest({
            headers: authHelpers.fakeTempAuthHeader(user.id),
        });
        const gameObject = await gameService.getByID(gameID);

        expect(responseObj.statusCode).to.be.equal(403);
        faction = gameObject.setup.factions.filter(newFaction => newFaction.id === faction.id)[0];
        const newModifier = faction.modifiers
            .filter(modifier => modifier.name === modifierName)[0];
        expect(newModifier.value).to.be.equal(oldModifierValue);
    });

    it('Will gracefully fail when the modifier name is wrong', async() => {
        // test is setup in beforeEach

        const responseObj = await createRequest({
            modifierName: modifierName + modifierName,
        });

        expect(responseObj.statusCode).to.be.equal(422);
    });

    it('Will gracefully fail when the request is missing the name', async() => {
        // test is setup in beforeEach

        const responseObj = await createRequest({
            modifierName: undefined,
        });

        expect(responseObj.statusCode).to.be.equal(422);
    });

    it('Will gracefully fail when the request is missing maxPlayerCount', async() => {
        // test is setup in beforeEach

        const responseObj = await createRequest({
            minPlayerCount: undefined,
        });

        expect(responseObj.statusCode).to.be.equal(422);
    });

    it('Will gracefully fail when the request is missing minPlayerCount', async() => {
        // test is setup in beforeEach

        const responseObj = await createRequest({
            maxPlayerCount: undefined,
        });

        expect(responseObj.statusCode).to.be.equal(422);
    });

    it('Will gracefully fail when the request is missing the value', async() => {
        // test is setup in beforeEach

        const responseObj = await createRequest({
            modifierValue: undefined,
        });

        expect(responseObj.statusCode).to.be.equal(422);
    });

    it('Will gracefully fail when the request is supposed to be boolean', async() => {
        const prevModifier = faction.modifiers
            .filter(modifier => typeof(modifier.value) === 'boolean')[0];

        const responseObj = await createRequest({
            modifierName: prevModifier.name,
            modifierValue: 300,
        });

        expect(responseObj.statusCode).to.be.equal(422);
    });

    it('Will gracefully fail when the request is supposed to be an integer', async() => {
        const prevModifier = faction.modifiers
            .filter(modifier => typeof(modifier.value) === 'number')[0];

        const responseObj = await createRequest({
            modifierName: prevModifier.name,
            modiferValue: true,
        });

        expect(responseObj.statusCode).to.be.equal(422);
    });

    it('Will not update games that have started', async() => {
        const setup = await quasiSetup.create({
            setupHiddenCount: 3,
        });
        await quasiGame.create({
            setupID: setup.id,
            hostID: setup.ownerID,
            isStarted: true,
            playerCount: 3,
        });
        const prevModifier = faction.modifiers
            .filter(modifier => typeof(modifier.value) !== 'boolean')[0];

        const responseObj = await createRequest({
            modifierName: prevModifier.name,
            modifierValue: 1,
            factionID: setup.factions[0].id,
            headers: authHelpers.fakeTempAuthHeader(setup.ownerID),
        });

        testControllerHelpers.assertUneditableSetupErrors(responseObj);
    });

    function createRequest(args = {}){
        const factionID = 'factionID' in args ? args.factionID : faction.id;
        const payload = {
            name: 'modifierName' in args ? args.modifierName : modifierName,
            value: 'modifierValue' in args ? args.modifierValue : newModifierValue,
            minPlayerCount: 'minPlayerCount' in args ? args.minPlayerCount : 0,
            maxPlayerCount: 'maxPlayerCount' in args ? args.maxPlayerCount : MAX_PLAYER_COUNT,
        };
        return requests.post(`api/factions/${factionID}/modifiers`, payload,
            args.headers || headers);
    }
});
