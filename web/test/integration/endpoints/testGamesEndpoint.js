require('../../init');
const { expect } = require('chai');
const sinon = require('sinon');
const { expectStatusCode } = require('../../assertions');

const roleModifiers = require('../../../models/enums/roleModifiers');
const setupModifiers = require('../../../models/enums/setupModifiers');

const authHelpers = require('../../authHelpers');
const requests = require('../../requests');

const setupRepo = require('../../../repos/setupRepo');

const actionService = require('../../../services/actionService');
const factionRoleModifierService = require('../../../services/setup/factionRoleModifierService');
const gameService = require('../../../services/gameService');
const moderatorService = require('../../../services/moderatorService');
const setupHiddenService = require('../../../services/setup/setupHiddenService');

const util = require('../../../utils/helpers');

const quasiGame = require('../../quasiModels/quasiGame');
const quasiSetup = require('../../quasiModels/quasiSetup');
const quasiUser = require('../../quasiModels/quasiUser');
const quasiWebSocket = require('../../quasiWebSocket');

const gameSchema = require('../../schemas/gameSchema');

const { getModeratorID } = require('../../../utils/lobbyUtil');

const { PLAYER_NAME } = require('../../fakeConstants');

const { MAX_PLAYER_COUNT } = require('../../../utils/constants');


describe('Games', () => {
    let clock;
    let ws;

    afterEach(() => {
        if(clock)
            clock.restore();
        if(ws)
            ws.close();
    });

    describe('Create game', () => {
        it('Creates a game from a setupID', async() => {
            const setup = await quasiSetup.create();
            const headers = authHelpers.fakeTempAuthHeader(setup.ownerID);

            const request = { name: PLAYER_NAME, setupID: setup.id };
            const responseObj = await requests.post('api/games', request, headers);

            expect(responseObj.statusCode).to.be.equal(200);
            expect(responseObj.json.response.setup.name).to.be.equal(setup.name);
        });

        it('Does not create another setup when setupID is specified', async() => {
            const userSetup = await quasiSetup.create();
            const headers = authHelpers.fakeTempAuthHeader(userSetup.ownerID);
            const allSetups = await setupRepo.getAll();
            const args = {
                setupID: userSetup.id,
            };

            const responseObj = await requests.post('api/games', args, headers);
            const allSetups2 = await setupRepo.getAll();

            expect(responseObj.statusCode).to.be.equal(200);
            expect(allSetups.length).to.be.equal(allSetups2.length);
        });

        it('Will not creates a game when body is not specified', async() => {
            await quasiUser.createUser();
            const headers = authHelpers.fakeFirebaseHeader();

            const responseObj = await requests.post('api/games', null, headers);

            expect(responseObj.statusCode).to.be.equal(400);
        });

        it('Will not create a game when the setupID is not a number', async() => {
            const user = await quasiUser.createUser();
            const headers = authHelpers.fakeTempAuthHeader(user.id);
            const args = {
                setupID: '',
            };

            const responseObj = await requests.post('api/games', args, headers);

            expect(responseObj.statusCode).to.be.equal(422);
        });

        it('Does not create a game when no header is specified', async() => {
            await quasiUser.createUser();
            const hostName = 'testHost';

            const responseObj = await requests.post('api/games', { name: hostName });

            expect(responseObj.statusCode).to.be.equal(403);
        });

        it('Does not error when no json is specified', async() => {
            await quasiUser.createUser();
            const headers = authHelpers.fakeFirebaseHeader();

            const responseObj = await requests.post('api/games', 'Not a json string', headers);

            expect(responseObj.statusCode).to.be.equal(400);
        });

        it('Does not create another game if user is in a game', async() => {
            const game = await quasiGame.create();
            const headers = authHelpers.fakeTempAuthHeader(game.users[0].id);

            const request = { name: PLAYER_NAME, setupID: game.setup.id };
            const responseObj = await requests.post('api/games', request, headers);

            expectStatusCode(422, responseObj);

            const games = await gameService.getAll();
            games.forEach(g => expect(g.users.length).to.be.equal(1));
            expect(games.length).to.be.equal(1);
        });

        it('Will send a game create event', async() => {
            const user = await quasiUser.createUser();
            const setup = await quasiSetup.create();
            const hostHeaders = authHelpers.fakeTempAuthHeader(setup.ownerID);
            const lobbyUserHeader = authHelpers.fakeTempAuthHeader(user.id);
            const messages = await addWebSocket(lobbyUserHeader);
            const request = { name: PLAYER_NAME, setupID: setup.id };

            const responseObj = await requests.post('api/games', request, hostHeaders);

            expect(responseObj.statusCode).to.equal(200);
            const newLobbyMessage = messages.find(({ event }) => event === 'lobbyCreate');
            gameSchema.checkGameCreateEvent(newLobbyMessage);
            expect(newLobbyMessage.users[0].isModerator).to.be.equal(true);
        });
    });

    describe('Delete game', () => {
        let game;
        let defaultHeader;
        beforeEach(async() => {
            game = await quasiGame.create();
            const modUser = await quasiUser.createMod();
            defaultHeader = authHelpers.fakeTempAuthHeader(modUser.id);
        });

        it('Deletes a game from memory', async() => {
            // in before each

            const responseObj = await deleteLobby();
            game = await gameService.getByLobbyID(game.joinID);

            expect(responseObj.statusCode).to.be.equal(204);
            expect(game).to.be.a('null');
        });

        it('Will 404 when gameID is not number', async() => {
            // in before each

            const responseObj = await deleteLobby({ gameID: 'abc' });

            expectStatusCode(404, responseObj);
        });

        it('Protects games from being deleted without \'Game editing privileges\'.', async() => {
            const user = await quasiUser.createUser();
            const userHeader = authHelpers.fakeTempAuthHeader(user.id);

            const responseObj = await deleteLobby({ header: userHeader });
            game = await gameService.getByLobbyID(game.joinID);

            expect(responseObj.statusCode).to.be.equal(403);
            expect(game).to.not.be.a('null');
        });

        it('Will send a lobby deleted event', async() => {
            const user = await quasiUser.createUser();
            const lobbyUserHeader = authHelpers.fakeTempAuthHeader(user.id);
            const messages = await addWebSocket(lobbyUserHeader);

            const responseObj = await deleteLobby();
            await new Promise(resolve => {
                setTimeout(resolve, 100);
            });

            expectStatusCode(204, responseObj);
            const lobbyDeleteMessage = messages.find(({ event }) => event === 'lobbyDelete');
            gameSchema.checkGameDeleteEvent(lobbyDeleteMessage);
        });

        function deleteLobby(args = {}){
            const header = 'header' in args ? args.header : defaultHeader;
            const gameID = 'gameID' in args ? args.gameID : game.id;
            return requests.delete(`api/games/${gameID}`, header);
        }
    });

    it('Gets game data for moderators in a lobby with no players', async() => {
        const gameObj = await quasiGame.create();
        const moderatorID = getModeratorID(gameObj);
        const userHeader = authHelpers.fakeTempAuthHeader(moderatorID);
        await moderatorService.create(moderatorID);

        const responseObj = await requests.get(`api/games?user_id=${moderatorID}`, userHeader);

        expect(responseObj.statusCode).to.be.equal(200);
        expect(Object.keys(responseObj.json.response).length).to.not.be.equal(0);
    });

    describe('Get games', () => {
        it('Will add names to game users', async() => {
            await quasiGame.create();

            const responseObj = await getLobbies();

            const lobbies = responseObj.json.response;
            expectStatusCode(200, responseObj);
            expect(lobbies.length).to.be.equal(1);
            lobbies.forEach(gameSchema.checkNotStarted);
        });

        function getLobbies(){
            return requests.get('api/games');
        }
    });

    describe('Get game by id', () => {
        it('Does not get games by id if the id is not a number', async() => {
            const user = await quasiUser.createUser();
            const headers = authHelpers.fakeTempAuthHeader(user.id);
            const fakeGameID = 'ABCD';

            const serverResponse = await requests.get(`api/games/${fakeGameID}`, headers);

            expect(serverResponse.statusCode).to.be.equal(404);
        });

        it('Filters ended night data if ended night is before 2/3s', async() => {
            clock = sinon.useFakeTimers();
            const gameObj = await quasiGame.create({
                isStarted: true,
                playerCount: 7,
                modifiers: [{
                    name: 'NIGHT_LENGTH',
                    value: 60,
                }],
            });
            const nonHostID = gameObj.users.find(user => !user.isModerator).id;
            const moderatorID = getModeratorID(gameObj);
            const headers = authHelpers.fakeTempAuthHeader(moderatorID);
            await actionService.submitActionMessage(nonHostID, { message: 'end night' });

            advanceSeconds(39);
            const serverResponse = await requests.get(`api/games/${gameObj.id}`, headers);

            expect(serverResponse.statusCode).to.equal(200);
            expect(serverResponse.json.response.players.some(player => player.endedNight))
                .to.equal(false);
        });

        it('Does not filter ended night data if ended night is after 2/3s', async() => {
            clock = sinon.useFakeTimers();
            const gameObj = await quasiGame.create({
                isStarted: true,
                playerCount: 7,
                modifiers: [{
                    name: 'NIGHT_LENGTH',
                    value: 60,
                }],
            });
            const nonHostID = gameObj.users.find(user => !user.isModerator).id;
            const moderatorID = getModeratorID(gameObj);
            const headers = authHelpers.fakeTempAuthHeader(moderatorID);
            await actionService.submitActionMessage(nonHostID, { message: 'end night' });

            advanceSeconds(41);
            const serverResponse = await requests.get(`api/games/${gameObj.id}`, headers);

            expect(serverResponse.statusCode).to.equal(200);
            expect(serverResponse.json.response.players.some(player => player.endedNight))
                .to.equal(true);
            gameSchema.checkInGame(serverResponse.json.response);
        });
    });

    describe('Game start output', () => {
        it('Does not output a faction on dead player flips on HIDDEN_FLIP modifier', async() => {
            const setup = await quasiSetup.create({
                setupHiddenCount: 4,
                modifiers: [{ name: setupModifiers.DAY_START, value: true }],
            });
            let game = await quasiGame.create({
                setupID: setup.id,
                hostID: setup.ownerID,
                isStarted: false,
                playerCount: 4,
            });
            await setAllRolesToHiddenFlips(setup);
            game = await gameService.start(game.id);

            const goon = game.players.find(p => p.factionRole.name === 'Goon');
            const fodder = game.players.find(p => p !== goon);
            await Promise.all(game.players
                .filter(player => player !== fodder)
                .map(player => actionService.submitActionMessage(player.userID, {
                    message: `vote ${fodder.name}`,
                })));
            game = await gameService.getByID(game.id);

            const player = game.players.find(p => p.name === fodder.name);
            expect(!!player.flip.factionID).to.be.equal(false);
        });

        it('Does not output setup hiddens that are out of range when getting a game', async() => {
            const setup = await quasiSetup.create({ setupHiddenCount: 3 });
            const gameObj = await quasiGame.create({
                setupID: setup.id,
                hostID: setup.ownerID,
                isStarted: false,
                playerCount: 3,
            });
            const hiddenID = gameObj.setup.hiddens[0].id;
            const setupHidden = await setupHiddenService.add(gameObj.setup.ownerID, {
                hiddenID,
                isExposed: false,
                mustSpawn: false,
                minPlayerCount: 4,
                maxPlayerCount: MAX_PLAYER_COUNT,
            });

            const game = await gameService.start(gameObj.id);

            expect(game.setup.setupHiddens.map(sh => sh.id)).not.include(setupHidden.id);
        });
    });

    function advanceSeconds(minutes){
        clock.tick(minutes * 1000);
    }

    async function addWebSocket(headers){
        const response = await quasiWebSocket.connect(headers);
        ws = response.socket;
        return response.messages;
    }
});

function setAllRolesToHiddenFlips(setup){
    const factionRoles = util.flattenList(setup.factions.map(faction => faction.factionRoles));
    const flipPromises = factionRoles.map(factionRole => factionRoleModifierService.create(
        setup.ownerID, factionRole.id, {
            name: roleModifiers.HIDDEN_FLIP,
            value: true,
            minPlayerCount: 0,
            maxPlayerCount: MAX_PLAYER_COUNT,
        },
    ));
    return Promise.all(flipPromises);
}
