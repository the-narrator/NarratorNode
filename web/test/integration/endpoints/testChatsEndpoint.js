require('../../init');
const { expect } = require('chai');
const { expectStatusCode } = require('../../assertions');
const authHelpers = require('../../authHelpers');

const helpers = require('../../../utils/helpers');

const { MAX_LOBBY_CHAT_COUNT } = require('../../../utils/constants');

const chatService = require('../../../services/chatService');
const gameService = require('../../../services/gameService');

const lobbyChatRepo = require('../../../repos/lobbyChatRepo');

const requests = require('../../requests');

const quasiGame = require('../../quasiModels/quasiGame');
const quasiUser = require('../../quasiModels/quasiUser');
const quasiWebSocket = require('../../quasiWebSocket');

const chatSchema = require('../../schemas/chatSchema');


describe('Chat Endpoints', () => {
    let game;
    let ws;
    beforeEach(async() => {
        game = await quasiGame.create();
    });

    afterEach(() => {
        if(ws)
            ws.close();
    });

    describe('Get game chats', () => {
        it('Will get game chats for a user', async() => {
            const headers = authHelpers.fakeTempAuthHeader(game.users[0].id);

            const serverResponse = await requests.get(`api/chats?gameID=${game.id}`, headers);

            expect(serverResponse.statusCode).to.be.equal(200);
        });

        it('Will fail gets gracefully with no gameID in query', async() => {
            const user = await quasiUser.createUser();
            const headers = authHelpers.fakeTempAuthHeader(user.id);

            const serverResponse = await requests.get('api/chats', headers);

            expect(serverResponse.statusCode).to.be.equal(404);
        });
    });

    describe('Get game lobby chat', () => {
        beforeEach(async() => {
            await sendLobbyMessage();
        });

        it('Will fetch game chats', async() => {
            // chat message created in beforeEach

            const serverResponse = await getLobbyMessages();

            expectStatusCode(200, serverResponse);
            serverResponse.json.response.forEach(chatSchema.check);
        });

        // it('Will fail gracefully if gameID does not exist', async() => {
        //     const gameID = game.id + 1;
        //
        //     const serverResponse = await getLobbyMessages({ gameID });
        //
        //     expectStatusCode(404, serverResponse);
        // });

        it('Will fail gracefully if gameID is not number', async() => {
            const gameID = 'NaN';

            const serverResponse = await getLobbyMessages({ gameID });

            expectStatusCode(404, serverResponse);
        });

        function getLobbyMessages(args = {}){
            const gameID = 'gameID' in args ? args.gameID : game.id;
            return requests.get(`api/chats/${gameID}/lobby`);
        }

        function sendLobbyMessage(){
            const text = helpers.getRandomString(5);
            return chatService.sendLobbyMessage(game.id, game.setup.ownerID, { text });
        }
    });

    describe('send lobby message', () => {
        it('Will send a message, saving it to the database', async() => {
            const prevChatMessageLength = (await lobbyChatRepo.get(game.id)).length;

            const serverResponse = await sendLobbyMessage();
            const newChatMessageLength = (await lobbyChatRepo.get(game.id)).length;

            expectStatusCode(200, serverResponse);
            expect(prevChatMessageLength + 1).to.be.equal(newChatMessageLength);
            chatSchema.check(serverResponse.json.response);
        });

        it('Will push new messages to other listening users', async() => {
            [game] = await quasiGame.addPlayersToGame(game.joinID, 1);
            const user2ID = game.users.find(user => user.id !== game.setup.ownerID).id;
            const user2Headers = authHelpers.fakeTempAuthHeader(user2ID);
            const messages = await addWebSocket(user2Headers);

            const serverResponse = await sendLobbyMessage();

            expectStatusCode(200, serverResponse);
            const lobbyChatMessage = messages.find(m => m.event === 'lobbyChatMessageEvent');
            chatSchema.check(lobbyChatMessage);
        });

        it('Will fail requests if no auth is given', async() => {
            const headers = undefined;

            const serverResponse = await sendLobbyMessage({ headers });

            expectStatusCode(403, serverResponse);
        });

        it('Will reject gracefully if no text', async() => {
            const text = undefined;

            const serverResponse = await sendLobbyMessage({ text });

            expectStatusCode(422, serverResponse);
        });

        it('Will reject gracefully if text is empty', async() => {
            const text = '';

            const serverResponse = await sendLobbyMessage({ text });

            expectStatusCode(422, serverResponse);
        });

        it('Will reject gracefully if text is too long', async() => {
            const text = helpers.getRandomString(MAX_LOBBY_CHAT_COUNT + 1);

            const serverResponse = await sendLobbyMessage({ text });

            expectStatusCode(422, serverResponse);
        });

        it('Will reject gracefully if gameID is not a number', async() => {
            const gameID = 'abc123';

            const serverResponse = await sendLobbyMessage({ gameID });

            expectStatusCode(404, serverResponse);
        });

        it('Will reject gracefully if gameID does not exist', async() => {
            const gameID = game.id + 1;

            const serverResponse = await sendLobbyMessage({ gameID });

            expectStatusCode(404, serverResponse);
        });

        it('Will delete game messages if game gets deleted', async() => {
            await sendLobbyMessage();
            let chatMessages = await lobbyChatRepo.get(game.id);
            expect(chatMessages.length).to.equal(1);

            await gameService.deleteGame(game.id);

            chatMessages = await lobbyChatRepo.get(game.id);
            expect(chatMessages.length).to.equal(0);
        });

        async function sendLobbyMessage(args = {}){
            const gameID = 'gameID' in args ? args.gameID : game.id;
            const text = 'text' in args ? args.text : 'someText';
            const payload = {
                text,
            };
            const hostID = game.users.find(user => user.id === game.setup.ownerID).id;
            const headers = 'headers' in args
                ? args.headers
                : authHelpers.fakeTempAuthHeader(hostID);
            return requests.post(`api/chats/${gameID}/lobby`, payload, headers);
        }
    });

    async function addWebSocket(headers){
        const response = await quasiWebSocket.connect(headers);
        ws = response.socket;
        return response.messages;
    }
});
