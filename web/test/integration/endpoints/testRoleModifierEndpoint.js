require('../../init');
const { expect } = require('chai');

const authHelpers = require('../../authHelpers');
const requests = require('../../requests');

const roleService = require('../../../services/setup/roleService');
const setupService = require('../../../services/setup/setupService');

const quasiGame = require('../../quasiModels/quasiGame');
const quasiSetup = require('../../quasiModels/quasiSetup');
const quasiUser = require('../../quasiModels/quasiUser');

const testControllerHelpers = require('../../testControllerHelpers');

const { FEATURED_KEY, FEATURED_PRIORITY } = require('../../fakeConstants');

const { MAX_PLAYER_COUNT } = require('../../../utils/constants');


describe('Role Modifier Endpoint', async() => {
    let setup;
    let headers;
    beforeEach(async() => {
        setup = await quasiSetup.create();
        headers = authHelpers.fakeTempAuthHeader(setup.ownerID);
        await quasiGame.create({ setupID: setup.id, hostID: setup.ownerID });
        const [role] = setup.roles;
        await roleService.createModifier(setup.ownerID, role.id, {
            name: 'AUTO_VEST',
            value: 30,
            minPlayerCount: 0,
            maxPlayerCount: MAX_PLAYER_COUNT,
        });
        setup = await setupService.getByID(setup.id);
    });

    it('Will update the role\'s modifier (boolean)', async() => {
        const [role] = setup.roles;
        const modifierArgs = {
            name: 'UNCONVERTABLE',
            value: true,
            minPlayerCount: 0,
            maxPlayerCount: MAX_PLAYER_COUNT,
        };
        await roleService.createModifier(setup.ownerID, role.id, modifierArgs);
        const args = {
            name: modifierArgs.name,
            value: !modifierArgs.value,
            roleID: role.id,
        };

        const responseObj = await createRoleModfier(args);
        setup = await setupService.getByID(setup.id);

        expect(responseObj.statusCode).to.be.equal(200);
        const newModifier = getNewModifier(role.id, modifierArgs);
        expect(newModifier.value).to.be.equal(args.value);
        expect(newModifier.value).to.be.equal(responseObj.json.response.value);
    });

    it('Will update the role\'s modifier (integer)', async() => {
        const [role] = setup.roles;
        const modifierArgs = {
            name: 'AUTO_VEST',
            value: 30,
            minPlayerCount: 0,
            maxPlayerCount: MAX_PLAYER_COUNT,
        };
        await roleService.createModifier(setup.ownerID, role.id, modifierArgs);
        const args = {
            name: modifierArgs.name,
            value: modifierArgs.value + 1,
            roleID: role.id,
        };

        const responseObj = await createRoleModfier(args);
        setup = await setupService.getByID(setup.id);

        expect(responseObj.statusCode).to.be.equal(200);
        const newModifier = getNewModifier(role.id, modifierArgs);
        expect(newModifier.value).to.be.equal(args.value);
        expect(newModifier.value).to.be.equal(responseObj.json.response.value);
    });

    it('Will insert more than one int modifier if player counts differ', async() => {
        const [role] = setup.roles;
        const modifierArgs = {
            name: 'AUTO_VEST',
            value: 30,
            minPlayerCount: 0,
            maxPlayerCount: MAX_PLAYER_COUNT,
        };
        await roleService.createModifier(setup.ownerID, role.id, modifierArgs);
        const args = {
            name: modifierArgs.name,
            value: modifierArgs.value + 1,
            roleID: role.id,
            minPlayerCount: modifierArgs.minPlayerCount + 1,
        };

        const responseObj = await createRoleModfier(args);
        setup = await setupService.getByID(setup.id);

        expect(responseObj.statusCode).to.be.equal(200);
        const modifiers = setup.roles.find(r => r.id === role.id).modifiers
            .filter(modifier => modifier.name === args.name);
        expect(modifiers.length).to.be.equal(2);
    });

    it('Will insert more than one bool modifier if player counts differ', async() => {
        const [role] = setup.roles;
        const modifierArgs = {
            name: 'UNCONVERTABLE',
            value: true,
            minPlayerCount: 0,
            maxPlayerCount: MAX_PLAYER_COUNT,
        };
        await roleService.createModifier(setup.ownerID, role.id, modifierArgs);
        const args = {
            name: modifierArgs.name,
            value: !modifierArgs.value,
            roleID: role.id,
            maxPlayerCount: modifierArgs.maxPlayerCount - 1,
        };

        const responseObj = await createRoleModfier(args);
        setup = await setupService.getByID(setup.id);

        expect(responseObj.statusCode).to.be.equal(200);
        const modifiers = setup.roles.find(r => r.id === role.id).modifiers
            .filter(modifier => modifier.name === args.name);
        expect(modifiers.length).to.be.equal(2);
    });

    it('Will not allow updates if the user doesn\'t own the setup', async() => {
        const user = await quasiUser.createUser();
        const otherHeaders = authHelpers.fakeTempAuthHeader(user.id);
        const { role, prevModifier } = getRoleModifierArguments('boolean');
        const args = {
            name: prevModifier.name,
            value: !prevModifier.value,
            roleID: role.id,
        };

        const responseObj = await createRoleModfier({ ...args, headers: otherHeaders });
        setup = await setupService.getByID(setup.id);

        expect(responseObj.statusCode).to.be.equal(422);
        const newModifier = getNewModifier(role.id, prevModifier);
        expect(newModifier.value).to.be.equal(prevModifier.value);
    });

    it('Will gracefully fail when the role doesn\'t exist', async() => {
        const { role, prevModifier } = getRoleModifierArguments('number');
        const roleID = role.id * 10;
        const args = {
            name: prevModifier.name,
            value: prevModifier.value + 1,
            roleID,
        };

        const responseObj = await createRoleModfier(args);

        expect(responseObj.statusCode).to.be.equal(422);
    });

    it('Will gracefully fail when the modifier name is wrong', async() => {
        const { role, prevModifier } = getRoleModifierArguments('boolean');
        const args = {
            name: prevModifier.name + prevModifier.name,
            value: !prevModifier.value,
            roleID: role.id,
        };

        const responseObj = await createRoleModfier(args);

        expect(responseObj.statusCode).to.be.equal(422);
    });

    it('Will gracefully fail when the request is missing the value', async() => {
        const { role, prevModifier } = getRoleModifierArguments('boolean');
        const args = {
            name: prevModifier.name,
            roleID: role.id,
        };

        const responseObj = await createRoleModfier(args);

        expect(responseObj.statusCode).to.be.equal(422);
    });

    it('Will gracefully fail when the request is missing the name', async() => {
        const { role, prevModifier } = getRoleModifierArguments('boolean');
        const args = {
            value: !prevModifier.value,
            roleID: role.id,
        };

        const responseObj = await createRoleModfier(args);

        expect(responseObj.statusCode).to.be.equal(422);
    });

    it('Will gracefully fail when the request is missing the minPlayerCount', async() => {
        const { role, prevModifier } = getRoleModifierArguments('boolean');
        const args = {
            value: !prevModifier.value,
            roleID: role.id,
            name: prevModifier.name,
            minPlayerCount: undefined,
        };

        const responseObj = await createRoleModfier(args);

        expect(responseObj.statusCode).to.be.equal(422);
    });

    it('Will gracefully fail when the request is missing the maxPlayerCount', async() => {
        const { role, prevModifier } = getRoleModifierArguments('boolean');
        const args = {
            value: !prevModifier.value,
            roleID: role.id,
            name: prevModifier.name,
            maxPlayerCount: undefined,
        };

        const responseObj = await createRoleModfier(args);

        expect(responseObj.statusCode).to.be.equal(422);
    });

    it('Will gracefully fail when the request is supposed to be boolean', async() => {
        const { role, prevModifier } = getRoleModifierArguments('boolean');
        const args = {
            name: prevModifier.name,
            value: 300,
            roleID: role.id,
        };

        const responseObj = await createRoleModfier(args);

        expect(responseObj.statusCode).to.be.equal(422);
    });

    it('Will gracefully fail when the request is supposed to be an integer', async() => {
        const { role, prevModifier } = getRoleModifierArguments('number');
        const args = {
            name: prevModifier.name,
            value: true,
            roleID: role.id,
        };

        const responseObj = await createRoleModfier(args);

        expect(responseObj.statusCode).to.be.equal(422);
    });

    it('Will not allow updates if the setup is frozen', async() => {
        await setupService.setPriority(setup.id, FEATURED_PRIORITY, FEATURED_KEY);
        const { role, prevModifier } = getRoleModifierArguments('boolean');
        const args = {
            name: prevModifier.name,
            value: !prevModifier.value,
            roleID: role.id,
        };

        const responseObj = await createRoleModfier(args);

        testControllerHelpers.assertUneditableSetupErrors(responseObj);
    });

    function createRoleModfier(input = {}){
        const args = {
            name: input.name,
            value: input.value,
            minPlayerCount: 'minPlayerCount' in input ? input.minPlayerCount : 0,
            maxPlayerCount: 'maxPlayerCount' in input ? input.maxPlayerCount : MAX_PLAYER_COUNT,
        };
        return requests.post(`api/roles/${input.roleID}/modifiers`,
            args, input.headers || headers);
    }

    function getRoleModifierArguments(typeString){
        let returnValue;
        setup.roles.forEach(role => {
            role.modifiers.forEach(modifier => {
                if(typeof(modifier.value) === typeString) // eslint-disable-line valid-typeof
                    returnValue = {
                        role,
                        prevModifier: modifier,
                    };
            });
        });
        return returnValue;
    }

    function getNewModifier(roleID, prevModifier){
        let returnValue;
        setup.roles.forEach(role => {
            role.modifiers.forEach(modifier => {
                if(modifier.name === prevModifier.name && role.id === roleID)
                    returnValue = modifier;
            });
        });
        return returnValue;
    }
});
