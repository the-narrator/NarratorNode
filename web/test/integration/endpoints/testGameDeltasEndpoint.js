require('../../init');

const { expect } = require('chai');
const sinon = require('sinon');
const { expectStatusCode } = require('../../assertions');

const requests = require('../../requests');

const helpers = require('../../../utils/helpers');

const gameService = require('../../../services/gameService');

const userIntegrationsRepo = require('../../../repos/userIntegrationsRepo');

const userTypes = require('../../../models/enums/userTypes');

const sc2mafiaClient = require('../../../channels/sc2mafia/sc2mafiaClient');

const quasiGame = require('../../quasiModels/quasiGame');
const quasiUser = require('../../quasiModels/quasiUser');
const fakeConstants = require('../../fakeConstants');


describe('Game Deltas', async() => {
    let findGameDeltasStub;
    let postGameStub;
    let serviceStub;

    beforeEach(() => {
        enableSc2Integration();
    });

    afterEach(() => {
        findGameDeltasStub.restore();
        if(postGameStub)
            postGameStub.restore();
        if(serviceStub)
            serviceStub.restore();
    });

    it('Will post a new game when a new game request is detected', async() => {
        const dayLength = 500;
        const nightLength = 400;
        findGameDeltasStub = sinon.stub(sc2mafiaClient, 'findGameDeltas');
        findGameDeltasStub.onCall(0).returns([{
            name: fakeConstants.PLAYER_NAME,
            externalUserID: fakeConstants.EXTERNAL_USER_ID,
            integrationType: userTypes.SC2MAFIA,
            gameModifiers: {
                DAY_LENGTH_START: dayLength,
                NIGHT_LENGTH: nightLength,
            },
        }]);
        postGameStub = sinon.stub(sc2mafiaClient, 'postGame');
        postGameStub.onCall(0).returns({
            setupThreadID: fakeConstants.SC2MAFIA_SETUP_THREAD_ID,
            signupThreadID: fakeConstants.SC2MAFIA_SIGNUP_THREAD_ID,
        });

        const responseObj = await requests.post('api/game_deltas');
        const gameResponse = responseObj.json.response[0];
        const gameObj = await gameService.getByLobbyID(gameResponse.joinID);
        const externalInfo = gameResponse.externalInfo;

        expect(responseObj.statusCode).to.be.equal(200);
        expect(responseObj.json.response.length).to.be.equal(1);
        expect(externalInfo.setupThreadID).to.be.equal(fakeConstants.SC2MAFIA_SETUP_THREAD_ID);
        expect(externalInfo.signupThreadID).to.be.equal(fakeConstants.SC2MAFIA_SIGNUP_THREAD_ID);
        expect(postGameStub.getCall(0).args.length).to.be.equal(3);
        expect(gameObj.modifiers.DAY_LENGTH_START.value).to.be.equal(dayLength);
        expect(gameObj.modifiers.NIGHT_LENGTH.value).to.be.equal(nightLength);
    });

    it('Will allow new games even if the user has hosted before', async() => {
        findGameDeltasStub = sinon.stub(sc2mafiaClient, 'findGameDeltas');
        findGameDeltasStub.onCall(0).returns([{
            name: fakeConstants.PLAYER_NAME,
            externalUserID: fakeConstants.EXTERNAL_USER_ID,
            integrationType: userTypes.SC2MAFIA,
        }]);
        postGameStub = sinon.stub(sc2mafiaClient, 'postGame');
        const user = await quasiUser.createUser();
        await userIntegrationsRepo.add(user.id, fakeConstants.EXTERNAL_USER_ID, userTypes.SC2MAFIA);

        const responseObj = await requests.post('api/game_deltas');

        expect(responseObj.statusCode).to.be.equal(200);
        expect(responseObj.json.response.length).to.be.equal(1);
    });

    it('Will not allow hosts to have duplicate games in other integrations', async() => {
        const game = await quasiGame.create();
        const userID = game.users[0].id;
        await userIntegrationsRepo.add(userID, fakeConstants.EXTERNAL_USER_ID, userTypes.SC2MAFIA);
        findGameDeltasStub = sinon.stub(sc2mafiaClient, 'findGameDeltas');
        findGameDeltasStub.onCall(0).returns([{
            name: fakeConstants.PLAYER_NAME,
            externalUserID: fakeConstants.EXTERNAL_USER_ID,
        }]);

        const responseObj = await requests.post('api/game_deltas');

        expectStatusCode(200, responseObj);
        expect(responseObj.json.response.length).to.be.equal(0);
    });

    function enableSc2Integration(){
        serviceStub = sinon.stub(helpers, 'serviceEnabled');
        serviceStub.onCall(0).returns(true);
    }
});
