require('../../init');
const { expect } = require('chai');
const { expectStatusCode } = require('../../assertions');

const requests = require('../../requests');

const baseAbilitySchema = require('../../schemas/baseAbilitySchema');


describe('Base Abilities endpoint', () => {
    it('Will fetch a list of available abilities', async() => {
        // no given

        const response = await getAvailableBaseAbilities();

        expectStatusCode(200, response);
        expect(response.json.response.length).to.not.be.equal(0);
        response.json.response.forEach(baseAbilitySchema.check);
    });

    function getAvailableBaseAbilities(){
        return requests.get('api/baseAbilities/');
    }
});
