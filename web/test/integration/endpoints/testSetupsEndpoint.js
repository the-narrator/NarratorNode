require('../../init');
const { expect } = require('chai');
const { expectStatusCode } = require('../../assertions');

const helpers = require('../../../utils/helpers');

const gameRepo = require('../../../repos/replayRepo');
const roleRepo = require('../../../repos/roleRepo');
const setupRepo = require('../../../repos/setupRepo');

const hiddenSpawnService = require('../../../services/setup/hiddenSpawnService');
const setupService = require('../../../services/setup/setupService');

const requests = require('../../requests');

const setupOverviewSchema = require('../../schemas/setupOverviewSchema');

const authHelpers = require('../../authHelpers');
const quasiGame = require('../../quasiModels/quasiGame');
const quasiSetup = require('../../quasiModels/quasiSetup');
const quasiUser = require('../../quasiModels/quasiUser');

const setupValidator = require('../../../validators/setupValidator');

const testHelpers = require('../../testHelpers');

const { MAX_PLAYER_COUNT } = require('../../../utils/constants');

const {
    FEATURED_KEY, FEATURED_PRIORITY, SETUP_NAME, SETUP_SLOGAN, SETUP_DESCRIPTION,
} = require('../../fakeConstants');


describe('Setups', () => {
    let baseHeaders;
    let setup;
    beforeEach(async() => {
        setup = await quasiSetup.create({ setupHiddenCount: 5 });
        baseHeaders = authHelpers.fakeTempAuthHeader(setup.ownerID);
    });

    it('Will get featured setups', async() => {
        const priority = helpers.getRandomNumber(25) + 25;
        await quasiSetup.create({
            priority,
            slogan: SETUP_SLOGAN,
            description: SETUP_DESCRIPTION,
        });

        const responseObj = await requests.get('api/setups/featured');
        const setups = responseObj.json.response;
        const setupWithPriority = setups.filter(s => s.priority === priority)[0];

        expect(responseObj.statusCode).to.be.equal(200);
        setupOverviewSchema.check(setupWithPriority);
        expect(setupWithPriority.slogan).to.be.equal(SETUP_SLOGAN);
        expect(setupWithPriority.description).to.be.equal(SETUP_DESCRIPTION);
    });

    describe('Fetching user setups', () => {
        it('Will get user setups', async() => {
            const userSetup = await quasiSetup.create();

            const responseObj = await requests.get(`api/setups?userID=${userSetup.ownerID}`);
            const setups = responseObj.json.response;

            expect(responseObj.statusCode).to.be.equal(200);
            expect(setups.length).to.be.equal(1);
            setupValidator.userSetupResponse(setups[0]);
            expect(userSetup.id).to.be.equal(setups[0].id);
        });

        it('Will gracefully fail user setup requests with a wrong userID type', async() => {
            // no given

            const responseObj = await requests.get('api/setups?userID=fakeGarbage');

            expect(responseObj.statusCode).to.be.equal(404);
        });

        it('Will gracefully fail user setup requests with no userID', async() => {
            // no given

            const responseObj = await requests.get('api/setups');

            expect(responseObj.statusCode).to.be.equal(404);
        });

        it('Will not return inactive setups', async() => {
            setup = await quasiSetup.create({
                isActive: false,
            });

            const responseObj = await requests.get(`api/setups?userID=${setup.ownerID}`);

            expect(responseObj.statusCode).to.be.equal(200);
            expect(responseObj.json.response.length).to.be.equal(0);
        });
    });

    describe('Get setup by id', () => {
        it('Will correctly give role ability modifier labels', async() => {
            // given in beforeEach

            const responseObj = await requests.get(`api/setups/${setup.id}`, baseHeaders);
            const roles = responseObj.json.response.roles;

            const architect = roles.find(role => role.name === 'Architect');
            const abilityModifiers = architect.abilities[0].modifiers;
            const backToBackModifier = abilityModifiers
                .find(modifier => modifier.name === 'BACK_TO_BACK');

            expect(backToBackModifier.label).to.be.equal(
                'May target the same people on consecutive days using "architect".',
            );
        });

        it('Will output the correct max player count', async() => {
            // given in beforeEach

            const responseObj = await requests.get(`api/setups/${setup.id}`, baseHeaders);

            expect(responseObj.json.response.setupHiddens.length).to.be.equal(5);
            expect(responseObj.json.response.maxPlayerCount).to.be.equal(5);
        });

        it('Will get a setup with a hidden that has no faction roles', async() => {
            await quasiGame.create({ setupID: setup.id, hostID: setup.ownerID });
            let [hidden] = setup.hiddens;
            await Promise.all(hidden.spawns
                .map(spawn => hiddenSpawnService.deleteSpawn(
                    setup.ownerID, spawn.id,
                )));

            const responseObj = await requests.get(`api/setups/${setup.id}`, baseHeaders);
            hidden = responseObj.json.response.hiddens.find(h => h.id === hidden.id);

            expect(responseObj.statusCode).to.be.equal(200);
            expect(hidden.spawns.length).to.be.equal(0);
        });

        it('Will get a setup with no setup hiddens', async() => {
            setup = await quasiSetup.create({ setupHiddenCount: 0 });
            for(let i = 0; i < 3; i++)
                await testHelpers.addSetupHiddenAny(setup, {
                    minPlayerCount: 4,
                    maxPlayerCount: MAX_PLAYER_COUNT,
                });

            const responseObj = await requests.get(`api/setups/${setup.id}`, baseHeaders);

            expect(responseObj.statusCode).to.be.equal(200);
            expect(responseObj.json.response.minPlayerCount).to.be.equal(3);
        });
    });

    describe('Create setup', () => {
        it('Will gracefully fail creates if isDefault is not a number', async() => {
            // Given created above

            const responseObj = await requests.post('api/setups', getSetupArgs({ isDefault: 4 }),
                baseHeaders);

            expect(responseObj.statusCode).to.be.equal(422);
        });

        it('Will create setup shells', async() => {
            // Given created above

            // eslint-disable-next-line max-len
            const responseObj = await requests.post('api/setups', getSetupArgs({ isDefault: false }), baseHeaders);
            const newSetup = responseObj.json.response;
            const [newSetupSchema, allSetups] = await Promise.all([
                setupRepo.getByID(newSetup.id),
                setupRepo.getAll(),
            ]);

            expect(responseObj.statusCode).to.be.equal(200);
            expect(newSetupSchema.id).to.be.equal(newSetup.id);
            expect(newSetupSchema.name).to.be.equal(SETUP_NAME);
            expect(newSetupSchema.description).to.be.equal(SETUP_DESCRIPTION);
            expect(newSetupSchema.slogan).to.be.equal(SETUP_SLOGAN);
            expect(allSetups.length).to.be.equal(2); // created one in the before each
            expect(newSetup.roles).to.deep.equal([]);
            expect(newSetup.factions).to.deep.equal([]);
            expect(newSetup.hiddens).to.deep.equal([]);
            expect(newSetup.setupHiddens).to.deep.equal([]);
        });

        it('Will create default setups', async() => {
            const responseObj = await requests.post('api/setups', getSetupArgs(), baseHeaders);
            const setupID = responseObj.json.response.id;
            const [newSetup, allSetups] = await Promise.all([
                setupRepo.getByID(setupID),
                setupRepo.getAll(),
            ]);

            expect(responseObj.statusCode).to.be.equal(200);
            expect(newSetup.id).to.be.equal(setupID);
            expect(newSetup.name).to.be.equal(SETUP_NAME);
            expect(newSetup.description).to.be.equal(SETUP_DESCRIPTION);
            expect(newSetup.slogan).to.be.equal(SETUP_SLOGAN);
            expect(allSetups.length).to.be.equal(2); // created one in the before each
        });

        it('Will gracefully fail if there is no setup name specified', async() => {
            const args = Object.assign({}, getSetupArgs(), { name: undefined });

            const responseObj = await requests.post('api/setups', args, baseHeaders);

            expect(responseObj.statusCode).to.be.equal(422);
        });

        it('Will not fail creates if there is no slogan specified', async() => {
            const args = Object.assign({}, getSetupArgs(), { slogan: undefined });

            const responseObj = await requests.post('api/setups', args, baseHeaders);

            expect(responseObj.statusCode).to.be.equal(200);
        });

        it('Will not fail creates if there is no description specified', async() => {
            const args = Object.assign({}, getSetupArgs(), { description: undefined });

            const responseObj = await requests.post('api/setups', args, baseHeaders);

            expect(responseObj.statusCode).to.be.equal(200);
        });

        it('Will gracefully fail if the setup name is too short', async() => {
            const args = Object.assign({}, getSetupArgs(), { name: '' });

            const responseObj = await requests.post('api/setups', args, baseHeaders);

            expect(responseObj.statusCode).to.be.equal(422);
        });

        it('Will gracefully fail if the slogan name is too long', async() => {
            const args = Object.assign({}, getSetupArgs(), { slogan: helpers.getRandomString(46) });

            const responseObj = await requests.post('api/setups', args, baseHeaders);

            expect(responseObj.statusCode).to.be.equal(422);
        });

        it('Will gracefully fail if the description name is too long', async() => {
            const args = Object.assign({}, getSetupArgs(),
                { description: helpers.getRandomString(513) });

            const responseObj = await requests.post('api/setups', args, baseHeaders);

            expect(responseObj.statusCode).to.be.equal(422);
        });

        it('Will gracefully fail if the setup name is too long', async() => {
            const args = Object.assign({}, getSetupArgs(), { name: helpers.getRandomString(26) });

            const responseObj = await requests.post('api/setups', args, baseHeaders);

            expect(responseObj.statusCode).to.be.equal(422);
        });
    });

    describe('Updating game setup', () => {
        it('Will return the new setup ID in the payload', async() => {
            const gameObj = await quasiGame.create({ setupID: setup.id, hostID: setup.ownerID });
            const setup2 = await quasiSetup.create({ ownerID: gameObj.setup.ownerID });
            const moderatorID = gameObj.users.find(user => user.isModerator).id;
            const headers = authHelpers.fakeTempAuthHeader(moderatorID);

            const responseObj = await requests.put('api/setups', {
                setupID: setup2.id,
                gameID: gameObj.id,
            },
            headers);

            expectStatusCode(200, responseObj);
            expect(responseObj.json.response.setup.id).to.be.equal(setup2.id);
        });

        it('Will not delete games when setting the same setup id', async() => {
            const gameObj = await quasiGame.create({ setupID: setup.id, hostID: setup.ownerID });

            const responseObj = await requests.put('api/setups', {
                setupID: gameObj.setup.id,
                gameID: gameObj.id,
            },
            baseHeaders);
            const dbGame = await gameRepo.getByID(gameObj.id);

            expect(responseObj.statusCode).to.be.equal(200);
            expect(dbGame).to.not.be.a('null');
        });

        it('Will gracefully fail when the game id is missing', async() => {
            await quasiGame.create();
            const newSetup = await quasiSetup.create();
            const args = { setupID: newSetup.id };

            const responseObj = await requests.put('api/setups', args, baseHeaders);

            expect(responseObj.statusCode).to.be.equal(422);
        });

        it('Will gracefully fail when the setup id is not a number', async() => {
            const { id: gameID } = await quasiGame.create(
                { setupID: setup.id, hostID: setup.ownerID },
            );
            const args = { setupID: 'notNumber', gameID };

            const responseObj = await requests.put('api/setups', args, baseHeaders);

            expect(responseObj.statusCode).to.be.equal(422);
        });

        it('Will gracefully fail when the requesting user is not a moderator', async() => {
            const gameObj = await quasiGame.create({
                setupID: setup.id,
                hostID: setup.ownerID,
                playerCount: 2,
            });
            const setup2 = await quasiSetup.create({ ownerID: gameObj.setup.ownerID });
            const userID = gameObj.users.find(user => !user.isModerator).id;
            const headers = authHelpers.fakeTempAuthHeader(userID);

            const responseObj = await requests.put('api/setups', {
                setupID: setup2.id,
                gameID: gameObj.id,
            },
            headers);

            expectStatusCode(403, responseObj);
        });
    });

    describe('Delete setups', () => {
        it('Will delete user setups', async() => {
            const roleID = setup.roles[0].id;

            const responseObj = await requests.delete(`api/setups/${setup.id}`, baseHeaders);
            expect(responseObj.statusCode).to.be.equal(204);

            try{
                await setupRepo.getByID(setup.id);
            }catch(err){
                expect(err.errors[0]).to.be.equal('No setup found with that id');
            }

            const role = await roleRepo.getByID(roleID);
            expect(role).to.be.a('null');
        });

        it('Will not delete setups if the non-mod user doesn\'t own the setup', async() => {
            const setupOwner = await quasiUser.createUser();
            const hacker = await quasiUser.createUser();
            const userSetup = await quasiSetup.create({ ownerID: setupOwner.id });
            const hackerHeaders = authHelpers.fakeTempAuthHeader(hacker.id);

            const responseObj = await requests.delete(`api/setups/${userSetup.id}`, hackerHeaders);
            setup = await setupRepo.getByID(userSetup.id);

            expect(responseObj.statusCode).to.be.equal(204);
            expect(setup).to.not.be.a('null');
        });

        it('Will delete setups even if mod doesn\'t own the setup', async() => {
            const modUser = await quasiUser.createMod();
            const userSetup = await quasiSetup.create();
            const modHeaders = authHelpers.fakeTempAuthHeader(modUser.id);

            const responseObj = await requests.delete(`api/setups/${userSetup.id}`, modHeaders);
            expect(responseObj.statusCode).to.be.equal(204);

            try{
                await setupRepo.getByID(userSetup.id);
            }catch(err){
                expect(err.errors[0]).to.be.equal('No setup found with that id');
            }
        });

        it('Will not delete setups with replays attached to it', async() => {
            await quasiGame.create({ hostID: setup.ownerID, setupID: setup.id });

            const responseObj = await requests.delete(`api/setups/${setup.id}`, baseHeaders);
            setup = await setupRepo.getByID(setup.id);

            expect(responseObj.statusCode).to.be.equal(204);
            expect(setup).to.not.be.a('null');
        });

        it('Will not delete setups that are featured', async() => {
            await setupService.setPriority(setup.id, FEATURED_PRIORITY, FEATURED_KEY);

            const responseObj = await requests.delete(`api/setups/${setup.id}`, baseHeaders);
            setup = await setupRepo.getByID(setup.id);

            expect(responseObj.statusCode).to.be.equal(204);
            expect(setup).to.not.be.a('null');
        });
    });

    describe('Setup cloning', () => {
        let user;
        beforeEach(async() => {
            user = await quasiUser.createUser();
        });

        it('Will clone a setup', async() => {
            // given in beforeEach

            const responseObj = await cloneSetup();

            expectStatusCode(200, responseObj);
            const responseObjStr = JSON.stringify(responseObj.json.response);
            const setupStr = JSON.stringify(setup);
            expect(responseObjStr.length).to.be.equal(setupStr.length);
            expect(responseObj.json.response.ownerID).to.be.equal(user.id);
            expect(responseObj.json.response.roles[0].id).to.not.be.equal(setup.roles[0].id);
            expect(responseObj.json.response.id).to.not.be.equal(setup.id);
        });

        it('Will require headers', async() => {
            // given in beforeEach

            const responseObj = await cloneSetup({ headers: undefined });

            expectStatusCode(403, responseObj);
        });

        it('Will handle when setupID does not exist', async() => {
            // given in beforeEach

            const responseObj = await cloneSetup({ setupID: 0 });

            expectStatusCode(422, responseObj);
        });

        it('Will handle when setupID does not exist', async() => {
            // given in beforeEach

            const responseObj = await cloneSetup({ setupID: 'ABC' });

            expectStatusCode(404, responseObj);
        });

        function cloneSetup(args = {}){
            const setupID = 'setupID' in args ? args.setupID : setup.id;
            const headers = 'headers' in args
                ? args.headers
                : authHelpers.fakeTempAuthHeader(user.id);
            return requests.post(`api/setups/${setupID}/clone`, null, headers);
        }
    });

    it('Will not create new setups on game start', async() => {
        await quasiGame.create({
            playerCount: 4,
            setupID: setup.id,
            isStarted: true,
        });

        const setups = await setupRepo.getAll();

        expect(setups.length).to.be.equal(1);
    });
});

function getSetupArgs(args = {}){
    return {
        description: SETUP_DESCRIPTION,
        name: SETUP_NAME,
        slogan: SETUP_SLOGAN,
        isDefault: Object.keys(args).includes('isDefault') ? args.isDefault : true,
    };
}
