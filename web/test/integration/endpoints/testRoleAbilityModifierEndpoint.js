require('../../init');
const { expect } = require('chai');

const authHelpers = require('../../authHelpers');
const requests = require('../../requests');

const gameService = require('../../../services/gameService');
const setupService = require('../../../services/setup/setupService');

const abilityModifierSchema = require('../../schemas/abilityModifierSchema');

const quasiGame = require('../../quasiModels/quasiGame');
const quasiSetup = require('../../quasiModels/quasiSetup');
const quasiUser = require('../../quasiModels/quasiUser');

const testControllerHelpers = require('../../testControllerHelpers');

const { FEATURED_KEY, FEATURED_PRIORITY } = require('../../fakeConstants');

const { MAX_PLAYER_COUNT } = require('../../../utils/constants');


describe('Role Ability Modifier Endpoint', () => {
    let gameID;
    let headers;
    let modifierBool;
    // let modifierString;
    let modifierInt;
    let setup;
    beforeEach(async() => {
        setup = await quasiSetup.create();
        const gameObject = await quasiGame.create({ setupID: setup.id, hostID: setup.ownerID });
        gameID = gameObject.id;
        headers = authHelpers.fakeTempAuthHeader(setup.ownerID);
        modifierBool = getRoleAbilityModifierArguments(gameObject, 'boolean');
        modifierBool.newValue = !modifierBool.oldValue;
        modifierInt = getRoleAbilityModifierArguments(gameObject, 'number');
        modifierInt.newValue = modifierInt.oldValue + 2; // want to get above the unlimited -1 value
        // modifierString = getRoleAbilityModifierArguments(gameObject, 'string');
        // modifierString.newValue = `${modifierString.oldValue}2`;
    });

    it('Will update the role ability\'s modifier (boolean)', async() => {
        // given setup in before each

        const responseObj = await upsertRequest({
            modifierName: modifierBool.name,
            modifierValue: modifierBool.newValue,
        });
        const gameObject = await gameService.getByID(gameID);

        expect(responseObj.statusCode).to.be.equal(200);
        const newModifier = getUpdatedModifier(gameObject, modifierBool.roleID,
            modifierBool.abilityID, modifierBool.name);
        expect(newModifier.value).to.be.equal(modifierBool.newValue);
        expect(newModifier.value).to.be.equal(responseObj.json.response.value);
    });

    // it('Will update the role ability\'s modifier (string)', async() => {
    //     // given setup in before each
    //
    //     const responseObj = await upsertRequest({
    //         modifierName: modifierString.name,
    //         modifierValue: modifierString.newValue,
    //         abilityID: modifierString.abilityID,
    //         roleID: modifierString.roleID,
    //     });
    //     const gameObject = await gameService.getByID(gameID);
    //
    //     expect(responseObj.statusCode).to.be.equal(200);
    //     const newModifier = getUpdatedModifier(gameObject, modifierString.roleID,
    //         modifierString.abilityID, modifierString.name);
    //     expect(newModifier.value).to.be.equal(modifierString.newValue);
    //     expect(newModifier.value).to.be.equal(responseObj.json.response.value);
    // });

    it('Will always output a default value', async() => {
        const args = {
            modifierName: modifierBool.name,
            modifierValue: modifierBool.newValue,
            minPlayerCount: 1,
        };

        const responseObj = await upsertRequest(args);
        setup = await setupService.getByID(setup.id);

        expect(responseObj.statusCode).to.be.equal(200);
        const role = setup.roles.find(r => r.id === modifierBool.roleID);
        const ability = role.abilities.find(a => a.id === modifierBool.abilityID);
        const defaultModifier = ability.modifiers
            .find(modifier => modifier.name === args.modifierName
            && modifier.minPlayerCount === 0 && modifier.maxPlayerCount === MAX_PLAYER_COUNT);
        expect(typeof(defaultModifier)).to.eq('object');
    });

    it('Will insert more than one bool ability modifier if player counts differ', async() => {
        const args = {
            modifierName: modifierBool.name,
            modifierValue: modifierBool.newValue,
        };

        await upsertRequest(args);
        const responseObj = await upsertRequest({
            ...args,
            minPlayerCount: 1,
        });
        setup = await setupService.getByID(setup.id);

        expect(responseObj.statusCode).to.be.equal(200);
        const role = setup.roles.find(r => r.id === modifierBool.roleID);
        const ability = role.abilities.find(a => a.id === modifierBool.abilityID);
        const modifiers = ability.modifiers.filter(modifier => modifier.name === args.modifierName);
        expect(modifiers.length).to.be.equal(2);
        modifiers.forEach(abilityModifierSchema.check);
    });

    it('Will update the role ability\'s modifier (integer)', async() => {
        // given setup in before each

        const responseObj = await upsertRequest({
            modifierName: modifierInt.name,
            modifierValue: modifierInt.newValue,
            abilityID: modifierInt.abilityID,
            roleID: modifierInt.roleID,
        });
        const gameObject = await gameService.getByID(gameID);

        expect(responseObj.statusCode).to.be.equal(200);
        const newModifier = getUpdatedModifier(gameObject, modifierInt.roleID,
            modifierInt.abilityID, modifierInt.name);
        expect(newModifier.value).to.be.equal(modifierInt.newValue);
        expect(newModifier.value).to.be.equal(responseObj.json.response.value);
    });

    it('Will insert more than one int ability modifier if player counts differ', async() => {
        const args = {
            modifierName: modifierInt.name,
            modifierValue: modifierInt.newValue,
            abilityID: modifierInt.abilityID,
            roleID: modifierInt.roleID,
        };

        await upsertRequest(args);
        const responseObj = await upsertRequest({
            ...args,
            minPlayerCount: 1,
        });
        setup = await setupService.getByID(setup.id);

        expect(responseObj.statusCode).to.be.equal(200);
        const role = setup.roles.find(r => r.id === args.roleID);
        const ability = role.abilities.find(a => a.id === args.abilityID);
        const modifiers = ability.modifiers.filter(modifier => modifier.name === args.modifierName);
        expect(modifiers.length).to.be.equal(2);
    });

    it('Will not allow updates if the user doesn\'t own the setup', async() => {
        const user = await quasiUser.createUser();

        const responseObj = await upsertRequest({
            modifierName: modifierBool.name,
            modifierValue: modifierBool.newValue,
            headers: authHelpers.fakeTempAuthHeader(user.id),
        });
        const gameObject = await gameService.getByID(gameID);

        expect(responseObj.statusCode).to.be.equal(422);
        const newModifier = getUpdatedModifier(gameObject, modifierBool.roleID,
            modifierBool.abilityID, modifierBool.name);
        expect(newModifier.value).to.be.equal(modifierBool.oldValue);
    });

    it('Will gracefully fail when the roleID doesn\'t exist', async() => {
        // given setup in before each

        const responseObj = await upsertRequest({
            modifierName: modifierInt.name,
            modifierValue: modifierInt.newValue,
            roleID: modifierInt.roleID * 10,
        });

        expect(responseObj.statusCode).to.be.equal(422);
    });

    it('Will gracefully fail when the abilityID doesn\'t exist', async() => {
        // given setup in before each

        const responseObj = await upsertRequest({
            modifierName: modifierInt.name,
            modifierValue: modifierInt.newValue,
            abilityID: modifierInt.abilityID * 10,
        });

        expect(responseObj.statusCode).to.be.equal(422);
    });

    it('Will gracefully fail when the modifier name is wrong', async() => {
        // given setup in before each

        const responseObj = await upsertRequest({
            modifierName: modifierBool.name + modifierBool.name,
        });

        expect(responseObj.statusCode).to.be.equal(422);
    });

    it('Will gracefully fail when the request is missing the value', async() => {
        // given setup in before each

        const responseObj = await upsertRequest({
            modifierValue: undefined,
        });

        expect(responseObj.statusCode).to.be.equal(422);
    });

    it('Will gracefully fail when the request is missing the name', async() => {
        // given setup in before each

        const responseObj = await upsertRequest({
            modifierName: undefined,
        });

        expect(responseObj.statusCode).to.be.equal(422);
    });

    it('Will gracefully fail when the request is missing minPlayerCount', async() => {
        // given setup in before each

        const responseObj = await upsertRequest({
            minPlayerCount: undefined,
        });

        expect(responseObj.statusCode).to.be.equal(422);
    });

    it('Will gracefully fail when the request is missing maxPlayerCount', async() => {
        // given setup in before each

        const responseObj = await upsertRequest({
            maxPlayerCount: undefined,
        });

        expect(responseObj.statusCode).to.be.equal(422);
    });

    it('Will gracefully fail when the request is supposed to be boolean', async() => {
        // given setup in before each

        const responseObj = await upsertRequest({
            modifierName: modifierInt.name,
            modifierValue: modifierBool.newValue,
        });

        expect(responseObj.statusCode).to.be.equal(422);
    });

    it('Will gracefully fail when the request is supposed to be an integer', async() => {
        // given setup in before each

        const responseObj = await upsertRequest({
            modifierName: modifierBool.name,
            modifierValue: modifierInt.newValue,
        });

        expect(responseObj.statusCode).to.be.equal(422);
    });

    it('Will not allow edits to featured setups', async() => {
        await setupService.setPriority(setup.id, FEATURED_PRIORITY, FEATURED_KEY);

        const responseObj = await upsertRequest();

        testControllerHelpers.assertUneditableSetupErrors(responseObj);
    });

    function upsertRequest(args = {}){
        const inputRoleID = 'roleID' in args ? args.roleID : modifierBool.roleID;
        const inputAbilityID = 'abilityID' in args ? args.abilityID : modifierBool.abilityID;
        const payload = {
            name: 'modifierName' in args ? args.modifierName : modifierBool.name,
            value: 'modifierValue' in args ? args.modifierValue : modifierBool.newValue,
            minPlayerCount: 'minPlayerCount' in args ? args.minPlayerCount : 0,
            maxPlayerCount: 'maxPlayerCount' in args ? args.maxPlayerCount : MAX_PLAYER_COUNT,
        };
        return requests.post(`api/roles/${inputRoleID}/abilities/${inputAbilityID}/modifiers`,
            payload, args.headers || headers);
    }
});

function getRoleAbilityModifierArguments(gameObject, typeString){
    let returnValue;
    gameObject.setup.roles.forEach(role => {
        role.abilities.forEach(ability => {
            ability.modifiers.forEach(modifier => {
                if(typeof(modifier.value) === typeString && !returnValue) // eslint-disable-line valid-typeof
                    returnValue = {
                        roleID: role.id,
                        abilityID: ability.id,
                        name: modifier.name,
                        oldValue: modifier.value,
                    };
            });
        });
    });
    return returnValue;
}

function getUpdatedModifier(gameObject, roleID, abilityID, modifierName){
    let returnValue;
    gameObject.setup.roles.forEach(role => {
        if(roleID !== role.id)
            return;
        role.abilities.forEach(roleAbility => {
            if(abilityID !== roleAbility.id)
                return;
            roleAbility.modifiers.forEach(modifier => {
                if(modifier.name === modifierName)
                    returnValue = modifier;
            });
        });
    });
    return returnValue;
}
