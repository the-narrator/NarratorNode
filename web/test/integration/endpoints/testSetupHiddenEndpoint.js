require('../../init');
const { expect } = require('chai');
const sinon = require('sinon');

const authHelpers = require('../../authHelpers');
const requests = require('../../requests');

const setupHiddenRepo = require('../../../repos/setupHiddenRepo');

const eventService = require('../../../services/eventService');
const setupHiddenService = require('../../../services/setup/setupHiddenService');
const setupService = require('../../../services/setup/setupService');

const setupHiddenSchema = require('../../schemas/setupHiddenSchema');

const { MAX_PLAYER_COUNT } = require('../../../utils/constants');

const quasiGame = require('../../quasiModels/quasiGame');
const quasiSetup = require('../../quasiModels/quasiSetup');
const quasiUser = require('../../quasiModels/quasiUser');
const quasiWebSocket = require('../../quasiWebSocket');

const testControllerHelpers = require('../../testControllerHelpers');

const { FEATURED_PRIORITY, FEATURED_KEY } = require('../../fakeConstants');


describe('Setup Hidden Endpoint', async() => {
    let eventSetupChangeSpy;
    let setupGetByIDSpy;
    let ws;
    let headers;
    let setup;
    beforeEach(async() => {
        setup = await quasiSetup.create();
        await quasiGame.create({ setupID: setup.id, hostID: setup.ownerID });
        headers = authHelpers.fakeTempAuthHeader(setup.ownerID);
    });

    afterEach(() => {
        if(setupGetByIDSpy)
            setupGetByIDSpy.restore();
        if(eventSetupChangeSpy)
            eventSetupChangeSpy.restore();
        if(ws)
            ws.close();
    });

    it('Will add a setup hidden', async() => {
        const hiddenID = setup.hiddens[0].id;
        const messages = await addWebSocket(headers);
        eventSetupChangeSpy = sinon.spy(eventService, 'setupChange');
        setupGetByIDSpy = sinon.spy(setupService, 'getByID');

        const responseObj = await createRequest();
        setup = await setupService.getByID(setup.id);
        await setupGetByIDSpy.returnValues[0];
        await eventSetupChangeSpy.returnValues[eventSetupChangeSpy.returnValues.length - 1];
        await new Promise(resolve => {
            setTimeout(resolve, 1000);
        });

        expect(responseObj.statusCode).to.be.equal(200);
        expect(setup.setupHiddens.length).to.be.equal(1);
        expect(setup.minPlayerCount).to.be.equal(3);
        setupHiddenSchema.check(responseObj.json.response);
        const setupHiddenAddEventObj = messages
            .find(message => message.event === 'setupHiddenAdd'
                && message.setupHidden.hiddenID === hiddenID);
        setupHiddenSchema.checkAddEvent(setupHiddenAddEventObj);
    });

    it('Will return mustSpawn true on setup hidden adds', async() => {
        // Given in beforeEach

        const responseObj = await createRequest({ mustSpawn: true });

        expect(responseObj.statusCode).to.be.equal(200);
        expect(responseObj.json.response.mustSpawn).to.be.equal(true);
    });

    it('Will add an exposed setup hidden', async() => {
        // Given in beforeEach

        const responseObj = await createRequest({ isExposed: true });
        const setupHiddens = await setupHiddenRepo.getBySetupID(setup.id);

        expect(responseObj.statusCode).to.be.equal(200);
        const newSetupHidden = setupHiddens.find(sh => sh.id === responseObj.json.response.id);
        expect(newSetupHidden.isExposed).to.be.equal(true);
    });

    it('Will not add a setup hidden if the setup is featured', async() => {
        await setupService.setPriority(setup.id, FEATURED_PRIORITY, FEATURED_KEY);

        const responseObj = await createRequest();

        testControllerHelpers.assertUneditableSetupErrors(responseObj);
    });

    it('Will not add a setup hidden if the hidden doesn\'t belong to the user', async() => {
        const user2 = await quasiUser.createUser();
        headers = authHelpers.fakeTempAuthHeader(user2.id);

        const responseObj = await createRequest();
        setup = await setupService.getByID(setup.id);

        expect(responseObj.statusCode).to.be.equal(422);
        expect(setup.setupHiddens.length).to.be.equal(0);
    });

    it('Will not add a setup hidden if the request doesn\'t have isExposed', async() => {
        // given in before each

        const responseObj = await createRequest({ isExposed: undefined });
        setup = await setupService.getByID(setup.id);

        expect(responseObj.statusCode).to.be.equal(422);
        expect(setup.setupHiddens.length).to.be.equal(0);
    });

    it('Will not add a setup hidden if the request doesn\'t have a hiddenID', async() => {
        // Given in before each

        const responseObj = await createRequest({ hiddenID: undefined });
        setup = await setupService.getByID(setup.id);

        expect(responseObj.statusCode).to.be.equal(422);
        expect(setup.setupHiddens.length).to.be.equal(0);
    });

    it('Will gracefully fail setup hidden add requests without min player count', async() => {
        // Given in before each

        const responseObj = await createRequest({ minPlayerCount: undefined });

        expect(responseObj.statusCode).to.be.equal(422);
    });

    it('Will gracefully fail setup hidden add requests without a mustSpawn attribute', async() => {
        // Given in before each

        const responseObj = await createRequest({ mustSpawn: undefined });

        expect(responseObj.statusCode).to.be.equal(422);
    });

    it('Will gracefully fail setup hidden add requests without max player count', async() => {
        // Given in before each

        const responseObj = await createRequest({ maxPlayerCount: undefined });

        expect(responseObj.statusCode).to.be.equal(422);
    });

    it('Will gracefully fail setup hidden adds with a negative min player count', async() => {
        // Given in before each

        const responseObj = await createRequest({ minPlayerCount: -1 });

        expect(responseObj.statusCode).to.be.equal(422);
    });

    it('Will gracefully fail setup hidden adds with an out of bounds min player count', async() => {
        // Given in before each

        const responseObj = await createRequest({ minPlayerCount: MAX_PLAYER_COUNT + 1 });

        expect(responseObj.statusCode).to.be.equal(422);
    });

    it('Will gracefully fail setup hiddens with a too low max player count', async() => {
        // Given in before each

        const responseObj = await createRequest({ maxPlayerCount: MAX_PLAYER_COUNT + 1 });

        expect(responseObj.statusCode).to.be.equal(422);
    });

    it('Will gracefully fail setup hidden with a too high max player count', async() => {
        // Given in before each

        const responseObj = await createRequest({ maxPlayerCount: MAX_PLAYER_COUNT + 1 });

        expect(responseObj.statusCode).to.be.equal(422);
    });

    it('Will delete a setup hidden', async() => {
        const messages = await addWebSocket(headers);
        eventSetupChangeSpy = sinon.spy(eventService, 'setupChange');
        setupGetByIDSpy = sinon.spy(setupService, 'getByID');
        const setupHidden = await setupHiddenService.add(setup.ownerID, {
            isExposed: false,
            mustSpawn: false,
            hiddenID: setup.hiddens[0].id,
            minPlayerCount: 0,
            maxPlayerCount: MAX_PLAYER_COUNT,
        });

        const responseObj = await requests.delete(`api/setupHiddens/${setupHidden.id}`, headers);
        setup = await setupService.getByID(setup.id);
        await setupGetByIDSpy.returnValues[0];
        await eventSetupChangeSpy.returnValues[eventSetupChangeSpy.returnValues.length - 1];
        await new Promise(resolve => {
            setTimeout(resolve, 1000);
        });

        expect(responseObj.statusCode).to.be.equal(204);
        expect(setup.setupHiddens.length).to.be.equal(0);
        const hasSetupHiddenRemoveEvent = messages
            .some(message => message.event === 'setupHiddenRemove'
                && message.setupHiddenID === setupHidden.id);
        expect(hasSetupHiddenRemoveEvent).to.be.equal(true);
    });

    it('Will not delete a setup hidden from a featured setup', async() => {
        const setupHidden = await setupHiddenService.add(setup.ownerID, {
            isExposed: false,
            mustSpawn: false,
            hiddenID: setup.hiddens[0].id,
            minPlayerCount: 0,
            maxPlayerCount: MAX_PLAYER_COUNT,
        });
        await setupService.setPriority(setup.id, FEATURED_PRIORITY, FEATURED_KEY);

        const responseObj = await requests.delete(`api/setupHiddens/${setupHidden.id}`, headers);

        testControllerHelpers.assertUneditableSetupErrors(responseObj);
    });

    function createRequest(args = {}){
        const payload = {
            hiddenID: 'hiddenID' in args ? args.hiddenID : setup.hiddens[0].id,
            isExposed: 'isExposed' in args ? args.isExposed : false,
            mustSpawn: 'mustSpawn' in args ? args.mustSpawn : false,
            maxPlayerCount: 'maxPlayerCount' in args ? args.maxPlayerCount : MAX_PLAYER_COUNT,
            minPlayerCount: 'minPlayerCount' in args ? args.minPlayerCount : 0,
        };
        return requests.post('api/setupHiddens', payload, headers);
    }

    async function addWebSocket(socketHeaders){
        const response = await quasiWebSocket.connect(socketHeaders);
        ws = response.socket;
        return response.messages;
    }
});
