require('../../init');
const { expect } = require('chai');

const authHelpers = require('../../authHelpers');
const requests = require('../../requests');

const setupService = require('../../../services/setup/setupService');

const quasiGame = require('../../quasiModels/quasiGame');
const quasiSetup = require('../../quasiModels/quasiSetup');
const quasiUser = require('../../quasiModels/quasiUser');

const testControllerHelpers = require('../../testControllerHelpers');

const { FEATURED_KEY, FEATURED_PRIORITY } = require('../../fakeConstants');

const { MAX_PLAYER_COUNT } = require('../../../utils/constants');


describe('Faction Role Modifier Endpoint', async() => {
    let setup;
    let headers;
    beforeEach(async() => {
        setup = await quasiSetup.create();
        headers = authHelpers.fakeTempAuthHeader(setup.ownerID);
    });

    it('Will update the faction role\'s modifier (boolean)', async() => {
        await quasiGame.create({ setupID: setup.id, hostID: setup.ownerID });
        const { factionRole, prevModifier } = getFactionRoleModifierArguments('boolean');
        const args = {
            name: prevModifier.name,
            value: !prevModifier.value,
            factionRoleID: factionRole.id,
        };

        const responseObj = await upsertFactionRoleModifier(args);
        setup = await setupService.getByID(setup.id);

        expect(responseObj.statusCode).to.be.equal(200);
        const newModifier = getNewModifier(factionRole.id, prevModifier);
        expect(newModifier.value).to.be.equal(args.value);
        expect(newModifier.value).to.be.equal(responseObj.json.response.value);
    });

    it('Will insert more than one bool modifier if player counts differ', async() => {
        await quasiGame.create({ setupID: setup.id, hostID: setup.ownerID });
        const { factionRole, prevModifier } = getFactionRoleModifierArguments('boolean');
        const args = {
            name: prevModifier.name,
            value: !prevModifier.value,
            factionRoleID: factionRole.id,
        };

        await upsertFactionRoleModifier(args);
        const responseObj = await upsertFactionRoleModifier({
            ...args,
            minPlayerCount: 1, // which is not the default of 0
        });
        setup = await setupService.getByID(setup.id);

        expect(responseObj.statusCode).to.be.equal(200);
        const modifiedFactionRole = setup.factions.find(f => f.id === factionRole.factionID)
            .factionRoles.find(fr => fr.id === factionRole.id);
        const modifiers = modifiedFactionRole.modifiers
            .filter(modifier => modifier.name === args.name);
        expect(modifiers.length).to.be.equal(2);
    });

    it('Will update the faction role\'s modifier (integer)', async() => {
        await quasiGame.create({ setupID: setup.id, hostID: setup.ownerID });
        const { factionRole, prevModifier } = getFactionRoleModifierArguments('number');
        const args = {
            name: prevModifier.name,
            value: prevModifier.value + 1,
            factionRoleID: factionRole.id,
        };

        const responseObj = await upsertFactionRoleModifier(args);
        setup = await setupService.getByID(setup.id);

        expect(responseObj.statusCode).to.be.equal(200);
        const newModifier = getNewModifier(factionRole.id, prevModifier);
        expect(newModifier.value).to.be.equal(args.value);
        expect(newModifier.value).to.be.equal(responseObj.json.response.value);
    });

    it('Will insert more than one int modifier if player counts differ', async() => {
        await quasiGame.create({ setupID: setup.id, hostID: setup.ownerID });
        const { factionRole, prevModifier } = getFactionRoleModifierArguments('number');
        const args = {
            name: prevModifier.name,
            value: prevModifier.value + 1,
            factionRoleID: factionRole.id,
        };

        await upsertFactionRoleModifier(args);
        const responseObj = await upsertFactionRoleModifier({
            ...args,
            minPlayerCount: 1, // which is not the default of 0
        });
        setup = await setupService.getByID(setup.id);

        expect(responseObj.statusCode).to.be.equal(200);
        const modifiedFactionRole = setup.factions.find(f => f.id === factionRole.factionID)
            .factionRoles.find(fr => fr.id === factionRole.id);
        const modifiers = modifiedFactionRole.modifiers
            .filter(modifier => modifier.name === args.name);
        expect(modifiers.length).to.be.equal(2);
    });

    it('Will not allow updates if the user doesn\'t own the setup', async() => {
        const user = await quasiUser.createUser();
        const otherHeaders = authHelpers.fakeTempAuthHeader(user.id);
        await quasiGame.create({ setupID: setup.id, hostID: setup.ownerID });
        const { factionRole, prevModifier } = getFactionRoleModifierArguments('boolean');
        const args = {
            name: prevModifier.name,
            value: !prevModifier.value,
            factionRoleID: factionRole.id,
        };

        const responseObj = await upsertFactionRoleModifier({ ...args, headers: otherHeaders });
        setup = await setupService.getByID(setup.id);

        expect(responseObj.statusCode).to.be.equal(422);
        const newModifier = getNewModifier(factionRole.id, prevModifier);
        expect(newModifier.value).to.be.equal(prevModifier.value);
    });

    it('Will gracefully fail when the factionRoleID doesn\'t exist', async() => {
        await quasiGame.create({ setupID: setup.id, hostID: setup.ownerID });
        const { factionRole, prevModifier } = getFactionRoleModifierArguments('number');
        const factionRoleID = factionRole.id * 10;
        const args = {
            name: prevModifier.name,
            value: prevModifier.value + 1,
            factionRoleID,
        };

        const responseObj = await upsertFactionRoleModifier(args);

        expect(responseObj.statusCode).to.be.equal(422);
    });

    it('Will gracefully fail when the modifier name is wrong', async() => {
        await quasiGame.create({ setupID: setup.id, hostID: setup.ownerID });
        const { factionRole, prevModifier } = getFactionRoleModifierArguments('boolean');
        const args = {
            name: prevModifier.name + prevModifier.name,
            value: !prevModifier.value,
            factionRoleID: factionRole.id,
        };

        const responseObj = await upsertFactionRoleModifier(args);

        expect(responseObj.statusCode).to.be.equal(422);
    });

    it('Will gracefully fail when the request is missing the value', async() => {
        await quasiGame.create({ setupID: setup.id, hostID: setup.ownerID });
        const { factionRole, prevModifier } = getFactionRoleModifierArguments('boolean');
        const args = {
            name: prevModifier.name,
            factionRoleID: factionRole.id,
        };

        const responseObj = await upsertFactionRoleModifier(args);

        expect(responseObj.statusCode).to.be.equal(422);
    });

    it('Will gracefully fail when the request is missing the maxPlayerCount', async() => {
        await quasiGame.create({ setupID: setup.id, hostID: setup.ownerID });
        const { factionRole, prevModifier } = getFactionRoleModifierArguments('boolean');
        const args = {
            name: prevModifier.name,
            factionRoleID: factionRole.id,
            maxPlayerCount: undefined,
            value: !prevModifier.value,
        };

        const responseObj = await upsertFactionRoleModifier(args);

        expect(responseObj.statusCode).to.be.equal(422);
    });

    it('Will gracefully fail when the request is missing minPlayerCount', async() => {
        await quasiGame.create({ setupID: setup.id, hostID: setup.ownerID });
        const { factionRole, prevModifier } = getFactionRoleModifierArguments('boolean');
        const args = {
            name: prevModifier.name,
            factionRoleID: factionRole.id,
            minPlayerCount: undefined,
            value: !prevModifier.value,
        };

        const responseObj = await upsertFactionRoleModifier(args);

        expect(responseObj.statusCode).to.be.equal(422);
    });

    it('Will gracefully fail when the request is missing the name', async() => {
        await quasiGame.create({ setupID: setup.id, hostID: setup.ownerID });
        const { factionRole, prevModifier } = getFactionRoleModifierArguments('boolean');
        const args = {
            value: !prevModifier.value,
            factionRoleID: factionRole.id,
        };

        const responseObj = await upsertFactionRoleModifier(args);

        expect(responseObj.statusCode).to.be.equal(422);
    });

    it('Will gracefully fail when the request is supposed to be boolean', async() => {
        await quasiGame.create({ setupID: setup.id, hostID: setup.ownerID });
        const { factionRole, prevModifier } = getFactionRoleModifierArguments('boolean');
        const args = {
            name: prevModifier.name,
            value: 300,
            factionRoleID: factionRole.id,
        };

        const responseObj = await upsertFactionRoleModifier(args);

        expect(responseObj.statusCode).to.be.equal(422);
    });

    it('Will gracefully fail when the request is supposed to be an integer', async() => {
        await quasiGame.create({ setupID: setup.id, hostID: setup.ownerID });
        const { factionRole, prevModifier } = getFactionRoleModifierArguments('number');
        const args = {
            name: prevModifier.name,
            value: true,
            factionRoleID: factionRole.id,
        };

        const responseObj = await upsertFactionRoleModifier(args);

        expect(responseObj.statusCode).to.be.equal(422);
    });

    it('Will not allow updates if the setup is frozen', async() => {
        await setupService.setPriority(setup.id, FEATURED_PRIORITY, FEATURED_KEY);
        const { factionRole, prevModifier } = getFactionRoleModifierArguments('boolean');
        const args = {
            name: prevModifier.name,
            value: !prevModifier.value,
            factionRoleID: factionRole.id,
        };

        const responseObj = await upsertFactionRoleModifier(args);

        testControllerHelpers.assertUneditableSetupErrors(responseObj);
    });

    function upsertFactionRoleModifier(input = {}){
        const args = {
            name: input.name,
            value: input.value,
            minPlayerCount: 'minPlayerCount' in input ? input.minPlayerCount : 0,
            maxPlayerCount: 'maxPlayerCount' in input ? input.maxPlayerCount : MAX_PLAYER_COUNT,
        };
        return requests.post(`api/factionRoles/${input.factionRoleID}/modifiers`,
            args, input.headers || headers);
    }

    function getFactionRoleModifierArguments(typeString){
        let returnValue;
        setup.factions.forEach(faction => {
            faction.factionRoles.forEach(factionRole => {
                factionRole.modifiers.forEach(modifier => {
                    if(typeof(modifier.value) === typeString) // eslint-disable-line valid-typeof
                        returnValue = {
                            factionRole,
                            prevModifier: modifier,
                        };
                });
            });
        });
        return returnValue;
    }

    function getNewModifier(factionRoleID, prevModifier){
        let returnValue;
        setup.factions.forEach(faction => {
            faction.factionRoles.forEach(factionRole => {
                factionRole.modifiers.forEach(modifier => {
                    if(modifier.name === prevModifier.name && factionRole.id === factionRoleID)
                        returnValue = modifier;
                });
            });
        });
        return returnValue;
    }
});
