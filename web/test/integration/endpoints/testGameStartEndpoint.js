require('../../init');
const { expect } = require('chai');

const authHelpers = require('../../authHelpers');
const requests = require('../../requests');

const gameIntegrationService = require('../../../services/gameIntegrationService');

const quasiGame = require('../../quasiModels/quasiGame');
const quasiSetup = require('../../quasiModels/quasiSetup');
const quasiWebSocket = require('../../quasiWebSocket');

const { getModeratorID } = require('../../../utils/lobbyUtil');


describe('Game Start', () => {
    let ws;
    afterEach(() => {
        if(ws)
            ws.close();
    });

    it('Will allow moderators to start a game', async() => {
        const gameObj = await quasiGame.create({ playerCount: 7 });
        const moderatorID = gameObj.users.find(user => user.isModerator).id;
        const headers = authHelpers.fakeTempAuthHeader(moderatorID);

        const responseObj = await requests.post(`api/games/${gameObj.id}/start`, null, headers);

        expect(responseObj.statusCode).to.be.equal(200);
    });

    it('Sends a profile to users in game', async() => {
        const gameObj = await quasiGame.create({ playerCount: 7 });
        const ogFactionIDs = gameObj.setup.factions.map(faction => faction.id);
        const moderatorID = getModeratorID(gameObj);
        const headers = authHelpers.fakeTempAuthHeader(moderatorID);
        const messages = await addWebSocket(headers);

        const responseObj = await requests.post(`api/games/${gameObj.id}/start`, null, headers);
        const game = responseObj.json.response;
        await new Promise(resolve => {
            setTimeout(resolve, 1000);
        });
        const gameIntegrations = await gameIntegrationService.getIntegrations(game.id);

        expect(responseObj.statusCode).to.be.equal(200);
        expect(gameIntegrations.length).to.not.equal(0);
        game.setup.factions.forEach(faction => expect(ogFactionIDs).to.include((faction.id)));
        const hasProfileUpdate = messages.some(message => message.profile
            && message.profile.gameID === gameObj.id
            && message.profile.roleCard);
        expect(hasProfileUpdate).to.be.equal(true);
    });

    it('Will stop game starts gracefully', async() => {
        const setup = await quasiSetup.create();
        const headers = authHelpers.fakeTempAuthHeader(setup.ownerID);
        const gameObj = await quasiGame.create({ hostID: setup.ownerID, setupID: setup.id });

        const responseObj = await requests.post(`api/games/${gameObj.id}/start`, null, headers);

        expect(responseObj.statusCode).to.be.equal(422);
    });

    it('Will throw if user is not moderator', async() => {
        const gameObj = await quasiGame.create({ playerCount: 7 });
        const userID = gameObj.users.find(user => !user.isModerator).id;
        const headers = authHelpers.fakeTempAuthHeader(userID);

        const responseObj = await requests.post(`api/games/${gameObj.id}/start`, null, headers);

        expect(responseObj.statusCode).to.be.equal(403);
    });

    async function addWebSocket(headers){
        const response = await quasiWebSocket.connect(headers);
        ws = response.socket;
        return response.messages;
    }
});
