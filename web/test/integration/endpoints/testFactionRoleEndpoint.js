require('../../init');
const { expect } = require('chai');

const helpers = require('../../../utils/helpers');

const authHelpers = require('../../authHelpers');
const requests = require('../../requests');

const factionRoleService = require('../../../services/setup/factionRoleService');
const setupService = require('../../../services/setup/setupService');

const factionRoleRepo = require('../../../repos/factionRoleRepo');

const quasiGame = require('../../quasiModels/quasiGame');
const quasiSetup = require('../../quasiModels/quasiSetup');

const testControllerHelpers = require('../../testControllerHelpers');

const factionRoleSchema = require('../../schemas/factionRoleSchema');

const { FEATURED_KEY, FEATURED_PRIORITY } = require('../../fakeConstants');


describe('Faction Roles endpoint', async() => {
    let setup;
    let headers;
    let factionRole;
    let newFactionRoleID;
    beforeEach(async() => {
        setup = await quasiSetup.create();
        headers = authHelpers.fakeTempAuthHeader(setup.ownerID);
        factionRole = setup.factions[0].factionRoles[0];
        newFactionRoleID = setup.factions
            .map(faction => faction.factionRoles.filter(fr => fr.id !== factionRole.id))[0][0].id;
    });

    it('Will give specific faction role information', async() => {
        await quasiGame.create({ setupID: setup.id, hostID: setup.ownerID });

        const responseObj = await getFactionRole();

        expect(responseObj.statusCode).to.be.equal(200);
        expect(responseObj.json.response.name).to.be.equal(factionRole.name);
        factionRoleSchema.check(responseObj.json.response);
    });

    it('Will fail gracefully gets when the factionRoleID doesn\'t exist', async() => {
        await quasiGame.create({ setupID: setup.id, hostID: setup.ownerID });
        const factionRoleID = factionRole.id * 10;

        const responseObj = await requests.get(`api/factionRoles/${factionRoleID}`, headers);

        expect(responseObj.statusCode).to.be.equal(422);
    });

    it('Will fail gracefully when the factionRoleID is not a number', async() => {
        await quasiGame.create({ setupID: setup.id, hostID: setup.ownerID });

        const responseObj = await getFactionRole({ factionRoleID: 'ABCD' });

        expect(responseObj.statusCode).to.be.equal(404);
    });

    it('Will create a faction role', async() => {
        await quasiGame.create({ setupID: setup.id, hostID: setup.ownerID });
        let faction = setup.factions[0];
        const role = getRoleNotInFaction(faction, setup.roles);
        const args = { roleID: role.id, factionID: faction.id };

        const responseObj = await requests.post('api/factionRoles', args, headers);
        factionRoleSchema.check(responseObj.json.response);
        setup = await setupService.getByID(setup.id);
        faction = setup.factions.find(f => f.id === faction.id);
        const newRoleIDs = faction.factionRoles.map(fr => fr.roleID);

        expect(responseObj.statusCode).to.be.equal(200);
        expect(newRoleIDs).to.include(role.id);
    });

    it('Will fail gracefully creates when no factionID is submitted', async() => {
        await quasiGame.create({ setupID: setup.id, hostID: setup.ownerID });
        const args = { roleID: setup.roles[0].id };

        const responseObj = await requests.post('api/factionRoles', args, headers);

        expect(responseObj.statusCode).to.be.equal(422);
    });

    it('Will fail gracefully creates when no roleID is submitted', async() => {
        await quasiGame.create({ setupID: setup.id, hostID: setup.ownerID });
        const args = { factionID: setup.factions[0].id };

        const responseObj = await requests.post('api/factionRoles', args, headers);

        expect(responseObj.statusCode).to.be.equal(422);
    });

    it('Will set a received faction role', async() => {
        await quasiGame.create({ setupID: setup.id, hostID: setup.ownerID });

        const responseObj = await updateReceivedRole();
        const dbFactionRole = await factionRoleRepo.getByID(factionRole.id);
        const apiFactionRole = await factionRoleService.get(setup.ownerID, factionRole.id);

        expect(responseObj.statusCode).to.be.equal(204);
        expect(dbFactionRole.receivedFactionRoleID).to.be.equal(newFactionRoleID);
        expect(apiFactionRole.receivedFactionRoleID).to.be.equal(newFactionRoleID);
    });

    it('Will do nothing if the args are empty', async() => {
        // given setup in beforeEach

        const responseObj = await updateReceivedRole({ receivedFactionRoleID: undefined });

        expect(responseObj.statusCode).to.be.equal(204);
    });

    it('Will not allow updates to faction roles if the setup is frozen', async() => {
        await setupService.setPriority(setup.id, FEATURED_PRIORITY, FEATURED_KEY);

        const responseObj = await updateReceivedRole();

        testControllerHelpers.assertUneditableSetupErrors(responseObj);
    });

    // eslint-disable-next-line max-len
    it('Will gracefully fail receivedFactionRole update requests when receive id does not exist', async() => {
        await quasiGame.create({ setupID: setup.id, hostID: setup.ownerID });
        // given setup in beforeEach

        const responseObj = await updateReceivedRole({ receivedFactionRoleID: 0 });

        expect(responseObj.statusCode).to.be.equal(422);
    });

    // eslint-disable-next-line max-len
    it('Will gracefully fail receivedFactionRole update requests when target id does not exist', async() => {
        await quasiGame.create({ setupID: setup.id, hostID: setup.ownerID });
        const factionRoleID = 1 + Math.max(...setup.factions
            .map(faction => Math.max(...faction.factionRoles.map(fr => fr.id))));

        const responseObj = await updateReceivedRole({ factionRoleID });

        expect(responseObj.statusCode).to.be.equal(422);
    });

    // eslint-disable-next-line max-len
    it('Will set the received role to nothing when the received faction role equals the faction role', async() => {
        await quasiGame.create({ setupID: setup.id, hostID: setup.ownerID });
        await factionRoleService.update(setup.ownerID, factionRole.id,
            { receivedFactionRoleID: newFactionRoleID });

        const responseObj = await updateReceivedRole({ receivedFactionRoleID: factionRole.id });
        const dbFactionRole = await factionRoleRepo.getByID(factionRole.id);

        expect(responseObj.statusCode).to.be.equal(204);
        expect(dbFactionRole.receivedFactionRoleID).to.be.eql(null);
    });

    it('Will remove the received faction role', async() => {
        await quasiGame.create({ setupID: setup.id, hostID: setup.ownerID });
        await factionRoleService.update(setup.ownerID, factionRole.id,
            { receivedFactionRoleID: newFactionRoleID });

        const responseObj = await updateReceivedRole({ receivedFactionRoleID: null });
        const dbFactionRole = await factionRoleRepo.getByID(factionRole.id);

        expect(responseObj.statusCode).to.be.equal(204);
        expect(dbFactionRole.receivedFactionRoleID).to.be.eql(null);
    });

    it('Will delete a faction role', async() => {
        await quasiGame.create({ setupID: setup.id, hostID: setup.ownerID });

        const responseObj = await requests.delete(`api/factionRoles/${factionRole.id}`,
            headers);
        setup = await setupService.getByID(setup.id);
        const factionRoles = helpers.flattenList(setup.factions
            .map(faction => faction.factionRoles));
        const oldFactionRole = factionRoles.find(fr => fr.id === factionRole.id);

        expect(responseObj.statusCode).to.be.equal(204);
        expect(oldFactionRole).to.be.a('undefined');
    });

    it('Will not add faction roles to games that are featured', async() => {
        await quasiGame.create({
            setupID: setup.id,
            hostID: setup.ownerID,
            started: false,
        });
        const [faction] = setup.factions;
        const role = getRoleNotInFaction(faction, setup.roles);
        const args = { roleID: role.id, factionID: faction.id };
        await setupService.setPriority(setup.id, FEATURED_PRIORITY, FEATURED_KEY);

        const responseObj = await requests.post('api/factionRoles', args, headers);

        testControllerHelpers.assertUneditableSetupErrors(responseObj);
    });

    it('Will not delete faction roles to games that are featured', async() => {
        await quasiGame.create({
            setupID: setup.id,
            hostID: setup.ownerID,
            started: false,
        });
        await setupService.setPriority(setup.id, FEATURED_PRIORITY, FEATURED_KEY);

        const responseObj = await requests.delete(`api/factionRoles/${factionRole.id}`, headers);

        testControllerHelpers.assertUneditableSetupErrors(responseObj);
    });

    function getFactionRole(input = {}){
        const urlFactionRoleID = input.factionRoleID || factionRole.id;
        return requests.get(`api/factionRoles/${urlFactionRoleID}`, headers);
    }

    function updateReceivedRole(input = {}){
        const urlFactionRoleID = input.factionRoleID || factionRole.id;
        const args = {
            receivedFactionRoleID: Object.keys(input).includes('receivedFactionRoleID')
                ? input.receivedFactionRoleID : newFactionRoleID,
        };
        return requests.put(`api/factionRoles/${urlFactionRoleID}`, args, headers);
    }
});

function getRoleNotInFaction(faction, roles){
    const factionRoleIDs = new Set(faction.factionRoles.map(factionRole => factionRole.roleID));
    return roles.find(role => !factionRoleIDs.has(role.id));
}
