require('../../init');
const { expect } = require('chai');
const { expectStatusCode } = require('../../assertions');

const setupModifiers = require('../../../models/enums/setupModifiers');

const authHelpers = require('../../authHelpers');
const requests = require('../../requests');

const replayRepo = require('../../../repos/replayRepo');

const actionService = require('../../../services/actionService');
const gameService = require('../../../services/gameService');
const moderatorService = require('../../../services/moderatorService');
const playerService = require('../../../services/playerService');

const gameSchema = require('../../schemas/gameSchema');
const playerSchema = require('../../schemas/playerSchema');

const quasiGame = require('../../quasiModels/quasiGame');
const quasiSetup = require('../../quasiModels/quasiSetup');
const quasiUser = require('../../quasiModels/quasiUser');
const quasiWebSocket = require('../../quasiWebSocket');

const { getModeratorID } = require('../../../utils/lobbyUtil');


describe('Players', async() => {
    let ws;
    afterEach(() => {
        if(ws)
            ws.close();
    });

    describe('Create game player', () => {
        it('Creates a player in an open game', async() => {
            const gameObj = await quasiGame.create();
            const user = await quasiUser.createUser();
            const newPlayerHeaders = authHelpers.fakeTempAuthHeader(user.id);
            const playerName = 'newPlayerName';
            const moderatorID = gameObj.users.find(u => u.isModerator).id;
            const hostHeaders = authHelpers.fakeTempAuthHeader(moderatorID);
            const messages = await addWebSocket(hostHeaders);

            const responseObj = await requests.post('api/players', {
                joinID: gameObj.joinID,
                playerName,
            }, newPlayerHeaders);
            await new Promise(resolve => {
                setTimeout(resolve, 1000);
            });

            expect(responseObj.statusCode).to.be.equal(200);
            const { response } = responseObj.json;
            expect(response.joinID).to.be.equal(gameObj.joinID);
            expect(response.players.map(player => player.name)).to.include(playerName);
            gameSchema.checkNotStarted(response);
            expect(messages.find(m => m.event === 'playerAdded')).to.be.an('object');
        });

        it('Doesn\'t allow you to join a game twice', async() => {
            const playerCount = 2;
            const gameObj = await quasiGame.create({ playerCount });
            const duplicateUserID = gameObj.users.find(user => !user.isModerator).id;
            const headers = authHelpers.fakeTempAuthHeader(duplicateUserID);

            const responseObj = await requests.post('api/players', {
                joinID: gameObj.joinID,
            }, headers);
            const game = await gameService.getByLobbyID(gameObj.joinID);

            expect(responseObj.statusCode).to.be.equal(422);
            expect(responseObj.json.errors.length).to.be.equal(1);
            expect(game.players.length).to.be.equal(playerCount);
        });

        it('Doesn\'t create a player if the joinID is too long', async() => {
            const user = await quasiUser.createUser();
            const headers = authHelpers.fakeTempAuthHeader(user.id);
            const playerName = 'newPlayerName';
            const joinID = 'ABCDE';

            const responseObj = await requests.post('api/players', {
                joinID,
                playerName,
            }, headers);

            expect(responseObj.statusCode).to.be.equal(422);
        });

        it('Will not change the host if someone joins', async() => {
            let gameObj = await quasiGame.create();
            const hostID = gameObj.users.find(user => user.isModerator).id;
            await moderatorService.create(hostID);
            const user = await quasiUser.createUser();
            const headers = authHelpers.fakeTempAuthHeader(user.id);

            const responseObj = await requests.post('api/players', {
                joinID: gameObj.joinID,
                playerName: 'someplayer',
            }, headers);
            gameObj = await gameService.getByLobbyID(gameObj.joinID);

            expect(responseObj.statusCode).to.be.equal(200);
            expect(responseObj.json.errors.length).to.be.equal(0);
            const moderatorIDs = gameObj.users.filter(({ isModerator }) => isModerator)
                .map(({ id }) => id);
            expect(moderatorIDs).to.deep.equal([hostID]);
        });
    });

    describe('Will leave the game', () => {
        it('Does not keep the game id in the database', async() => {
            const gameObj = await quasiGame.create();
            const moderatorID = gameObj.users.find(user => user.isModerator).id;
            const headers = authHelpers.fakeTempAuthHeader(moderatorID);

            await requests.delete('api/players', headers);

            try{
                await replayRepo.getByID(gameObj.id);
            }catch(err){
                expect(err.toString()).to.be.equal('Error: No game with that id.');
            }
        });

        it('Will keep the game in the database if everyone leaves and game is done', async() => {
            const setup = await quasiSetup.create({
                modifiers: [{
                    name: setupModifiers.DAY_START,
                    value: true,
                }],
                setupHiddenCount: 4,
            });
            const gameObj = await quasiGame.create({
                playerCount: 4,
                setupID: setup.id,
                isStarted: true,
            });
            const townUserIDs = gameObj.players
                .filter(player => player.factionRole.team.name !== 'Mafia')
                .map(player => player.userID);
            const mafiaName = gameObj.players
                .filter(player => !townUserIDs.includes(player.userID))[0].name;
            const voteSubmissions = townUserIDs
                .map(townUserID => {
                    const actionMessage = { message: `vote ${mafiaName}` };
                    return actionService.submitActionMessage(townUserID, actionMessage);
                });
            await Promise.all(voteSubmissions);

            const leavePromises = gameObj.players
                .map(player => {
                    const headers = authHelpers.fakeTempAuthHeader(player.userID);
                    return requests.delete('api/players', headers);
                });
            await Promise.all(leavePromises);

            const game = await replayRepo.getByID(gameObj.id);
            expect(game).to.not.be.a('null');
        });

        // eslint-disable-next-line max-len
        it('Closes a game if the host leaves, is a moderator, and is the only one in game', async() => {
            let gameObj = await quasiGame.create();
            const moderatorID = gameObj.users.find(user => user.isModerator).id;
            await moderatorService.create(moderatorID);
            const headers = authHelpers.fakeTempAuthHeader(moderatorID);

            const responseObj = await requests.delete('api/players', headers);
            gameObj = await gameService.getByLobbyID(gameObj.joinID);

            expect(responseObj.statusCode).to.be.equal(204);
            expect(responseObj.data).to.equal('');
            expect(gameObj).to.be.a('null');
        });

        it('Will reassign the host if a moderator leaves', async() => {
            let gameObj = await quasiGame.create({ playerCount: 2 });
            let moderatorID = getModeratorID(gameObj);
            await moderatorService.create(moderatorID);
            const newHostID = gameObj.users.find(user => !user.isModerator).id;
            const headers = authHelpers.fakeTempAuthHeader(moderatorID);

            const responseObj = await requests.delete('api/players', headers);
            gameObj = await gameService.getByLobbyID(gameObj.joinID);

            expect(responseObj.statusCode).to.be.equal(204);
            moderatorID = getModeratorID(gameObj);
            expect(moderatorID).to.be.equal(newHostID);
        });
    });

    describe('Will delete another game user', () => {
        let gameObj;
        let moderatorID;
        let kickedPlayer;
        beforeEach(async() => {
            gameObj = await quasiGame.create({ playerCount: 2 });
            moderatorID = getModeratorID(gameObj);
            kickedPlayer = gameObj.players.find(player => player.id !== moderatorID);
        });

        it('Will kick a player', async() => {
            // given in beforeEach

            const responseObj = await removeUser();
            gameObj = await gameService.getByID(gameObj.id);
            const kickedUser = gameObj.users.find(user => user.id === kickedPlayer.userID);
            kickedPlayer = gameObj.players.find(player => player.id === kickedPlayer.userID);

            expectStatusCode(200, responseObj);
            expect(gameObj.players.length).to.be.equal(1);
            playerSchema.checkGameUserDeleteResponse(responseObj.json.response);
            expect(kickedPlayer).to.be.an('undefined');
            expect(kickedUser).to.be.an('undefined');
        });

        it('Will gracefully fail when there\'s no playerName', async() => {
            const userID = undefined;

            const responseObj = await removeUser({ userID });

            expect(responseObj.statusCode).to.be.equal(422);
        });

        it('Will gracefully fail when there are arguments specified', async() => {
            const headers = authHelpers.fakeTempAuthHeader(moderatorID);
            const args = {};

            const responseObj = await requests.delete(
                `api/players/kick?playerID=${kickedPlayer.id}`, headers, args,
            );

            expect(responseObj.statusCode).to.not.be.equal(500);
            expect(responseObj.statusCode).to.be.greaterThan(299);
        });

        function removeUser(args = {}){
            const userID = 'userID' in args
                ? args.playerID
                : kickedPlayer.userID;
            const queryParamString = requests.getQueryParams({ userID });
            const requestHeaders = 'headers' in args
                ? args.headers
                : authHelpers.fakeTempAuthHeader(moderatorID);
            return requests.delete(
                `api/players/kick${queryParamString}`, requestHeaders,
            );
        }
    });

    describe('Will add bots', () => {
        it('Will add a bot', async() => {
            const gameObj = await quasiGame.create();
            const moderatorID = gameObj.users.find(user => user.isModerator).id;
            const headers = authHelpers.fakeTempAuthHeader(moderatorID);
            const args = { botCount: 1 };

            const responseObj = await requests.post('api/players/bots', args, headers);

            expect(responseObj.statusCode).to.be.equal(200);
            expect(responseObj.json.response.players).to.be.an('array');
            expect(responseObj.json.response.setup).to.be.an('object');
        });

        it('Will fail bot requests if bot count is not positive', async() => {
            const gameObj = await quasiGame.create();
            const moderatorID = gameObj.users.find(user => user.isModerator).id;
            const headers = authHelpers.fakeTempAuthHeader(moderatorID);
            const args = { botCount: 0 };

            const responseObj = await requests.post('api/players/bots', args, headers);

            expect(responseObj.statusCode).to.be.equal(422);
        });
    });

    describe('Will delete players', () => {
        let gameObj;
        let playerCount;
        let botID;
        beforeEach(async() => {
            gameObj = await quasiGame.create({
                playerCount: 2,
                setupHiddenCount: 5,
            });
            const response = await playerService.addBots(gameObj.setup.ownerID, 1);
            botID = response.players.find(p => !p.userID).id;
            playerCount = response.players.length;
        });

        it('Will remove a player', async() => {
            // given in above

            const responseObj = await deletePlayer();

            expectStatusCode(200, responseObj);
            playerSchema.checkPlayerDeleteResponse(responseObj.json.response);
            await assertPlayerCount(playerCount - 1);
        });

        it('Will gracefully handle playerID not being an int', async() => {
            const playerID = 'abc';

            const responseObj = await deletePlayer({ playerID });

            expectStatusCode(404, responseObj);
        });

        it('Will gracefully handle playerID not being sent', async() => {
            const playerID = undefined;

            const responseObj = await deletePlayer({ playerID });

            expectStatusCode(404, responseObj);
        });

        it('Will gracefully handle gameID not being an int', async() => {
            const gameID = 'abc';

            const responseObj = await deletePlayer({ gameID });

            expectStatusCode(404, responseObj);
        });

        it('Will gracefully handle gameID not being sent', async() => {
            const gameID = undefined;

            const responseObj = await deletePlayer({ gameID });

            expectStatusCode(404, responseObj);
        });

        it('Will handle non moderator bot deletes with a 403', async() => {
            const user = await quasiUser.createUser();
            await playerService.create(user.id, 'somePlayerName', gameObj.joinID);
            const headers = authHelpers.fakeTempAuthHeader(user.id);

            const responseObj = await deletePlayer({ headers });

            expectStatusCode(403, responseObj);
        });

        it('Will stop started game deletes', async() => {
            await gameService.start(gameObj.id);

            const responseObj = await deletePlayer();

            expectStatusCode(422, responseObj);
        });

        async function assertPlayerCount(newPlayerCount){
            const newGameObj = await gameService.getByID(gameObj.id);
            expect(newGameObj.players.length).to.be.equal(newPlayerCount);
        }

        function deletePlayer(args = {}){
            const gameID = 'gameID' in args ? args.gameID : gameObj.id;
            const playerID = 'playerID' in args
                ? args.playerID
                : botID;
            const queryParamString = requests.getQueryParams({ gameID, playerID });
            const requestHeaders = 'headers' in args
                ? args.headers
                : authHelpers.fakeTempAuthHeader(gameObj.setup.ownerID);
            return requests.delete(`api/players/bots${queryParamString}`, requestHeaders);
        }
    });

    describe('Update player name', () => {
        let gameObj;
        let newPlayerName;
        beforeEach(async() => {
            gameObj = await quasiGame.create();
            newPlayerName = `${gameObj.players[0].name}2`;
        });

        it('Will update the name of a user\'s player', async() => {
            // given in beforeEach

            const serverResponse = await updatePlayerName();

            expectStatusCode(204, serverResponse);
        });

        it('Will gracefully fail if no name in body', async() => {
            // given in beforeEach

            const serverResponse = await updatePlayerName({ playerName: undefined });

            expectStatusCode(422, serverResponse);
        });

        it('Will gracefully fail if no headers', async() => {
            // given in beforeEach

            const serverResponse = await updatePlayerName({ headers: undefined });

            expectStatusCode(403, serverResponse);
        });

        it('Will push player updates to other listening users', async() => {
            [gameObj] = await quasiGame.addPlayersToGame(gameObj.joinID, 1);
            const user2ID = gameObj.users.find(user => user.id !== gameObj.setup.ownerID).id;
            const user2Headers = authHelpers.fakeTempAuthHeader(user2ID);
            const messages = await addWebSocket(user2Headers);

            await updatePlayerName();

            const playerUpdateMessage = messages.find(m => m.event === 'playerNameUpdate');
            expect(typeof playerUpdateMessage).to.be.eq('object');
            playerSchema.checkNameUpdate(playerUpdateMessage);
            expect(playerUpdateMessage.playerName).to.be.equal(newPlayerName);
        });

        it('Will not push player updates if the was no actual update', async() => {
            [gameObj] = await quasiGame.addPlayersToGame(gameObj.joinID, 1);
            const user2ID = gameObj.users.find(user => user.id !== gameObj.setup.ownerID).id;
            const user2Headers = authHelpers.fakeTempAuthHeader(user2ID);
            const messages = await addWebSocket(user2Headers);

            const oldPlayer = gameObj.players.find(p => p.userID === gameObj.setup.ownerID);
            const serverResponse = await updatePlayerName({ playerName: oldPlayer.name });

            expectStatusCode(204, serverResponse);
            const playerUpdateMessages = messages.filter(m => m.event === 'playerNameUpdate');
            expect(playerUpdateMessages.length).to.be.equal(0);
        });

        function updatePlayerName(args = {}){
            const playerName = 'playerName' in args ? args.playerName : newPlayerName;
            const playerID = gameObj.players[0].id;
            const headers = 'headers' in args ? args.headers
                : authHelpers.fakeTempAuthHeader(gameObj.setup.ownerID);
            return requests.put(`api/players/${playerID}`, { name: playerName }, headers);
        }
    });

    async function addWebSocket(headers){
        const response = await quasiWebSocket.connect(headers);
        ws = response.socket;
        return response.messages;
    }
});
