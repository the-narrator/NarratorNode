require('../../init');
const { expect } = require('chai');

const authHelpers = require('../../authHelpers');
const requests = require('../../requests');

const setupRepo = require('../../../repos/setupRepo');

const gameService = require('../../../services/gameService');
const setupService = require('../../../services/setup/setupService');

const quasiGame = require('../../quasiModels/quasiGame');
const quasiSetup = require('../../quasiModels/quasiSetup');
const quasiUser = require('../../quasiModels/quasiUser');

const setupModifierSchema = require('../../schemas/setupModifierSchema');

const testControllerHelpers = require('../../testControllerHelpers');

const { FEATURED_PRIORITY, FEATURED_KEY } = require('../../fakeConstants');

const { MAX_PLAYER_COUNT } = require('../../../utils/constants');


describe('Setup Modifier Endpoint', () => {
    let setup;
    let headers;
    let modifierName;
    let newModifierValue;
    let oldModifierValue;
    let gameID;
    beforeEach(async() => {
        setup = await quasiSetup.create();
        gameID = (await quasiGame.create({ setupID: setup.id, hostID: setup.ownerID })).id;
        headers = authHelpers.fakeTempAuthHeader(setup.ownerID);
        const modifier = Object.values(setup.setupModifiers)
            .filter(m => typeof(m.value) === 'boolean')[0];
        modifierName = modifier.name;
        oldModifierValue = modifier.value;
        newModifierValue = !modifier.value;
    });

    it('Will update the setups\'s modifier (boolean)', async() => {
        // given in before each

        const responseObj = await createRequest();
        const gameObject = await gameService.getByID(gameID);

        expect(responseObj.statusCode).to.be.equal(200);
        const newModifier = Object.values(gameObject.setup.setupModifiers)
            .filter(modifier => modifier.name === modifierName)[0];
        expect(newModifier.value).to.be.equal(newModifierValue);
        expect(newModifier.value).to.be.equal(responseObj.json.response.value);
        gameObject.setup.setupModifiers.forEach(setupModifierSchema.check);
    });

    it('Will update the setup\'s modifier (integer)', async() => {
        const prevModifier = setup.setupModifiers
            .find(modifier => typeof(modifier.value) === 'number');
        const newValue = prevModifier.value + 1;

        const responseObj = await createRequest({
            modifierName: prevModifier.name,
            modifierValue: newValue,
        });
        const gameObject = await gameService.getByID(gameID);

        expect(responseObj.statusCode).to.be.equal(200);
        const newModifier = Object.values(gameObject.setup.setupModifiers)
            .filter(modifier => modifier.name === prevModifier.name)[0];
        expect(newModifier.value).to.be.equal(newValue);
        expect(newModifier.value).to.be.equal(responseObj.json.response.value);
    });

    it('Will update the setup\'s modifier (string)', async() => {
        const prevModifier = setup.setupModifiers
            .find(modifier => typeof(modifier.value) === 'string');
        const newValue = `${prevModifier.value}2`;

        const responseObj = await createRequest({
            modifierName: prevModifier.name,
            modifierValue: newValue,
        });
        const gameObject = await gameService.getByID(gameID);

        expect(responseObj.statusCode).to.be.equal(200);
        const newModifier = Object.values(gameObject.setup.setupModifiers)
            .find(modifier => modifier.name === prevModifier.name);
        expect(newModifier.value).to.be.equal(newValue);
        expect(newModifier.value).to.be.equal(responseObj.json.response.value);
    });

    it('Will insert more than one modifier with the same name (int)', async() => {
        const prevModifier = setup.setupModifiers
            .find(modifier => typeof(modifier.value) === 'number');
        const value = prevModifier.value + 1;
        const { name } = prevModifier;

        await createRequest({ name, minPlayerCount: 0, value });
        await createRequest({ name, maxPlayerCount: 1, value });
        const gameObject = await gameService.getByID(gameID);
        const modifiers = await setupRepo.getModifiersByID(setup.id);

        expect(gameObject.setup.setupModifiers
            .filter(m => m.name === modifierName).length).to.be.equal(2);
        expect(modifiers.filter(m => m.name === modifierName).length).to.be.equal(2);
    });

    it('Will insert more than one modifier with the same name (bool)', async() => {
        // given in before each

        await createRequest({ name: modifierName, minPlayerCount: 0 });
        await createRequest({ name: modifierName, maxPlayerCount: 1 });
        const gameObject = await gameService.getByID(gameID);
        const modifiers = await setupRepo.getModifiersByID(setup.id);

        expect(gameObject.setup.setupModifiers
            .filter(m => m.name === modifierName).length).to.be.equal(2);
        expect(modifiers.filter(m => m.name === modifierName).length).to.be.equal(2);
    });


    it('Will not allow updates if the setup is featured', async() => {
        await setupService.setPriority(setup.id, FEATURED_PRIORITY, FEATURED_KEY);

        const responseObj = await createRequest();

        testControllerHelpers.assertUneditableSetupErrors(responseObj);
    });

    it('Will not allow updates if the user doesn\'t own the setup', async() => {
        const user = await quasiUser.createUser();

        const responseObj = await createRequest({
            headers: authHelpers.fakeTempAuthHeader(user.id),
        });
        const gameObject = await gameService.getByID(gameID);

        expect(responseObj.statusCode).to.be.equal(422);
        const newModifier = Object.values(gameObject.setup.setupModifiers)
            .find(modifier => modifier.name === modifierName);
        expect(newModifier.value).to.be.equal(oldModifierValue);
    });

    it('Will gracefully fail when the modifier name is wrong', async() => {
        // given in before each

        const responseObj = await createRequest({
            modifierName: modifierName + modifierName,
        });

        expect(responseObj.statusCode).to.be.equal(422);
    });

    it('Will gracefully fail when the request is missing the name', async() => {
        // given in before each

        const responseObj = await createRequest({
            modifierName: undefined,
        });

        expect(responseObj.statusCode).to.be.equal(422);
    });

    it('Will gracefully fail when the request is missing the maxPlayerCount', async() => {
        // given in before each

        const responseObj = await createRequest({
            minPlayerCount: undefined,
        });

        expect(responseObj.statusCode).to.be.equal(422);
    });

    it('Will gracefully fail when the request is missing the minPlayerCount', async() => {
        // given in before each

        const responseObj = await createRequest({
            maxPlayerCount: undefined,
        });

        expect(responseObj.statusCode).to.be.equal(422);
    });

    it('Will gracefully fail when the request is missing the value', async() => {
        // given in before each

        const responseObj = await createRequest({
            modifierValue: undefined,
        });

        expect(responseObj.statusCode).to.be.equal(422);
    });

    it('Will gracefully fail when the request is supposed to be boolean', async() => {
        const prevModifier = Object.values(setup.setupModifiers)
            .find(modifier => typeof(modifier.value) === 'boolean');

        const responseObj = await createRequest({
            modifierName: prevModifier.name,
            modifierValue: 300,
        });

        expect(responseObj.statusCode).to.be.equal(422);
    });

    it('Will gracefully fail when the request is supposed to be an integer', async() => {
        const prevModifier = Object.values(setup.setupModifiers)
            .find(modifier => typeof(modifier.value) === 'number');

        const responseObj = await createRequest({
            modifierName: prevModifier.name,
            modifierValue: true,
        });

        expect(responseObj.statusCode).to.be.equal(422);
    });

    function createRequest(args = {}){
        const payload = {
            name: 'modifierName' in args ? args.modifierName : modifierName,
            value: 'modifierValue' in args ? args.modifierValue : newModifierValue,
            minPlayerCount: 'minPlayerCount' in args ? args.minPlayerCount : 0,
            maxPlayerCount: 'maxPlayerCount' in args ? args.maxPlayerCount : MAX_PLAYER_COUNT,
        };
        return requests.post('api/setupModifiers', payload, args.headers || headers);
    }
});
