const actionService = require('../../services/actionService');
const gameService = require('../../services/gameService');
const phaseService = require('../../services/phaseService');
const playerService = require('../../services/playerService');
const userService = require('../../services/userService');

const gamePhase = require('../../models/enums/gamePhase');


async function create(gameOptions = {}){
    const quasiSetup = require('./quasiSetup');
    const quasiUser = require('./quasiUser');
    const hostUser = gameOptions.hostID
        ? await userService.getByID(gameOptions.hostID)
        : await quasiUser.createUser();
    const setupHiddenCount = gameOptions.setupHiddenCount || gameOptions.playerCount || 1;
    const playerCount = gameOptions.playerCount || 1;
    if(!gameOptions.setupID)
        gameOptions.setupID = (await quasiSetup
            .create({ ownerID: hostUser.id, setupHiddenCount })).id;
    const game = await gameService.create(hostUser.id, hostUser.name, gameOptions.setupID);

    await addPlayersToGame(game.joinID, playerCount - 1);

    await gameService.updateModifier(hostUser.id, { name: 'DISCUSSION_LENGTH', value: 0 });
    const gameModifierPromises = (gameOptions.modifiers || [])
        .map(modifier => gameService.updateModifier(hostUser.id, modifier));
    await Promise.all(gameModifierPromises);
    if(gameOptions.isStarted || gameOptions.isFinished)
        await gameService.start(game.id);
    if(gameOptions.isFinished)
        await endGame(game);
    return gameService.filterNeededUserInfo(await gameService.getByID(game.id));
}

async function addPlayersToGame(joinID, playerCount){
    const quasiUser = require('./quasiUser');
    let promises = [];
    for(let i = 0; i < playerCount; i++)
        promises.push(quasiUser.createUser(`guest${i + 1}`));

    const users = await Promise.all(promises);
    promises = users
        .map(user => playerService.create(user.id, user.name, joinID));
    return Promise.all(promises);
}

module.exports = {
    addPlayersToGame,
    create,
};

async function endGame(game){
    game = await gameService.getByID(game.id);
    while (game.phase.name !== gamePhase.FINISHED){
        if(game.phase.name !== gamePhase.NIGHTTIME){
            const player = game.players.find(p => !p.flip);
            const player2 = game.players.find(p => !p.flip && p.userID !== player.userID);
            await actionService.submitActionMessage(player.userID,
                { message: `vote ${player2.name}` });
        }
        await phaseService.setExpiration(game.joinID, 0);
        game = await gameService.getByID(game.id);
    }
}
