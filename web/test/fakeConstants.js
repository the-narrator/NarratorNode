module.exports = {
    JOIN_ID: 'ABCD',

    AUTH_TOKEN: 'test auth token',

    COLOR: '#6495EE',

    EXTERNAL_USER_ID: 'EXTERNAL_USER_ID',
    EXTERNAL_USER_ID2: 'EXTERNAL_USER_ID2',
    EXTERNAL_USER_ID3: 'EXTERNAL_USER_ID3',

    FACTION_NAME: 'FakeTown',

    FEATURED_KEY: 'featureKey',
    FEATURED_PRIORITY: 12,

    FIREBASE_AUTH_TOKEN: 'firebaseAuthToken',
    FIREBASE_EXTERNAL_ID: 'firebase',

    GAME_ID: 1234,

    HIDDEN_NAME: 'FakeTown Hidden',

    PLAYER_NAME: 'PLAYERNAME',

    ROLE_NAME: 'Fake Citizen',

    SETUP_DESCRIPTION: 'Setup Description',
    SETUP_NAME: 'Test setup name',
    SETUP_SLOGAN: 'Setup slogan',

    SC2MAFIA_PM_ID: 'PM_ID',
    SC2MAFIA_SIGNUP_THREAD_ID: 11111,
    SC2MAFIA_SETUP_THREAD_ID: 22222,
    SC2MAFIA_GAME_THREAD_ID: 11001100,
    SC2MAFIA_POST_ID: 33333,
};
