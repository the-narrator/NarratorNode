const userService = require('../services/userService');

const fakeConstants = require('./fakeConstants');


function fakeFirebaseHeader(){
    return {
        authType: 'firebase',
        auth: fakeConstants.FIREBASE_AUTH_TOKEN,
    };
}

function fakeTempAuthHeader(userID){
    const authToken = userService.addTempAuth(userID);
    return {
        authType: 'tempAuth',
        auth: authToken,
    };
}

module.exports = {
    fakeFirebaseHeader,
    fakeTempAuthHeader,
};
