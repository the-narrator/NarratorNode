const fakeConstants = require('./fakeConstants');

function auth(){
    return {
        verifyIdToken,
    };
}

const credential = {
    cert: firebaseFile => {}, /* eslint-disable-line no-unused-vars */
};

function initializeApp(){

}

module.exports = {
    auth,
    credential,
    initializeApp,
};

async function verifyIdToken(){
    return {
        user_id: fakeConstants.FIREBASE_EXTERNAL_ID,
        firebase: {
            sign_in_provider: 'anonymous',
        },
    };
}
