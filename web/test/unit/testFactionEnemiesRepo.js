require('../init');
const { expect } = require('chai');

const factionRepo = require('../../repos/factionRepo');
const factionEnemiesRepo = require('../../repos/factionEnemiesRepo');

const quasiSetup = require('../quasiModels/quasiSetup');

const sqlErrors = require('../../repos/util/sqlErrors');


describe('Faction Enemies Repo', () => {
    let setup;
    beforeEach(async() => {
        setup = await quasiSetup.create();
    });

    it('Should not allow non existent factions in the first column', async() => {
        const [faction1] = setup.factions;
        await factionEnemiesRepo.deleteAll();

        try{
            await factionEnemiesRepo.setEnemies(faction1.id, faction1.id * 100, setup.id);
        }catch(err){
            return expect(err.errno).to.be.equal(sqlErrors.NONEXISTANT_ID);
        }
        throw new Error('Expected an error and didn\'t get one!');
    });

    it('Should not allow non existent factions in the second column', async() => {
        const [faction1] = setup.factions;
        await factionEnemiesRepo.deleteAll();

        try{
            await factionEnemiesRepo.setEnemies(faction1.id * 100, faction1.id, setup.id);
        }catch(err){
            return expect(err.errno).to.be.equal(sqlErrors.NONEXISTANT_ID);
        }
        throw new Error('Expected an error and didn\'t get one!');
    });

    it('should not allow setup_id to be null', async() => {
        const [faction1, faction2] = setup.factions;
        await factionEnemiesRepo.deleteAll();

        try{
            await factionEnemiesRepo.setEnemies(faction1.id, faction2.id);
        }catch(err){
            return expect(err.errno).to.be.equal(sqlErrors.BAD_NULL);
        }
        throw new Error('Expected an error and didn\'t get one!');
    });

    it('should not allow a non existant setup id', async() => {
        const [faction1, faction2] = setup.factions;
        await factionEnemiesRepo.deleteAll();

        try{
            await factionEnemiesRepo.setEnemies(faction1.id, faction2.id, 0);
        }catch(err){
            return expect(err.errno).to.be.equal(sqlErrors.NONEXISTANT_ID);
        }
        throw new Error('Expected an error and didn\'t get one!');
    });

    it('Should not allow duplicate entries in the db', async() => {
        const [faction1, faction2] = setup.factions;
        await factionEnemiesRepo.deleteAll();
        await factionEnemiesRepo.setEnemies(faction1.id, faction2.id, setup.id);

        try{
            await factionEnemiesRepo.setEnemies(faction1.id, faction2.id, setup.id);
        }catch(err){
            return expect(err.errno).to.be.equal(sqlErrors.DUPLICATE_ROW);
        }
        throw new Error('Expected an error and didn\'t get one!');
    });

    it('Should not allow reversed duplicate entries in the db', async() => {
        const [faction1, faction2] = setup.factions;
        await factionEnemiesRepo.deleteAll();
        await factionEnemiesRepo.setEnemies(faction2.id, faction1.id, setup.id);

        try{
            await factionEnemiesRepo.setEnemies(faction1.id, faction2.id, setup.id);
        }catch(err){
            return expect(err.errno).to.be.equal(sqlErrors.DUPLICATE_ROW);
        }
        throw new Error('Expected an error and didn\'t get one!');
    });

    it('Should delete faction enemy entries on faction delete', async() => {
        const [faction1, faction2] = setup.factions;
        await factionEnemiesRepo.deleteAll();
        await factionEnemiesRepo.setEnemies(faction2.id, faction1.id, setup.id);

        await factionRepo.deleteByID(setup.id, faction1.id);
        const factionEnemyIDs = await factionEnemiesRepo.getBySetupID(setup.id);

        factionEnemyIDs.forEach(([enemyID1, enemyID2]) => {
            expect(faction1).to.not.be.equal(enemyID1);
            expect(faction1).to.not.be.equal(enemyID2);
        });
    });
});
