require('../init');

const { expect } = require('chai');

const playerRepo = require('../../repos/playerRepo');
const playerRolesRepo = require('../../repos/playerRolesRepo');
const gameRepo = require('../../repos/replayRepo');

const quasiGame = require('../quasiModels/quasiGame');

const sqlErrors = require('../../repos/util/sqlErrors');

describe('Game Spawns Repo', () => {
    let game;
    beforeEach(async() => {
        game = await quasiGame.create({
            isStarted: true,
            playerCount: 3,
        });
    });

    it('Should not allow duplicate setup hiddens', async() => {
        const setupHiddenIDs = game.setup.setupHiddens.map(setupHidden => setupHidden.id);
        const [player] = game.players;
        const playerID = (await playerRepo.getByNameAndUserID(player.name, player.userID)).id;
        const newSetupHiddenID = setupHiddenIDs.find(id => id !== player.setupHiddenID);

        try{
            await playerRolesRepo.insert(game.id, newSetupHiddenID, playerID);
        }catch(err){
            return expect(err.errno).to.be.equal(sqlErrors.DUPLICATE_ROW);
        }
        throw new Error('Expected an error and didn\'t get one!');
    });

    it('Should not allow duplicate players', async() => {
        const setupHiddenID = game.players[0].setupHiddenID;
        const [newPlayer] = game.players.filter(player => player.setupHiddenID !== setupHiddenID);
        const playerID = (await playerRepo.getByNameAndUserID(newPlayer.name, newPlayer.userID)).id;

        try{
            await playerRolesRepo.insert(game.id, setupHiddenID, playerID);
        }catch(err){
            return expect(err.errno).to.be.equal(sqlErrors.DUPLICATE_ROW);
        }
        throw new Error('Expected an error and didn\'t get one!');
    });

    it('Should delete game spawns when a game is deleted', async() => {
        // no given

        await gameRepo.deleteAll();
        const playerRoles = await playerRolesRepo.getByGameID(game.id);

        expect(playerRoles).to.deep.eql({});
    });
});
