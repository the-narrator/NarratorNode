require('../init');

const { expect } = require('chai');

const gameSpawnsRepo = require('../../repos/gameSpawnsRepo');
const gameRepo = require('../../repos/replayRepo');

const quasiGame = require('../quasiModels/quasiGame');

const helpers = require('../../utils/helpers');

const sqlErrors = require('../../repos/util/sqlErrors');

describe('Game Spawns Repo', () => {
    let game;
    beforeEach(async() => {
        game = await quasiGame.create({
            isStarted: true,
            playerCount: 3,
        });
    });

    it('Should not allow duplicate setup hiddens', async() => {
        const factionRoleIDs = getFactionRoleIDs(game.setup);
        const setupHidden = game.setup.setupHiddens[0];
        const newFactionRoleID = factionRoleIDs.find(id => id !== setupHidden.factionRoleID);

        try{
            await gameSpawnsRepo.insert(game.id, setupHidden.id, newFactionRoleID);
        }catch(err){
            return expect(err.errno).to.be.equal(sqlErrors.DUPLICATE_ROW);
        }
        throw new Error('Expected an error and didn\'t get one!');
    });

    it('Should delete game spawns when a game is deleted', async() => {
        // no given

        await gameRepo.deleteAll();
        const gameSpawns = await gameSpawnsRepo.getByGameID(game.id);

        expect(gameSpawns).to.deep.eql({});
    });
});

function getFactionRoleIDs(setup){
    const nestedIDs = setup.factions.map(faction => faction.factionRoles
        .map(factionRole => factionRole.id));
    return helpers.flattenList(nestedIDs);
}
