require('../init');
const { expect } = require('chai');

const gameRepo = require('../../repos/replayRepo');
const gameModifierRepo = require('../../repos/gameModifierRepo');

const quasiGame = require('../quasiModels/quasiGame');

const sqlErrors = require('../../repos/util/sqlErrors');


describe('Game Settings Repo', () => {
    let game;
    beforeEach(async() => {
        game = await quasiGame.create();
    });

    it('Should not allow duplicate gameID / name combinations in int table', async() => {
        await gameModifierRepo.insert(game.id, 'DAY_LENGTH_START', 30);

        try{
            await gameModifierRepo.insert(game.id, 'DAY_LENGTH_START', 30);
        }catch(err){
            return expect(err.errno).to.be.equal(sqlErrors.DUPLICATE_ROW);
        }
        throw new Error('Expected an error and didn\'t get one!');
    });

    it('Should not allow duplicate gameID / name combinations in bool table', async() => {
        await gameModifierRepo.insert(game.id, 'OMNISCIENT_DEAD', false);

        try{
            await gameModifierRepo.insert(game.id, 'OMNISCIENT_DEAD', false);
        }catch(err){
            return expect(err.errno).to.be.equal(sqlErrors.DUPLICATE_ROW);
        }
        throw new Error('Expected an error and didn\'t get one!');
    });

    it('Should delete game modifiers when a game is deleted', async() => {
        // settings are automatically created upon game creation

        await gameRepo.deleteAll();
        const modifiers = await gameModifierRepo.getByGameID(game.id);

        expect(modifiers).to.deep.eql({});
    });
});
