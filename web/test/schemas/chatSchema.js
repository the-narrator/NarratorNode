const baseValidator = require('../../validators/baseValidator');


const chatSchema = {
    type: 'object',
    properties: {
        createdAt: {
            type: 'date-time',
            required: true,
        },
        id: {
            type: 'number',
            required: true,
        },
        text: {
            type: 'string',
            required: true,
        },
        userID: {
            type: 'number',
            required: true,
        },
    },
};

function check(query){
    baseValidator.validate(query, chatSchema);
}

module.exports = {
    check,
};
