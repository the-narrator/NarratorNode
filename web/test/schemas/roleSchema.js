const baseValidator = require('../../validators/baseValidator');

const { abilityDefinition } = require('./abilitySchema');


const roleDefinition = {
    abilities: {
        type: 'array',
        items: {
            type: 'object',
            properties: abilityDefinition,
            required: true,
        },
        required: true,
    },
    id: {
        type: 'number',
        required: true,
    },
};

const roleSchema = {
    type: 'object',
    properties: roleDefinition,
};

function check(query){
    baseValidator.validate(query, roleSchema);
}

module.exports = {
    roleDefinition,

    check,
};
