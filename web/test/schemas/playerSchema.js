const baseValidator = require('../../validators/baseValidator');

const { setupDefinition } = require('./setupSchema');


const playerInGameDefinition = {
    votePower: {
        type: 'number',
        required: true,
    },
};

const playerInGameSchema = {
    type: 'object',
    properties: playerInGameDefinition,
};

const gameUserDeleteResponseSchema = {
    type: 'object',
    properties: {
        moderatorIDs: {
            type: 'array',
            items: {
                type: 'number',
            },
            required: true,
        },
        setup: {
            type: 'object',
            required: true,
            properties: setupDefinition,
        },
    },
};

const playerDeleteResponseSchema = {
    type: 'object',
    properties: {
        setup: {
            type: 'object',
            required: true,
            properties: setupDefinition,
        },
    },
};

const nameUpdateSchema = {
    type: 'object',
    properties: {
        playerName: {
            type: 'string',
            required: true,
        },
        playerID: {
            type: 'number',
            required: true,
        },
    },
};

function checkGameUserDeleteResponse(response){
    baseValidator.validate(response, gameUserDeleteResponseSchema);
}

function checkPlayerDeleteResponse(response){
    baseValidator.validate(response, playerDeleteResponseSchema);
}

function checkNameUpdate(response){
    baseValidator.validate(response, nameUpdateSchema);
}

function checkInGame(query){
    baseValidator.validate(query, playerInGameSchema);
}

module.exports = {
    playerInGameDefinition,

    checkGameUserDeleteResponse,
    checkPlayerDeleteResponse,
    checkNameUpdate,
    checkInGame,
};
