const baseValidator = require('../../validators/baseValidator');

const { abilityDefinition } = require('./abilitySchema');


const factionAbilityDefinition = {
    ...abilityDefinition,
};

const factionAbilitySchema = {
    type: 'object',
    properties: factionAbilityDefinition,
};

function check(query){
    baseValidator.validate(query, factionAbilitySchema);
}

module.exports = {
    check,
};
