const baseValidator = require('../../validators/baseValidator');


const factionDefinition = {
    id: {
        type: 'number',
        required: true,
    },
};

const factionSchema = {
    type: 'object',
    properties: factionDefinition,
};

function check(query){
    baseValidator.validate(query, factionSchema);
}

module.exports = {
    check,
};
