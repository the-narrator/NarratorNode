const baseValidator = require('../../validators/baseValidator');


const factionRoleDefinition = {
    id: {
        type: 'number',
        required: true,
    },
};

const factionRoleSchema = {
    type: 'object',
    properties: factionRoleDefinition,
};

function check(query){
    baseValidator.validate(query, factionRoleSchema);
}

module.exports = {
    check,
};
