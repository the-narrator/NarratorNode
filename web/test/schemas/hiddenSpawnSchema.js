const baseValidator = require('../../validators/baseValidator');


const hiddenSpawnDefinition = {
    id: {
        type: 'number',
        required: true,
    },
    hiddenID: {
        type: 'number',
        required: true,
    },
    factionRoleID: {
        type: 'number',
        required: true,
    },
    maxPlayerCount: {
        required: true,
        type: 'number',
    },
    minPlayerCount: {
        required: true,
        type: 'number',
    },
};

const hiddenSpawn = {
    type: 'object',
    properties: hiddenSpawnDefinition,
};

function check(query){
    baseValidator.validate(query, hiddenSpawn);
}

module.exports = {
    check,
};
