const { expect } = require('chai');

function expectStatusCode(expectedCode, response){
    try{
        expect(response.statusCode).to.equal(expectedCode);
    }catch(e){
        if(response.json && expectedCode === 200)
            throw { statusCode: response.statusCode, errors: response.json.errors };
        const stringErrors = JSON.stringify(response.json);
        throw new Error(
            `expected ${expectedCode} but got ${response.statusCode} : ${stringErrors}`,
        );
    }
}

module.exports = {
    expectStatusCode,
};
