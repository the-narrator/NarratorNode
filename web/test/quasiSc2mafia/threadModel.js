const POSTS_PER_PAGE = 2;

function get(attributes = {}){
    return new Thread(attributes.posts);
}

module.exports = {
    get,

    POSTS_PER_PAGE,
};

function Thread(posts){
    this.posts = posts;
}

Thread.prototype.getPage = function(pageNumber){
    const index = pageNumber - 1;
    const endPostNumber = Math.min(this.posts.length, pageNumber * POSTS_PER_PAGE);
    const page = {
        perpage: POSTS_PER_PAGE,
        postbits: this.posts.slice(index * POSTS_PER_PAGE, endPostNumber),
        totalposts: this.posts.length,
    };

    const pageNav = getPageNav(this, pageNumber);
    if(pageNav)
        page.pagenav = pageNav;

    if(page.postbits.length === 1)
        page.postbits = page.postbits[0];

    return page;
};

Thread.prototype.getPageCount = function(){
    return Math.ceil(this.posts.length / POSTS_PER_PAGE);
};

function getPageNav(thread, pageNumber){
    const totalPages = Math.ceil(thread.posts.length / POSTS_PER_PAGE);
    if(thread.posts.length > POSTS_PER_PAGE)
        return {
            nextpage: Math.min(pageNumber + 1, totalPages),
            pagenumber: pageNumber,
            totalpages: totalPages,
        };
}
