const { EXTERNAL_USER_ID, SC2MAFIA_POST_ID } = require('../fakeConstants');


function get(attributes = {}){
    return {
        post: {
            username: attributes.externalUserID || EXTERNAL_USER_ID,
            message_bbcode: attributes.text || '',
            postid: attributes.id || SC2MAFIA_POST_ID,
        },
    };
}

module.exports = {
    get,
};
