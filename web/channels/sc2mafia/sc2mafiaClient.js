const helpers = require('../../utils/helpers');

const { THREAD_THROTTLE } = require('./constants');

const userType = require('../../models/enums/userTypes');

const sc2mafiaGameRepo = require('./repos/sc2mafiaGameRepo');
const gameRepo = require('../../repos/replayRepo');
const restClient = require('./sc2mafiaRestClient');

const newGameRequest = require('./mappers/newGameRequest');
const newSignsRequest = require('./mappers/newSignsRequest');
const playerVotesRequest = require('./mappers/playerVotesRequest');


const cache = {};

function clearCache(threadID){
    delete cache[threadID];
}

function connect(){
    return restClient.connect();
}

async function getCondorcetVotes(threadID){
    if(!cache[threadID])
        cache[threadID] = getEmptyCache();

    let threadResults = await restClient.getNewPosts(threadID, null, cache[threadID].lastPage);
    cache[threadID] = addCondorcetVoteResults(cache[threadID], threadResults.posts, threadID);

    while (cache[threadID].lastPage !== threadResults.lastPage){
        const lastPage = Math.min(threadResults.lastPage,
            cache[threadID].lastPage + THREAD_THROTTLE);
        const queryPromises = [];
        for(let i = threadResults.nextPage; threadResults.nextPage && i <= lastPage; i++)
            queryPromises.push(restClient.getNewPosts(threadID, null, i));

        const queryResults = await Promise.all(queryPromises);
        for(let i = 0; i < queryResults.length; i++){
            threadResults = queryResults[i];
            cache[threadID] = addCondorcetVoteResults(cache[threadID], threadResults.posts,
                threadID);
        }
        cache[threadID].lastPage = lastPage;
    }

    return cache[threadID];
}

async function findGameDeltas(lastCheckedTime){
    const pmOverviews = await restClient.getPMs();
    const pms = await Promise.all(pmOverviews.map(pmOverview => restClient.getPM(pmOverview)));
    const gameRequests = newGameRequest.map(pms, lastCheckedTime);
    // await newPMs.map(pm => restClient.deletePM(pm.id));
    return gameRequests;
}

// eslint-disable-next-line no-unused-vars
function createIntegration(gameID, args){

}

async function hasIntegration(gameID){
    try{
        const game = await gameRepo.getByID(gameID);
        const sc2mafiaGame = await sc2mafiaGameRepo.getByJoinID(game.lobbyID);
        return !!sc2mafiaGame;
    }catch(err){
        return false;
    }
}

// eslint-disable-next-line no-unused-vars
async function onNewPost(threadID){
    const newPosts = await getNewPosts(threadID);
    const playerJoins = newSignsRequest.map(newPosts);
    const playerVotes = playerVotesRequest.map(newPosts);
    return {
        playerJoins,
        playerVotes,
    };
}

async function postGame(user, game, tempAuthToken){
    const setupThreadID = await restClient.postSetup(user, game);
    const signupThreadID = await restClient.postSignups(game, setupThreadID);
    await sc2mafiaGameRepo.insert(game.joinID, setupThreadID, signupThreadID);
    await sendNarratorLink(user.name, tempAuthToken, true);
    return {
        setupThreadID,
        signupThreadID,
    };
}

async function sendNarratorLink(externalUserID, tempAuthToken, isHost){
    return restClient.sendNarratorLink(tempAuthToken, externalUserID, isHost);
}

function onDayStart(request){
    return onNightStart(request);
}

async function onGameStart(){
    // const gameThreadID = await restClient.postGameThread(request);
    // await sc2mafiaGameRepo.updateGameThreadID(game.joinID, gameThreadID);
}

async function onGameEnd(request){
    const gameThreadID = await sc2mafiaGameRepo.getByJoinID(request.joinID);
    if(gameThreadID)
        return Promise.all([
            restClient.onGameEnd(gameThreadID.gameThreadID, request),
            sc2mafiaGameRepo.deleteByJoinID(request.joinID),
        ]);
}

async function onNightStart(request){
    const externalGame = await sc2mafiaGameRepo.getByJoinID(request.joinID);
    if(!externalGame)
        return;
    if(externalGame.gameThreadID)
        return restClient.updateGameStatus(request, externalGame.gameThreadID);
    const gameThreadID = await restClient.postGameThread(request);
    return sc2mafiaGameRepo.updateGameThreadID(request.joinID, gameThreadID);
}

async function onNewPlayer(game){
    const externalGame = await sc2mafiaGameRepo.getByJoinID(game.joinID);
    if(!externalGame)
        return;
    const signupPostID = await restClient.getOpeningPostID(externalGame.signupThreadID);
    return restClient.updateSignups(game, signupPostID, externalGame.setupThreadID);
}

async function onPlayerRemove(game){
    const externalGame = await sc2mafiaGameRepo.getByJoinID(game.joinID);
    if(!externalGame)
        return;
    const signupPostID = await restClient.getOpeningPostID(externalGame.signupThreadID);
    return restClient.updateSignups(game, signupPostID, externalGame.setupThreadID);
}

async function onRoleCardUpdate(request){
    const userService = require('../../services/userService');
    const user = await userService.getNestedInfo(request.userID);
    const integrations = user.integrations
        .filter(integration => integration.type === userType.SC2MAFIA);
    if(integrations.length)
        return restClient.sendRoleCardUpdate(request, integrations[0].externalID);
}

async function onSetupChange({ joinID }){
    const externalGame = await sc2mafiaGameRepo.getByJoinID(joinID);
    if(!externalGame)
        return;
    const setupPostID = await restClient.getOpeningPostID(externalGame.setupThreadID);
    const gameService = require('../../services/gameService');
    let game = await gameService.getByLobbyID(joinID);
    game = await gameService.filterNeededUserInfo(game);
    return restClient.updateSetup(game, setupPostID);
}

async function onLobbyClose(request){
    const externalGame = await sc2mafiaGameRepo.getByJoinID(request.joinID);
    if(!externalGame)
        return;
    const promises = [
        restClient.abandonSignupThread(externalGame.signupThreadID),
        restClient.abandonSetupThread(externalGame.setupThreadID),
        // restClient.deleteThread(44645);// externalGame.signupThreadID);
        sc2mafiaGameRepo.deleteByJoinID(request.joinID),
    ];
    return Promise.all(promises);
}

async function onNewVote(request){
    const externalGame = await sc2mafiaGameRepo.getByJoinID(request.joinID);
    if(externalGame)
        return restClient.postVote(request, externalGame.gameThreadID);
}

module.exports = {
    clearCache,
    connect,
    getCondorcetVotes,
    createIntegration,
    hasIntegration,

    findGameDeltas,
    onNewPost,
    postGame,
    sendNarratorLink,

    onDayStart,
    onFactionCreate: onSetupChange,
    onFactionDelete: onSetupChange,
    onFactionModifierUpdate: onSetupChange,
    onFactionRoleAbilityModifierUpdate: onSetupChange,
    onFactionRoleModifierUpdate: onSetupChange,
    onGameEnd,
    onGameStart,
    onLobbyClose,
    onNewPlayer,
    onNewVote,
    onNightStart,
    onPlayerRemove,
    onRoleCardUpdate,
    onRoleAbilityModifierUpdate: onSetupChange,
    onSetupChange,
    onSetupModifierUpdate: onSetupChange,
};

function addCondorcetVoteResults(currentResults, posts, threadID){
    const newResults = Object.assign({}, currentResults);
    if(!currentResults.options.length)
        resetCondorcetVoteOptions(newResults, posts[0]);

    posts.forEach(post => {
        const { externalUserID } = post;
        let voteSubmission;
        try{
            voteSubmission = getCondorcetInputFromPost(post, newResults.options);
        }catch(err){}
        if(!voteSubmission)
            return;
        if(externalUserID === newResults.voteController){
            try{
                resetCondorcetVoteOptions(newResults, post);
            }catch(err){}
        }else{
            const postID = post.externalID;
            newResults.voterMetadata[externalUserID] = {
                ranking: voteSubmission,
                url: `http://sc2mafia.com/forum/showthread.php/${threadID}`
                    + `?p=${post.externalID}&viewfull=1#post${postID}`,
            };
        }
    });
    return newResults;
}

function getCondorcetInputFromPost(post, options){
    let text = post.message;

    const startTag = getStartTag(text);
    if(!startTag)
        return;
    const endTag = getNextTag(text, startTag);
    if(endTag === null) // unescaped tag
        return;

    if(!startTag.isCondorcet){
        const newTextToSearch = text.substring(endTag.nextCharIndex, text.length);
        return getCondorcetInputFromPost(
            Object.assign({}, post, { message: newTextToSearch }), options,
        );
    }

    text = text.substring(startTag.nextCharIndex, endTag.startCharIndex);

    let ranking = text.split(',').join('\n')
        .split('\n')
        .filter(message => message); // filters out empties
    ranking = ranking.filter(helpers.onlyUniqueFilterFunction);
    if(!options)
        return ranking;
    return ranking.map(option => (typeof(option) === 'object' ? option : [option]));
}

function getEmptyCache(){
    return {
        lastPage: 1,
        options: [],
    };
}

async function getNewPosts(threadID){
    const game = await sc2mafiaGameRepo.getByThreadID(threadID);
    if(!game)
        return [];
    let latestPostID = game.latestPostID;
    let threadResults = await restClient.getNewPosts(threadID, latestPostID);
    let posts = threadResults.posts;
    while (!threadResults.isLastPage){
        threadResults = await restClient.getNewPosts(threadID, null, threadResults.nextPage);
        posts = posts.concat(threadResults.posts);
    }
    if(posts.length){
        latestPostID = posts[posts.length - 1].externalID;
        await sc2mafiaGameRepo.updateLatestPostID(game.joinID, latestPostID);
    }
    posts.forEach(post => {
        post.joinID = game.joinID;
    });
    return posts;
}

function getNextTag(text, startTag){
    text = text.substring(startTag.nextCharIndex).toLowerCase();

    const tagStartIndex = text.indexOf(`[/${startTag.tagType}]`);
    if(tagStartIndex === -1)
        return null;

    return {
        nextCharIndex: startTag.nextCharIndex + tagStartIndex + startTag.tagType.length + 3,
        startCharIndex: startTag.nextCharIndex + tagStartIndex,
    };
}

function getStartTag(text){
    const startIndex = text.indexOf('[');
    if(startIndex === -1)
        return;

    text = text.substring(startIndex + 1).toLowerCase();
    const endTagIndex = text.search(/[^A-Za-z]/);
    const nextCharIndex = text.indexOf(']');
    const noEndTagSpoiler = 'Could not find close of tag';
    if(nextCharIndex === -1)
        throw noEndTagSpoiler;

    const tagText = text.substring(0, nextCharIndex);
    const tagType = text.substring(0, endTagIndex);

    return {
        isCondorcet: tagText === 'spoiler=cvote',
        tagType,
        nextCharIndex: startIndex + nextCharIndex + 2,
    };
}

function resetCondorcetVoteOptions(newResults, post){
    newResults.voteController = post.externalUserID;
    newResults.options = getCondorcetInputFromPost(post) || newResults.options;
    newResults.voterMetadata = {};
}
