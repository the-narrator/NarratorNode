const config = require('../../../config');

const vBulletinClient = require('./vBulletinClient.js');

const abandonSignupThreadRequest = require('./restMappers/abandonSignupThreadRequest');
const abandonSetupThreadRequest = require('./restMappers/abandonSetupThreadRequest');
const deletePMRequest = require('./restMappers/deletePMRequest');
const gameEndRequest = require('./restMappers/gameEndRequest');
const getNewPostsRequest = require('./restMappers/getNewPostsRequest');
const getOpeningPostIDRequest = require('./restMappers/getOpeningPostIDRequest');
const getPMRequest = require('./restMappers/getPMRequest');
const getPMsRequest = require('./restMappers/getPMsRequest');
const postGameThreadRequest = require('./restMappers/postGameThreadRequest');
const postSetupRequest = require('./restMappers/postSetupRequest');
const postSignupsRequest = require('./restMappers/postSignupsRequest');
const postVoteRequest = require('./restMappers/postVoteRequest');
const roleCardUpdateRequest = require('./restMappers/roleCardUpdateRequest.js');
const sendNarratorLinkRequest = require('./restMappers/sendNarratorLinkRequest');
const updateGameStatusRequest = require('./restMappers/updateGameStatusRequest');
const updateSetupRequest = require('./restMappers/updateSetupMapper');
const updateSignupsRequest = require('./restMappers/updateSignupsRequest');


async function connect(){
    const initConfig = {
        apiUrl: 'https://www.sc2mafia.com/forum/api.php',
        apiKey: config.sc2mafia.api_key,
        platformName: 'testing script',
        platformVersion: '1',
    };
    const credentials = {
        username: 'TheNarrator',
        password: config.sc2mafia.password,
    };
    await vBulletinClient.apiInit(initConfig);
    setInterval(async() => {
        vBulletinClient.login(credentials);
    }, 1000 * 60 * 10);
    return vBulletinClient.login(credentials);
}

async function getNewPosts(threadID, lastSeenPostID, pageNumber){
    const request = getNewPostsRequest.logicToRequest(threadID, lastSeenPostID, pageNumber);
    const response = await vBulletinClient.getThread(request);
    return getNewPostsRequest.responseToLogic(response, lastSeenPostID);
}

async function getOpeningPostID(threadID){
    const request = getOpeningPostIDRequest.logicToRequest(threadID);
    const response = await vBulletinClient.getThread(request);
    return getOpeningPostIDRequest.responseToLogic(response);
}

async function getPM(pm){
    const request = getPMRequest.logicToRequest(pm);
    const response = await vBulletinClient.getPrivateMessage(request);
    return getPMRequest.responseToLogic(response);
}

async function getPMs(){
    const response = await vBulletinClient.getPrivateMessages();
    return getPMsRequest.responseToLogic(response);
}

// not working
function deletePM(pmID){
    const request = deletePMRequest.logicToRequest(pmID);
    return vBulletinClient.deletePM(request);
}

function sendNarratorLink(authToken, name, isHost){
    const request = sendNarratorLinkRequest.logicToRequest(authToken, name, isHost);
    return vBulletinClient.sendPrivateMessage(request);
}

async function postSetup(user, game){
    const request = postSetupRequest.logicToRequest(user, game);
    const response = await vBulletinClient.newThread(request);
    return postSetupRequest.responseToLogic(response);
}

async function postSignups(game, setupThreadID){
    const request = postSignupsRequest.logicToRequest(game, setupThreadID);
    const response = await vBulletinClient.newThread(request);
    return postSetupRequest.responseToLogic(response);
}

function updateSetup(game, setupPostID){
    const request = updateSetupRequest.logicToRequest(game, setupPostID);
    return vBulletinClient.editPost(request);
}

function updateSignups(game, signupPostID, setupThreadID){
    const request = updateSignupsRequest.logicToRequest(game, signupPostID, setupThreadID);
    return vBulletinClient.editPost(request);
}

function abandonSignupThread(threadID){
    const request = abandonSignupThreadRequest.logicToRequest(threadID);
    return vBulletinClient.newPost(request);
}

function abandonSetupThread(threadID){
    const request = abandonSetupThreadRequest.logicToRequest(threadID);
    return vBulletinClient.newPost(request);
}

function sendRoleCardUpdate(roleCard, externalUserID){
    const request = roleCardUpdateRequest.logicToRequest(roleCard, externalUserID);
    return vBulletinClient.sendPrivateMessage(request);
}

async function postGameThread(game){
    const request = postGameThreadRequest.logicToRequest(game);
    const response = await vBulletinClient.newThread(request);
    return postGameThreadRequest.responseToLogic(response);
}

async function updateGameStatus(game, gameThreadID){
    const request = updateGameStatusRequest.logicToRequest(game, gameThreadID);
    return vBulletinClient.newPost(request);
}

function postVote(voteRequest, gameThreadID){
    const request = postVoteRequest.logicToRequest(voteRequest, gameThreadID);
    return vBulletinClient.newPost(request);
}

function deleteThread(threadID){
    return vBulletinClient.modCloseThread(threadID);
}

function onGameEnd(gameThreadID, endGameRequest){
    const request = gameEndRequest.logicToRequest(gameThreadID, endGameRequest);
    return vBulletinClient.newPost(request);
}

module.exports = {
    connect,

    getNewPosts,
    getOpeningPostID,
    getPM,
    getPMs,
    deletePM,

    sendNarratorLink,
    postSetup,
    postSignups,
    updateSetup,
    updateSignups,

    sendRoleCardUpdate,
    postGameThread,
    updateGameStatus,

    postVote,

    abandonSetupThread,
    abandonSignupThread,
    deleteThread,

    onGameEnd,
};
