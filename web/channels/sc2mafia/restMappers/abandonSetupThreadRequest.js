function logicToRequest(threadID){
    return {
        threadid: threadID,
        message: 'This setup has been abandoned.',
        signature: true,
    };
}

module.exports = {
    logicToRequest,
};
