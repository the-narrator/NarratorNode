const CIRCLE_JERK = 336;

const updateSetupMapper = require('./updateSetupMapper');


function logicToRequest(user, game){
    return {
        forumid: CIRCLE_JERK,
        subject: `A-FM ${game.setup.name} (Setup)`,
        message: updateSetupMapper.logicToRequest(game).message,
    };
}

function responseToLogic(response){
    return response.threadid;
}

module.exports = {
    logicToRequest,
    responseToLogic,
};
