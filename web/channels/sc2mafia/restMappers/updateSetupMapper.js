const hiddenSpawnUtil = require('../../../utils/hiddenSpawnUtil');
const modifierUtil = require('../../../utils/modifierUtil');
const setupHiddenUtil = require('../../../utils/setupHiddenUtil');
const utils = require('./utils');
const { getHostName } = require('../../../utils/lobbyUtil');

const DEFAULT_GOLDEN = utils.DEFAULT_GOLDEN;
const DEFAULT_WHITE = utils.DEFAULT_WHITE;


function getRolesList(game){
    const hiddens = {};
    const factionRoleIDToColor = {};
    game.setup.hiddens.forEach(hidden => {
        hiddens[hidden.id] = hidden;
    });
    game.setup.factions.forEach(faction => {
        faction.factionRoles.forEach(factionRole => {
            factionRoleIDToColor[factionRole.id] = faction.color;
        });
    });
    const playerCount = game.players.length;
    const isHiddenSpawnSpawnable = hiddenSpawn => hiddenSpawnUtil
        .isSpawnable(playerCount, hiddenSpawn);
    const setupHiddens = game.setup.setupHiddens
        .filter(setupHidden => setupHiddenUtil.isSpawnable(playerCount, setupHidden))
        .map(setupHidden => hiddens[setupHidden.hiddenID])
        .map(({ name, spawns }) => [
            name, spawns.filter(isHiddenSpawnSpawnable).map(spawn => spawn.factionRoleID),
        ]);
    setupHiddens.sort(([sh1], [sh2]) => sh1.localeCompare(sh2));
    const rolesList = setupHiddens
        .map(([name, factionRoleIDs]) => utils.wrapColor(name,
            getHiddenColor(factionRoleIDs, factionRoleIDToColor)))
        .join('\n');
    return `${rolesList}`;
}

function logicToRequest(game, setupPostID){
    let message = getTitle(game);
    message += `[CENTER]${getRolesList(game)}[/CENTER]\n`;
    message += getGameplayOverview(game);
    message += getTeams(game);
    message += getOrderOfOperations();
    message += getRules(game);
    return {
        postid: setupPostID,
        message,
        reason: 'Setup was updated',
    };
}

module.exports = {
    getRolesList,
    logicToRequest,
};

function getTitle(game){
    const hostName = getHostName(game);
    const message = `[B][U][CENTER]${hostName}'s ${game.setup.name} Setup[/CENTER][/U][/B]\n`;
    return utils.wrapColor(wrapSize(message, 5), DEFAULT_GOLDEN);
}

function getGameplayOverview(game){
    let text = `${utils.wrapColor(wrapSize('General Settings', 4), DEFAULT_WHITE)}\n\n`;
    const overviewBullets = [];
    const setupModifiers = game.setup.setupModifiers;
    if(modifierUtil.getValue(setupModifiers, 'DAY_START'))
        overviewBullets.push('Game will start with a Day 1.');
    else
        overviewBullets.push('Game will start with a Night 0.');
    overviewBullets.push(`Day lengths will be ${game.modifiers.DAY_LENGTH_START.value} seconds.`);
    overviewBullets.push(`Night lengths will be ${game.modifiers.NIGHT_LENGTH.value} seconds  .`);
    text += wrapList(overviewBullets);
    return `${utils.wrapColor(text, DEFAULT_WHITE)}\n`;
}

function getHiddenColor(factionRoleIDs, factionRoleIDToColor){
    const colors = factionRoleIDs
        .map(factionRoleID => factionRoleIDToColor[factionRoleID]);
    const colorSet = new Set(colors);
    if(colorSet.size !== 1)
        return DEFAULT_WHITE;
    return colors[0];
}

function getTeams(game){
    const factions = game.setup.factions.map(f => f);
    factions.sort((f1, f2) => f1.name.localeCompare(f2.name));
    return factions.map(faction => {
        const factionName = `${faction.name} Faction`;
        const factionInfo = getFactionInformation(faction);
        return utils.wrapSpoiler(factionName, factionInfo);
    }).join('\n');
}

function getFactionInformation(faction){
    let text = `${utils.wrapColor(`${wrapSize(faction.name, 5)}\n`, faction.color)}\n`;
    text += `${wrapList([faction.description, getEnemyText(faction)])}\n`;
    if(faction.abilities.length){
        text += `${wrapSize('Available Abilities', 4)}\n\n`;
        text += getFactionAbilities(faction.abilities, faction.color);
        text += '\n';
    }
    text += `${wrapSize('Available Roles', 4)}\n\n`;
    text += getFactionRoles(faction.factionRoles, faction.color);
    return utils.wrapColor(text, DEFAULT_WHITE);
}

function getEnemyText(faction){
    const enemies = [...faction.enemies].sort((e1, e2) => e1.name.localeCompare(e2.name));
    const enemiesBBC = enemies
        .map(enemyFaction => utils.wrapColor(enemyFaction.name, enemyFaction.color))
        .join(', ');
    return `Must eliminate ${enemiesBBC}.`;
}

function getFactionRoles(factionRoles, color){
    factionRoles.sort((fr1, fr2) => ((fr1.name > fr2.name) ? 1 : -1));
    return factionRoles.map(factionRole => {
        let text = `${wrapUnderline(utils.wrapColor(factionRole.name, color))}\n`;
        text += wrapList(factionRole.details);
        return text;
    }).join('\n');
}

function getFactionAbilities(abilities, color){
    return abilities.map(ability => {
        let text = `${wrapUnderline(utils.wrapColor(ability.name, color))}\n`;
        let descriptionList = [ability.description];
        descriptionList = descriptionList.concat(ability.details);
        text += wrapList(descriptionList);
        return text;
    }).join('\n');
}

const orderOfOperations = [
    'Commuting',
    'Grave Diggers, in reverse order',
    'Sending other people to do actions for you',
    'Witch manipulation',
    'Commuting again, if missed the first round',
    'Vistations for sending other people to do actions for you',
    'Oracle/Snitch action',
    'Role Blocking',
    'Jailor Executions',
    'Operator/Intended target switching',
    'Coroner',
    'Bus Driving',
    'Coward hiding',
    'Vesting',
    'Undousing',
    'Bodyguard moving',
    'Dousing',
    'All kill abilities at once: Veteran alerting, Serial Killer stabbing, Disguiser killing, '
        + 'Mass Murderer spree killing, Joker killing, Interceptor, Electromaniac charging, '
        + 'Cult clubbing, Burning, Gun Shooting, Mafia kill',
    'Bodyguard attacks',
    'Jester suicides',
    'Doctors',
    'Disguiser takes persona',
    'Commuting if applicable',
    'Cleaning',
    'Silencing, Disfranchising, Blackmailing, Puppeteering, and Ghost controlling',
    'Framing',
    'Enforcing',
    'Mason Recruiting',
    'Cult visiting',
    'Cult recruiting',
    'Drug Dealing',
    'Items passed out: guns, vests, breadd',
    'Amnesiac remembers',
    'Investigations: Sheriff, Investigator, Scout, Arms Detector',
    'Poisoning',
    'Suiting',
    'Jester annoys',
    'Normal visitations',
    'Role watching: spy, lookout, detective, agent',
];

function getOrderOfOperations(){
    let text = '\n';
    text += `${wrapSize('Order of Operations', 4)}\n\n`;
    text += wrapList(orderOfOperations);
    text = utils.wrapColor(text, DEFAULT_WHITE);
    text += '\n';
    return utils.wrapSpoiler('Order of Operations', text);
}

function getRules(game){
    return `${utils.wrapColor('[B][U]Rules of Conduct[/U][/B]\n', DEFAULT_GOLDEN)
    }1. [URL="http://www.sc2mafia.com/forum/showthread.php/42151-Forum-Mafia-Rules"]Forum Mafia Rules[/URL]\n` // eslint-disable-line max-len
        + '2. Inactive players will be replaced by another player, if necessary, they will be modkilled.\n' // eslint-disable-line max-len
        + `3. To ask the Host a question, you must highlight the question [COLOR="#00FF00"]in green[/COLOR], and mention the host [MENTION=6527]${getHostName(game)}[/MENTION].  You may also pm me on Discord.\n` // eslint-disable-line max-len
        + '4. Minimum of 5 posts per game day.\n'
        + '5. English shall be the only language used.\n'
        + '6. Do not post links to other websites.\n'
        + '7. Pictures are allowed in moderation.\n'
        + '8. Videos are not allowed.\n'
        + '9. Directly quoting any feedback or PM by me is forbidden.\n'
        + '10. Have Fun.';
}

function wrapSize(text, size){
    return `[SIZE=${size}]${text}[/SIZE]`;
}

function wrapList(bullets){
    return `[LIST]${bullets.map(bullet => `[*]${bullet}\n`).join('\n')}[/LIST]`;
}

function wrapUnderline(text){
    return `[U]${text}[/U]`;
}
