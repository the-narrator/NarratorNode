const utils = require('./utils');


function logicToRequest(request, externalUserID){
    let message = 'You are a ';
    message += utils.wrapColor(request.roleName, request.color);
    message += '.';
    return {
        title: 'Role card',
        message,
        recipients: externalUserID,
    };
}

module.exports = {
    logicToRequest,
};
