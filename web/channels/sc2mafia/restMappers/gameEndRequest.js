function logicToRequest(gameThreadID, endGameRequest){
    const winnerNames = endGameRequest.winners.map(winner => winner.name);
    let message;
    if(!winnerNames.length)
        message = 'Nobody won!';
    else if(winnerNames.length === 1)
        message = `${winnerNames[0]} has won!`;
    else
        message = `${winnerNames.join(',')} have won!`;

    return {
        threadid: gameThreadID,
        message,
        signature: true,
    };
}

module.exports = {
    logicToRequest,
};
