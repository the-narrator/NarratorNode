const config = require('../../../../config');


function logicToRequest(authToken, sc2mafiaName, isHost){
    const url = `http://${config.app_url}/?auth_token=${authToken}`;
    let message;
    if(isHost)
        message = 'Here is your link to edit the setup.  You will also use this link to submit '
                    + 'actions.  Please do not share this!\n\n';
    else
        message = 'Here is your link to view the setup interactively.  You will also use this '
                    + 'link to submit actions.  Please do not share this!\n\n';

    return {
        title: 'Narrator Link',
        message: message + wrapURL(url, 'Narrator Link'),
        recipients: sc2mafiaName,
    };
}

module.exports = {
    logicToRequest,
};

function wrapURL(url, text){
    return `[URL="${url}"]${text}[/URL]`;
}
