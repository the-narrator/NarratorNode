const DEFAULT_GOLDEN = '#FFD700';
const DEFAULT_WHITE = 'white';

function wrapBold(text){
    return `[B]${text}[/B]`;
}

function wrapColor(text, colorString){
    return `[COLOR=${colorString}]${text}[/COLOR]`;
}

function wrapSpoiler(title, text){
    return `[SPOILER=${title}]\n${text}[/SPOILER]`;
}

module.exports = {
    DEFAULT_GOLDEN,
    DEFAULT_WHITE,

    wrapBold,
    wrapColor,
    wrapSpoiler,
};
