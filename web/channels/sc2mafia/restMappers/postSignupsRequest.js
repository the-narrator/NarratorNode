const DEFAULT_GOLDEN = '#FFD700';
const DEFAULT_WHITE = 'white';

const CIRCLE_JERK = 336;


function logicToRequest(game, setupThreadID){
    return {
        forumid: CIRCLE_JERK,
        subject: `A-FM ${game.setup.name} (Signups)`,
        message: getSignupPost(game, setupThreadID),
    };
}

function responseToLogic(response){
    return response.threadid;
}

module.exports = {
    logicToRequest,
    responseToLogic,
};

function getSignupPost(game, setupThreadID){
    const url = `http://www.sc2mafia.com/forum/showthread.php/${setupThreadID}`;
    const rolesListSize = game.setup.setupHiddens.length || 3;
    const title = `[B][U][CENTER]A-FM ${game.setup.name} (${rolesListSize}P)[/CENTER][/U][/B]\n`;
    let message = wrapColor(wrapSize(title, 5), DEFAULT_GOLDEN);

    const link = `[B][U][CENTER]${wrapURL(url, 'Link to Setup')}[/CENTER][/U][/B]\n`;
    message += wrapColor(wrapSize(link, 5), DEFAULT_GOLDEN);
    message += hr();
    message += getPlayerSignupsText(game);
    return message;
}

function getPlayerSignupsText(game){
    const rolesListSize = game.setup.setupHiddens.length;
    let text = '\n[B][U][CENTER]Signups[/CENTER][/U][/B]\n';
    text = wrapColor(wrapSize(text, 4), DEFAULT_GOLDEN);
    for(let i = 0; i < rolesListSize; i++){
        let signupSlot = `${i + 1}. `;
        if(game.players[i])
            signupSlot += game.players[i].name;
        signupSlot += '\n';
        text += `[CENTER]${wrapColor(signupSlot, DEFAULT_WHITE)}[/CENTER]`;
    }
    return text;
}

function hr(){
    return '[HR][/HR]';
}

function wrapSize(text, size){
    return `[SIZE=${size}]${text}[/SIZE]`;
}

function wrapColor(text, colorString){
    return `[COLOR=${colorString}]${text}[/COLOR]`;
}

function wrapURL(url, text){
    return `[URL="${url}"]${text}[/URL]`;
}
