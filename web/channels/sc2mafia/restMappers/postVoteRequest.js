const utils = require('./utils');


function logicToRequest(voteRequest, gameThreadID){
    let message = voteRequest.voteText;
    if(voteRequest.voter){ // this is a user that voted
        const voterName = voteRequest.voterName;
        message = message.replace(`${voterName} `, `${utils.wrapBold(voterName)} `);
    }
    if(voteRequest.voteTarget){
        const { voteTargets } = voteRequest;
        voteTargets.forEach(voteTarget => {
            message = message.replace(`${voteTarget}.`, `${utils.wrapBold(voteTarget)}.`);
        });
    }
    return {
        threadid: gameThreadID,
        message,
        signature: true,
    };
}

module.exports = {
    logicToRequest,
};
