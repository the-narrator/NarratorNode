const gamePhase = require('../../../models/enums/gamePhase');
const updateSetupMapper = require('./updateSetupMapper');
const utils = require('./utils');


function logicToRequest(game, gameThreadID){
    let message = '[CENTER]';
    message += getDeadPlayers(game);
    message += getOverview(game);
    message += getCountDownText(game);
    message += getMinVoteCountText(game);
    message += '[/CENTER]';

    return {
        threadid: gameThreadID,
        message,
        signature: true,
    };
}

module.exports = {
    logicToRequest,
};

function getDeadPlayers(game){
    const graveyard = game.graveYard || [];
    let text = graveyard
        .map(grave => {
            let message = `${grave.playerName || grave.name} was `;
            let attacks;
            if(grave.deathType.attacks)
                attacks = grave.deathType.attacks
                    .map(attack => attack.text);
            else
                attacks = grave.deathType;
            message += attacks.join(', ').toLowerCase();
            message += '.\n';
            const roleName = grave.roleName || grave.role.name;
            const color = grave.color || grave.role.color;
            message += `Their role was ${utils.wrapColor(roleName, color)}.`;
            if(grave.lastWill)
                utils.wrapSpoiler(grave.playerName, grave.lastWill);
            return message;
        });
    if(text){
        text = text.join('\n\n');
        text += '\n\n';
    }
    return utils.wrapColor(text, utils.DEFAULT_WHITE);
}

function getOverview(game){
    let text = '[TABLE="width: 800, class: grid, align: center"]';
    text += '[TR][TD]Role List[/TD][td]Living Players[/TD][TD]Graveyard[/TD][/TR]';

    text += '[TR][TD]';
    text += updateSetupMapper.getRolesList(game);
    text += '[/TD]';

    text += '[TD]';
    text += getPlayers(game.players);
    text += '[/TD]';

    text += '[TD]';
    text += getGraveyard(game.players);
    text += '[/TD][/TR]';

    text += '[/TABLE]';

    return utils.wrapColor(text, utils.DEFAULT_WHITE);
}

function getPlayers(players){
    const playerNames = players
        .filter(player => !player.flip)
        .map(player => player.name);
    playerNames.sort();
    return playerNames.join('\n');
}

function getGraveyard(players){
    return players
        .filter(player => player.flip)
        .map(player => {
            const name = player.name;
            const role = utils.wrapColor(player.flip.name, player.flip.color);
            return `${name} [I][${role}][/I]`;
        })
        .join('\n');
}

function getCountDownText(game){
    let text = '\n';
    let time;
    if(game.phase.name === gamePhase.NIGHTTIME){
        time = game.modifiers.NIGHT_LENGTH.value;
        text += `Night 0 ends in ${time} seconds.`;
    }else{
        time = game.modifiers.DAY_LENGTH_START.value;
        text += `Day 1 ends in ${time} seconds.`;
    }
    text = utils.wrapColor(text, utils.DEFAULT_WHITE);
    const days = time / 60 / 60 / 24;
    text += '\n\n';
    text += '[COUNTDOWN]';
    text += `${days}:00:00:00`;
    text += '[/COUNTDOWN]';
    return text;
}

function getLivingCount(players){
    let count = 0;
    for(let i = 0; i < players.length; i++)
        if(!players[i].flip)
            count++;

    return count;
}

function getMinVoteCountText(game){
    if(game.phase.name === gamePhase.NIGHTTIME)
        return '';
    let minCount = getLivingCount(game.players);
    const playerCount = getLivingCount(game.players);
    if(minCount % 2 === 0)
        minCount++;
    minCount /= 2;
    minCount += 0.5;
    return utils.wrapColor(`\n\nWith ${playerCount} left alive, it takes ${minCount} `
        + 'to vote someone out of the game.', utils.DEFAULT_WHITE);
}
