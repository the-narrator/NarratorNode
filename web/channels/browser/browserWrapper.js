const firebase = require('./firebase');

const helpers = require('../../utils/helpers');
const setHelpers = require('../../utils/setHelpers');

const gameUsersRepo = require('../../repos/gameUsersRepo');

const setupHiddenAddEvent = require('./eventMappers/setupHiddenAddEvent');
const setupHiddenRemoveEvent = require('./eventMappers/setupHiddenRemoveEvent');
const timerUpdateEvent = require('./eventMappers/timerUpdateEvent');


async function getActiveGameUsersByUserID(gameID){
    const userIDs = await gameUsersRepo.getUserIDsByGameID(gameID);
    const app = require('../../app');
    const webSocketIDs = app.getSocketUserIDs();
    return setHelpers.intersection(userIDs, webSocketIDs);
}

async function onUserActivityStatusChange(changedUserID, isActive){
    const app = require('../../app');
    const userIDs = await gameUsersRepo.getBySiblingUserID(changedUserID);
    let request = {
        event: 'playerStatusChange',
        isActive,
        userID: changedUserID,
    };
    request = JSON.stringify(request);
    [...userIDs]
        .filter(userID => userID !== changedUserID)
        .forEach(userID => {
            app.socketPush(userID, request);
        });
}

// specific functions above
// overloaded functions below

function getOnlineUserIDs(){
    const app = require('../../app');
    return app.getSocketUserIDs();
}

async function getUserInfoByAuthToken(authToken){
    if(helpers.serviceEnabled('firebase'))
        return firebase.getUserInfoByAuthToken(authToken);
    return {
        externalID: 'externalID',
        isGuest: false,
        name: 'noFirebaseGuest',
    };
}

function createIntegration(){}

function hasIntegration(){
    return Promise.resolve(true);
}

function onDayStart(request){
    pushPhaseChange(request);
    pushTimer(request);
}

async function onGameEnd(request){
    const app = require('../../app');
    const userIDs = await gameUsersRepo.getUserIDsByGameID(request.gameID);
    request = JSON.stringify(request);
    [...userIDs].forEach(userID => {
        app.socketPush(userID, request);
    });
}

async function onHostChange(request){
    const app = require('../../app');
    const userIDs = await gameUsersRepo.getIDsByLobbyID(request.joinID);
    request = JSON.stringify(request);
    [...userIDs].forEach(userID => {
        app.socketPush(userID, request);
    });
}

async function onLobbyChatMessage(request){
    const app = require('../../app');
    const lobbyChatEvent = JSON.stringify({
        ...request,
        event: 'lobbyChatMessageEvent',
    });
    const userIDs = await gameUsersRepo.getUserIDsByGameID(request.gameID);
    [...userIDs].forEach(gameUserID => app.socketPush(gameUserID, lobbyChatEvent));
}

async function onLobbyCreate(game){
    const app = require('../../app');
    const userService = require('../../services/userService');
    const users = await userService.getOnlineUsers();
    const request = JSON.stringify({
        ...game,
        event: 'lobbyCreate',
    });
    return users.forEach(({ id: userID }) => app.socketPush(userID, request));
}

async function onLobbyDelete({ gameID }){
    const app = require('../../app');
    const userService = require('../../services/userService');
    const users = await userService.getOnlineUsers();
    const request = JSON.stringify({
        gameID,
        event: 'lobbyDelete',
    });
    return users.forEach(({ id: userID }) => app.socketPush(userID, request));
}

async function onNewPlayer(request){
    const app = require('../../app');
    const userIDs = await gameUsersRepo.getIDsByLobbyID(request.joinID);
    request = { ...request, event: 'playerAdded' };
    request = JSON.stringify(request);
    [...userIDs]
        .filter(userID => userID !== request.userID)
        .forEach(userID => {
            app.socketPush(userID, request);
        });
}

async function onNewVote(request){
    const app = require('../../app');
    const userIDs = await gameUsersRepo.getIDsByLobbyID(request.joinID);
    request = { ...request, event: 'voteUpdate' };
    request = JSON.stringify(request);
    [...userIDs]
        .filter(userID => userID !== request.userID)
        .forEach(userID => {
            app.socketPush(userID, request);
        });
}

function onNightStart(request){
    pushPhaseChange(request);
    pushTimer(request);
}

async function onPhaseEndBid(request){
    const app = require('../../app');
    const playerService = require('../../services/playerService');
    const userIDs = await gameUsersRepo.getUserIDsByGameID(request.gameID);
    [...userIDs]
        .forEach(async userID => {
            const requestString = JSON.stringify({
                ...request,
                players: await playerService.filterKeys(request, userID),
            });
            app.socketPush(userID, requestString);
        });
}

function onPhaseReset(request){
    pushTimer(request);
}

async function onPlayerNameUpdate(request){
    const app = require('../../app');
    const userIDs = await gameUsersRepo.getIDsByLobbyID(request.joinID);
    request = { ...request, event: 'playerNameUpdate' };
    request = JSON.stringify(request);
    [...userIDs]
        .forEach(userID => {
            app.socketPush(userID, request);
        });
}

async function onPlayerRemove(request){
    const app = require('../../app');
    const userIDs = await gameUsersRepo.getIDsByLobbyID(request.joinID);
    const kickedID = request.userID;
    request = { ...request, event: 'playerExit' };
    request = JSON.stringify(request);
    [...userIDs]
        .forEach(userID => {
            app.socketPush(userID, request);
        });
    const kickEvent = {
        event: 'kicked',
        userID: kickedID,
    };
    app.socketPush(kickedID, JSON.stringify(kickEvent));
}

async function onPlayerUpdate(request){
    const app = require('../../app');
    const userIDs = await gameUsersRepo.getIDsByLobbyID(request.joinID);
    request = JSON.stringify(request);
    userIDs.forEach(userID => {
        app.socketPush(userID, request);
    });
}

async function onSetupChange(request){
    const app = require('../../app');
    const userIDs = await gameUsersRepo.getIDsByLobbyID(request.joinID);
    request = { ...request };
    request.event = 'setupChange';
    request = JSON.stringify(request);
    userIDs.forEach(userID => {
        app.socketPush(userID, request);
    });
}

async function onSetupHiddenAdd({ gameID, ...setupHidden }){
    const app = require('../../app');
    const userIDs = await gameUsersRepo.getUserIDsByGameID(gameID);
    let event = setupHiddenAddEvent.logicToRequest(setupHidden);
    event = JSON.stringify(event);
    userIDs.forEach(userID => {
        app.socketPush(userID, event);
    });
}

async function onSetupHiddenRemove({ gameID, setupHiddenID }){
    const app = require('../../app');
    const userIDs = await gameUsersRepo.getUserIDsByGameID(gameID);
    let event = setupHiddenRemoveEvent.logicToRequest({ setupHiddenID });
    event = JSON.stringify(event);
    userIDs.forEach(userID => {
        app.socketPush(userID, event);
    });
}

async function onTimerUpdate({ joinID, endTime }){
    const app = require('../../app');
    const userIDs = await gameUsersRepo.getIDsByLobbyID(joinID);
    let event = timerUpdateEvent.logicToRequest({ endTime });
    event = JSON.stringify(event);
    userIDs.forEach(userID => {
        app.socketPush(userID, event);
    });
}

async function onTrialStart(request){
    pushTimer(request);
    const app = require('../../app');
    const userIDs = await gameUsersRepo.getIDsByLobbyID(request.joinID);
    request = JSON.stringify(request);
    [...userIDs]
        .forEach(userID => {
            app.socketPush(userID, request);
        });
}

function onVotePhaseReset(request){
    pushTimer(request);
}

function onVotePhaseStart(request){
    pushTimer(request);
    pushPhaseChange(request);
}

module.exports = {
    getActiveGameUsersByUserID,
    onUserActivityStatusChange,

    getOnlineUserIDs,
    getUserInfoByAuthToken,
    createIntegration,
    hasIntegration,

    onDayStart,
    onGameEnd,
    onHostChange,
    onLobbyChatMessage,
    onLobbyCreate,
    onLobbyDelete,
    onNewPlayer,
    onNewVote,
    onNightStart,
    onPhaseEndBid,
    onPhaseReset,
    onPlayerNameUpdate,
    onPlayerRemove,
    onPlayerUpdate,
    onSetupChange,
    onSetupHiddenAdd,
    onSetupHiddenRemove,
    onTimerUpdate,
    onTrialStart,
    onVotePhaseReset,
    onVotePhaseStart,
};

async function pushTimer(request){
    const userIDs = await gameUsersRepo.getIDsByLobbyID(request.joinID);
    const app = require('../../app');
    const phaseService = require('../../services/phaseService');
    const message = JSON.stringify({
        timer: phaseService.getEndTime(request.joinID),
        guiUpdate: true,
    });
    userIDs.forEach(userID => {
        app.socketPush(userID, message);
    });
}

async function pushPhaseChange(request){
    const userIDs = await gameUsersRepo.getIDsByLobbyID(request.joinID);
    const app = require('../../app');
    const profileMap = {};
    (request.profiles || []).forEach(profile => {
        profileMap[profile.userID] = profile;
    });
    request = { ...request };
    userIDs.forEach(userID => {
        request.profile = profileMap[userID] || null;
        const stringRequest = JSON.stringify(request);
        app.socketPush(userID, stringRequest);
    });
}
