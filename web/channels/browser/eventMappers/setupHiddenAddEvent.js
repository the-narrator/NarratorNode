function logicToRequest(setupHidden){
    return {
        event: 'setupHiddenAdd',
        setupHidden,
    };
}

module.exports = {
    logicToRequest,
};
