/* eslint-disable no-unused-vars, func-names */

function NotificationWrapper(){

}

NotificationWrapper.prototype.onActionSubmit = function(request){
    return request.pmContents;
};

NotificationWrapper.prototype.onBroadcast = function(request){
    return request.pmContents;
};

NotificationWrapper.prototype.onDayDeath = function(request){
    return request.contents;
};

NotificationWrapper.prototype.onGameStart = function(request){
    return 'Game has started';
};

NotificationWrapper.prototype.onLobbyClose = function(request){
    return 'Game canceled';
};

NotificationWrapper.prototype.onNewPlayer = function(request){
    return `${request.playerName} has joined the lobby`;
};

NotificationWrapper.prototype.onVote = function(request){
    return request.voteText;
};

module.exports = {
    NotificationWrapper,
};

// announce day end
// aanounce day start
// announce win message
// announce mayor/marshall reveal
// announce day events (explosions, burn, electro)
