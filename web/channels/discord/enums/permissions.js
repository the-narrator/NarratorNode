module.exports = {
    MANAGE_ROLES: {
        label: 'Manage Permissions',
        value: 'MANAGE_ROLES',
    },
    MANAGE_MESSAGES: {
        label: 'Manage Messages',
        value: 'MANAGE_MESSAGES',
    },
    MANAGE_WEBHOOKS: {
        label: 'Manage Webhooks',
        value: 'MANAGE_WEBHOOKS',
    },
};
