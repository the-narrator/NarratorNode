const actionService = require('../../discordServices/actionService');
const channelPrefixService = require('../../discordServices/channelCommandService');
const gameService = require('../../discordServices/gameService');
const messageService = require('../../discordServices/messageService');
const userService = require('../../discordServices/userService');

const discordClient = require('../../discordWrapper');
const discordJsonClient = require('../../discordJsonClient');
const discordUserClient = require('../../discordUserClient');

const helpers = require('../../../../utils/helpers');

const Constants = require('../../Constants');
const { PRIVATE } = require('../../enums/channelTypes');


const ALLOWED_NOT_JOINED_COMMANDS = [
    Constants.setup,
    Constants.commands,
    Constants.help,
    Constants.echo,
    Constants.info,
    Constants.link,
    Constants.status,
    Constants.host,
    Constants.join,
    Constants.play,
    Constants.in,
    Constants.autotag,
    Constants.setups,
    Constants.changecommand,
    Constants.stats,
    Constants.history,
    Constants.leaderboard,
    Constants.subscribe,
    Constants.unsubscribe,
    Constants.invite,
].map(c => c.toLowerCase());

const SETUP_COMMANDS = [
    Constants.name,
    Constants.prefer,
    Constants.nightlength.toLowerCase(),
    Constants.daylength.toLowerCase(),
    Constants.day,
    Constants.night,
    Constants.daystart.toLowerCase(),
    Constants.nightstart.toLowerCase(),
    Constants.moderate,
];


async function eventToLogic(m){
    const channelCommand = await getChannelCommand(m);
    const text = m.content;
    if(text === channelCommand)
        return;
    const guildID = m.channel.type === 'text' ? m.channel.guild.id : null;
    m = {
        args: m.content,
        channelCommand, // TODO change to channelPrefix
        channelID: m.channel.id,
        channelPrefix: channelCommand,
        discordID: m.author.id,
        externalID: m.id,
        guildID,
        isDM: m.channel.type !== 'text',
        originChannelID: m.channel.id,
        raw: m,
    };

    if(!m.isDM){
        if(text.toLowerCase() === 'help')
            return discordUserClient.help(m);
        if(!text.startsWith(channelCommand))
            return discordClient.tryDayMessage(m);
    }

    let starter;
    if(text.startsWith(channelCommand))
        starter = channelCommand.length;
    else
        starter = 0;

    let ind = text.indexOf(' ');
    if(ind === -1)
        ind = text.length;
    const command = text.substring(starter, ind).toLowerCase();

    // function names must be lowercase
    if(Constants[command]){
        const metadata = await gameService.getGameMetadataByMessage(m);
        m.gameMetadata = metadata;
        if(!metadata && !ALLOWED_NOT_JOINED_COMMANDS.includes(command))
            return m.raw.reply('You must be part of the game to submit this command.')
                .catch(helpers.log);
        if(SETUP_COMMANDS.includes(command) && metadata && metadata.isStarted)
            return m.raw.reply("You can't edit game settings after game start.");
        m.args = text.substring(command.length + starter + 1, text.length);
        try{
            await discordUserClient[command](m);
        }catch(err){
            if(err.userErrorText)
                m.raw.reply(err.userErrorText);
            else
                helpers.log(err, 'Unhandled discord exception on user interaction');
        }
        return m;
    }

    if(!m.isDM)
        await messageService.deleteMessage(m.raw);

    const allowedCommands = await gameService.getGameAbilities(m);
    const abilityMetadata = allowedCommands.find(a => a.command.toLowerCase() === command);
    if(!abilityMetadata){
        await unknownCommandPrompt(m, command);
        return m;
    }

    if(!abilityMetadata.publicCommand && !m.isDM){
        await unknownCommandPrompt(m, command);
        return m;
    }

    let rem = text.substring(starter, text.length);
    if(m.isDM)
        rem = rem.replace('@', '').replace(/#[0-9]*/, '');

    rem = await stripDiscordMentions(rem);

    try{
        await actionService.submitAction(m, rem);
    }catch(err){
        if(err.userErrorText)
            m.raw.reply(err.userErrorText);
        else
            helpers.log(err, 'Unhandled discord exception on user interaction');
    }

    return m;
}

module.exports = {
    eventToLogic,
};

async function stripDiscordMentions(rem){
    const start = rem.indexOf('<@');
    if(start === -1)
        return rem;
    const end = rem.indexOf('>', start);
    if(end === -1)
        return rem;

    let discordID = rem.substring(start + 2, end);
    if(discordID.startsWith('!'))
        discordID = discordID.substring(1, discordID.length);
    const userID = await userService.getUserID(discordID);
    if(!userID)
        return rem;
    const profileService = require('../../../../services/profileService');
    const profile = await profileService.get(userID);
    if(!profile || !profile.startingPlayerName)
        return rem;

    const search = rem.substring(start, end + 1);
    return rem.replace(new RegExp(search, 'g'), profile.startingPlayerName);
}

function getChannelCommand(m){
    if(m.channel.type === PRIVATE)
        return '';
    return channelPrefixService.getPrefix(m.channel.id);
}

function unknownCommandPrompt(m, command){
    const { channelCommand } = m;
    const message = `Unknown command \`${command}\`! Use **${channelCommand}`
        + 'commands** to see available commands.';
    return discordJsonClient.sendMessage({ channelID: m.originChannelID, message });
}
