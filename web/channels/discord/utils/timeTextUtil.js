function getMinutesLabel(seconds){
    return `**${seconds / 60}** minutes`;
}

module.exports = {
    getMinutesLabel,
};
