function userError(userErrorText){
    return {
        userErrorText,
    };
}

module.exports = {
    userError,
};
