function getMessage(profiles){
    profiles = profiles.filter(p => p.roleCard.actions.length);
    return `${''
        + '**PlayerActions**\n'}${
        profiles.map(profile => `- ${profile.name} : _${profile.roleCard.actions
            .map(action => action.simpleText)}_`).join('\n')}`;
}

module.exports = {
    getMessage,
};
