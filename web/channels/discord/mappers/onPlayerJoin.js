function getMessage(request, channelIDs){
    return {
        channelIDs,
        message: `${request.playerName} has joined the lobby.`,
    };
}

module.exports = {
    getMessage,
};
