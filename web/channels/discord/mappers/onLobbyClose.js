function getMessage(channelIDs){
    return {
        channelIDs: [...channelIDs],
        message: 'Game canceled.',
    };
}

module.exports = {
    getMessage,
};
