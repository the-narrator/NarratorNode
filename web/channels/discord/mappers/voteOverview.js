const { getPlayerIndexMap } = require('./mapperUtils');
const { SKIP_DAY_NAME } = require('../../../utils/constants');


function getMessage(votes, game, requester, channelIDs){
    const hiddenVotes = isHiddenVotes(votes, game, requester.userID);
    const cVotes = votes.voterToVotes;
    const targetsToVoters = {};
    const voters = Object.keys(cVotes).filter(playerName => playerName !== SKIP_DAY_NAME);
    const deadNames = game.graveyard.map(player => player.name);
    voters.sort();
    for(let i = 0; i < voters.length; i++){
        const voter = voters[i];
        let targets = cVotes[voter];
        if(!targets.length)// not voting anyone
            targets = ['Not Voting'];

        targets.forEach(target => {
            if(!targetsToVoters[target])
                targetsToVoters[target] = [];

            if(!hiddenVotes || voter === requester.name)
                targetsToVoters[target].push(voter);
        });
    }
    game.players.map(player => player.name)
        .filter(playerName => !deadNames.includes(playerName))
        .filter(playerName => !targetsToVoters[playerName])
        .forEach(playerName => {
            targetsToVoters[playerName] = [];
        });

    const playerIndexMap = getPlayerIndexMap(game.players);
    const voted = Object.keys(targetsToVoters);
    const fields = voted.filter(target => {
        if(target === SKIP_DAY_NAME)
            return true;
        if(target === 'Not Voting')
            return true;
        return votes.allowedTargets.includes(target);
    }).map(target => {
        const value = getVoteSubListEndingText(target, voters, votes.settings, targetsToVoters);
        if(target !== SKIP_DAY_NAME && target !== 'Not Voting')
            target = `[${playerIndexMap[target]}] ${target}`;
        return {
            name: `**${target}**`,
            value,
            inline: target !== 'Not Voting',
        };
    }).filter(field => field.value);
    fields.sort(voteListSorter);

    const response = {
        message: {
            embed: {
                description: '**Vote Counts**',
                fields,
                footer: {
                    text: 'Hosted by The Narrator',
                },
            },
        },
    };
    if(requester.discordID)
        response.discordID = requester;
    else
        response.channelIDs = channelIDs;
    return response;
}

module.exports = {
    getMessage,
};

function getVoteSubListEndingText(target, voters, voteSettings, votes){
    let targetVoters = votes[target];
    targetVoters.sort();
    const value = targetVoters.join('\n');
    if(target === 'Not Voting')
        return value;
    targetVoters = votes[target];
    if(voteSettings.voteSystemType === 'plurality'){
        const voteThreshhold = Math.floor(voters.length / 2) + 1;
        return `${value}\n**L - ${voteThreshhold - targetVoters.length}**`;
    }
    if(targetVoters.length)
        return value;
    return `${value}*No votes*`;
}

function voteListSorter(x, y){
    if(x.name === '**Not Voting**')
        return 1;
    if(y.name === '**Not Voting**')
        return -1;
    return y.value.split('\n').length - x.value.split('\n').length;
}

function isHiddenVotes(votes, game, requesterUserID){
    if(!votes.settings.secretVoting)
        return false;
    if(!requesterUserID)
        return false;
    return !game.players.find(player => player.userID === requesterUserID);
}
