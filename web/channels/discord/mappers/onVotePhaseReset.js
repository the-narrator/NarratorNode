function getMessage(request, game, channelIDs){
    const voteOverviewMapper = require('./voteOverview');
    const voteCount = {
        channelIDs,
        message: voteOverviewMapper.getMessage(request.votes, game, {}, channelIDs).message,
    };

    let message = 'There is no vote consensus.  Votes have been reset.';
    if(request.resetsRemaining === 1)
        message += '  This is your last chance before everyone dies.';
    else
        message += `  You have ${request.resetsRemaining} more chances before everyone dies.`;

    return [voteCount, {
        channelIDs,
        message,
    }];
}

module.exports = {
    getMessage,
};
