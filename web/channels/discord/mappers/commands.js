const gamePhase = require('../../../models/enums/gamePhase');


function getMessage(m, userAbilities, isGameInChannel, game, userID){
    const prefix = `**${m.channelCommand}`;
    let output = '__Bot commands__\n';
    // if(m.args === 'autotag'){
    //     prefix = `${prefix}autotag `;

    //     output += `${prefix}on** - Turns on autotag or adds your available tags\n`;
    //     output += `${prefix}off** - Turns off your autotag contribution\n`;
    //     output += `${prefix}ignorehost** - Blocks hosts from autotagging you ever again\n`;
    //     output += `${prefix}dnd** - Blocks autotags\n`;
    //     output += `${prefix}enable** - Removes name from Do not Disturb list\n`;
    //     return reply(m, output);
    // }

    if(game){
        if(!game.isStarted){ // game not started
            output += `${prefix}leave** - Leaves current game\n`;
            output += `${prefix}name <newName> ** - Changes your game name to the one specified\n`;
            // output += prefix + "autotag help** - Displayes possible autotag commands\n";
            output += `${prefix}dayLength <number> ** - Changes day length\n`;
            output += `${prefix}nightLength <number> ** - Changes night length\n`;
            // eslint-disable-next-line max-len
            output += `${prefix}autoParity <on/off>** - Sets whether a day will auto skip if impossible to resolve otherwise\n`;
            // eslint-disable-next-line max-len
            output += `${prefix}chatRate <number>** - Sets how long a user has to wait in between message sending.\n`;
            if(game.setup.isEditable){
                output += `${prefix}dayStart** - Makes game start at day\n`;
                output += `${prefix}nightStart** - Makes game start at night\n`;
                output += `${prefix}punch <on/off>** - Enables daytime punches/ITA\n`;
                output += `${prefix}skip <on/off>** - Sets whether a day can be skipped\n`;
            }
            // eslint-disable-next-line max-len
            output += `${prefix}prefer <roleName/factionName>** Submits preference for a role or faction in game\n`;
            output += `${prefix}clearPrefers** Removes all prefers for the current game setup\n`;
            output += `${prefix}moderate <on/off>** - `
                + 'Sets whether the host is participating in the game\n';
            output += `${prefix}setup <name>** - Changes current setup\n`;
            output += `${prefix}setup** displays available auto-setups\n`;
            output += `${prefix}start** - Starts game\n`;
        }else{
            let ability;
            let added = false;
            for(let i = 0; i < userAbilities.length; i++){
                ability = userAbilities[i];

                if(ability.publicCommand || m.isDM){
                    output += `${prefix + ability.command}** - ${ability.description}\n`;
                    added = true;
                }
            }
            if(game.phase.name === gamePhase.VOTES_OPEN)
                output += `${prefix}voteCount** - Lists current vote counts\n`;
            output += `${prefix}living** - Lists living people\n`;
            output += `${prefix}roleCard** - Shows rolecard again.\n`;
            if(game.users.find(user => userID === user.id && user.isModerator)){
                output += `${prefix}editTimer <number>** - Edit phase timer`;
                output += `${prefix}playerActions** - View player actions\n`;
                output += `${prefix}playerRoles** - View player roles\n`;
            }

            if(added)
                output += '\n**To see an example** of command usage, '
                    + `use ${prefix}usage *command***`;
        }
    }else{
        output += `${prefix}info** - Displays current game status\n`;
        output += `${prefix}invite** - Instructions on inviting this bot to your server\n`;
        output += `${prefix}changeCommand <command>** - changes the command prefix for the bot\n`;
        output += `${prefix}stats** - view your basic stats with Narrator\n`;
        output += `${prefix}leaderboard** - view the global leaderboard within Narrator\n`;
        if(!m.isDM){
            output += `${prefix}subscribe** - subscribe to this channel's started games\n`;
            output += `${prefix}unsubscribe** - stop being notified of channel's started games\n`;
        }
        if(isGameInChannel)
            output += `${prefix}join** - Joins current game\n`;
        else
            output += `${prefix}host** - Hosts a new game in the current channel\n`;
    }
    return output;
}

module.exports = {
    getMessage,
};
