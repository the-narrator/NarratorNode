const Constants = require('../Constants');


function getOverview(request){
    const message = getText(request);
    return {
        channelID: request.channelID,
        message,
    };
}

module.exports = {
    getOverview,
};

function getText({ channelPrefix }){
    // eslint-disable-next-line prefer-template
    return '__Available Vote Systems__:\n'
        // eslint-disable-next-line max-len
        + '1. **Plurality Hammer** - Single vote. Day ends early when one person has +50% of the vote.\n'
        // eslint-disable-next-line max-len
        + '2. **Diminishing Pool** - Single vote. Day phase alternates between voting and trials.  To be on trial, two people must have nominated you.  After trials, non trialed people are not eligible for daytime elimination.  Trials happen only once per player per day.  51% of votes on a person ends the day.\n'
        // eslint-disable-next-line max-len
        + '3. **MultiPlurality** - Multi vote.  Vote for as many people simultaneously.  When day time ends, person with most votes gets eliminated.\n'
        // eslint-disable-next-line max-len
        + '4. **No Elimination** - Single vote.  Votes can be submitted, but no one gets eliminated.\n\n'
        + 'Example usage: **' + channelPrefix + Constants.votesystem + ' 3** for multi vote';
}
