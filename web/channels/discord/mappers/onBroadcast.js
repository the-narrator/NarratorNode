function getMessage(request, channelIDs){
    return {
        channelIDs,
        message: request.pmContents,
    };
}

module.exports = {
    getMessage,
};
