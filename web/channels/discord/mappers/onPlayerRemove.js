function getMessage(discordName, channelID){
    return {
        channelID,
        message: `${discordName} has left the lobby.`,
    };
}

module.exports = {
    getMessage,
};
