const discordJsonClient = require('../discordJsonClient');

const subscriptionMessagesRepo = require('../discordRepos/subscriptionMessagesRepo');


const safeDeletes = new Set();

function saveSubscribeNotification(messageIDchannelID){
    return subscriptionMessagesRepo.saveSubscribeMessage(messageIDchannelID);
}

// expects the actual message object from discordJS,
// not narrator's mapped object
function deleteMessage(message){
    safeDeletes.add(message.id);
    return discordJsonClient.deleteMessage(message);
}

async function deleteSubscriptionMessages(gameID){
    const messages = await subscriptionMessagesRepo.getByGameID(gameID);
    return Promise.all([
        ...messages.map(discordJsonClient.deleteMessageByIDs),
        subscriptionMessagesRepo.deleteByGameID(gameID),
    ]);
}

async function handleMessageUpdate(newMessage, oldMessage){
    const discordID = newMessage.author.id;
    if(newMessage.channel.type !== 'text')
        return;
    if(oldMessage && newMessage.content === oldMessage.content)
        return;
    const channelID = newMessage.channel.id;
    const gameService = require('./gameService');
    const metadata = await gameService.getGameMetadataByChannelID(newMessage.channel.id);
    if(!metadata || !metadata.isStarted)
        return;

    const channelCommandService = require('./channelCommandService');
    const channelCommand = await channelCommandService.getPrefix(channelID);
    if(!oldMessage && newMessage.content.startsWith
        && newMessage.content.startsWith(channelCommand))
        return;

    if(unmarkSafeDeletedMessage(newMessage.id))
        return;

    if(oldMessage && newMessage){
        const { distance } = require('fastest-levenshtein');
        const calcDist = distance(newMessage.content, oldMessage.content);
        if(calcDist === 1)
            return;
    }

    const wasModkilled = await require('./userService').modkill(discordID, channelID);
    if(wasModkilled === false) // wasModkilled could be undefined
        newMessage.reply('stop editing your messages.  '
            + '**If you edit or delete your messages again, I will modkill you.**');
}

function unmarkSafeDeletedMessage(messageExternalID){
    return safeDeletes.delete(messageExternalID);
}

module.exports = {
    saveSubscribeNotification,
    deleteMessage,
    deleteSubscriptionMessages,
    handleMessageUpdate,
    unmarkSafeDeletedMessage,
};
