const discordJsonClient = require('../discordJsonClient');

const helpers = require('../../../utils/helpers');

const gameUserRepo = require('../../../repos/gameUsersRepo');
const userIntegrationsRepo = require('../../../repos/userIntegrationsRepo');

const userService = require('../../../services/userService');

const userTypes = require('../../../models/enums/userTypes');

async function getByGameID(gameID){
    const gameUsers = await gameUserRepo.getByGameID(gameID);
    const userIDs = gameUsers.map(({ userID }) => userID);
    const map = await getUserIDToDiscordIDMap(userIDs);
    return gameUsers.map(user => ({
        ...user,
        discordID: map[user.userID],
    }));
}

function getDiscordID(userID){
    return userIntegrationsRepo.getDiscordID(userID);
}

async function getNameByID(userID){
    const users = await userService.getNamesFromIDs(new Set([userID]));
    return users[0].name;
}

async function getUserID(discordID){
    const user = await userIntegrationsRepo.getByExternalID(discordID, userTypes.DISCORD);
    if(user)
        return user.userID;
    const name = await discordJsonClient.getName(discordID);
    const { id } = await userService.create(name, discordID, userTypes.DISCORD);
    return id;
}

async function getUserIDToDiscordIDMap(userIDs){
    const userIdDiscordIDs = await Promise.all(userIDs.map(async userID => {
        const discordID = await getDiscordID(userID);
        return [userID, discordID];
    }));
    return Object.fromEntries(userIdDiscordIDs);
}

const modkilledUsers = {};

// returns whether or not the user is modkilled
async function modkill(discordID, channelID){
    const userID = await getUserID(discordID);
    if(!userID)
        return;

    if(modkilledUsers[discordID] > 1)
        return;

    if(modkilledUsers[discordID]){
        try{
            const playerService = require('../../../services/playerService');
            await playerService.modkill(userID);
            modkilledUsers[discordID]++;
        }catch(err){
            helpers.log(err, 'Couldn\'t modkill in discord');
            const message = `I'm having trouble stopping <@!${discordID}> `
                + 'from talking when they\'re not supposed to.';
            await discordJsonClient.sendPublicMessage(channelID, message);
        }
        return true;
    }
    modkilledUsers[discordID] = 1;
    return false;
}

async function cleanupOnGameEnd(discordID){
    delete modkilledUsers[discordID];
}

module.exports = {
    getByGameID,
    getDiscordID,
    getNameByID,
    getUserID,
    getUserIDToDiscordIDMap,
    modkill,
    cleanupOnGameEnd,
};
