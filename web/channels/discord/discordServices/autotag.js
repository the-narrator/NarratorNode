const messageService = require('./messageService');


function AutoTag(discNarrator, channelID, starter){
    this.bot = discNarrator;
    this.channelID = channelID;
    this.searchers = [];
    this.tagMessages = [];
    this.isLocatingTag = false;

    this.isSearching = true;

    this.addSearcher(starter);
}

AutoTag.prototype.stop = function(){
    this.searchers = [];
    this.isSearching = false;

    const cleanupChannel = this.bot.bot.channels.get(this.channelID);
    this.tagMessages.forEach(messageID => {
        cleanupChannel.fetchMessage(messageID)
            .then(m => messageService.deleteMessage(m));
    });
};

AutoTag.prototype.isSearching = function(authorID){
    return this.searchers.indexOf(authorID) !== -1;
};

AutoTag.prototype.addSearcher = function(authorID){
    if(!this.isSearcher(authorID))
        this.searchers.push(authorID);

    if(!this.isLocatingTag){
        this.isLocatingTag = true;
        const tagger = this;
        setTimeout(() => {
            tagger.tagSomeone();
        }, 1000);
    }
};

AutoTag.prototype.removeSearcher = function(authorID){
    const index = this.searchers.indexOf(authorID);
    if(index > -1)
        this.searchers.splice(index, 1);
};

AutoTag.prototype.isSearcher = function(authorID){
    return this.searchers.indexOf(authorID) !== -1;
};

AutoTag.prototype.tagSomeone = function(){
    if(!this.isSearching || !this.searchers.length){
        this.isLocatingTag = false;
        return;
    }
    const searchers = this.searchers.map(u => u);

    const tagger = this;

    let qString = 'SELECT a.token FROM player_saved_games a, player_saved_games b WHERE a.token IS NOT NULL AND (';
    this.searchers.forEach((token, i) => {
        if(i)
            qString += ' OR ';
        qString += `b.token = '${token}'`;
    });
    qString += ') AND a.game_id = b.game_id GROUP BY a.token';

    let canTagList = null;
    query(qString)
        .then(results => {
            canTagList = results.map(r => r.token); // everyone seekers has played with before

            let guildMembers = tagger.bot.bot.channels.get(tagger.channelID).guild.members;
            guildMembers = guildMembers.filter(u => u.presence.status !== 'dnd' && u.presence.status !== 'offline').keyArray(); // filters on guild members that are here

            canTagList = canTagList.filter(user_id => guildMembers.indexOf(user_id) !== -1);

            const gameMembers = tagger.bot.channelToUser[tagger.channelID]; // filter out people already in game
            canTagList = canTagList.filter(user_id => gameMembers.indexOf(user_id) === -1);

            if(!canTagList.length)
                throw 'no one left to tag';

            qString = 'SELECT user_id FROM discord_last_tagged WHERE (';
            canTagList.forEach((user_id, i) => {
                if(i)
                    qString += ' OR ';
                qString += (`user_id = ${user_id}`);
            });

            qString += ') AND timestamp > NOW();';
            return query(qString);
        }).then(dndUsers => {
            dndUsers = dndUsers.map(r => r.user_id);
            canTagList = canTagList.filter(user_id => dndUsers.indexOf(user_id) === -1); // filtered out people that are on cooldown or never want to get tagged

            qString = 'SELECT blocker FROM discord_autotag_block WHERE ';
            searchers.forEach((user_id, i) => {
                if(i)
                    qString += ' OR ';
                qString += (`blocked = ${user_id}`);
            });
            return query(`${qString};`);
        }).then(blockers => {
            blockers = blockers.map(r => r.blocker);
            canTagList = canTagList.filter(user_id => blockers.indexOf(user_id) === -1); // filter out if one of the people to tag blocked someone in the searchers

            if(!canTagList.length)
                throw 'no one left to tag';

            return gameService.getGameState(tagger.bot.idToToken[tagger.searchers[0]]);
        })
        .then(gameState => {
            gameState = gameState.requestGameState;
            const playerCount = []; // use gameService.getByGameID for this;
            const rolesList = gameState.roles.length;
            const needed = rolesList - playerCount;
            const prefix = tagger.bot.getChannelCommand(tagger.channelID);

            const toTag = canTagList[Math.floor(Math.random() * canTagList.length)];
            let text = `<@!${toTag}>, You are being tagged to play _**${gameState.setupName}**_ by `;
            searchers.forEach((s_id, i) => {
                if(i)
                    i += ', ';
                text += `<@!${s_id}>`;
            });
            text += '.\n';

            if(needed)
                text += ` They need ${needed} more players to start`;
            else
                text += ' They want more players for a better game';
            text += `.\n To stop _them_ from ever tagging you again: **${prefix}autotag ignorehost**`;
            text += `.\n To stop _anyone_ from tagging you again: **${prefix}autotag dnd**.`;

            const p = tagger.bot.channelPush(text, tagger.channelID);
            const q = query(`INSERT INTO discord_last_tagged (user_id, timestamp) VALUES ('${toTag}', DATE_ADD(NOW(), INTERVAL 2 HOUR)) ON DUPLICATE KEY UPDATE timestamp = DATE_ADD(NOW(), INTERVAL 1 MINUTE);`);
            return Promise.all([p, q]);
        })
        .then(m => {
            tagger.tagMessages.push(m[0].id);
        })
        .catch(err => {
            if(err === 'no one left to tag'){
                let text = '';
                searchers.forEach((s_id, i) => {
                    if(i)
                        i += ', ';
                    text += `<@!${s_id}>`;
                });
                text += " I can't find anyone else appropriate to tag for you.";
                return tagger.bot.channelPush(text, tagger.channelID);
            }
        })
        .then(() => {
            setTimeout(() => {
                tagger.tagSomeone();
            }, 30000);
        });
};


module.exports = {
    AutoTag: AutoTag,
};
