const discord = require('discord.js');

const config = require('../../../config');

const helpers = require('../../utils/helpers');

const { PRIVATE } = require('./enums/channelTypes');
const { MANAGE_MESSAGES, MANAGE_ROLES, MANAGE_WEBHOOKS } = require('./enums/permissions');

const client = new discord.Client();

client.on('error', err => {
    if(typeof err === 'object' && err.code === 'ECONNRESET')
        return;

    helpers.log(err, 'Discord Json Client error');
});

async function connect(){
    const onMessage = require('./events/external/onMessage');

    const guildService = require('./discordServices/guildService');
    const messageService = require('./discordServices/messageService');

    const timerService = require('../../services/timerService.js');

    await new Promise(resolve => {
        client.on('ready', resolve);
        client.login(config.discord.master_key);
    });

    client.on('message', async m => {
        if(client.user.id === m.author.id)
            return;
        if(m.author.bot)
            return;

        m = await onMessage.eventToLogic(m);
        if(!m) // these are commands the wrapper sent
            return;

        // discord talkers not in game could keep game alive
        if(m.gameMetadata)
            timerService.setGameExpiration(m.gameMetadata.lobbyID);
    });

    function onMessageUpdate(newMessage, oldMessage){
        if(client.user.id === newMessage.author.id)
            return;
        messageService.handleMessageUpdate(newMessage, oldMessage);
    }

    client.on('messageUpdate', (oldMessage, newMessage) => {
        onMessageUpdate(newMessage, oldMessage);
    });

    client.on('messageDelete', message => {
        onMessageUpdate(message);
    });

    client.on('guildMemberAdd', gMember => guildService.handleGuildMemberAdd(gMember));
}

async function deleteMessage(discordMessage){
    if(discordMessage.channel.type === PRIVATE
        || await hasChannelPermission(discordMessage.channel.id, MANAGE_MESSAGES))
        return discordMessage.delete();
}

async function deleteMessageByIDs(obj){
    const channel = client.channels.cache.get(obj.channelID);
    const message = await channel.messages.fetch(obj.messageID);
    const messageService = require('./discordServices/messageService');
    return messageService.deleteMessage(message);
}

async function createChannel(guildID, channelName, options, everyoneOverwrites = []){
    const nightChatsGuild = client.guilds.cache.get(guildID);
    const everyoneID = nightChatsGuild.roles.everyone.id;

    const channel = await nightChatsGuild.channels.create(channelName, {
        ...options,
        permissionOverwrites: [{
            allow: [],
            id: everyoneID,
            deny: everyoneOverwrites,
            type: 'role',
        }],
    });
    return channel.id;
}

async function isInNightChatsGuild(discordID){
    const nightChatsGuild = client.guilds.cache.get(config.discord.night_chat_server);
    return nightChatsGuild.members.cache.keyArray().includes(discordID);
}

async function getChannelPermissionOverwrites(channelID){
    const channel = await getChannel(channelID);
    return channel.permissionOverwrites;
}

function addChatPermission(snowflake, channelIDs){
    return Promise.all([...channelIDs].map(
        channelID => setChannelPermissions(channelID, snowflake, { SEND_MESSAGES: true }),
    ));
}

async function hasChannelPermission(channel, permission){
    if(typeof channel === 'string')
        channel = await getChannel(channel);
    const permissions = channel.permissionsFor(channel.guild.me);
    return permissions.has(permission.value);
}

async function hasChannelPermissions(channelID, permissionArray){
    const channel = await getChannel(channelID);
    const { me } = channel.guild;
    return Object.fromEntries(
        permissionArray.map(
            permissionValue => [permissionValue, channel.permissionsFor(me).has(permissionValue)],
        ),
    );
}

async function setChannelPermissions(channelID, snowflake, permission){
    const channel = await getChannel(channelID);
    if(!(await hasChannelPermission(channel, MANAGE_ROLES)))
        return;
    try{
        return await channel.createOverwrite(snowflake, permission);
    }catch(err){
        helpers.log(err);
    }
}

function removeChatPermission(snowflake, channelIDs){
    return Promise.all([...channelIDs].map(
        channelID => setChannelPermissions(channelID, snowflake, { SEND_MESSAGES: false }),
    ));
}

async function removeEveryoneChatPermission(channelIDs){
    return Promise.all([...channelIDs].map(
        async channelID => {
            const channel = await getChannel(channelID);
            const everyoneRoleID = channel.guild.roles.everyone.id;
            return removeChatPermission(everyoneRoleID, new Set([channelID]));
        },
    ));
}

async function resetChatPermissions(channelID){
    const channel = await getChannel(channelID);
    const permissions = channel.permissionOverwrites
        .filter(permission => permission.type === 'member');
    await Promise.all(permissions.map(permission => permission.delete()));
    return addChatPermission(channel.guild.roles.everyone.id, new Set([channelID]));
}

async function deleteChannel(channelID){
    const channel = await getChannel(channelID);
    if(channel)
        return channel.delete();
}

async function getInviteURL(channelID){
    const channel = await getChannel(channelID);
    const invite = await channel.createInvite();
    return invite.toString();
}

async function getName(discordID, channelID){
    if(channelID){
        const channel = await getChannel(channelID);
        const { nickname } = channel.guild.members.cache.get(discordID);
        if(nickname)
            return nickname;
    }
    const user = await getUser(discordID);
    return user.username;
}

async function sendDirectMessage(discordID, message){
    if(!message)
        return;
    try{
        const user = await getUser(discordID);
        const dm = await user.createDM();
        return dm.send(message);
    }catch(err){
        if(typeof err !== 'object' || err.code !== 50007)
            helpers.log(err, `failed to send discord direct message\n${discordID}\n${message}`);
    }
}

function sendMessage(request){
    if(Array.isArray(request))
        return Promise.all(request.map(sendMessage));

    if(request.channelIDs)
        return Promise.all(
            [...request.channelIDs].map(
                channelID => sendMessage({ channelID, message: request.message }),
            ),
        );


    if(request.channelID)
        return sendPublicMessage(request.channelID, request.message);

    if(request.discordID)
        return sendDirectMessage(request.discordID, request.message);
}

function sendPublicMessage(channelIDs, message){
    if(!(channelIDs instanceof Set))
        channelIDs = new Set([channelIDs]);
    return Promise.all(
        [...channelIDs].map(
            async channelID => {
                const channel = await client.channels.fetch(channelID);
                return channel.send(message);
            },
        ),
    );
}

function reply(m, message){
    return m.raw.reply(message).catch(helpers.log);
}

async function sendWebhook(channelID, name, content){
    if(Array.isArray(channelID))
        return Promise.all(channelID.map(
            cID => sendWebhook(cID, name, content),
        ));
    const channel = await getChannel(channelID);
    if(!(await hasChannelPermission(channelID, MANAGE_WEBHOOKS)))
        return;
    const webhookCollection = await channel.fetchWebhooks();
    let webhook = webhookCollection.find(wh => wh.owner.id === client.user.id);
    if(!webhook)
        webhook = await channel.createWebhook(name, {});
    else if(webhook.name !== name)
        await webhook.edit({ name });

    return webhook.send(content);
}

async function getUserStatus(discordID){
    const user = await getUser(discordID, true);
    return user.presence.status;
}

module.exports = {
    _client: client,

    connect,
    deleteMessage,
    deleteMessageByIDs,

    createChannel,
    isInNightChatsGuild,
    getChannelPermissionOverwrites,
    addChatPermission,
    hasChannelPermission,
    hasChannelPermissions,
    setChannelPermissions,
    removeChatPermission,
    removeEveryoneChatPermission,
    resetChatPermissions,
    deleteChannel,

    getInviteURL,
    getName,

    reply,
    sendDirectMessage,
    sendMessage,
    sendPublicMessage,
    sendWebhook,

    getUserStatus,
};

async function getChannel(channelID){
    return client.channels.cache.get(channelID);
}

async function getUser(discordID){
    return client.users.fetch(discordID);
}
