const db = require('../../../db_communicator');


async function getAll(){
    const results = await db.query('SELECT channel_id FROM discord_night_channels;');
    return results.map(result => result.channel_id);
}

async function getChannelID(gameID, chatName){
    const queryText = ''
        + 'SELECT channel_id FROM discord_night_channels '
        + 'WHERE game_id = ? AND chat_name = ?';
    const results = await db.query(queryText, [gameID, chatName]);
    if(!results.length)
        return null;
    return results[0].channel_id;
}

async function getChannelIDsByGameID(gameID){
    const queryText = ''
        + 'SELECT channel_id FROM discord_night_channels '
        + 'WHERE game_id = ?;';
    const results = await db.query(queryText, [gameID]);
    return results.map(r => r.channel_id);
}

function deleteAll(){
    return db.query('DELETE FROM discord_night_channels;');
}

function deleteByGameID(gameID){
    return db.query('DELETE FROM discord_night_channels WHERE game_id = ?;', [gameID]);
}

async function save(discordRoomID, gameID, chatName){
    const query = ''
        + 'INSERT INTO discord_night_channels (channel_id, game_id, chat_name) VALUES (?, ?, ?)';
    return db.query(query, [discordRoomID, gameID, chatName]);
}

module.exports = {
    deleteAll,
    deleteByGameID,
    getChannelID,
    getChannelIDsByGameID,
    getAll,
    save,
};
