function dbNotFoundException(entityID){
    const err = `Entity was not found with that id: ${entityID}`;
    throw new Error(err);
}

module.exports = {
    dbNotFoundException,
};
