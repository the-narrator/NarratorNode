const chalk = require('chalk');
const crypto = require('crypto');
const { spawn } = require('child_process');

const config = require('../../config');

const mariadbLocation = 'mariadb-java-client-2.3.0.jar';

const integrationType = require('../models/enums/integrationTypes');


let argSeparator;
if(config.windws_host)// indicates windows system
    argSeparator = ';';
else
    argSeparator = ':';

function isDictEqual(o1, o2){
    const keys1 = Object.keys(o1);
    const keys2 = Object.keys(o2);
    if(keys1.length !== keys2.length)
        return false;
    let key;
    let value1;
    let value2;
    for(let i = 0; i < keys1.length; i++){
        key = keys1[i];
        if(!(key in o2))
            return false;
        value1 = o1[key];
        value2 = o2[key];

        if(typeof value1 !== typeof value2)
            return false;
        if(typeof value1 === 'object'){
            if(!isDictEqual(value1, value2))
                return false;
        }else if(value1 !== value2){
            return false;
        }
    }
    return true;
}

function colorLog(color, message){
    console.log(chalk[color](message)); // eslint-disable-line no-console
}

function complexObjectToString(object){
    if(typeof object !== 'object')
        return object;
    const output = {};
    Object.keys(object).forEach(key => {
        output[key] = object[key].toString();
    });
    return output;
}

function flattenList(inputList){
    return inputList.reduce((acc, val) => acc.concat(val), []);
}

function getRandomNumber(cap){
    return Math.floor(Math.random() * cap);
}

function getRandomString(length){
    if(!length)
        length = 128;
    // slice ignores how long it actually is. no errors thrown
    return crypto.randomBytes(Math.ceil(length / 2))
        .toString('hex') // convert to hexadecimal format
        .slice(0, length);
}

function getWrapper(wrapperType){
    const app = require('../app');
    if(wrapperType === 'firebase' || wrapperType === 'browser')
        return app.browserWrapper;
    if(wrapperType === integrationType.DISCORD)
        return app.discordWrapper;
    if(wrapperType === 'notification')
        return app.notificationWrapper;
    if(wrapperType === integrationType.SC2MAFIA)
        return require('../channels/sc2mafia/sc2mafiaClient');
    throw `Unknown wrapper type: ${wrapperType}.`;
}

function isAlphaNumeric(value){
    if(typeof value !== 'string')
        return false;
    return value.match(/^[a-zA-Z0-9]+$/i) !== null;
}

function isAlpha(value){
    if(typeof value !== 'string')
        return false;
    return value.match(/^[a-zA-Z]+$/i) !== null;
}

function isEmptyObject(object){
    if(typeof object !== 'object')
        return false;
    return !Object.keys(object).length;
}

function isInt(value){
    if(Number.isNaN(value))
        return false;

    const x = parseFloat(value);
    return (x | 0) === x; // eslint-disable-line no-bitwise
}

function log(err, message){
    if(err === undefined && message === undefined)
        return;
    if(!err){
        err = message;
        message = null;
    }

    if(!err)
        console.trace(); // eslint-disable-line no-console
    else
        console.log(err); // eslint-disable-line no-console
    if(message)
        console.log(message); // eslint-disable-line no-console
}

// placeholder for future use
function memoizeDebounce(func, wait = 0, options = {}){
    // eslint-disable-next-line no-undef
    const mem = _.memoize(() => _.debounce(func, wait, options), options.resolver);
    return function(...args){
        mem.apply(this, args).apply(this, args);
    };
}

function onlyUniqueFilterFunction(value, index, self){
    return self.indexOf(value) === index;
}

function runJavaProg(javaFileName, args){
    return new Promise((resolve, reject) => {
        let buff = '';
        let errBuff = '';

        if(!args)
            args = '';

        const javaComm = spawn('java', [
            '-cp', `../../../libs/${mariadbLocation}${argSeparator}.`,
            `nnode/${javaFileName}`,
            args,
        ], { cwd: '../src/main/java' });

        javaComm.stderr.on('data', errData => {
            errBuff += errData.toString();
        });
        javaComm.stdout.on('data', data => {
            buff += data.toString();
        });
        javaComm.on('exit', code => {
            if(errBuff.length !== 0)
                log(errBuff);
            if(code === 0)
                resolve(buff);
            else
                reject(errBuff);
        });
    });
}

function serviceEnabled(serviceName){
    return process.argv.includes(`--${serviceName}`);
}

module.exports = {
    colorLog,
    complexObjectToString,
    isAlpha,
    isAlphaNumeric,
    isDictEqual,
    isEmptyObject,
    isInt,
    flattenList,
    getRandomNumber,
    getRandomString,
    getWrapper,
    log,
    memoizeDebounce,
    onlyUniqueFilterFunction,
    runJavaProg,
    serviceEnabled,
};
