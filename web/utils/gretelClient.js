const { omit } = require('lodash');
const { spawn } = require('child_process');

const config = require('../../config.json');
const helpers = require('./helpers');

const loggingService = require('../services/loggingService');

const mariadbLocation = 'mariadb-java-client-2.3.0.jar';


let argSeparator;
if(config.pc_env)// indicates windows system
    argSeparator = ';';
else
    argSeparator = ':';

const idToResolve = {};
let javaProcess = null;
let buffer = '';

function close(){
    javaProcess.stdin.pause();
    javaProcess.kill();
}

function connect(){
    if(javaProcess)
        return;
    javaProcess = spawn('java', ['-cp',
        `../../../libs/${mariadbLocation}${argSeparator}.`,
        'App',
    ], { cwd: '../src/main/java' });

    javaProcess.stdin.setEncoding('utf-8');
    javaProcess.stdout.on('data', data => {
        data = data.toString().replace('\r', '');
        onReceiveData(data);
    });
    javaProcess.stderr.on('data', data => {
        helpers.log(`(javaErr) : ${data}`);
    });
    javaProcess.on('exit', code => {
        helpers.log(`child process exited with code ${code}`);
        process.exit(0);
    });
}

async function sendRequest(method, url, args){
    const requestID = helpers.getRandomString();
    let request = {
        requestID,
        method,
        url,
        body: args || {},

        // to later refactor out
        httpRequest: true,
    };
    request = JSON.stringify(request);
    request = `${request.replace('$$', '')}\n`;

    const startTime = new Date().getTime();
    const result = await new Promise((resolve, reject) => {
        idToResolve[requestID] = [resolve, reject];
        javaProcess.stdin.write(request);
    });
    const endTime = new Date().getTime();
    if(endTime - startTime > config.java_process_max_call)
        loggingService.reportSlowCall({
            name: 'Gretel request',
            time: endTime - startTime,
            method,
            url,
            args,
        });
    return result;
}

function sendInstanceRequest(message){
    if(typeof message === 'object')
        message = JSON.stringify(message);
    const request = `${message.replace('$$', '')}\n`;
    javaProcess.stdin.write(request);
}

module.exports = {
    close,
    connect,
    sendInstanceRequest,
    sendRequest,
};

function onReceiveData(dataBuffer){
    const app = require('../app');
    const eventService = require('../services/eventService');

    buffer += dataBuffer;

    while (buffer.includes('$$')){
        const endIndex = buffer.indexOf('$$');
        const objectStr = buffer.substring(0, endIndex);
        let response;
        buffer = buffer.substring(endIndex + 2, buffer.length);
        try{
            response = JSON.parse(objectStr);
        }catch(err){
            return helpers.log(err, 'Failed to parse object from JavaRequests.');
        }

        try{
            if(response.logMessage)
                loggingService.reportSlowCall(omit(response, 'logMessage'));
            else if(response.event)
                eventService[response.event](response);
            else if(response.httpResponse)
                resolveRequest(response);
            else
                app.socketPush(response.userID, objectStr);
        }catch(err){
            helpers.log(err, response);
        }
    }
}

function resolveRequest(responseObj){
    const resolve = idToResolve[responseObj.requestID][0];
    const reject = idToResolve[responseObj.requestID][1];
    delete idToResolve[responseObj.requestID];
    if(responseObj.criticalError)
        reject(responseObj);
    else
        resolve(responseObj);
}
