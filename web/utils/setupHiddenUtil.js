function isSpawnable(playerCount, setupHidden){
    return playerCount >= setupHidden.minPlayerCount && playerCount <= setupHidden.maxPlayerCount;
}

module.exports = {
    isSpawnable,
};
