function getHostName(game){
    return game.users.find(user => user.isModerator).name;
}

function getModeratorID(game){
    return game.users.find(user => user.isModerator).id;
}

module.exports = {
    getHostName,
    getModeratorID,
};
