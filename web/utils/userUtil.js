function isNonParticipatingModerator({ players, users }, requesterUserID){
    const isModerator = requesterUserID && users.find(user => user.id === requesterUserID
        && user.isModerator);
    if(!isModerator)
        return false;
    const player = players.find(({ userID }) => userID === requesterUserID);
    return !player || !player.isParticipating;
}

module.exports = {
    isNonParticipatingModerator,
};
