function getValue(modifiers, modifierName){
    return modifiers.find(m => m.name === modifierName).value;
}

module.exports = {
    getValue,
};
