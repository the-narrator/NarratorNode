const baseValidator = require('./baseValidator');


const createRoleRequestSchema = {
    type: 'object',
    properties: {
        abilities: {
            type: 'array',
            items: {
                type: 'string',
            },
            required: true,
        },
        name: {
            type: 'string',
            required: true,
            minLength: '1',
        },
        setupID: {
            type: 'number',
            required: true,
        },
    },
};

const upsertRoleAbilityModifierRequestSchema = {
    type: 'object',
    properties: {
        name: {
            type: 'string',
            required: true,
            length: '1',
        },
        value: {
            type: ['number', 'boolean', 'string'],
            required: true,
        },
        minPlayerCount: {
            type: 'number',
            required: true,
        },
        maxPlayerCount: {
            type: 'number',
            required: true,
        },
    },
};

const upsertRoleModifierRequestSchema = {
    type: 'object',
    properties: {
        name: {
            type: 'string',
            required: true,
            length: '1',
        },
        value: {
            type: ['number', 'boolean'],
            required: true,
        },
        minPlayerCount: {
            type: 'number',
            required: true,
        },
        maxPlayerCount: {
            type: 'number',
            required: true,
        },
    },
};

function createRoleRequest(query){
    baseValidator.validate(query, createRoleRequestSchema);
}

function upsertRoleAbilityModifierRequest(query){
    baseValidator.validate(query, upsertRoleAbilityModifierRequestSchema);
}

function upsertRoleModifierRequest(query){
    baseValidator.validate(query, upsertRoleModifierRequestSchema);
}

module.exports = {
    createRoleRequest,
    upsertRoleAbilityModifierRequest,
    upsertRoleModifierRequest,
};
