const { validate } = require('jsonschema'); // https://www.npmjs.com/package/jsonschema


function raiseValidationError(object, schema, statusCode = 422){
    if(object === null || object === undefined)
        throw {
            statusCode: 422,
            errors: ['Base validator input object was null.'],
        };

    const validateResponse = validate(object, schema);
    if(!validateResponse.errors.length)
        return;

    throw {
        statusCode,
        errors: validateResponse.errors.map(error => error.stack),
    };
}

module.exports = {
    validate: raiseValidationError,
};
