const baseValidator = require('./baseValidator');


const updateFactionAbilityModifierRequestSchema = {
    type: 'object',
    properties: {
        name: {
            type: 'string',
            required: true,
            length: '1',
        },
        value: {
            type: ['number', 'boolean'],
            required: true,
        },
        minPlayerCount: {
            type: 'number',
            required: true,
        },
        maxPlayerCount: {
            type: 'number',
            required: true,
        },
    },
};

function updateFactionAbilityModifierRequest(query){
    baseValidator.validate(query, updateFactionAbilityModifierRequestSchema);
}

module.exports = {
    updateFactionAbilityModifierRequest,
};
