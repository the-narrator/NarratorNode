const baseValidator = require('./baseValidator');


const updatePhaseRemainingTimeRequestSchema = {
    type: 'object',
    properties: {
        seconds: {
            type: 'number',
            required: true,
        },
        joinID: {
            type: 'string',
            required: true,
        },
    },
};

function updatePhaseRemainingTimeRequest(query){
    baseValidator.validate(query, updatePhaseRemainingTimeRequestSchema);
}

module.exports = {
    updatePhaseRemainingTimeRequest,
};
