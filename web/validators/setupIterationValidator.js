const baseValidator = require('./baseValidator');


const setupIterationSchema = {
    type: 'object',
    properties: {
        spawns: {
            type: 'array',
            items: {
                type: 'object',
                properties: {
                    setupHiddenID: {
                        type: 'number',
                        required: true,
                    },
                    factionRoleID: {
                        type: 'number',
                        required: true,
                    },
                },
            },
            required: true,
        },
    },
};

function createSetupIteration(query){
    baseValidator.validate(query, setupIterationSchema);
}

module.exports = {
    createSetupIteration,
};
