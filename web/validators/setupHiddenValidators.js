const { MAX_PLAYER_COUNT } = require('../utils/constants');

const baseValidator = require('./baseValidator');


const createHiddenRequestSchema = {
    type: 'object',
    properties: {
        hiddenID: {
            type: 'integer',
            required: true,
        },
        isExposed: {
            type: 'boolean',
            required: true,
        },
        minPlayerCount: {
            type: 'integer',
            required: true,
            minimum: 0,
            maximum: MAX_PLAYER_COUNT,
        },
        maxPlayerCount: {
            type: 'integer',
            required: true,
            minimum: 3,
            maximum: MAX_PLAYER_COUNT,
        },
        mustSpawn: {
            type: 'boolean',
            required: true,
        },
    },
};

function createSetupHiddenRequest(query){
    baseValidator.validate(query, createHiddenRequestSchema);
}

module.exports = {
    createSetupHiddenRequest,
};
