const baseValidator = require('./baseValidator');


const createFactionModifierRequestSchema = {
    type: 'object',
    properties: {
        name: {
            type: 'string',
            required: true,
            length: '1',
        },
        value: {
            type: ['number', 'boolean'],
            required: true,
        },
        minPlayerCount: {
            type: 'number',
            required: true,
        },
        maxPlayerCount: {
            type: 'number',
            required: true,
        },
    },
};

function createFactionModifierRequest(query){
    baseValidator.validate(query, createFactionModifierRequestSchema);
}

module.exports = {
    createFactionModifierRequest,
};
