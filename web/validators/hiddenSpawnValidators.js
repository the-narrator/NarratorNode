const { MAX_PLAYER_COUNT } = require('../utils/constants');

const baseValidator = require('./baseValidator');


const createHiddenSpawnDefinition = {
    factionRoleID: {
        type: 'number',
        required: true,
    },
    hiddenID: {
        type: 'number',
        required: true,
    },
    minPlayerCount: {
        type: 'number',
        required: true,
        minimum: 0,
        maximum: MAX_PLAYER_COUNT,
    },
    maxPlayerCount: {
        type: 'number',
        required: true,
        minimum: 3,
        maximum: MAX_PLAYER_COUNT,
    },
};

const createHiddenSpawnsRequestSchema = {
    type: 'array',
    items: {
        type: 'object',
        properties: createHiddenSpawnDefinition,
        required: true,
    },
};

function createHiddenSpawnsRequest(query){
    baseValidator.validate(query, createHiddenSpawnsRequestSchema);
}

module.exports = {
    createHiddenSpawnsRequest,
};
