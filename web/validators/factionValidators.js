const baseValidator = require('./baseValidator');


const updateFactionDefinition = {
    description: {
        type: 'string',
        required: true,
        maxLength: '500',
    },
    name: {
        type: 'string',
        required: true,
        minLength: '1',
    },
};

const createFactionRequestSchema = {
    type: 'object',
    properties: {
        ...updateFactionDefinition,
        color: {
            type: 'string',
            required: true,
            minLength: '7',
            maxLength: '7',
        },
        setupID: {
            type: 'number',
            required: true,
        },
    },
};

const addAbilityRequestSchema = {
    type: 'object',
    properties: {
        ability: {
            type: 'string',
            required: true,
        },
    },
};

const updateFactionRequestSchema = {
    type: 'object',
    properties: updateFactionDefinition,
};

function createFactionRequest(query){
    baseValidator.validate(query, createFactionRequestSchema);
}

function updateFactionRequest(query){
    baseValidator.validate(query, updateFactionRequestSchema);
}

function addAbilityRequest(query){
    baseValidator.validate(query, addAbilityRequestSchema);
}

module.exports = {
    createFactionRequest,
    updateFactionRequest,
    addAbilityRequest,
};
