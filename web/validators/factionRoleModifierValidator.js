const baseValidator = require('./baseValidator');


const createFactionRoleModifierRequestSchema = {
    type: 'object',
    properties: {
        name: {
            type: 'string',
            required: true,
            length: '1',
        },
        value: {
            type: ['number', 'boolean'],
            required: true,
        },
        minPlayerCount: {
            type: 'number',
            required: true,
        },
        maxPlayerCount: {
            type: 'number',
            required: true,
        },
    },
};

function createFactionRoleModifierRequest(query){
    baseValidator.validate(query, createFactionRoleModifierRequestSchema);
}

module.exports = {
    createFactionRoleModifierRequest,
};
