const baseValidator = require('./baseValidator');


const updateSetupModifierRequestSchema = {
    type: 'object',
    properties: {
        name: {
            type: 'string',
            required: true,
            length: '1',
        },
        value: {
            type: ['number', 'boolean', 'string'],
            required: true,
        },
        minPlayerCount: {
            type: 'number',
            required: true,
        },
        maxPlayerCount: {
            type: 'number',
            required: true,
        },
    },
};

function updateSetupModifierRequest(query){
    baseValidator.validate(query, updateSetupModifierRequestSchema);
}

module.exports = {
    updateSetupModifierRequest,
};
