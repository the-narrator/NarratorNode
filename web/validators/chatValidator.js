const baseValidator = require('./baseValidator');

const { MAX_LOBBY_CHAT_COUNT } = require('../utils/constants');


const sendLobbyMessageRequestSchema = {
    type: 'object',
    properties: {
        text: {
            type: 'string',
            minLength: '1',
            maxLength: MAX_LOBBY_CHAT_COUNT.toString(10),
            required: true,
        },
    },
};

function sendLobbyMessage(query){
    baseValidator.validate(query, sendLobbyMessageRequestSchema);
}

module.exports = {
    sendLobbyMessage,
};
