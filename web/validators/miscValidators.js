const baseValidator = require('./baseValidator');


const serviceErrorSchema = {
    type: 'object',
    properties: {
        errors: {
            type: 'array',
            required: true,
            items: {
                type: 'string',
            },
            minItems: 1,
        },
        statusCode: {
            type: 'integer',
            required: true,
            minimum: 200,
            maximum: 500,
        },
    },
};

function serviceError(object){
    baseValidator.validate(object, serviceErrorSchema);
}

module.exports = {
    serviceError,
};
