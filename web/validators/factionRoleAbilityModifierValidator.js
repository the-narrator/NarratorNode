const baseValidator = require('./baseValidator');


const upsertFactionRoleAbilityModifierRequestSchema = {
    type: 'object',
    properties: {
        name: {
            type: 'string',
            required: true,
            length: '1',
        },
        value: {
            type: ['number', 'boolean'],
            required: true,
        },
        minPlayerCount: {
            type: 'number',
            required: true,
        },
        maxPlayerCount: {
            type: 'number',
            required: true,
        },
    },
};

function upsertFactionRoleAbilityModifierRequest(query){
    baseValidator.validate(query, upsertFactionRoleAbilityModifierRequestSchema);
}

module.exports = {
    upsertFactionRoleAbilityModifierRequest,
};
