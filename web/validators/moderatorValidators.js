const baseValidator = require('./baseValidator');


const repickRequestSchema = {
    type: 'object',
    properties: {
        gameID: {
            type: 'number',
            required: true,
        },
        repickTarget: {
            type: 'string',
        },
    },
};

function repickRequest(query){
    baseValidator.validate(query, repickRequestSchema);
}

module.exports = {
    repickRequest,
};
