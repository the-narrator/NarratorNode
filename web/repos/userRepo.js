const db = require('../db_communicator');
const exceptions = require('../exceptions');
const userTypes = require('../models/enums/userTypes');


async function create(name){
    const results = await db.query('INSERT INTO users (name) VALUES (?);', [name]);
    return {
        id: parseInt(results.insertId, 10),
        name,
    };
}

function deleteAll(){
    return db.query('DELETE FROM users;');
}

async function getAll(){
    const queryText = 'SELECT id, name, points, win_rate FROM users';
    const results = await db.query(queryText);
    // eslint-disable-next-line camelcase
    return results.map(parseResult);
}

async function getByGameID(gameID){
    const queryText = ''
        + 'SELECT id, name FROM users '
        + 'LEFT JOIN game_users ON users.id = game_users.user_id '
        + 'WHERE game_id = ?;';
    const results = await db.query(queryText, [gameID]);
    // eslint-disable-next-line camelcase
    return results.map(parseResult);
}

async function getByID(userID, defaultValue){
    const results = await db.query('SELECT * from users WHERE id = ?;', [userID]);
    if(results.length)
        return parseResult(results[0]);

    if(defaultValue)
        return defaultValue;
    exceptions.dbNotFoundException(userID);
}

async function getOne(){
    const queryText = 'SELECT id, name FROM users LIMIT 1';
    const results = await db.query(queryText);
    return parseResult(results[0]);
}

// async function saveUser(externalID, userType, isGuest){
//     var result = await db.query('INSERT INTO users (token, type, is_guest) VALUES (?, ?, ?);', [externalID, userType, isGuest]);
//     return {
//         id: parseInt(result.insertId),
//         token: externalID,
//         isGuest: isGuest
//     }
// }

async function findByExternalID(externalID, userType){
    const queryText = 'SELECT users.id as id '
        + 'FROM user_integrations '
        + 'LEFT JOIN users '
        + 'ON users.id = user_integrations.user_id '
        + 'WHERE user_integrations.external_id = ? '
        + 'AND user_integrations.integration_type = ?;';
    const results = await db.query(queryText, [externalID, userType]);
    if(!results.length || results[0].id === null)
        return null;
    return parseResult(results[0]);
}

function findByFirebaseToken(externalID){
    return findByExternalID(externalID, userTypes.FIREBASE);
}

module.exports = {
    create,
    deleteAll,
    getAll,
    getByGameID,
    getByID,
    getOne,
    findByExternalID,
    findByFirebaseToken,
    // saveUser: saveUser
};

function parseResult({ id, name }){
    return {
        id: parseInt(id, 10),
        name,
    };
}
