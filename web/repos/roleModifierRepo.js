const db = require('../db_communicator');


async function getBySetupID(setupID){
    const boolQueryText = ''
        + 'SELECT role_id FROM role_bool_modifiers '
        + 'LEFT JOIN roles ON roles.id = role_bool_modifiers.role_id '
        + 'WHERE roles.setup_id = ?;';
    const intQueryText = ''
        + 'SELECT role_id FROM role_int_modifiers '
        + 'LEFT JOIN roles ON roles.id = role_int_modifiers.role_id '
        + 'WHERE roles.setup_id = ?;';
    const [boolResults, intResults] = await Promise.all([
        db.query(boolQueryText, [setupID]),
        db.query(intQueryText, [setupID]),
    ]);

    return [
        ...boolResults.map(result => ({ role_id: result.role_id })),
        ...intResults.map(result => ({ role_id: result.role_id })),
    ];
}

module.exports = {
    getBySetupID,
};
