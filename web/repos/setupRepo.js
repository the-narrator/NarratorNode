const userPermissions = require('../models/enums/userPermissions');
const db = require('../db_communicator');

const httpHelpers = require('../utils/httpHelpers');


async function create(setup){
    const queryText = 'INSERT INTO setups '
        + '(owner_id, name, is_active) '
        + 'VALUES (?, ?, ?);';
    const params = [setup.ownerID, setup.name, setup.isActive];
    const result = await db.query(queryText, params);
    return {
        id: result.insertId,
        ownerID: setup.ownerID,
        name: setup.name,
    };
}

async function getByID(setupID){
    const queryText = 'SELECT description, slogan, name, owner_id as ownerID, '
        + 'is_editable as isEditable '
        + 'FROM setups '
        + 'WHERE id = ?;';
    const results = await db.query(queryText, [setupID]);
    if(!results.length)
        throw httpHelpers.httpError('No setup found with that id', 404);
    return {
        description: results[0].description,
        id: setupID,
        isEditable: !!results[0].isEditable,
        name: results[0].name,
        ownerID: parseInt(results[0].ownerID, 10),
        slogan: results[0].slogan,
    };
}

async function getFeaturedSetups(){
    const queryText = ''
        + 'SELECT featured_setups.priority, setups.id, setups.name, setups.slogan, '
        + 'setups.description, featured_setups.key '
        + 'FROM featured_setups '
        + 'INNER JOIN setups ON setups.id = featured_setups.setup_id';
    const results = await db.query(queryText);
    return results.map(result => ({
        description: result.description,
        name: result.name,
        key: result.key,
        id: parseInt(result.id, 10),
        priority: result.priority,
        slogan: result.slogan,
    }));
}

async function getModifiersByID(setupID){
    const boolQueryText = ''
        + 'SELECT name, value, min_player_count, max_player_count '
        + 'FROM setup_bool_modifiers '
        + 'WHERE setup_id = ?;';
    const intQueryText = ''
        + 'SELECT name, value, min_player_count, max_player_count '
        + 'FROM setup_int_modifiers '
        + 'WHERE setup_id = ?;';
    const [preBoolResults, preIntResults] = await Promise.all([
        db.query(boolQueryText, [setupID]),
        db.query(intQueryText, [setupID]),
    ]);
    const boolResults = preBoolResults.map(result => ({
        ...result,
        value: !!result.value,
    }));
    const intResults = preIntResults.map(result => ({
        ...result,
        value: parseInt(result.value, 10),
    }));
    return boolResults.concat(intResults).map(result => ({
        name: result.name,
        value: result.value,
        minPlayerCount: parseInt(result.min_player_count, 10),
        maxPlayerCount: parseInt(result.max_player_count, 10),
    }));
}

async function getUserSetups(userID){
    const queryText = ''
        + 'SELECT setups.id, setups.name, setups.slogan, setups.description, setups.is_editable '
        + 'FROM setups '
        + 'WHERE owner_id = ? AND is_active = true;';
    const results = await db.query(queryText, [userID]);
    return results.map(result => ({
        description: result.description,
        name: result.name,
        id: parseInt(result.id, 10),
        isEditable: !!result.is_editable,
        slogan: result.slogan,
    }));
}

function setEditable(setupID){
    const queryText = ''
        + 'UPDATE setups SET is_editable = true WHERE id = ? '
        + 'AND NOT exists (SELECT 1 FROM replays WHERE setup_id = ? and replays.is_started) '
        + 'AND NOT exists (SELECT 1 FROM featured_setups WHERE setup_id = ?);';
    return db.query(queryText, [setupID, setupID, setupID]);
}

function setUneditable(setupID){
    const queryText = 'UPDATE setups SET is_editable = false WHERE id = ?';
    return db.query(queryText, [setupID]);
}

function upsertModifier(setupID, {
    name, value, minPlayerCount, maxPlayerCount,
}){
    const tableName = typeof(value) === 'boolean' ? 'setup_bool_modifiers' : 'setup_int_modifiers';
    const queryText = ''
        + `INSERT INTO ${tableName} `
        + '(setup_id, name, value, min_player_count, max_player_count, upserted_at) '
        + 'VALUES (?, ?, ?, ?, ?, NOW()) '
        + 'ON DUPLICATE KEY UPDATE value = ?, upserted_at = NOW();';
    return db.query(queryText, [setupID, name, value, minPlayerCount, maxPlayerCount, value]);
}

function getAll(){
    const queryText = 'SELECT name, owner_id, id '
        + 'FROM setups';
    return db.query(queryText);
}

function deleteEditableSetups(setupID, userID){
    const queryText = 'DELETE FROM setups '
        + 'WHERE ('
        + '  owner_id = ? OR '
        + '  ? in (SELECT user_id FROM user_permissions WHERE permission_type = ?) '
        + ') '
        + 'AND id = ? '
        + 'AND id NOT IN ('
        + '  SELECT setup_id FROM featured_setups) '
        + 'AND id NOT IN ('
        + '  SELECT setup_id FROM replays);';
    return db.query(queryText, [userID, userID, userPermissions.ADMIN, setupID]);
}

function deactivateNoneditableSetups(setupID, userID){
    const queryText = 'UPDATE setups '
        + 'SET is_active = false '
        + 'WHERE id = ? '
        + 'AND owner_id = ?;';
    return db.query(queryText, [setupID, userID]);
}

function deleteAll(){
    return db.query('DELETE FROM setups;');
}

module.exports = {
    create,
    upsertModifier,
    getByID,
    getFeaturedSetups,
    getModifiersByID,
    getUserSetups,
    setEditable,
    setUneditable,
    deleteEditableSetups,
    deactivateNoneditableSetups,

    getAll,
    deleteAll,
};
