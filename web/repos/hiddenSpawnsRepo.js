const db = require('../db_communicator');

function create(ownerID, setupID, hiddenID, factionRoleID, minPlayerCount, maxPlayerCount){
    const queryText = ''
        + 'INSERT INTO hidden_spawns '
        + 'SELECT NULL as id, ? as hidden_id, ? as faction_role_id, ? as setup_id, '
        + '? as min_player_count, ? as max_player_count FROM setups '
        + 'WHERE setups.id = ? AND setups.owner_id = ?;';
    return db.query(queryText,
        [hiddenID, factionRoleID, setupID, minPlayerCount, maxPlayerCount, setupID, ownerID]);
}

async function isEditable(hiddenSpawnID){
    const queryText = ''
        + 'SELECT setups.is_editable FROM hiddens '
        + 'INNER JOIN setups ON setups.id = hiddens.setup_id '
        + 'INNER JOIN hidden_spawns ON hidden_spawns.hidden_id = hiddens.id '
        + 'WHERE hidden_spawns.id = ?;';
    const results = await db.query(queryText, [hiddenSpawnID]);
    return results.length && results[0].is_editable;
}

module.exports = {
    create,
    isEditable,
};
