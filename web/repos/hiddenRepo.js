const db = require('../db_communicator');


async function create(setupID, hiddenName){
    const queryText = ''
        + 'INSERT INTO hiddens (setup_id, name) '
        + 'VALUES (?, ?);';
    const result = await db.query(queryText, [setupID, hiddenName]);
    return result.insertId;
}

async function isEditable(hiddenIDs){
    const queryText = ''
        + 'SELECT setups.is_editable FROM hiddens '
        + 'INNER JOIN setups ON setups.id = hiddens.setup_id '
        + `WHERE hiddens.id in (${hiddenIDs.join(',')});`;
    const results = await db.query(queryText);
    return results.reduce((acc, result) => acc && result.is_editable, results.length);
}

module.exports = {
    create,
    isEditable,
};
