const db = require('../db_communicator');


function insert(gameID, name, value){
    const table = typeof(value) === 'number' ? 'game_int_modifiers'
        : 'game_bool_modifiers';
    const queryText = ''
        + `INSERT INTO ${table} (game_id, name, value) `
        + 'VALUES (?, ?, ?);';
    return db.query(queryText, [gameID, name, value]);
}

async function getByGameID(gameID){
    const booleanQueryText = ''
        + 'SELECT name, value FROM game_bool_modifiers '
        + 'WHERE game_id = ?;';
    const intQueryText = ''
        + 'SELECT name, value FROM game_int_modifiers '
        + 'WHERE game_id = ?;';
    const [intResults, boolResults] = await Promise.all([
        db.query(intQueryText, [gameID]),
        db.query(booleanQueryText, [gameID]),
    ]);
    const modifiers = {};
    intResults.forEach(result => {
        modifiers[result.name] = parseInt(result.value, 10);
    });
    boolResults.forEach(result => {
        modifiers[result.name] = !!result.value;
    });
    return modifiers;
}

module.exports = {
    insert,
    getByGameID,
};
