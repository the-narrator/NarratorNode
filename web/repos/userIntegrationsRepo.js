const db = require('../db_communicator');

const userType = require('../models/enums/userTypes');


function add(userID, externalID, integrationType){
    const queryText = 'INSERT IGNORE INTO user_integrations '
        + '(user_id, external_id, integration_type) '
        + 'VALUES (?, ?, ?);';
    return db.query(queryText, [userID, externalID, integrationType]);
}

async function getByUserID(userID){
    const query = 'SELECT * '
        + 'FROM user_integrations '
        + 'WHERE user_id = ?;';
    const results = await db.query(query, [userID]);
    return results.map(result => ({
        userID,
        externalID: result.external_id,
        integrationType: result.integration_type,
    }));
}

async function getByExternalID(externalID, integrationType){
    const query = 'SELECT * '
        + 'FROM user_integrations '
        + 'WHERE external_id = ? '
        + 'AND integration_type = ?;';
    const results = await db.query(query, [externalID, integrationType]);
    if(!results.length)
        return null;
    const result = results[0];
    return {
        userID: parseInt(result.user_id, 10),
        externalID: result.external_id,
        integrationType: result.integration_type,
    };
}

async function getDiscordID(userID){
    const query = 'SELECT external_id '
        + 'FROM user_integrations '
        + 'WHERE user_id = ? '
        + 'AND integration_type = ?;';
    const results = await db.query(query, [userID, userType.DISCORD]);
    if(!results.length)
        return null;
    return results[0].external_id;
}

module.exports = {
    add,
    getDiscordID,
    getByExternalID,
    getByUserID,
};
