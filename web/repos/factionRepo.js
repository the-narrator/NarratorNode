const db = require('../db_communicator');


function deleteByID(setupID, factionID){
    const queryText = 'DELETE FROM factions WHERE setup_id = ? AND id = ?;';
    return db.query(queryText, [setupID, factionID]);
}

async function getEdibilityDetails(factionID){
    const queryText = ''
        + 'SELECT setups.is_editable, setups.owner_id FROM factions '
        + 'INNER JOIN setups ON setups.id = factions.setup_id '
        + 'WHERE factions.id = ?;';
    const results = await db.query(queryText, [factionID]);
    if(!results.length)
        return { isEditable: false };
    const [result] = results;
    return {
        isEditable: result.is_editable,
        ownerID: parseInt(result.owner_id, 10),
    };
}

module.exports = {
    deleteByID,
    getEdibilityDetails,
};
