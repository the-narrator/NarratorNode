const db = require('../db_communicator');


async function getByID(gameID){
    const queryText = 'SELECT setup_id, is_started, instance_id FROM replays '
        + 'WHERE id = ?;';
    const results = await db.query(queryText, [gameID]);
    if(!results.length)
        throw new Error('No game with that id.');
    return parseResult(results[0], { gameID });
}


async function getByLobbyID(lobbyID){
    const queryText = 'SELECT id, setup_id, is_started, instance_id FROM replays '
        + 'WHERE instance_id = ?;';
    const results = await db.query(queryText, [lobbyID]);
    if(!results.length)
        throw new Error('No game with that id.');
    return parseResult(results[0], { lobbyID });
}

async function getJoinIDByUserID(userID){
    const queryText = ''
        + 'SELECT replays.instance_id FROM replays LEFT JOIN game_users '
        + 'ON replays.id = game_users.game_id '
        + 'WHERE user_id = ? AND is_started = true AND instance_id IS NOT NULL;';
    const results = await db.query(queryText, [userID]);
    if(!results.length)
        return null;
    return results[0].instance_id;
}

function deleteAll(){
    return db.query('DELETE FROM replays;');
}

module.exports = {
    getByID,
    getByLobbyID,
    getJoinIDByUserID,
    deleteAll,
};

function parseResult(result, { gameID, lobbyID }){
    return {
        id: gameID || parseInt(result.id, 10),
        isStarted: !!result.is_started,
        lobbyID: lobbyID || result.instance_id,
        setupID: parseInt(result.setup_id, 10),
    };
}
