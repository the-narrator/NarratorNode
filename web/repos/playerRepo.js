const db = require('../db_communicator');


async function getByNameAndUserID(name, userID){
    const queryText = ''
        + 'SELECT id FROM players '
        + 'WHERE name = ? AND user_id = ?;';
    const results = await db.query(queryText, [name, userID]);
    if(!results.length)
        return null;
    return { name, userID, id: results[0].id };
}

async function getNamesByUserIDs(userIDSet){
    if(!userIDSet.size)
        return [];
    const userIDs = [...userIDSet];
    const queryText = ''
        + 'SELECT user_id as userID, name FROM players '
        + `WHERE user_id IN (${userIDs.join(',')});`;
    const results = await db.query(queryText);
    return results.map(({ name, userID }) => ({
        name,
        userID: parseInt(userID, 10),
    }));
}

module.exports = {
    getByNameAndUserID,
    getNamesByUserIDs,
};
