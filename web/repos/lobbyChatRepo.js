const db = require('../db_communicator');
const httpHelpers = require('../utils/httpHelpers');
const sqlErrors = require('../repos/util/sqlErrors');


async function createMessage({ gameID, text, userID }){
    const query = ''
        + 'INSERT INTO lobby_chat '
        + '(game_id, user_id, text) '
        + 'VALUES (?, ?, ?);';
    let results;
    try{
        results = await db.query(query, [gameID, userID, text]);
    }catch(err){
        if([sqlErrors.BAD_FIELD_ERROR, sqlErrors.NONEXISTANT_ID].includes(err.errno))
            throw httpHelpers.httpError('No game with that ID', 404);
        throw err;
    }
    return parseInt(results.insertId, 10);
}

async function get(gameID){
    const query = ''
        + 'SELECT created_at, id, user_id, text FROM lobby_chat '
        + 'WHERE game_id = ?;';
    const results = await db.query(query, [gameID]);
    return results.map(r => ({
        createdAt: r.created_at,
        id: parseInt(r.id, 10),
        text: r.text,
        userID: parseInt(r.user_id, 10),
    }));
}

module.exports = {
    createMessage,
    get,
};
