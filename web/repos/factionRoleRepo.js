const db = require('../db_communicator');


async function getByID(factionRoleID){
    const queryText = ''
        + 'SELECT id, received_faction_role_id, role_id from faction_roles '
        + 'WHERE id = ?;';
    const [result] = await db.query(queryText, [factionRoleID]);
    if(!result)
        return null;
    return {
        id: result.id,
        roleID: result.role_id,
        receivedFactionRoleID: result.received_faction_role_id
            ? parseInt(result.received_faction_role_id, 10) : null,
    };
}

async function isEditable(factionRoleID){
    const queryText = ''
        + 'SELECT setups.is_editable FROM setups '
        + 'INNER JOIN faction_roles ON faction_roles.setup_id = setups.id '
        + 'WHERE faction_roles.id = ?;';
    const results = await db.query(queryText, [factionRoleID]);
    return results.length && results[0].is_editable;
}

module.exports = {
    getByID,
    isEditable,
};
