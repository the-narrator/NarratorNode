const db = require('../db_communicator');


async function createIteration(setupID, playerCount, authorID){
    const _ = 'INSERT INTO setup_iterations (setup_id, player_count, author_id)';
    const queryText = `${_} VALUES (?, ?, ?);`;
    const result = await db.query(queryText, [setupID, playerCount, authorID]);
    return parseInt(result.insertId, 10);
}

module.exports = {
    createIteration,
};
