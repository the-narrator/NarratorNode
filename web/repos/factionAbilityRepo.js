const db = require('../db_communicator');


async function getModifiersBySetupID(setupID){
    const boolQueryText = ''
        + 'SELECT faction_ability_bool_modifiers.name, value FROM faction_ability_bool_modifiers '
        + 'LEFT JOIN faction_abilities ON faction_abilities.id = faction_ability_bool_modifiers.faction_ability_id '// eslint-disable-line max-len
        + 'LEFT JOIN factions ON factions.id = faction_abilities.faction_id '
        + 'WHERE factions.setup_id = ?;';
    const intQueryText = ''
        + 'SELECT faction_ability_int_modifiers.name, value FROM faction_ability_int_modifiers '
        + 'LEFT JOIN faction_abilities ON faction_abilities.id = faction_ability_int_modifiers.faction_ability_id '// eslint-disable-line max-len
        + 'LEFT JOIN factions ON factions.id = faction_abilities.faction_id '
        + 'WHERE factions.setup_id = ?;';
    const [boolResults, intResults] = await Promise.all([
        db.query(boolQueryText, [setupID]),
        db.query(intQueryText, [setupID]),
    ]);
    return [
        ...boolResults.map(result => ({
            name: result.name,
            value: !!result.value,
        })),
        ...intResults.map(result => ({
            name: result.name,
            value: result.value,
        }))];
}

async function isEditable(factionAbilityID){
    const queryText = ''
        + 'SELECT setups.is_editable FROM faction_abilities '
        + 'INNER JOIN factions ON factions.id = faction_abilities.faction_id '
        + 'INNER JOIN setups ON setups.id = factions.setup_id '
        + 'WHERE faction_abilities.id = ?;';
    const results = await db.query(queryText, [factionAbilityID]);
    return results.length && results[0].is_editable;
}

module.exports = {
    getModifiersBySetupID,
    isEditable,
};
