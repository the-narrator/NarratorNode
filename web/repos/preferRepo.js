const db = require('../db_communicator');


async function getBySetupIDUserID(setupID, userID){
    const queryText = ''
        + 'SELECT entity_id, prefer_type FROM player_preferences '
        + 'WHERE user_id = ? AND setup_id = ?;';
    const results = await db.query(queryText, [userID, setupID]);
    return results.map(r => ({
        entityID: parseInt(r.entity_id, 10),
        preferType: r.prefer_type,
    }));
}

function deleteBySetupIDUserID(setupID, userID){
    const queryText = ''
        + 'DELETE FROM player_preferences '
        + 'WHERE user_id = ? AND setup_id = ?';
    return db.query(queryText, [userID, setupID]);
}

module.exports = {
    getBySetupIDUserID,
    deleteBySetupIDUserID,
};
