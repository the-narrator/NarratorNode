const db = require('../db_communicator');


async function getByID(roleID){
    const queryText = 'SELECT name FROM roles WHERE id = ?;';
    const results = await db.query(queryText, [roleID]);
    if(!results.length)
        return null;
    return {
        id: roleID,
        name: results[0].name,
    };
}

async function isEditable(roleID){
    const queryText = ''
        + 'SELECT setups.is_editable FROM roles '
        + 'INNER JOIN setups ON setups.id = roles.setup_id '
        + 'WHERE roles.id = ?;';
    const results = await db.query(queryText, [roleID]);
    return results.length && results[0].is_editable;
}

module.exports = {
    getByID,
    isEditable,
};
