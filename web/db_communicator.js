const mysql = require('mysql');

const config = require('../config.json');

const loggingService = require('./services/loggingService');

let logging = false;

function setLogging(loggingInput){
    logging = loggingInput;
}

const dbConnection = mysql.createPool({
    connectionLimit: 5,
    host: config.db_hostname,
    user: config.db_username,
    password: config.db_password,
    database: config.db_name,
    supportBigNumbers: true,
    bigNumberStrings: true,
    charset: 'utf8mb4',
});


// https://github.com/brianc/node-postgres/wiki/Parameterized-queries-and-Prepared-Statements
async function query(qString, params){
    if(!params)
        params = [];

    const startTime = new Date().getTime();
    const returnValue = await new Promise((resolve, reject) => {
        dbConnection.query(qString, params, (err, results) => {
            if(err && err.code !== 'ER_EMPTY_QUERY')
                return reject(err);

            if(err && err.code === 'ER_EMPTY_QUERY')
                results = [];

            if(logging){
                console.log(qString, params); /* eslint-disable-line no-console */
                console.log(`\t${JSON.stringify(results)}`); /* eslint-disable-line no-console */
            }
            resolve(results);
        });
    });
    const endTime = new Date().getTime();
    if(endTime - startTime > config.query_max_time)
        loggingService.reportSlowCall({
            name: 'Hansel db query',
            time: endTime - startTime,
            query: qString,
        });

    return returnValue;
}

function close(){
    dbConnection.end();
}

module.exports = {
    query,
    close,
    setLogging,
};
