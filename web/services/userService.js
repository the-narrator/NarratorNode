const authValidators = require('../validators/authValidators');
const helpers = require('../utils/helpers');
const httpHelpers = require('../utils/httpHelpers');

const browserWrapper = require('../channels/browser/browserWrapper');

const playerRepo = require('../repos/playerRepo');
const userRepo = require('../repos/userRepo');
const userIntegrationsRepo = require('../repos/userIntegrationsRepo');


const tempAuthToID = {};
const tempIDToAuth = {};

function addTempAuth(userID){
    const authToken = helpers.getRandomString(128);
    if(tempIDToAuth[userID]){
        const oldToken = tempIDToAuth[userID];
        delete tempAuthToID[oldToken];
    }
    tempAuthToID[authToken] = userID;
    tempIDToAuth[userID] = authToken;
    return authToken;
}

async function create(name, externalID, integrationType){
    const user = await userRepo.create(name);
    if(!externalID)
        return user;
    await userIntegrationsRepo.add(user.id, externalID, integrationType);
    user.integrations = [{
        externalID,
        integrationType,
    }];
    return user;
}

async function getByAuthToken(request, allowCreate = false){
    authValidators.socketAuth(request);
    const { token } = request;
    const wrapperType = request.wrapperType.toLowerCase();

    if(wrapperType === 'tempauth'){
        if(tempAuthToID[token])
            return getByID(tempAuthToID[token]);
        throw httpHelpers.httpError('Authentication failed.', 422);
    }

    const wrapper = helpers.getWrapper(wrapperType);
    const externalUserInfo = await wrapper.getUserInfoByAuthToken(token);
    if(!externalUserInfo)
        throw httpHelpers.httpError('Authentication failed.', 422);

    if(allowCreate)
        return getOrCreateByExternalID(externalUserInfo.name, externalUserInfo.externalID,
            wrapperType);

    const user = await findByExternal(externalUserInfo.externalID, wrapperType);
    if(user)
        return user;
    throw httpHelpers.httpError('Unregistered user.', 403);
}

function getByID(userID){
    return userRepo.getByID(userID);
}

function getIDByAuthToken(authToken){
    return tempAuthToID[authToken];
}

async function getNamesFromIDs(userIDs){
    const userNames = await playerRepo.getNamesByUserIDs(userIDs);
    const userIDMap = userNames.reduce((acc, player) => ({
        ...acc,
        [player.userID]: {
            id: player.userID,
            name: player.name,
        },
    }), {});
    const noNameUsers = [...userIDs]
        .filter(userID => !userIDMap[userID])
        .map(userID => ({ id: userID, name: 'guest' }));

    return [
        ...Object.values(userIDMap),
        ...noNameUsers,
    ];
}

async function getOrCreateByExternalID(name, externalID, wrapperType){
    const user = await findByExternal(externalID, wrapperType);
    if(user)
        return user;

    if(!name)
        throw new Error('Creating a user needs a name.');
    return create(name, externalID, wrapperType);
}

async function findByExternal(externalID, wrapperType){
    const user = await userRepo.findByExternalID(externalID, wrapperType);
    if(user)
        return getNestedInfo(user.id, user);
    return null;
}

async function getNestedInfo(userID, cachedUser, cachedIntegrations){
    const user = cachedUser || await userRepo.getByID(userID);
    user.integrations = cachedIntegrations || await userIntegrationsRepo.getByUserID(userID);
    return user;
}

function getOnlineUsers(){
    const browserUserIDs = browserWrapper.getOnlineUserIDs();
    return getNamesFromIDs(browserUserIDs);
}

function getUsers(){
    return userRepo.getAll();
}

function getUsersByGameID(gameID){
    return userRepo.getByGameID(gameID);
}

module.exports = {
    addTempAuth,
    create,
    findByExternal,
    getByAuthToken,
    getByID,
    getOrCreateByExternalID,
    getIDByAuthToken,
    getNamesFromIDs,
    getNestedInfo,
    getOnlineUsers,
    getUsers,
    getUsersByGameID,
};
