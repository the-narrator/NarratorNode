const helpers = require('../utils/helpers');
const httpHelpers = require('../utils/httpHelpers');

const userIntegrationsRepo = require('../repos/userIntegrationsRepo');


async function addAuthToken(auth, wrapperType, authToken){
    const userService = require('./userService');
    const userID = userService.getIDByAuthToken(authToken);
    if(!userID)
        throw httpHelpers.httpError('Unnknown auth token.', 401);

    const wrapper = helpers.getWrapper(wrapperType);
    const externalUserInfo = await wrapper.getUserInfoByAuthToken(auth);
    if(!externalUserInfo)
        throw httpHelpers.httpError('Authentication failed.', 422);

    const { externalID } = externalUserInfo;
    await userIntegrationsRepo.add(userID, externalID, wrapperType);

    return userService.getNestedInfo(userID);
}

module.exports = {
    addAuthToken,
};
