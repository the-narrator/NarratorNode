const httpHelpers = require('../utils/httpHelpers');
const gretelClient = require('../utils/gretelClient');


async function addBots(userID, botCount){
    const serverResponse = await gretelClient.sendRequest('POST', 'players', { userID, botCount });
    if(serverResponse.errors.length)
        throw httpHelpers.httpError(serverResponse.errors, 422);
    return serverResponse.response;
}

async function deletePlayer(gameID, playerKickID){
    const serverResponse = await gretelClient.sendRequest(
        'DELETE', 'players', { gameID, playerKickID },
    );
    if(serverResponse.errors.length)
        throw httpHelpers.httpError(serverResponse.errors, 422);
    return serverResponse.response;
}

async function create(userID, playerName, joinID){
    const args = {
        userID,
        joinID,
        playerName: playerName || 'guest',
    };
    const { errors, response: game } = await gretelClient.sendRequest('POST', 'players', args);
    if(errors.length)
        throw httpHelpers.httpError(errors, 422);
    return game;
}

async function filterKeys(game, requesterUserID){
    const players = game.players;
    players.sort((p1, p2) => p1.name.localeCompare(p2.name));
    if(!game.isStarted || !requesterUserID)
        return players;
    const phaseService = require('./phaseService');
    const phaseLength = phaseService.getPhaseLength(game.joinID) * 1000;
    const endTime = phaseService.getEndTime(game.joinID);
    const now = new Date().getTime();
    if(now > endTime - (phaseLength / 3))
        return game.players;
    return players.map(player => {
        if(player.userID === requesterUserID)
            return player;
        return { ...player, endedNight: false };
    });
}

async function kick(kickerID, userID){
    const args = { kickerID, userID };
    const serverResponse = await gretelClient.sendRequest('DELETE', 'players', args);
    if(serverResponse.errors.length)
        throw httpHelpers.httpError(serverResponse.errors, 422);
    return serverResponse.response;
}

async function leave(userID){
    const args = {
        kickedUserID: userID,
        kickerID: userID,
    };
    const responseObj = await gretelClient.sendRequest('DELETE', 'players', args);
    if(responseObj.errors.length)
        throw httpHelpers.httpError(responseObj.errors, 422);
}

async function modkill(userID){
    const phaseService = require('./phaseService');
    const timeLeft = await phaseService.getPercentTimeLeftByUserID(userID);
    const responseObj = await gretelClient.sendRequest('PUT', 'players', { userID, timeLeft });
    if(responseObj.errors.length)
        throw httpHelpers.httpError(responseObj.errors, 422);
}

async function updatePlayerName(userID, playerName){
    const args = { userID, playerName };
    const responseObj = await gretelClient.sendRequest('PUT', 'players', args);

    if(responseObj.errors.length)
        throw httpHelpers.httpError(responseObj.errors, 422);

    if(!responseObj.response)
        return;
    const { joinID, playerID } = responseObj.response;
    const eventService = require('./eventService');
    await eventService.playerNameUpdate({ ...args, joinID, playerID });
}

module.exports = {
    addBots,
    deletePlayer,
    create,
    filterKeys,
    kick,
    leave,
    modkill,
    updatePlayerName,
};
