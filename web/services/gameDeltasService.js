const helpers = require('../utils/helpers');

const userService = require('../services/userService');

const userTypes = require('../models/enums/userTypes');

const sc2mafiaClient = require('../channels/sc2mafia/sc2mafiaClient');


const TEN_MINUTES = 1000 * 60 * 10;

let lastCheckTime = new Date();

async function findGameDeltas(){
    const requestTime = lastCheckTime;
    lastCheckTime = new Date();
    if(!helpers.serviceEnabled('sc2mafia'))
        return [];
    const gameDeltas = await sc2mafiaClient.findGameDeltas(requestTime);

    let gamesAndUsers = await Promise.all(gameDeltas.map(getExternalGameUser));
    const gameService = require('./gameService');
    const games = await gameService.getAll();

    const userIDsInGames = games.map(game => game.users.map(({ id }) => id));
    const usersInGamesSet = new Set(helpers.flattenList(userIDsInGames));
    gamesAndUsers = gamesAndUsers
        .filter(userAndGame => !userAndGame.user || !usersInGamesSet.has(userAndGame.user.id));

    return postNewGames(gamesAndUsers, gameService);
}

setInterval(findGameDeltas, TEN_MINUTES);

module.exports = {
    findGameDeltas,
};

function postNewGames(gameRequests, gameService){
    const setupService = require('./setup/setupService');
    return Promise.all(gameRequests.map(async request => {
        const hostName = request.user.name;
        const user = request.user || await userService.create(hostName, request.externalUserID,
            request.integrationType);
        const setupID = await setupService.getDefaultSetupID(user.id);
        const game = await gameService.create(user.id, hostName, setupID);
        await Object.entries(request.externalGame.gameModifiers || {})
            .map(([name, value]) => gameService.updateModifier(user.id, { name, value }));
        const token = await userService.addTempAuth(user.id);
        const externalInfo = await sc2mafiaClient.postGame(request.externalGame, game, token);
        return {
            externalInfo,
            joinID: game.joinID,
            host: user,
        };
    }));
}

async function getExternalGameUser(externalGame){
    const user = await userService.getOrCreateByExternalID(externalGame.name,
        externalGame.externalUserID, userTypes.SC2MAFIA);
    return {
        externalGame,
        user,
    };
}
