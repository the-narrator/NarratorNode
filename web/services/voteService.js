const httpHelpers = require('../utils/httpHelpers');
const gretelClient = require('../utils/gretelClient');


async function getVotes(gameID){
    const args = {
        gameID,
    };
    const javaResponse = await gretelClient.sendRequest('GET', 'votes', args);
    if(javaResponse.errors.length)
        throw httpHelpers.httpError(javaResponse.errors, 422);
    return javaResponse.response;
}

module.exports = {
    getVotes,
};
