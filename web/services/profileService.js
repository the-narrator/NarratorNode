const httpHelpers = require('../utils/httpHelpers');
const gretelClient = require('../utils/gretelClient');


async function get(userID){
    const javaResponse = await gretelClient.sendRequest('GET', 'profiles', { userID });
    if(javaResponse.errors.length)
        throw httpHelpers.httpError(javaResponse.errors, 422);
    javaResponse.response.userID = userID;
    return javaResponse.response;
}

module.exports = {
    get,
};
