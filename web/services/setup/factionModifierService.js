const httpHelpers = require('../../utils/httpHelpers');
const gretelClient = require('../../utils/gretelClient');


async function create(userID, factionID, args){
    const factionSerivce = require('./factionService');
    await factionSerivce.ensureEditableByUser(userID, factionID);
    args = Object.assign({}, { userID }, { factionID }, args);
    const javaResponse = await gretelClient.sendRequest('POST', 'factionModifiers', args);
    if(javaResponse.errors.length)
        throw httpHelpers.httpError(javaResponse.errors, 422);
    const eventService = require('../eventService');
    eventService.factionModifierUpsert(javaResponse.response);
    return javaResponse.response.modifier;
}

module.exports = {
    create,
};
