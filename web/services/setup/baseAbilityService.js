const httpHelpers = require('../../utils/httpHelpers');
const gretelClient = require('../../utils/gretelClient');


async function getBaseAbilities(){
    const gretelResponse = await gretelClient.sendRequest('GET', 'baseAbilities');
    if(gretelResponse.errors.length)
        throw httpHelpers.httpError(gretelResponse.errors, 422);
    return gretelResponse.response;
}

module.exports = {
    getBaseAbilities,
};
