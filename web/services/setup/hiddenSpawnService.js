const httpHelpers = require('../../utils/httpHelpers');
const gretelClient = require('../../utils/gretelClient');

const hiddenRepo = require('../../repos/hiddenRepo');
const hiddenSpawnRepo = require('../../repos/hiddenSpawnsRepo');


async function createMany(userID, spawns){
    const hiddenIDs = new Set(spawns.map(spawn => spawn.hiddenID));
    const isEditable = await hiddenRepo.isEditable(Array.from(hiddenIDs));
    if(!isEditable)
        throw httpHelpers.frozenSetupError();
    const request = { userID, spawns };
    const javaResponse = await gretelClient.sendRequest('POST', 'hiddenSpawns', request);
    if(javaResponse.errors.length)
        throw httpHelpers.httpError(javaResponse.errors, 422);
    return javaResponse.response;
}

function createSpawn(ownerID, setupID, hiddenID, factionRoleID, minPlayerCount, maxPlayerCount){
    // runs the risk of not creating anything
    return hiddenSpawnRepo.create(ownerID, setupID, hiddenID, factionRoleID,
        minPlayerCount, maxPlayerCount);
}

async function deleteSpawn(userID, hiddenSpawnID){
    const isEditable = await hiddenSpawnRepo.isEditable(hiddenSpawnID);
    if(!isEditable)
        throw httpHelpers.frozenSetupError();
    const javaResponse = await gretelClient.sendRequest('DELETE', 'hiddenSpawns',
        { userID, hiddenSpawnID });
    if(javaResponse.errors.length)
        throw httpHelpers.httpError(javaResponse.errors, 422);
}

module.exports = {
    createMany,
    createSpawn,
    deleteSpawn,
};
