const httpHelpers = require('../../utils/httpHelpers');
const gretelClient = require('../../utils/gretelClient');

const factionRoleRepo = require('../../repos/factionRoleRepo');


async function upsert(userID, factionRoleID, abilityID, args){
    const isEditable = await factionRoleRepo.isEditable(factionRoleID);
    if(!isEditable)
        throw httpHelpers.frozenSetupError();
    args = Object.assign({}, { userID }, { factionRoleID, abilityID }, args);
    const javaResponse = await gretelClient.sendRequest('POST', 'factionRoleAbilityModifiers',
        args);
    if(javaResponse.errors.length)
        throw httpHelpers.httpError(javaResponse.errors, 422);
    const eventService = require('../eventService');
    await eventService.factionRoleAbilityModifierUpsert(javaResponse.response);
    return javaResponse.response.modifier;
}

module.exports = {
    upsert,
};
