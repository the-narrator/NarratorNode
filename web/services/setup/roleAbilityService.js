const httpHelpers = require('../../utils/httpHelpers');
const gretelClient = require('../../utils/gretelClient');

const roleRepo = require('../../repos/roleRepo');


async function createModifier(userID, roleID, abilityID, args){
    const isEditable = await roleRepo.isEditable(roleID);
    if(!isEditable)
        throw httpHelpers.frozenSetupError();
    args = Object.assign({}, { userID }, { roleID, abilityID }, args);
    const javaResponse = await gretelClient.sendRequest('POST', 'roleAbilityModifiers', args);
    if(javaResponse.errors.length)
        throw httpHelpers.httpError(javaResponse.errors, 422);
    const eventService = require('../eventService');
    await eventService.roleAbilityModifierUpsert(javaResponse.response);
    return javaResponse.response.modifier;
}

module.exports = {
    createModifier,
};
