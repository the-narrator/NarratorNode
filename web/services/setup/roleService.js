const httpHelpers = require('../../utils/httpHelpers');
const gretelClient = require('../../utils/gretelClient');

const roleRepo = require('../../repos/roleRepo');
const setupRepo = require('../../repos/setupRepo');


async function create(userID, role){
    const setup = await setupRepo.getByID(role.setupID);
    if(!setup.isEditable)
        throw httpHelpers.frozenSetupError();
    const args = { ...role, userID };
    const javaResponse = await gretelClient.sendRequest('POST', 'roles', args);
    if(javaResponse.errors.length)
        throw httpHelpers.httpError(javaResponse.errors, 422);
    return javaResponse.response;
}

async function createModifier(userID, roleID, args){
    const isEditable = await roleRepo.isEditable(roleID);
    if(!isEditable)
        throw httpHelpers.frozenSetupError();
    const javaResponse = await gretelClient.sendRequest('POST', 'roles',
        { ...args, userID, roleID });
    if(javaResponse.errors.length)
        throw httpHelpers.httpError(javaResponse.errors, 422);
    return javaResponse.response.modifier;
}

async function get(userID, roleID){
    const javaResponse = await gretelClient.sendRequest('GET', 'roles', { userID, roleID });
    if(javaResponse.errors.length)
        throw httpHelpers.httpError(javaResponse.errors, 422);
    return javaResponse.response;
}

async function deleteRole(userID, roleID){
    const isEditable = await roleRepo.isEditable(roleID);
    if(!isEditable)
        throw httpHelpers.frozenSetupError();
    const javaResponse = await gretelClient.sendRequest('DELETE', 'roles', { userID, roleID });
    if(javaResponse.errors.length)
        throw httpHelpers.httpError(javaResponse.errors, 422);
    return javaResponse.response;
}

module.exports = {
    create,
    createModifier,
    get,
    deleteRole,
};
