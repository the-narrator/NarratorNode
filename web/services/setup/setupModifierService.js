const httpHelpers = require('../../utils/httpHelpers');
const gretelClient = require('../../utils/gretelClient');

const setupRepo = require('../../repos/setupRepo');


async function create(userID, modifier){
    const args = { ...modifier, userID };
    const javaResponse = await gretelClient.sendRequest('POST', 'setupModifiers', args);
    if(javaResponse.errors.length)
        throw httpHelpers.httpError(javaResponse.errors, 422);
    const eventService = require('../eventService');
    eventService.setupModifierUpsert(javaResponse.response);
    return javaResponse.response.modifier;
}

async function createV2(userID, setupID, modifier){
    const setupService = require('./setupService');
    await setupService.ensureEditable(setupID, userID);
    return setupRepo.upsertModifier(setupID, modifier);
}

module.exports = {
    create,
    createV2,
};
