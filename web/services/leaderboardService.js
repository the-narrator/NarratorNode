function getColumn(column){
    if(!['points', 'gamesPlayed', 'winRate'].includes(column))
        return {};
}

module.exports = {
    getColumn,
};
