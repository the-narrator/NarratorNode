const httpHelpers = require('../utils/httpHelpers');
const gretelClient = require('../utils/gretelClient');

const preferRepo = require('../repos/preferRepo');


async function prefer(userID, preferName, setupID){
    const serverResponse = await gretelClient.sendRequest('POST', 'prefers',
        { userID, preferName, setupID });
    if(serverResponse.errors.length)
        throw httpHelpers.httpError(serverResponse.errors, 422);
    return serverResponse.response;
}

function deleteBySetupIDUserID(setupID, userID){
    return preferRepo.deleteBySetupIDUserID(setupID, userID);
}

module.exports = {
    prefer,
    deleteBySetupIDUserID,
};
