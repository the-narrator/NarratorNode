const helpers = require('../utils/helpers');
const sc2mafiaClient = require('../channels/sc2mafia/sc2mafiaClient');

const userType = require('../models/enums/userTypes');


function createIntegration(gameID, integrationType, args){
    const integration = helpers.getWrapper(integrationType);
    return integration.createIntegration(gameID, args);
}

function clearCache(threadID){
    sc2mafiaClient.clearCache(threadID);
}

async function sc2mafiaNewPost(threadID){
    const playerService = require('./playerService');
    const userService = require('./userService');
    const actions = await sc2mafiaClient.onNewPost(threadID);
    return Promise.all([
        ...actions.playerJoins.map(async({ name, externalID, joinID }) => {
            const user = await userService.getOrCreateByExternalID(name, externalID,
                userType.SC2MAFIA);
            await playerService.create(user.id, name, joinID);
            const authToken = userService.addTempAuth(user.id);
            return sc2mafiaClient.sendNarratorLink(name, authToken);
        }),
        getPlayerVotePromise(actions.playerVotes),
    ]);
}

function getCondorcetVotes(threadID){
    return sc2mafiaClient.getCondorcetVotes(threadID);
}

function getActiveBrowserUserIDs(gameID){
    const app = require('../app');
    return app.browserWrapper.getActiveGameUsersByUserID(gameID);
}

module.exports = {
    createIntegration,
    clearCache,
    getActiveBrowserUserIDs,
    getCondorcetVotes,
    sc2mafiaNewPost,
};

async function getPlayerVotePromise(playerVotes){
    const actionService = require('./actionService');
    const userService = require('./userService');
    for(let i = 0; i < playerVotes.length; i++){
        const playerVote = playerVotes[i];
        const user = await userService.findByExternal(playerVote.externalID, userType.SC2MAFIA);
        if(!user)
            continue;
        await actionService.submitActionMessage(user.id,
            { message: `vote ${playerVote.voteTarget}` })
            .catch(() => {});
    }
}
