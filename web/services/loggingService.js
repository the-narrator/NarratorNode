const config = require('../../config');


function reportSlowCall(metadata){
    const discordJsonClient = require('../channels/discord/discordJsonClient');
    metadata = JSON.stringify(metadata, null, 2).substr(0, 350);
    const message = `\`\`\`${metadata}\`\`\``;
    return discordJsonClient.sendPublicMessage([config.discord.analyticsChannelID], message)
        .catch(() => {});
}

module.exports = {
    reportSlowCall,
};
