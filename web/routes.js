/* eslint-disable max-len */
const express = require('express');

const actionController = require('./controllers/actionController');
const baseAbilityController = require('./controllers/baseAbilityController');
const channelController = require('./controllers/channelController');
const chatController = require('./controllers/chatController');
const condorcetController = require('./controllers/condorcetController');
const factionAbilityController = require('./controllers/factionAbilityController');
const factionController = require('./controllers/factionController');
const factionModifierController = require('./controllers/factionModifierController');
const factionRoleController = require('./controllers/factionRoleController');
const gameController = require('./controllers/gameController');
const gameDeltaController = require('./controllers/gameDeltaController');
const gameModifierController = require('./controllers/gameModifierController');
const hiddenController = require('./controllers/hiddenController');
const hiddenSpawnController = require('./controllers/hiddenSpawnController');
const moderatorController = require('./controllers/moderatorController');
const phaseController = require('./controllers/phaseController');
const playerController = require('./controllers/playerController');
const profileController = require('./controllers/profileController');
const roleController = require('./controllers/roleController');
const setupController = require('./controllers/setupController');
const setupHiddenController = require('./controllers/setupHiddenController');
const setupModifierController = require('./controllers/setupModifierController');
const userController = require('./controllers/userController');
const userIntegrationController = require('./controllers/userIntegrationController');


const app = express();
require('express-ws')(app);

const routes = [
    ['PUT         /api/actions', actionController.submitActionMessage],
    ['DELETE      /api/actions', actionController.deleteAction],

    ['GET         /api/baseAbilities', baseAbilityController.getBaseAbilities],

    ['POST        /api/channels/sc2mafia/thread', channelController.sc2mafiaNewPost],
    ['GET         /api/channels/browser/activeUserIDs', channelController.getActiveBrowserUserIDs],
    ['GET         /api/channels/sc2mafia/condorcet', channelController.getCondorcetVotes],
    ['DELETE      /api/channels/sc2mafia/condorcet', channelController.clearCache],

    ['GET         /api/chats/:gameID/lobby', chatController.getLobbyMessages],
    ['POST        /api/chats/:gameID/lobby', chatController.sendLobbyMessage],
    ['GET         /api/chats', chatController.getUserChat],

    ['POST        /api/condorcet', condorcetController.resolveInput],

    ['POST        /api/factionAbilities/:factionAbilityID/modifiers', factionAbilityController.upsert],

    ['POST        /api/factions/:factionID/abilities', factionController.addAbility],
    ['POST        /api/factions/:factionID/enemies/:enemyID', factionController.addEnemy],
    ['POST        /api/factions/:factionID/checkables/:checkableID', factionController.addSheriffCheckable],
    ['DELETE      /api/factions/:factionID/abilities/:abilityID', factionController.deleteAbility],
    ['POST        /api/factions/', factionController.createFaction],
    ['DELETE      /api/factions/:factionID', factionController.deleteFaction],
    ['POST        /api/factions/:factionID/modifiers', factionModifierController.createModifier],

    ['POST        /api/factionRoles', factionRoleController.create],
    ['GET         /api/factionRoles/:factionRoleID', factionRoleController.get],
    ['POST        /api/factionRoles/:factionRoleID/abilities/:abilityID/modifiers', factionRoleController.upsertAbilityModifier],
    ['POST        /api/factionRoles/:factionRoleID/modifiers', factionRoleController.upsertModifier],
    ['PUT         /api/factionRoles/:factionRoleID', factionRoleController.update],
    ['DELETE      /api/factionRoles/:factionRoleID', factionRoleController.deleteFactionRole],

    ['GET         /api/games/userState', gameController.getDeprecated],
    ['GET         /api/games', gameController.getActive],
    ['GET         /api/games/:gameID', gameController.getByID],
    ['POST        /api/games/:gameID/start', gameController.start],
    ['POST        /api/games', gameController.create],
    ['DELETE      /api/games/:gameID', gameController.deleteGame],

    ['POST        /api/game_deltas', gameDeltaController.findGameDeltas],

    ['POST        /api/gameModifiers', gameModifierController.upsert],

    ['POST        /api/hiddens', hiddenController.create],
    ['DELETE      /api/hiddens/:hiddenID', hiddenController.deleteHidden],

    ['POST        /api/hiddenSpawns', hiddenSpawnController.create],
    ['DELETE      /api/hiddenSpawns/:hiddenSpawnID', hiddenSpawnController.deleteSpawn],

    ['POST        /api/moderators', moderatorController.create],
    ['POST        /api/moderators/repick', moderatorController.repick],
    ['DELETE      /api/moderators', moderatorController.deleteModerator],

    ['PUT         /api/phases', phaseController.setExpirationViaUser],

    ['POST        /api/players/bots', playerController.addBots],
    ['DELETE      /api/players/bots', playerController.deletePlayer],
    ['POST        /api/players', playerController.create],
    ['PUT         /api/players/:playerID', playerController.updatePlayerName],
    ['DELETE      /api/players/kick', playerController.kick],
    ['DELETE      /api/players', playerController.leave],

    ['GET         /api/profiles', profileController.get],

    ['POST        /api/roles', roleController.createRole],
    ['GET         /api/roles/:roleID', roleController.getByID],
    ['POST        /api/roles/:roleID/abilities/:abilityID/modifiers', roleController.upsertAbilityModifier],
    ['POST        /api/roles/:roleID/modifiers', roleController.upsertModifier],
    ['DELETE      /api/roles/:roleID', roleController.deleteRole],

    ['POST        /api/setups/:setupID/featured', setupController.createFeatured],
    ['DELETE      /api/setups/:setupID/featured', setupController.deleteFeatured],
    ['POST        /api/setups', setupController.create],
    ['GET         /api/setups/featured', setupController.getFeatured],
    ['GET         /api/setups/:setupID', setupController.getByID],
    ['POST        /api/setups/:setupID/clone', setupController.clone],
    ['GET         /api/setups/:setupID/iterations', setupController.getIterations],
    ['POST        /api/setups/:setupID/iterations', setupController.createIteration],
    ['GET         /api/setups', setupController.getUserSetups],
    ['PUT         /api/setups/:setupID/factions/:factionID', setupController.updateFaction],
    ['DELETE      /api/setups/:setupID/factions/:factionID/enemies/:enemyFactionID', setupController.deleteFactionEnemy],
    ['PUT         /api/setups', setupController.updateGameSetup],
    ['DELETE      /api/setups/:setupID', setupController.deleteSetup],

    ['POST        /api/setupHiddens', setupHiddenController.createSetupHidden],
    ['DELETE      /api/setupHiddens/:setupHiddenID', setupHiddenController.deleteSetupHidden],

    ['POST        /api/setupModifiers', setupModifierController.upsert],

    ['GET         /api/users/online', userController.getOnline],
    ['GET         /api/users', userController.getAll],
    ['POST        /api/users/:userID/auth_token', userController.addTempAuth],

    ['POST        /api/user_integrations', userIntegrationController.addAuthToken],
];

function errorHandler(func){
    return async(request, response, next) => {
        try{
            const controllerResponse = await func(request);
            if(controllerResponse === undefined)
                return response.status(204).end();
            // doesn't account for non json responses, but i think that's ok
            response.json({
                errors: [],
                response: controllerResponse,
            });
        }catch(err){
            next(err);
        }
    };
}

routes.forEach(([methodUrl, func]) => {
    const [method, url] = methodUrl.split(' ').filter(x => x);
    app[method.toLowerCase()](url, errorHandler(func));
});

module.exports = {
    app,
};

require('./controllers/staticController');
