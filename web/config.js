const configJson = require('../config.json');


function isWaitingForEvents(){
    return configJson.env !== 'prod';
}

module.exports = {
    isWaitingForEvents,
};
