# The Narrator [Web Server]

## Dev Setup
1. clone the repo
`git clone git@gitlab.com:the-narrator/NarratorNode.git`

### Create Config
#### Install jq 
* for mac, this can be done using `brew install jq`
* for windows, this can be done using `choco install jq`

#### Add Environment variables:
Next, we're going to add some environment variables that the backend needs.
 Add the following lines to ~/.bash_profile or ~/.zshrc (depending on which you use).  End of the file is fine.
* `export NARRATOR_CONFIG_LOCATION=<REPO_LOCATION>/config.json` # full path to this repo's location
* `export NARRATOR_ENVIRONMENT=local` # instructs the backend how to use the config 

`source ~/.bash_profile` `source ~/.zshrc` or when you're done

#### Create config file
Now, we're going to need to create the config file that the narrator will reference. In this repo directory
* [mac], sh ./scripts/create_config.sh,
* [windows], cd into scripts and ./create_config.bat
1. If you're on windows, make sure you update config.json's `pc_env` to be true.

### Java Setup
1. install java sdk.  You'll need this to compile the files and run eclipse
* for mac, `brew tap adoptopenjdk/openjdk` and then `brew install --cask adoptopenjdk8`
* for windows, I use 1.8

2. Temporarily comment or delete out the flyway block in `build.gradle`
3. Open Eclipse
4. Go to File -> Import -> Gradle> Existing Gradle Project
5. Click next/finish.  For project directory, set it as the narrator repo directory.  All the defaults should be fine
8. Run LogicTests as Junit by right clicking on LogicTests.java in the Package explorer.  It'll angrily fail.  That's okay.
9. Open run configurations by clicking the little black arrow by the green triangle.
10. Go to Junit -> Logic Tests.  Click the environment tab.  Input a new variable.  The name being `NARRATOR_CONFIG_LOCATION` and the value being the location of config.json
11. Logic tests should now run! 

### Node Setup

1. Download nodejs (currently on 12.18.3)
* for mac, I recommend installing nvm https://github.com/nvm-sh/nvm#installing-and-updating
* for pc, https://nodejs.org/en/download/
2. run `npm install`

### DB Setup
1. Install mariadb 
* for mac: https://mariadb.com/kb/en/library/installing-mariadb-on-macos-using-homebrew/
* for pc: https://chocolatey.org/packages/mariadb
* go ahead and follow all the steps until you log in.  The root password won't work until the next step
2. run `sudo mysql_secure_installation`
3. Create a database called `narrator` by opening up a mariadb terminal, and running `CREATE DATABASE narrator;`
4. Install flyway 
* for mac it's `brew install flyway`
* for pc, visit https://flywaydb.org/
5. Add the following to your environment variables (~/bash_profile), just like you did with NARRATOR_CONFIG_LOCATION
* DB_USERNAME -> your db username
* DB_PASSWORD -> your db password
* DB_HOSTNAME -> localhost
* DB_NAME -> narrator
6. Run database migrations `npm run cleanMigrate` or `npm run cleanMigrateWindows`
7. Open up config.json, and edit the following keys (they should be at the bottom of this file)
* db_username -> your db username
* db_password -> your db password
* db_hostname -> localhost
* db_name -> narrator
8. Run NodeTests with Junit by right clicking on NodeTests.java in the Package explorer.  It'll fail.  That's okay
9. Open run configurations by clicking the little black arrow by the green triangle.
10. Go to Junit -> Node Tests.  Click the environment tab.  Input a new variable.  The name being `NARRATOR_CONFIG_LOCATION` and the value being the location of config.json
11. Node Tests should now work!

### Firebase/Authentication setup
1. You'll need a copy of the firebase key.  If you don't have access to the production server, ask a code contributor.  Otherwise, 
* `scp ${SERVER_USERNAME}@narrator.systeminplace.net:/home/voss/prod/firebase.json .` to get firebase file

## Running server 

1. Whenever you make a change to the java files, you'll need to compile them so that node can use them

```sh scripts/compile.sh```

2. To start the server

```npm run dev ```

## Enabling different integrations

By default, running the api keeps all the other integrations turned off.  These integrations include

- **discord**: Enables discord integration, starting workflows initiated by channels the bot is added to.
- **firebase**: Without firebase, all external HTTP requests from different browsers look like they're coming from the same user.  Turn this on and your chrome, incognito, firefox (etc) browsers will like they're coming from different people. 
- **sc2mafia**: Enables sc2mafia integration, starting workflows initiated by pms.

You can turn on these integrations while running the back end with flags.  For example, to run with all integrations

```npm run dev -- --discord --sc2mafia --firebase```

Integration flag order does not matter.
