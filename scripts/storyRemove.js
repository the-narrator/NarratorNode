const mysql = require('mysql');

const config  = require('../config.json');


var db_connection = mysql.createConnection({
  host     : 'localhost',
  user     : config.db_username,
  password : config.db_password,
});

var game_id;

function connectDB(){
  return new Promise((resolve, reject) => {
    db_connection.connect(function(err) {
      if(err === null){
        console.log('Database connection success!\n\tUsing \'narrator\' database');
        db_connection.query('use narrator');
        resolve();
      }else
        if(keys.runningLocally){
          if(err.code !== 'ER_ACCESS_DENIED_ERROR' && err.code !== 'ECONNREFUSED'){
            console.log(err);
          }else{
            console.log("No connection to database, because this is a local session.");
            db_connection = null;
          }
        }else{
          console.log(err);
        }
    });
  })
}

function isInt(value) {
  var x;
  if (isNaN(value)) {
    return false;
  }
  x = parseFloat(value);
  return (x | 0) === x;
}

function query(qString){
  return new Promise((resolve, reject) => {
    if(!db_connection){
      resolve();
      return;
    }
    db_connection.query(qString, (err, results) => {
      if(err){
        console.log('queryError', err);
        reject(err);
      }
      else{
        resolve(results);
      }
    });
  });
}

function deleteStory(game_id){
  var promises = [];
  promises.push(query("delete from saved_games where game_id=" + game_id));
  promises.push(query("delete from commands_saved_games where game_id=" + game_id));
  promises.push(query("delete from player_saved_games where game_id=" + game_id));
  promises.push(query("delete from faction_saved_games where game_id=" + game_id));
  promises.push(query("delete from enemies_saved_games where game_id=" + game_id));
  promises.push(query("delete from sheriff_detectables_saved_games where game_id=" + game_id));
  promises.push(query("delete from roles_list_saved_games where game_id=" + game_id));
  promises.push(query("DELETE FROM roles_list_composition_saved_games WHERE game_id=" + game_id));
  promises.push(query("DELETE FROM player_input WHERE game_id=" + game_id));
  promises.push(query("delete from role_modifiers_saved_games where game_id = " + game_id));
  promises.push(query("delete from ability_makeup_saved_games where game_id = " + game_id));
  promises.push(query("delete from team_abilities_saved_games where game_id = " + game_id));
  promises.push(query("delete from team_modifiers_saved_games where game_id = " + game_id));
  return promises;
}

function deleteAll(excluded){
    var qString = 'SELECT game_id FROM saved_games';
    if(!excluded)
      qString += ' WHERE isFinished = false;';
    else
      qString += ';'
    return query(qString)
    .then((results) => {
      var promises = [];
      for(var r in results){
        game_id = results[r].game_id;
        if(!excluded || excluded.indexOf(game_id.toString()) === -1)
          promises = promises.concat(deleteStory(game_id));
      }
      return Promise.all(promises);
    });
}

connectDB()
.then(()=>{
  var args = process.argv;
  var qString;
  if(args[2] === 'all'){
    if(args.length <= 3)
      return deleteAll();
    else
      return deleteAll(args.slice(3))
  }
  if(args.length <= 2){
    return query('SELECT game_id, instance_id, date FROM saved_games WHERE isFinished = false;');
  }else if(isInt(args[2])){
    var promises = [];
    for(var i = 2; i < args.length; i++){
      game_id = args[i];
      promises = promises.concat(deleteStory(game_id));
    }

    return Promise.all(promises);
  }else{
    throw 'arguments must be none or integers or \'all\'';
  }
})
.then((results) => {
  if(process.argv.length <= 2){
    console.log(results);
  }else{
    console.log('finished deleting ');
    if(results.length)
      console.log('deleted ' + results.length + ' entries');
  }
}).catch((err) => {
  console.log(err);
}).then(() => {
  process.exit();
});
