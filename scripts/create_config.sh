#!/usr/bin/env bash
JQ_ARGS=".narrator_environment.$NARRATOR_ENVIRONMENT.db_hostname"
DB_HOSTNAME=$(cat config_template.json | jq -r $JQ_ARGS)

JQ_ARGS=".narrator_environment.$NARRATOR_ENVIRONMENT.db_name"
DB_NAME=$(cat config_template.json | jq -r $JQ_ARGS)

JQ_ARGS=".narrator_environment.$NARRATOR_ENVIRONMENT.discord_app_url"
DISCORD_BASE_URL=$(cat config_template.json | jq -r $JQ_ARGS)

JQ_ARGS=".narrator_environment.$NARRATOR_ENVIRONMENT.port_number"
PORT_NUMBER=$(cat config_template.json | jq -r $JQ_ARGS)

JQ_ARGS=".narrator_environment.$NARRATOR_ENVIRONMENT.random_role_assignment"
RANDOM_ROLE_ASSIGNMENT=$(cat config_template.json | jq -r $JQ_ARGS)

JQ_ARGS=".narrator_environment.$NARRATOR_ENVIRONMENT.test_mode"
TEST_MODE=$(cat config_template.json | jq -r $JQ_ARGS)

JQ_ARGS=".narrator_environment.$NARRATOR_ENVIRONMENT.thread_core_count"
THREAD_CORE_COUNT=$(cat config_template.json | jq -r $JQ_ARGS)

JQ_ARGS=".narrator_environment.$NARRATOR_ENVIRONMENT.thread_service_time"
THREAD_SERVICE_TIME=$(cat config_template.json | jq -r $JQ_ARGS)

JQ_ARGS=".narrator_environment.$NARRATOR_ENVIRONMENT.thread_utilization_percentage"
THREAD_UTILIZATION_PERCENTAGE=$(cat config_template.json | jq -r $JQ_ARGS)

JQ_ARGS=".narrator_environment.$NARRATOR_ENVIRONMENT.thread_wait_time"
THREAD_WAIT_TIME=$(cat config_template.json | jq -r $JQ_ARGS)

jq ".firebase_token = \"$FIREBASE_TOKEN\"" config_template.json > config.json
mv config.json config_template.json

jq ".sc2mafia.password = \"$SC2MAFIA_PASSWORD\"" config_template.json > config.json
mv config.json config_template.json

jq ".sc2mafia.api_key = \"$SC2MAFIA_API_KEY\"" config_template.json > config.json
mv config.json config_template.json

jq ".port_number = $PORT_NUMBER" config_template.json > config.json
mv config.json config_template.json

jq ".db_username = \"$DB_USERNAME\"" config_template.json > config.json
mv config.json config_template.json

jq ".db_password = \"$DB_PASSWORD\"" config_template.json > config.json
mv config.json config_template.json

jq ".db_hostname = \"$DB_HOSTNAME\"" config_template.json > config.json
mv config.json config_template.json

jq ".db_name = \"$DB_NAME\"" config_template.json > config.json
mv config.json config_template.json

jq ".test_mode = $TEST_MODE" config_template.json > config.json
mv config.json config_template.json

jq ".thread_core_count = \"$THREAD_CORE_COUNT\"" config_template.json > config.json
mv config.json config_template.json

jq ".thread_service_time = \"$THREAD_SERVICE_TIME\"" config_template.json > config.json
mv config.json config_template.json

jq ".thread_utilization_percentage = \"$THREAD_UTILIZATION_PERCENTAGE\"" config_template.json > config.json
mv config.json config_template.json

jq ".thread_wait_time = \"$THREAD_WAIT_TIME\"" config_template.json > config.json
mv config.json config_template.json

jq ".env = \"$NARRATOR_ENVIRONMENT\"" config_template.json > config.json
mv config.json config_template.json

jq ".random_role_assignment = $RANDOM_ROLE_ASSIGNMENT" config_template.json > config.json
mv config.json config_template.json

jq ".app_url = .narrator_environment.$NARRATOR_ENVIRONMENT.app_url" config_template.json > config.json
mv config.json config_template.json

jq ".discord = .narrator_environment.$NARRATOR_ENVIRONMENT.discord" config_template.json > config.json
mv config.json config_template.json

jq ".discord.master_key = \"$DISCORD_MASTER_KEY\"" config_template.json > config.json
mv config.json config_template.json

jq ".discord.bot_keys = \"$DISCORD_BOT_KEYS\"" config_template.json > config.json

jq ".discord.master_key = \"\"" config_template.json > config_template.json2
mv config_template.json2 config_template.json

jq ".sc2mafia.password = \"\"" config_template.json > config_template.json2
mv config_template.json2 config_template.json

jq ".sc2mafia.api_key = \"\"" config_template.json > config_template.json2
mv config_template.json2 config_template.json
