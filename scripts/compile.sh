#!/usr/bin/env bash
find src -type f -name "*.class" -delete

javac -cp src/main/java src/main/java/App.java
javac -cp src/main/java src/main/java/nnode/HappeningGenerator.java
javac -cp src/main/java src/main/java/nnode/SeedFinder.java
javac -cp src/main/java src/main/java/nnode/StatRefresher.java
