@echo off

cd ../

set JQ_ARGS=".narrator_environment.%NARRATOR_ENVIRONMENT%.db_hostname"
type config_template.json | jq.exe -r %JQ_ARGS% > temp.txt
set /p DB_HOSTNAME=<temp.txt

set JQ_ARGS=".narrator_environment.%NARRATOR_ENVIRONMENT%.db_name"
type config_template.json | jq -r %JQ_ARGS% > temp.txt
set /p DB_NAME=<temp.txt

set JQ_ARGS=".narrator_environment.%NARRATOR_ENVIRONMENT%.port_number"
type config_template.json | jq -r %JQ_ARGS% > temp.txt
set /p PORT_NUMBER=<temp.txt

set JQ_ARGS=".narrator_environment.%NARRATOR_ENVIRONMENT%.random_role_assignment"
type config_template.json | jq -r %JQ_ARGS% > temp.txt
set /p RANDOM_ROLE_ASSIGNMENT=<temp.txt

set JQ_ARGS=".narrator_environment.%NARRATOR_ENVIRONMENT%.test_mode"
type config_template.json | jq -r %JQ_ARGS% > temp.txt
set /p TEST_MODE=<temp.txt

set JQ_ARGS=".narrator_environment.%NARRATOR_ENVIRONMENT%.thread_core_count"
type config_template.json | jq -r %JQ_ARGS% > temp.txt
set /p THREAD_CORE_COUNT=<temp.txt

set JQ_ARGS=".narrator_environment.%NARRATOR_ENVIRONMENT%.thread_service_time"
type config_template.json | jq -r %JQ_ARGS% > temp.txt
set /p THREAD_SERVICE_TIME=<temp.txt

set JQ_ARGS=".narrator_environment.%NARRATOR_ENVIRONMENT%.thread_utilization_percentage"
type config_template.json | jq -r %JQ_ARGS% > temp.txt
set /p THREAD_UTILIZATION_PERCENTAGE=<temp.txt

set JQ_ARGS=".narrator_environment.%NARRATOR_ENVIRONMENT%.thread_wait_time"
type config_template.json | jq -r %JQ_ARGS% > temp.txt
set /p THREAD_WAIT_TIME=<temp.txt

jq ".firebase_token = \"%FIREBASE_TOKEN%\"" config_template.json > config.json
copy config.json config_template_temp.json

jq ".sc2mafia.password = \"%SC2MAFIA_PASSWORD%\"" config_template_temp.json > config.json
copy config.json config_template_temp.json

jq ".sc2mafia.api_key = \"%SC2MAFIA_API_KEY%\"" config_template_temp.json > config.json
copy config.json config_template_temp.json

jq ".port_number = %PORT_NUMBER%" config_template_temp.json > config.json
copy config.json config_template_temp.json

jq ".db_username = \"%DB_USERNAME%\"" config_template_temp.json > config.json
copy config.json config_template_temp.json

jq ".db_password = \"%DB_PASSWORD%\"" config_template_temp.json > config.json
copy config.json config_template_temp.json

jq ".db_hostname = \"%DB_HOSTNAME%\"" config_template_temp.json > config.json
copy config.json config_template_temp.json

jq ".db_name = \"%DB_NAME%\"" config_template_temp.json > config.json
copy config.json config_template_temp.json

jq ".thread_core_count = \"%THREAD_CORE_TIME%\"" config_template_temp.json > config.json
copy config.json config_template_temp.json

jq ".thread_service_time = \"%THREAD_SERVICE_TIME%\"" config_template_temp.json > config.json
copy config.json config_template_temp.json

jq ".thread_utilization_percentage = \"%THREAD_UTILIZATION_PERCENTAGE%\"" config_template_temp.json > config.json
copy config.json config_template_temp.json

jq ".thread_wait_time = \"%THREAD_WAIT_TIME%\"" config_template_temp.json > config.json
copy config.json config_template_temp.json

jq ".test_mode = %TEST_MODE%" config_template_temp.json > config.json
copy config.json config_template_temp.json

jq ".env = \"%NARRATOR_ENVIRONMENT%\"" config_template_temp.json > config.json
copy config.json config_template_temp.json

jq ".random_role_assignment = %RANDOM_ROLE_ASSIGNMENT%" config_template_temp.json > config.json
copy config.json config_template_temp.json

jq ".app_url = .narrator_environment.%NARRATOR_ENVIRONMENT%.app_url" config_template_temp.json > config.json
copy config.json config_template_temp.json

jq ".discord = .narrator_environment.%NARRATOR_ENVIRONMENT%.discord" config_template_temp.json > config.json
copy config.json config_template_temp.json

jq ".discord.master_key = \"%DISCORD_MASTER_KEY%\"" config_template_temp.json > config.json
copy config.json config_template_temp.json

jq ".discord.bot_keys = \"%DISCORD_BOT_KEYS%\"" config_template_temp.json > config.json

del temp.txt
del config_template_temp.json

cd scripts

rem jq ".discord.master_key = \"\"" config_template.json > config_template.json2
rem copy config_template.json2 config_template.json

rem jq ".sc2mafia.password = \"\"" config_template.json > config_template.json2
rem copy config_template.json2 config_template.json

rem jq ".sc2mafia.api_key = \"\"" config_template.json > config_template.json2
rem copy config_template.json2 config_template.json
