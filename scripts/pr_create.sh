#!/usr/bin/env bash
CURRENT_BRANCH=$(git symbolic-ref --short -q HEAD)
FROM_BRANCH=$(LC_ALL=C tr -dc 'A-Za-z0-9' </dev/urandom | head -c 16)
TO_BRANCH=$(LC_ALL=C tr -dc 'A-Za-z0-9' </dev/urandom | head -c 16)
echo $FROM_BRANCH

git checkout -b $FROM_BRANCH

git checkout -b $TO_BRANCH
git reset --hard HEAD^

git push --set-upstream origin $TO_BRANCH

git checkout $FROM_BRANCH
git push --set-upstream origin $FROM_BRANCH -o merge_request.create -o merge_request.target=$TO_BRANCH

git checkout $CURRENT_BRANCH

git branch -D $FROM_BRANCH
git branch -D $TO_BRANCH
