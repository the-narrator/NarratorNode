const { TestBot } = require('./discordTestBot');
const Constants = require('./Constants');

const tBots = [];

for(let i = 1; i <= 7; i++)
    tBots.push(getDiscordTestBot(i));


function getDiscordTestBot(t){
    return new TestBot(t);
}

function connectBots(){
    const ps = [];

    for(let i = 0; i < tBots.length; i++)
        ps.push(tBots.join());


    return Promise.all(ps);
}

connectBots()
    .then(() => {
        console.log('bots connected');
        tBots[0].waitForMessage(m => {
            if(m.content === '!host')
                for(let i = 0; i < tBots.length; i++)
                    tBots[i].join();
        });
    }).catch(err => {
        console.log(err);
    });
